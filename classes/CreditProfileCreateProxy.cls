global class CreditProfileCreateProxy {
    
    /**/
    @invocablemethod
    global static void invocablemethod(List<ID> profileIds){ 
        if(profileIds!=null&&profileIds.size()!=0)
            for (ID cpId:profileIds){    
                if(!Test.isRunningTest())
                    CreditProfileCalloutUpdate.createBusinessCreditProfile(
                        getBusinessCustomerId(cpId)
                        ,cpId);
            }
    }
    
    private static Long getBusinessCustomerId(ID profileId){
        Long businessCustomerId;
        Credit_Profile__c cp = [select id, name
                                ,Account__r.CustProfId__c
                                from Credit_Profile__c
                                where id = :profileId];
        if(cp.Account__r!=null&&cp.Account__r.CustProfId__c!=null) 
            businessCustomerId = Long.valueof(cp.Account__r.CustProfId__c);
        return businessCustomerId;
    }
    
}