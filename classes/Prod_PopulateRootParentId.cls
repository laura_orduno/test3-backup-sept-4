global with sharing class Prod_PopulateRootParentId implements Database.Batchable<sObject>
{
    public String getParentLN(String ln) 
    {
        String retval = ''; 
        Integer lnz;
        String[] lns = ln.split('\\.');
                lnz = lns.size()-1;
                lns.remove(lnz); 
                retval =  String.join(lns,'.');
        return retval;
    }

    private String getRootLineNumber(String lineNum)
    {
        String[] lns = lineNum.split('\\.');
        return lns[0];
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        string query ='Select Id, AccountId, vlocity_cmt__ServiceAccountId__c, vlocity_cmt__rootItemId__c,vlocity_cmt__parentItemId__c,vlocity_cmt__LineNumber__c from Asset where (orderMgmt_BPI_Id__c <> NULL and vlocity_cmt__LineNumber__c <> NULL and Account.RecordType.DeveloperName = \'RCID\' AND Account.InActive__c = FALSE)';
        
        return Database.getQueryLocator(query);   
    }

    Map<String, List<Asset>> accountIdListAssetIds = new Map<String, List<Asset>>();
    //Map<String,String> lineNumberToAssetId = new Map<String,String>();
    Map<String,Map<String,String>> lineNumberToAssetId = new Map<String,Map<String,String>>();
    Map<String,Map<String,String>> lineNumToRootParentLineNumber = new Map<String,Map<String,String>>();
    List<Asset> assetsToBeUpdated = new List<Asset>();

    global void execute(Database.BatchableContext BC,List<Asset> Assets)
    {
        string assetQuery ='Select Id, AccountId, vlocity_cmt__ServiceAccountId__c, vlocity_cmt__rootItemId__c,vlocity_cmt__parentItemId__c,vlocity_cmt__LineNumber__c from Asset where orderMgmt_BPI_Id__c <> NULL and vlocity_cmt__LineNumber__c <> NULL and Account.RecordType.DeveloperName = \'RCID\' AND Account.InActive__c = FALSE ';
        assetsToBeUpdated.clear();
        Set<Id> accIds = new Set<Id>();
        for(Asset asset:Assets){
           // if(acc.Assets.size()>0){
                accIds.add(asset.vlocity_cmt__ServiceAccountId__c);
            //}
        }
        assetQuery+='AND vlocity_cmt__ServiceAccountId__c IN :accIds FOR UPDATE';
        system.debug('AssetQuery'+assetQuery);
        List<Asset> totalassetList = new List<Asset>();
        totalassetList = Database.query(assetQuery);
        system.debug('totalassetList -- '+totalassetList);
        
            for(Asset asset1:totalassetList)
            { 
                if(accountIdListAssetIds.containsKey(asset1.AccountId))
                {
                    Asset asset = asset1;
                    accountIdListAssetIds.get(asset1.AccountId).add(asset);
                }
                else
                {
                    accountIdListAssetIds.put(asset1.AccountId,new List<Asset>{asset1});
                }
            }
        
        system.debug('accountIdListAssetIds---->'+accountIdListAssetIds.keySet());
        //Map<AccountId,Map<Linenum,assetid>>
        //
        for(String accountId:accountIdListAssetIds.keySet())
        {
            List<Asset> assetList = new List<Asset>();
            assetList = accountIdListAssetIds.get(accountId);
            for(Asset asset:assetList)
            {
                String lineNumber = asset.vlocity_cmt__LineNumber__c;
                String assetId = asset.Id;
                //lineNumberToAssetId.put(lineNumber,assetId);
                if(lineNumberToAssetId.containsKey(accountId))
                {
                    lineNumberToAssetId.get(accountId).put(lineNumber,assetId);
                }
                else
                {
                    lineNumberToAssetId.put(accountId,new Map<String,String>{lineNumber => assetId});
                }
            }
        }

        system.debug('lineNumberToAssetId -- '+JSON.serializePretty(lineNumberToAssetId));
        for(String accountId:accountIdListAssetIds.keySet())
        {
            List<Asset> assetList = new List<Asset>();
            assetList = accountIdListAssetIds.get(accountId);
            for(Asset asset:assetList)
            {
                String lineNum = asset.vlocity_cmt__LineNumber__c;
                //Map the lineNum to its rootId and parent line num

                //Eg (001.001.001,{('RootId','001')}
                //Eg (001.001.001,{('ParentId','001.001')}
                Map<String,String> parentLineNumMap = new Map<String,String>();
                if(lineNum != null)
                {
                    parentLineNumMap.put('ParentId',getParentLN(lineNum));
                    lineNumToRootParentLineNumber.put(lineNum,parentLineNumMap);
                    
                    //get the root line number from the line number
                    String rootLineNum = getRootLineNumber(lineNum);
                    lineNumToRootParentLineNumber.get(lineNum).put('RootId',rootLineNum);
                } 
            }      
        }

        system.debug('lineNumToRootParentLineNumber -- '+JSON.serializePretty(lineNumToRootParentLineNumber));
        for(String accountId : accountIdListAssetIds.keySet())
        {
            List<Asset> assetList = new List<Asset>();
            assetList = accountIdListAssetIds.get(accountId);
            for(Asset asset:assetList)
            {   
                String lineNum = asset.vlocity_cmt__LineNumber__c;
                system.debug('lineNumber'+lineNum);
                if(lineNum != null)
                {
                    system.debug('lineNumToRootParentLineNumber'+lineNumToRootParentLineNumber+'lineNumToRootParentLineNumber'+lineNumToRootParentLineNumber.containsKey(lineNum));
                    if(lineNumToRootParentLineNumber != null && lineNumToRootParentLineNumber.containsKey(lineNum))
                    {
                        system.debug('lineNumberToAssetId'+lineNumberToAssetId+'lineNumberToAssetId COntains'+lineNumberToAssetId.containsKey(lineNumToRootParentLineNumber.get(lineNum).get('RootId')));
                        //if(lineNumberToAssetId != null && lineNumberToAssetId.containsKey(lineNumToRootParentLineNumber.get(lineNum).get('RootId')))
                        if(lineNumberToAssetId != null && lineNumberToAssetId.containsKey(accountId))
                        {
                            if(lineNumberToAssetId.get(accountId).containsKey(lineNumToRootParentLineNumber.get(lineNum).get('RootId')))
                            {
                               
                                //get the id for the rootLineNum for the current line item
                                system.debug('updating rootItem Id');
                                //Id rootId = lineNumberToAssetId.get(lineNumToRootParentLineNumber.get(lineNum).get('RootId'));
                                system.debug('rootid -- '+lineNumberToAssetId.get(accountId).get(lineNumToRootParentLineNumber.get(lineNum).get('RootId')));
                                Id rootId = lineNumberToAssetId.get(accountId).get(lineNumToRootParentLineNumber.get(lineNum).get('RootId'));
                                asset.vlocity_cmt__RootItemId__c = rootId;
                            }
                        }
                        if(lineNumberToAssetId != null && lineNumberToAssetId.containsKey(accountId))
                        {
                            if(lineNumberToAssetId.get(accountId).containsKey(lineNumToRootParentLineNumber.get(lineNum).get('ParentId')))
                            {
                                //get the id for the parentLineNum for the current line item
                                system.debug('updating parentItem Id');
                                Id parentId = lineNumberToAssetId.get(accountId).get(lineNumToRootParentLineNumber.get(lineNum).get('ParentId'));
                                asset.vlocity_cmt__ParentItemId__c = parentId;
                            }  
                        }
                    }
                    assetsToBeUpdated.add(asset);
                }
            }
        }
        system.debug('assetstoUpdate'+assetsToBeUpdated.size());
        system.debug('assetsToBeUpdated -- '+JSON.serializePretty(assetsToBeUpdated));
        if(assetsToBeUpdated != null && assetsToBeUpdated.size() > 0)
            update assetsToBeUpdated;
    }
    
    
    global void finish(Database.BatchableContext BC){
    }
}