/*****************************************************
    Name: SendEmailFromContactOrLead

    Usage: This class used  for update their information for contact or lead and
           save the datetime for last stay in touch

    Author – Clear Task

    Date – 04/02/2011
    
    Revision History
******************************************************/
global with sharing class SendEmailFromContactOrLead{ 
 /*
  * Variable in SendEmailFromContactOrLead Class
  */ 
   // used for contact Id after click on 'Send Email' Button in Contact
   private String contactId=ApexPages.currentPage().getParameters().get('contId');
   // used for lead Id after click on 'Send Email' Button in Lead
   private String leadId=ApexPages.currentPage().getParameters().get('leadId');     
   // used for display Message for Mail has been done or not
   private String emailStatus; 
   // used in Email Template for Link  
   //private String templateEmailLink='Please Click: ' ;
   //used to store Contact record
   private Contact updateContact;
   //used for store Lead Record
   private Lead updateLead;
   //used in Subject part for Email
   public static String EMAIL_SUBJECT= StayInTouchEmailTemplate__c.getValues('Email Subject').EmailTemplateName__c ;  
   public static String EMAIL_TEMPLATE_CONTACT = StayInTouchEmailTemplate__c.getValues('Contact Email Template').EmailTemplateName__c ;  
   public static String EMAIL_TEMPLATE_LEAD = StayInTouchEmailTemplate__c.getValues('Lead Email Template').EmailTemplateName__c ;      
   public string  isContactOrLead{get; set;}
   private String isLead= 'Lead';
   public String strUrl{get; set;}
   public String tabStyle{get; set;} 
   
   
 //private string isContactOrLeadTab='Lead'; 
   public String getIsLead(){
       return isLead;
   }
  // Map<String,SiteSettings__c> siteSettings = SiteSettings__c.getAll();
 /*
  * Constructure of SendEmailFromContactOrLead Class
  */
   public SendEmailFromContactOrLead(){ 
        tabStyle = 'Contact';     
        String strRandomNumber=RandomStringutils.randomGUID();
        if(contactId !=null && contactId != '') {
             updateContact=[Select c.FirstName ,c.LastName ,c.Email , c.Last_Stay_In_Touch_Email_Sent_Date__c, c.GUID__c From Contact c where c.id=:contactId ];
             updateContact.GUID__c=strRandomNumber;
        } 
        if(leadId !=null && leadId != '') {
             updateLead=[Select l.FirstName ,l.LastName, l.Email , l.Last_Stay_In_Touch_Email_Sent_Date__c, l.GUID__c From Lead l where l.id=:leadId ];             
             updateLead.GUID__c=strRandomNumber;        
        }
        //using Custom Settings to get the site domain name
        //strUrl = SiteSettings__c.getValues('Email Link').Site_URL__c + '/UpdateInfo?';        
        //templateEmailLink = strUrl ;
   }
 /*
  * sendEmailStatus methid is for update the record with current timestamp with GUID and sent the mail
  */
   public void sendEmailStatus(){
        string userEmail= ''; 
        try{
          if(updateContact != null && contactId !=null && contactId != '') {              
              updateContact.Last_Stay_In_Touch_Email_Sent_Date__c=System.now();             
              update updateContact;
              userEmail=updateContact.Email; 
              isContactOrLead= 'Contact: '+updateContact.FirstName +' '+updateContact.LastName ;
              List<String> toMail =new List<String>(); 
              toMail.add(userEmail);
              List<String> ccEmailIdList =new List<String>();
              List<String> bccEmailIdList =new List<String>();
              try{ 
                  sendEmailTemplate(toMail, ccEmailIdList, bccEmailIdList, EMAIL_TEMPLATE_CONTACT , updateContact);
                  emailStatus=System.Label.mail_has_been_sent_successfully + ' ' + userEmail;
                  tabStyle = 'Contact';
              }catch(EmailException e){                  
                  emailStatus=System.Label.mail_has_not_been_sent_successfully + ' ' + userEmail;
                  System.debug('Inside Catch After Sending Email ::'+emailStatus);     
                  updateLead.Last_Stay_In_Touch_Email_Sent_Date__c=null;              
                  updateLead.GUID__c='';
                  update updateContact;          
              }
          }
          if(updateLead != null && leadId !=null && leadId != '') {              
              updateLead.Last_Stay_In_Touch_Email_Sent_Date__c=System.now();
              update updateLead;
              userEmail=updateLead.Email;
              isContactOrLead= 'Lead: '+updateLead.FirstName +' '+updateLead.LastName;
              List<String> toMail =new List<String>(); 
              toMail.add(userEmail);
              List<String> ccEmailIdList =new List<String>();
              List<String> bccEmailIdList =new List<String>();
              try{  
                  sendEmailTemplate(toMail, ccEmailIdList, bccEmailIdList, EMAIL_TEMPLATE_LEAD, updateLead);
                  emailStatus=System.Label.mail_has_been_sent_successfully + ' ' + userEmail;
                  tabStyle = 'Lead';
              }catch(EmailException e){                  
                  emailStatus=System.Label.mail_has_not_been_sent_successfully + ' ' + userEmail;
                  System.debug('Inside Catch After Sending Email ::'+emailStatus);     
                  updateLead.Last_Stay_In_Touch_Email_Sent_Date__c=null;              
                  updateLead.GUID__c='';
                  update updateLead;          
              } 
          }          
        }catch(Exception e){
            emailStatus=System.Label.mail_and_Update_has_not_been_done_successfully;
        }          
  }
 /*
  * getEmailStatus method for return the status of updation and Email 
  */
  public String getEmailStatus(){
    return emailStatus;
  }
 /*
  * cancel  method for return to previous page while its from Contact or Lead
  */
  public PageReference cancel(){
    if(contactId != null){
        return new PageReference('/'+contactId);
    }else if(leadId != null){
        return new PageReference('/'+leadId);
    }
    return null;
  }
  /*
  * sendEmailTemplate method for send Email by Template
  */
  public void sendEmailTemplate(List<String> toMail, List<String> ccEmailIdList, List<String> bccEmailIdList, String strTemplateName, SObject sObj){
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setToAddresses(toMail);
        if(ccEmailIdList != null && ccEmailIdList.size()>0){
            email.setCcAddresses(ccEmailIdList);
        }
        if(bccEmailIdList != null && bccEmailIdList.size()>0){
            email.setBccAddresses(bccEmailIdList);
            email.setBccSender(true);
        }
        EmailTemplate templateName = [select id, ownerid from EmailTemplate where Name =:strTemplateName];
        email.setTemplateId(templateName.id);
        email.setTargetObjectId(templateName.ownerid); 
        email.setWhatId(sObj.Id);  
        email.setSaveAsActivity(false);                       
        try{           
            Messaging.SendEmailResult [] r =Messaging.sendEmail(new Messaging.SingleEmailMessage [] {email});         
        }catch(Exception e){ } 
    }   
}