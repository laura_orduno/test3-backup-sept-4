@isTest
private class OCOM_CreateContractStepTest {
     private static Id createData(){  
        Id contractReqRecId = Contracts__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('eContract').getRecordTypeId();
        Id contractRecId = Contract.SObjectType.getDescribe().getRecordTypeInfosByName().get('Corporate Wireless').getRecordTypeId();
		Id vlContractRecId = Contract.SObjectType.getDescribe().getRecordTypeInfosByName().get('Vlocity Contracts').getRecordTypeId();
		Id baContractRecId = Contract.SObjectType.getDescribe().getRecordTypeInfosByName().get('Business Anywhere').getRecordTypeId();
		
		
        Id contractLineRecordId= vlocity_cmt__ContractLineItem__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('eContract Line Item').getRecordTypeId();
        Id rcidAccntRecordTypeId=Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        Account acct = new Account(Name='testacct', Phone='9222022768', RecordTypeId=rcidAccntRecordTypeId);
        insert acct;
        Contact testContactObj = new Contact(firstname = 'TestConFirstName', LastName = 'TestConLastname', accountId = acct.Id , 
                                             Phone = '123456' , fax = '1234567', Email = 'test@gmail.com');
        insert testContactObj;
        Opportunity opp = new Opportunity(AccountId=acct.id, Name='testopp', CloseDate=system.today(),Probability=0,StageName='test stage');
        insert opp;        
        Contracts__c ConReq= new Contracts__c(RecordTypeId=contractReqRecId,Contract_Type__c='New',Opportunity__c=opp.id );
        insert ConReq;   
        Contract testContractObj = new Contract(Complex_Fulfillment_Contract__c=ConReq.id,Status='Draft',Contract_Type__c='Amendment',
                                                AccountId=acct.id,ContractTerm=12,StartDate=system.today(),
                                                Customer_Signor__c=testContactObj.id,TELUS_Signor__c=userinfo.getuserid(), Agreement_Type__c = 'Amendment',Effective_Date__c=Datetime.now());
        insert testContractObj;
		
        
        Product2 testProductObj = new Product2(Name = 'Laptop X200', 
                                               Family = 'Hardware' ,vlocity_cmt__TrackAsAgreement__c = TRUE, Sellable__c = true);
        insert testProductObj;
        List<vlocity_cmt__ContractLineItem__c> testContractLineItemList =new List<vlocity_cmt__ContractLineItem__c>();
        vlocity_cmt__ContractLineItem__c cli1 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=9,TELUS_Printed_Product_Name__c = testProductObj.Name,vlocity_cmt__Product2Id__c=testProductObj.id,vlocity_cmt__ContractId__c=testContractObj.id,vlocity_cmt__OneTimeTotal__c=12,name='test0',vlocity_cmt__OneTimeCharge__c=2,vlocity_cmt__Quantity__c=12,vlocity_cmt__LineNumber__c = '0001');
        testContractLineItemList.add(cli1);
        
        vlocity_cmt__ContractLineItem__c cli2 = new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=6,vlocity_cmt__Product2Id__c=testProductObj.id,vlocity_cmt__ContractId__c=testContractObj.id,vlocity_cmt__OneTimeTotal__c=6,name='test1',vlocity_cmt__OneTimeCharge__c=3,vlocity_cmt__Quantity__c=12,TELUS_Printed_Product_Name__c = testProductObj.Name,vlocity_cmt__LineNumber__c = '0001.0001');
        
        testContractLineItemList.add(cli2);
        vlocity_cmt__ContractLineItem__c cli3  = new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=3,vlocity_cmt__Product2Id__c=testProductObj.id,vlocity_cmt__ContractId__c=testContractObj.id,vlocity_cmt__OneTimeTotal__c=5,name='test2',vlocity_cmt__OneTimeCharge__c=4,vlocity_cmt__Quantity__c=12,TELUS_Printed_Product_Name__c = testProductObj.Name,vlocity_cmt__LineNumber__c = '0002');
        testContractLineItemList.add(cli3);
        
        vlocity_cmt__ContractLineItem__c cli4 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=3,vlocity_cmt__Product2Id__c=testProductObj.id,vlocity_cmt__ContractId__c=testContractObj.id,vlocity_cmt__OneTimeTotal__c=5,name='test3',vlocity_cmt__OneTimeCharge__c=4,vlocity_cmt__Quantity__c=12,TELUS_Printed_Product_Name__c = testProductObj.Name,vlocity_cmt__LineNumber__c = '0002.0001');
        
        testContractLineItemList.add(cli4);
        insert  testContractLineItemList;
        
       Contract testContractObj1 = new Contract(Complex_Fulfillment_Contract__c=ConReq.id,Status='Draft',Contract_Type__c='Amendment',
                                                AccountId=acct.id,ContractTerm=12,StartDate=system.today(),
                                                Customer_Signor__c=testContactObj.id,TELUS_Signor__c=userinfo.getuserid(), Agreement_Type__c = 'Amendment',Effective_Date__c=Datetime.now());
        insert testContractObj1;
        
		//testContractObj.status='Draft';
		update testContractObj;
		
        testContractObj.InitialContract__c=String.valueOf(testContractObj1.id).substring(0,7);       
		testContractObj.status='Contract Registered';
		testContractObj.vlocity_cmt__OriginalContractId__c=testContractObj1.id;
		update testContractObj;
		testContractObj.recordtypeid=contractRecId;
		testContractObj.InitialContract__c=null;
		testContractObj.Contract_Type__c=null;
		update testContractObj;
		
		testContractObj.recordtypeid=vlContractRecId;
		update testContractObj;
		
		testContractObj.recordtypeid=baContractRecId;
		update testContractObj;
		return testContractObj.id;
		
    }
    @testSetup
    public static void testDataSetup(){
        Map<String,Object> TestinputMap = new Map<String,Object>();
        Map<String,Object> TestoutMap = new Map<String,Object>();
    }
    
    public static void insertFieldMapper(){
        String nameSpaceprefix = 'vlocity_cmt__'; 
        /*Vlocity_cmt__CustomFieldMap__c customFieldMap1 = new Vlocity_cmt__CustomFieldMap__c(  Name='OrderItem>ContractLineItem__c0',
                                                                                            Vlocity_cmt__SourceFieldName__c = 'ListPrice',
                                                                                            Vlocity_cmt__DestinationFieldName__c =nameSpaceprefix+'UnitPrice__c',
                                                                                            Vlocity_cmt__DestinationSObjectType__c=nameSpaceprefix+'ContractLineItem__c',
                                                                                            vlocity_cmt__SourceFieldType__c='currency',                                  
                                                                                            Vlocity_cmt__SourceSObjectType__c='OrderItem');
        Vlocity_cmt__CustomFieldMap__c customFieldMap2 = new Vlocity_cmt__CustomFieldMap__c(  Name='OrderItem>ContractLineItem__c1',
                                                                                            Vlocity_cmt__SourceFieldName__c = 'Quantity',
                                                                                            Vlocity_cmt__DestinationFieldName__c =nameSpaceprefix+'Quantity__c',
                                                                                            Vlocity_cmt__DestinationSObjectType__c=nameSpaceprefix+'ContractLineItem__c',
                                                                                            Vlocity_cmt__SourceFieldType__c='double',
                                                                                            Vlocity_cmt__SourceSObjectType__c='OrderItem');
        Vlocity_cmt__CustomFieldMap__c customFieldMap3 = new Vlocity_cmt__CustomFieldMap__c(  Name='OrderItem>ContractLineItem__c2',
                                                                                            Vlocity_cmt__SourceFieldName__c = 'vlocity_cmt__LineNumber__c',
                                                                                            Vlocity_cmt__DestinationFieldName__c = 'vlocity_cmt__LineNumber__c',
                                                                                            Vlocity_cmt__SourceFieldType__c='string',
                                                                                            Vlocity_cmt__DestinationSObjectType__c=nameSpaceprefix+'ContractLineItem__c',
                                                                                            Vlocity_cmt__SourceSObjectType__c='OrderItem');
        Vlocity_cmt__CustomFieldMap__c customFieldMap4 = new Vlocity_cmt__CustomFieldMap__c(  Name='OrderItem>ContractLineItem__c3',
                                                                                            Vlocity_cmt__SourceFieldName__c = 'PricebookEntryId',
                                                                                            Vlocity_cmt__DestinationFieldName__c =nameSpaceprefix+'PricebookEntryId__c',
                                                                                            Vlocity_cmt__SourceFieldSObjectType__c='PricebookEntry',
                                                                                            Vlocity_cmt__SourceFieldType__c='reference',
                                                                                            Vlocity_cmt__DestinationSObjectType__c=nameSpaceprefix+'ContractLineItem__c',
                                                                                            Vlocity_cmt__SourceSObjectType__c='OrderItem');  
        Vlocity_cmt__CustomFieldMap__c customFieldMap5 = new Vlocity_cmt__CustomFieldMap__c(Name='OrderItem>ContractLineItem__c4',
                                                                                            Vlocity_cmt__SourceFieldName__c = 'vlocity_cmt__RecurringTotal__c',
                                                                                            Vlocity_cmt__DestinationFieldName__c ='vlocity_cmt__RecurringTotal__c',
                                                                                            
                                                                                            Vlocity_cmt__SourceFieldType__c='currency',
                                                                                            Vlocity_cmt__DestinationSObjectType__c=nameSpaceprefix+'ContractLineItem__c',
                                                                                            Vlocity_cmt__SourceSObjectType__c='OrderItem');  */
        Vlocity_cmt__CustomFieldMap__c customFieldMap1 = new Vlocity_cmt__CustomFieldMap__c(	Name='OrderItem>ContractLineItem__c0',
																	Vlocity_cmt__SourceFieldName__c = 'ListPrice',
																	Vlocity_cmt__DestinationFieldName__c =nameSpaceprefix+'UnitPrice__c',
																	Vlocity_cmt__DestinationSObjectType__c=nameSpaceprefix+'ContractLineItem__c',
																	vlocity_cmt__SourceFieldType__c='currency',																	
																	Vlocity_cmt__SourceSObjectType__c='OrderItem');
         Vlocity_cmt__CustomFieldMap__c customFieldMap2 = new Vlocity_cmt__CustomFieldMap__c(	Name='OrderItem>ContractLineItem__c1',
																	Vlocity_cmt__SourceFieldName__c = 'Quantity',
																	Vlocity_cmt__DestinationFieldName__c =nameSpaceprefix+'Quantity__c',
																	Vlocity_cmt__DestinationSObjectType__c=nameSpaceprefix+'ContractLineItem__c',
																	Vlocity_cmt__SourceFieldType__c='double',
																	Vlocity_cmt__SourceSObjectType__c='OrderItem');
		 Vlocity_cmt__CustomFieldMap__c customFieldMap3 = new Vlocity_cmt__CustomFieldMap__c(	Name='OrderItem>ContractLineItem__c2',
																	Vlocity_cmt__SourceFieldName__c = 'vlocity_cmt__LineNumber__c',
																	Vlocity_cmt__DestinationFieldName__c = 'vlocity_cmt__LineNumber__c',
																	Vlocity_cmt__SourceFieldType__c='string',
																	Vlocity_cmt__DestinationSObjectType__c=nameSpaceprefix+'ContractLineItem__c',
																	Vlocity_cmt__SourceSObjectType__c='OrderItem');
		 Vlocity_cmt__CustomFieldMap__c customFieldMap4 = new Vlocity_cmt__CustomFieldMap__c(	Name='OrderItem>ContractLineItem__c3',
																	Vlocity_cmt__SourceFieldName__c = 'PricebookEntryId',
																	Vlocity_cmt__DestinationFieldName__c =nameSpaceprefix+'PricebookEntryId__c',
																	Vlocity_cmt__SourceFieldSObjectType__c='PricebookEntry',
																	Vlocity_cmt__SourceFieldType__c='reference',
																	Vlocity_cmt__DestinationSObjectType__c=nameSpaceprefix+'ContractLineItem__c',
																	Vlocity_cmt__SourceSObjectType__c='OrderItem');	
		  Vlocity_cmt__CustomFieldMap__c customFieldMap5 = new Vlocity_cmt__CustomFieldMap__c(Name='OrderItem>ContractLineItem__c4',
																	Vlocity_cmt__SourceFieldName__c = 'vlocity_cmt__RecurringTotal__c',
																	Vlocity_cmt__DestinationFieldName__c ='vlocity_cmt__RecurringTotal__c',
																	
																	Vlocity_cmt__SourceFieldType__c='currency',
																	Vlocity_cmt__DestinationSObjectType__c=nameSpaceprefix+'ContractLineItem__c',
																	Vlocity_cmt__SourceSObjectType__c='OrderItem');
        Vlocity_cmt__CustomFieldMap__c customFieldMap6 = new Vlocity_cmt__CustomFieldMap__c(Name='ConReq>Contract0',
                                                                                            Vlocity_cmt__SourceFieldName__c = 'Account__c',
                                                                                            Vlocity_cmt__DestinationFieldName__c ='AccountId',
                                                                                            Vlocity_cmt__DestinationSObjectType__c='Contract',
                                                                                            Vlocity_cmt__SourceFieldType__c='reference',
                                                                                            Vlocity_cmt__SourceSObjectType__c='Contracts__c');
        Vlocity_cmt__CustomFieldMap__c customFieldMap7 = new Vlocity_cmt__CustomFieldMap__c(Name='ConReq>Contract1',
                                                                                            Vlocity_cmt__SourceFieldName__c = 'order__c',
                                                                                            Vlocity_cmt__DestinationFieldName__c =nameSpaceprefix+'OrderId__c',
                                                                                            Vlocity_cmt__DestinationSObjectType__c='Contract',
                                                                                            Vlocity_cmt__SourceFieldType__c='reference',
                                                                                            Vlocity_cmt__SourceSObjectType__c='Contracts__c'); 
        List<Vlocity_cmt__CustomFieldMap__c> listCustomFieldMaps = new List<Vlocity_cmt__CustomFieldMap__c> ();
        listCustomFieldMaps.add(customFieldMap1);
        listCustomFieldMaps.add(customFieldMap2);
        listCustomFieldMaps.add(customFieldMap3);
        listCustomFieldMaps.add(customFieldMap4);
        listCustomFieldMaps.add(customFieldMap5);
        listCustomFieldMaps.add(customFieldMap6);
        listCustomFieldMaps.add(customFieldMap7);
        insert listCustomFieldMaps;
    }  
    
  
    
    static testMethod void generateOrUpdateContractTest(){
        
        String methodName             = 'getContractDetails';
        Map<String,Object> outMap     =  new Map<String,Object>();
        Map<String,Object> options    =  new Map<String,Object>();
        Map<String,Object> inputMap   =  new Map<String,Object>(); 
        Map<String,Object> typeConVal =  new Map<String,Object>();
        Map<String,Object> CrtContVal =  new Map<String,Object>();
        
         
        Map<String,Object> TestinputMap = new Map<String,Object>();
        Map<String,Object> TestoutMap = new Map<String,Object>();
        OCOM_OrderProductBeforeTrigger_Helper.isNotApplicable=true;
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        
        
        Order ordObj=[select opportunityid,Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,
                             RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c,orderMgmtId__c 
                      FROM   Order where id=:orderId];
        ordObj.orderMgmtId__c='OS1234567890';
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        update ordObj;
        insertFieldMapper();
        
        
        system.debug('Account ID______' + ordobj.AccountId);
        TestinputMap.put('orderId',ordObj.id);
    	TestinputMap.put('creditStatus','Approved');
    	TestinputMap.put('areContractable',True);
        TestinputMap.put('isMoveOutOrder',false);
    	
    	Boolean isSuccess = (Boolean)TestoutMap.get('isSuccess');
    	String contractID = (String)TestoutMap.get('contractID');
     	System.debug('////comp'+contractID); 
        
        
        Contracts__c Conreq = new Contracts__c(Account__c = ordObj.accountID, Order__c=ordObj.id,Opportunity__c=ordObj.opportunityid);
        insert Conreq;
        System.debug('/////Conreq'+Conreq.Account__c);
        
         List<OrderItem> ordItemObj=new List<OrderItem>();
             for(orderItem oi: [select id,Contract_Request__c
                                FROM   OrderItem where Orderid=:orderId]){
                                   oi.Contract_Request__c = Conreq.id;
                                    ordItemObj.add(oi);
                                }
       
        update ordItemObj;
        
        Test.startTest();
        createData();
    	OCOM_CreateContractStep.generateOrUpdateContract(Conreq.id,TestinputMap);  
        Test.stopTest();
    } 
    
    static testMethod void UpdateContractStatusWithEnvelopeTest(){
    	TestDataHelper.testContractCreation1();
        list<Contract> con = [select id,status from Contract];
        con[0].status = 'In Progress';
        System.debug('////////contract'+con);
        Test.startTest();
        OCOM_CreateContractStep.UpdateContractStatusWithEnvelope(con);
        Test.stopTest();
    }  
    
    @isTest 
    static void testinvokeMethod(){
        String methodName             = 'getContractDetails';
        Map<String,Object> outMap     =  new Map<String,Object>();
        Map<String,Object> options    =  new Map<String,Object>();
        Map<String,Object> inputMap   =  new Map<String,Object>(); 
        Map<String,Object> typeConVal =  new Map<String,Object>();
        Map<String,Object> CrtContVal =  new Map<String,Object>();
        
         
        Map<String,Object> TestinputMap = new Map<String,Object>();
        Map<String,Object> TestoutMap = new Map<String,Object>();
        OCOM_OrderProductBeforeTrigger_Helper.isNotApplicable=true;
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        
        
        Order ordObj=[select OpportunityId,Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,
                             RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c,orderMgmtId__c 
                      FROM   Order where id=:orderId];
        ordObj.orderMgmtId__c='OS1234567890';
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        update ordObj;
        insertFieldMapper();
        
        
        system.debug('Account ID______' + ordobj.AccountId);
        TestinputMap.put('orderId',ordObj.id);
    	TestinputMap.put('creditStatus','Approved');
    	TestinputMap.put('areContractable',True);
        TestinputMap.put('isMoveOutOrder',false);
    	
    	Boolean isSuccess = (Boolean)TestoutMap.get('isSuccess');
    	String contractID = (String)TestoutMap.get('contractID');
     	System.debug('////comp'+contractID); 
        
        
        Contracts__c Conreq = new Contracts__c(Account__c = ordObj.accountID, Order__c=ordObj.id,Opportunity__c=ordObj.OpportunityId);
        insert Conreq;
        System.debug('/////Conreq'+Conreq.Account__c);
        
        
         List<OrderItem> ordItemObj=new List<OrderItem>();
             for(orderItem oi: [select Contract_Request__c
                                FROM   OrderItem where Orderid=:orderId]){
                                   oi.Contract_Request__c = Conreq.id;
                                    ordItemObj.add(oi);
                                }
       
        update ordItemObj;
        
        OCOM_CreateContractStep.generateOrUpdateContract(Conreq.id,TestinputMap);
            
        typeConVal.put('ContractRequestIdFromURL',Conreq.id);
        inputMap.put('TypeofContract',typeConVal);
        CrtContVal.put(ConReq.id,ConReq);
        inputMap.put('CreateTheContract',CrtContVal);
        inputMap.put('ContextId',ConReq.id);
        
        list<Contract> lstContract = new list<Contract>();
        Test.startTest();
        List<Contract> cList= [select id from Contract LIMIT 1];
        Contract c;
        if(!cList.isEmpty()){
            c=cList.get(0);
        }
        else {
            createData();
            cList= [select id from Contract LIMIT 1];
            if(!cList.isEmpty()){
                c=cList.get(0);
            }
        }
        System.debug('//////c'+c);
        lstContract.add(c);
        vlocity_cmt__ContractVersion__c testContractVersion = new vlocity_cmt__ContractVersion__c(Name='Test Version 1', vlocity_cmt__ContractId__c = c.id);
        insert testContractVersion;
        
        map<String,Object> outval=new map<string,Object>();
        outval.put('contractObj',c);
        outmap.put('createContractResponse',outval);
        System.debug('////outmap'+outmap);
        
        Contract testContractObj = new Contract(Complex_Fulfillment_Contract__c=conreq.id,Status='Draft',
                                                Contract_Type__c='Amendment',Envelop_Id__c ='TestEnvelop',
                                                AccountId=conreq.account__c,ContractTerm=12,StartDate=system.today(),
                                                Customer_Signor__c=createNewContact(conreq.account__c).id,TELUS_Signor__c=userinfo.getuserid(), 
                                                Agreement_Type__c = 'Amendment');
        insert testContractObj;
        
        
        
        OCOM_CreateContractStep ccs = new OCOM_CreateContractStep();
        ccs.invokeMethod('Else',inputMap,outMap,options);
        ccs.invokeMethod(methodName,inputMap,outMap,options);
        
        testContractObj.status = 'In Progress';
        update testContractObj;
        ccs.invokeMethod(methodName,inputMap,outMap,options);
        
        Test.stopTest();
    }   
    
    public static Contact createNewContact(Id AccountId){
        Contact testContactObj = new Contact(firstname = 'TestConFirstName', LastName = 'TestConLastname', accountId = AccountId , 
                                             Phone = '123456' , fax = '1234567', Email = 'test@gmail.com');
        insert testContactObj;
        return testContactObj;
    }
    @IsTest
    private static void isTLCflagTest1(){
         String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
       
        
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
        List<Contract> contractList=[select id from contract];
		List<Contracts__c> contrRList=[select SFDC_Contract__c,id,order__C from Contracts__c ];
        for(Contracts__c c:contrRList){
            c.order__C=orderId;
            c.SFDC_Contract__c=contractList.get(0).id;
        }
        update contrRList;
        System.debug('from testclass contrRList.size()='+contrRList.size());
        List<OrderItem> oiList=[select Term__c,id,ordermgmtid__C,vlocity_cmt__RootItemId__c,contract_request__c,Contract_Action__c,contract_line_item__C from orderitem where order.parentid__c=:orderId];
		String firstId=oiList.get(0).id;
       List<vlocity_cmt__ContractLineItem__c> clilist=[select id,orderlineitemid__C from vlocity_cmt__ContractLineitem__c];
        String firstCli=clilist.get(0).id;
        for(OrderItem oiInst:oiList){
            oiInst.contract_line_item__C=firstCli;
			oiInst.ordermgmtid__C='1234567';
            oiInst.Term__c='36';
			oiInst.vlocity_cmt__RootItemId__c=firstId;
			if(!contrRList.isEmpty()){
				oiInst.contract_request__c=contrRList.get(0).id;
			}
			//oiInst.Contract_Action__c='Change_Replace';
		}
        update oiList;
        
        integer i=0;
        for(vlocity_cmt__ContractLineitem__c cliIn:clilist){
            cliIn.orderlineitemid__C=oiList.get(i).id;
            i++;
        }
        update clilist;
        OCOM_CreateContractStep.isTLCFlag(orderId);
		Test.stopTest();
    }
     @IsTest
    private static void isTLCflagTest2(){
         String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
       
         List<Order> childOrd=[select id,status from order where parentid__c=:orderId ] ;
       
        Test.startTest();
        OrdrTestDataFactory.associateContractWithOrder(orderId);
         Boolean cancel=true;
        for(Order cOrd:childOrd){
            if(cancel){
               cOrd.status='Cancelled'; 
            }
            cancel=false;
        }
        update childOrd;
        List<Contract> contractList=[select id from contract];
		List<Contracts__c> contrRList=[select SFDC_Contract__c,id,order__C from Contracts__c ];
        for(Contracts__c c:contrRList){
            c.order__C=orderId;
            c.SFDC_Contract__c=contractList.get(0).id;
        }
        update contrRList;
        System.debug('from testclass contrRList.size()='+contrRList.size());
        List<OrderItem> oiList=[select Term__c,id,ordermgmtid__C,vlocity_cmt__RootItemId__c,contract_request__c,Contract_Action__c,contract_line_item__C from orderitem where order.parentid__c=:orderId];
		String firstId=oiList.get(0).id;
       List<vlocity_cmt__ContractLineItem__c> clilist=[select id,orderlineitemid__C from vlocity_cmt__ContractLineitem__c];
        String firstCli=clilist.get(0).id;
        for(OrderItem oiInst:oiList){
            oiInst.contract_line_item__C=firstCli;
			oiInst.ordermgmtid__C='1234567';
            oiInst.Term__c='36';
			oiInst.vlocity_cmt__RootItemId__c=firstId;
			if(!contrRList.isEmpty()){
				oiInst.contract_request__c=contrRList.get(0).id;
			}
			//oiInst.Contract_Action__c='Change_Replace';
		}
        update oiList;
        
        integer i=0;
        for(vlocity_cmt__ContractLineitem__c cliIn:clilist){
            cliIn.orderlineitemid__C=oiList.get(i).id;
            i++;
        }
        update clilist;
        OCOM_CreateContractStep.isTLCFlag(orderId);
		Test.stopTest();
    }
}