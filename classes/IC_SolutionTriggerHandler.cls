public class IC_SolutionTriggerHandler{
    public static boolean firstRun = true;
    Map<id, IC_Inventory__c> oldInventoryItemsMap = new Map<id, IC_Inventory__c>();
    Map<id, List<IC_Solution__c>> casesMap = new Map<id, List<IC_Solution__c>>();
    List<IC_SolutionTriggerHandler.ICSItem> sendNewNotificationList = new List<IC_SolutionTriggerHandler.ICSItem>();
    List<IC_SolutionTriggerHandler.ICSItem> sendCancelNotificationList = new List<IC_SolutionTriggerHandler.ICSItem>();
    List <IC_SolutionTriggerHandler.ICSItem> newICSItemsList = new List<IC_SolutionTriggerHandler.ICSItem>();
    Map <id,IC_SolutionTriggerHandler.ICSItem> oldICSItemsMap = new Map<Id,IC_SolutionTriggerHandler.ICSItem>();
   
    public String errorMessage {get;set;}
   
    private static RecordType caseTSRecordType= [select Id, Name from RecordType where SObjectType = 'Case' and Name = 'SMB Care Technical Support' Limit 1];
    
   /*
    * Handel the Insert Case for the Given Trigger.New Soluction Objects Map
    */
    public boolean handleInsert(Map<id,IC_Solution__c> newSolutionsMap){
        initNew(newSolutionsMap);
        List<IC_Inventory__c>  inventoryItemsToUpdate = new List<IC_Inventory__c>();
        for(IC_SolutionTriggerHandler.ICSItem icsItem : this.newICSItemsList) 
        {
            if(icsItem.recordType == 'Loaner Device' || icsItem.recordType == 'Internet Fax' ){
                IC_Inventory__c inventoryItem = new IC_Inventory__c(Id=icsItem.InventoryId);
                if(icsItem.InventoryLoanedOut==false){
                    inventoryItem.Loaned_Out__c=true;
                    inventoryItemsToUpdate.add(inventoryItem);
                    if(icsItem.InventoryRecordType=='Device'){
                        sendNewNotificationList.add(icsItem);
                    }
                }else{
                    this.errorMessage=Label.IC_Inventory_Selected_By_Another_User.replace('{InvRecType}',icsItem.inventoryRecordType);
                    return false;
                }
            }
        }
        try{
            update inventoryItemsToUpdate;
            for(IC_SolutionTriggerHandler.ICSItem icsItemNotify : sendNewNotificationList){
                IC_NotificationHelper.sendNewDeviceNotificationEmail(icsItemNotify);
            }
        }catch(DmlException e){
            errorMessage = Label.error_in_saving_Data  + ' ' + e.getMessage();
            destroy();
            return false;
        }
        destroy();
        return true;
    }
    
    /*
     * Handel the Insert Case for the Given Trigger.New Soluction Objects Map
     */
     public boolean handleUpdate(Map<Id,IC_Solution__c> newSolutionMap,Map <Id,IC_Solution__c>  oldSolutionMap){ 
        initNew(newSolutionMap);
        initOld(oldSolutionMap);
        //To update List    
        List<IC_Inventory__c>  inventoryItemsToUpdate = new List<IC_Inventory__c>();
        List<Case>  caseItemsToUpsert = new List<Case>();
        Map<Id,Case>  caseItemsToUpsertMap = new Map<Id,Case>();
        for(IC_SolutionTriggerHandler.ICSItem icsItem : this.newICSItemsList){
            if(icsItem.recordType=='Loaner Device' || icsItem.recordType == 'Internet Fax'){
                if(!handelInventoryUpdates(icsItem,inventoryItemsToUpdate)){
                    return false;
                }
                handelSoulutionStatusUpdatesForLoanerDevice(icsItem);
            }
            if(icsItem.recordType=='Tethering' && icsItem.status=='Closed' && 
               (icsItem.BusinessBAN =='Wireless Business' || icsItem.BusinessBAN =='Wireless Consumer')){
                createWirelessBillingHandOffCase(icsItem,caseItemsToUpsert);
            }
            handelCaseStatusUpdates(icsItem,caseItemsToUpsertMap);
            
        }
        try{
            update inventoryItemsToUpdate;
            for(IC_SolutionTriggerHandler.ICSItem icsItem : sendCancelNotificationList){
                IC_NotificationHelper.sendCancelDeviceNotificationEmail(icsItem);
            }
            for(IC_SolutionTriggerHandler.ICSItem icsItem : sendNewNotificationList){
                IC_NotificationHelper.sendNewDeviceNotificationEmail(icsItem);
            }
        }catch(DmlException e){
            errorMessage = Label.error_in_saving_Data  + ' ' + e.getMessage();
            System.debug(errorMessage);
            destroy();
            return false;
        }
        try{
            upsert caseItemsToUpsert;
            upsert  caseItemsToUpsertMap.values();
        }catch(DmlException e){
            errorMessage = Label.error_in_saving_Data  + ' ' + e.getMessage();
            destroy();
            return false;
        }
        destroy();
        return true;
        
    }
    
    /*
     * Handel Case status related updates and update the Case object
     *  
     */
    private void handelCaseStatusUpdates(IC_SolutionTriggerHandler.ICSItem icsItem, Map<Id,Case>  caseItemsToUpsertMap){
        if(icsItem.Status=='Provisioning Complete' && icsItem.CaseStatus=='Open'){
            Case casetoUpdate = new Case(id=icsItem.CaseId);
            casetoUpdate.Status = 'Solutions Provided';
            caseItemsToUpsertMap.put(icsItem.CaseId,casetoUpdate);
        }
        if((icsItem.Status=='To Be Closed' || icsItem.Status=='Closed') && icsItem.CaseStatus != 'Closing Solutions'){
            Case casetoUpdate = new Case(id=icsItem.CaseId);
            casetoUpdate.Status = 'Closing Solutions';
            icsItem.CaseStatus = 'Closing Solutions';
            //caseItemsToUpsert.add(casetoUpdate);
            caseItemsToUpsertMap.put(icsItem.CaseId,casetoUpdate);
        }
        if((icsItem.Status=='Closed' || icsItem.Status=='Cancelled') && icsItem.CaseStatus == 'Closing Solutions'){
           boolean hasClosedStatus = false;
           boolean caseCanBeClosed = false;
            List<IC_Solution__c> solutionList = casesMap.get(icsItem.CaseId);
             /* Close the case when all ICSs are Closed or Cancelled, and there is at least one ICS that is “Closed”. Examples:
                    Two ICSs, one closed one cancelled, then close the case
                    Two ICSs, both cancelled, do not close case
                    Three ICSs, one closed one cancelled one open, do not close case
              */
              if(solutionList!=null){
                 for(IC_Solution__c solu : solutionList){
                     if(solu.status__c == 'Closed'){
                         hasClosedStatus = true;
                         caseCanBeClosed = true;
                     }else if(solu.status__c == 'Cancelled'){
                         caseCanBeClosed = true;
                     }else{
                         caseCanBeClosed = false; 
                         break; 
                     }
                  }
              }    
              if(hasClosedStatus && caseCanBeClosed ){
                  Case casetoUpdate = new Case(id=icsItem.CaseId);
                  casetoUpdate.status = 'Closed';
                  casetoUpdate.Case_Resolution__c = 'ICS Provided';
                  caseItemsToUpsertMap.put(icsItem.CaseId,casetoUpdate);
              }
         }
        if((icsItem.Status != 'Closed' || icsItem.Status !='Cancelled') && icsItem.CaseStatus=='Closed'){
            Case casetoUpdate = new Case(id=icsItem.CaseId);
            if(icsItem.Status=='New' || icsItem.Status=='Device Not Fulfilled' ){
                casetoUpdate.status = 'Open';    
            }else if(icsItem.Status =='Provisioning Complete'){
                casetoUpdate.status = 'Solutions Provided'; 
            }else if(icsItem.Status =='To Be Closed'){
                casetoUpdate.status = 'Closing Solutions';  
            }
            
            caseItemsToUpsertMap.put(icsItem.CaseId,casetoUpdate);
        }
    }
    
   /*
    *  Handel the Inevtory Object to Loan out or not when IC_Solution change the inventory item 
    *  or IC_Solution status change 
    */        
    private boolean handelInventoryUpdates(IC_SolutionTriggerHandler.ICSItem newIcsItem, 
                                                             List<IC_Inventory__c>  inventoryItemsToUpdate){
        if(newIcsItem.status=='Cancelled' || newIcsItem.status=='Closed'){
            ICSItem oldIcsItem = oldICSItemsMap.get(newIcsItem.Id);
              if(!(oldIcsItem.status.equals('Cancelled')||oldIcsItem.status.equals('Closed'))){ 
                    IC_Inventory__c inventoryItem = new IC_Inventory__c(Id=newIcsItem.InventoryId);
                    inventoryItem.Loaned_Out__c=false;
                    inventoryItemsToUpdate.add(inventoryItem);
                    if(newIcsItem.Status=='Cancelled' && newIcsItem.InventoryRecordType=='Device'){ 
                        sendCancelNotificationList.add(newIcsItem);
                    }   
              }  
        }else{ 
            ICSItem oldIcsItem = oldICSItemsMap.get(newIcsItem.Id);
            IC_Inventory__c newInventoryItem = new IC_Inventory__c(id=newIcsItem.InventoryId); 
            IC_Inventory__c oldInventoryItem = new IC_Inventory__c(id=oldIcsItem.InventoryId); 
            if(newIcsItem.InventoryId != oldIcsItem.InventoryId){
                if(newIcsItem.InventoryLoanedOut == false){
                    if(!(oldIcsItem.status=='Closed' || oldIcsItem.status=='Cancelled')){
                        oldInventoryItem.Loaned_Out__c =false;   
                        inventoryItemsToUpdate.add(oldInventoryItem);
                    }
                    newInventoryItem.Loaned_Out__c =true;
                    
                    inventoryItemsToUpdate.add(newInventoryItem);
                    if(newIcsItem.InventoryRecordType=='Device'){
                        oldIcsItem.CustomerName = newIcsItem.CustomerName;
                        oldIcsItem.CaseContactId = newIcsItem.CaseContactId;// Assign the Customer name from New Object to Old
                        oldIcsItem.CaseContactEmail = newIcsItem.CaseContactEmail;
                        oldIcsItem.CaseNumber = newIcsItem.CaseNumber;
                        oldIcsItem.OwnerEmail = newIcsItem.OwnerEmail;
                        sendCancelNotificationList.add(oldIcsItem);
                        sendNewNotificationList.add(newIcsItem);
                    }
                }else{
                    this.errorMessage=Label.IC_Inventory_Selected_By_Another_User.replace('{InvRecType}',newIcsItem.InventoryRecordType);
                    return false;
                }
            }else{
                if(oldIcsItem.status=='Cancelled' || oldIcsItem.status=='Closed'){
                    if(newIcsItem.InventoryLoanedOut == false){
                        newInventoryItem.Loaned_Out__c =true;
                        inventoryItemsToUpdate.add(newInventoryItem);
                    }else{
                        this.errorMessage=Label.IC_Inventory_Selected_By_Another_User.replace('{InvRecType}',newIcsItem.InventoryRecordType);
                        return false;
                    }
                }
            }
        }
        return true;
    }
   
   /*
    *  Check the IC_Solution status and Send Required Notifcations 
    */
    private void handelSoulutionStatusUpdatesForLoanerDevice(IC_SolutionTriggerHandler.ICSItem icsItem){
        if(icsItem.Status=='To Be Closed'  && icsItem.InventoryRecordType=='Device' && icsItem.ProviderDeliveryMethod=='Customer Pick-up' ){
            IC_NotificationHelper.sendReturnDeviceNotificationEmail(icsItem); 
        }
        /* TODO: Remove this code after customer email testing has been completed
        if(icsItem.Status=='To Be Closed' && icsItem.InventoryRecordType=='Device'){
            if(icsItem.CaseContactEmail!=null && icsItem.CaseContactEmail !='' ){
                IC_NotificationHelper.sendCustomerNotificationEmail(icsItem); 
            }
        }*/
        
    }
    
   /*
    * Init the newICSItemsList for the Trigger.New Records with ICSItem Objects and the Case Id Map
    */
     private void initNew(Map<id,IC_Solution__c > newItemsMap)
     {
        List<IC_Solution__c> newSolutionList =getNewICSItemsList(newItemsMap);
        for(IC_Solution__c newICSRecord: newSolutionList){
             casesMap.put(newICSRecord.Case__r.Id, null);
             newICSItemsList.add(new ICSItem(newICSRecord));
        }
        pupulateCasesSolutionListMap();
    }
    
   /*
    * Init the oldICSItemsMap for the Trigger.Old Records with ICSItem Objects 
    */
     private void initOld(Map<id,IC_Solution__c > oldItemsMap)
     {
        for(Id oldICSRecordId: oldItemsMap.keySet()){
            IC_Solution__c oldSolutionItem   = oldItemsMap.get(oldICSRecordId);
            oldInventoryItemsMap.put(oldSolutionItem.Inventory_Item__c,null);
        }
        pupulateOldInventoryItemsMap();
        for(Id oldICSRecordId: oldItemsMap.keySet()){
             IC_Solution__c oldSolutionItem  = oldItemsMap.get(oldICSRecordId);
             IC_Inventory__c oldInventory =  oldInventoryItemsMap.get(oldSolutionItem.Inventory_Item__c);
             oldICSItemsMap.put(oldICSRecordId ,new ICSItem(oldSolutionItem,oldInventory));
        }
        
    }
    
    private void pupulateOldInventoryItemsMap(){
        for(IC_Inventory__c inventoryItem : [select Id, Name , Loaned_Out__c, RecordType.Name, Device_Provider__r.Name, Device_Provider__r.ICS_Notification_Email__c, Device_Provider__r.ICS_Delivery_Method__c 
            from IC_Inventory__c where Id in : oldInventoryItemsMap.keySet()]) {
          oldInventoryItemsMap.put(inventoryItem.Id, inventoryItem);
        }
    }
   
    public void destroy(){
        newICSItemsList= null;
        sendNewNotificationList =null;
        casesMap = null;
        sendNewNotificationList = null;
        sendCancelNotificationList = null;
        oldICSItemsMap =null;
        oldInventoryItemsMap=null;
    }
   
   
   /*
    * Populate the Cases Map with Solution list under each Case Objet
    */
    private void pupulateCasesSolutionListMap(){
        for(Id caseId : casesMap.keySet()) {
            List<IC_Solution__c> solutionsUnderCase = [Select id, status__c, Case__r.id from IC_Solution__c where Case__r.id =:caseId];
            if(solutionsUnderCase!=null){
                casesMap.put(caseId, solutionsUnderCase);
            }
        }
    }
    
    
   /*
    * Return the List of IC_Solution__c for the Trigger New
    */
    private List<IC_Solution__c> getNewICSItemsList(Map<id,IC_Solution__c> newSolutionMap){
        List<IC_Solution__c> solutionList = 
            [Select Id, Name, status__c, RecordType.Name, Inventory_Item__c,Owner_Email__c,
                 Inventory_Item__r.Id, Inventory_Item__r.Name, Inventory_Item__r.Loaned_Out__c, Inventory_Item__r.RecordType.Name, Inventory_Item__r.Device_Type__c,
                 Inventory_Item__r.Device_Provider__r.Name, Inventory_Item__r.Device_Provider__r.ICS_Notification_Email__c, Inventory_Item__r.Device_Provider__r.ICS_Delivery_Method__c, Inventory_Item__r.Device_Provider__r.Address__c,
                 Business_BAN__c, Consumer_BAN__c, Provisioning_Date__c, Provisioning_End_Date__c,
                 Case__r.Id, Case__r.AccountId, Case__r.Account.Name, Case__r.Contact.Email, Case__r.CaseNumber, Case__r.ContactId, Case__r.Origin, Case__r.Status,
                 Case__r.Contact.Name,Case__r.Contact.Phone, Case__r.Onsite_Contact_Name__r.Name,Case__r.Onsite_Contact_Name__r.Phone, Case__r.Order_or_Ticket_Number__c
                 from IC_Solution__c where id =:newSolutionMap.keySet()];
        return solutionList;
    }
    
   /*
    * Create a new New Billing Hand Off Case Object with the Record type 'SMB Care Technical Support'
    */
    private void createWirelessBillingHandOffCase(ICSItem item, List<Case> listToInsert){
        Case caseobj = new Case();
        caseobj.RecordTypeId = caseTSRecordType.id;
        caseobj.ParentId =item.CaseId;
        caseobj.AccountId = item.AccountId;
        caseobj.ContactId = item.CaseContactId;
        caseobj.Type = 'Service Trouble';
        caseobj.Subject = 'ICS Tethering Billing Credit';
        caseobj.Origin = item.CaseOrigin;
        caseobj.Priority = 'Medium';
        caseobj.Status='New';
        caseobj.Description='Customer Account Type: '+item.BusinessBAN+
                            '\nSubscriber Number: '+ item.ConsumerBAN+ 
                            '\nTethering Data Credit Period: ' + item.ProvisioningDate.format() +' to ' + item.ProvisioningEndDate.format();
        listToInsert.add(caseobj);
       
    }
   /*
    * Wrapper Class to hold the Solution Invetory and Case Fields 
    */
    public class ICSItem {
        //Solution Related Fields
        Public Id Id {get;set;}
        Public String Name {get;set;}
        public String RecordType {get; set;}
        public String Status {get; set;}
        public String BusinessBAN {get; set;}
        public String ConsumerBAN {get; set;}
        public Date ProvisioningDate {get; Set;}
        public Date ProvisioningEndDate {get; Set;}
        public String OwnerEmail {get; set;}
        //Inventory Item related Fields
        public Id InventoryId  {get; set;}
        public String InventoryName  {get; set;}
        public Boolean InventoryLoanedOut  {get; set;}
        public String InventoryRecordType  {get; set;}
        public String InventoryType  {get; set;}
        //Provider related Fields
        public String ProviderName  {get; set;}
        public String[] ProviderContactEmails  {get; set;}
        public String ProviderDeliveryMethod {get; set;}
        public String ProviderAddress {get; set;}
        
        //Case Related Fields
        public String CaseId  {get; set;}
        public String CaseNumber  {get; set;}
        public Id CaseContactId {get; set;}
        public String CaseContactEmail  {get; set;}
        public String CaseOrigin {get; set;}
        public String CaseStatus {get; set;}
        public String CaseOrderOrTicketNumber {get; set;}
        public String CaseContactName {get; set;}
        public String CaseContactPhone {get; set;}
        public String CaseOnsiteContactName {get; set;}
        public String CaseOnsiteContactPhone {get; set;}
        //Customer name from Account Name
        public String CustomerName {get; set;}
        public String AccountId {get; set;}
            
       /*
        *ICSItem Class Constructor
        */
        public ICSItem(IC_Solution__c solutionItem){
            this.Id=solutionItem.id;
            this.Name=solutionItem.Name;
            this.Status = solutionItem.status__c;
            this.RecordType = solutionItem.RecordType.Name;
            this.BusinessBAN = solutionItem.Business_BAN__c;
            this.ConsumerBAN = solutionItem.Consumer_BAN__c;
            this.ProvisioningDate = solutionItem.Provisioning_Date__c;            
            this.ProvisioningEndDate = solutionItem.Provisioning_End_Date__c;
            this.OwnerEmail = solutionItem.Owner_Email__c;
            this.CaseId = solutionItem.Case__r.Id;
            this.CaseNumber = solutionItem.Case__r.CaseNumber;
            this.CaseContactId=solutionItem.Case__r.ContactId;
            this.CaseContactEmail =solutionItem.Case__r.Contact.Email;
            this.CaseOrigin=solutionItem.Case__r.Origin;
            this.CaseStatus=solutionItem.Case__r.Status;
            
            this.CaseOrderOrTicketNumber=solutionItem.Case__r.Order_or_Ticket_Number__c;
            this.CaseContactName=solutionItem.Case__r.Contact.Name;
            this.CaseContactPhone=solutionItem.Case__r.Contact.Phone;
            this.CaseOnsiteContactName=solutionItem.Case__r.Onsite_Contact_Name__r.Name;
            this.CaseOnsiteContactPhone=solutionItem.Case__r.Onsite_Contact_Name__r.Phone;
            
            this.CustomerName = solutionItem.Case__r.Account.Name;
            this.AccountId = solutionItem.Case__r.Account.Id;
            
            if(solutionItem.Inventory_Item__c != null){
                //Inventory Item related Fields
                this.InventoryId= solutionItem.Inventory_Item__r.Id;
                this.InventoryName= solutionItem.Inventory_Item__r.Name;
                this.InventoryLoanedOut= solutionItem.Inventory_Item__r.Loaned_Out__c;
                this.InventoryRecordType= solutionItem.Inventory_Item__r.RecordType.Name;
                this.InventoryType = solutionItem.Inventory_Item__r.Device_Type__c;
                //Provider related Fields
                if(this.InventoryRecordType =='Device'){
                    this.ProviderName= solutionItem.Inventory_Item__r.Device_Provider__r.Name;
                    this.ProviderContactEmails= solutionItem.Inventory_Item__r.Device_Provider__r.ICS_Notification_Email__c.split(',');
                    this.ProviderDeliveryMethod = solutionItem.Inventory_Item__r.Device_Provider__r.ICS_Delivery_Method__c;
                    this.ProviderAddress = solutionItem.Inventory_Item__r.Device_Provider__r.Address__c;
                }
            }
        }
        
        /*
        *ICSItem Class Constructor to Create with the Inevtory Object 
        */
        public ICSItem(IC_Solution__c solutionItem, IC_Inventory__c oldInventory ){
            this.Id=solutionItem.id;
            this.Name=solutionItem.Name;
            this.Status = solutionItem.status__c;
            this.RecordType = solutionItem.RecordType.Name;
            
            if(oldInventory != null){
                //Inventory Item related Fields
                this.InventoryId= oldInventory.Id;
                this.InventoryName= oldInventory.Name;
                this.InventoryLoanedOut= oldInventory.Loaned_Out__c;
                this.InventoryRecordType= oldInventory.RecordType.Name;
                //Provider related Fields
                if(this.InventoryRecordType =='Device'){
                    this.ProviderName= oldInventory.Device_Provider__r.Name;
                    this.ProviderContactEmails= oldInventory.Device_Provider__r.ICS_Notification_Email__c.split(',');
                    this.ProviderDeliveryMethod = oldInventory.Device_Provider__r.ICS_Delivery_Method__c;
                }
            }
        }
        
        /*
        *ICSItem Class Defualt 
        */
        public ICSItem(){
           
        }
        
    }
            
      
}