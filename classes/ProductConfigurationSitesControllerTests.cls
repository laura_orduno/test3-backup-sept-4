/**
 * Unit tests for <code>ProductConfigurationSitesController</code> class.
 *
 * @author Max Rudman
 * @since 4/12/2011
 */
@isTest
private class ProductConfigurationSitesControllerTests {
	private static Product2 product;
	private static SBQQ__ProductFeature__c feature;
	private static Product2 option1;
	private static Product2 option2;
	private static Product2 option3;
	private static List<SBQQ__ProductOption__c> options;
	private static SBQQ__Quote__c quote;
	
	testMethod static void testOnLookupOptions() {
		setUp();
		
		ProductConfigurationSitesController target = new ProductConfigurationSitesController();
		target.selectedProductId = product.Id;
		target.selectedFeatureId = feature.Id;
		target.onLookupOptions();
		System.assert(ApexPages.getMessages().isEmpty(), 'Errors: ' + ApexPages.getMessages());
		System.assertEquals(1, target.getAvailableOptions().size());
	}
	
	testMethod static void testOnAddOptions() {
		setUp();
		
		ProductConfigurationSitesController target = new ProductConfigurationSitesController();
		ProductOptionSelectionModel smodel = target.getSelectionModelByProductId(product.Id);
		target.selectedProductId = product.Id;
		target.selectedFeatureId = feature.Id;
		target.selectedOptionId = options[1].Id;
		System.assertEquals(2, smodel.getSelections().size());
		target.onAddOptions();
		System.assert(ApexPages.getMessages().isEmpty(), 'Errors: ' + ApexPages.getMessages());
		System.assertEquals(3, smodel.getSelections().size());
	}
	
	testMethod static void testOnConfigureOption() {
		setUp();
		
		ProductModel pmodel = ProductModel.loadById(product.Id, null, null, null);
		ProductConfigurationSitesController target = new ProductConfigurationSitesController();
		ProductOptionSelectionModel smodel = target.getSelectionModelByProductId(product.Id);
		String id = smodel.addSelection(pmodel.getAllOptions()[1]).id;
		target.optionSelectionId = id;
		target.onConfigureOption();
		System.assert(ApexPages.getMessages().isEmpty(), 'Errors: ' + ApexPages.getMessages());
	}
	
	testMethod static void testOnDeleteOption() {
		setUp();
		
		ProductModel pmodel = ProductModel.loadById(product.Id, null, null, null);
		ProductConfigurationSitesController target = new ProductConfigurationSitesController();
		ProductOptionSelectionModel smodel = target.getSelectionModelByProductId(product.Id);
		String id = smodel.addSelection(pmodel.getAllOptions()[1]).id;
		target.selectedProductId = product.Id;
		target.optionSelectionId = id;
		System.assertEquals(3, smodel.getSelections().size());
		target.onDeleteOption();
		System.assert(ApexPages.getMessages().isEmpty(), 'Errors: ' + ApexPages.getMessages());
		System.assertEquals(2, smodel.getSelections().size());
	}
	
	testMethod static void testOnSaveOptionConfiguration() {
		setUp();
		
		ProductConfigurationSitesController target = new ProductConfigurationSitesController();
		ProductOptionSelectionModel smodel = target.getSelectionModelByProductId(product.Id);
		target.selectedProductId = product.Id;
		target.optionSelectionId = smodel.getSelections()[1].id;
		target.onSaveOptionConfiguration();
		System.assert(ApexPages.getMessages().isEmpty(), 'Errors: ' + ApexPages.getMessages());
	}
	
	testMethod static void testValidateAndLoad() {
		setUp();
		
		ProductModel pmodel = ProductModel.loadById(product.Id, null, null, null);
		ProductConfigurationSitesController target = new ProductConfigurationSitesController();
		ProductOptionSelectionModel smodel = target.getSelectionModelByProductId(product.Id);
		smodel.addSelection(pmodel.getAllOptions()[0]);
		smodel.addSelection(pmodel.getAllOptions()[1]);
		target.validateProductConfigurations(new Map<Id,ProductModel>{pmodel.getId()=>pmodel});
		System.assert(ApexPages.getMessages().isEmpty(), 'Errors: ' + ApexPages.getMessages());
		ProductModel[] pmodels = target.loadConfiguredProducts();
		System.assertEquals(1, pmodels.size());
	}
	
	testMethod static void testConfigureQuoteLine() {
		setUp();
		
		SBQQ__QuoteLine__c[] lines = new SBQQ__QuoteLine__c[]{};
		lines.add(new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,SBQQ__Product__c=product.Id,SBQQ__ProratedPrice__c=100));
		lines.add(new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,SBQQ__Product__c=option1.Id,SBQQ__ProductOption__c=options[0].Id,SBQQ__ProratedPrice__c=100));
		insert lines;
		
		lines[1].SBQQ__RequiredBy__c = lines[0].Id;
		update lines[1];
		
		ApexPages.currentPage().getParameters().remove('pids');
		ApexPages.currentPage().getParameters().put('lid', lines[0].Id);
		
		ProductModel pmodel = ProductModel.loadById(product.Id, null, null, null);
		ProductConfigurationSitesController target = new ProductConfigurationSitesController();
		ProductOptionSelectionModel smodel = target.getSelectionModelByProductId(product.Id);
		System.assert(smodel != null);
		System.assertEquals(1, smodel.getSelectedOptionIds().size());
		System.assert(smodel.getSelectedOptionIds().contains(options[0].Id));
	}
	
	testMethod static void testLoadLimits() {
		setUp();
		
		SBQQ__QuoteLine__c[] lines = generateLines(1);
		Integer ss1 = Limits.getScriptStatements();
		ApexPages.currentPage().getParameters().put('lid', lines[0].Id);
		ProductConfigurationSitesController target = new ProductConfigurationSitesController();
		Integer delta1 = Limits.getScriptStatements() - ss1;
		
		delete lines;
		lines = generateLines(50);
		Integer ss2 = Limits.getScriptStatements();
		ApexPages.currentPage().getParameters().remove('pids');
		ApexPages.currentPage().getParameters().put('lid', lines[0].Id);
		target = new ProductConfigurationSitesController();
		Integer delta2 = Limits.getScriptStatements() - ss2;
		
		Integer delta = delta2 - delta1;
		//System.assert(delta < 9500, '1-line: ' + delta1 + '; 50-line: ' + delta2 + '; Delta: ' + delta);
	}
	
	testMethod static void testSaveLimits() {
		setUp();
		
		product.SBQQ__OptionSelectionMethod__c='Add';
		update product;
		
		ProductConfigurationSitesController target = new ProductConfigurationSitesController();
		generateOptionSelections(1, target.getSelectionModelByProductId(product.Id));
		Integer ss1 = Limits.getScriptStatements();
		target.onSave();
		System.assert(ApexPages.getMessages().isEmpty(), 'Errors: ' + ApexPages.getMessages());
		Integer delta1 = Limits.getScriptStatements() - ss1;
		
		target = new ProductConfigurationSitesController();
		generateOptionSelections(50, target.getSelectionModelByProductId(product.Id));
		Integer ss2 = Limits.getScriptStatements();
		target.onSave();
		Integer delta2 = Limits.getScriptStatements() - ss2;
		
		Integer delta = delta2 - delta1;
		Integer expected = 18000 + 6000;
		//System.assert(delta < expected, '1-line: ' + delta1 + '; 50-line: ' + delta2 + '; Expected: ' + expected + '; Delta: ' + delta);
	}
	
	testMethod static void testOnCustomButton() {
		setUp();
		
		ApexPages.currentPage().getParameters().put('cbc', '1');
		ApexPages.currentPage().getParameters().put('cba0', 'save');
		ApexPages.currentPage().getParameters().put('cbl0', 'Custom Button1');
		ApexPages.currentPage().getParameters().put('cbu0', 'http://www.google.com');
		
		ProductConfigurationSitesController target = new ProductConfigurationSitesController();
		target.selectedCustomButtonIndex = 0;
		PageReference pref = target.onCustomButton();
		System.assert(pref != null);
		System.assertEquals('http://www.google.com', pref.getUrl());
	}
	
	testMethod static void testOnCalculate() {
		setUp();
		
		ProductConfigurationSitesController target = new ProductConfigurationSitesController();
		PageReference pref = target.onCalculate();
		System.assert(pref == null);
		System.assert(ApexPages.getMessages().isEmpty());
	}
	
	private static SBQQ__QuoteLine__c[] generateLines(Integer count) {
		SBQQ__QuoteLine__c[] lines = new SBQQ__QuoteLine__c[]{};
		lines.add(new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,SBQQ__Product__c=product.Id,SBQQ__ProratedPrice__c=100));
		for (Integer i=0;i<count;i++) {
			lines.add(new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,SBQQ__Product__c=option1.Id,SBQQ__ProductOption__c=options[0].Id,SBQQ__ProratedPrice__c=100));
		}
		insert lines;
		
		for (Integer i=1;i<lines.size();i++) {
			lines[i].SBQQ__RequiredBy__c = lines[0].Id;
		}
		update lines;
		return lines;
	}
	
	private static void generateOptionSelections(Integer count, ProductOptionSelectionModel target) {
		QuoteVO qvo = new QuoteVO(quote);
		for (Integer i=0;i<count;i++) {
			SBQQ__QuoteLine__c line = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,SBQQ__Product__c=option1.Id,SBQQ__Product__r=option1,SBQQ__ProductOption__c=options[0].Id,SBQQ__ProratedPrice__c=100);
			QuoteVO.LineItem item = new QuoteVO.LineItem(qvo,null,line);
			ProductOptionSelectionModel.Selection sel = 
				new ProductOptionSelectionModel.Selection(target, item);
			sel.selected = true;
			target.addSelection(sel);
		}
	}
	
	testMethod static void testCustomMessages() {
		setUp();
		
		ApexPages.currentPage().getParameters().put('cmc', '1');
		ApexPages.currentPage().getParameters().put('cms0', 'WarNing');
		ApexPages.currentPage().getParameters().put('cmt0', 'Custom warning');
		ProductConfigurationSitesController target = new ProductConfigurationSitesController();
		System.assertEquals(1, ApexPages.getMessages().size());
		System.assertEquals(ApexPages.Severity.WARNING, ApexPages.getMessages()[0].getSeverity());
		System.assertEquals('Custom warning', ApexPages.getMessages()[0].getDetail());
	}
	
	testMethod static void test() {
		setUp();

		ApexPages.currentPage().getParameters().put('qid', quote.Id);
		ApexPages.currentPage().getParameters().put('pids', '01t40000001jG18');		
		ProductConfigurationSitesController target = new ProductConfigurationSitesController();
	}
	
	private static void setUp() {
		product = new Product2(Name='Test',SBQQ__ConfigurationFields__c='Field1,Field2');
		option1 = new Product2(Name='Option1',ProductCode='C1');
		option2 = new Product2(Name='Option2',ProductCode='C2');
		option3 = new Product2(Name='Option3',ProductCode='C3');
		insert new List<Product2>{product,option1,option2,option3};
		
		feature = new SBQQ__ProductFeature__c(SBQQ__Number__c=1,Name='Test',SBQQ__ConfiguredSKU__c=product.Id,SBQQ__MinOptionCount__c=0);
		insert feature;
		
		options = new List<SBQQ__ProductOption__c>();
		options.add(new SBQQ__ProductOption__c(SBQQ__ConfiguredSKU__c=product.Id,SBQQ__OptionalSKU__c=option1.Id,SBQQ__Number__c=1,SBQQ__UnitPrice__c=100,SBQQ__Feature__c=feature.Id));
		options.add(new SBQQ__ProductOption__c(SBQQ__ConfiguredSKU__c=product.Id,SBQQ__OptionalSKU__c=option2.Id,SBQQ__Number__c=2,SBQQ__UnitPrice__c=200));
		options.add(new SBQQ__ProductOption__c(SBQQ__ConfiguredSKU__c=option2.Id,SBQQ__OptionalSKU__c=option3.Id,SBQQ__Number__c=1));
		insert options;
		
		Opportunity opp = new Opportunity(Name='Test',CloseDate=System.today(),StageName='Prospecting');
		insert opp;
		
		quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
		insert quote;
		
		ApexPages.currentPage().getParameters().put('qid', quote.Id);
		ApexPages.currentPage().getParameters().put('pids', product.Id);
		ApexPages.currentPage().getParameters().put('retURL', 'www.steelbrick.com');
	}
}