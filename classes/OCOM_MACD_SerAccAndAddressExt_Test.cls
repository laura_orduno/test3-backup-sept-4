@isTest(SeeAllData=false)
public class OCOM_MACD_SerAccAndAddressExt_Test {
    testMethod static void CreateServiceAccounttest() {
        Schema.DescribeSObjectResult d = Schema.SObjectType.Account; 
          Map<String,Schema.RecordTypeInfo> rtMapByName = d.getRecordTypeInfosByName();
          
          Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
          Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();        
          
          Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
          Account seracc = new Account(name='123 street',recordtypeid=recSerTypeId,parentid=RCIDacc.id);          
          
          List<Account> acclist= new List<Account>();          
          acclist.add(RCIDacc);
          acclist.add(seracc );
          insert acclist;
          
        smbcare_address__c address= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987650',account__c=RCIDacc.id); 
        smbcare_address__c address3= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987652',account__c=RCIDacc.id,service_account_Id__c=seracc.id); 
        smbcare_address__c address2= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987651',account__c=RCIDacc.id); 
        smbcare_address__c address5= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987654',account__c=RCIDacc.id); 
        smbcare_address__c address4= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987652',account__c=RCIDacc.id,service_account_Id__c=seracc.id); 
        smbcare_address__c address6= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987654',account__c=RCIDacc.id); 
        List<smbcare_address__c> smbCarelist= new List<smbcare_address__c>(); 
        smbCarelist.add(address4);
        smbCarelist.add(address6);
        insert smbCarelist;
        Test.StartTest();
        ServiceAddressData SData = new ServiceAddressData();
        SData.isManualCapture=true;
        SData.isSACG=false;
        SData.IsILEC = false;
        SData.fmsId='987651';
       // PageReference pageref = Page.OCOM_PAC_MACD;
       // Test.setCurrentPage(pageref);        
       // pageref.getparameters().put('id', seracc.Id);
        OCOM_PAC_MACD_Controller con= new OCOM_PAC_MACD_Controller();
        OCOM_MACD_ServiceAccountAndAddressExt ext = new OCOM_MACD_ServiceAccountAndAddressExt(con);      
        ext.CreateServiceAccount(RCIDacc.Id,address,SData);
        SData.fmsId='987652';
        SData.isManualCapture=false;
        ext.CreateServiceAccount(RCIDacc.id,address3,SData);
        SData.fmsId='987654';
        SData.isManualCapture=false;
        ext.CreateServiceAccount(RCIDacc.Id,address5,SData);
        SData.fmsId='987650';
        SData.isManualCapture=false;
        ext.CreateServiceAccount(RCIDacc.Id,address2,SData);
        Test.StopTest();        
    }


}