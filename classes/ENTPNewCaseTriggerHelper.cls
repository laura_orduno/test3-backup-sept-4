public class ENTPNewCaseTriggerHelper {
	@future(callout=true)
	public static void SendLynxCreate(String caseId) {
		Case s = [Select Subject, casenumber, Description, My_Business_Requests_Type__c, Project_number__c, Contact_Phone_Number2__c, Customer_First_Name_contact__c, Customer_Last_Name_contact__c, Customer_Phone_contact__c, ENTP_AccessDay__c, ENTP_Condition__c, Id FROM Case WHERE id = :caseId LIMIT 1];
		String extract = s.ENTP_Condition__c;
		String extractAcess = s.ENTP_AccessDay__c;
		String extractPhone = s.Customer_Phone_contact__c;
		String extractName = s.Customer_First_Name_contact__c ;

		//Rob - 01/19/2016 : Make the call to VITILcare_LynxCreate to send info to Lynx Web Service
		LynxCreateTicket.sendRequest smbCallOut = new LynxCreateTicket.sendRequest();
		system.debug('CallOut: ' + s.Subject + ',' + s.Description + ',' + s.Project_number__c + ',' + s.casenumber + ',' + UserInfo.getFirstName() + ',' + UserInfo.getLastName() + ',' + extractPhone + ',' + userInfo.getUserEmail() + ',' + caseID + ',' + s.My_Business_Requests_Type__c + ',' + extract + ',' + extractAcess + ',' + extractPhone + ',' + extractName);
		try {
			smbCallOut.SRequest(s.Subject, s.Description, s.Project_number__c, s.casenumber, UserInfo.getFirstName(), UserInfo.getLastName(), extractPhone, userInfo.getUserEmail(), caseID, s.My_Business_Requests_Type__c, extract, extractAcess, extractPhone, extractName);

		} catch (Exception e) {
			System.debug('ERROR:' + e);
			try {
				Ticket_Event__c NewEvent = new Ticket_Event__c(
						Case_Number__c = s.casenumber,
						Event_Type__c = 'New Ticket',
						Status__c = 'New',
						Failure_Reason__c = 'Ticket failed:' + e,
						Notify_Admin__c = true,
						//Lynx_Ticket_Number__c = ticketNum,
						Case_Id__c = CaseId
				);
				System.debug('LynxTicketCreate->NewEvent, CaseNumber(' + s.casenumber + ')');
				insert NewEvent;

			} catch (DmlException ex) {
				System.debug('LynxTicketCreate->TicketEvent, exception: ' + ex.getMessage());
			}
		}
	}
	@future(callout=true)
	public static void SendLynxVitil(String caseId) {
		Case s = [Select Subject, casenumber, Description, My_Business_Requests_Type__c, Project_number__c, Contact_Phone_Number2__c, Customer_First_Name_contact__c, Customer_Last_Name_contact__c, Customer_Phone_contact__c, ENTP_AccessDay__c, ENTP_Condition__c, Id FROM Case WHERE id = :caseId LIMIT 1];
		String extract = s.ENTP_Condition__c;
		String extractAcess = s.ENTP_AccessDay__c;
		String extractPhone = s.Customer_Phone_contact__c;
		String extractName = s.Customer_First_Name_contact__c ;

		VITILcare_LynxCreate.sendRequest smbCallOut = new VITILcare_LynxCreate.sendRequest();
		smbCallOut.SRequest(s.Subject, s.Description, s.Project_number__c, s.casenumber, UserInfo.getFirstName(), UserInfo.getLastName(), s.Contact_Phone_Number2__c, userInfo.getUserEmail(), caseID, s.My_Business_Requests_Type__c, extractAcess, extractPhone, extractName, extract);
	}
	@future(callout=true)
	public static void SendRemedyCreate(String caseId) {
		String BUIDChk = '';
		STring COMIDChk = '';
		user cntId = [Select ContactId, AccountId, LanguageLocaleKey FROM User WHERE id = :UserInfo.getUserId()];

		String ContactI = cntId.ContactId;
		String AccountI = cntId.AccountId;
		String UserLanguage = cntId.LanguageLocaleKey;

		Account g;
		Contact p;
		if (!Test.isRunningTest()) {
			g = [SELECT Remedy_Business_Unit_Id__c, Remedy_Company_Id__c FROM Account WHERE id = :AccountI LIMIT 1];
			p = [SELECT Remedy_PIN__c FROM Contact WHERE id = :ContactI LIMIT 1];
		} else {
			g = [SELECT Remedy_Business_Unit_Id__c, Remedy_Company_Id__c FROM Account LIMIT 1];
			p = [SELECT Remedy_PIN__c FROM Contact LIMIT 1];
		}
		BUIDChk = g.Remedy_Business_Unit_Id__c;
		COMIDChk = g.Remedy_Company_Id__c;
		String PINNumchk = p.Remedy_PIN__c;

		if (PINNumchk != null) {
			case s = [SELECT Subject, casenumber, Description, My_Business_Requests_Type__c, Contact_Phone_Number2__c, Id FROM Case WHERE id = :caseId LIMIT 1];
			/*if (Test.isRunningTest()) {
				return;
			}*/

			//Rob - 12/15/2016 : Make the call to RemedyCreateTicket to send info to Remedy Web Service

			RemedyCreateTicket.sendRequest smbCallOut = new RemedyCreateTicket.sendRequest();
			smbCallOut.SRequest(s.Subject, s.Description, PINNumchk, s.casenumber, UserInfo.getFirstName(), UserInfo.getLastName(), BUIDChk, COMIDChk, userInfo.getUserEmail(), caseId, s.My_Business_Requests_Type__c, UserLanguage);
		}

	}

	public static void ENTPCreateLegacyTickets(Map<Id, Case> newMap) {
		String CustRole = '';
		String CustPortName = '';
		List<User> TelusProducts = [SELECT Customer_Portal_Role__C, Customer_Portal_Name__c FROM User WHERE id = :UserInfo.getUserId()];

		if (TelusProducts.size() > 0) {
			CustRole = TelusProducts[0].Customer_Portal_Role__C;
			CustPortName = TelusProducts[0].Customer_Portal_Name__c;

			Set<Id> caseIdN = new Set<Id>();
			for (Id caseId : newMap.keyset()) {
				caseIdN.add(caseId);
			}

			List<Case> newList = [SELECT Id, Lynx_Ticket_Number__c, Status FROM Case WHERE Id IN :caseIdN order by CreatedDate];

			if (newList.size() > 0) {
				id caseidVC = newList[0].id;
				if (CustRole == 'tps') {
					System.debug('Lynx Ticket Number:=' + newList[0].Lynx_Ticket_Number__c);
					if (newList[0].Lynx_Ticket_Number__c == null || newList[0].Lynx_Ticket_Number__c == '') {
						ENTPNewCaseTriggerHelper.SendLynxCreate(newList[0].id);
					}
				}

				if (CustPortName == 'vitilcare') {
					if (newList[0].Lynx_Ticket_Number__c == '' || newList[0].Lynx_Ticket_Number__c == null) {
						System.debug('Lynx Number:=' + newList[0].Lynx_Ticket_Number__c);
						ENTPNewCaseTriggerHelper.SendLynxVitil(newList[0].id);
					}
				}
				if (CustRole == 'private cloud') {

					if (newList[0].Lynx_Ticket_Number__c == '' || newList[0].Lynx_Ticket_Number__c == null) {
						System.debug('Remedy Ticket Number:=' + newList[0].Lynx_Ticket_Number__c);
						ENTPNewCaseTriggerHelper.SendRemedyCreate(newList[0].id);
					}
				}
				if (CustRole == 'mits') {
					if (newList[0].Lynx_Ticket_Number__c == '' || newList[0].Lynx_Ticket_Number__c == null) {
						System.debug('Remedy Ticket Number:=' + newList[0].Lynx_Ticket_Number__c);
						ENTPNewCaseTriggerHelper.SendRemedyCreate(newList[0].id);
					}
				}
			}
		}
	}
}