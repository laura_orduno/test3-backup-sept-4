/*
 * Tests for trac_RepairCallout
 *
 * @author: Grant Adamson, Traction On Demand 
 */
 
@isTest
private class trac_RepairCalloutTest {

	private static testMethod void testValidateSuccess() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Case c = new Case(ESN_MEID_IMEI__c = trac_RepairTestUtils.REPAIR_ESN_SUCCESS,
											Repair_Type__c = 'Device',
											Mobile_number__c = '6041112222');
											
		insert c;
		
		trac_RepairCalloutResponse resp = trac_RepairCallout.validate(c);
		
		//system.assertEquals(true,resp.success);
	}
	
	
	private static testMethod void testValidateWarning() {
		system.runas(trac_RepairTestUtils.getTestUser()) {
			trac_RepairTestUtils.createTestEndpoint();
			
			Case c = new Case(ESN_MEID_IMEI__c = trac_RepairTestUtils.REPAIR_ESN_WARNING,
												Repair_Type__c = 'Device',
												Mobile_number__c = '6041112222');
												
			insert c;
			
			trac_RepairCalloutResponse resp = trac_RepairCallout.validate(c);
			
			//system.assertEquals(true,resp.success);
			//system.assertEquals('WARNING',resp.messageType);
//			system.assertEquals('Subscriber is Not Found.',resp.messageBody);
		}
	}
	
	
	private static testMethod void testValidateFailureProductNotFound() {
		system.runas(trac_RepairTestUtils.getTestUser()) {
			trac_RepairTestUtils.createTestEndpoint();
			
			Case c = new Case(ESN_MEID_IMEI__c = trac_RepairTestUtils.REPAIR_ESN_NOT_FOUND,
												Repair_Type__c = 'Device',
												Mobile_number__c = '6041112222');
												
			insert c;
			
			trac_RepairCalloutResponse resp = trac_RepairCallout.validate(c);
			
			//system.assertEquals(false,resp.success);
//			system.assertEquals('Product Info is Not Found.',resp.messageBody);
		}
	}
	
	
	private static testMethod void testValidateFailureESNPhoneNoMatch() {
		system.runas(trac_RepairTestUtils.getTestUser()) {
			trac_RepairTestUtils.createTestEndpoint();
			
			Case c = new Case(ESN_MEID_IMEI__c = trac_RepairTestUtils.REPAIR_ESN_NO_MATCH,
												Repair_Type__c = 'Device',
												Mobile_number__c = '6041112222');
												
			insert c;
			
			trac_RepairCalloutResponse resp = trac_RepairCallout.validate(c);
			
			//system.assertEquals(false,resp.success);
			//system.assertEquals('Equipment serial # and phone number does not match, please verify the subscriber', resp.messageBody);
		}
	}
	
	
	private static testMethod void testValidateException() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Case c = new Case(ESN_MEID_IMEI__c = trac_RepairTestUtils.REPAIR_ESN_EXCEPTION,
											Repair_Type__c = 'Device',
											Mobile_number__c = '6041112222');
											
		insert c;
		
		trac_RepairCalloutResponse resp = trac_RepairCallout.validate(c);
		
		//system.assertEquals(false,resp.success);
		//system.assertEquals('It Broke',resp.messageBody);
	}
	
	private static testMethod void testValidateMissingEndpoint() {
		Case c = new Case(ESN_MEID_IMEI__c = trac_RepairTestUtils.REPAIR_ESN_SUCCESS,
											Repair_Type__c = 'Device',
											Mobile_number__c = '6041112222');
											
		insert c;
		
		trac_RepairCalloutResponse resp = trac_RepairCallout.validate(c);
		
		//system.assertEquals(false,resp.success);
		//system.assertEquals(Label.MISSING_ENDPOINT, resp.messageBody);
	}
	
	
	private static testMethod void testInitCreateRepair() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Case c = new Case(ESN_MEID_IMEI__c = trac_RepairTestUtils.REPAIR_ESN_SUCCESS,
											Repair_Type__c = 'Device',
											Mobile_number__c = '6041112222',
											Customer_Title_client__c = 'Mr.',
											Customer_First_Name_client__c = 'CliFirst',
											Customer_Last_Name_client__c = 'CliLast',
											Accessory_SKU__c = 'jkgdfkhdfgdf',
											Customer_Title_contact__c = 'Mr.',
											Customer_First_Name_contact__c = 'ConFirst',
											Customer_Last_Name_contact__c = 'ConLast',
											Customer_Phone_contact__c = '6041234567',
											Customer_Email_contact__c = 'contact@example.com',
											Address_shipping__c = '1234 Telus Road',
											City_shipping__c = 'Burnaby',
											Province_shipping__c = 'BC',
											Postal_Code_shipping__c = 'V5J1A1',
											Descrip_of_Defect_Troubleshooting_Perf__c = 'on fire',
											Defects_behaviour__c = 'Intermittent',
											Is_there_any_Cosmetic_Damage__c = true,
											Comments_create_repair__c = 'chipped plastic on top left corner',
											Estimated_Repair_Cost_Up_To__c = 240.32,
											Shipping_Required__c = false,
											Use_Alternate_Address__c = false);
		//c.put('physical_damage__c','Yes');											
		insert c;
		
		trac_RepairCalloutResponse resp = trac_RepairCallout.initCreateRepair(c);
		
		//system.assertEquals(true,resp.success);
	}
	
	
	private static testMethod void testInitCreateRepairFrenchUser() {
		User u = trac_RepairTestUtils.getTestUser();
		u.LanguageLocaleKey = 'FR';
		
		system.runas(u) {
			trac_RepairTestUtils.createTestEndpoint();
			
			Case c = new Case(ESN_MEID_IMEI__c = trac_RepairTestUtils.REPAIR_ESN_SUCCESS,
												Repair_Type__c = 'Device',
												Mobile_number__c = '6041112222',
												Customer_Title_client__c = 'Mr.',
												Customer_First_Name_client__c = 'CliFirst',
												Customer_Last_Name_client__c = 'CliLast',
												Accessory_SKU__c = 'jkgdfkhdfgdf',
												Customer_Title_contact__c = 'Mr.',
												Customer_First_Name_contact__c = 'ConFirst',
												Customer_Last_Name_contact__c = 'ConLast',
												Customer_Phone_contact__c = '6041234567',
												Customer_Email_contact__c = 'contact@example.com',
												Address_shipping__c = '1234 Telus Road',
												City_shipping__c = 'Burnaby',
												Province_shipping__c = 'BC',
												Postal_Code_shipping__c = 'V5J1A1',
												Descrip_of_Defect_Troubleshooting_Perf__c = 'on fire',
												Defects_behaviour__c = 'Intermittent',
												Is_there_any_Cosmetic_Damage__c = true,
												Comments_create_repair__c = 'chipped plastic on top left corner',
												Estimated_Repair_Cost_Up_To__c = 240.32,
												Shipping_Required__c = false,
												Use_Alternate_Address__c = false);
							
			//c.put('physical_damage__c','Yes');					
			insert c;
			
			trac_RepairCalloutResponse resp = trac_RepairCallout.initCreateRepair(c);
			
			//system.assertEquals(true,resp.success);
		}
	}
	
	
	private static testMethod void testInitCreateRepairFailure() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Case c = new Case(ESN_MEID_IMEI__c = trac_RepairTestUtils.REPAIR_ESN_FAILURE,
											Repair_Type__c = 'Device',
											Mobile_number__c = '6041112222',
											Customer_Title_client__c = 'Mr.',
											Customer_First_Name_client__c = 'CliFirst',
											Customer_Last_Name_client__c = 'CliLast',
											Accessory_SKU__c = 'jkgdfkhdfgdf',
											Customer_Title_contact__c = 'Mr.',
											Customer_First_Name_contact__c = 'ConFirst',
											Customer_Last_Name_contact__c = 'ConLast',
											Customer_Phone_contact__c = '6041234567',
											Customer_Email_contact__c = 'contact@example.com',
											Address_shipping__c = '1234 Telus Road',
											City_shipping__c = 'Burnaby',
											Province_shipping__c = 'BC',
											Postal_Code_shipping__c = 'V5J1A1',
											Descrip_of_Defect_Troubleshooting_Perf__c = 'on fire',
											Defects_behaviour__c = 'Intermittent',
											Is_there_any_Cosmetic_Damage__c = true,
											Comments_create_repair__c = 'chipped plastic on top left corner',
											Estimated_Repair_Cost_Up_To__c = 240.32,
											Shipping_Required__c = false,
											Use_Alternate_Address__c = false);
											
		//c.put('physical_damage__c','Yes');
		insert c;
		
		trac_RepairCalloutResponse resp = trac_RepairCallout.initCreateRepair(c);
		
		//system.assertEquals(false,resp.success);
	}
	
	
	private static testMethod void testInitCreateRepairException() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Case c = new Case(ESN_MEID_IMEI__c = trac_RepairTestUtils.REPAIR_ESN_EXCEPTION,
											Repair_Type__c = 'Device',
											Mobile_number__c = '6041112222',
											Customer_Title_client__c = 'Mr.',
											Customer_First_Name_client__c = 'CliFirst',
											Customer_Last_Name_client__c = 'CliLast',
											Accessory_SKU__c = 'jkgdfkhdfgdf',
											Customer_Title_contact__c = 'Mr.',
											Customer_First_Name_contact__c = 'ConFirst',
											Customer_Last_Name_contact__c = 'ConLast',
											Customer_Phone_contact__c = '6041234567',
											Customer_Email_contact__c = 'contact@example.com',
											Address_shipping__c = '1234 Telus Road',
											City_shipping__c = 'Burnaby',
											Province_shipping__c = 'BC',
											Postal_Code_shipping__c = 'V5J1A1',
											Descrip_of_Defect_Troubleshooting_Perf__c = 'on fire',
											Defects_behaviour__c = 'Intermittent',
											Is_there_any_Cosmetic_Damage__c = true,
											Comments_create_repair__c = 'chipped plastic on top left corner',
											Estimated_Repair_Cost_Up_To__c = 240.32,
											Shipping_Required__c = false,
											Use_Alternate_Address__c = false);
											
		//c.put('physical_damage__c','Yes');
		insert c;
		
		trac_RepairCalloutResponse resp = trac_RepairCallout.initCreateRepair(c);
		
		//system.assertEquals(false,resp.success);
		//system.assertEquals('It Broke',resp.messageBody);
		//system.assertEquals('It Broke', c.Create_Repair_Last_Error__c);
	}
	
	
	private static testMethod void testInitCreateRepairLongException() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Case c = new Case(ESN_MEID_IMEI__c = trac_RepairTestUtils.REPAIR_ESN_LONG_EXCEPTION,
											Repair_Type__c = 'Device',
											Mobile_number__c = '6041112222',
											Customer_Title_client__c = 'Mr.',
											Customer_First_Name_client__c = 'CliFirst',
											Customer_Last_Name_client__c = 'CliLast',
											Accessory_SKU__c = 'jkgdfkhdfgdf',
											Customer_Title_contact__c = 'Mr.',
											Customer_First_Name_contact__c = 'ConFirst',
											Customer_Last_Name_contact__c = 'ConLast',
											Customer_Phone_contact__c = '6041234567',
											Customer_Email_contact__c = 'contact@example.com',
											Address_shipping__c = '1234 Telus Road',
											City_shipping__c = 'Burnaby',
											Province_shipping__c = 'BC',
											Postal_Code_shipping__c = 'V5J1A1',
											Descrip_of_Defect_Troubleshooting_Perf__c = 'on fire',
											Defects_behaviour__c = 'Intermittent',
											Is_there_any_Cosmetic_Damage__c = true,
											Comments_create_repair__c = 'chipped plastic on top left corner',
											Estimated_Repair_Cost_Up_To__c = 240.32,
											Shipping_Required__c = false,
											Use_Alternate_Address__c = false);
								
		//c.put('physical_damage__c','Yes');			
		insert c;
		
		trac_RepairCalloutResponse resp = trac_RepairCallout.initCreateRepair(c);
		
		//system.assertEquals(false,resp.success);
		//system.assertEquals(255, c.create_repair_last_error__c.length());
	}
	
	
	private static testMethod void testInitCreateRepairMissingEndpoint() {	
		Case c = new Case(ESN_MEID_IMEI__c = trac_RepairTestUtils.REPAIR_ESN_SUCCESS,
											Repair_Type__c = 'Device',
											Mobile_number__c = '6041112222',
											Customer_Title_client__c = 'Mr.',
											Customer_First_Name_client__c = 'CliFirst',
											Customer_Last_Name_client__c = 'CliLast',
											Accessory_SKU__c = 'jkgdfkhdfgdf',
											Customer_Title_contact__c = 'Mr.',
											Customer_First_Name_contact__c = 'ConFirst',
											Customer_Last_Name_contact__c = 'ConLast',
											Customer_Phone_contact__c = '6041234567',
											Customer_Email_contact__c = 'contact@example.com',
											Address_shipping__c = '1234 Telus Road',
											City_shipping__c = 'Burnaby',
											Province_shipping__c = 'BC',
											Postal_Code_shipping__c = 'V5J1A1',
											Descrip_of_Defect_Troubleshooting_Perf__c = 'on fire',
											Defects_behaviour__c = 'Intermittent',
											Is_there_any_Cosmetic_Damage__c = true,
											Comments_create_repair__c = 'chipped plastic on top left corner',
											Estimated_Repair_Cost_Up_To__c = 240.32,
											Shipping_Required__c = false,
											Use_Alternate_Address__c = false);
										
		//c.put('physical_damage__c','Yes');	
		insert c;
		
		trac_RepairCalloutResponse resp = trac_RepairCallout.initCreateRepair(c);
		
		//system.assertEquals(false,resp.success);
		//system.assertEquals(Label.MISSING_ENDPOINT,resp.messageBody);
	}
	
	
	private static testMethod void testGetRepairStatus() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Set<String> repairIds = new Set<String>{'1234', '5678'};
		
		Map<String,String> idToStatus = trac_RepairCallout.getRepairStatus(repairIds);
		
		//system.assert(idToStatus.size() > 0);
	}
	
	
	private static testMethod void testGetRepairStatusEmptyResponse() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Set<String> repairIds = new Set<String>{trac_RepairTestUtils.REPAIR_ID_EMPTY, '12345'};
		
		Map<String,String> idToStatus = trac_RepairCallout.getRepairStatus(repairIds);
		
		//system.assertEquals(0,idToStatus.size());
	}
	
	
	private static testMethod void testGetRepairStatusException() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Set<String> repairIds = new Set<String>{'12345', trac_RepairTestUtils.REPAIR_ID_EXCEPTION};
		
		Boolean caughtException = false;
		try {
			Map<String,String> idToStatus = trac_RepairCallout.getRepairStatus(repairIds);
		} catch (Exception e) {
			caughtException = true;
		}
		
		//system.assert(caughtException);
	}
	
	
	private static testMethod void testGetRepair() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Case c = new Case(subject = 'test');
		
		insert c;
		
		trac_RepairCalloutResponse resp = trac_RepairCallout.getRepair(c);
		
		//system.assertEquals(true,resp.success);
	}
	
	
	private static testMethod void testGetRepairIdMismatch() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Case c = new Case(subject = 'test', repair__c = trac_RepairTestUtils.REPAIR_ID_NO_MATCH);
		
		insert c;
		
		trac_RepairCalloutResponse resp = trac_RepairCallout.getRepair(c);
		
		// checking for the mismatch not actually performed at this level
		//system.assertEquals(true,resp.success);
	}
	
	
	private static testMethod void testGetRepairFailure() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Case c = new Case(repair__c = trac_RepairTestUtils.REPAIR_ID_FAILURE);
		
		insert c;
		
		trac_RepairCalloutResponse resp = trac_RepairCallout.getRepair(c);
		
		//system.assertEquals(false,resp.success);
	}
	
	
	private static testMethod void testGetRepairException() {
		trac_RepairTestUtils.createTestEndpoint();
		
		Case c = new Case(repair__c = trac_RepairTestUtils.REPAIR_ID_EXCEPTION);
		
		insert c;
		
		trac_RepairCalloutResponse resp = trac_RepairCallout.getRepair(c);
		
		//system.assertEquals(false,resp.success);
		//system.assertEquals('It Broke',resp.messageBody);
	}
	
	
	private static testMethod void testGetRepairMissingEndpoint() {		
		Case c = new Case(subject = 'test');
		
		insert c;
		
		trac_RepairCalloutResponse resp = trac_RepairCallout.getRepair(c);
		
		//system.assertEquals(false,resp.success);
		//system.assertEquals(Label.MISSING_ENDPOINT, resp.messageBody);
	}
	
	
	private static testMethod void testUpdateQuoteStatus() {
		system.runas(trac_RepairTestUtils.getTestUser()) {
			trac_RepairTestUtils.createTestEndpoint();
			
			Case c = new Case(repair__c = trac_RepairTestUtils.REPAIR_ID_SUCCESS);
			
			insert c;
			
			trac_RepairCalloutResponse resp = trac_RepairCallout.updateQuoteStatus(c,'ACCEPTED');
			
			//system.assertEquals(true,resp.success);
		}
	}
	
	
	private static testMethod void testUpdateQuoteStatusFailure() {
		system.runas(trac_RepairTestUtils.getTestUser()) {
			trac_RepairTestUtils.createTestEndpoint();
			
			Case c = new Case(repair__c = trac_RepairTestUtils.REPAIR_ID_FAILURE);
			
			insert c;
			
			trac_RepairCalloutResponse resp = trac_RepairCallout.updateQuoteStatus(c,'ACCEPTED');
			
			//system.assertEquals(false,resp.success);
		}
	}
	
	
	private static testMethod void testUpdateQuoteStatusException() {
		system.runas(trac_RepairTestUtils.getTestUser()) {
			trac_RepairTestUtils.createTestEndpoint();
			
			Case c = new Case(repair__c = trac_RepairTestUtils.REPAIR_ID_EXCEPTION);
			
			insert c;
			
			trac_RepairCalloutResponse resp = trac_RepairCallout.updateQuoteStatus(c,'ACCEPTED');
			
			//system.assertEquals(false,resp.success);
		}
	}
	
	
	private static testMethod void testUpdateQuoteStatusMissingTeamTelusId() {
		User u = trac_RepairTestUtils.getTestUser();
		u.Team_TELUS_ID__c = null;
		
		system.runas(u) {
			trac_RepairTestUtils.createTestEndpoint();
			
			Case c = new Case(repair__c = trac_RepairTestUtils.REPAIR_ID_EXCEPTION);
			
			insert c;
			
			trac_RepairCalloutResponse resp = trac_RepairCallout.updateQuoteStatus(c,'ACCEPTED');
			
			//system.assertEquals(false,resp.success);
			//system.assertEquals(Label.TEAM_TELUS_ID_MISSING, resp.messageBody);
		}
	}
	
	
	private static testMethod void testUpdateQuoteStatusMissingEndpoint() {
		system.runas(trac_RepairTestUtils.getTestUser()) {		
			Case c = new Case(repair__c = trac_RepairTestUtils.REPAIR_ID_SUCCESS);
			
			insert c;
			
			trac_RepairCalloutResponse resp = trac_RepairCallout.updateQuoteStatus(c,'ACCEPTED');
			
			//system.assertEquals(false,resp.success);
			//system.assertEquals(Label.MISSING_ENDPOINT, resp.messageBody);
		}
	}
	
	
	private static testMethod void testGetCleanLocale() {
		User u = trac_RepairTestUtils.getTestUser();
		u.LocaleSIDKey = 'en_IN';
		insert u;
		
		system.runAs(u) {
			//system.assertEquals('en_CA', trac_RepairCallout.getCleanLocale());
		}
		
		u.LocaleSIDKey = 'fr_CA';
		u.LanguageLocaleKey = 'fr';
		update u;
		
		system.runAs(u) {
			//system.assertEquals('fr_CA', trac_RepairCallout.getCleanLocale());
		}
	}
	
}