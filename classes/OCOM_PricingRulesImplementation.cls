global with sharing class OCOM_PricingRulesImplementation implements vlocity_cmt.GlobalInterfaces.PricingInterface {
    global void priceLineItems(SObject parent, List<SObject> itemList) {
        if(itemList.isEmpty())return;
        vlocity_cmt.DefaultPricingImplementation defaultPricing = new vlocity_cmt.DefaultPricingImplementation();
        defaultPricing.priceLineItems(parent, itemList);

       // Trun OFF TelcoOrderItemTriggerHandler trigger 
       // OCOM_VlocityTriggerUtil.turnOffVlocityTrigger();
            
        
        String objectName = String.valueOf(itemList[0].getSObjectType());
        List<Id> ruleIds = new List<Id>();
        
        Map <String,Id> mapRules = new Map <String,Id>();
        
        for(vlocity_cmt__Rule__c rule : [SELECT Id, Name FROM vlocity_cmt__Rule__c WHERE vlocity_cmt__Type__c = 'Pricing' AND vlocity_cmt__ObjectName__c =: objectName AND vlocity_cmt__IsActive__c = true]){
            //ruleIds.add(rule.Id);
            mapRules.put(rule.name,rule.id);
        }
        
        ruleIds.add(mapRules.get('OCOM Text Calculation Procedure') );
        
        
        
        Map<String, Object> input = new Map<String, Object>();
        Map<String, Object> output = new Map<String, Object>();
        input.put('ruleIdentifierType', 'Id');
        input.put('ruleIdentifiersList', ruleIds);
        //input.put('ruleIdentifiersList', new List<String>{'test rule'});
        
        vlocity_cmt.FlowStaticMap.flowMap.put('parent', parent);
        vlocity_cmt.FlowStaticMap.flowMap.put('itemList', itemList);
        input.put('isTriggeredByFlow', false);
        input.put('flowMap', vlocity_cmt.FlowStaticMap.flowMap);
        
        
        vlocity_cmt.RuleSupport ruleSupport = new vlocity_cmt.RuleSupport();
        RuleSupport.invokeMethod('executeRules',input, output, null);    
        
        // Trun ON TelcoOrderItemTriggerHandler trigger 
       // OCOM_VlocityTriggerUtil.turnOnVlocityTrigger();
        



        //ruleIds = new List<Id>();
                
        ////ruleIds.add(mapRules.get('Set Override Child Items Prices to Zero') );
        //ruleIds.add(mapRules.get('OCOM Manual Price Override') );



        //input = new Map<String, Object>();
        //output = new Map<String, Object>();
        //input.put('ruleIdentifierType', 'Id');
        //input.put('ruleIdentifiersList', ruleIds);
        ////input.put('ruleIdentifiersList', new List<String>{'test rule'});
       
        //Id SobjectId = parent.Id;

        //if(itemlist.size()>0){
            
        //    Id objectId = itemlist[0].Id; 

        //    DescribeSObjectResult describeResult = objectId.getSObjectType().getDescribe();
        //    List<String> fieldNames = new List<String>( describeResult.fields.getMap().keySet() );

        //    String query =
        //    ' SELECT ' +
        //    String.join( fieldNames, ',' ) +
        //    ' FROM ' +
        //    describeResult.getName() +
        //    ' WHERE ' +
        //    SobjectId.getSObjectType().getDescribe().getName() + 'Id = \'' + SobjectId + '\'' ;

        //     ItemList = Database.query( query );
            
        //}

        //vlocity_cmt.FlowStaticMap.flowMap.put('parent', parent);
        //vlocity_cmt.FlowStaticMap.flowMap.put('itemList', itemList);
        //input.put('isTriggeredByFlow', false);
        //input.put('flowMap', vlocity_cmt.FlowStaticMap.flowMap);
        
        
        //ruleSupport = new vlocity_cmt.RuleSupport();
        //RuleSupport.invokeMethod('executeRules',input, output, null);      
        
        // Trun ON TelcoOrderItemTriggerHandler trigger  
     
    }
}