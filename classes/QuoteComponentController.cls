public with sharing virtual class QuoteComponentController {
  public QuoteComponentParent parentController { get; 
    set {
      if (value != null) {
        parentController = value;
        parentController.setComponentController(this);
      }
    }
  }
}