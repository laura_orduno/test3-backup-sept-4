@isTest
public with sharing class trac_External_Assignee_Test {
		
		@isTest(SeeAllData=true) // required due to ADRA Lead Allocation configuration
    private static void testExternalAssigneeLead() {
        insertTestExternalUserPortalSettings();
        
        Lead l = new Lead(
	        FirstName = 'Alex',
	        LastName = 'Miller',
	        Company = 'Developer',
	        Phone = '6049969449',
	        Email = 'amiller@tractionondemand.com'
        );
        insert l;
        
        Task t = new Task(
        	External_Assignee__c = 'alex@google.com',
        	Priority = 'Test',
        	ActivityDate = Date.today().addDays(-1),
        	Type = 'Call',
        	Subject = 'Test call',
        	Status = 'Not Started',
        	WhoId = l.Id,
        	Call_Outcome__c = 'not null',
        	SMB_Call_Type__c = 'not null either'
        );
        insert t;
        
        Task txi = [
        	Select Token__c
        	From Task
        	Where Id = :t.Id
        ];
        system.assertNotEquals(null, txi.Token__c);
        
        update new Task(id = t.id, Status = 'Completed');
    }
    
    
    private static testMethod void testExternalAssigneeContact() {
        insertTestExternalUserPortalSettings();
        
        Account a = new Account(Name = 'TestAccount');
        insert a;
        
        Contact c = new Contact(
        	FirstName = 'Alex',
        	LastName = 'Miller',
        	AccountId = a.Id,
        	Phone = '6049969449',
        	Email = 'amiller@tractionondemand.com'
        );
        insert c;
        
        Task t = new Task(
        	ActivityDate = Date.today().addDays(-1),
        	Type = 'Call',
        	Priority = 'Test',
        	Status = 'Not Started',
        	WhoId = c.Id,
        	WhatId = a.Id,
        	Call_Outcome__c = 'not null',
        	SMB_Call_Type__c = 'not null either',
        	External_Assignee__c = 'alex@google.com'
        );
        insert t;
        
        Task txi = [
        	Select Token__c
        	From Task
        	Where Id = :t.Id
        ];
        system.assertNotEquals(null, txi.Token__c);
        
        update new Task(id = t.id, Status = 'Completed');
    }
    
    
    private static testMethod void testExternalAssigneeOpportunity() {
        insertTestExternalUserPortalSettings();
        
        Account a = new Account(Name = 'TestAccount');
        insert a;
        
        Contact c = new Contact(
        	FirstName = 'Alex',
        	LastName = 'Miller',
        	AccountId = a.Id,
        	Phone = '6049969449',
        	Email = 'amiller@tractionondemand.com'
        );
        insert c;
        
        Opportunity o = new Opportunity(
        	Name = 'test opp',
        	AccountId = a.id,
        	CloseDate = date.today(),
        	StageName = 'test'
        );
        insert o;
        
        Task t = new Task(
        	ActivityDate = Date.today().addDays(-1),
        	Type = 'Call',
        	Priority = 'Test',
        	Status = 'Not Started',
        	WhoId = c.Id,
        	WhatId = o.Id,
        	Call_Outcome__c = 'not null',
        	SMB_Call_Type__c = 'not null either',
        	External_Assignee__c = 'alex@google.com'
        );
        insert t;
        
        Task txi = [
        	Select Token__c
        	From Task
        	Where Id = :t.Id
        ];
        system.assertNotEquals(null, txi.Token__c);
        
        update new Task(id = t.id, Status = 'Completed');
    }
    
    @isTest
    private static void insertTestExternalUserPortalSettings() {
    	// if org already contains custom settings, remove for test
    	delete [SELECT Id from ExternalUserPortal__c];
    	
    	ExternalUserPortal__c eups = new ExternalUserPortal__c(
    		Base_URL__c = 'http://login.salesforce.com',
    		HMAC_Digest_Key__c = '/eYPmJ0eXTb+8R/X7YE9Skv6Ly8tfChkR0gdWt9ngXg=',
    		Portal_URL__c = 'http://telus.force.com/externalservice'
    	);
    	
    	insert eups;
    }
    
    
}