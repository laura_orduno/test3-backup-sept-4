/*
###########################################################################
# File..................: CallToE2Vault.cls
# Created by............: Gaurav Tiwari (IBM)
# Created Date..........: 14-Dec-2015
# Description...........: This Class is used to call external E2Vault system to get the bill pdf url. 
#                         It also contains the method to parse the resoponse.
# 
#
# *********** VERSION UPDATES ***********************************************************************
#
# 23-Feb-2016    Gaurav Tiwari        Created File
#
*/
public class CallToE2Vault  {
   
     public static Boolean showErrMsg1 {get;set;}
     public static Boolean showErrMsg {get;set;}
     /*
     * Initialize constructor
     */
     public CallToE2Vault(){}
     
     /* 
     * Description  Purpose - Instantiate a new HTTP request to E2vault System. This is Rest call and e2vault return the JSON object
     */
    @TestVisible private static HttpResponse doCallout(String strUrl){
       
    // Instantiate a new http object
        E2VaultConfig__c e2Config = E2VaultConfig__c.getInstance('CertificateName');
        String certificateName= e2Config.value__c;
        
        Http h = new Http();
    
        // Instantiate a new HTTP request, specify the method (GET) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setEndpoint(strUrl);
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('Accept', 'application/json');
        req.setHeader('Authorization','Bearer token');
        req.setClientCertificateName(certificateName); // Certificate name to access E2Vault system 
        req.setMethod('GET');
        
        HttpResponse res = new HttpResponse();
        res = h.send(req);
        System.debug('E2Vault_Rest HttpResponse  Response ---> '+ res.getBody());
        return res;
    }  
    
    /* 
     * Description - This method will parse the response and return PDF url.
     */
     
     public static String getPdfURL(String endPointUrl){
        String pdfurl;
        String strSignature;
        
        HttpResponse  res = doCallout(endPointUrl);
        strSignature = res.getBody();
        System.debug('**ResponseSecond='+ strSignature);
        if (res.getStatus() == 'OK' && res.getStatusCode() == 200){
            JSONParser parser = JSON.createParser(strSignature);
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)) {
                    String fieldName = parser.getText();
                    parser.nextToken();
                    if (fieldName == 'url') {
                        pdfurl= parser.getText();
                        System.debug('parser RESULT          URL=========> '+ pdfurl);
                    } 
                }
            }
       }else{
            //addErrorMessage(System.Label.E2vault_Error_Message);
            showErrMsg = true;
       }
       return pdfurl;
    }
    
  
    /* 
     * Description - This method will parse the response and will extract the bill dates and locator id
     */
    public static Map<String, String> getLocatorIds(String endPointUrl){
        Map<String, String> m1 = new Map<String, String>();
        String strSignature;
        String billDt;
        String locatorId;
        String nlocatorid;
        HttpResponse  res1 = doCallout(endPointUrl);
        system.debug('Sandip@@@ ' + res1.getBody());
        strSignature = res1.getBody();
        System.debug('Gaurav'+ res1.getStatus());
        
        if(res1.getStatusCode() == 400){
           system.debug('@@@Label' + System.Label.E2vault_Error_Message_2);
           //addErrorMessage(System.Label.E2vault_Error_Message_2);
           showErrMsg1 = true;
           return null;
        }
            
        if(res1.getStatusCode() == 404){
           //addErrorMessage(res1.getStatus());
           showErrMsg = true;
           return null;
        }
        if (res1.getStatus() == 'OK' && res1.getStatusCode() == 200){    
            JSONParser parser = JSON.createParser(strSignature);
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)) {
                    String fieldName = parser.getText();
                    parser.nextToken();
                    
                    if (fieldName == 'statusTxt') {
                       String SCode= parser.getText();
                        if (SCode== 'No information found for the given BAN') {
                           //addErrorMessage(System.Label.E2vault_Error_Message_2);
                           showErrMsg1 = true;
                           return null;
                        }
                    }
                    if (fieldName == 'statusCd'){
                        String StCode= parser.getText();
                        if (StCode== '404') {
                           //addErrorMessage(System.Label.E2vault_Error_Message_2);
                           showErrMsg1 = true;
                           return null;
                        }
                    }
                    
                    if (fieldName == 'locatorId') {
                        system.debug('parse locatorid');
                        locatorid= parser.getText();
                        nlocatorid=locatorid;
                        System.debug('parser RESULT          LocatorId=========> '+ locatorid);
                    }  
                   
                    if (fieldName == 'billDt') {
                        system.debug('parse bill date');
                        billdt= parser.getText();
                        String[] myDateOnly = billdt.split('-');
                        Integer myIntDate = integer.valueOf(myDateOnly[2]);
                        system.debug('myintdate'+myIntDate);
                        Integer myIntMonth = integer.valueOf(myDateOnly[1]);
                        Integer myIntYear = integer.valueOf(myDateOnly[0]);
                        Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);
                        system.debug('date format'+d);
                   
                      // Display the date in the particular format
                        Datetime myDatetime = d;
                        String myDatetimeStr = myDatetime.formatGmt('MMMM d,  yyyy');
                        m1.put(myDatetimeStr, nlocatorid);
                    }
                
                       System.debug('parser RESULT gaurav Map=========> '+ m1);  
                }
            }
        }else{
            //addErrorMessage(System.Label.E2vault_Error_Message);
            showErrMsg = true;
        }
        return m1;
    }  
 
    /*
    * Display error message on page
    */
    public static void addErrorMessage(String msg){
        system.debug('@@@msg' + msg);
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, msg);
        ApexPages.addMessage(myMsg);
    }
    
    /*
    * Log error message in object
    */
    @future
    public static void logError(String classAndMethodName, String recId, String errorCodeAndMsg, String stackTrace){
        webservice_integration_error_log__c errorLog = new webservice_integration_error_log__c
                                                    (apex_class_and_method__c=classAndMethodName,
                                                     external_system_name__c='E2Vault',
                                                     webservice_name__c='E2Vault-InvoiceInfo',
                                                     object_name__c='Account',
                                                     sfdc_record_id__c=recId,
                                                     Error_Code_and_Message__c = errorCodeAndMsg,
                                                     Stack_Trace__c = stackTrace
                                                     );
        insert errorLog;
    }
}