@isTest
public with sharing class AptsDocuSignStatusTriggerTest { 
	static testMethod void testMethod1() {
		//create account
        Account account = new Account(Name = 'test Accout for Comply Custom API 1');
        insert account;
        
        //create contact
        Contact cont = new Contact (LastName='Tiger', Email='Scott@example.com');
        insert cont;
        
        //create agreement
        /*
        Apttus__APTS_Agreement__c agreeement = new Apttus__APTS_Agreement__c(Name = 'test Agreement for Comply Custom API 1', Apttus__Account__c = account.Id, Apttus__Status_Category__c = 'Request', Apttus__Status__c = 'Request', Apttus__Contract_Start_Date__c = date.today(), Apttus__Contract_End_Date__c = date.today().addDays(365), Apts_Envelope_Id__c = '8fd115ee-43c2-480a-bcd3-008910ec4988', Printed_Agreement_Type__c ='BusinessAnywhere',APTS_Email_Address_Customer_Auth_Contact__c = 'abc@apttus.com', APTS_Customer_Signing_Authority__c = 'abc',Contract_Signor__c=cont.id);
        agreeement.Apttus__Term_Months__c = 12;
        insert agreeement;
        */
        contract vContract=new contract(name='test Agreement for Comply Custom API 1',accountid=account.id,status='Request',StartDate=system.today(),ContractTerm=12,Envelop_Id__c='8fd115ee-43c2-480a-bcd3-008910ec4988',Email_Address_Customer_Auth_Contact__c='abc@apttus.com',Customer_Signing_Authority__c='cont.id');
        insert vContract;
        //create docusign status 
        dsfs__DocuSign_Status__c docuSignStatus = new dsfs__DocuSign_Status__c(dsfs__Sender__c = 'Vlocity', dsfs__Sender_Email__c = 'abc@apttus.com', dsfs__Subject__c = 'Please Sign this Contract :', dsfs__Contract__c=vContract.Id);
        insert docuSignStatus;
        
        test.startTest();
        
        dsfs__DocuSign_Status__c docuSignStatus1 = new dsfs__DocuSign_Status__c();
        docuSignStatus1 = [SELECT Id, dsfs__Envelope_Status__c, dsfs__Voided_Reason__c FROM dsfs__DocuSign_Status__c WHERE dsfs__Contract__c=:vContract.Id];
        if(docuSignStatus1 != null) {
        	docuSignStatus1.dsfs__Envelope_Status__c = 'Voided';
        	docuSignStatus1.dsfs__Voided_Reason__c = 'Envelope has expired.';
        	
        	update docuSignStatus1;
        }
        
        test.stopTest();
        /*
        Apttus__APTS_Agreement__c agreeement1 = [SELECT Id, Apttus__Status__c FROM Apttus__APTS_Agreement__c WHERE Id = :agreeement.Id];
        system.assert(agreeement1.Apttus__Status__c == 'Acceptance Expired');*/
	}
}