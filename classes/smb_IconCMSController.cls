public class smb_IconCMSController {
    
    public string objId {get;set {objId=value;}}
    public string objAPIName {get;set {objAPIName=value;}}
    public string iconClick {get;set {iconClick=value;}}
    public boolean renderIcon {get;set {renderIcon=value;}}
    public string errorMessage {get;set;}  

    public Component.Apex.OutputPanel getThePanel(){
        
        list<Field_Icon__c> fieldIconList;
        sObject sobj = null;
        
        try {
            fieldIconList=[select account_icon__r.name, account_icon__r.Hover_Text_Custom_Label__c, account_icon__r.image_url__c,
                                  account_icon__r.css__c, account_icon__r.onClick__c, field_api_name__c, data_type__c, value__c
                           from field_icon__c
                           where account_icon__r.is_active__c = true and object_api_name__c =: objAPIName];

            list<string> fieldNames=new list<string>();
            for(field_icon__c fldIcon:fieldIconList){
                boolean strFound = false;
                for(string tmpStr:fieldNames){
                    if(tmpStr == fldIcon.field_api_name__c)
                        strFound = true;
                }
                if(!strFound){
                    fieldNames.add(fldIcon.field_api_name__c);
                }
            }
            string fieldNameStr = string.join(fieldNames,',');
            
            if(String.isNotEmpty(fieldNameStr)) {
                System.debug('smb_IconCMSController:sobject query: select id,'+fieldNameStr+' from '+objAPIName+' where id=\''+objId+'\' limit 1');
                sobj=database.query('select id,'+fieldNameStr+' from '+objAPIName+' where id=\''+objId+'\' limit 1');
            }
        }
        catch(queryexception qe) {
            errorMessage=qe.getmessage();
            sobj = null;
        }
        
        Component.Apex.OutputPanel outPanel = new Component.Apex.OutputPanel();
        
        if(sobj != null){
            for(field_icon__c fldIcon:fieldIconList){
                boolean showIcon = false;
                
                if(fldIcon.Data_Type__c.equalsIgnoreCase('Text')){
                    string strValue = (string) sobj.get(fldIcon.Field_API_Name__c);
                    if(fldIcon.Value__c.equalsIgnoreCase(strValue)){
                        showIcon = true;
                    }
                }
                else if(fldIcon.Data_Type__c.equalsIgnoreCase('Checkbox')){
                    boolean boolValue = (boolean) sobj.get(fldIcon.Field_API_Name__c);
                    if(boolValue){
                        showIcon = true;
                    }
                }
                else{
                    showIcon = false;
                }

                if(showIcon){
                    Component.Apex.Image inImage = new Component.Apex.Image();
                    
                    inImage.value = fldIcon.Account_Icon__r.Image_URL__c;
                    
                    if(string.isNotEmpty(fldIcon.Account_Icon__r.onClick__c)) {
                        // Replace language token
                        string tmpStr = fldIcon.Account_Icon__r.onClick__c;
                        string tmpLang = UserInfo.getLanguage();
                        if(tmpLang.containsIgnoreCase('fr'))
                            tmpLang = 'fr';
                        else
                            tmpLang = 'en';
                        inImage.onclick = tmpStr.replace('{!language}', tmpLang);
                    }
                    else if(string.isNotEmpty(iconClick)) {
                        inImage.onclick = iconClick;
                    }
                    
                    // Get the Hover Text from the Custom Label
                    Component.Apex.OutputText output = new Component.Apex.OutputText();
                    output.expressions.value = '{!$Label.' + fldIcon.Account_Icon__r.Hover_Text_Custom_Label__c + '}';
                    inImage.title = String.valueOf(output.value);
                    
                    if(string.isNotEmpty(fldIcon.Account_Icon__r.css__c)){
                        inImage.style = fldIcon.Account_Icon__r.css__c;
                    }
                    else{
                        inImage.styleClass = 'iconImage';
                    }
                    
                    outPanel.childComponents.add(inImage);
                }
            }
        }
        else{
            System.debug('smb_IconCMSController:sobject = null; objId = '+objId+', objAPIName = '+objAPIName);
        }
        return outPanel;
    }
}