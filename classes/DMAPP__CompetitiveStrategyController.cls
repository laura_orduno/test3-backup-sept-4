/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class CompetitiveStrategyController {
    @RemoteAction
    global static DMAPP.OpportunityCompetitorsRestResource.Response deleteCompetitiveStrategyCompetitor(String compId) {
        return null;
    }
    @RemoteAction
    global static DMAPP.CompetitiveStrategyRestResource.CompetitiveStrategy getCompetitiveStrategy(String id) {
        return null;
    }
    @RemoteAction
    global static List<DMAPP__DM_Opportunity_Competitor_Extra__c> getCompetitiveStrategyCompetitors(String oppId) {
        return null;
    }
    @RemoteAction
    global static String postCompetitiveStrategyCompetitor(String oppId, DMAPP.OpportunityCompetitorsRestResource.CompetitorChanges competitorChanges) {
        return null;
    }
    @RemoteAction
    global static DMAPP.CompetitiveStrategyRestResource.Response putCompetitiveStrategy(String id, DMAPP.CompetitiveStrategyRestResource.CompetitiveStrategyChanges changes) {
        return null;
    }
    @RemoteAction
    global static DMAPP.OpportunityCompetitorsRestResource.Response putCompetitiveStrategyCompetitor(String oppId, DMAPP.OpportunityCompetitorsRestResource.CompetitorChanges competitorChanges) {
        return null;
    }
}
