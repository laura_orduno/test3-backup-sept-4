@isTest
public class batch_VOCSurveySuppression_Test {
    private static final Integer numVocSurveysPerType = 10;
    private static final Integer surveyTypeStart = 30;
    private static final Integer surveyTypeEnd = 35;
    
    @isTest(SeeAllData=false) 
    public static void test1(){
        Test.startTest();
        
		createDependentRecords();
    	createVOCSurveys();        

        batch_VOCSurveySuppression bc = new batch_VOCSurveySuppression();        
        Database.executeBatch(bc);

        Test.stopTest();
    }
    
    @isTest(SeeAllData=false) 
    public static void test2(){
        Test.startTest();
        
		createDependentRecords();
    	createVOCSurveys();        

        scheduledBatch_VOCSurveySuppression sh1 = new scheduledBatch_VOCSurveySuppression();     
        String sch = '0  00 1 3 * ?';
        system.schedule('Test', sch, sh1);
        
//        batch_VOCSurveySuppression bc = new batch_VOCSurveySuppression();        
//        Database.executeBatch(bc);
        Test.stopTest();
    }
    
    private static void createVOCSurveys(){
        List<VOC_Survey__c> result = new List<VOC_Survey__c>();
        
        for(integer i=batch_VOCSurveySuppression_Test.surveyTypeStart;i<=batch_VOCSurveySuppression_Test.surveyTypeEnd;i++){
	        for(integer counter=0;counter<batch_VOCSurveySuppression_Test.numVocSurveysPerType;counter++){
			    result.add(createVOCSurvey(i, counter+1));
            }    
        }
        
        insert result;
    }    
    
    private static VOC_Survey__c createVOCSurvey(Integer surveyType, Integer counter){
    	Voc_Survey__c voc = new VOC_Survey__c(
            Account_No__c='test',
            Agent_ID__c='T876522',
            Business_Name__c='test',
            Case_Owner__c='test',
            Case_Subject__c='test',
            Customer_Email__c='test'+surveyType+'_'+counter+'@test.com',
            Customer_First_Name__c='first'+surveyType+'_'+counter,
            Customer_Last_Name__c='last'+surveyType+'_'+counter,
            Customer_Phone__c='4555555555',
            Custom_ID__c='testtest'+surveyType+'_'+counter,
            Interaction_Date__c=Date.today(),
            Location__c='test',
            manager_ids__c=userinfo.getuserid(),
            RCID__c='122323',
            Reference__c='test',
            Ticket__c='test',
            Survey_Type__c=surveyType,
            update_manager_emails__c=true,
            manager_callback__c=true
		);

//System.debug('createVOCSurvey: ' + voc);        
        return voc;
    }     
    
    private static void createDependentRecords(){
		List<VOC_Survey_Type_Suppression_Percentages__c> vocSuppressionPercentageList = new List<VOC_Survey_Type_Suppression_Percentages__c>();
        vocSuppressionPercentageList.add( new VOC_Survey_Type_Suppression_Percentages__c(Name='SurveyType31', Survey_Type__c= 31, Percentage__c=0.30) );
        insert vocSuppressionPercentageList;

        List<VOC_Survey_Type__c> vocTypes = new List<VOC_Survey_Type__c>();
        List<voc_survey_emails__c> surveyEmails = new List<voc_survey_emails__c>();
        map<integer,integer> surveyTypeIndexMap=new map<integer,integer>();

        Map<Integer, VOC_Survey_Type__c> vocSurveyTypeMap = new Map<Integer, VOC_Survey_Type__c>();

        for(integer i=batch_VOCSurveySuppression_Test.surveyTypeStart;i<=batch_VOCSurveySuppression_Test.surveyTypeEnd;i++){
            VOC_Survey_Type__c voctype = new VOC_Survey_Type__c(Requires_Fielded__c = false, Survey_URL__c = 'test', Type__c=i);
			vocSurveyTypeMap.put(i, voctype);

            vocTypes.add(voctype);

            surveyTypeIndexMap.put(integer.valueof(voctype.type__c),i);
            voc_survey_emails__c surveyEmail = new voc_survey_emails__c(name=''+i);
            surveyEmails.add(surveyEmail);
        }
        insert vocTypes;
        
        for(voc_survey_emails__c surveyEmail:surveyEmails){
            voc_survey_type__c surveyType = vocSurveyTypeMap.get(surveyTypeIndexMap.get(integer.valueof(surveyEmail.name)));
            surveyEmail.survey_type_id__c=surveyType.id;
        }
        insert surveyEmails;
    }
}