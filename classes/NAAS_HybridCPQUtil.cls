global with sharing class NAAS_HybridCPQUtil implements vlocity_cmt.VlocityOpenInterface {

// create Order before
global Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options)  {

    if (methodName.equals('createOcomOrder')) {
      createOCOMOrder(input,output,options);
    }

    return true;
}

// This method will create OCOM Specific Order
private void createOcomOrder(Map<String, Object> input, Map<String, Object> output, Map<String, Object> options){
        try{
            Order newOcomOrder = new Order();
            String RcidAcctId;
            String ServiceAcctId;
            String ContactId;
            // RCID Id
            RcidAcctId = (String)input.get('ParentAccId');
                  system.debug('inputmap::' + input);
                  system.debug('ServiceAccountId' + (String)input.get('ServiceAcctId') + RcidAcctId);
            // Service Adderess
            ServiceAcctId = (String)input.get('ServiceAcctId');
            // Contact Id
            
            ContactId = (String)input.get('ContactId');
            if(!String.isempty(RcidAcctId)){
              if(!String.isEmpty(ServiceAcctId)){
                  // Assumption
                  // Service Account must have RCID Account as parent
                  Account servAcct = [select parentId from Account where Id = :ServiceAcctId ];
                  if(RcidAcctId == null || RcidAcctId == '')
                                    RcidAcctId = servAcct.parentId;
                  //////////////////////////
                  //Assigning RCID Account AccountId
                  //////////////////////////
                  newOcomOrder.accountId = RcidAcctId;

                  List<SmbCare_Address__c> serAddress= [select Id,
                                                               FMS_Address_ID__c,
                                                               Address__c,
                                                               City__c,
                                                               Province__c,
                                                               Postal_Code__c,
                                                               Country__c
                                                               from Smbcare_address__c where Service_Account_Id__c =:ServiceAcctId];

                  if(serAddress.size()>0){
                      // Contact Id
                      newOcomOrder.Service_Address_Text__c = serAddress[0].Address__c + ' ' + serAddress[0].City__c + ' ' + serAddress[0].Province__c + ' ' + serAddress[0].Postal_Code__c + ' ' + serAddress[0].Country__c;
                      newOcomOrder.FMS_Address_ID__c=serAddress[0].FMS_Address_ID__c;
                      newOcomOrder.service_address__c=serAddress[0].Id;
                      newOcomOrder.EffectiveDate = Date.today();
                      newOcomOrder.Status = 'Not Submitted';

                      // service rep
                      newOcomOrder.Sales_Representative__c = UserInfo.getUserId();
                      // customer contact
                      newOcomOrder.CustomerAuthorizedById= ContactId;
                  }

              }else
              {
                // Throw CPQ exception
              }
            }

            insert newOcomOrder;
            // put to output
            Output.put('OrderId', newOcomOrder.Id);
        }

        catch (DMLException de){

        }

}

}