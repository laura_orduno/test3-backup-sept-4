@isTest
private class smb_CustomerProfileUtilityExtension_Test {
	
	private static testMethod void testCreateCustomerInCP() {
        //..... Test Custom Setting data......
        SMBCare_WebServices__c cs_endpoint = new SMBCare_WebServices__c(); 
        cs_endpoint.Name = 'Customer_EndPoint';
        cs_endpoint.Value__c = 'https://xmlgwy-pt1.telus.com:9030/at02/CMO/InformationMgmt/CustomerManagementService_v3_0_1_vs0';
        insert cs_endpoint;
		
        SMBCare_WebServices__c cs_username = new SMBCare_WebServices__c(); 
        cs_username.Name = 'username';
        cs_username.Value__c = 'APP_SFDC';
        insert cs_username;
        
        SMBCare_WebServices__c cs_password = new SMBCare_WebServices__c(); 
        cs_password.Name = 'password';
        cs_password.Value__c = 'soaorgid';
        insert cs_password;
                
    Account dummyAccount = new Account (
        	name = 'Test Account',
        	Operating_Name_DBA__c = 'Test Account',
        	billingStreet = '3777 Kingsway',
        	billingCity = 'Burnaby',
        	billingState = 'BC',
        	billingPostalCode = 'V7C 2K5',
        	billingCountry = 'CAN'
        );
                
        insert dummyAccount;
        
        smb_CreateCustomerTriggerCalloutHelper sc = new smb_CreateCustomerTriggerCalloutHelper();
        
        sc.prepareCustomerServiceCall(new list<account>{dummyAccount});
        
		smb_CreateCustomerTriggerCalloutHelper.performCustomerServiceCall(new list<id>{dummyAccount.Id});
                
        PageReference pageRef = Page.smb_CustomerProfileUtility;
        pageRef.getParameters().put('id',dummyAccount.id);
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.StandardController controller = new ApexPages.StandardController(dummyAccount);
        
        smb_CustomerProfileUtilityExtension extension = new smb_CustomerProfileUtilityExtension(controller);
        
        extension.createCustomerInCP();
        
        Account updatedAccount = [SELECT Id, RCID__c, CBUCID_Parent_Id__c, Error_Message__c
        						  FROM Account
        						  WHERE Id = :dummyAccount.Id];
        
        if(updatedAccount.Error_Message__c != null && updatedAccount.Error_Message__c.length() > 0){            
        }    
        else{
            System.assert(String.isNotBlank(updatedAccount.RCID__c));
            System.assert(String.isNotBlank(updatedAccount.CBUCID_Parent_Id__c));            
        }
	}
}