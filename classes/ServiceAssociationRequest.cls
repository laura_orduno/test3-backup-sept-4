/**
 * DTO that is used by integration service but it can be used by other purposes as well.
 */
public class ServiceAssociationRequest {
    public string uuid{get;set;}
    public string requester{get;set;}
    public string partner{get;set;}
    public string email{get;set;}
    public string can{get;set;}
    public string firstName{get;set;}
    public string lastName{get;set;}
    public string brand{get;set;}
    public ServiceAssociationRequest(string requester,string partner,string email,string can,string firstName,string lastName,string brand){
        this.requester=requester;
        this.partner=partner;
        this.email=email;
        this.can=can;
        this.firstName=firstName;
        this.lastName=lastName;
        this.brand=brand;
    }
}