public without sharing class ProductConfigurationSitesController extends AbstractProductConfigurationController  {
	public Id quoteId {get; private set;}
	private Id lineId;
	private Id groupId;
	private String returnURL;
	private String saveURL;
	
	public Boolean includeBundled {get; set;}
	public Boolean includeNested {get; set;}
	public String groupField {get; set;}
	public String totalFilterField {get; set;}
	public String totalFilterValue {get; set;}
	public String totalFormatPattern {get; set;}
	public String priceFormatPattern {get; set;}
	public String calculateButtonLabel {get; private set;}
	public String saveButtonLabel {get; private set;}
	public String cancelButtonLabel {get; private set;}
	public Boolean calculateButtonDisplayed {get; set;}
	public Boolean addOn {get; private set;}
	
	public ProductConfigurationSitesController() {
		super();
		
		ApexPages.currentPage().getParameters().get('retURL');
		ApexPages.currentPage().getParameters().get('retURL');
		
		returnURL = ApexPages.currentPage().getParameters().get('retURL');
		System.assert(returnURL != null, 'Parameter retURL is required');
		saveURL = ApexPages.currentPage().getParameters().get('saveURL');
		addOn = (ApexPages.currentPage().getParameters().get('addon') == '1'); 
		
		quoteId = (Id)ApexPages.currentPage().getParameters().get('qid');
		System.assert(quoteId != null, 'Missing quote ID');
		QuoteDAO qdao = new QuoteDAO();
		quote = qdao.loadByIds(new Set<Id>{quoteId})[0];
		super.init(quote);
		
		lineId = (Id)ApexPages.currentPage().getParameters().get('lid');
		groupId = (Id)ApexPages.currentPage().getParameters().get('gid');
		String pids = ApexPages.currentPage().getParameters().get('pids');
		System.assert((lineId != null) || (pids != null), 'Must supply either pids or lid parameter');
		
		if (lineId != null) {
			QuoteVO.LineItem item = quote.getItemById(lineId);
			if (item != null) {
				configureQuoteLineRow(item);
			}
		} else {
			Set<Id> productIds = new Set<Id>((List<Id>)pids.split(','));
			if (!quote.lineItems.isEmpty()) {
				// Look for configured products in this quote; Handles user using a Back button
				// TODO: Refactor this to bulkify.
				for (QuoteVO.LineItem item : quote.lineItems) {
					if (productIds.contains(item.productId) && item.isRootPackage()) {
						configureQuoteLineRow(item);
						productIds.remove(item.productId);
					}
				}
			}
			
			// Load remaining products
			if (!productIds.isEmpty()) {	
				configureProducts(ProductModel.loadWithOptionPrices(productIds, accountId, pricebookId, currencyCode));
				
				// Convert selections to quote lines rigt away so we don't have to do it all at once on save.
				ProductModel[] products = loadConfiguredProducts();
				
				for (ProductModel product : products) {
					product.updateQuote(quote, null, true);
				}
			}
		}
		
		includeNested = ApexPages.currentPage().getParameters().get('csin') == '1';
		includeBundled = ApexPages.currentPage().getParameters().get('csib') == '1';
		groupField = ApexPages.currentPage().getParameters().get('csgf');
		totalFilterField = ApexPages.currentPage().getParameters().get('cstff');
		totalFilterValue = ApexPages.currentPage().getParameters().get('cstfv');
		totalFormatPattern = ApexPages.currentPage().getParameters().get('cstfp');
		if (totalFormatPattern == null) {
			totalFormatPattern = '{0,number,currency}';
		}
		priceFormatPattern = ApexPages.currentPage().getParameters().get('cspfp');
		if (priceFormatPattern == null) {
			priceFormatPattern = '{0,number,currency}';
		}
		
		calculateButtonLabel = ApexPages.currentPage().getParameters().get('calculateBtnLbl');
		if (calculateButtonLabel == null) {
			calculateButtonLabel = 'Calculate';
		}
		calculateButtonDisplayed = (ApexPages.currentPage().getParameters().get('calculateBtnHidden') != '1');
		saveButtonLabel = ApexPages.currentPage().getParameters().get('saveBtnLbl');
		if (saveButtonLabel == null) {
			saveButtonLabel = 'Save';
		}
		cancelButtonLabel = ApexPages.currentPage().getParameters().get('cancelBtnLbl');
		if (cancelButtonLabel == null) {
			cancelButtonLabel = 'Cancel';
		}
		String prefix = Site.getPrefix();
		if (prefix == null) {
			prefix = '';
		}
		customConfigurationPagePrefix = prefix + '/';
		initCustomMessages();
	}
	
	private void initCustomMessages() {
		String countStr = ApexPages.currentPage().getParameters().get('cmc');
		if (!StringUtils.isBlank(countStr)) {
			for (Integer i=0;i<Integer.valueOf(countStr);i++) {
				String msg = ApexPages.currentPage().getParameters().get('cmt' + i);
				if (!StringUtils.isBlank(msg)) {
					String severity = ApexPages.currentPage().getParameters().get('cms' + i);
					ApexPages.addMessage(new ApexPages.Message(QuoteUtils.toSeverity(severity), msg));
				}
			}
		}
	}
	
	public PageReference onCalculate() {
		try {
			restoreConfiguredProducts();
			List<SBQQ__ProductOption__c> rootOptions = new List<SBQQ__ProductOption__c>();
			for (ProductOptionSelectionModel smodel : configuredOptionSelections) {
				for (ProductOptionSelectionModel.Selection sel : smodel.getSelections()) {
					if (sel.selected == true) {
						rootOptions.add(sel.editableOption);
					}
				}
			}
			ProductOptionPriceRuleEvaluator priceRuleEvaluator = new ProductOptionPriceRuleEvaluator(quoteId, rootOptions);
			priceRuleEvaluator.evaluate();
		} catch (Exception e) {
			ApexPages.addMessages(e);
		}
		return null;
	}
	
	public override PageReference executeSave() {
		return onSave();
	}
	
	public PageReference onCancel() {
		return new PageReference(returnURL);
	}
	
	public PageReference onSave() {
		ProductModel[] products = loadConfiguredProducts();
		if (!validateProductConfigurations(ProductModel.indexById(products))) {
			return null;
		}
		
		QuoteDAO qdao = new QuoteDAO();
		for (ProductModel pmodel : products) {
			pmodel.updateQuote(quote, null, false);
		}
		
		quote.renumber();
		QuoteCalculator qcalc = new QuoteCalculator(quote);
		qcalc.calculate();
		qdao.save(quote);
		
		//System.assert(false, 'T1:' + Limits.getScriptStatements());
		
		if (saveURL != null) {
			return new PageReference(saveURL);
		}
		return new PageReference(returnURL);
	}
}