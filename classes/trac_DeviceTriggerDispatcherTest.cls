@isTest
private class trac_DeviceTriggerDispatcherTest
{
	@isTest(seeAllData=false)
	static void receiveDevice()
	{

		List<Case> testCases = new List<Case>();		
		List<Device__c> testDevices = new List<Device__c>();

		Set<Id> ccIds = new Set<Id>();

		
		Case testCase = new Case(Subject = 'Testing reparting Device', Priority = '1');

		testCases.add(testCase);
		insert testCase;

		testDevices.add( new Device__c(
			Local_Parent_Id__c = String.valueOf(testCase.Id),
			Serial_Number__c = '18198'			
		));
		Test.startTest();
		insert testDevices;
		Test.stopTest();
	}
}