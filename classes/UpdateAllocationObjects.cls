/*
* Update allocations when expired flag is set to true. This is done to invoke the workflows. 
* Called from AllocationExpired trigger
* Author - Rucha Pradhan (Clear Task) - 07/26/2011
*/
public with sharing class UpdateAllocationObjects {

  @future 
  public static void updateAllocations(List<String> ids) {
    List<ADRA__Allocation__c> toBeUpdated = new  List<ADRA__Allocation__c>();
    
    for(String allocationId : ids){         
        toBeUpdated.add(new ADRA__Allocation__c(Id = allocationId, Expiration_Trigger__c = true));
    }
    
    if(!toBeUpdated.isEmpty()){
        System.debug('Size of list to be updated is: ' + toBeUpdated.size());
        update toBeUpdated;
    }   
  }
  
}