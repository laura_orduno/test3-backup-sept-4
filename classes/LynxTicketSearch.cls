public class LynxTicketSearch {

	@future(callout=true)
	public static void sendRequest(string ticketNum, string caseNumber, ID CaseId, String TicketEventId) {

		System.debug('CALLOUT Variables- LynxTicketSearch: ' + ticketNum + ' CaseNumber: ' + caseNumber + ' CaseID: ' + CaseId + ' TicketEventID: ' + TicketEventId);
		// Create the request envelope
		DOM.Document doc = new DOM.Document();
		//String endpoint = 'https://soa-mp-toll-st01.tsl.telus.com:443/SMO/ProblemMgmt/LynxTroubleTicketMgmtService_v1_0_vs0';
		//String endpoint = 'https://xmlgwy-pt1.telus.com:9030/st01/SMO/ProblemMgmt/TroubleTicketODSQueryService_v1_5_vs0';
		string endpoint = '';

		// TODO: Re-leverage custom setting for endpoint, no hard-coding
		if (!Test.isRunningTest()) {
			endpoint = SMBCare_WebServices__c.getInstance('TTODS_Endpoint_Search_Lynx').Value__c;
		} else {
			endpoint = 'https://xmlgwy.telus.com:9030/SMO/ProblemMgmt/LynxTroubleTicketMgmtService_v1_0_vs0';
		}
		//String endpoint = SMBCare_WebServices__c.getInstance('TTODS_Endpoint_Search_Lynx').Value__c;
		//String endpoint = 'https://xmlgwy.telus.com:9030/SMO/ProblemMgmt/LynxTroubleTicketMgmtService_v1_0_vs0';
		////////////
		String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
		string tro = 'http://www.telus.com/schema/servicestatus/troubleticket';
		// string ebon = 'http://ebonding.telus.com';
		// string asr = 'http://assurance.ebonding.telus.com';

		string logicalId = 'TELUS-TPS';

		dom.XmlNode envelope
				= doc.createRootElement('Envelope', soapNS, 'soapenv');
		envelope.setNamespace('tro', tro);

		dom.XmlNode header = envelope.addChildElement('Header', soapNS, null);

		dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
		dom.XmlNode QueryTicket = body.addChildElement('QueryTicketSearchCriteria', tro, 'tro');
		QueryTicket.addChildElement('requestSystemName', tro, null).
				addTextNode(logicalId);
		QueryTicket.addChildElement('requestUserId', tro, null).
				addTextNode('');
		dom.XmlNode TicketId = QueryTicket.addChildElement('telusTicketId', tro, 'tro');
		TicketId.addChildElement('value', tro, null).
				addTextNode(ticketNum);
		TicketId.addChildElement('operator', tro, null).
				addTextNode('=');
		dom.XmlNode includeOpenTickets = QueryTicket.addChildElement('includeOpenTickets', tro, 'tro');
		includeOpenTickets.addChildElement('toInclude', tro, null).
				addTextNode('true');
		dom.XmlNode includeClosedTickets = QueryTicket.addChildElement('includeClosedTickets', tro, 'tro');
		includeClosedTickets.addChildElement('toInclude', tro, null).
				addTextNode('true');
		QueryTicket.addChildElement('upToXLatestModified', tro, null).
				addTextNode('200');
		dom.XmlNode includeActivities = QueryTicket.addChildElement('includeActivities', tro, 'tro');
		includeActivities.addChildElement('toInclude', tro, null).
				addTextNode('True');
		dom.XmlNode activityTypes = includeActivities.addChildElement('activityTypes', tro, 'tro');
		activityTypes.addChildElement('value', tro, null).
				addTextNode('Route');
		activityTypes.addChildElement('value', tro, null).
				addTextNode('Notes');
		activityTypes.addChildElement('value', tro, null).
				addTextNode('Call - Inbound');
		activityTypes.addChildElement('value', tro, null).
				addTextNode('Notify Customer');
		activityTypes.addChildElement('value', tro, null).
				addTextNode('Resolution');
		activityTypes.addChildElement('value', tro, null).
				addTextNode('Test Remarks');
		activityTypes.addChildElement('operator', tro, null).
				addTextNode('IN');
		dom.XmlNode activityStates = includeActivities.addChildElement('activityStates', tro, 'tro');
		activityStates.addChildElement('value', tro, null).
				addTextNode('Cancelled');
		activityStates.addChildElement('operator', tro, null).
				addTextNode('NOT IN');
		includeActivities.addChildElement('upToXLatestModified', tro, null).
				addTextNode('200');
		System.debug(doc.toXmlString());

		// TODO: add exception handling

		// Send the request
		HttpRequest req = new HttpRequest();
		req.setMethod('POST');
		req.setEndpoint(endpoint);
		// TODO: specify a timeout of 2 minutes
		// I.E. req.setTimeout(milliseconds);
		String creds = '';

		if (!Test.isRunningTest()) {
			SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username');
			SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');
			if (String.isNotBlank(wsUsername.Value__c) && String.isNotBlank(wsPassword.Value__c))
				creds = wsUsername.Value__c + ':' + wsPassword.Value__c;
		} else {
			creds = 'APP_SFDC:soaorgid';
		}

		String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(creds));

		//Blob headerValue = Blob.valueOf(encodedusernameandpassword);
		String authorizationHeader = 'BASIC ' + encodedusernameandpassword;
		// EncodingUtil.base64Encode(headerValue);
		req.setHeader('Authorization', authorizationHeader);
		req.setHeader('Content-Type', 'text/xml');

		req.setBodyDocument(doc);

		// TODO: Determine the level of detail webservice is returning in regards to errors.

		Http http = new Http();
		HttpResponse res = new HttpResponse();
		if (!Test.isRunningTest()) {
			res = http.send(req);
		} else {
			if (TicketEventId == null || TicketEventId == '') {
				res = ENTPUtils.mockWebserviceResponse(ENTPUtils.LYNX_XMLCREATE_RESPONSE);
			} else {
				if (ticketNum != '' || ticketNum != null) {
					res = ENTPUtils.mockWebserviceResponse(ENTPUtils.LYNX_TICKET_SEARCH_XML_RESPONSE);
				} else {
					res = ENTPUtils.mockWebserviceResponse(ENTPUtils.LYNX_XMLCREATE_RESPONSE);
				}
			}
		}

		System.debug('GetBody line 110:' + res.getBody());
		// System.assertEquals(200, res.getStatusCode());

		dom.Document resDoc = res.getBodyDocument();
		XmlStreamReader reader = res.getXmlStreamReader();

		// Read through the XML
		String TELUSTicketId = '';
		while (reader.hasNext()) {
			System.debug('Event Type:' + reader.getEventType());
			if (reader.getEventType() == XmlTag.START_ELEMENT) {
				System.debug(reader.getLocalName());
				if ('TELUSTicketId' == (reader.getLocalName())) {
					boolean isSafeToGetNextXmlElement = true;
					while (isSafeToGetNextXmlElement) {
						if (reader.getEventType() == XmlTag.END_ELEMENT) {
							break;
						} else if (reader.getEventType() == XmlTag.CHARACTERS) {
							TELUSTicketId = reader.getText();
							System.debug('TELUSTicketId :' + TELUSTicketId);
						}
						// Always use hasNext() before calling next() to confirm
						// that we have not reached the end of the stream

						if (reader.hasNext()) {

							reader.next();

						} else {

							isSafeToGetNextXmlElement = false;
							break;
						}
					}

				}
			}
			reader.next();
		}
		if (TELUSTicketId == null || TELUSTicketId == '') {
			if (TicketEventId == null || TicketEventId == '') {
				try {
					Ticket_Event__c NewEvent = new Ticket_Event__c(
							Case_Number__c = caseNumber,
							Event_Type__c = 'New ticket',
							Status__c = 'Verify',
							Failure_Reason__c = 'Ticket number not verified',
							Lynx_Ticket_Number__c = ticketNum,
							Case_Id__c = CaseId
					);
					System.debug('LynxTicketSearch->NewEvent, CaseNumber(' + caseNumber + ')');

					insert NewEvent;

				}

				catch (DmlException e) {
					System.debug('LynxTicketSearch->TicketEvent, exception: ' + e.getMessage());
				}
			} else {
				try {
					Ticket_Event__c NewEvent = [Select Event_Type__c, Status__c, Failure_Reason__c from Ticket_Event__c where Id = :TicketEventId];
					NewEvent.Event_Type__c = 'Verify ticket';
					NewEvent.Status__c = 'New';
					NewEvent.Failure_Reason__c = 'Failed to validate Lynx Ticket Number';
					Update NewEvent;
				} catch (DmlException e) {
					System.debug('LynxTicketSearch->TicketEventUpdateNotValidated, exception: ' + e.getMessage());
				}
			}

		} else {
			try {
				Ticket_Event__c NewEvent = [Select Event_Type__c, Status__c, Failure_Reason__c from Ticket_Event__c where Id = :TicketEventId];
				NewEvent.Event_Type__c = 'New ticket';
				NewEvent.Status__c = 'Complete';
				NewEvent.Failure_Reason__c = '';
				Update NewEvent;
			} catch (DmlException e) {
				System.debug('LynxTicketSearch->TicketEventUpdateValidated, exception: ' + e.getMessage());
			}
			Case tmpCase = [
					SELECT Id, CaseNumber, Lynx_Ticket_Number__c, Status
					FROM Case
					WHERE CaseNumber = :caseNumber
			];
			if (tmpCase != null && ticketNum != null) {
				tmpCase.Lynx_Ticket_Number__c = ticketNum;
				System.debug('Collab tmpCase.NotifyCollaboratorString__c: ' + tmpCase.NotifyCollaboratorString__c);

				/** DANE REFACTORING
			   if (tmpCase.NotifyCollaboratorString__c != null && tmpCase.NotifyCollaboratorString__c.length() > 0) {
					List<String> collabEmails = tmpCase.NotifyCollaboratorString__c.split(';', 0);
					if (collabEmails.size() > 0) {
					   ENTP_Case_Collaborator_Settings__c collabSettings = ENTP_Case_Collaborator_Settings__c.getOrgDefaults();
						System.debug('Collab collabSettings: ' + collabSettings);
						//String templateName = collabSettings.New_Case_Template_Name__c;
						//List<EmailTemplate> templates = [select id from EmailTemplate where DeveloperName = :templateName LIMIT 1];
						//System.debug('Collab templates: ' + templates);
						Id emailTemplateId = collabSettings.New_Case_Template_Id__c;
						if (emailTemplateId != null) {
							//Id emailTemplateId = templates[0].Id;
							//Id emailTemplateId = '00X4000000163uYEAQ';
							Id fromEmailId = null;
							List<OrgWideEmailAddress> fromEmails = [select Id from OrgWideEmailAddress where DisplayName = 'Do Not Reply ENTP'];
							if (fromEmails.size() > 0) {
								fromEmailId = fromEmails[0].Id;
							}
							try {
								List<Messaging.SendEmailResult> mailResults = ENTPUtils.sendTemplatedCaseEmails(collabEmails, fromEmailId, emailTemplateId, tmpCase.Id);
							} catch (Exception e) {
								System.debug('Collab caught ENTPUtls.sendTemplatedCaseEmails() exception: ' + e);
							}
						}
					}
				}**/

			} System.debug('LynxTickNumber, CaseNumber(' + tmpCase.caseNumber + '), TicketNumber(' + ticketNum + ')');
			update tmpCase;
		}
		envelope = resDoc.getRootElement();
		//  String troubleTick = 'http://www.telus.com/schema/servicestatus/troubleticket';
		//String wsa = 'http://schemas.xmlsoap.org/soap/envelope/';
		//Dom.XMLNode TroubleTicket = resDoc.getRootElement();
		// dom.XmlNode header1 = envelope.getChildElement('Header', soapNS);
		// String TELUSTicketId = TroubleTicket.getChildElement('TELUSTicketId',null).getText();
		//System.debug('TELUSTicketId: ' + TELUSTicketId);
		System.debug(resDoc.toXmlString());
		System.debug('Envelope=:' + envelope);
		//  System.debug('TroubleTicket=:' + TroubleTicket);
		//System.debug(header1);
		//System.debug(testTime);

		//  return null;
	}

}