global class OC_SalesAgentOnBeHalfDealerController {
    public String siteBaseUrl {get;set;}
    public String cacheKey {get;set;}
    public String roleId {get;set;}
   // public String salesRepJson {get;set;}
   // public String outletJson {get;set;}
    public String respStr {get;set;}
    public List<ChannelOrgOutletsJSONExplicit> channelOrgOutletsJsonObjList {get;set;}
    public List<ChannelOrgSalesRepsJSONExplicit> channelOrgSalesRepJsonObjList {get;set;}
    public List<String> channelOrgOutletsStrList {get;set;}
    public List<String> channelOrgSalesRepStrList {get;set;}
    public Map<String,String> keyValMap {get;set;}
    public Map<String,String> channelOrgOutletsStrMap {get;set;}
    public Map<String,String> channelOrgSalesRepStrMap {get;set;}
    public Set<String> keySet1 {get;set;}
    public Set<String> keySet2 {get;set;}
    /*public List<MyData> channelOrgOutletsStrData {get;set;}
    public List<MyData> channelOrgSalesRepStrData {get;set;}
    public class MyData{
        public String key{get;set;}
        public String value{get;set;}
    }*/
   global OC_SalesAgentOnBeHalfDealerController(){
       System.debug('start');
       
       String startUrl = System.currentPageReference().getParameters().get('startURL');
       System.debug('startUrl='+startUrl);
        System.debug(System.currentPageReference().getParameters().get('key'));
       System.debug(ApexPages.currentPage().getParameters());
       System.debug(ApexPages.currentPage().getParameters());
         String keyStr = apexpages.currentpage().getparameters().get('key');
         System.debug('keyStr='+keyStr);
    }
    
    global PageReference forwardToLandingPage() {
        cacheKey = System.currentPageReference().getParameters().get('key');
        System.debug('cacheKey=' + cacheKey);
        //roleId = System.currentPageReference().getParameters().get('roleId');
        roleId = UserInfo.getUserRoleId(); //testing
        System.debug('roleId=' + roleId);
        siteBaseUrl = Site.getBaseUrl();                
        System.debug('baseUrl=' + siteBaseUrl);
        //temp
        //cacheKey=null;
        if(String.isNotBlank(cacheKey)) { 
            cacheKey = cacheKey.trim();
            ChannelPortalCacheMgmtSrvReqRes.GetCacheListResponseType resp = ChannelPortalCacheMgmtWSCallout.getCacheList(cacheKey);
            
            ChannelPortalCacheMgmtSrvReqRes.MapItem[] customCacheList = resp.customCacheList;
            
            keyValMap = new Map<String,String>();
            for(ChannelPortalCacheMgmtSrvReqRes.MapItem mapItemObj:customCacheList){
                if(String.isNotBlank(mapItemObj.key) && 'channelOrgSalesReps_JSON'.equalsIgnoreCase(mapItemObj.key)){
                    continue;
                }
                if(String.isNotBlank(mapItemObj.key) && 'channelOrgOutlets_JSON'.equalsIgnoreCase(mapItemObj.key)){
                    continue;
                }
                keyValMap.put(mapItemObj.key, mapItemObj.value); 
            }
            
            respStr = JSON.serialize(resp);
            String cmsKey = 'local.orderingCache.KEY' + keyValMap.get('loginUserID');
            System.debug('PartnerCommunitiesLoginController cmsKey= '+ cmsKey);
            Object ob1 = Cache.Org.get(cmsKey);
            if(ob1==null){
                cmsKey = 'local.orderingCache.KEY' + keyValMap.get('portalUserID');
                ob1 = Cache.Org.get(cmsKey);
                if(ob1==null){
                    String federationId = [SELECT FederationIdentifier FROM User WHERE Id = :UserInfo.getUserId()].FederationIdentifier;
                    String cmsAltKey = 'local.orderingCache.KEY' + federationId;
                    ob1 = Cache.Org.get(cmsAltKey);
                }
            }
            if(ob1==null){
                return null;
            }
            System.debug('DATA FROM LoginController '+JSON.serialize(ob1));
            ChannelPortalCacheMgmtData dataObj = (ChannelPortalCacheMgmtData)Cache.Org.get(cmsKey);
            channelOrgSalesRepJsonObjList = dataObj.channelOrgSalesRepJsonObjList;
            channelOrgSalesRepStrList = new List<String>();
            channelOrgOutletsStrMap = new Map<String,String>();
            channelOrgSalesRepStrMap = new Map<String,String>();
            if(channelOrgSalesRepJsonObjList != null){
                for(ChannelOrgSalesRepsJSONExplicit ob:channelOrgSalesRepJsonObjList){
                    channelOrgSalesRepStrList.add('firstName:'+ob.firstName);
                    channelOrgSalesRepStrList.add('lastName:'+ob.lastName);
                    channelOrgSalesRepStrList.add('salesRepPin:'+ob.salesRepPin);
                    channelOrgSalesRepStrList.add('salesRepCategoryKeys:'+ob.salesRepCategoryKeys);
                    channelOrgSalesRepStrMap.put('firstName',ob.firstName);
                    channelOrgSalesRepStrMap.put('lastName',ob.lastName);
                    channelOrgSalesRepStrMap.put('salesRepPin',ob.salesRepPin);
                    //channelOrgSalesRepStrMap.put('salesRepCategoryKeys',ob.salesRepCategoryKeys);                    
                }
                keySet1 = channelOrgSalesRepStrMap.keySet();
            }
            
            channelOrgOutletsStrList = new List<String>();
            channelOrgOutletsJsonObjList = dataObj.channelOrgOutletsJsonObjList;
            if(channelOrgOutletsJsonObjList != null){
                for(ChannelOrgOutletsJSONExplicit ob:channelOrgOutletsJsonObjList){
                    channelOrgOutletsStrList.add('currentChannelOutletID:'+ob.currentChannelOutletID);
                    channelOrgOutletsStrList.add('currentChannelOutletDescription:'+ob.currentChannelOutletDescription);
                    channelOrgOutletsStrList.add('currentOutletCategoryKeys:'+ob.currentOutletCategoryKeys);
                    channelOrgOutletsStrMap.put('currentChannelOutletID',ob.currentChannelOutletID);
                    channelOrgOutletsStrMap.put('currentChannelOutletDescription',ob.currentChannelOutletDescription);
                    //channelOrgOutletsStrMap.put('currentOutletCategoryKeys',ob.currentOutletCategoryKeys);
                }
                if(null != channelOrgOutletsStrMap){
                    keySet2 = channelOrgOutletsStrMap.keySet();
                }
            }
        }
 
       // updatePartnerUser();
 
        
        String landingPageUrl = Site.getPathPrefix() + '/home/home.jsp';
        return Network.forwardToAuthPage(landingPageUrl);
        //return null;
    }
}