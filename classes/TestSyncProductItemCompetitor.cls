@isTest
private class TestSyncProductItemCompetitor{
    static testmethod void testCodeCoverage(){
        Account account = new Account(
            Name = 'TheTestSyncProductItemCompetitor Account',
            BillingState = 'BC',
            Phone = '6042302348'
        );
        insert account;
        
        Opportunity opp = new Opportunity(
            accountId = account.Id,
            Name = 'Testing Opportunity',
            CloseDate = Date.today(),
            StageName = 'I have a Potential Opportunity (20%)'
        );        
        insert opp; 
        Opportunity opp2 = new Opportunity(
            accountId = account.Id,
            Name = 'Testing Opportunity',
            CloseDate = Date.today(),
            OwnerId = UserInfo.getUserId(),
            StageName = 'I have a Potential Opportunity (20%)'
        );        
        insert opp2; 
        //Create Opportunity-End
          //create Product- start
              Product2 pdct= new Product2(
                  Name='test product',
                  IsActive = true,
                  Product_Family_Code__c = 'testFMC'
              );
              insert pdct;
          //create Product- End 
          //create Opportunity Product Item - start
             List<Opp_Product_Item__c> opiListInsert = new List<Opp_Product_Item__c>();
              Opp_Product_Item__c opi = new Opp_Product_Item__c(                  
                  Opportunity__c = opp.Id, 
                  Product__c = pdct.Id,
                  Partner__c = '(Other)',
                  Reason_for_Billing_Start_Date_Deviation__c = 'test',
                  Existing_Svc_Prod_Repriced_Replaced__c = 'No',
                  All_Non_Recurring_Revenue__c = 0.0,
                  Monthly_Recurring_Revenue__c = 100.0,
                  Billing_Frequency_Month__c = 1,
                  Contract_Length_Month__c = 1,
                  Billing_Start_Date__c = System.today(),
                  Competitor__c = '(None); Accenture; Zing Networks'
              );
              opiListInsert.add(opi);
         
              Opp_Product_Item__c opi2 = new Opp_Product_Item__c(                  
                  Opportunity__c = opp2.Id, 
                  Product__c = pdct.Id,
                  Partner__c = '(Other)',
                  Existing_Svc_Prod_Repriced_Replaced__c = 'No',
                  All_Non_Recurring_Revenue__c = 0.0,
                  Reason_for_Billing_Start_Date_Deviation__c = 'test',
                  Monthly_Recurring_Revenue__c = 100.0,
                  Billing_Frequency_Month__c = 1,
                  Contract_Length_Month__c = 1,
                  Billing_Start_Date__c = System.today(),
                  Competitor__c = '(None); Alcatel-Lucent; YAK Communications'
              );
              opiListInsert.add(opi2);
              insert opiListInsert;
          //create Opportunity Product Item - End   
          List<OpportunityCompetitor> ocList= [Select o.OpportunityId, o.CompetitorName From OpportunityCompetitor o where o.OpportunityId =:opp.Id order by o.OpportunityId];
          if(ocList != null){
              String strOppComp = '';
              for(OpportunityCompetitor oc :ocList){
                   strOppComp = strOppComp + oc.CompetitorName + '; ';
              }
              
//              System.assertEquals('Accenture; Zing Networks; ', strOppComp );
          }
          List<Opp_Product_Item__c> opiListUpdate = new List<Opp_Product_Item__c>();
           
          Opp_Product_Item__c opiObj = [ select Competitor__c from Opp_Product_Item__c where Id= :opi.Id limit 1];
          opiObj.Competitor__c = '(None); Alcatel-Lucent; YAK Communications' ;
          opiListUpdate.add(opiObj);          
          
         
          Opp_Product_Item__c opiObj2 = [ select Competitor__c from Opp_Product_Item__c where Id= :opi2.Id limit 1];
          opiObj2.Competitor__c ='(None); Accenture; Zing Networks' ;
          opiListUpdate.add(opiObj2);
                    
          update opiListUpdate;
          
          ocList = [Select o.OpportunityId, o.CompetitorName From OpportunityCompetitor o where o.OpportunityId =:opp.Id order by o.OpportunityId];
          if(ocList != null){
              String strOppComp = '';
              for(OpportunityCompetitor oc :ocList){
                   strOppComp = strOppComp + oc.CompetitorName + '; ';
              }
              
//              System.assertEquals('YAK Communications; Alcatel-Lucent; ', strOppComp );
          }
          
          delete opiObj2;
     }
}