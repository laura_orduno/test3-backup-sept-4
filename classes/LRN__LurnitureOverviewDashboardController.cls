/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class LurnitureOverviewDashboardController {
    @RemoteAction
    global static void assignKPIVideos(String dueDate, String commaSeperatedVideoIds, String assignVideoComment, String userId) {

    }
    @RemoteAction
    global static List<LRN.LurnitureOverviewDashboardController.PitchingOverviewWrapper> fetchPitchingOverviewData(String duration) {
        return null;
    }
    @RemoteAction
    global static List<LRN.LurnitureOverviewDashboardController.UserView> getUsersViewedVideo(String duration, String videoId) {
        return null;
    }
    @RemoteAction
    global static LRN.LurniturePitchingRoomService.VideoScoreCard getVideoAverageScoreCard(String duration, String videoId) {
        return null;
    }
    @RemoteAction
    global static List<LRN.LurnitureOverviewDashboardController.CertifiedVideoWrapper> loadCertifiedVideos(String duration) {
        return null;
    }
    @RemoteAction
    global static LRN.LurnitureDashboardWrapper loadDashboardWrapper(String duration) {
        return null;
    }
    global static LRN.LurnitureDashboardWrapper loadDashboardWrapperForCustomDateRange(String startDateString, String endDateString) {
        return null;
    }
    @RemoteAction
    global static LRN.LurnitureDashboardWrapper loadDashboardWrapperForCustomDateRange(String startDateString, String endDateString, String isCustom) {
        return null;
    }
    @RemoteAction
    global static List<LRN.LurnitureOverviewDashboardController.OpportunityWrapperForPerMonthClosing> loadMonthlyClosedOpportunityReport(String duration, String userId) {
        return null;
    }
    @RemoteAction
    global static List<LRN.LurnitureOverviewDashboardController.OpportunityWrapper> loadOpportunitiesClosedWithVideo(String duration) {
        return null;
    }
    @RemoteAction
    global static List<LRN.LurnitureOverviewDashboardController.OpportunityWrapper> loadOpportunitiesClosedWithoutVideo(String duration) {
        return null;
    }
    @RemoteAction
    global static List<LRN.LurnitureOverviewDashboardController.OpportunityWrapper> loadOpportunitiesForClosingDays(String duration, String userId) {
        return null;
    }
    @RemoteAction
    global static List<LRN.LurnitureOverviewDashboardController.OpportunityWrapper> loadOpportunitiesForDealSize(String duration, String userId) {
        return null;
    }
    @RemoteAction
    global static List<LRN.LurnitureOverviewDashboardController.PitchingVideoWrapper> loadPitchingVideos(String duration, String userId) {
        return null;
    }
    @RemoteAction
    global static List<LRN.LurnitureOverviewDashboardController.PublishedVideoWrapper> loadPublishedVideos(String duration, String userId) {
        return null;
    }
    @RemoteAction
    global static List<LRN.LurnitureOverviewDashboardController.OpportunityWrapper> loadRelaedOpportunities(String duration) {
        return null;
    }
    @RemoteAction
    global static List<LRN.LurnitureOverviewDashboardController.SubmittedVideoWrapper> loadSubmittedVideos(String duration) {
        return null;
    }
    @RemoteAction
    global static List<LRN.LurnitureOverviewDashboardController.OpportunityDetailWrapper> loadTeamRelatedData(String duration, String userId) {
        return null;
    }
    @RemoteAction
    global static List<LRN.LurnitureOverviewDashboardController.AssignedVideoWrapper> loadUserAssignedVideos(String duration, String userId) {
        return null;
    }
    @RemoteAction
    global static List<LRN.LurnitureOverviewDashboardController.UserClosingDays> loadUserClosingDays(String duration, String userId) {
        return null;
    }
    @RemoteAction
    global static List<LRN.LurnitureOverviewDashboardController.VideosWithOpportunityWrpper> loadUserPublishedVideosReatedToOpportunity(String duration, String userId) {
        return null;
    }
    @RemoteAction
    global static List<LRN.LurnitureOverviewDashboardController.RecommendedVideoWrapper> loadUserRecommendedVideos(String duration, String userId) {
        return null;
    }
    @RemoteAction
    global static List<LRN.LurnitureOverviewDashboardController.VideosWithOpportunityWrpper> loadUserViewedVideosReatedToOpportunity(String duration, String userId) {
        return null;
    }
    @RemoteAction
    global static List<LRN.LurnitureOverviewDashboardController.VideoViewWrapper> loadVideoViewed(String duration, String userId) {
        return null;
    }
    global static void loadViewersByVideoId(String duration, String videoId) {

    }
    @RemoteAction
    global static List<LRN.LurnitureOverviewDashboardController.PublishedVideoWrapper> loadViewsOnPublishedVideos(String duration, String userId) {
        return null;
    }
global class AssignedVideoWrapper {
}
global class CertifiedVideoWrapper {
}
global class OpportunityDetailWrapper {
}
global class OpportunityWrapper {
}
global class OpportunityWrapperForPerMonthClosing {
    global OpportunityWrapperForPerMonthClosing() {

    }
}
global class PitchingOverviewWrapper {
}
global class PitchingVideoWrapper {
}
global class PublishedVideoWrapper {
}
global class RecommendedVideoWrapper {
}
global class SubmittedVideoWrapper {
}
global class UserClosingDays {
}
global class UserView {
    global UserView() {

    }
}
global class VideoViewWrapper {
}
global class VideosWithOpportunityWrpper {
}
}
