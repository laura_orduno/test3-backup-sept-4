/**
 * Value object that wraps Product2 and related records.
 * 
 * @author Max Rudman
 * @since 2/11/2011
 */
public class ProductVO {
	public Product2 record {get; private set;}
	public List<SBQQ__BlockPrice__c> blockPrices {get; private set;}
	public List<Option> options {get; private set;}
	public List<Feature> features {get; private set;}
	public List<Constraint> constraints {get; private set;}
	public List<SBQQ__Cost__c> costs {get; private set;}
	public List<SBQQ__RelatedContent__c> relatedContent {get; set;}
	public List<SBQQ__ConfigurationRule__c> configurationRules {get; set;}
	
	private Map<Id,Option> optionsById = new Map<Id,Option>();
	private Map<Id,Feature> featuresById = new Map<Id,Feature>();
	
	/**
	 * Default constructor.
	 */
	public ProductVO(Product2 record) {
		this.record = record;
		initBlockPrices();
		initCosts();
		initFeatures();
		initOptions();
		initConstraints();
	}
	
	public String getName() {
		return record.Name;
	}
	
	public Option getOptionById(Id optionId) {
		return optionsById.get(optionId);
	}
	
	public Feature getFeatureById(Id featureId) {
		return featuresById.get(featureId);
	}
	
	/**
	 * Returns options that are not assigned to a feature.
	 */
	public List<Option> getOptionsWithoutFeature() {
		List<Option> result = new List<Option>();
		for (Option option : options) {
			if (option.feature == null) {
				result.add(option);
			}
		}
		return result;
	}
	
	public Boolean isConfigurationRequired() {
		return (record.SBQQ__ConfigurationType__c != null) && record.SBQQ__ConfigurationType__c.equalsIgnoreCase('required');
	}
	
	public Boolean isConfigurationAllowed() {
		return (record.SBQQ__ConfigurationType__c != null) && record.SBQQ__ConfigurationType__c.equalsIgnoreCase('allowed');
	}
	
	/**
     * Tests if this line item is a dynamically-priced subscription
     */
    public Boolean isDynamicSubscription() {
        return (record.SBQQ__SubscriptionPricing__c == 'Percent of Total');
    }
    
    /**
     * Tests if this line item is a subscription.
     */
    public Boolean isSubscription() {
        return (record.SBQQ__SubscriptionPricing__c != null);
    }
	
	/**
	 * Initializes block prices collection.
	 */
	private void initBlockPrices() {
		try {
			blockPrices = record.SBQQ__BlockPrices__r;
		} catch (SObjectException soe) {
			// Assume VO was loaded without block prices.
			blockPrices = null;
		}
	}
	
	/**
	 * Initializes costs collection.
	 */
	private void initCosts() {
		try {
			costs = record.SBQQ__Costs__r;
		} catch (SObjectException soe) {
			// Assume VO was loaded without costs;
			costs = null;
		}
	}
	
	/**
	 * Initializes features collection.
	 */
	private void initFeatures() {
		try {
			features = new List<Feature>();
			for (SBQQ__ProductFeature__c rec : record.SBQQ__Features__r) {
				// Copy currency code from product (which set in DAO to value passed in from the quote)
				// so the prices render in correct currency
				QuoteUtils.setCurrencyCode(rec, QuoteUtils.getCurrencyCode(record));
				Feature feature = new Feature(this, rec);
				features.add(feature);
				featuresById.put(feature.record.Id, feature);
			}
		} catch (SObjectException soe) {
			// Assume VO was loaded without features.
			features = null;
		}
	}
	
	/**
	 * Initializes options collection.
	 */
	private void initOptions() {
		try {
			options = new List<Option>();
			for (SBQQ__ProductOption__c rec : record.SBQQ__Options__r) {
				// Copy currency code from product (which set in DAO to value passed in from the quote)
				// so the prices render in correct currency
				QuoteUtils.setCurrencyCode(rec, QuoteUtils.getCurrencyCode(record));
				Feature feature = featuresById.get(rec.SBQQ__Feature__c);
				Option option = new Option(this, rec, feature);
				options.add(option);
				optionsById.put(option.record.Id, option);
				if (feature != null) {
					feature.options.add(option);
				}
			}
		} catch (SObjectException soe) {
			// Assume VO was loaded without options.
			options = null;
		}
	}
	
	/**
	 * Initializes constraints collection.
	 */
	private void initConstraints() {
		try {
			constraints = new List<Constraint>();
			for (SBQQ__OptionConstraint__c rec : record.SBQQ__Option_Constraints__r) {
				Constraint constr = new Constraint(this, rec);
				constraints.add(constr);
				Option constrainedOption = optionsById.get(rec.SBQQ__ConstrainedOption__c);
				Option constrainingOption = optionsById.get(rec.SBQQ__ConstrainingOption__c);
				if ((constrainedOption != null) && (constrainingOption != null)) {
					if (constr.isExclusion()) {
						constrainedOption.addExclusion(constrainingOption);
						constrainingOption.addExclusion(constrainedOption);
					} else if (constr.isDependency()) {
						constrainedOption.addDependency(constrainingOption);
					}
				}
			}
		} catch (SObjectException soe) {
			// Assume VO was loaded without constraints.
			constraints = null;
		}
	}
	
	/**
	 * Collects IDs of optional products.
	 */
	public Set<Id> getOptionProductIds() {
		Set<Id> ids = new Set<Id>();
		for (Option option : options) {
			if (option.record.SBQQ__OptionalSKU__c != null) {
				ids.add(option.record.SBQQ__OptionalSKU__c);
			}
		}
		return ids;
	}
	
	public Set<Id> getCoreProductIds() {
		Set<Id> ids = new Set<Id>();
		if (record.Core_Product_Ids__c != null) {
			for (String pid : record.Core_Product_Ids__c.split(',')) {
				pid = pid.trim();
				ids.add((Id)pid);
			}
		}
		return ids;
	}
	
	public Boolean isBlockPriced() {
		return record.SBQQ__PricingMethod__c == 'Block';
	}
	
	public Boolean isConfigurationEventAlways() {
		return ((record.SBQQ__ConfigurationEvent__c == null) || record.SBQQ__ConfigurationEvent__c.equalsIgnoreCase('Always'));
	}
	
	public Boolean isConfigurationEventAdd() {
		return ((record.SBQQ__ConfigurationEvent__c != null) && record.SBQQ__ConfigurationEvent__c.equalsIgnoreCase('Add'));
	}
	
	public Boolean isConfigurationEventEdit() {
		return ((record.SBQQ__ConfigurationEvent__c != null) && record.SBQQ__ConfigurationEvent__c.equalsIgnoreCase('Edit'));
	}
	
	public Boolean hasConfigurationFields() {
		return !StringUtils.isBlank(record.SBQQ__ConfigurationFields__c);
	}
	
	public Decimal getCost(String currencyCode) {
		for (SBQQ__Cost__c cost : record.SBQQ__Costs__r) {
			if (!UserInfo.isMultiCurrencyOrganization()) {
				return cost.SBQQ__UnitCost__c;
			} else if (cost.get('CurrencyIsoCode') == currencyCode) {
				return cost.SBQQ__UnitCost__c;
			}
		}
		return null;
	}
	
	public Decimal getDefaultListPrice() {
		if (record.PricebookEntries.size() == 1) {
			return record.PricebookEntries[0].UnitPrice;
		}
		return null;
	}
	
	public Decimal getListPrice(Id pricebookId, String currencyCode) {
		for (PricebookEntry entry : record.PricebookEntries) {
			if (entry.Pricebook2Id == pricebookId) {
				if (!UserInfo.isMultiCurrencyOrganization()) {
					return entry.UnitPrice;
				} else if (entry.get('CurrencyIsoCode') == currencyCode) {
					return entry.UnitPrice;
				}
			}
		}
		return null;
	}
	
	/**
	 * Value object that wraps ProductOption__c record.
	 */
	public class Option {
		// Public properties
		public SBQQ__ProductOption__c record {get; private set;}
		public ProductVO parent {get; private set;}
		public ProductVO product {get; set;}
		public Feature feature {get; private set;}
		
		// Private state
		private Set<Id> dependencyIds; // IDs of options this option depends on
		private Set<Id> exclusionIds; // IDs of options this option is exclusive with.
		
		/**
		 * Default constructor
		 */
		public Option(ProductVO parent, SBQQ__ProductOption__c record, Feature feature) {
			this.parent = parent;
			this.record = record;
			this.feature = feature;
			dependencyIds = new Set<Id>();
			exclusionIds = new Set<Id>();
		}
		
		/**
		 * Tests if quantity of this option is editable.
		 */
		public Boolean isQuantityEditable() {
			return (record.SBQQ__Quantity__c == null) && (record.SBQQ__Bundled__c == false);
		}
		
		/**
		 * Marks this options as dependent on specified option.
		 */
		public void addDependency(Option dependency) {
			dependencyIds.add(dependency.record.Id);
		}
		
		/**
		 * Marks this option as exclusive with specified option.
		 */
		public void addExclusion(Option exclusion) {
			exclusionIds.add(exclusion.record.Id);
		}
		
		/**
		 * Tests if this option is mutually exclusive with specified option. 
		 */
		public Boolean isExclusiveWith(Option option) {
			return exclusionIds.contains(option.record.Id);
		}
		
		/**
		 * Tests if this option is dependent upon specified option.
		 */
		public Boolean isDependentOn(Option option) {
			return dependencyIds.contains(option.record.Id);
		}
		
		/**
		 * Tests if this option has dependency on any other option. 
		 */
		public Boolean hasDependencies() {
			return !dependencyIds.isEmpty();
		}
		
		/**
		 * Tests if this option has exclusive with any other option.
		 */
		public Boolean hasExclusions() {
			return !exclusionIds.isEmpty();
		}
		
		public Boolean isConfigurationAllowed() {
			return (record.SBQQ__ProductConfigurationType__c != null) && record.SBQQ__ProductConfigurationType__c.equalsIgnoreCase('allowed');
		}
		
		public Boolean isConfigurationRequired() {
			return (record.SBQQ__ProductConfigurationType__c != null) && record.SBQQ__ProductConfigurationType__c.equalsIgnoreCase('required');
		}
	}
	
	/**
	 * Value object that wraps ProductFeature__c record.
	 */
	public class Feature {
		public ProductVO parent {get; private set;}
		public SBQQ__ProductFeature__c record {get; private set;}
		public List<Option> options {get; private set;}
		
		/**
		 * Default constructor.
		 */
		public Feature(ProductVO parent, SBQQ__ProductFeature__c record) {
			this.parent = parent;
			this.record = record;
			options = new List<Option>();
		}
	}
	
	/**
	 * Value object that wraps OptionConstraint__c record.
	 */
	public class Constraint {
		public SBQQ__OptionConstraint__c record {get; private set;}
		public ProductVO parent {get; private set;}
		
		public Constraint(ProductVO parent, SBQQ__OptionConstraint__c record) {
			this.parent = parent;
			this.record = record;
		}
		
		public Boolean isExclusion() {
			return record.SBQQ__Type__c == 'Exclusion';
		}
		
		public Boolean isDependency() {
			return record.SBQQ__Type__c == 'Dependency';
		}
		
		public Id getConstrainingOptionId() {
			return record.SBQQ__ConstrainingOption__c;
		}
		
		public Id getConstrainedOptionId() {
			return record.SBQQ__ConstrainedOption__c;
		}
	}
	
	public SBQQ__BlockPrice__c getBlockPriceByQuantity(Decimal quantity, String currencyCode) {
		if (quantity == null) {
			return null;
		}
		if (record.SBQQ__BlockPrices__r != null) {
			for (SBQQ__BlockPrice__c price : record.SBQQ__BlockPrices__r) {
				if (UserInfo.isMultiCurrencyOrganization()) {
					String code = (String)price.get('CurrencyIsoCode');
					if (code != currencyCode) {
						continue;
					}
				}
				if ((quantity >= price.SBQQ__LowerBound__c) && (quantity < price.SBQQ__UpperBound__c)) {
					return price;
				}
			}
		}
		return null;
	}
	
	public Decimal getBlockPriceAmountByQuantity(Decimal quantity, String currencyCode) {
		SBQQ__BlockPrice__c price = getBlockPriceByQuantity(quantity, currencyCode);
		return (price != null) ? price.SBQQ__Price__c : null;
	}
	
}