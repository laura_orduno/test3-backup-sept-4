@IsTest
public with sharing class RestApi_MyAccountHelper_TestUtils {

	public static final String SUBJECT = 'This is the subject for this case';
	public static final String DESCRIPTION = 'Description for this case';
	public static final String UUID = '909090';
	public static final String UUIDNOT = '1010101';
	public static final String REGUUID = '190190190';
	public static final String CASE_ORIGIN = 'My Account';
	public static final String EMAIL = 'puser000@amamama.com';
	public static final String BILLINGACCOUNTNUMBER = '1337';
	public static final String REQUESTTYPE = 'Account update';
	public static final List<String> COLLABORATORS = new list<String>{
			'test1@telus.com', 'test2@telus.com', 'test3@telus.com'
	};
	public static final String CHECKCOLLABORATORS = 'test1@telus.com;test2@telus.com;test3@telus.com';
	public static final Boolean STOPNOTIFCATIONS = true;
	public static final Boolean ALLOWNOTIFICATIONS = false;
	public static final Boolean CHECKALLOWNOTIFICATIONS = true;
	public static final String BADUUID = '909091';
	public static final String ATTACHMENTNAME = 'test.jpg';
	public static final Blob ATTACHMENTBODY = Blob.valueOf('iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAAMvElEQVRoge2ZfZBU1ZXAf/d99Mf0xzAzMAwwfAjoCIwgOAYwRrCAIimT0sgWJSDGmFhY2V00m2RrQTeWVYFUuVuJbrK7sfygEEJMKK1FZsu4JBGIxijJEESQDBJhZoCZYYbpoT9fv3737h/d/abfdM+ARvePLU/Vqdvz5r1zz++ec+897z74VD6VT0TEx2lsO9TpcIuAhQbME9AgIaQAHRLAuRy8A7xpwMHVMPhx9f1Xg/wUahTcp8H9wPSIrmfqqqpCYb9fqzIM0DRQCpnLkc5miWezzkXLSsWlDOrwXg6e9sG21XnQ/3uQAsA/CthY5/PJKQ0N4XG1tejBICKRgFQKZVkIKUGpvBoGyjDAMLBTKS4kEnQkEokBKZWCf/XBDz4q0EcC2Qlf0eDf6oNBc+aMGcFwXR309UF3NyoWQ3McRIlxAajCbwUoTYNgEBUKoXw+BhMJTsXjyYuOY0m4fx289ImCPAuRKthuatrKuTNmVI2ZPBnV0YE8cwbNttHAVVGiRVElKoutrqPCYVQgwEA8ztFUKuXA7gw88FXIfOwg26AhAK/VVlVd1XzDDX7dsnCOHkVLpz0AGiAiEbS5cxFTp4Lfn+/IslCnT6PeeQeVSCALMK4aBkQiWFJyLB7PxKU8rsGK1XDxYwPZBeMF/HHimDH11yxaZMr330eeOoWmFBqgA1okgr5mDfpttyFuvhlRABguKpOB119HtrYiX3gBZzhUKIQ0DP4cj2cvSNmp4MZ1MPBXg+yEqA5/nFRbO23m4sWGc+QIsqtrCCAQwHzoIYyNGxHhMEIIj3oglPLqwADOD3+I85OfIDMZnAKM8vuRfj/tiUT2gpQnLFh4uTS7LMgu2DM2HF4x59Zbg7k//QnZ2ZkHAPRp0/Dv2oU+Z47ruKZpHohiq5TywEgph34fPYq9Zg1OZycScADl8+GYJu+mUqmUUj+/C+4bzU9ttH/uhPt8mra8afHioN3eTq6zc+jBxYvx/+pXaLNnl0WhElTp7zJtbsb49a/RFi507atsFmyba3y+KgF37YA7RvN1xIg8D/UGnJrb3ByO1tWROXDAjYTZ0kLV3r2IQMB1cHhrDwww0NaG1duLzGQwolEis2YRnT3bExFPm8lg3XYbdlvbUGQCAQZsm5OOM2DAVSNVA8ZIIDr8U104bFQ3NZF+9dWhSEyciP/558HvL0sfIQTnWlt5/8c/pu+NN1C5XJnd4MSJTF2/nukbNuCrr0cIgVIq3/r9GDt2kFu+HM6fdyMTNU3CjuO/BH8HbLniiGyHOhM6FyxcGDSzWezDh92lNfzCC5grVpRFIdvby9vr19P/u9+NNDYeMceM4fonnmDiqlVl0bFfeYX03Xd7luakUrznOHEHJtwDyeH2Ks4RA+6uDgZlcOpUrBMn3E3MWLYMfdmysvsT7e3sX7LkiiEA7FiMQ/fey/tPPln2P33lSvSlS4c2UMchoGlUAQJWVbJXEUSDB8ZPnx6yz55FptP58kIIfJs3A0MrEEC2r483V60i3dV1xRClcuyRR+jYudP9u2jb2LwZhMiDFGq1WiEiBmy4IpDnoV7CzDFTppA9c8atkbS5c9Gbmz33KqV458EHSZ0+XWbYjEaZ/rWv8ZlnnmHR9u3MefhhwjNnVoR591vfIn7ypOeaPm8eoqUl3w8gpSQKSLjxKagabqNssgtYWmWaab2mJmJ3d6MVDBkrVrjOFydo7NAhzre2ljk2fvlyPrNtG4H6es/1ax9+mL88/TRHvv1tpG27151MhvYtW7j+2WfdPgCMz38e+9ChfFSkRNM0fEplQnAz8D+ltssiosGC6rq6kHPpEiqXc/PUXL7cs6kBnCl0XCrjli5l8Ysv4hs7tmwnRwiuuv9+Wp57ruy57r17sXp7PX3oN93kKTQVUAV+AfOHP18WER2uD0SjWm5w0H0YXUe79lq3AyEE0rbp3bfPOwimyfU/+hHCMJBSVixRACZ++ctc9/jjpM6d8/zP6u3FLAwAgNbUhNI0lJTuXPGBD5h7WRAHJprBIE4qhaTwLlFbCz6fZ7TSHR3YA95aru6WWwhOnepCFKFLIYrRmbZhQ1nESssWpRQEAlBTg+rvdwdVz/vUONzvstQSENJM000rCRAKDRkvOJMpGc2iRK+7rswpKWXZPnE5LQVWoZAntQoOV182IkBZXspstmx+lC7BQ6MgXAdGi4jH0cuotKyyFzKGXjhHBhEQz9k2fsMYenhgACUlQtddR3zDViSAZHt7WVqVAn8YGMhvhE4sVvYSRoV6qwxEwjk7nZ4nGhqKD0E6Te7cOczGodQMNDaih0I4yaFq4cJvfkOmpwd/SQ01XIrXTj3xBINHjniuT7rnHmo++1kXxunqKotIYdHuHG630s5+ODU46GjRqCec2QMHvCNmmtQuWeJ50EmnOfad76BK5kWlydz/xhuc3LKFnpdfdrW3tZXg1Kme+7O//73bvwSkEGQh4+TPxkYHkdAW7+9PadEoyudzDwmsffvKHJtw111lo9DT2sq73/wm0rLciV464Xv37aNt7VqUlJ7n6m69Ff+kSV6Q115zIVShXMmALaBteL9l1e8vYJwNZ69budK0jh8n29mZfxsMBBj71lsYNTWel6LDd95J7K23yoCCU6Ywaf16ovPmoRwHq7ubnj176N+/v+xeoess2LuXyNy5LkTu4kUuLlqEk8nko6Jp5JSiQynbB9WrIT0qCMAuONrY1NRcPX488YMHERRK+K9/ncgjj3hArLNnafvSl8heuFDJ1BXJ9E2bmPyNb3iiEX/sMVLbtrkZoQyDQcchptTBtbBkuI2K1a+C/+g7cyZlNDTkD9LIhze5YwfO2bOeDv2TJjHnqacwotGPBDFh3ToaH3jAYzN3+jTJnTs9c0MKQUKphIL/rGSnIogJu9KZjEicPk1g1izXoGNZxL77XZTjeCZwpKWF+Xv3EmpqumIAYZpM37SJq7dudcsPpRTKcYg/9hjStoeioetYUmJDLgv/VcmeXunibrDuhGprYGBB/Y03mlZXFzKbBSD3wQeodBr/5z7nWV6NMWOYsHYtvrFjSZ08Se7SpcoAhkH97bcz68knqV25EkogAOJbtpB66aUhCE1D6Tr9uVzShq1fgQMV7Y40Yk9BVRg+aJwzpz5aX09s/340pfLHoEJQ873vEVq7dsjQsPf35PHjXGprw+7rw8lkMCIRgjNnUnPTTeiRSMUKIbljB4OPPopUyl36RSBAwrbVgON0mHDNash+KBCAZ2BtUIinm5Yurcr19pI+dsxzphu64w5qvv99RCBQblhUNl1xk0yniW3aRHLPHs/mJ/x+HKDbstLdsOYf4JeAVcluxdQqiO9lOPVFmJ3p6ZkxbsECQ1oWucGh6sA+cQLrt7/Fd/XV6BMnjmJqZMm+/TYXN24kffBgGQS6To9lZWLws42wrchN/qToikB08nW//y/w+g253BezPT3hsS0thrRtcrGYe6PT00Ni927s48cxJ0xAb2iAEaLhipRYf/gDA48+Suzxx8n19nohAgGE309fKpVOKPXnzfBQqnDMhfcw35WRevQBfiAABJbCpHvhxTHV1WMnL1rkszo6SJ04gSikSennA2P8eKpWrsQ/fz5GY6N7mK0sC6erC6utjeSrr+L09uavl3ikhEALhfKTOx7PJqQ8+wNYdxT6yZ/9lqpnrowGEijV+dDw9/BcJBhsnLxggV8oRfLoUZx4vOyjzmjGVYXfCsA00SMRHMehPx63BqU8tRX+9nT+s0LReWvYb1dGSi2NfGWsF7Ub7DfhvxfkcldluromG4ahVzc3IwIBcokEsnCq6PkyNYqW7thGdTVaOEwymaQvkbC7lPrlZvjn3vxnOBvIFdQuUU+xNhJI8WWseNyrASIJ6hU4OBnORmKxluT58yJQXa2Hm5owotG8c7aNdJzRYQwDPRRCq6nBiEaxMhn6Y7HsJdsefA22/gv8PDuUPvYIrUdGm5U6+bTyV9LxUP0gfLURVpmGQfW4cf5wXR1GOJyfD6lUvgLOZvNzSdMQhoHQdRCCXDJJKpEgnkxaOaXkSdj97/Czi/koZMmnznAtplTZofLlvo9oJTC+Cq05DWr+BpZdA7cHYQZCWMFg0AwEg6ZumnkIpXCkxMlmyVpWNm1ZOQG+OLS/B62/gAPdcKlktItqVYAqW3qvBKQoRceLapa0RTWmQOQLcMMEuLoWmvwwTiukrwQnAz0Xob0LTu6Fw72Qwpv/ObzpM1wrHBR8OJDivRUBSrS4OJR+Gy3tp3SeOyWaY3SgEQE+CkipaKNAFEGKrzHFfkoXNLegLrS5YUBZhq1KnxRIJTvDQYoww6USSLH9VP7fyP8CNGNTUEDntqIAAAAASUVORK5CYII=');
	public static final String DEFAULTRTNAME = 'SMB Care Billing';
	public static final Id DEFAULTRTID = Schema.SObjectType.Case.getRecordTypeInfosByName().get(DEFAULTRTNAME).getRecordTypeId();
	public static final String ORIGIN = 'My Account';

	public static final String CONTACTFIRST = 'Phillip';
	public static final String CONTACTLAST = 'Smith';
	public static final String CONTACTFULL = CONTACTFIRST + ' ' + CONTACTLAST;
	public static final String USERNAME = 'psmith@telus.com' + System.currentTimeMillis();
	public static final String EXTERNALSTATUS = 'EX In Progress';
	public static final String INTERNALSTATUS = 'IN In Progress';

	public static final String REQUEST_TYPE_GET_CASES = 'get-cases';
	public static final String REQUEST_TYPE_UPDATE_CASE = 'update-case';
	public static final String REQUEST_TYPE_CREATE_CASE = 'create-case';
	public static final String REQUEST_TYPE_GET_CASE_DETAILS = 'get-case-details';
	public static final String REQUEST_TYPE_CREATE_COMMENT = 'create-comment';
	public static final String REQUEST_TYPE_CREATE_ATTACHMENT = 'create-attachment';
	public static final String REQUEST_TYPE_TEST = 'test';

	public static User createPortalUser(Account porAcc) {
		trac_TriggerHandlerBase.blockTrigger = true;
		Contact contact1 = new Contact(
				FirstName = CONTACTFIRST,
				Lastname = CONTACTLAST,
				AccountId = porAcc.Id,
				Email = System.now().millisecond() + 'test@test.com'
		);

		insert contact1;
		trac_TriggerHandlerBase.blockTrigger = false;

		return new User(
				ProfileId = [SELECT Id FROM Profile WHERE Name = 'Customer Community User Custom'].Id,
				LastName = CONTACTLAST,
				FirstName = CONTACTFIRST,
				Email = EMAIL,
				Username = USERNAME,
				CompanyName = 'TEST',
				Title = 'title',
				Alias = 'alias',
				TimeZoneSidKey = 'America/Los_Angeles',
				EmailEncodingKey = 'UTF-8',
				LanguageLocaleKey = 'en_US',
				LocaleSidKey = 'en_US',
				ContactId = contact1.Id,
				FederationIdentifier = UUID
		);
	}

	public static Case createCase(Account a, User u) {
		Case caseToInsert = new Case();
		caseToInsert.Origin = CASE_ORIGIN;
		caseToInsert.accountId = a.Id;
		caseToInsert.contactId = u.ContactId;
		caseToInsert.status = INTERNALSTATUS;
		caseToInsert.Subject = 'test';
		return caseToInsert;
	}

	public static Segment__c createSegment() {
		String filler = 'fil';
		return new Segment__c(
				CBU_Abbreviated_Name__c = filler,
				CBU_Code__C = filler,
				CBU_Name__c = filler,
				CBU_Description__c = filler,
				Channel_Region__c = filler,
				CSH_Managing_Director__c = filler,
				CSH_Segment_Name__c = filler,
				CSH_Sub_Segment_Description__c = filler,
				ILEC_Flag__c = filler,
				Marketing_Managing_Director__c = filler,
				Marketing_Segment_Name__c = filler,
				Marketing_Sub_Segment_Description__C = filler,
				Marketing_Sub_Segment_Short_Description__C = filler,
				Cust_Segment_A__c = 'SBS');
	}

	public static RestApi_MyAccountHelper_GetCases.RequestUser createRequestUser() {
		RestApi_MyAccountHelper_GetCases.RequestUser ru = new RestApi_MyAccountHelper_GetCases.RequestUser();
		ru.uuid = UUID;
		ru.firstName = CONTACTFIRST;
		ru.lastName = CONTACTLAST;
		ru.email = EMAIL;
		return ru;
	}

	public static ExternalToInternal__c createEtoI() {
		return new ExternalToInternal__c(
				Name = 'Example',
				Identifier__c = 'MyAccount',
				Internal__c = INTERNALSTATUS,
				External__c = EXTERNALSTATUS
		);
	}

	public static ExternalToInternal__c createEtoIRT() {
		return new ExternalToInternal__c(
				Name = 'RequestTest',
				Identifier__c = 'Test',
				External__c = REQUESTTYPE,
				Internal__c = DEFAULTRTNAME
		);
	}

	public static ExternalToInternal__c createEtoICancelled() {
		return new ExternalToInternal__c(
				Name = 'RequestTest2',
				Identifier__c = 'Test',
				External__c = 'Cancelled',
				Internal__c = 'Cancelled'
		);
	}

	public static CaseComment createCaseComment(Case c) {
		return new CaseComment(
				ParentId = c.id,
				CommentBody = 'Comment Body',
				IsPublished = true
		);
	}

	public static Attachment createAttachment(Case c) {
		return new Attachment(
				ParentId = c.id,
				Name = 'Test.txt',
				Body = Blob.valueOf('Random Text')
		);
	}

	public static User createRegularUser() {
		return new User(
				ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
				LastName = CONTACTLAST,
				FirstName = CONTACTFIRST,
				Email = 'puser000@amamama.com',
				Username = 'sysadmin' + USERNAME,
				CompanyName = 'TEST',
				Title = 'title',
				Alias = 'alias',
				TimeZoneSidKey = 'America/Los_Angeles',
				EmailEncodingKey = 'UTF-8',
				LanguageLocaleKey = 'en_US',
				LocaleSidKey = 'en_US',
				FederationIdentifier = REGUUID
		);
	}

	public static Account createAccount(Id segment) {
		Account a = new Account(Name = 'Account Inc.', Billing_Account_ID__c = BILLINGACCOUNTNUMBER, BCAN__c = BILLINGACCOUNTNUMBER, sub_Segment_Code__c = segment);
		return a;
	}

	public static EmailTemplate createEmailTemplate(String name) {
		EmailTemplate validEmailTemplate = new EmailTemplate();
		validEmailTemplate.isActive = true;
		validEmailTemplate.Name = name;
		validEmailTemplate.DeveloperName = name + System.currentTimeMillis();
		validEmailTemplate.TemplateType = 'text';
		validEmailTemplate.FolderId = UserInfo.getUserId();
		return validEmailTemplate;
	}

	public static void createMBRCustomSetting() {
		trac_TriggerHandlerBase.blockTrigger = true;
		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		EmailTemplate e = null;

		Account a = new Account(Name = 'DummyAcc');
		insert a;

		Contact c = new Contact(
				FirstName = 'DummyId',
				Lastname = 'DummyCon',
				AccountId = a.Id,
				Email = System.now().millisecond() + 'wat@test.com'
		);

		insert c;

		System.runAs(thisUser) {
			e = RestApi_MyAccountHelper_TestUtils.createEmailTemplate('Test');
			insert e;
		}
		// Attempt to workaround MIXED DML Operations https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_dml_non_mix_sobjects_test_methods.htm
		System.runAs(thisUser) {
			List<OrgWideEmailAddress> emails = [Select Id From OrgWideEmailAddress LIMIT 1];//Where DisplayName = 'My Account Business Requests'];

			MBR_Case_Collaborator_Settings__c settings = MBR_Case_Collaborator_Settings__c.getOrgDefaults();
			settings.Dummy_Contact_Id__c = c.id;
			settings.My_Account_Org_Wide_Email_Id__c = emails.isEmpty() == false ? emails[0].id : null;
			settings.New_My_Account_Case_Template_Id__c = e.id;
			settings.Update_My_Account_Case_Template_Id__c = e.id;
			upsert settings MBR_Case_Collaborator_Settings__c.Id;
		}
		trac_TriggerHandlerBase.blockTrigger = false;
	}

	public static MASRUserBundle createMASREnabledUser() {
		trac_TriggerHandlerBase.blockTrigger = true;

		Segment__c segment = createSegment();
		insert segment;

		Account parentAcc = new Account(
				Name = 'Account Inc.',
				sub_Segment_Code__c = segment.id,
				RecordTypeId = RestApi_MyAccountHelper_Utility.RT_RCID
		);
		insert parentAcc;

		Account myAcct = new Account(
				Name = 'Account Inc.',
				Billing_Account_ID__c = BILLINGACCOUNTNUMBER,
				BCAN__c = BILLINGACCOUNTNUMBER,
				RecordTypeId = PortalConstants.ACCOUNT_RECORDTYPE_BAN,
				Billing_Account_Type__c = PortalConstants.MASR_ACCOUNT_TYPE,
				Billing_Account_Subtype__c = PortalConstants.MASR_ACCOUNT_SUBTYPE
		);
		insert myAcct;

		User myUser = createPortalUser(myAcct);
		insert myUser;

		Contact c = [SELECT MASR_Enabled__c FROM Contact WHERE Id = :myUser.ContactId];
		c.MASR_Enabled__c = true;
		update c;

		trac_TriggerHandlerBase.blockTrigger = false;

		return new MASRUserBundle(parentAcc, myAcct, myUser);
	}

	public class MASRUserBundle {
		public Account parentAccount { get; set; }
		public Account banAccount { get; set; }
		public User u { get; set; }

		public MASRUserBundle(Account parentAccount, Account banAccount, User u) {
			this.parentAccount = parentAccount;
			this.banAccount = banAccount;
			this.u = u;
		}
	}
}