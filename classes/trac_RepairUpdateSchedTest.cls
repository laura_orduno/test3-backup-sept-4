/*
 * Tests for trac_RepairUpdateSched
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */
 
@isTest
private class trac_RepairUpdateSchedTest {
  
  // coverage only
  private static testMethod void testSched() {
    trac_RepairUpdateSched.schedule();
  }
}