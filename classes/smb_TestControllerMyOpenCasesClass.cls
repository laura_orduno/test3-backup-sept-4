@isTest(SeeAllData = true)
private class smb_TestControllerMyOpenCasesClass {
    
    static testMethod void validateMyOpenCases() {
       list<case> lstCase = new list<case>();
       
       RecordType rt = [SELECT Id, Name, DeveloperName
                        FROM RecordType
                        WHERE sObjectType = 'Account' and
                        DeveloperName = 'RCID' and 
                        isActive = true ];
       
       RecordType CaseRT = [SELECT Id, Name, DeveloperName
                            FROM RecordType
                            WHERE sObjectType = 'Case' and
                            DeveloperName = 'SMB_Care_Billing' and 
                            isActive = true ];
                            
       RecordType ConRT = [SELECT Id, Name, DeveloperName
                            FROM RecordType
                            WHERE sObjectType = 'Contact' and
                            DeveloperName = 'Standard_Contact' and 
                            isActive = true ];
       
       Account AccRec = new Account(Name = 'RCID',RecordTypeId = rt.id,
                                    BillingStreet = '3777 Kingsway',
                                    BillingCity = 'Burnaby',
                                    BillingState = 'BC',
                                    BillingCountry = 'CAN',
                                    BillingPostalCode = 'V7C 2K5',
                                    RCID__c = '987654');                 
       
       Insert AccRec;
       
       Contact Con = new Contact(Account=AccRec,lastname='test',phone='1234232323',
                                 Language_Preference__c='English');
       Insert Con;
       
       case c1 = new case(ownerid= UserInfo.getUserId(),Recordtypeid = CaseRT.id,
                          Account = Accrec,Origin='Phone',Priority='High',
                          Status = 'New',type='Billing Interpretation',
                          Subject='Test',Description='test',contact =Con );
                          
       case c2 = new case(ownerid= UserInfo.getUserId(),Recordtypeid = CaseRT.id,
                          Account = Accrec,Origin='Phone',Priority='Medium',
                          Status = 'New',type='Billing Interpretation',
                          Subject='Test',Description='test',contact =Con );
                          
       case c3 = new case(ownerid= UserInfo.getUserId(),Recordtypeid = CaseRT.id,
                          Account = Accrec,Origin='Phone',Priority='low',
                          Status = 'New',type='Billing Interpretation',
                          Subject='Test',Description='test',contact =Con );
      
       lstCase.add(c1);
       lstCase.add(c2);
       lstCase.add(c3);
       insert lstCase;
       smb_Controller_MyOpenCases ConIns = new smb_Controller_MyOpenCases ();
       ConIns.sortBy = 'CaseNumber';
       ConIns.sortDir = 'ASC';
       ConIns.sortList(lstCase); 
       ConIns.empty();        
       
       
       
       
       
       
       
    }
}