/*
 * 
 */
global with sharing class NAAS_ProductTechnicalAvailabilityImpl {
    
    public NAAS_InOrderFlowPACController controllerRef;
   
    public NAAS_ProductTechnicalAvailabilityImpl(NAAS_InOrderFlowPACController controller) {
            this.controllerRef=controller;
            
    }
   
    public NAAS_ProductTechnicalAvailabilityImpl(NAAS_OutOrderFlowPACController controller) {
        controllerRef = null;
        
    } 
    
            
 }