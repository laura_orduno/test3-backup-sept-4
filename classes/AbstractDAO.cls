/**
 * Abstract implementation of a Data Access Object (DAO).
 * 
 * @author Max Rudman
 * @since 4/6/2011
 */
public abstract class AbstractDAO {
	//public SortField[] sortFields {get; set;}
	
	protected void saveChildren(Schema.SObjectField parentField, List<SObject> children) {
		if (children.isEmpty()) {
			return;
		}
		
		Schema.SObjectType childType = children[0].getSObjectType();
		Set<Id> parentIds = new Set<Id>();
		for (SObject rec : children) {
			Id pid = (Id)rec.get(parentField);
			if (pid != null) {
				parentIds.add(pid);
			}
		}
		
		List<SObject> inserts = new List<SObject>();
		List<SObject> updates = new List<SObject>();
		Set<Id> existingIds = new Set<Id>();
		for (SObject rec : children) {
			if (rec.Id != null) {
				updates.add(rec);
				existingIds.add(rec.Id);
			} else {
				inserts.add(rec);
			}
		}
		
		List<SObject> deleted = new List<SObject>();
		if (!parentIds.isEmpty()) {
			QueryBuilder qb = new QueryBuilder(childType);
			qb.getSelectClause().addField('Id');
			qb.getWhereClause().addExpression(qb.inExpr(parentField, parentIds));
			for (SObject oldRec : Database.query(qb.buildSOQL())) {
				if (!existingIds.contains(oldRec.Id)) {
					deleted.add(oldRec);
				}
			}
		}
		
		Database.delete(deleted, true);
		Database.insert(inserts, true);
		Database.update(updates, true);
	}
	
	/* protected QueryBuilder createChildSubQuery(Schema.SObjectType parentType, Schema.SObjectField parentField, Schema.SObjectField[] fields) {
		QueryBuilder query = new QueryBuilder(MetaDataUtils.findRelationship(parentType, parentField).getRelationshipName());
		if (fields != null) {
			query.getSelectClause().addFields(fields);
		}
		return query;
	} */
	
	protected void addCustomFields(QueryBuilder query, Schema.SObjectType type) {
		for (Schema.SObjectField field : MetaDataUtils.getFields(type)) {
			if (field.getDescribe().isCustom()) {
				query.getSelectClause().addField(field);
			}
		}
	}
	
	/*
	protected void addSortFields(QueryBuilder query) {
		if (sortFields != null) {
			for (SortField sfield : sortFields) {
				if (sfield.ascending) {
					query.getOrderByClause().addAscending(sfield.expression);
				} else {
					query.getOrderByClause().addDescending(sfield.expression);
				}
			}
		}
	}
	
	public class SortField {
		public String expression {get; private set;}
		public Boolean ascending {get; private set;}
		
		public SortField(Schema.SObjectField field, Boolean ascending) {
			this(field.getDescribe().getName(), ascending);
		}
		
		public SortField(String expression, Boolean ascending) {
			this.expression = expression;
			this.ascending = ascending;
		}
	} */
	
	public class LoadException extends Exception {}
}