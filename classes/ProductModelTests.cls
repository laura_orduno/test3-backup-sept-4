/**
 * Unit tests for <code>ProductModel</code> class.
 *
 * @author maax Rudman
 * @since 4/12/2011
 */
@isTest
private class ProductModelTests {
	private static Product2 product;
	private static SBQQ__ProductFeature__c feature;
	private static Product2 option1;
	private static Product2 option2;
	private static List<SBQQ__ProductOption__c> options;
	private static SBQQ__Quote__c quote;
	
	testMethod static void testLoadByIds() {
		setUp();
		
		ProductModel[] result = ProductModel.loadByIds(new Set<Id>{product.Id}, null, null, null);
		System.assertEquals(1, result.size());
	}
	
	testMethod static void testGetters() {
		setUp();
		
		ProductModel target = ProductModel.loadByIds(new Set<Id>{product.Id}, null, null, null)[0];
		System.assertEquals(2, target.getAllOptions().size());
		String fkeys = target.getFeatureKeysEncoded();
		System.assertEquals(target.features[0].getkey() + ',' + target.otherFeature.getKey() + ',configurationForm', fkeys);
		System.assertEquals(feature.Id, target.getFeatureByKey(target.features[0].getKey()).id);
		ProductFeatureModel defaultFeature = target.getDefaultFeature();
		System.assertEquals(defaultFeature, target.features[0]);
	}
	
	testMethod static void testFeatures() {
		setUp();
		
		ProductModel target = ProductModel.loadByIds(new Set<Id>{product.Id}, null, null, null)[0];
		ProductFeatureModel pfm = target.getDefaultFeature();
		System.assertEquals('Add Options', pfm.getAddOptionDialogTitle());
		System.assertEquals('Add Option Button', pfm.getAddOptionButtonLabel());
		System.assertEquals('ExcludedField__c', pfm.getAddOptionExcludedFields());
		System.assertEquals('Addl Instructions', pfm.getAdditionalInstructions());
	}
	
	testMethod static void testOptions() {
		setUp();
	}
	
	testMethod static void testUpdateQuote() {
		setUp();
		
		QuoteVO qvo = new QuoteVO(quote);
		QuoteVO.LineItemGroup grp = qvo.addGroup(new SBQQ__QuoteLineGroup__c());
		
		ProductVO pvo = ProductService.getInstance().findProductsWithNestedOptionsAndPrices(new Set<Id>{product.Id}, null, null).get(product.Id);
		ProductModel target = new ProductModel(pvo);
		
		ProductOptionSelectionModel posm = new ProductOptionSelectionModel(target);
		posm.addSelection(target.getOptionById(options[0].Id));
		target.setSelections(posm);
		
		target.updateQuote(qvo, grp.key);
		System.assertEquals(2, qvo.lineItems.size());
	}
	
	private static void setUp() {
		Account account = new Account(Name='Test');
		insert account;
		
		Pricebook2 pricebook = new Pricebook2(Name='TestPricebook',IsActive=true);
		insert pricebook;
		
		OpportunityStage os = [SELECT MasterLabel FROM OpportunityStage WHERE IsClosed = false AND IsActive = true LIMIT 1];
		Opportunity opportunity = new Opportunity(Name='Test',StageName=os.MasterLabel,CloseDate=System.today(),AccountId=account.Id,Pricebook2Id=pricebook.Id);
		insert opportunity;
		
		quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opportunity.Id,SBQQ__LineItemsGrouped__c=false);
		insert quote;
		
		product = new Product2(Name='Test',SBQQ__ConfigurationFields__c='Field1,Field2',SBQQ__ConfiguredCodePattern__c='{1}.{2}');
		option1 = new Product2(Name='Option1',ProductCode='C1');
		option2 = new Product2(Name='Option2',ProductCode='C2');
		insert new List<Product2>{product,option1,option2};
		
		feature = new SBQQ__ProductFeature__c(SBQQ__Number__c=1,Name='Test',SBQQ__ConfiguredSKU__c=product.Id,SBQQ__MinOptionCount__c=1,AddOptionDialogTitle__c='Add Options',AddOptionButtonLabel__c='Add Option Button',AddOptionExcludedFields__c='ExcludedField__c',AdditionalInstructions__c='Addl Instructions');
		insert feature;
		
		options = new List<SBQQ__ProductOption__c>();
		options.add(new SBQQ__ProductOption__c(SBQQ__ConfiguredSKU__c=product.Id,SBQQ__OptionalSKU__c=option1.Id,SBQQ__Number__c=1,SBQQ__UnitPrice__c=100,SBQQ__ComponentCodePosition__c=1,SBQQ__Feature__c=feature.Id));
		options.add(new SBQQ__ProductOption__c(SBQQ__ConfiguredSKU__c=product.Id,SBQQ__OptionalSKU__c=option2.Id,SBQQ__Number__c=2,SBQQ__UnitPrice__c=200,SBQQ__ComponentCodePosition__c=2));
		insert options;
	}
}