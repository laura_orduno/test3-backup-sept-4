/* Developed By: Hari Neelalaka, IBM
*  Created Date: 25-Dec-2014
*/
@isTest

private Class SRS_SchConditioncalculation_Test{
    
    static testMethod void testSchConditioncalculation(){         
        test.startTest();
        SRS_SchConditioncalculation schConCal=new SRS_SchConditioncalculation();
        String schedule = '0 0 23 * * ?';
        system.schedule('Schedule Condition calculation', schedule, schConCal);
        test.stopTest();                    
    }
}