public without sharing class QuoteCreditCheckController {
        
    public void onInit() {
        try {
            String qid = ApexPages.currentPage().getParameters().get('qid');
            SBQQ__Quote__c  quote = [SELECT Credit_Check_Requested__c FROM SBQQ__Quote__c WHERE Id = :qid]; 
            try { quote.Credit_Check_Requested__c = true;
                update quote; } catch (Exception e) { ApexPages.addMessages(e); }
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Quote sent for credit check')); } catch (Exception e) { QuoteUtilities.log(e); }
    }
    
    public PageReference onContinue() {
        try {
            String summaryUrl = ApexPages.currentPage().getParameters().get('summaryUrl');
            PageReference pref = new PageReference(summaryUrl);
            pref.setRedirect(true);
            return pref; } catch (Exception e) { QuoteUtilities.log(e); } return null;
    }
}