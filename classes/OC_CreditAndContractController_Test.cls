@istest(seealldata=false)
private class OC_CreditAndContractController_Test{
    static testMethod void testMethod1() {
        Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
        insert RCIDacc;
        
        order masterOrder = new order(Ban__c='draft',Reason_Code__c='34aas',General_Notes_Remarks__c='123123',accountId=RCIDacc.id,effectivedate=system.today(),status='Draft');   
        insert masterOrder;
        
        Test.startTest();
        PageReference pageref = Page.OC_CreditAndContract;
        Test.setCurrentPage(pageref);        
        pageref.getparameters().put('parentOrderid',masterOrder.Id);
        //ApexPages.StandardController sc_1 = new ApexPages.standardController(masterOrder);
        OC_CreditAndContractController corder= new OC_CreditAndContractController();
        Test.stopTest();
    }
    
    @isTest
    private static void consolidateContractAttributesTest(){
         String orderId=OrdrTestDataFactory.singleMethodForOrderIdV2();
        Order objOrder=[select id from order where id=:orderId];
		Test.startTest();
        OC_CreditAndContractController obj=new OC_CreditAndContractController();
        obj.objOrder=objOrder;
        obj.consolidateContractAttributes();
        Test.stopTest();
        
    }
    
}