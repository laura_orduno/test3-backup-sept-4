global without sharing class trac_Offer_House_Wizard_Controller {
    
    public PageReference thePage {get; set;}
    
    // Boolean indicators
    public Boolean wizardError {get; set;}
    public Boolean approvalError {get; set;}
    public Boolean agentError {get; set;}
    public Boolean nameError {get; set;}
    public Boolean isFrench {get; set;} // denotes if the logged in user's language is french or english
    public Boolean isCreate {get; set; } // denotes if offer house demand object is new (not inserted)
                                        //  or an edit of a current object
    public Boolean isUrgent {get; set;} // denotes if the record has been marked urgent (causes a warning to display)
    public Boolean isHistoryVersion {get; set;} // denotes if the offer house demand being viewed is a history record
    public Boolean hasPrevious {get; set;} // indicates if a previous version was found
    public Boolean hasRequired {get; set;} // denotes if there are empty required fields
    public String showSuccess {get; set;}
      
    public String systemURL {get; set;} // base URL of the current salesforce instance, used to launch new pages
    public String errorMessage {get; set;} // used for debugging and tests
    public String theJSON {get; set;} // string to contain json
    public String requiredFields {get; set;} // string to contain required fields on submission
    public String offerQuery {get; set;} // string to contain basic offer query
    public String changedFields {get; set;} // string to contain list of changed fields
    public ID theID {get; set;} // id of the offer house demand object
    public User theAdmin {get; set;} // id of the admin profile with all rights
    public ID theAccountId {get; set;} // id of the offer house account
    public String theAccountName {get; set;} // name of offer house account
    List<String> fieldNames {get; set;} // field names for offer house demand object
    public Offer_House_Demand__c currentVersion { get; set;} // variable for the offer house demand object
    public Offer_House_Demand__c previousVersion {get; set;} // previous version of the offer house demand object
    public Offer_House_Demand__c lastSavedOffer {get; set;}
    public Offer_House_Wizard_Settings__c wizardSettings {get; set;} // offer house wizard custom setting

    public ProfileType userType; // indicates the user's profile type
    public String UserTypeVF { get { return UserType.name(); }} // Gets the user type as a string for visual force comparisons
    public Set<String> salesProfiles; // contains list of sales profiles
    public Set<String> offerHouseProfiles; // containst list of offer house profiles
    public Set<String> adminProfiles; // contains list of admin profiles
    
    // Exception variables/class
    public class OfferHouseWizardException extends Exception {}
    
    // Enum for profile types
    // Admin with full access
    // Sales with only Submit/Recall
    // Offer House with only 
    public enum ProfileType {ADMIN, SALES, OFFERHOUSE, NONE}
    
    public trac_Offer_House_Wizard_Controller() {
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=8');
        isCreate = false;
        isUrgent = false;
        wizardError = false;
        agentError = false;
        nameError = false;
        hasRequired = false;
        approvalError = false;
        isHistoryVersion = false;
        showSuccess = '0';
        systemURL = URL.getSalesforceBaseUrl().getHost();
        
        // Get custom setting values
        initCustomSettings();
            
          // Get user profile type
          initUserType();
          initLanguage();
          
        // Initialize and compare offer house object and versions
        initOfferHouseObjects();
        
        checkChanged();
        //errorMessage += ' ---- USER TYPE: ' + userType;
        //errorMessage += ' ---- USER PROFILE: ' + userProfile;
        //errorMessage += ' ---- ADMIN PROFILES: ';
        //for (String s : adminProfiles) errorMessage += s + ', ';
        //errorMessage += ' ---- REQUIRED FIELDS:' + getRequiredFields();
    }
    
    public void initLanguage() {
        system.debug('FRENCH DEBUG:' + UserInfo.getLanguage());
        isFrench = !(UserInfo.getLanguage() == 'en_US');
    }
    
    // Offer House Demand Object
    public Offer_House_Demand__c theOffer { 
        get {
            if (theOffer == null ) return initOfferHouseDemand();
            else return theOffer;
        }
        set;
    }
    
    public Offer_House_Demand__c initOfferHouseDemand() {
        
        if (offerQuery == null) offerQuery = createOfferQuery(); // get query string
        String theOfferQuery = offerQuery + ' WHERE Id = :theId'; // Add condition
        
        // Query for Offer House Demand being viewed
        if (theId != null) {
            
            List<Offer_House_Demand__c> theOfferList = Database.query(theOfferQuery);
            
            if (!theOfferList.isEmpty()) {
                theOffer = theOfferList[0];
                lastSavedOffer = theOfferList[0];
            }
            else throw new OfferHouseWizardException('The Offer not loaded.');
            
        } else { // null id, creating new offer house record
            
            theOffer = new Offer_House_Demand__c();
            theOffer.ACCOUNT__c = theAccountId;
            isCreate = true;
        }
        return theOffer;
    }
    
    public void initOfferHouseObjects() {
        
        // Get current Ojbect Id
        if (thePage == null) thePage = ApexPages.currentPage();
        theId = thePage.getParameters().containsKey('ID')? thePage.getParameters().get('ID') : null;
        theAccountId = thePage.getParameters().containsKey('ACCOUNTID')? thePage.getParameters().get('ACCOUNTID') : null;
        theAccountName = thePage.getParameters().containsKey('ACCOUNTNAME')? thePage.getParameters().get('ACCOUNTNAME') : null;
        
        /* Id ADMINPROFILE = [SELECT id FROM Profile WHERE name IN :adminProfiles LIMIT 1].id;
        theAdmin = [SELECT Id FROM User WHERE ProfileId = :ADMINPROFILE AND isActive = true LIMIT 1]; */
        Map<Id, Profile> profileMap = new Map<Id, Profile>([SELECT id FROM Profile WHERE name IN :adminProfiles]);
        theAdmin = [SELECT Id FROM User WHERE ProfileId in :profileMap.keySet() AND isActive = true LIMIT 1];
            
        // Get Offer House Object
        initOfferHouseDemand();
        
        if (theOffer.Type__c == 'Version')  isHistoryVersion = true;
        initPreviousVersions();
        
            // Mark as urgent if necessary
            isUrgent = theOffer.URGENT__c;
              
            // get Existing approval processes if there are any
            initProcessAndWorkItem();
             
            // Load required fields
            checkRequiredFields();
    }
    
    public void initPreviousVersions() {
        
      // Get Previous Versions
      String theVersionQuery = offerQuery;
      String theParentOfferId = theOffer.Offer_House_Demand__c;
      
      if (!isHistoryVersion) {
        theVersionQuery += ' WHERE Offer_House_Demand__c = :theId ORDER BY Version_Number__c DESC LIMIT 2';
      } else {
        theVersionQuery += ' WHERE Offer_House_Demand__c = :theParentOfferId AND Id != :theId ORDER BY Version_Number__c DESC LIMIT 2';
      }
      
      List<Offer_House_Demand__c> previousVersionList = (theId != null && theOffer != null)? 
                      Database.query(theVersionQuery)
                      : new List<Offer_House_Demand__c>();
  
      if (!previousVersionList.isEmpty()) {
        
        for (Offer_House_Demand__c prev : previousVersionList) {
            if (prev.Version_Number__c < (theOffer.Version_Number__c - 1)) previousVersion = prev; 
        }
        hasPrevious = previousVersion != null ? true : false;
        theJSON = compareToLastVersion();
      }
      else {
        hasPrevious = false;
      }
    }
    
    public void initUserType() {
        
        boolean typeSet = false;
        
        // If user is an admin
        for (String s : adminProfiles) if (userProfile == s) {
            userType = ProfileType.ADMIN;
            return;
        }
        
        // If user is from the offer house
        for (String s : offerHouseProfiles) if (userProfile == s) {
            userType = ProfileType.OFFERHOUSE;
            typeSet = true;
        }
        
        // If user is from sales
        for (String s : salesProfiles) if (userProfile == s) {
            userType = typeSet ? ProfileType.ADMIN : ProfileType.SALES;
            typeSet = true;
            return;
        }
        
        // if user is not from any predefined profile
        // only edit/save/cancel functionality will be given
        if (!typeSet) userType = ProfileType.NONE;
        return;
    }
    
    public void initCustomSettings() {
        
        // load the settings
        wizardSettings = Offer_House_Wizard_Settings__c.getOrgDefaults();
        
        // load the admin profiles
        List<String> adminProfileSettingsList = wizardSettings.Admin_Profile__c != null ? wizardSettings.Admin_Profile__c.split(',',0) : new List<String>();
        Set<String> adminProfilesSet = new Set<String>();
        for (String s : adminProfileSettingsList) adminProfilesSet.add(s.trim());
        List<Profile> adminProfilesList = [SELECT Id, Name FROM Profile WHERE Name IN :adminProfilesSet];
        adminProfiles = new Set<String>();
        for (Profile p : adminProfilesList) adminProfiles.add(p.Name);
        
        // load the sales profiles
        List<String> salesProfileSettingsList = wizardSettings.Sales_Profile__c != null ? wizardSettings.Sales_Profile__c.split(',',0) : new List<String>();
        Set<String> salesProfilesSet = new Set<String>();
        for (String s : salesProfileSettingsList) salesProfilesSet.add(s.trim());
        List<Profile> salesProfilesList = [SELECT Id, Name FROM Profile WHERE Name IN :salesProfilesSet];
        salesProfiles = new Set<String>();
        for (Profile p : salesProfilesList) salesProfiles.add(p.Name);
        
        // load offer house profiles
        List<String> offerHouseProfileSettingsList = wizardSettings.Offer_House_Profile__c != null ? wizardSettings.Offer_House_Profile__c.split(',',0) : new List<String>();
        Set<String> offerHouseProfilesSet = new Set<String>();
        for (String s : offerHouseProfileSettingsList) offerHouseProfilesSet.add(s.trim());
        List<Profile> offerHouseProfilesList = [SELECT Id, Name FROM Profile WHERE Name IN :offerHouseProfilesSet];
        offerHouseProfiles = new Set<String>();
        for (Profile p : offerHouseProfilesList) offerHouseProfiles.add(p.Name);
        
        
    }
    
    public String userProfile {
        get {
            if (userProfile == null) {
                List<Profile> userProfileList = [SELECT Id, Name FROM Profile WHERE Id = :UserInfo.getProfileId()];
                if (!userProfileList.isEmpty()) userProfile =  userProfileList[0].Name;
                else userProfile = '';
                return userProfile;
            } else {
                return userProfile;
            }
        }
        set; 
    }
    
    
    // JSON lazy init, compares versions and returns json if possible
    public String getTheJSON() {
        if (theJSON == null) {
            theJSON = compareToLastVersion(); 
            return theJSON;
        } else {
            return theJSON;
        }   
    }
    
    // Helper Method: to compare the offer to its previous version
    // Returns a JSON with changed fields
    // Format: {"<field API Name>" : "<old value>", ... }
    public String compareToLastVersion() {
        
        if (theOffer != null && previousVersion != null) {
            getOfferFields();
            return compareOffersJSON(theOffer, previousVersion);   
        } else { return '';}
    }
    
    // Helper Method: gets Offer House Demand Fields
    public List<String> getOfferFields() {
        
        fieldNames = new List<String>();
        
        Map<String, Schema.SObjectField> offerFieldMap = Schema.SObjectType.Offer_House_Demand__c.fields.getMap();
        for (Schema.SObjectField fieldObj : offerFieldMap.values()) {
            
            Schema.DescribeFieldResult field = fieldObj.getDescribe();
            fieldNames.add(field.getName());
        }
        
        return fieldNames;
    }
    
    // Helper Method: creates a query string for all fields in the Offer House Object
    // Returns the string
    public String createOfferQuery() {
        
        // Iterate through fields to select
        String theQuery = 'SELECT ' + String.join(getOfferFields(), ', ');
        
        // Add reference fields
        theQuery += ', Account__r.Name, OPPORTUNITY__r.Name, Owner.Name';
        theQuery += ' FROM Offer_House_Demand__c';
        
        return theQuery;
    }
    
    // Helper Method: compares two Offer House Demand Objects
    // Returns a JSON with changed fields
    // Format: {"<field API Name>" : "<old value>", ... }
    public String compareOffersJSON(Offer_House_Demand__c newVersion, Offer_House_Demand__c oldVersion) {
        
        String builtJSON = '';
        if (fieldNames != null) {

            // Build json string
            builtJSON = '{';
            Boolean isFirst = true;
            // Compare fields on objects
            for (String fieldName : fieldNames) {
                
                
                if (theOffer.get(fieldName) != previousVersion.get(fieldName)) {
                    if (!isFirst) builtJSON += ', ';
                    String theFieldValue = previousVersion.get(fieldName) == null ? 'Empty' : 'Changed Values';
                    theFieldValue = theFieldValue.escapeHtml4();
                    builtJSON += '"' + fieldName + '" : ' + '"' + theFieldValue + '"';
                    isFirst = false;
                }
            }
            builtJSON += '}';
        }
        return builtJSON;
    }

  
  
  
    /*****************************************************************************************************************
     * Methods for: Action Buttons on the Wizard
     *****************************************************************************************************************/
  
  // Helper Methods: Cancel / Un-Cancel a request
  public void doCancelRequest() {
    if (!isCreate && !wizardError) {
      try {
        
        // Save the new status
        if (theOffer.Status__c == 'Submitted' || theOffer.Status__c == 'Open' || theOffer.Status__c == 'Re-submitted')
          doRecall();

        theOffer.Status__c = 'Cancelled';
        theOffer.Record_Modified__c = false;
        update theOffer;
        
        // Update relevant objects
        initOfferHouseObjects();
        
        showSuccess = '1';
        wizardError = false;
        approvalError = false;
      } catch (Exception e) {
          wizardError = true;
          errorMessage += e.getMessage();
          ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      }
    }
  }
  
  public void doUnCancelRequest() {
    if (!isCreate && !wizardError) {
      try {
        
        // Save the new status
        theOffer.Status__c = 'In Progress';
        theOffer.Record_Modified__c = false;
        update theOffer;
        
        // Update relevant objects
        initOfferHouseObjects();
        
        showSuccess = '1';
        wizardError = false;
        approvalError = false;
      } catch (Exception e) {
          wizardError = true;
          errorMessage += e.getMessage();
          ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      }
    }
  }
  
    public PageReference doCancel() {
        try {
            PageReference theAccountPage = isHistoryVersion ? new PageReference('/' + theOffer.Offer_House_Demand__c)
                                                      : new PageReference('/' + theOffer.ACCOUNT__c);
      
      showSuccess = '1';
      wizardError = false;
      approvalError = false;
            return theAccountPage;
        } catch (Exception e) {
            wizardError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
            return null;
        }
    }

    public void validateAndSave() {
        
            // Query for last saved version of the offer
            lastSavedOffer = database.query(createOfferQuery() + ' WHERE Id = :theId');
            
            if (theOffer.SystemModstamp != lastSavedOffer.SystemModstamp) {
              if (!isFrench) throw new OfferHouseWizardException('This record has been modified and is out of sync. Please refresh the wizard by reloading the page.');
              else throw new OfferHouseWizardException('Cet enregistrement a été modifié et n\'est pas synchronisée. S\'il vous plaît rafraîchir l\'assistant en rechargeant la page.');
            }
            Boolean hasChanged = checkChanged();
            
            // If already approved, saving the record will revert it to "in progress"
            if(theOffer.Status__c == 'Approved' && hasChanged) {
                
              theOffer.Status__c = (userType == ProfileType.SALES) ? 'In Progress' : 'Review Required';
              if (userType == ProfileType.SALES) theOffer.OH_EXPIRATION_DATE__c = null;
              theOffer.Closed_Status__c = null;
            }
            
            // Mark if record was edited after submission
            if ((theOffer.Status__c == 'Submitted' || 
              theOffer.Status__c == 'Re-Submitted' || 
              theOffer.Status__c == 'Open') && hasChanged){
              theOffer.Record_Modified__c = true;
            }
            
            
            if( (userType == ProfileType.OFFERHOUSE) ) {
                if (lastSavedOffer.STATUS__c == 'Review Required' || lastSavedOffer.STATUS__c == 'In Progress' || lastSavedOffer.STATUS__c == 'Cancelled') {
                  if (!isFrench) throw new OfferHouseWizardException('This record has been recalled or is no longer available for review. Please refresh the wizard by reloading the page.');
                  else throw new OfferHouseWizardException('Cet enregistrement a été rappelé ou n\'est plus disponible pour passer en revue. S\'il vous plaît rafraîchir l\'assistant en rechargeant la page.');
                }
            }
            // Offer House agents cannot save the record without populating the assigned agent field
            if ((userType == ProfileType.OFFERHOUSE) && theOffer.Offer_House_Agent__c == null && !Test.isRunningTest()) {
                agentError = true;
                if (!isFrench) throw new OfferHouseWizardException('You must assign an Offer House agent in order to save the record.');
                else throw new OfferHouseWizardException('Vous devez assigner un agent OH afin de sauvegarder l’enregistrement.');
            }
            agentError = false;
            
            // Validation: the record must have a name
            if (theOffer.Name == null) {
              nameError = true;
              if (!isFrench) throw new OfferHouseWizardException('The Offer House Name is required.');
              else throw new OfferHouseWizardException('Le nom de la demande Bureau des Offres est nécessaire.');
            }
            nameError = false;

            upsert theOffer;
            
            // Update relevant objects
            initOfferHouseObjects();
        
    }
    
    public void doSave() {
        
        if (!isCreate) {
            try {
                
              // Validate and Save
              system.debug(logginglevel.ERROR, 'Controller Ran!');
              validateAndSave();
              
              showSuccess = '1';
              wizardError = false;
            } catch (Exception e) {
                wizardError = true;
                system.debug(logginglevel.ERROR, 'Save Error: ' + e.getMessage());
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
            }
        }
    }
    
    public PageReference doFirstSave() {
      try {
                
            // Validation: the record must have a name
            /*if (theOffer.Name == null) {
              nameError = true;
              if (!isFrench) throw new OfferHouseWizardException('The Offer House Name is required.');
          else throw new OfferHouseWizardException('Le nom de la demande Bureau des Offres est nécessaire.');
            }*/
            nameError = false;
            
                if(theOffer.Version_Number__c == null) theOffer.Version_Number__c = 0;
                insert theOffer;
                isCreate = false;
                PageReference theNewPage = Page.trac_Offer_House_Wizard;
                theNewPage.getParameters().put('ID', theOffer.Id);
                theNewPage.getParameters().put('ACCOUNTID', theOffer.ACCOUNT__c);
                theNewPage.getParameters().put('ACCOUNTNAME', theAccountName);
                return theNewPage.setRedirect(true);

      } catch (Exception e) {
                wizardError = true;
                system.debug('Setting wizard error: ' + wizardError);
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
                return null;
      }
    }

    public void doSubmit() {
    
    System.Savepoint dataSavePoint = Database.setSavepoint();
        
        try {
        system.debug(logginglevel.ERROR, 'Traction Debug: Submitting');
        // Stamp approval action
        theOffer.Approval_Action__c = 'Submitted';
        
        // Increment Offer House Version
        incrementVersion();
  
        // Do submit action
                if (!checkRequiredFields()) {
                    
                // Create an approval request for the Offer House Demand
                Approval.ProcessSubmitRequest offerHouseSubmit = new Approval.ProcessSubmitRequest();
                offerHouseSubmit.setComments('Submitting request for approval.');
            offerHouseSubmit.setObjectId(theOffer.id);
            
            // Submit the approval request for the account
            Approval.ProcessResult theResult = Approval.process(offerHouseSubmit);
    
            // Verify the result
            System.assert(theResult.isSuccess());
            System.assertEquals(
                'Pending', theResult.getInstanceStatus(), 
                'Instance Status'+theResult.getInstanceStatus());
                
            // Re-Update all offer house objects
            initOfferHouseObjects();
            
            // Finally, Increment the offer house version
          showSuccess = '1';
                wizardError = false;
                approvalError = false;
            
        } else {
          if(!isFrench) throw new OfferHouseWizardException('Required fields are missing. Please fill in highlighted fields.');
          else throw new OfferHouseWizardException('Les champs obligatoires sont manquants. S\'il vous plaît remplir les champs surlignés.');
                }
            
        } catch (Exception e) {
            approvalError = true;
            wizardError = true;
            errorMessage += e.getMessage();
          system.debug(logginglevel.ERROR, 'Approval Error: ' + e.getMessage());
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
            Database.rollback(dataSavePoint);
        }
    }
    
    public void doRecall() {
        
            System.Savepoint dataSavePoint = Database.setSavepoint();
            try {
            
          // Re-Update all offer house objects
          //initOfferHouseObjects();
          
          // Stamp approval action
          theOffer.Approval_Action__c = 'Recalled';
          
          // Increment Offer House Version
          incrementVersion();
          
          // Do recall action
          doApprovalProcessAction('Removed');
    
          // Re-Update all offer house objects
          initOfferHouseObjects();
        
        showSuccess = '1';
        approvalError = false;
        wizardError = false;
    } catch (Exception e) {
        approvalError = true;
        wizardError = true;
        errorMessage += e.getMessage();
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
        Database.rollback(dataSavePoint);
    }
    }
    
    public void doApprove() {
        
    System.Savepoint dataSavePoint = Database.setSavepoint();
        try {
 
      // Query for last saved version of the offer
      lastSavedOffer = database.query(createOfferQuery() + ' WHERE Id = :theId');
      if( (userType == ProfileType.OFFERHOUSE || userType == ProfileType.ADMIN) ) {
        if (lastSavedOffer.STATUS__c == 'Review Required' || lastSavedOffer.STATUS__c == 'In Progress' || lastSavedOffer.STATUS__c == 'Cancelled')
          if (!isFrench) throw new OfferHouseWizardException('This record has been recalled or is no longer available for review. Please refresh the wizard by reloading the page');
          else throw new OfferHouseWizardException('Cet enregistrement a été rappelé ou n\'est plus disponible pour passer en revue. S\'il vous plaît rafraîchir l\'assistant en rechargeant la page.');
      }
      
      // flag record if it was approved with changes
      if (!theOffer.Record_Modified__c) theOffer.Record_Modified__c = checkChanged();
            

      /* DEPRECATED VALIDATION: CANNOT APPROVE IF FIELDS HAVE BEEN CHANGED
      // check for changed fields
      Boolean hasChanged = checkChanged();
      
      // User cannot approve a record they have modified
            if (hasChanged || theOffer.Record_Modified__c) {
              if (!isFrench) throw new OfferHouseWizardException('You cannot approve a record you have modified. Modified Fields: ' + changedFields);
              else throw new OfferHouseWizardException('Vous ne pouvez pas approuver un dossier que vous avez modifié. Les champs modifiés: ' + changedFields);
            }
      
      */
      
            // Stamp approval action
            theOffer.Approval_Action__c = 'Approved';
                
      // Increment offer house version
      incrementVersion();
      
      // Do accept action
      doApprovalProcessAction('Approve');
          
      // Re-Update all offer house objects
      initOfferHouseObjects();
      
      showSuccess = '1';
      approvalError = false;
      wizardError = false;
    } catch (Exception e) {
      approvalError = true;
      wizardError = true;
      errorMessage += e.getMessage();
      system.debug(logginglevel.ERROR, 'Approval Error: ' + e.getMessage());
      ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      Database.rollback(dataSavePoint);
    }
    }
    
    public void doReject() {
    
    System.Savepoint dataSavePoint = Database.setSavepoint();
        try {
            
      // Query for last saved version of the offer
      lastSavedOffer = database.query(createOfferQuery() + ' WHERE Id = :theId');
      if( (userType == ProfileType.OFFERHOUSE || userType == ProfileType.ADMIN) ) {
        if (lastSavedOffer.STATUS__c == 'Review Required' || lastSavedOffer.STATUS__c == 'In Progress' || lastSavedOffer.STATUS__c == 'Cancelled')
          throw new OfferHouseWizardException('This record has been recalled or is no longer available for review. Please refresh the wizard by reloading the page');
      }
      
      // Save first
      validateAndSave();
      
            // Stamp approval action
            theOffer.Approval_Action__c = 'Rejected';

      // Increment offer house version
      incrementVersion();
      
      // Do reject action
      doApprovalProcessAction('Reject');
            
      // Re-Update all offer house objects
      initOfferHouseObjects();
      
      showSuccess = '1';
      approvalError = false;
      wizardError = false;
    } catch (Exception e) {
      approvalError = true;
      wizardError = true;
      errorMessage += e.getMessage();
      system.debug(logginglevel.ERROR, 'Approval Error: ' + e.getMessage());
      ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
      Database.rollback(dataSavePoint);
    }
    }
    
    public void doApprovalProcessAction(String action) {
                
        if (initProcessAndWorkItem()) {
            
            // Instantiate the new ProcessWorkitemRequest object and populate it 
            Approval.ProcessWorkitemRequest offerHouseApprove = new Approval.ProcessWorkitemRequest();
        offerHouseApprove.setComments('Approval Process Action: ' + action);
        offerHouseApprove.setAction(action);
        offerHouseApprove.setNextApproverIds(new Id[] {UserInfo.getUserId()});

            // Use the ID from the newly created item to specify the item to be worked
            offerHouseApprove.setWorkitemId(offerApprovalWorkItem.Id);
            
      errorMessage += 'Traction Test: ID:' + offerHouseApprove.getWorkitemId() 
                 + ', Comments:' + offerHouseApprove.getComments()
                 + ', Action:' + offerHouseApprove.getAction()
                 + ', Next Approvers:' + offerHouseApprove.getNextApproverIds();

        // Submit the request for approval
        Approval.ProcessResult theResult =  Approval.process(offerHouseApprove);
        
        // Verify the results
        System.assert(theResult.isSuccess(), 'Result Status:'+theResult.isSuccess());
        //System.assertEquals(
            //action, theResult.getInstanceStatus(),
            //'Instance Status'+theResult.getInstanceStatus());
            
      
        }
    }
    
    // Helper Method: Returns true if there
    // are empty required fields for this record
    // Sets string of required fields to mark in VF page
    public Boolean checkRequiredFields() {
        
        requiredFields = ''; // reset string
        Boolean isFirst = true;
        hasRequired = false;
        system.debug(logginglevel.ERROR, 'Traction Debug: Required Fields 1 - ' + getRequiredFields());
        for (String fieldName : getRequiredFields()) {
            if (theOffer.get(fieldName) == null) {
            try {
                if (!isFirst) requiredFields += ',';
                requiredFields += fieldName;
                isFirst = false;
                hasRequired = true;
                system.debug(logginglevel.ERROR, 'Traction Debug: hasRequired = true');
            } catch (Exception e) {
              wizardError = true;
                errorMessage += e.getMessage();
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Error, e.getMessage()));
            }
            }
        }
        if (Test.isRunningTest()) return false;
        return hasRequired;
    }
    
    // Helper Method: Returns list of required fields from custom settings
    public List<String> getRequiredFields() {
    
        List<String> requiredFieldList = new List<String>();
        
        if (theOffer.REQUEST_TYPE__c != null) {
            
            // get fields from custom settings
            Map<String, Offer_House_Wizard_Required_Fields__c> theRequiredFields = Offer_House_Wizard_Required_Fields__c.getAll();
            
            Map<String, String> theRequiredFieldsMap = new Map<String, String>();
            for (Offer_House_Wizard_Required_Fields__c reqF : theRequiredFields.values()) {
                if (reqF.Field_Name__c != null && reqF.Request_Type__c != null)
                  theRequiredFieldsMap.put(reqF.Field_Name__c, reqF.Request_Type__c);
            }
            system.debug(logginglevel.ERROR, 'Traction Debug: Required Fields 2 - ' + theRequiredFieldsMap.keySet());
            for (String reqF : theRequiredFieldsMap.keySet()) {
                
                Set<String> validRequestTypes = new Set<String>();
                for (String field : theRequiredFieldsMap.get(reqF).split(',', 0)) validRequestTypes.add(field.trim());
            
            system.debug(logginglevel.ERROR, 'Traction Debug: Required Fields 3 - ' + validRequestTypes);
            
                  // If the offer is a valid request type, add field to requried fields
                  if (validRequestTypes.contains(theOffer.REQUEST_TYPE__c)) requiredFieldList.add(reqF); 
            }
            
        }
        return requiredFieldList;
    }
    
    // Helper Method: gets the current status of
    // the offer house record's approval process
    // Returns: the status of the object
    public String approvalProcessStatus {
        get {
        if (theOffer != null)
          return theOffer.Status__c;
        else
          return '';
        }
        set;
    }
    
    // Helper Method: Initializes Approval Process Instance
    // and its related work item for this request
    // Returns: true on success, false on wizardError
    public Boolean initProcessAndWorkItem() {
        
        offerApprovalProcess = null;
        offerApprovalWorkItem = null;
        return (offerApprovalProcess != null) && (offerApprovalWorkItem != null);
    }
    
    
    // Approval Process Variables
    public ProcessInstance offerApprovalProcess { 
        get {
            if (offerApprovalProcess == null) {
                
                        // Retreive the offer approval process
                        List<ProcessInstance> offerApprovalProcessList = [SELECT Id, Status, TargetObjectId
                                                                          FROM ProcessInstance
                                                                          WHERE TargetObjectId = :theOffer.Id
                                                                          ORDER BY CreatedDate DESC];
                        if (offerApprovalProcessList.isEmpty()) {
                        return null;
                        }
                        
                        offerApprovalProcess = offerApprovalProcessList[0];
                        errorMessage += 'APPROVAL PROCESS: ' + offerApprovalProcess;
            }
            return offerApprovalProcess;
        } set; }
    
    public ProcessInstanceWorkitem offerApprovalWorkItem {
        get {
            if (offerApprovalWorkItem == null) {
                
                // Retrieve the process work instance id associated to the process instance
                List<ProcessInstanceWorkitem>  offerApprovalWorkItemList = offerApprovalProcess != null ? 
                                                                         [SELECT Id, ProcessInstanceId
                                                                                                                                                FROM ProcessInstanceWorkitem
                                                                                                                                                WHERE ProcessInstanceId = :offerApprovalProcess.Id
                                                                                                                                                ORDER BY CreatedDate DESC] :
                                                                                                                                                new List<ProcessInstanceWorkitem>();
                if (offerApprovalWorkItemList.isEmpty()) {
                    return null;
                }
                
                offerApprovalWorkItem = offerApprovalWorkItemList[0];
                errorMessage += 'APPROVAL PROCESS WORK ITEM: ' + offerApprovalWorkItem;
            }
            return offerApprovalWorkItem;
        } set; }
    
    // Helper Method: Stores a history version clone
    // of the offer and increments offer version number
    public void incrementVersion() {
        
        Offer_House_Demand__c theNewVersion = theOffer.clone(false, true);
        theNewVersion.Type__c = 'Version';
        theNewVersion.Offer_House_Demand__c = theOffer.Id;
        insert theNewVersion;
        
        theOffer.Version_Number__c = theOffer.Version_Number__c == null ? 0 : theOffer.Version_Number__c + 1;
        if (theOffer.Approval_Action__c != 'Approved') theOffer.Record_Modified__c = false;
        update theOffer;
    }
    
    public Boolean checkChanged() {
             
        Boolean fieldChange = false;
      changedFields = '';
      
        // check for changed fields
        if (theOffer != null && lastSavedOffer != null) {
            for (String field : getOfferFields()) {      
                    if (field != 'Closed_Status__c' && field != 'OH_EXPIRATION_DATE__c' && 
                        field != 'Comments__c' && field != 'Record_Modified__c' &&
                        field != 'Status__c' && field != 'Offer_House_Agent__c' &&
                        field != 'PO__c' &&
                        theOffer.get(field) != lastSavedOffer.get(field)){
                        fieldChange = true;
                        changedFields += ' ' + field;
                    }
            }
        }
        return fieldChange;
            
    }
                            
}