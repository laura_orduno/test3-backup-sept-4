@isTest
private class OrdrTechnicalAvailabilityManagerTest {
	
    @isTest
    private static void testCheckProductTechnicalAvailability() {
        
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
        OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
        OrdrTestDataFactory.createOrderingWSCustomSettings(OrdrConstants.CREDENTIAL_USERNAME, 'abc');
        OrdrTestDataFactory.createOrderingWSCustomSettings(OrdrConstants.CREDENTIAL_PASSWORD, 'abcde');
            //Ordering_WS__c wsUserName =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_USERNAME);
              //  Ordering_WS__c wsPassword =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_PASSWORD); 
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        // Call the method that invokes a callout
        List<OrdrTechnicalAvailabilityManager.ProductCategory> categories = OrdrTechnicalAvailabilityManager.checkProductTechnicalAvailability(serviceAddressId);
        // Verify that a fake result is returned
        System.assertEquals(4, categories.size());
        Test.stopTest();
        
    }
    
    @isTest
    private static void testCheckProductTechnicalAvailability_Exception() {
        
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
        OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
          OrdrTestDataFactory.createOrderingWSCustomSettings(OrdrConstants.CREDENTIAL_USERNAME, 'abc');
        OrdrTestDataFactory.createOrderingWSCustomSettings(OrdrConstants.CREDENTIAL_PASSWORD, 'abcde');  
        Test.startTest();
        try {
            Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl(false));
            // Call the method that invokes a callout
            List<OrdrTechnicalAvailabilityManager.ProductCategory> categories = OrdrTechnicalAvailabilityManager.checkProductTechnicalAvailability(serviceAddressId);
            System.assert(false, 'Exception expected');
        }
        catch(Exception e) {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        Test.stopTest();
        
    }    
    
    @isTest
    private static void testCheckWirelineTnPortability() {
        
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
        OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
            OrdrTestDataFactory.createOrderingWSCustomSettings(OrdrConstants.CREDENTIAL_USERNAME, 'abc');
        OrdrTestDataFactory.createOrderingWSCustomSettings(OrdrConstants.CREDENTIAL_PASSWORD, 'abcde');
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        // Call the method that invokes a callout
        OrdrTechnicalAvailabilityManager.TnPortability tnPortability = OrdrTechnicalAvailabilityManager.checkWirelineTnPortability(serviceAddressId, '6046956229');
        // Verify that a fake result is returned
        System.assertEquals('true', tnPortability.portableIndicator);
        Test.stopTest();
        
    }  
    

    @isTest
    private static void testCheckWirelineTnPortability_Exception() {
        
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
        OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
            OrdrTestDataFactory.createOrderingWSCustomSettings(OrdrConstants.CREDENTIAL_USERNAME, 'abc');
        OrdrTestDataFactory.createOrderingWSCustomSettings(OrdrConstants.CREDENTIAL_PASSWORD, 'abcde');
        Test.startTest();
        try {
            Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl(false));
            // Call the method that invokes a callout
            OrdrTechnicalAvailabilityManager.TnPortability tnPortability = OrdrTechnicalAvailabilityManager.checkWirelineTnPortability(serviceAddressId, '6046956229');
            System.assert(false, 'Exception expected');
        }
        catch(Exception e) {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        Test.stopTest();
        
    }      
    
    @isTest
    private static void testCheckWirelessTnPortability() {

        OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
        OrdrTestDataFactory.createOrderingWSCustomSettings(OrdrConstants.CREDENTIAL_USERNAME, 'abc');
        OrdrTestDataFactory.createOrderingWSCustomSettings(OrdrConstants.CREDENTIAL_PASSWORD, 'abcde');
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        // Call the method that invokes a callout
        OrdrTechnicalAvailabilityManager.TnPortability tnPortability = OrdrTechnicalAvailabilityManager.checkWirelessTnPortability('6046956229', 'EXT_2H', '1');
        // Verify that a fake result is returned
        System.assertEquals('true', tnPortability.portableIndicator);
        Test.stopTest();
        
    }   

    
    @isTest
    private static void testCheckWirelessTnPortability_Exception() {

        OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
        OrdrTestDataFactory.createOrderingWSCustomSettings(OrdrConstants.CREDENTIAL_USERNAME, 'abc');
        OrdrTestDataFactory.createOrderingWSCustomSettings(OrdrConstants.CREDENTIAL_PASSWORD, 'abcde');
        Test.startTest();
        try {
            Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl(false));
            // Call the method that invokes a callout
            OrdrTechnicalAvailabilityManager.TnPortability tnPortability = OrdrTechnicalAvailabilityManager.checkWirelessTnPortability('6046956229', 'EXT_2H', '1');
            System.assert(false, 'Exception expected');
        }
        catch(Exception e) {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        Test.stopTest();
        
    }   
    
    
    @isTest
    private static void testGetServicePath() {

        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
        OrdrTestDataFactory.createOrderingWSCustomSettings('ClearanceCheckEndpoint', 'https://xmlgwy-pt1.telus.com:9030/pt01/RMO/InventoryMgmt/FindResource_v2_0_vs1_PT');
        OrdrTestDataFactory.createOrderingWSCustomSettings(OrdrConstants.CREDENTIAL_USERNAME, 'abc');
        OrdrTestDataFactory.createOrderingWSCustomSettings(OrdrConstants.CREDENTIAL_PASSWORD, 'abcde');
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        // Call the method that invokes a callout
        List<OrdrTechnicalAvailabilityManager.ServicePath> paths = OrdrTechnicalAvailabilityManager.getServicePath(serviceAddressId);
        // Verify that a fake result is returned
        System.assertEquals(3, paths.size());
        Test.stopTest();
        
    } 
    @isTest
    private static void TestGetGponDropStatus0(){
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
        OrdrTestDataFactory.createOrderingWSCustomSettings('QueryResourceWirelineEndpoint', 'https://xmlgwy');
        OrdrTestDataFactory.createOrderingWSCustomSettings(OrdrConstants.CREDENTIAL_USERNAME, 'abc');
        OrdrTestDataFactory.createOrderingWSCustomSettings(OrdrConstants.CREDENTIAL_PASSWORD, 'abcde');
        Test.startTest();
        OrdrWsTpMockImpl ob=new OrdrWsTpMockImpl();
        ob.throwErr=true;
        Test.setMock(WebServiceMock.class, ob);
        try{
          OrdrTechnicalAvailabilityManager.getGponDropStatus(serviceAddressId);  
        }catch(Exception e){}
        
        Test.stopTest();
    }
    @isTest
    private static void TestGetGponDropStatus(){
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
        OrdrTestDataFactory.createOrderingWSCustomSettings('QueryResourceWirelineEndpoint', 'https://xmlgwy');
        OrdrTestDataFactory.createOrderingWSCustomSettings(OrdrConstants.CREDENTIAL_USERNAME, 'abc');
        OrdrTestDataFactory.createOrderingWSCustomSettings(OrdrConstants.CREDENTIAL_PASSWORD, 'abcde');
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        OrdrTechnicalAvailabilityManager.getGponDropStatus(serviceAddressId);
        Test.stopTest();
    }
    
    @isTest
    private static void testCheckWirelessTnPortability_Async() {

        OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
        OrdrTestDataFactory.createOrderingWSCustomSettings(OrdrConstants.CREDENTIAL_USERNAME, 'abc');
        OrdrTestDataFactory.createOrderingWSCustomSettings(OrdrConstants.CREDENTIAL_PASSWORD, 'abcde');
        Test.startTest();
        try {
            Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
            // Call the method that invokes a callout
            OrdrWsCheckGenericTechnicalAvailability.checkGenericTechnicalAvailabilityFault1_element obj1 = new OrdrWsCheckGenericTechnicalAvailability.checkGenericTechnicalAvailabilityFault1_element();
            OrdrWsCheckGenericTechnicalAvailability.checkGenericTechnicalAvailabilityFault2_element obj2 = new OrdrWsCheckGenericTechnicalAvailability.checkGenericTechnicalAvailabilityFault2_element();
            OrdrWsCheckGenericTechnicalAvailability.AsynCheckGenericTechnicalAvailabilitySOAPPort chkObj = new OrdrWsCheckGenericTechnicalAvailability.AsynCheckGenericTechnicalAvailabilitySOAPPort();
            TpFulfillmentResourceOrderV3.ResourceOrder resourceOrder = new TpFulfillmentResourceOrderV3.ResourceOrder();
            chkObj.beginCheckGenericTechnicalAvailability(null, resourceOrder);
        }
        catch(Exception e) {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        Test.stopTest();
        
    }
}