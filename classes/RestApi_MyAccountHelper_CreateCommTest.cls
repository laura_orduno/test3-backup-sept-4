@isTest
public class RestApi_MyAccountHelper_CreateCommTest {
    
    private static final String COMMENT_BODY = 'This is the comment body';
    private static final String MOCKCASE = '500c000000AW1s8AAD';

    private static Case myCase;
    private static RestApi_MyAccountHelper_CreateComment createCommentObj;
    private static RestApi_MyAccountHelper_CreateComment.RequestUser ru;
    private static RestApi_MyAccountHelper_CreateComment.RequestComment rc;
    private static RestApi_MyAccountHelper_CreateComment.CreateCommentResponse resp;

    private static void setup() {
        trac_TriggerHandlerBase.blockTrigger = true;

        RestApi_MyAccountHelper_TestUtils.MASRUserBundle bundle = RestApi_MyAccountHelper_TestUtils.createMASREnabledUser();

    	myCase = RestApi_MyAccountHelper_TestUtils.createCase(bundle.banAccount, bundle.u);
    	insert myCase;

    	ru = createRequestUser();
        rc = createRequestComment(myCase);
        trac_TriggerHandlerBase.blockTrigger = false;
    }


    @isTest
    private static void testRequest() {
    	setup();
        createRequest(ru, rc);
    
        Test.startTest();
        createCommentObj = new RestApi_MyAccountHelper_CreateComment(RestContext.request.requestBody.toString()); 
        resp = createCommentObj.response;          
        Test.stopTest();
        
        System.debug('Response : ' + resp);
        
        //TODO: assert the fields of resp object.
        //TOOD: assert that the CaseComment was inserted.
    }

    @isTest static void testNoUser(){
        setup();
        createRequest(null, rc);
        try {
            Test.startTest();
            createCommentObj = new RestApi_MyAccountHelper_CreateComment(RestContext.request.requestBody.toString()); 
            resp = createCommentObj.response;          
            Test.stopTest();
            System.assert(false, 'error should be thrown');
        } catch (Exception e){
            System.assert(true, 'error was thrown');
        }
    }

    @isTest static void testNoUUID(){
        setup();
        ru.UUID = null;
        createRequest(ru, rc);
        try {
            Test.startTest();
            createCommentObj = new RestApi_MyAccountHelper_CreateComment(RestContext.request.requestBody.toString()); 
            resp = createCommentObj.response;          
            Test.stopTest();
            System.assert(false, 'error should be thrown');
        } catch (Exception e){
            System.assert(true, 'error was thrown');
        }
    }

    @isTest static void testNoCommentBody(){
        setup();
        rc.body = null;
        createRequest(ru, rc);
        try {
            Test.startTest();
            createCommentObj = new RestApi_MyAccountHelper_CreateComment(RestContext.request.requestBody.toString()); 
            resp = createCommentObj.response;          
            Test.stopTest();
            System.assert(false, 'error should be thrown');
        } catch (Exception e){
            System.assert(true, 'error was thrown');
        }
    }

    @isTest static void testNoCommentParent(){
        setup();
        rc.parentId = null;
        createRequest(ru, rc);
        try {
            Test.startTest();
            createCommentObj = new RestApi_MyAccountHelper_CreateComment(RestContext.request.requestBody.toString()); 
            resp = createCommentObj.response;          
            Test.stopTest();
            System.assert(false, 'error should be thrown');
        } catch (Exception e){
            System.assert(true, 'error was thrown');
        }
    }

    @isTest static void testInvalidCase(){
        setup();
        rc.parentId = MOCKCASE;
        createRequest(ru, rc);
        try {
            Test.startTest();
            createCommentObj = new RestApi_MyAccountHelper_CreateComment(RestContext.request.requestBody.toString()); 
            resp = createCommentObj.response;          
            Test.stopTest();
            System.assert(false, 'error should be thrown');
        } catch (Exception e){
            System.assert(true, 'error was thrown');
        }
    }

    @isTest static void testInvalidUUID(){
        setup();
        ru.uuid = RestApi_MyAccountHelper_TestUtils.UUIDNOT;
        createRequest(ru, rc);
        try {
            Test.startTest();
            createCommentObj = new RestApi_MyAccountHelper_CreateComment(RestContext.request.requestBody.toString()); 
            resp = createCommentObj.response;          
            Test.stopTest();
            System.assert(false, 'error should be thrown');
        } catch (Exception e){
            System.assert(true, 'error was thrown');
        }
    }




    private static void createRequest(RestApi_MyAccountHelper_CreateComment.RequestUser ru, RestApi_MyAccountHelper_CreateComment.RequestComment rcc) {
    	RestApi_MyAccountHelper_CreateComment.CreateCommentRequestObject commentRequest = new RestApi_MyAccountHelper_CreateComment.CreateCommentRequestObject();
    	commentRequest.user = ru;
    	commentRequest.comment = rc;
    	commentRequest.type = RestApi_MyAccountHelper_TestUtils.REQUEST_TYPE_CREATE_COMMENT;
    	RestRequest request = new RestRequest();
        request.requestUri ='/services/apexrest/RestApi_MyAccount/';
        request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(JSON.serialize(commentRequest));
        RestContext.request = request;
    }

    private static RestApi_MyAccountHelper_CreateComment.RequestUser createRequestUser(){
    	RestApi_MyAccountHelper_CreateComment.RequestUser ru = new RestApi_MyAccountHelper_CreateComment.RequestUser();
    	ru.uuid = RestApi_MyAccountHelper_TestUtils.UUID;
    	return ru;
    }

    private static RestApi_MyAccountHelper_CreateComment.RequestComment createRequestComment(Case testCase){
    	RestApi_MyAccountHelper_CreateComment.RequestComment comment = new RestApi_MyAccountHelper_CreateComment.RequestComment();
    	comment.body = COMMENT_BODY;
    	if (testCase != null) {
        	comment.parentId = testCase.Id;
        }
		return comment; 
    }
}