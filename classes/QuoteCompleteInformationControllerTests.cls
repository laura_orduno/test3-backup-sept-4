@isTest
private class QuoteCompleteInformationControllerTests {
	private static Web_Account__c account;
	private static SBQQ__Quote__c quote;
	private static SBQQ__QuoteLine__c line;
	private static AdditionalInformation__c addlInfo;

	static testMethod void test() {
		setUp();
		ApexPages.currentPage().getParameters().put('qid', quote.Id);
		ApexPages.currentPage().getParameters().put('stage', QuoteStatus.SENT_FOR_CREDIT_CHECK);
		
		QuoteCompleteInformationController controller = new QuoteCompleteInformationController();
		
        Map<SBQQ__QuoteLine__c, List<QuoteConfigFieldSetModel>> addlInfoWrapperByQuoteLine = 
                QuoteCustomConfigurationService.buildQuoteConfigFieldSetModelByQuoteLine(QuoteStatus.SENT_FOR_CREDIT_CHECK, 
                new AdditionalInformation__c[]{addlInfo}, new SBQQ__QuoteLine__c[]{line});
        controller.buildProductAddlInfoWrapperList(addlInfoWrapperByQuoteLine);
		controller.updateFieldsState();
		controller.quoteLines = new SBQQ__QuoteLine__c[]{line};
		controller.onSave();
		controller.onCancel();
	}

	private static void setUp() {
		account = new Web_Account__c(Name='UnitTest');
		insert account;
		
		Product2 p = new Product2(Name='Test',SBQQ__ConfigurationFields__c='Service_Term__c',Network__c='CDMA',Category__c='Devices');
		insert p;
		
		Opportunity opp = new Opportunity(Name='Test',Web_Account__c=account.Id,CloseDate=System.today(),StageName='Prospecting');
		insert opp;
		
		quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
		quote.Rate_Band__c = 'F';
		quote.Province__c = 'BC';
		insert quote;
		
		SBQQ__ProductOption__c productOption = new SBQQ__ProductOption__c(SBQQ__Number__c=1,SBQQ__OptionalSKU__c=p.Id);
		insert productOption;
		
		line = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,SBQQ__Bundle__c=true,SBQQ__Product__c=p.Id,Category__c='Devices',SBQQ__ProductOption__c=productOption.Id);
		insert line;
	
		addlInfo = new AdditionalInformation__c(Quote__c=quote.Id, Quote_Line__c=line.Id);
		insert addlInfo;
	}

}