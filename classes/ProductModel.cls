/**
 * Model implementation for working with products including associated options, features and constraints.
 * 
 * @author Max Rudman
 * @since 7/23/2009
 */
public class ProductModel {
	public Integer level {get; set;}
	public Id id {get; set;}
	public List<ProductFeatureModel> features {get; private set;}
	public ProductFeatureModel otherFeature {get; private set;}
	public QuoteVO.LineItem lineItem {get; set;}
	
	// Convenience properties
	
	//public String json {get{return toJSON();}}
	public Boolean configurationRequired {get{return vo.isConfigurationRequired();}}
	public Integer featureCount {get{return features.size();}}
	public String configurationFormTitle {get{return vo.record.SBQQ__ConfigurationFormTitle__c;}}
	public String configurationFormKey {get{return 'configurationForm';}}
	
	//Custom Configuration
	public Boolean customConfigurationRequired{get{return vo.record.SBQQ__CustomConfigurationRequired__c;}}
	public String customConfigurationPage{get{return vo.record.SBQQ__CustomConfigurationPage__c;}}
	
	// Internal state
	private ProductVO vo;
	private transient Map<String,ProductFeatureModel> featureRowsById;
	private transient Map<Id,ProductOptionModel> optionRowsById;
	private ProductOptionSelectionModel selections;
	
	
	public ProductModel(ProductVO product) {
		this(product, 0);
	}
	
	/**
	 * Constructs a new ProductModel object from specified "configured" product and associated
	 * options. Options list is processed and categorized into 2 groups:
	 * <ol>
	 * <li>"Stand-alone" options.</li>
	 * <li>Options grouped by feature.</li>
	 * </ol>
	 */
	public ProductModel(ProductVO vo, Integer level) {
		System.assert(vo != null, 'ProductModel: VO is null');
		this.vo = vo;
		this.level = level;
		this.id = vo.record.Id;
		
		if (vo.features != null) {
			features = new List<ProductFeatureModel>();
			for (ProductVO.Feature feature : vo.features) {
				features.add(new ProductFeatureModel(this, feature));
			}
			
			List<ProductVO.Option> optionsWithNoFeature = vo.getOptionsWithoutFeature();
			if (!optionsWithNoFeature.isEmpty()) {
				otherFeature = new ProductFeatureModel(this);
				otherFeature.addOptions(optionsWithNoFeature);
				features.add(otherFeature);
			}
			
			for (Integer i=0;i<features.size();i++) {
				features[i].index = i;
			}
		}
	}
	
	public static ProductModel loadWithOptionPrices(Id id, Id accountId, Id pricebookId, String currencyCode) {
		ProductVO vo =
			ProductService.getInstance().findProductWithOptionPrices(id, accountId, pricebookId, currencyCode);
		return new ProductModel(vo);
	}
	
	public static ProductModel[] loadWithOptionPrices(Set<Id> ids, Id accountId, Id pricebookId, String currencyCode) {
		List<ProductModel> result = new List<ProductModel>();
		List<ProductVO> vos =
			ProductService.getInstance().findProductsWithOptionPrices(ids, accountId, pricebookId, currencyCode).values();
		for (ProductVO vo : vos) {
			result.add(new ProductModel(vo));
		}
		
		return result;
	}
	
	
	/* public static List<ProductModel> loadByIds(Set<Id> ids) {
		return loadByIds(ids, null, null, null);
	} */
	
	public static List<ProductModel> loadByIds(Set<Id> ids, Id accountId, Id pricebookId, String currencyCode) {
		List<ProductModel> result = new List<ProductModel>();
		List<ProductVO> vos =
			ProductService.getInstance().findProductsWithOptionsAndPrices(ids, accountId, pricebookId, currencyCode).values();
		for (ProductVO vo : vos) {
			result.add(new ProductModel(vo));
		}
		return result;
	}
	
	/* public static ProductModel loadById(Id id) {
		return loadByIds(new Set<Id>{id})[0];
	}*/
	
	public static ProductModel loadById(Id id, Id accountId, Id pricebookId, String currencyCode) {
		return loadByIds(new Set<Id>{id}, accountId, pricebookId, currencyCode)[0];
	}
	
	public Id getId() {
		return vo.record.Id;
	}
	
	public ProductVO getVO() {
		return vo;
	}
	
	public String getValidatorFunctionName() {
		return 'validateConfiguration_' + vo.record.Id;
	}
	
	public String getFeatureKeysEncoded() {
		List<String> keys = new List<String>();
		for (ProductFeatureModel feature : features) {
			keys.add(feature.getKey());
		}
		if (vo.hasConfigurationFields()) {
			keys.add(configurationFormKey);
		}
		return StringUtils.join(keys, ',', null);
	}
	
	public ProductFeatureModel getDefaultFeature() {
		if (!features.isEmpty()) {
			return features[0];
		}
		return null;
	}
	
	public ProductOptionSelectionModel getSelections() {
		return selections;
	}
	
	public void setSelections(ProductOptionSelectionModel selections) {
		this.selections = selections;
	}
	
	public void cloneOption(ProductOptionModel option) {
		//TODO: Implement cloneOption
	}
	
	public void deleteOption(ProductOptionModel option) {
		//TODO: Implement deleteOption
	}
	
	public List<ProductOptionModel> getAllOptions() {
		List<ProductOptionModel> result = new List<ProductOptionModel>();
		for (ProductFeatureModel feature : features) {
			for (ProductOptionModel orow : feature.options) {
				result.add(orow);
			}
		}
		return result;
	}
	
	/**
	 * Tests if the product represented by this model has optional products.
	 */
	public Boolean hasOptions() {
		return !vo.options.isEmpty();
	}
	
	/**
	 * Tests if the product represented by this model contains required options only.
	 */
	public Boolean hasRequiredOptionsOnly() {
		Boolean result = true;
		for (ProductOptionModel orow : getAllOptions()) {
			result = result && orow.required;
		}
		return result;
	}
	
	public Boolean isConfigurationRequired() {
		return !hasRequiredOptionsOnly();
	}
	
	/**
	 * Returns number of "stand-alone" optional products.
	 */
	public Integer getOtherOptionCount() {
		return vo.getOptionsWithoutFeature().size();
	}
	
	public ProductOptionModel getOptionById(Id optionId) {
		return getOptionById(optionId, true);
	}
	
	private ProductOptionModel getOptionById(Id optionId, Boolean tryReset) {
		if (optionRowsById == null) {
			optionRowsById = new Map<Id,ProductOptionModel>();
			for (ProductOptionModel orow : getAllOptions()) {
				optionRowsById.put(orow.vo.record.Id, orow);
			}
		}
		ProductOptionModel option = optionRowsById.get(optionId);
		if ((option == null) && (tryReset)) {
			// Reset cache and try again
			optionRowsById = null;
			return getOptionById(optionId, false);
		}
		return option;
	}
	
	public ProductFeatureModel getFeatureByKey(String key) {
		if (featureRowsById == null) {
			featureRowsById = new Map<String,ProductFeatureModel>();
			for (ProductFeatureModel feature : features) {
				featureRowsById.put(feature.getKey(), feature);
			}
		}
		return featureRowsById.get(key);
	}
	
	public QuoteVO.LineItem[] updateQuote(QuoteVO qvo, Integer groupKey) {
		return updateQuote(qvo, groupKey, true);
	}
	
	
	public QuoteVO.LineItem[] updateQuote(QuoteVO qvo, Integer groupKey, Boolean nested) {
		List<QuoteVO.LineItem> results = new List<QuoteVO.LineItem>();
		
		ProductOptionSelectionModel optionSelections = getSelections();
		QuoteVO.LineItem parentItem = null;
		if (optionSelections != null) {
			// Parent Item is used for modifying existing bundles
			// Not applicable if there is no option selections
			parentItem = qvo.getItemByKey(optionSelections.lineItemKey);
		}
		
		if (parentItem == null) {
			QuoteVO.LineItemGroup liGrp = qvo.getGroupByKey(groupKey);
			// New line item
			parentItem = qvo.addLineItem(new QuoteVO.LineItem(qvo, liGrp, getVO()));
			if (optionSelections != null) {
				optionSelections.lineItem = parentItem;
			}
			if (liGrp != null) {
				liGrp.addLineItem(parentItem);
			}
			results.add(parentItem);
		}
		if (hasOptions() && (getSelections() != null)) {
			results.addAll(updateComponents(parentItem, this, getSelections(), nested));
		}
		
		return results;
	}
	
	private List<QuoteVO.LineItem> updateComponents(QuoteVO.LineItem parentItem, ProductModel product, ProductOptionSelectionModel selectionModel, Boolean nested) {
		Integer level = (parentItem.optionLevel == null) ? 1 : (parentItem.optionLevel + 1).intValue();
		
		// The "Required By" line must be marked as a package
		// NOTE: This "package" flag is used throughout code base to test if quote line
		// is a package or not.
		parentItem.bundle = true;
		
		if (selectionModel.configurationFormExists) {
			// Map custom configuration fields from Product Option to Quote Line
			SBQQ__QuoteLine__c line = (SBQQ__QuoteLine__c)parentItem.line; 
			line.Selection_ID__c = selectionModel.getConfigurationData().Selection_Id__c;
			//QuoteUtils.mapCustomFields(parentItem.line, selectionModel.getConfigurationData(), selectionModel.getConfigurationFieldNames());
		}
		
		List<QuoteVO.LineItem> results = new List<QuoteVO.LineItem>();
		Set<Integer> currentKeys = new Set<Integer>();
		for (ProductOptionSelectionModel.Selection selection : selectionModel.getSelections()) {
			if (selection.selected == false) {
				continue;
			}
			if ((selection.option == null) || (selection.optionalProduct == null)) {
				System.assert(product.getOptionById(selection.optionId) != null, 'Selection without option: ' + selection.optionId);
				selection.restoreOption(product.getOptionById(selection.optionId));
			}
			System.assert(selection.option != null, 'Missing option: ' + selection.optionId);
			QuoteVO.LineItem componentItem = (selection.lineItemKey != null) ? parentItem.getComponentByKey(selection.lineItemKey) : null;
			if (componentItem == null) {
				System.assert(selection.optionalProduct != null, 'Component product not loaded');
				componentItem = parentItem.addComponent(selection.optionalProduct.getVO());
				selection.lineItem = componentItem;
			} else {
				componentItem.configurationRequired = false;
			}
			
			currentKeys.add(componentItem.key);
			// Must set list price every time because of potential price rules
			componentItem.listPrice = selection.listPrice;
			componentItem.optionLevel = level;
			componentItem.bundled = selection.option.vo.record.SBQQ__Bundled__c;
			componentItem.optionDiscountRate = selection.editableOption.SBQQ__Discount__c;
			componentItem.optionDiscountAmount = selection.editableOption.SBQQ__DiscountAmount__c;
			componentItem.productOptionId = selection.option.vo.record.Id;
			if ((selection.priceEditable == true) && (selection.editableOption.SBQQ__UnitPrice__c != null)) {
				if (componentItem.isPricingMethodList()) {
					componentItem.listPrice = selection.editableOption.SBQQ__UnitPrice__c;
				} else if (componentItem.isPricingMethodCustom()) {
					componentItem.specialPrice = selection.editableOption.SBQQ__UnitPrice__c;
				}
			}
			
			Decimal optionQty = selection.editableOption.SBQQ__Quantity__c;
			if (optionQty == null) {
				optionQty = 1;
			}
			if (selection.option.isSuggested()) {
				componentItem.quantity = optionQty;
				componentItem.bundledQuantity = null;
			} else {
				componentItem.bundledQuantity = optionQty;
			}
			
			if (selection.option.required || (level > 1) || (!selection.option.isSuggested())) {
				// Link to parent item; Should this be in the QuoteVO?
				componentItem.requiredBy = parentItem.line;
			}
			results.add(componentItem);
			if (selection.getNestedSelections() != null) {
				selection.getNestedSelections().lineItem = componentItem;
			}
			if (nested) {
				if (selection.option.nested || 
						((selection.optionalProduct != null) && selection.optionalProduct.getVO().hasConfigurationFields())) {
					// Need to restore the product
					selection.getNestedSelections().restoreProduct(selection.optionalProduct, true);
					results.addAll(updateComponents(componentItem, selection.optionalProduct, selection.getNestedSelections(), nested));
				}
			}
		}
		parentItem.removeAllComponentsExcept(currentKeys);
		return results;
	}
	
	public static Map<Id,ProductModel> indexById(ProductModel[] value) {
		Map<Id,ProductModel> result = new Map<Id,ProductModel>();
		for (ProductModel pmodel : value) {
			result.put(pmodel.getId(), pmodel);
		}
		return result;
	}
	
	public class InvalidStateException extends Exception {}
}