/**
 * Test suite for MetaDataUtilsTests.
 * 
 * @author Max Rudman
 * @since 8/29/2009
 */
@isTest
private class MetaDataUtilsTests {
	testMethod static void testGetFieldNames() {
		System.assert(MetaDataUtils.getFieldNames('Account').size() > 0);
		System.assert(MetaDataUtils.getFieldNames(Account.sObjectType).size() > 0);
	}
	
	testMethod static void testsGetFields() {
		System.assert(MetaDataUtils.getFieldNames('Account').size() > 0);
	}
	
	testMethod static void testGetField() {
		Integer count = Limits.getFieldsDescribes();
		System.assert(MetaDataUtils.getField('aCCount', 'nAMe') != null);
		System.assert(MetaDataUtils.getField(Account.sObjectType, 'nAMe') != null);
		//System.assertEquals(count + 1, Limits.getFieldsDescribes());
		for (Integer i=0;i<100;i++) {
			MetaDataUtils.getField('Account', 'Name');
		}
		//System.assertEquals(count + 1, Limits.getFieldsDescribes());
	}
	
	testMethod static void testGetPicklistValues() {
		Integer count = Limits.getPicklistDescribes();
		System.assert(MetaDataUtils.getPicklistValues('Account', 'Type').size() > 0);
		//System.assertEquals(count + 1, Limits.getPicklistDescribes());
		for (Integer i=0;i<100;i++) {
			MetaDataUtils.getPicklistValues('Account', 'Type');
		}
		
		System.assert(MetaDataUtils.getPicklistValues('Account', 'Type2') == null);
	}
}