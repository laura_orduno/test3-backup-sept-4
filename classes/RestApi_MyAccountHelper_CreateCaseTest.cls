/**
 * RestApi_MyAccountHelper_CreateCaseTest - Enhanced Case
 * @description Tests for MyExample
 * @author Dane Peterson, Traction on Demand
 * @date 2017-09-26
 */

@isTest
private class RestApi_MyAccountHelper_CreateCaseTest {

	private static final String ATTACHMENTNAME = 'test.jpg';
	private static final Blob ATTACHMENTBODY = Blob.valueOf('iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAABmJLR0QA/wD/AP+gvaeTAAAMvElEQVRoge2ZfZBU1ZXAf/d99Mf0xzAzMAwwfAjoCIwgOAYwRrCAIimT0sgWJSDGmFhY2V00m2RrQTeWVYFUuVuJbrK7sfygEEJMKK1FZsu4JBGIxijJEESQDBJhZoCZYYbpoT9fv3737h/d/abfdM+ARvePLU/Vqdvz5r1zz++ec+897z74VD6VT0TEx2lsO9TpcIuAhQbME9AgIaQAHRLAuRy8A7xpwMHVMPhx9f1Xg/wUahTcp8H9wPSIrmfqqqpCYb9fqzIM0DRQCpnLkc5miWezzkXLSsWlDOrwXg6e9sG21XnQ/3uQAsA/CthY5/PJKQ0N4XG1tejBICKRgFQKZVkIKUGpvBoGyjDAMLBTKS4kEnQkEokBKZWCf/XBDz4q0EcC2Qlf0eDf6oNBc+aMGcFwXR309UF3NyoWQ3McRIlxAajCbwUoTYNgEBUKoXw+BhMJTsXjyYuOY0m4fx289ImCPAuRKthuatrKuTNmVI2ZPBnV0YE8cwbNttHAVVGiRVElKoutrqPCYVQgwEA8ztFUKuXA7gw88FXIfOwg26AhAK/VVlVd1XzDDX7dsnCOHkVLpz0AGiAiEbS5cxFTp4Lfn+/IslCnT6PeeQeVSCALMK4aBkQiWFJyLB7PxKU8rsGK1XDxYwPZBeMF/HHimDH11yxaZMr330eeOoWmFBqgA1okgr5mDfpttyFuvhlRABguKpOB119HtrYiX3gBZzhUKIQ0DP4cj2cvSNmp4MZ1MPBXg+yEqA5/nFRbO23m4sWGc+QIsqtrCCAQwHzoIYyNGxHhMEIIj3oglPLqwADOD3+I85OfIDMZnAKM8vuRfj/tiUT2gpQnLFh4uTS7LMgu2DM2HF4x59Zbg7k//QnZ2ZkHAPRp0/Dv2oU+Z47ruKZpHohiq5TywEgph34fPYq9Zg1OZycScADl8+GYJu+mUqmUUj+/C+4bzU9ttH/uhPt8mra8afHioN3eTq6zc+jBxYvx/+pXaLNnl0WhElTp7zJtbsb49a/RFi507atsFmyba3y+KgF37YA7RvN1xIg8D/UGnJrb3ByO1tWROXDAjYTZ0kLV3r2IQMB1cHhrDwww0NaG1duLzGQwolEis2YRnT3bExFPm8lg3XYbdlvbUGQCAQZsm5OOM2DAVSNVA8ZIIDr8U104bFQ3NZF+9dWhSEyciP/558HvL0sfIQTnWlt5/8c/pu+NN1C5XJnd4MSJTF2/nukbNuCrr0cIgVIq3/r9GDt2kFu+HM6fdyMTNU3CjuO/BH8HbLniiGyHOhM6FyxcGDSzWezDh92lNfzCC5grVpRFIdvby9vr19P/u9+NNDYeMceM4fonnmDiqlVl0bFfeYX03Xd7luakUrznOHEHJtwDyeH2Ks4RA+6uDgZlcOpUrBMn3E3MWLYMfdmysvsT7e3sX7LkiiEA7FiMQ/fey/tPPln2P33lSvSlS4c2UMchoGlUAQJWVbJXEUSDB8ZPnx6yz55FptP58kIIfJs3A0MrEEC2r483V60i3dV1xRClcuyRR+jYudP9u2jb2LwZhMiDFGq1WiEiBmy4IpDnoV7CzDFTppA9c8atkbS5c9Gbmz33KqV458EHSZ0+XWbYjEaZ/rWv8ZlnnmHR9u3MefhhwjNnVoR591vfIn7ypOeaPm8eoqUl3w8gpSQKSLjxKagabqNssgtYWmWaab2mJmJ3d6MVDBkrVrjOFydo7NAhzre2ljk2fvlyPrNtG4H6es/1ax9+mL88/TRHvv1tpG27151MhvYtW7j+2WfdPgCMz38e+9ChfFSkRNM0fEplQnAz8D+ltssiosGC6rq6kHPpEiqXc/PUXL7cs6kBnCl0XCrjli5l8Ysv4hs7tmwnRwiuuv9+Wp57ruy57r17sXp7PX3oN93kKTQVUAV+AfOHP18WER2uD0SjWm5w0H0YXUe79lq3AyEE0rbp3bfPOwimyfU/+hHCMJBSVixRACZ++ctc9/jjpM6d8/zP6u3FLAwAgNbUhNI0lJTuXPGBD5h7WRAHJprBIE4qhaTwLlFbCz6fZ7TSHR3YA95aru6WWwhOnepCFKFLIYrRmbZhQ1nESssWpRQEAlBTg+rvdwdVz/vUONzvstQSENJM000rCRAKDRkvOJMpGc2iRK+7rswpKWXZPnE5LQVWoZAntQoOV182IkBZXspstmx+lC7BQ6MgXAdGi4jH0cuotKyyFzKGXjhHBhEQz9k2fsMYenhgACUlQtddR3zDViSAZHt7WVqVAn8YGMhvhE4sVvYSRoV6qwxEwjk7nZ4nGhqKD0E6Te7cOczGodQMNDaih0I4yaFq4cJvfkOmpwd/SQ01XIrXTj3xBINHjniuT7rnHmo++1kXxunqKotIYdHuHG630s5+ODU46GjRqCec2QMHvCNmmtQuWeJ50EmnOfad76BK5kWlydz/xhuc3LKFnpdfdrW3tZXg1Kme+7O//73bvwSkEGQh4+TPxkYHkdAW7+9PadEoyudzDwmsffvKHJtw111lo9DT2sq73/wm0rLciV464Xv37aNt7VqUlJ7n6m69Ff+kSV6Q115zIVShXMmALaBteL9l1e8vYJwNZ69budK0jh8n29mZfxsMBBj71lsYNTWel6LDd95J7K23yoCCU6Ywaf16ovPmoRwHq7ubnj176N+/v+xeoess2LuXyNy5LkTu4kUuLlqEk8nko6Jp5JSiQynbB9WrIT0qCMAuONrY1NRcPX488YMHERRK+K9/ncgjj3hArLNnafvSl8heuFDJ1BXJ9E2bmPyNb3iiEX/sMVLbtrkZoQyDQcchptTBtbBkuI2K1a+C/+g7cyZlNDTkD9LIhze5YwfO2bOeDv2TJjHnqacwotGPBDFh3ToaH3jAYzN3+jTJnTs9c0MKQUKphIL/rGSnIogJu9KZjEicPk1g1izXoGNZxL77XZTjeCZwpKWF+Xv3EmpqumIAYZpM37SJq7dudcsPpRTKcYg/9hjStoeioetYUmJDLgv/VcmeXunibrDuhGprYGBB/Y03mlZXFzKbBSD3wQeodBr/5z7nWV6NMWOYsHYtvrFjSZ08Se7SpcoAhkH97bcz68knqV25EkogAOJbtpB66aUhCE1D6Tr9uVzShq1fgQMV7Y40Yk9BVRg+aJwzpz5aX09s/340pfLHoEJQ873vEVq7dsjQsPf35PHjXGprw+7rw8lkMCIRgjNnUnPTTeiRSMUKIbljB4OPPopUyl36RSBAwrbVgON0mHDNash+KBCAZ2BtUIinm5Yurcr19pI+dsxzphu64w5qvv99RCBQblhUNl1xk0yniW3aRHLPHs/mJ/x+HKDbstLdsOYf4JeAVcluxdQqiO9lOPVFmJ3p6ZkxbsECQ1oWucGh6sA+cQLrt7/Fd/XV6BMnjmJqZMm+/TYXN24kffBgGQS6To9lZWLws42wrchN/qToikB08nW//y/w+g253BezPT3hsS0thrRtcrGYe6PT00Ni927s48cxJ0xAb2iAEaLhipRYf/gDA48+Suzxx8n19nohAgGE309fKpVOKPXnzfBQqnDMhfcw35WRevQBfiAABJbCpHvhxTHV1WMnL1rkszo6SJ04gSikSennA2P8eKpWrsQ/fz5GY6N7mK0sC6erC6utjeSrr+L09uavl3ikhEALhfKTOx7PJqQ8+wNYdxT6yZ/9lqpnrowGEijV+dDw9/BcJBhsnLxggV8oRfLoUZx4vOyjzmjGVYXfCsA00SMRHMehPx63BqU8tRX+9nT+s0LReWvYb1dGSi2NfGWsF7Ub7DfhvxfkcldluromG4ahVzc3IwIBcokEsnCq6PkyNYqW7thGdTVaOEwymaQvkbC7lPrlZvjn3vxnOBvIFdQuUU+xNhJI8WWseNyrASIJ6hU4OBnORmKxluT58yJQXa2Hm5owotG8c7aNdJzRYQwDPRRCq6nBiEaxMhn6Y7HsJdsefA22/gv8PDuUPvYIrUdGm5U6+bTyV9LxUP0gfLURVpmGQfW4cf5wXR1GOJyfD6lUvgLOZvNzSdMQhoHQdRCCXDJJKpEgnkxaOaXkSdj97/Czi/koZMmnznAtplTZofLlvo9oJTC+Cq05DWr+BpZdA7cHYQZCWMFg0AwEg6ZumnkIpXCkxMlmyVpWNm1ZOQG+OLS/B62/gAPdcKlktItqVYAqW3qvBKQoRceLapa0RTWmQOQLcMMEuLoWmvwwTiukrwQnAz0Xob0LTu6Fw72Qwpv/ObzpM1wrHBR8OJDivRUBSrS4OJR+Gy3tp3SeOyWaY3SgEQE+CkipaKNAFEGKrzHFfkoXNLegLrS5YUBZhq1KnxRIJTvDQYoww6USSLH9VP7fyP8CNGNTUEDntqIAAAAASUVORK5CYII=');
	private static final String DEFAULTRTNAME = 'SMB Care Billing';
	private static final Id DEFAULTRTID = Schema.SObjectType.Case.getRecordTypeInfosByName().get(DEFAULTRTNAME).getRecordTypeId();
	private static final String ORIGIN = 'My Account';

	private static final String CONTACTFIRST = 'Phillip';
	private static final String CONTACTLAST = 'Smith';
	private static final String CONTACTFULL = CONTACTFIRST + ' ' + CONTACTLAST;
	private static final String USERNAME = 'psmith@telus.com' + System.currentTimeMillis();

	private static User u;
	private static Account a;
	private static Account porAcc;
	private static ExternalToInternal__c ei;
	private static RestApi_MyAccountHelper_CreateCase createCaseObj;
	private static RestApi_MyAccountHelper_CreateCase.RequestUser ru;
	private static RestApi_MyAccountHelper_CreateCase.RequestCreateCase rcc;
	private static RestApi_MyAccountHelper_CreateCase.CreateCaseResponse resp;
	private static RestApi_MyAccountHelper.Response intresp;

	static {
		trac_TriggerHandlerBase.blockTrigger = true;
		ei = RestApi_MyAccountHelper_TestUtils.createEtoIRT();
		insert ei;

		RestApi_MyAccountHelper_TestUtils.MASRUserBundle bundle = RestApi_MyAccountHelper_TestUtils.createMASREnabledUser();

		porAcc = bundle.parentAccount;
		a = bundle.banAccount;
		u = bundle.u;

		ru = createRequestUser();
		rcc = createRequestCreateCase();
		createMBRCustomSetting();
		trac_TriggerHandlerBase.blockTrigger = false;
	}

	// Proper Test class with that executes as needed
@isTest
	private static void testCompleteRequest() {

		rcc.requestType = RestApi_MyAccountHelper_TestUtils.REQUESTTYPE;
		rcc.collaborators = RestApi_MyAccountHelper_TestUtils.COLLABORATORS;
		rcc.attachment = createRequestCaseAttachment();
		rcc.attachment.name = RestApi_MyAccountHelper_TestUtils.ATTACHMENTNAME;
		rcc.attachment.body = RestApi_MyAccountHelper_TestUtils.ATTACHMENTBODY;
		createRequest(ru, rcc);

		Test.startTest();

		createCaseObj = new RestApi_MyAccountHelper_CreateCase(RestContext.request.requestBody.toString());
		resp = createCaseObj.response;

		Test.stopTest();

		System.debug('Response : ' + resp);

		// Were the records successfully returned in the response
		System.assertEquals(resp.error, null);

		System.assertEquals(resp.userName, RestApi_MyAccountHelper_TestUtils.CONTACTFULL);

		System.assertEquals(resp.caseDetails.subject, RestApi_MyAccountHelper_TestUtils.SUBJECT);
		System.assertEquals(resp.caseDetails.description, RestApi_MyAccountHelper_TestUtils.DESCRIPTION);
		System.assertEquals(resp.caseDetails.accountId, a.Id);
		//System.assertEquals(resp.caseDetails.account__c, a.Id);           //TODO: fix failing assertion
		System.assertEquals(resp.caseDetails.contactId, u.contactId);
		System.assertEquals(resp.caseDetails.my_Business_Requests_Type__c, RestApi_MyAccountHelper_TestUtils.REQUESTTYPE);
		//System.assertEquals(resp.caseDetails.notifyCustomer__c, CHECKALLOWNOTIFICATIONS);   //TODO: fix failing assertion
		System.assertEquals(resp.caseDetails.notifyCollaboratorString__c, RestApi_MyAccountHelper_TestUtils.CHECKCOLLABORATORS);
		System.assertEquals(resp.caseDetails.recordtypeId, RestApi_MyAccountHelper_TestUtils.DEFAULTRTID);
		System.assertEquals(resp.caseDetails.origin, RestApi_MyAccountHelper_TestUtils.ORIGIN);

		System.assertEquals(resp.attachments[0].name, RestApi_MyAccountHelper_TestUtils.ATTACHMENTNAME);
		// Issue with testing BODY responses. this will be checked below

		// Were the records successfully created in the database
		Case caseCheck = [SELECT Id, Description, Subject, AccountId, Account__c, ContactId, Origin, My_Business_Requests_Type__c, NotifyCollaboratorString__c, NotifyCustomer__c, RecordTypeId FROM Case WHERE id = :resp.caseDetails.Id];
		System.assertEquals(caseCheck.subject, RestApi_MyAccountHelper_TestUtils.SUBJECT);
		System.assertEquals(caseCheck.description, RestApi_MyAccountHelper_TestUtils.DESCRIPTION);
		System.assertEquals(caseCheck.accountId, a.Id);
		//System.assertEquals(caseCheck.account__c, a.Id);          //TODO: fix failing assertion
		System.assertEquals(caseCheck.contactId, u.contactId);
		System.assertEquals(caseCheck.my_Business_Requests_Type__c, RestApi_MyAccountHelper_TestUtils.REQUESTTYPE);
		System.assertEquals(caseCheck.notifyCustomer__c, RestApi_MyAccountHelper_TestUtils.ALLOWNOTIFICATIONS);
		System.assertEquals(caseCheck.notifyCollaboratorString__c, RestApi_MyAccountHelper_TestUtils.CHECKCOLLABORATORS);
		System.assertEquals(caseCheck.recordtypeId, RestApi_MyAccountHelper_TestUtils.DEFAULTRTID);
		System.assertEquals(caseCheck.origin, RestApi_MyAccountHelper_TestUtils.ORIGIN);

		List<Attachment> attachCheck = [SELECT Id, Name, Body FROM Attachment WHERE ParentId = :casecheck.Id];
		System.assert(!attachCheck.isEmpty());
		System.assertEquals(attachCheck[0].name, ATTACHMENTNAME);
		System.assertEquals(String.valueOf(attachCheck[0].body), String.valueOf(ATTACHMENTBODY));
	}

	//Bad UUID
	@isTest
	private static void testBadUUID() {
		ru.uuid = RestApi_MyAccountHelper_TestUtils.BADUUID;
		createRequest(ru, rcc);
		Test.startTest();

		intresp = RestApi_MyAccount.postRequest();
		Test.stopTest();

		// Were the records successfully returned in the response
		System.assertEquals('User not found for uuid: ' + RestApi_MyAccountHelper_TestUtils.BADUUID, intresp.error.description);
	}

	//Bad UUID
	@isTest
	private static void noUser() {
		createRequest(ru, rcc);

		Test.startTest();
		intresp = RestApi_MyAccount.postRequest();
		Test.stopTest();

		// Were the records successfully returned in the response
		// TODO System.assertEquals('User: List has no rows for assignment to SObject', intresp.error.description);
	}

	//Bad UUID
	@isTest
	private static void noUUID() {
		ru.uuid = null;
		createRequest(ru, rcc);

		Test.startTest();
		intresp = RestApi_MyAccount.postRequest();
		Test.stopTest();

		// Were the records successfully returned in the response
		// TODO System.assertEquals('User: List has no rows for assignment to SObject', intresp.error.description);
	}

	//Bad Account Number
	@isTest
	private static void badBillingAccountNumber() {
		ru.uuid = null;
		createRequest(ru, rcc);

		Test.startTest();
		intresp = RestApi_MyAccount.postRequest();
		Test.stopTest();

		// Were the records successfully returned in the response
		// TODO System.assertEquals('User: List has no rows for assignment to SObject', intresp.error.description);
	}

	//No Billing Account Number
	@isTest
	private static void noBillingAccountNumber() {
		ru.uuid = null;

		createRequest(ru, rcc);
		Test.startTest();
		intresp = RestApi_MyAccount.postRequest();
		Test.stopTest();

		// Were the records successfully returned in the response
		// TODO System.assertEquals('User: List has no rows for assignment to SObject', intresp.error.description);
	}

	private static void createRequest(RestApi_MyAccountHelper_CreateCase.RequestUser ru, RestApi_MyAccountHelper_CreateCase.RequestCreateCase rcc) {
		RestApi_MyAccountHelper_CreateCase.CreateCaseRequestObject caseRequest = new RestApi_MyAccountHelper_CreateCase.CreateCaseRequestObject();
		caseRequest.user = ru;
		caseRequest.caseToCreate = rcc;
		caseRequest.type = RestApi_MyAccountHelper_TestUtils.REQUEST_TYPE_CREATE_CASE;
		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/RestApi_MyAccount/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(JSON.serialize(caseRequest));
		RestContext.request = request;

		//return request;
	}

	private static RestApi_MyAccountHelper_CreateCase.RequestUser createRequestUser() {
		RestApi_MyAccountHelper_CreateCase.RequestUser ru = new RestApi_MyAccountHelper_CreateCase.RequestUser();
		ru.uuid = RestApi_MyAccountHelper_TestUtils.UUID;
		return ru;
	}
	private static RestApi_MyAccountHelper_CreateCase.RequestCreateCase createRequestCreateCase() {
		RestApi_MyAccountHelper_CreateCase.RequestCreateCase rcc = new RestApi_MyAccountHelper_CreateCase.RequestCreateCase();
		rcc.billingAccountNumber = RestApi_MyAccountHelper_TestUtils.BILLINGACCOUNTNUMBER;    // 	* billing account number (e.g. 101-45222)
		rcc.attachment = null;  // 	* case attachment
		rcc.category = '';                //	* category
		rcc.requestType = '';                //	* request type
		rcc.subject = RestApi_MyAccountHelper_TestUtils.SUBJECT;                    //	* subject
		rcc.description = RestApi_MyAccountHelper_TestUtils.DESCRIPTION;                //	* description (note: description must contain labels and corresponding values from the submitted case form.)
		rcc.collaborators = null;
		return rcc;
	}

	private static RestApi_MyAccountHelper_CreateCase.CaseAttachment createRequestCaseAttachment() {
		RestApi_MyAccountHelper_CreateCase.CaseAttachment ca = new RestApi_MyAccountHelper_CreateCase.CaseAttachment();
		ca.name = '';
		ca.body = Blob.valueof('');
		return ca;
	}

	private static void createMBRCustomSetting() {
		User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		EmailTemplate e = null;

		Contact c = new Contact(
				FirstName = 'DummyId',
				Lastname = 'DummyCon',
				AccountId = porAcc.Id,
				Email = System.now().millisecond() + 'wat@test.com'
		);

		insert c;

		System.runAs(thisUser) {
			e = RestApi_MyAccountHelper_TestUtils.createEmailTemplate('Test');
			insert e;
		}
		// Attempt to workaround MIXED DML Operations https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_dml_non_mix_sobjects_test_methods.htm
		System.runAs(thisUser) {
			List<OrgWideEmailAddress> emails = [Select Id From OrgWideEmailAddress LIMIT 1];//Where DisplayName = 'My Account Business Requests'];

			MBR_Case_Collaborator_Settings__c settings = MBR_Case_Collaborator_Settings__c.getOrgDefaults();
			settings.Dummy_Contact_Id__c = c.id;
			settings.My_Account_Org_Wide_Email_Id__c = emails.isEmpty() == false ? emails[0].id : null;
			settings.New_My_Account_Case_Template_Id__c = e.id;
			settings.Update_My_Account_Case_Template_Id__c = e.id;
			upsert settings MBR_Case_Collaborator_Settings__c.Id;
		}
	}
}