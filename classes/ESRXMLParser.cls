/********************************************************************************************
 * Class Name : ESRXMLParser
 * Purpose    : The Purpose of this class is to parse the XML response fron the ESR System.     
 * 
 * Modification History 
 *-------------------------------------------------------------------------------------------
 * Author               			Date                Version             Remarks
 *-------------------------------------------------------------------------------------------
 * Tech Mahindra(Swapnil)			Apr/21/2016          1.0                 Initial Creation
 * 
 *
 **********************************************************************************************/


global class ESRXMLParser {

    private String xmlString;
    public XmlFault fault {get;set;}
    public CreateServiceResult serviceResult{get;set;}
    //Constructor
    public ESRXMLParser(string xmlString)
    {
        this.xmlString = xmlString;
        this.fault = new XmlFault();
        serviceResult= new CreateServiceResult();
    }
    //Parse Method for Response XML
    public ESRXMLParser ParseXMLString(){
        try{
            Dom.Document doc = new Dom.Document();
            doc.load(xmlString);    
            
            Dom.XmlNode node = doc.getRootElement();
            TraceXml(node);
            
            return this;
           
        }
        
        catch(Exception e){
          throw e;
        }
       
    }
    //XML node parser Method for sucess and fault
    public void TraceXml(Dom.XmlNode envelope){
        if(envelope != NULL){
            for(Dom.XmlNode childEnvelope : envelope.getChildElements()){
                if(childEnvelope.getName()=='Header'){
                    // if having multiple header then iterate here otherwise get text()
                }
                else if(childEnvelope.getName()=='Body'){
                    for(Dom.XmlNode childBody : childEnvelope.getChildElements()){
                        if(childBody != NULL && childBody.getName()=='Fault'){
                            for(Dom.XmlNode faultchild : childBody.getChildElements()){
                                if(faultchild != NULL && faultchild.getName()=='faultcode'){
                                    fault.faultcode = faultchild.getText().remove('s:');
                                }
                                else if(faultchild != NULL && faultchild.getName()=='faultstring'){
                                    fault.faultstring = faultchild.getText();
                                }
                            }         
                        }
                        else if(childBody != NULL && childBody.getName()=='createServiceResponse' ){
                            for(Dom.XmlNode createServiceResponseChild : childBody.getChildElements()){
                                if(createServiceResponseChild != NULL && createServiceResponseChild.getName()=='createServiceResult'){
                                    for(Dom.XmlNode createServiceResultChild : createServiceResponseChild.getChildElements()){
                                        if(createServiceResultChild != NULL && createServiceResultChild.getName()=='contextData'){
                                            //no data
                                        }
                                        if(createServiceResultChild != NULL && createServiceResultChild.getName()=='dateTimeStamp'){
                                            serviceResult.dateTimeStamp = createServiceResultChild.getText();
                                        }
                                        if(createServiceResultChild != NULL && createServiceResultChild.getName()=='errorCode'){
                                            serviceResult.errorCode = createServiceResultChild.getText();
                                        }
                                        if(createServiceResultChild != NULL && createServiceResultChild.getName()=='messageType'){
                                            serviceResult.messageType = createServiceResultChild.getText();
                                        }
                                        if(createServiceResultChild != NULL && createServiceResultChild.getName()=='transactionId'){
                                            //no data
                                        }
                                        if(createServiceResultChild != NULL && createServiceResultChild.getName()=='serviceResponseId'){
                                            serviceResult.serviceResponseId = createServiceResultChild.getText();
                                        }
                                    }
                                }
                            }                    
                        }
                    }
                }
            } 
        }
        
    }
    //end of TraceXml()
    //Entity Class for create service result for Sucess Response 
    public class CreateServiceResult{
        public String dateTimeStamp{get;set;}
        public String errorCode{get;set;}
        public String messageType{get;set;}
        public String transactionId{get;set;}
        public String serviceResponseId{get;set;}
    }
     //Entity Class for create service result for Error Response 
    public class XmlFault {
        public String faultcode {get;set;}
        public String faultstring {get; set;}
    }
}