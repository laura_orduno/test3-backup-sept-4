@isTest(SeeAllData=false)
public class ENTPNewCaseCtlrMITS_test {
  
/*****    
     static User u {get;set;}

    static {
        u = MBRTestUtils.createPortalUser('');
        MBRTestUtils.createExternalToInternalCustomSettings();
        MBRTestUtils.createParentToChildCustomSettings();
        MBRTestUtils.createTypeToFieldsCustomSettings();

    }
*/
    
    @isTest public static void testOther() {
        ENTPNewCaseMITSCtlr ctlr = new ENTPNewCaseMITSCtlr();
            String CatListPull1='test';
            System.assertEquals( CatListPull1, 'test');
        ctlr.CatListPull = 'test';
        ctlr.getParentCase();
        ctlr.getParentCaseType();
        ctlr.getParentCategory();
        ctlr.getCaseToInsert();
        
        System.assert(ctlr.getCategories().size() > 0);
        System.assert(ctlr.getRequestTypes().size() > 0);        
        System.assert(ctlr.createNewOnSubmit  == false);
        ctlr.createNewOnSubmit = true;
        System.assert(ctlr.createNewOnSubmit == true);
        ctlr.clear();
        ctlr.requestType = 'onlineStatus';
//        System.assert(ctlr.getRecordType('Dummy') == 'Generic');
        Attachment a  = ctlr.attachment;

        
        ENTPNewCaseMITSCtlr ctlr2 = new ENTPNewCaseMITSCtlr();

        System.assert(ctlr2.getCategories().size() > 0);
        System.assert(ctlr2.getRequestTypes().size() > 0);        
        System.assert(ctlr2.createNewOnSubmit  == false);
        ctlr2.createNewOnSubmit = true;
        System.assert(ctlr2.createNewOnSubmit == true);
        ctlr2.clear();
        ctlr2.requestType = 'onlineStatus';
    }

    @isTest public static  void testResetValues() {
        MBRTestUtils.createParentToChildCustomSettings();
        MBRTestUtils.createExternalToInternalCustomSettings();

        PageReference pref = Page.ENTPNewCaseMITS;
        pref.getParameters().put('category', 'CCICatRequest1');
        Test.setCurrentPage(pref);

        ENTPNewCaseMITSCtlr ctlr = new ENTPNewCaseMITSCtlr();
        // reset values
        ctlr.caseSubmitted = true;
        ctlr.resetValues();
        System.assert(ctlr.selectedCategory == Label.MBRSelectCategory);

        ctlr.caseSubmitted = false;
       
//        pref.getParameters().put('category', null);
        ctlr.resetValues();
       // System.assert(ctlr.selectedCategory == 'Mainstream/Data(i.e T1, T3, DS0, etc)');
    }

    @isTest public static void testInitCheck() {
        User u = MBRTestUtils.createENTPPortalUser('test-user-1', true, 'en_US');        
        System.runAs(u) {
            ENTPNewCaseMITSCtlr ctlr = new ENTPNewCaseMITSCtlr();
            System.assert(ctlr.initCheck() == null);
            System.assert(ctlr.getCATLISTPULL().size() >= 0);
            ctlr.setCATLISTPULL('test');
            
        }
    }
    
    @isTest public static void testCreateNewCase() {
        MBRTestUtils.createExternalToInternalCustomSettings();
//        MBRTestUtils.createParentToChildCustomSettings();
//        MBRTestUtils.createTypeToFieldsCustomSettings();

        //System.runAs(u) { // sharing issue with this context

            PageReference pref = Page.ENTPNewCaseMITS;
            pref.getParameters().put('category', 'CCICatRequest1');
            Test.setCurrentPage(pref);
        
            ENTPNewCaseMITSCtlr ctlr = new ENTPNewCaseMITSCtlr();
            ctlr.receiveInput();
            ctlr.attachment.Name='Unit Test Attachment';
            ctlr.attachment.body=Blob.valueOf('Unit Test Attachment Body');

//System.debug('66: ctlr.createNewCase() + ' ctlr.createNewCase());        
//            System.assert(ctlr.createNewCase()ctlr.createNewCase() == null);
            ctlr.pageValidationError = label.mbrMissingRecordType;
            ctlr.requestType = label.mbrSelectCategoryFirst;   
//            System.assert(ctlr.createNewCase() == null);
            
            ctlr.selectedCategory = 'Test';
            ctlr.requestType = label.mbrSelectCategoryFirst;  
//            System.assert(ctlr.createNewCase() == null); 
            
            ctlr.requestType = label.mbrSelectType;  
//            System.assert(ctlr.createNewCase() == null);

  ctlr.caseToInsert.Description = 'Test Description';
            ctlr.requestType = 'Voice - PRI';  
            ctlr.createNewCase();            //System.assert(ctlr.createNewCase() == null);

            ctlr.caseToInsert.recordtypeid = null;   
//            System.assert(ctlr.createNewCase() == null);

            ctlr.requestType = 'Mainstream/Data (i.e. T1, T3, DS0, etc)';
            ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId;   
            ctlr.createNewCase();
        //}
    }

    @isTest public static void testCreateParentCase() {
        MBRTestUtils.createExternalToInternalCustomSettings();

        ENTPNewCaseMITSCtlr ctlr = new ENTPNewCaseMITSCtlr();


        ctlr.selectedCategory = 'Account and billing';
        ctlr.requestType = 'Voice - PRI';
        //ctlr.caseToInsert.TypeOfIncident = 'Customer';
        ctlr.caseToInsert.Description = 'Test Description';
        ctlr.caseToInsert.Subject = 'Test Subject';
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId;   
        ctlr.createNewCase();

        List<Case> parentCases = [SELECT Id, CaseNumber, My_Business_Requests_Type__c FROM Case ORDER BY CreatedDate DESC];
        Case parentCase = null;
        system.debug('ParentCase:'+parentCases.size());
        if (parentCases.size() > 0) {
            parentCase = parentCases[0];
            parentCase.status = 'Closed';
            update parentCase;
        }

       
System.debug('parentCase: ' + parentCase);        
        // set pageReference and querystring params
        PageReference submitPageRef = Page.ENTPNewCaseMITS;
        Test.setCurrentPage(submitPageRef);
        ApexPages.currentPage().getParameters().put('reopen', parentCase.CaseNumber);

        // instantiate a second controller for the child insert
       	ENTPNewCaseMITSCtlr ctlr2 = new ENTPNewCaseMITSCtlr();
        ctlr2.selectedCategory = 'Account and billing';
        ctlr2.requestType = 'ASR Activities';
         ctlr2.caseToInsert.Description = 'Test Description';
        ctlr2.caseToInsert.Subject = 'Test Subject Child';
        ctlr2.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId;   
        ctlr2.createNewCase();
            
        Case childCase = [SELECT Id, Parent.Id, ParentId, CaseNumber FROM Case WHERE ParentId != null ORDER BY CreatedDate DESC LIMIT 1][0];
        System.assertEquals(parentCase.Id, childCase.Parent.Id);

    }

    @isTest(SeeAllData=false)
    public static void testTypeOfIncidentForm(){
       // public ENTPFormWrapper formWrapper {get; set;}
        MBRTestUtils.createExternalToInternalCustomSettings();
        
        User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US');   
  
            ENTPNewCaseMITSCtlr ctlr = new ENTPNewCaseMITSCtlr();
       
            ctlr.requestType = 'ENT Care Assure';
            ctlr.selectedCategory = 'Enterprise';
            ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 
    
            ctlr.formWrapper.getMITSTicketInfo.AltContactName = 'Test';
            ctlr.formWrapper.getMITSTicketInfo.AltContactNumber = 'Test';
            ctlr.formWrapper.getMITSTicketInfo.AltEmailAdd = 'Test';
            ctlr.formWrapper.getMITSTicketInfo.IssueStartOccur = 'Test';
            ctlr.formWrapper.getMITSTicketInfo.TypeOfIncident = 'Test';
            ctlr.formWrapper.getMITSTicketInfo.TypeOfProblem = 'Test';
           
            ctlr.createNewCase();
        
        	ctlr.userLanguage = 'en_US';
            ctlr.createNewCase();
        
            List<Case> cases = [SELECT Id, Description, Subject FROM Case];
   
    }
   
  @isTest(SeeAllData=false)
    public static void testTypeOfIncidentFormFr(){
       // public ENTPFormWrapper formWrapper {get; set;}
        MBRTestUtils.createExternalToInternalCustomSettings();
        
        User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'fr');   
  
            ENTPNewCaseMITSCtlr ctlr = new ENTPNewCaseMITSCtlr();
       
            ctlr.requestType = 'ENT Care Assure';
            ctlr.selectedCategory = 'Enterprise';
            ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 
    
            ctlr.formWrapper.getMITSTicketInfo.AltContactName = 'Test';
            ctlr.formWrapper.getMITSTicketInfo.AltContactNumber = 'Test';
            ctlr.formWrapper.getMITSTicketInfo.AltEmailAdd = 'Test';
            ctlr.formWrapper.getMITSTicketInfo.IssueStartOccur = 'Test';
            ctlr.formWrapper.getMITSTicketInfo.TypeOfIncident = 'Test';
            ctlr.formWrapper.getMITSTicketInfo.TypeOfProblem = 'Test';
           
            ctlr.createNewCase();
        
        	ctlr.userLanguage = 'fr';
            ctlr.createNewCase();
        
            List<Case> cases = [SELECT Id, Description, Subject FROM Case];
   
    }
   
    @isTest public static void testUpdateCasePriorityToUrgent() {
        
        ENTPNewCaseMITSCtlr ctlr = new ENTPNewCaseMITSCtlr();

       String originValue = 'ENTP';
      
        List<Case> cases = new List<Case>();
        cases.add( new Case(Subject='4', Status = 'New', Priority = 'Medium', Origin = originValue) ); // subject will be used for day of week
        cases.add( new Case(Subject='5', Status = 'New', Priority = 'Medium', Origin = originValue) );
        cases.add( new Case(Subject='6', Status = 'New', Priority = 'Medium', Origin = originValue) );
        cases.add( new Case(Subject='0', Status = 'New', Priority = 'Medium', Origin = originValue) );
        cases.add( new Case(Subject='0', Status = 'New', Priority = 'Urgent', Origin = originValue) );
        cases.add( new Case(Subject='0', Status = 'In Progress', Priority = 'Medium', Origin = originValue) );

        System.debug('akong: cases A: ' + cases);

        try {
            insert cases;
        } catch (Exception e) {
            System.debug('akong: case insert exception: ' + e);
        }

        Test.startTest();
        ENTPNewCaseMITSCtlr.updateCasePriorityToUrgent();
        Test.stopTest();

        List<Case> urgentCases = [SELECT Id, Priority FROM Case WHERE Priority = 'Urgent'];
       System.assertEquals(5, urgentCases.size());
    }


}