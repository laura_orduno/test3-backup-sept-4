/*
    ###########################################################################
    # File..................: TBOworkRequestContExtTest
    # Version...............: 1
    # Created by............: Umesh Atry (TechM)
    # Created Date..........: 21/03/2016 
    # Last Modified by......: Umesh Atry (TechM)
    # Last Modified Date....: 25/07/2016
    # Description...........: Test Class for TBOWorkRequestEmailComponent,TboWorkRequestContrExtension,TBOContractAssessmentEmailComponent
    #
    ############################################################################
    */
@isTest
private class TBOworkRequestContExtTest {

    static testMethod void TestTBOWorkRequest_Positive() {
        Schema.DescribeSObjectResult tboSchema = Schema.SObjectType.Account;
        Map < String, Schema.RecordTypeInfo > AccountRecordTypeInfo = tboSchema.getRecordTypeInfosByName();
        Schema.DescribeSObjectResult tboCaseSchema = Schema.SObjectType.Case;
        Map < String, Schema.RecordTypeInfo > caseRecordTypeInfo = tboCaseSchema.getRecordTypeInfosByName();
        Schema.DescribeSObjectResult tboWRSchema = Schema.SObjectType.Work_Request__c;
        Map < String, Schema.RecordTypeInfo > wrRecordTypeInfo = tboWRSchema.getRecordTypeInfosByName();

        Account RCIDAcc = new Account(Name = 'testTBORCID', recordtypeid = AccountRecordTypeInfo.get('RCID').getRecordTypeId(), RCID__C = 'RCID123', Inactive__c = false, Billing_Account_Active_Indicator__c = 'Y');
        insert RCIDAcc;
        Account inacc = new Account(Name = 'outgoingTBORCID', recordtypeid = AccountRecordTypeInfo.get('RCID').getRecordTypeId(), RCID__C = 'RCID1345623', Inactive__c = false, Billing_Account_Active_Indicator__c = 'Y');
        insert inacc;
        
        List<Contact> lstContact = new List<Contact>();
        
        contact cont = new contact();

        cont.LastName = 'Test';
        cont.AccountId = RCIDAcc.id;
        //insert cont;
        lstContact.add(cont);
        Contact cont1 = new Contact();

        cont1.LastName = 'Test1';
        cont1.AccountId = inacc.id;
        //insert cont1;
         lstContact.add(cont1);
         
         Insert lstContact;
        
        List<Case> lstCase = new List<Case>();
        User usr= [SELECT Id, IsActive FROM User where IsActive =true limit 1];
        Case c = new Case(Subject = 'testTBO', Created_Date__c = system.TODAY(), status = 'In Progress',
            TBO_In_Progress_Status__c = 'Customer Validation', Requested_Transfer_Date__c = System.Today(),
            Transfer_Type__c = 'abc', Other_Working_TELUS_Service__c = 'sddsds', Authorized_Outgoing_Permission__c = true,
            Directory_Information__c = 'Listing', Listing_Name__c = 'dsffsdff',
            Business_Type__c = 'official', Jurisdiction__c = 'bc', Enter_State__c = 'US',
            recordtypeid = caseRecordTypeInfo.get('TBO Request').getRecordTypeId(),

            Accountid = RCIDAcc.id, contactId = cont.id, Master_Account_Name__c = inacc.id,
            Contact_phone_number__c = cont1.id);
          //insert c;
          lstCase.add(c);
          Case c1 = new Case(Subject = 'testTBO', Created_Date__c = system.TODAY(), status = 'Cancelled',
             Requested_Transfer_Date__c = System.Today(),
            Transfer_Type__c = 'Amalgamation', Other_Working_TELUS_Service__c = 'sddsds', Authorized_Outgoing_Permission__c = true,
            Directory_Information__c = 'Listing', Listing_Name__c = 'dsffsdff',
            Business_Type__c = 'official', Jurisdiction__c = 'bc', Enter_State__c = 'US',
            recordtypeid = caseRecordTypeInfo.get('TBO Request').getRecordTypeId(),

            Accountid = RCIDAcc.id, contactId = cont.id, Master_Account_Name__c = inacc.id,ownerid = usr.id,
            Contact_phone_number__c = cont1.id,Resolution_Details__c ='case cancelled');
          //insert c1;
            lstCase.add(c1);
            Insert lstCase;
          c1.subject='Cancelled';
          update c1;

        Case_Account__c caseAcc = new Case_Account__c();
        caseAcc.Case__c = c.Id;
        caseAcc.Account__c = RCIDAcc.Id;
        caseAcc.Service_type__c = 'Fiber';

        insert caseAcc;
        
        try{
            Work_Request__c testWorkReq = new Work_Request__c();
            testWorkReq.recordtypeid = wrRecordTypeInfo.get('TBO Work Request').getRecordTypeId();
            testWorkReq.Status__c = 'New';
            testWorkReq.Service_type__c = 'Fibre';
            testWorkReq.Case__c = c.Id;
            insert testWorkReq;
            
           testWorkReq.Service_type__c = 'Adcom';
           update testWorkReq;
            //System.assertnotEquals(testWorkReq.Service_type__c, 'Adcom');
            
            Work_Request__c testWorkReq1 = new Work_Request__c();
            testWorkReq1.recordtypeid = wrRecordTypeInfo.get('TBO Work Request').getRecordTypeId();
            testWorkReq1.Status__c = 'New';
            testWorkReq1.Service_type__c = 'Fibre';
            testWorkReq1.Case__c = c.Id;
            insert testWorkReq1;
        
        Test.setCurrentPageReference(new PageReference('Page.TBOCaseAccountLists'));

        System.currentPageReference().getParameters().put('Id', testWorkReq.Id);
        TBOWorkRequestEmailComponent e = new TBOWorkRequestEmailComponent();
        e.wrID = testWorkReq.Id;
          Test.startTest();
        // start - added unit test for TboWorkRequestContrExtension class 26-May-2016 (UmeshA)
        ApexPages.StandardController sc = new ApexPages.standardController(testWorkReq);
        TboWorkRequestContrExtension tborc = new TboWorkRequestContrExtension(sc);
        tborc.getCaseAccounts();
        // end - added unit test for TboWorkRequestContrExtension class 26-May-2016 (UmeshA)
        System.assertEquals(tborc.workRequest.id, testWorkReq.id);
    
        List < Case_Account__c > events = e.CaseAcc;
        
        System.assertEquals(testWorkReq.Id, e.wrID);
        

        Test.stopTest();   
        }
        catch(Exception e){}



    }
    static testMethod void TestTBOWorkRequest_Negative() {
        Schema.DescribeSObjectResult tboSchema = Schema.SObjectType.Account;
        Map < String, Schema.RecordTypeInfo > AccountRecordTypeInfo = tboSchema.getRecordTypeInfosByName();
        Schema.DescribeSObjectResult tboCaseSchema = Schema.SObjectType.Case;
        Map < String, Schema.RecordTypeInfo > caseRecordTypeInfo = tboCaseSchema.getRecordTypeInfosByName();
        Schema.DescribeSObjectResult tboWRSchema = Schema.SObjectType.Work_Request__c;
        Map < String, Schema.RecordTypeInfo > wrRecordTypeInfo = tboWRSchema.getRecordTypeInfosByName();
        
        Account RCIDAcc = new Account(Name = 'testTBORCID', recordtypeid = AccountRecordTypeInfo.get('RCID').getRecordTypeId(), RCID__C = 'RCID123', Inactive__c = false, Billing_Account_Active_Indicator__c = 'Y');
        insert RCIDAcc;
        Account inacc = new Account(Name = 'outgoingTBORCID', recordtypeid = AccountRecordTypeInfo.get('RCID').getRecordTypeId(), RCID__C = 'RCID1343423', Inactive__c = false, Billing_Account_Active_Indicator__c = 'Y');
        insert inacc;
        contact cont = new contact();

        cont.LastName = 'Test';
        cont.AccountId = RCIDAcc.id;
        insert cont;
        Contact cont1 = new Contact();

        cont1.LastName = 'Test1';
        cont1.AccountId = inacc.id;
        insert cont1;
        Case c = new Case(Subject = 'testTBO', Created_Date__c = system.TODAY(), status = 'In Progress',
            TBO_In_Progress_Status__c = 'Customer Validation', Requested_Transfer_Date__c = System.Today(),
            Transfer_Type__c = 'abc', Other_Working_TELUS_Service__c = 'sddsds', Authorized_Outgoing_Permission__c = true,
            Directory_Information__c = 'Listing', Listing_Name__c = 'dsffsdff',
            Business_Type__c = 'official', Jurisdiction__c = 'bc', Enter_State__c = 'US',
            recordtypeid = caseRecordTypeInfo.get('TBO Request').getRecordTypeId(),

            Accountid = RCIDAcc.id, contactId = cont.id, Master_Account_Name__c = inacc.id,
            Contact_phone_number__c = cont1.id);
        insert c;


        Case_Account__c caseAcc = new Case_Account__c();
        caseAcc.Case__c = c.Id;
        caseAcc.Account__c = RCIDAcc.Id;
        caseAcc.Service_type__c = 'Fiber';

        insert caseAcc;

        Work_Request__c testWorkReq = new Work_Request__c();
        testWorkReq.recordtypeid = wrRecordTypeInfo.get('TBO Work Request').getRecordTypeId();
        testWorkReq.Status__c = 'New';
        testWorkReq.Service_type__c = 'Fiber';
        testWorkReq.Case__c = c.Id;
        insert testWorkReq;

        system.assertEquals(testWorkReq.Service_type__c, caseAcc.Service_type__c);

        Test.setCurrentPageReference(new PageReference('Page.TBOCaseAccountLists'));

        System.currentPageReference().getParameters().put('Id', null);
        TBOWorkRequestEmailComponent e = new TBOWorkRequestEmailComponent();
        e.wrID = testWorkReq.id;
        Test.startTest();
        // start - added unit test for TboWorkRequestContrExtension class 26-May-2016 (UmeshA)
        
        ApexPages.StandardController sc = new ApexPages.standardController(testWorkReq);
        TboWorkRequestContrExtension tborc = new TboWorkRequestContrExtension(sc);
        tborc.error = 'Id is not valid';
        
        tborc.getCaseAccounts();
        //end - added unit test for TboWorkRequestContrExtension class 26-May-2016 (UmeshA)
        e.CaseAcc = null;
        List < Case_Account__c > events = e.CaseAcc;
        Test.stopTest();
        //System.assertNotEquals(testWorkReq.Id, e.wrID);

    }
    static testMethod void TestTBOWorkRequest_Negative_null() {
        ApexPages.StandardController sc = new ApexPages.standardController(new Case_Account__c());
        TboWorkRequestContrExtension tborc = new TboWorkRequestContrExtension(sc);
        tborc.workRequest = null;
        tborc.getCaseAccounts();        
    }
    static testMethod void TBOContractAssessmentEmailComponent_Negative_null() {
        Schema.DescribeSObjectResult tboSchema = Schema.SObjectType.Account;
        Map < String, Schema.RecordTypeInfo > AccountRecordTypeInfo = tboSchema.getRecordTypeInfosByName();
        Schema.DescribeSObjectResult tboCaseSchema = Schema.SObjectType.Case;
        Map < String, Schema.RecordTypeInfo > caseRecordTypeInfo = tboCaseSchema.getRecordTypeInfosByName();
        Schema.DescribeSObjectResult tboWRSchema = Schema.SObjectType.Work_Request__c;
        Map < String, Schema.RecordTypeInfo > wrRecordTypeInfo = tboWRSchema.getRecordTypeInfosByName();
        
        Account RCIDAcc = new Account(Name = 'testTBORCID', recordtypeid = AccountRecordTypeInfo.get('RCID').getRecordTypeId(), RCID__C = 'RCID123', Inactive__c = false, Billing_Account_Active_Indicator__c = 'Y');
        insert RCIDAcc;
        Account inacc = new Account(Name = 'outgoingTBORCID', recordtypeid = AccountRecordTypeInfo.get('RCID').getRecordTypeId(), RCID__C = 'RCID1343423', Inactive__c = false, Billing_Account_Active_Indicator__c = 'Y');
        insert inacc;
        contact cont = new contact();

        cont.LastName = 'Test';
        cont.AccountId = RCIDAcc.id;
        insert cont;
        Contact cont1 = new Contact();

        cont1.LastName = 'Test1';
        cont1.AccountId = inacc.id;
        insert cont1;
        Case c = new Case(Subject = 'testTBO', Created_Date__c = system.TODAY(), status = 'In Progress',
            TBO_In_Progress_Status__c = 'Customer Validation', Requested_Transfer_Date__c = System.Today(),
            Transfer_Type__c = 'abc', Other_Working_TELUS_Service__c = 'sddsds', Authorized_Outgoing_Permission__c = true,
            Directory_Information__c = 'Listing', Listing_Name__c = 'dsffsdff',
            Business_Type__c = 'official', Jurisdiction__c = 'bc', Enter_State__c = 'US',
            recordtypeid = caseRecordTypeInfo.get('TBO Request').getRecordTypeId(),

            Accountid = RCIDAcc.id, contactId = cont.id, Master_Account_Name__c = inacc.id,
            Contact_phone_number__c = cont1.id);
        insert c;
        // added by umesh for testing TBOContractAssessmentEmailComponent class
          TBOContractAssessmentEmailComponent abc = new TBOContractAssessmentEmailComponent();
          abc.tboID = c.id;
          List<Contract_Assessment__c> events = abc.ContractAssessmentList;
           System.assertEquals(abc.tboID,  c.id);
         // added by umesh for testing TBOContractAssessmentEmailComponent class      
    }
}