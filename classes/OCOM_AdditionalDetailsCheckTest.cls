@isTest

private class OCOM_AdditionalDetailsCheckTest {
    @testSetup
    static void testDataSetup() {
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select Account.Credit_Profile__c,AccountId,Shipping_Address_Contact__c,Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c,orderMgmtId__c FROM Order where id=:orderId];
        ordObj.orderMgmtId__c='ADC1234567890';
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        //ordObj.Shipping_Address__c='';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        update ordObj;
        
        Account anAccount = [select Account_Status__c from Account where id=:ordObj.AccountId limit 1];
        anAccount.Account_Status__c = 'ACTIVE';
        update anAccount;
        
        OrderItem anOrderItem=[select vlocity_cmt__RecurringTotal__c from OrderItem where OrderId=:orderId limit 1];
        anOrderItem.vlocity_cmt__JSONAttribute__c = '{"TELUSCHAR":[{"$$AttributeDefinitionStart$$":null,"attributeRunTimeInfo":{"value":null,"uiDisplayType":"Text","dataType":"Text"},"categorydisplaysequence__c":0,"customconfiguitemplate__c":null,"attributecloneable__c":false,"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P220000001L8cEAE","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":false,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":false,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":null,"isactive__c":false,"attributefilterable__c":false,"attributedisplaysequence__c":"23","attributeconfigurable__c":false,"attributeuniquecode__c":null,"categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q22000000HINREA4","attributeid__c":null,"objectid__c":"01t22000000KPsZAAW","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"attributeRunTimeInfo":{"value":" ","uiDisplayType":"Text","dataType":"Text"},"categorydisplaysequence__c":0,"customconfiguitemplate__c":null,"attributecloneable__c":true,"valuedescription__c":null,"valuedatatype__c":"Text","value__c":" ","uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P220000001LCBEA2","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":false,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":false,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Contact Name","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"25","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-4585","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q22000000HINREA4","attributeid__c":"a7R220000004Gi8EAE","objectid__c":"01t22000000KPsZAAW","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"attributeRunTimeInfo":{"value":null,"uiDisplayType":"Text","dataType":"Text"},"categorydisplaysequence__c":0,"customconfiguitemplate__c":null,"attributecloneable__c":true,"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P220000001L8WEAU","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":false,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":false,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Contact Number","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"10","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-4720","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q22000000HINREA4","attributeid__c":"a7R220000004GlFEAU","objectid__c":"01t22000000KPsZAAW","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"attributeRunTimeInfo":{"value":null,"uiDisplayType":"Text","dataType":"Text"},"categorydisplaysequence__c":0,"customconfiguitemplate__c":null,"attributecloneable__c":true,"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P220000001L8XEAU","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":false,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":false,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Contract No.","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"11","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-4721","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q22000000HINREA4","attributeid__c":"a7R220000004GlGEAU","objectid__c":"01t22000000KPsZAAW","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"attributeRunTimeInfo":{"selectedItem":{"value":"Yes","displayText":"Yes","id":"9145615224913186244"},"values":[{"value":"No","displayText":"No","id":"9145615224913186246"},{"value":"Yes","displayText":"Yes","id":"9145615224913186244"}],"default":[{"value":"Yes","displayText":"Yes","id":"9145615224913186244"}],"uiDisplayType":"Dropdown","dataType":"Picklist"},"categorydisplaysequence__c":0,"customconfiguitemplate__c":null,"attributecloneable__c":true,"valuedescription__c":null,"valuedatatype__c":"Picklist","value__c":"Yes","uidisplaytype__c":"Dropdown","rulemessage__c":null,"isrequired__c":true,"id":"a7P220000001L8TEAU","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":false,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":false,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Credit Assessment Required","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"6","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-4717","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q22000000HINREA4","attributeid__c":"a7R220000004Gl3EAE","objectid__c":"01t22000000KPsZAAW","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"attributeRunTimeInfo":{"default":"TELUS Offer","uiDisplayType":"Text","dataType":"Text"},"categorydisplaysequence__c":0,"customconfiguitemplate__c":null,"attributecloneable__c":true,"valuedescription__c":null,"valuedatatype__c":"Text","value__c":"TELUS Offer","uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":true,"id":"a7P220000001L8REAU","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Offering Category","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"2","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-4714","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q22000000HINREA4","attributeid__c":"a7R220000004Gl9EAE","objectid__c":"01t22000000KPsZAAW","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"attributeRunTimeInfo":{"default":"LW-TELUS-Offer","uiDisplayType":"Text","dataType":"Text"},"categorydisplaysequence__c":0,"customconfiguitemplate__c":null,"attributecloneable__c":true,"valuedescription__c":null,"valuedatatype__c":"Text","value__c":"LW-TELUS-Offer","uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":true,"id":"a7P220000001L8QEAU","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Offering ID","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"1","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-4463","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q22000000HINREA4","attributeid__c":"a7R220000004GdIEAU","objectid__c":"01t22000000KPsZAAW","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"attributeRunTimeInfo":{"value":"0","default":"0","uiDisplayType":"Text","dataType":"Text"},"categorydisplaysequence__c":0,"customconfiguitemplate__c":null,"attributecloneable__c":true,"valuedescription__c":null,"valuedatatype__c":"Text","value__c":"0","uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P220000001L8ZEAU","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":false,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":0,"ishidden__c":false,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"One-time Charges, NRC","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"13","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-4723","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q22000000HINREA4","attributeid__c":"a7R220000004GkyEAE","objectid__c":"01t22000000KPsZAAW","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"attributeRunTimeInfo":{"value":"0","default":"0","uiDisplayType":"Text","dataType":"Text"},"categorydisplaysequence__c":0,"customconfiguitemplate__c":null,"attributecloneable__c":true,"valuedescription__c":null,"valuedatatype__c":"Text","value__c":"0","uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":true,"id":"a7P220000001L8YEAU","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":false,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":0,"ishidden__c":false,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Recurring Charges, MRC","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"12","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-4722","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q22000000HINREA4","attributeid__c":"a7R220000004GjdEAE","objectid__c":"01t22000000KPsZAAW","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"attributeRunTimeInfo":{"uiDisplayType":"Date","dataType":"Date"},"categorydisplaysequence__c":0,"customconfiguitemplate__c":null,"attributecloneable__c":true,"valuedescription__c":null,"valuedatatype__c":"Date","value__c":"2017-03-06","uidisplaytype__c":"Date","rulemessage__c":null,"isrequired__c":false,"id":"a7P220000001L8aEAE","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Requested Due Date","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"21","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-4724","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q22000000HINREA4","attributeid__c":"a7R220000004GgFEAU","objectid__c":"01t22000000KPsZAAW","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"attributeRunTimeInfo":{"selectedItem":{"value":"WLN","displayText":"WLN","id":"9145615376313186289"},"values":[{"value":"LDIT","displayText":"LDIT","id":"9145615376313186293"},{"value":"WLS","displayText":"WLS","id":"9145615376313186291"},{"value":"WLN","displayText":"WLN","id":"9145615376313186289"}],"default":[{"value":"WLN","displayText":"WLN","id":"9145615376313186289"}],"uiDisplayType":"Dropdown","dataType":"Picklist"},"categorydisplaysequence__c":0,"customconfiguitemplate__c":null,"attributecloneable__c":true,"valuedescription__c":null,"valuedatatype__c":"Picklist","value__c":"WLN","uidisplaytype__c":"Dropdown","rulemessage__c":null,"isrequired__c":true,"id":"a7P220000001L8bEAE","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":false,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":false,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Responsible Provisioning Group","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"22","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-4725","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q22000000HINREA4","attributeid__c":"a7R220000004GjeEAE","objectid__c":"01t22000000KPsZAAW","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"attributeRunTimeInfo":{"value":" ","uiDisplayType":"Text","dataType":"Text"},"categorydisplaysequence__c":0,"customconfiguitemplate__c":null,"attributecloneable__c":true,"valuedescription__c":null,"valuedatatype__c":"Text","value__c":" ","uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P220000001L8VEAU","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":false,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":false,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Shipping Address","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"9","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-4719","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q22000000HINREA4","attributeid__c":"a7R220000004GiUEAU","objectid__c":"01t22000000KPsZAAW","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"attributeRunTimeInfo":{"selectedItem":{"value":"Yes","displayText":"Yes","id":"9145615364113186259"},"values":[{"value":"No","displayText":"No","id":"9145615364113186260"},{"value":"Yes","displayText":"Yes","id":"9145615364113186259"}],"default":[{"value":"No","displayText":"No","id":"9145615364113186260"}],"uiDisplayType":"Dropdown","dataType":"Picklist"},"categorydisplaysequence__c":0,"customconfiguitemplate__c":null,"attributecloneable__c":true,"valuedescription__c":null,"valuedatatype__c":"Picklist","value__c":"No","uidisplaytype__c":"Dropdown","rulemessage__c":null,"isrequired__c":false,"id":"a7P220000001L8UEAU","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":false,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":false,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Shipping Required","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"8","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-4718","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q22000000HINREA4","attributeid__c":"a7R220000004GhHEAU","objectid__c":"01t22000000KPsZAAW","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"attributeRunTimeInfo":{"selectedItem":{"value":"Fibre - FIFA","displayText":"Fibre - FIFA","id":"9146332526913370225"},"default":[],"values":[{"value":"Mobile","displayText":"Mobile","id":"9146332526913370223"},{"value":"Fibre - Non-FIFA","displayText":"Fibre - Non-FIFA","id":"9146332526913370224"},{"value":"Fibre - FIFA","displayText":"Fibre - FIFA","id":"9146332526913370225"},{"value":"Copper","displayText":"Copper","id":"9146332526913370226"}],"uiDisplayType":"Dropdown","dataType":"Picklist"},"categorydisplaysequence__c":0,"customconfiguitemplate__c":null,"attributecloneable__c":true,"valuedescription__c":null,"valuedatatype__c":"Picklist","value__c":null,"uidisplaytype__c":"Dropdown","rulemessage__c":null,"isrequired__c":true,"id":"a7P220000001L8SEAU","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":false,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":false,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Technology","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"4","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-4715","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q22000000HINREA4","attributeid__c":"a7R220000004GlSEAU","objectid__c":"01t22000000KPsZAAW","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"attributeRunTimeInfo":{"uiDisplayType":"Text","dataType":"Text"},"categorydisplaysequence__c":0,"customconfiguitemplate__c":null,"attributecloneable__c":true,"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P220000001L8dEAE","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Line Item External ID","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"4","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-4539","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q22000000HINREA4","attributeid__c":"a7R220000004GgeEAE","objectid__c":"01t22000000KPsZAAW","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"attributeRunTimeInfo":{"value":null,"uiDisplayType":"Text Area","dataType":"Text"},"categorydisplaysequence__c":0,"customconfiguitemplate__c":null,"attributecloneable__c":true,"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text Area","rulemessage__c":null,"isrequired__c":false,"id":"a7P220000001L9LEAU","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":false,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":false,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Comments","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"23","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-4563","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q22000000HINREA4","attributeid__c":"a7R220000004GfvEAE","objectid__c":"01t22000000KPsZAAW","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"attributeRunTimeInfo":{"uiDisplayType":"Text","dataType":"Text"},"categorydisplaysequence__c":0,"customconfiguitemplate__c":null,"attributecloneable__c":true,"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":false,"id":"a7P220000001L8eEAE","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":true,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":true,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Move Cross-Reference","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"10","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-4581","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q22000000HINREA4","attributeid__c":"a7R220000004GhkEAE","objectid__c":"01t22000000KPsZAAW","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"attributeRunTimeInfo":{"value":"jj","uiDisplayType":"Text","dataType":"Text"},"categorydisplaysequence__c":0,"customconfiguitemplate__c":null,"attributecloneable__c":true,"valuedescription__c":null,"valuedatatype__c":"Text","value__c":null,"uidisplaytype__c":"Text","rulemessage__c":null,"isrequired__c":true,"id":"a7P220000001LAsEAM","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":false,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":false,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Description","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"24","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-4748","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q22000000HINREA4","attributeid__c":"a7R220000004Gl4EAE","objectid__c":"01t22000000KPsZAAW","$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"attributeRunTimeInfo":{"selectedItem":{"value":"Standalone Wireless","displayText":"Standalone Wireless","id":"9146036527813916334"},"default":[],"values":[{"value":"TELUSHealth","displayText":"TELUSHealth","id":"9146036637713916365"},{"value":"Other","displayText":"Other","id":"9146036527813916340"},{"value":"Standalone Wireless","displayText":"Standalone Wireless","id":"9146036527813916334"}],"uiDisplayType":"Dropdown","dataType":"Picklist"},"categorydisplaysequence__c":0,"customconfiguitemplate__c":null,"attributecloneable__c":true,"valuedescription__c":null,"valuedatatype__c":"Picklist","value__c":null,"uidisplaytype__c":"Dropdown","rulemessage__c":null,"isrequired__c":true,"id":"a7P220000001LEpEAM","querylabel__c":null,"isquerydriven__c":false,"isreadonly__c":false,"querycode__c":null,"objecttype__c":"Product2","valueinnumber__c":null,"ishidden__c":false,"isconfigurable__c":true,"hasrule__c":false,"formatmask__c":null,"displaysequence__c":null,"attributedisplayname__c":"Type","isactive__c":true,"attributefilterable__c":false,"attributedisplaysequence__c":"26","attributeconfigurable__c":true,"attributeuniquecode__c":"ATTRIBUTE-4804","categoryname__c":"Characteristics","categorycode__c":"TELUSCHAR","attributecategoryid__c":"a7Q22000000HINREA4","attributeid__c":"a7R220000004Gn3EAE","objectid__c":"01t22000000KPsZAAW","$$AttributeDefinitionEnd$$":null}]}';
        anOrderItem.vlocity_cmt__RecurringTotal__c=400.00;
        update anOrderItem;

    }
    
    @isTest
    private static void validateCreditAssessmentTest() {
        Order ordObj = [SELECT Id FROM Order WHERE orderMgmtId__c='ADC1234567890' LIMIT 1];
        if (ordObj != null) {
            system.debug('OCOM_AdditionalDetailsCheckTest::validateCreditAssessmentTest() order id (' + ordObj.id + ')');
            OCOM_AdditionalDetailsCheck additionalDetailsCheck = new OCOM_AdditionalDetailsCheck();
            Map<String, Object> outputMap = new Map<String, Object>();
            Map<String, Object> inputMap = new Map<String, Object>();
            Map<String, Object> options = new Map<String, Object>();
            inputMap.put('ContextId', (Object)ordObj.id);
            Test.startTest();
            additionalDetailsCheck.invokeMethod('ValidateAndAmend', inputMap, outputMap, options);
            Test.stopTest();
            system.debug('OCOM_AdditionalDetailsCheckTest::validateCreditAssessmentTest() errorMsgs (' + additionalDetailsCheck.errorMsgs + ')');
            system.debug('OCOM_AdditionalDetailsCheckTest::validateCreditAssessmentTest() expectedErrorMessage (' + string.valueof(system.Label.OCOM_Credit_Assessment_Required) + ')');
           // system.assert(additionalDetailsCheck.errorMsgs.contains(string.valueof(system.Label.OCOM_Credit_Assessment_Required)), true);
        }
    }


    @isTest
    private static void amendContractsTest_withOutAmendStatus() {
        Order ordObj = [SELECT Id FROM Order WHERE orderMgmtId__c='ADC1234567890' LIMIT 1];
        List<product2> prod2UpdateList = new List<product2>();

        for(OrderItem OrdrItem : [Select Id,vlocity_cmt__JSONAttribute__c,PriceBookEntry.Product2.Id ,PriceBookEntry.product2.Sellable__c,
                                              Pricebookentry.Product2.vlocity_cmt__TrackAsAgreement__c
                                              FROM OrderItem WHERE OrderId = :ordObj.id 
                                              ORDER BY vlocity_cmt__LineNumber__c Limit 1]){ 
                                Product2 prod2Update = new product2(id = OrdrItem.PriceBookEntry.Product2.Id , vlocity_cmt__TrackAsAgreement__c = true, Sellable__c = true);
                                prod2UpdateList.add(prod2Update);
            }
            System.debug('prod2UpdateList_size()____::' + prod2UpdateList.size());
            
        if(prod2UpdateList != null && !prod2UpdateList.isEmpty()  ){
            update prod2UpdateList;
        }
        if (ordObj != null) {
            system.debug('OCOM_AdditionalDetailsCheckTest::amendContractsTest_negative() order id (' + ordObj.id + ')');
            OCOM_AdditionalDetailsCheck additionalDetailsCheck = new OCOM_AdditionalDetailsCheck();
            Map<String, Object> outputMap = new Map<String, Object>();
            Map<String, Object> inputMap = new Map<String, Object>();
            Map<String, Object> options = new Map<String, Object>();
            inputMap.put('ContextId', (Object)ordObj.id);
            Test.startTest();
            additionalDetailsCheck.amendContracts(inputMap,outputMap,options);
            system.assertEquals('SUCCESS',additionalDetailsCheck.sErrorMessage);
            additionalDetailsCheck.validateContract(null,null,null);
            Test.stopTest();
        }
    }
    @isTest
    private static void amendContractsTest_withAmendStatus() {
        Order ordObj = [SELECT Id FROM Order WHERE orderMgmtId__c='ADC1234567890' LIMIT 1];
        List<product2> prod2UpdateList = new List<product2>();
        List<OrderItem> oliToUpdateList = new List<orderItem>();
        for(OrderItem OrdrItem : [Select Id,vlocity_cmt__JSONAttribute__c,PriceBookEntry.Product2.Id ,PriceBookEntry.product2.Sellable__c,
                                              Pricebookentry.Product2.vlocity_cmt__TrackAsAgreement__c
                                              FROM OrderItem WHERE OrderId = :ordObj.id 
                                              ORDER BY vlocity_cmt__LineNumber__c Limit 1]){ 
                                Product2 prod2Update = new product2(id = OrdrItem.PriceBookEntry.Product2.Id , vlocity_cmt__TrackAsAgreement__c = true, Sellable__c = true);
                                prod2UpdateList.add(prod2Update);
                                OrdrItem.amendStatus__c = 'change';
                                oliToUpdateList.add(OrdrItem);
            }
        System.debug('prod2UpdateList_size()____112::' + prod2UpdateList.size());

        if(prod2UpdateList != null && !prod2UpdateList.isEmpty()  ){
            update prod2UpdateList;

        }
         if(oliToUpdateList != null && !oliToUpdateList.isEmpty()  ){
            update oliToUpdateList;
            
        }
        if (ordObj != null) {
            system.debug('OCOM_AdditionalDetailsCheckTest::amendContractsTest_withAmendStatus() order id (' + ordObj.id + ')');
            OCOM_AdditionalDetailsCheck additionalDetailsCheck = new OCOM_AdditionalDetailsCheck();
            Map<String, Object> outputMap = new Map<String, Object>();
            Map<String, Object> inputMap = new Map<String, Object>();
            Map<String, Object> options = new Map<String, Object>();
            inputMap.put('ContextId', (Object)ordObj.id);
            Test.startTest();
            additionalDetailsCheck.amendContracts(inputMap,outputMap,options);
            system.assertEquals('SUCCESS',additionalDetailsCheck.sErrorMessage);
            Test.stopTest();
        }
    }
     @isTest
    private static void amendContractsTest_withAmendStatus_negative() {
         Test.startTest();
        Order ordObj = [SELECT Id FROM Order WHERE orderMgmtId__c='ADC1234567890' LIMIT 1];
        List<product2> prod2UpdateList = new List<product2>();
        List<OrderItem> oliToUpdateList = new List<orderItem>();
        for(OrderItem OrdrItem : [Select Id,vlocity_cmt__JSONAttribute__c,PriceBookEntry.Product2.Id ,PriceBookEntry.product2.Sellable__c,
                                              Pricebookentry.Product2.vlocity_cmt__TrackAsAgreement__c
                                              FROM OrderItem WHERE OrderId = :ordObj.id 
                                              ORDER BY vlocity_cmt__LineNumber__c Limit 1]){ 
                                Product2 prod2Update = new product2(id = OrdrItem.PriceBookEntry.Product2.Id , vlocity_cmt__TrackAsAgreement__c = true, Sellable__c = true);
                                prod2UpdateList.add(prod2Update);
                                OrdrItem.amendStatus__c = 'change';
                                oliToUpdateList.add(OrdrItem);
            }
        Map<String,Object> TestinputMap = new Map<String,Object>();
        Map<String,Object> TestoutMap = new Map<String,Object>();
        TestDataHelper.testContractCreation();
        TestDataHelper.testContractObj.vlocity_cmt__OrderId__c = ordObj.id;
        TestDataHelper.testContractObj.Status='Contract Sent';
        update TestDataHelper.testContractObj;

        System.debug('prod2UpdateList_size()____112::' + prod2UpdateList.size());

        if(prod2UpdateList != null && !prod2UpdateList.isEmpty()  ){
            update prod2UpdateList;

        }
         if(oliToUpdateList != null && !oliToUpdateList.isEmpty()  ){
            update oliToUpdateList;
            
        }
        if (ordObj != null) {
            system.debug('OCOM_AdditionalDetailsCheckTest::amendContractsTest_withAmendStatus2() order id (' + ordObj.id + ')');
            OCOM_AdditionalDetailsCheck additionalDetailsCheck = new OCOM_AdditionalDetailsCheck();
            Map<String, Object> outputMap = new Map<String, Object>();
            Map<String, Object> inputMap = new Map<String, Object>();
            Map<String, Object> options = new Map<String, Object>();
            inputMap.put('ContextId', (Object)ordObj.id);
           
            vlocity_cmt.FlowStaticMap.FlowMap.put('forContractTestCoverageNoEnvelopException',true);
            additionalDetailsCheck.amendContracts(inputMap,outputMap,options);
            system.assertEquals('FAILURE',additionalDetailsCheck.sErrorMessage);
            Test.stopTest();
        }
    }
}