public class NAAS_Capture_OLI_ShippingAddController{
    public String oderId {get; set;}
    public boolean getisValMaploaded(){
       if(null !=canadaPostValCompMap && ((Map<string,string>)canadaPostValCompMap).size()>0)
       {
       
        return true;
        }
       else return false;
    }
    
    public Object canadaPostValCompMap {get; set;}
    Public Map<string,string> MapcanadaPost{get;set;}
    public String contactName {get; set;}
    //Defect# 13111 danish added on 5/16/2017
    public String contactEmail {get; set;}
    public String contactNumber {get; set;}
    //END
    Public static Boolean enableNextbtn {get; set;} 
    public List<OLIWrapper> OLIWrapperList {get;set;}
    //added by danish on 23/02/2017 to be used in NAAS_SubmitOrderPerSiteController to check if all addresses are valid.
    Public Boolean AllshippingAddressAreValid {get;set;}
    public Map<String,List<OLIWrapper>> OLIWrapperToServiceAddressMap;
    public Map<String,List<OLIWrapper>> getOLIWrapperToServiceAddressMap(){
        system.debug('!!!OLIWrapperToServiceAddressMap 0 '+ OLIWrapperToServiceAddressMap );
        OLIWrapperToServiceAddressMap = new Map<String,List<OLIWrapper>>(); 
        OLIWrapperList = new List<OLIWrapper>(); 
        //MapcanadaPost = new map<string,string>();
        //MapcanadaPost.putall((Map<string,string>)canadaPostValCompMap);
        return init(OderId); 
    }
    public AddressData PAData  { get; set; }
    //////
    
    
    ////
    public NAAS_Capture_OLI_ShippingAddController(){
        system.debug('%%######%% START: OCOM_Capture_OLI_ShippingAddController:OCOM_Capture_OLI_ShippingAddController  %%######%% ');
        system.debug('OCOM_Capture_OLI_ShippingAddController constructor oderId(' + canadaPostValCompMap + ')');          
        PAData  = new AddressData();
        MapcanadaPost = new map<string,string>();
       // isShippableOffersPresent=true;
       // system.debug('%%######%% END: OCOM_Capture_OLI_ShippingAddController:OCOM_Capture_OLI_ShippingAddController  %%######%% ');
        
    }
    public Map<String,List<OLIWrapper>> init(String orderId){
     system.debug('%%######%% START: OCOM_Capture_OLI_ShippingAddController:init  %%######%% ');
     system.debug('OCOM_Capture_OLI_ShippingAddController constructor oderId 1 (' + canadaPostValCompMap + ')');          
        Map<string,OrderItem>offerIDToOfferMap = new Map<string,OrderItem>();
        Map<String,List<OLIWrapper>>  OLIWrapperToServiceAddrMap1 = new Map<String,List<OLIWrapper>>();
        List<OrderItem> ordrItemRecs =[select id,Shipping_Contact_Email__c,Shipping_Contact_Number__c,Shipping_Address_Contact__c,pricebookentry.product2.OCOM_Shippable__c,Shipping_Address__c,
                                       vlocity_cmt__RootItemId__c,order.ServiceAddressSummary__c,order.Account.Name,order.OwnerId,pricebookentry.name,
                                       pricebookentry.product2.OCOM_Bookable__c,pricebookentry.product2.ProductSpecification__r.orderMgmtId__c,
                                       orderId,vlocity_cmt__JSONAttribute__c, vlocity_cmt__JSONNode__c,order.Service_Address__r.FMS_Address_ID__c,
                                       order.Service_Address__r.Street__c,order.Service_Address__r.Building_Number__c,order.Service_Address__r.City__c,
                                       order.Service_Address__r.Province__c,order.Service_Address__r.Postal_Code__c,order.Service_Address__r.COID__c,
                                       order.Service_Address__r.Country__c,Service_Address_Full_Name__c  from OrderItem where orderId =:orderId];
        for(OrderItem o:ordrItemRecs){
            system.debug('!!!OrderItem '+o.pricebookentry.name);
            offerIDToOfferMap.put(o.id,o);
            if(o.pricebookentry.product2.OCOM_Shippable__c == 'Yes'){
                OLIWrapper OLIWrapper = new OLIWrapper(o);
                if(String.isNotBlank(o.vlocity_cmt__JSONAttribute__c)){
                    OLIWrapper.deviceName = o.pricebookentry.Name;// (String) readOLICharacterstic(o.vlocity_cmt__JSONAttribute__c,'Device Name','Read',null);
                    OLIWrapper.subcriberName =((String) readOLICharacterstic(o.vlocity_cmt__JSONAttribute__c,'Subscriber First Name','Read',null))+' '+((String) readOLICharacterstic(o.vlocity_cmt__JSONAttribute__c,'Subscriber Last Name','Read',null) );
                    system.debug('OLIWrapper.subcriberName '+OLIWrapper.subcriberName);
                }
                // Canada api address check
                if(null !=canadaPostValCompMap && ((Map<string,string>)canadaPostValCompMap).size()>0 && MapcanadaPost.size() == 0){
                    MapcanadaPost.putall((Map<string,string>)canadaPostValCompMap);    
                }
                if(MapcanadaPost.containskey(o.Service_Address_Full_Name__c)){ //MapcanadaPost 
                    if(MapcanadaPost.get(o.Service_Address_Full_Name__c) == 'Valid' || (o.Service_Address_Full_Name__c != o.Shipping_Address__c)){
                        OLIWrapper.isProcessed = true;
                    }
                    else{
                        OLIWrapper.isProcessed = false;
                    }
                }
                //
                OLIWrapper.contactName= o.Shipping_Address_Contact__c;                
                OLIWrapper.contactNumber= o.Shipping_Contact_Number__c; 
                OLIWrapper.contactEmail= o.Shipping_Contact_Email__c; 
                OLIWrapperList.add(OLIWrapper);
            }            
            OLIWrapperToServiceAddrMap1.put(o.order.ServiceAddressSummary__c,OLIWrapperList);
        }
        if(null !=OLIWrapperList && OLIWrapperList.size()>0){
            for(OLIWrapper oWrap:OLIWrapperList){
                if(offerIDToOfferMap.containsKey(oWrap.oli.vlocity_cmt__RootItemId__c)){
                    oWrap.OfferName=(offerIDToOfferMap.get(oWrap.oli.vlocity_cmt__RootItemId__c)).pricebookentry.name;   
                }                
            }
        }
        system.debug('%%######%% END: OCOM_Capture_OLI_ShippingAddController:init  %%######%% ');
        enableNextbtn = checkIfAllShippingAddrAreValid();
        AllshippingAddressAreValid = checkIfAllShippingAddrAreValid();
        System.debug('checkIfAllShippingAddrAreValid>>>>>>>>>>gives>>>>>>>>>'+checkIfAllShippingAddrAreValid());
        return OLIWrapperToServiceAddrMap1;
    }
    
    public static Object readOLICharacterstic(String JsonChar,string charKeyName,String operationType,Map<String,String> charactersticValueMap){
        
        system.debug('%%######%% START: OCOM_Capture_OLI_ShippingAddController:readOLICharacterstic  %%######%% ');
        system.debug('!!! JsonChar '+JsonChar);
        try{ 
            if(String.isNotBlank(JsonChar)){
                Map<String, Object> attributes = (Map<String, Object>) JSON.deserializeUntyped(JsonChar);
                system.debug('JsonChar Map '+attributes);
                for (String key : attributes.keySet()) {
                    for (Object attribute : (List<Object>) attributes.get(key)) {
                        Map<String, Object> attributeMap = (Map<String, Object>) attribute;
                        String attributeName = (String) attributeMap.get('attributedisplayname__c');
                        if(attributeName.toUpperCase() == charKeyName.toUpperCase()){
                            String value = (String) attributeMap.get('value__c');
                            Map<String, Object> runTimeInfoMap = (Map<String, Object>) attributeMap.get('attributeRunTimeInfo');
                            system.debug('!!!! JsonChar attributeMap '+attributeMap);
                            if(String.isBlank(value)) {
                               value = (String) runTimeInfoMap.get('value'); 
                            }
                            system.debug('!!!! JsonChar value 0 '+value);                            
                            //String value = (String) attributeMap.get('value__c');                  
                            if(null != charactersticValueMap && charactersticValueMap.containsKey(attributeName) ){
                                if(operationType=='Update'){
                                   if(runTimeInfoMap.containsKey('value'))
                                    {runTimeInfoMap.put('value',charactersticValueMap.get(attributeName));}
                                   if(attributeMap.containskey('value__c'))  
                                    {attributeMap.put('value__c',charactersticValueMap.get(attributeName));}
                                }else{
                                    system.debug('!!!! JsonChar value 1 '+value+' '+operationType+' '+charactersticValueMap );
                                    system.debug('%%######%% END: OCOM_Capture_OLI_ShippingAddController:readOLICharacterstic  %%######%% ');
                                    return value;
                                } 
                                
                            }                   
                        }
                    }
                }
                if(operationType=='Update'){
                    system.debug('!!!! JsonChar value attributes '+attributes);
                    system.debug('%%######%% END: OCOM_Capture_OLI_ShippingAddController:readOLICharacterstic  %%######%% ');
                    return  JSON.serialize(attributes);}
                system.debug('!!!! JsonChar value 3');
                system.debug('%%######%% END: OCOM_Capture_OLI_ShippingAddController:readOLICharacterstic  %%######%% ');
                return ' ';
            }else{
                system.debug('%%######%% END: OCOM_Capture_OLI_ShippingAddController:readOLICharacterstic  %%######%% ');
                return JsonChar;
            }
            
        }catch(exception e){
            system.debug('!!!!! exception try 0 '+e);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.OCOM_ExceptionMessagePrefix+e.getMessage()));
            system.debug('%%######%% END: OCOM_Capture_OLI_ShippingAddController:readOLICharacterstic  %%######%% ');
            return JsonChar; 
            
        }
    }
    Public Pagereference SaveShipping()
    {
        system.debug('%%######%% START: OCOM_Capture_OLI_ShippingAddController:SaveShipping  %%######%% ');
        String sAddr = ' '+(String)PAData.addressLine1+ ' ' +(String)PAData.city+ ' ' +(String)PAData.province+ ' '+(String)PAData.postalCode+' ' +(String)PAData.country;
        List <OrderItem> selectedOLIs=New List<OrderItem>();
        Map<String,String> charactersticValueMap = new  Map<String,String>();
        charactersticValueMap.put('Shipping Address Characteristic',sAddr);
        charactersticValueMap.put('Shipping Address',sAddr);
        charactersticValueMap.put('Contact name',contactName); 
        charactersticValueMap.put('City of use',PAData.city);
        charactersticValueMap.put('Province of use',PAData.province);
      
            system.debug('!!!OLIWrapperToServiceAddressMap '+ OLIWrapperToServiceAddressMap );
            system.debug('!!! OLIWrapperList'+ OLIWrapperList);
            if(OLIWrapperList.size()>0){
                //  for(List<OLIWrapper> olList :OLIWrapperToServiceAddressMap.values())
                for(OLIWrapper ol :OLIWrapperList){
                    system.debug('!!! vlocity_cmt__JSONAttribute__c '+ol.oli.vlocity_cmt__JSONAttribute__c);
                    String JsonChar =ol.oli.vlocity_cmt__JSONAttribute__c;
                    if(ol.isSelected){
                        //Canada api address check
                        if(ol.isProcessed == false){
                           ol.isProcessed = true; 
                           MapcanadaPost.put(ol.oli.Service_Address_Full_Name__c,'Valid');
                        }
                        if(String.isNotBlank(contactName)){
                            ol.oli.Shipping_Address_Contact__c=ol.contactName=contactName;
                            system.debug('!!!! contactName'+contactName);
                        }
                    //Defect# 13111 danish added on 5/16/2017
                    //set Shipping Contact Email
                    if(String.isNotBlank(contactEmail)){
                        ol.oli.Shipping_Contact_Email__c=ol.contactEmail=contactEmail;
                        system.debug('!!!! contactEmail'+contactEmail);
                        
                    }
                    //set Shipping Contact Number
                    if(String.isNotBlank(contactNumber)){
                        ol.oli.Shipping_Contact_Number__c=ol.contactNumber=contactNumber;
                        system.debug('!!!! contactNumber'+contactNumber);
                        
                    }
                        if(String.isNotBlank(sAddr) && String.isNotBlank(PAData.addressLine1)){
                            ol.oli.Shipping_Address__c =sAddr;
                            if(String.isNotBlank(ol.oli.vlocity_cmt__JSONAttribute__c)){
                                for(string key :charactersticValueMap.keySet()){
                                    system.debug('!!!!debug before JSON '+JsonChar); 
                                    JsonChar=(String) readOLICharacterstic(JsonChar,key,'Update',charactersticValueMap);
                                    system.debug('!!!!debug After JSON '+JsonChar); 
                                }
                                ol.oli.vlocity_cmt__JSONAttribute__c= JsonChar;
                            }
                        }
                        system.debug('!!!!debug  '+ol.oli.Shipping_Address__c ); 
                        selectedOLIs.add(ol.oli);   
                    }
                }
                PAData.addressLine1 = ' ';
                PAData.city = ' ' ;
                PAData.province = ' ';
                PAData.postalCode =' ' ;
                PAData.country ='';
            }
            try{
                update selectedOLIs;
             //  enableNextbtn = checkIfAllShippingAddrAreValid();  
               System.debug('MAP that is checked for any invalid address>>>OLIWrapperList'+ OLIWrapperList);
                //  OLIWrapperList.clear();           
            }
            catch(exception e){
                system.debug('!!!!! exception try '+e);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,Label.OCOM_ExceptionMessagePrefix+e.getMessage()));
                system.debug('%%######%% END: OCOM_Capture_OLI_ShippingAddController:SaveShipping  %%######%% ');
                return null;
            }   
        system.debug('%%######%% END: OCOM_Capture_OLI_ShippingAddController:SaveShipping  %%######%% ');
        return null;
    } 
    Public Boolean checkIfAllShippingAddrAreValid(){
      
     
          Integer noOfOLIWithInvalidAddress = 0;
          Integer noOfOLIWithvalidAddress = 0;
          System.debug('OLI wrapper List>>'+OLIWrapperList);
          if(OLIWrapperList.size() > 0){
              for(OLIWrapper ol :OLIWrapperList){
                   If(ol.isProcessed == False){
                          noOfOLIWithInvalidAddress++;
                       }
                   If(ol.isProcessed == true){
                           noOfOLIWithvalidAddress++;
                   }
               }
          }
          if(noOfOLIWithInvalidAddress>0){
             return false;
          }  
          else{
          
          return true;
          }
     }  
   
    Public class OLIWrapper{
        Public boolean isSelected {get;set;}
        Public boolean isProcessed {get;set;}
        Public String deviceName {get;set;}
        Public String OfferName {get;set;}
        Public String subcriberName {get;set;}
        public OrderItem oli {get;set;}
        public String contactName  {get;set;}
        //Defect# 13111 danish added on 5/16/2017
        public String contactNumber  {get;set;}
        public String contactEmail  {get;set;}
        //END
        public OLIWrapper(orderItem o){
            this.isSelected= false;
            this.oli=o;
        }
    }
}