@isTest
private without sharing class VITILcareContactChangeCtlr_test {
     @isTest public static void testOther() {
        // just for coverage
             
        VITILcareContactChangeCtlr ctlr = new VITILcareContactChangeCtlr();

        System.assert(ctlr.getCategories().size() > 0);
        System.assert(ctlr.getRequestTypes().size() > 0);        
        System.assert(ctlr.createNewOnSubmit  == false);
        ctlr.createNewOnSubmit = true;
        System.assert(ctlr.createNewOnSubmit == true);
        ctlr.clear();
        ctlr.requestType = 'onlineStatus';
//        System.assert(ctlr.getRecordType('Dummy') == 'Generic');
        Attachment a  = ctlr.attachment;

        
        VITILcareContactChangeCtlr ctlr2 = new VITILcareContactChangeCtlr();

        System.assert(ctlr2.getCategories().size() > 0);
        System.assert(ctlr2.getRequestTypes().size() > 0);        
        System.assert(ctlr2.createNewOnSubmit  == false);
        ctlr2.createNewOnSubmit = true;
        System.assert(ctlr2.createNewOnSubmit == true);
        ctlr2.clear();
        ctlr2.requestType = 'onlineStatus';
       
    }
      @isTest public static  void testResetValues() {
        MBRTestUtils.createParentToChildCustomSettings();
        MBRTestUtils.createExternalToInternalCustomSettings();

        PageReference pref = Page.VITILcareContactChange;
        pref.getParameters().put('category', 'CCICatRequest1');
        Test.setCurrentPage(pref);

        VITILcareContactChangeCtlr ctlr = new VITILcareContactChangeCtlr();
        // reset values
        ctlr.caseSubmitted = true;
        ctlr.resetValues();
        System.assert(ctlr.selectedCategory == Label.MBRSelectCategory);

        ctlr.caseSubmitted = false;
       
//        pref.getParameters().put('category', null);
        ctlr.resetValues();
       // System.assert(ctlr.selectedCategory == 'Mainstream/Data(i.e T1, T3, DS0, etc)');
    }

    @isTest public static void testInitCheck() {
        User u = MBRTestUtils.createPortalUser('');        
        System.runAs(u) {
            VITILcareContactChangeCtlr ctlr = new VITILcareContactChangeCtlr();
            System.assert(ctlr.initCheck() == null);
        }
    }
    
@isTest(SeeAllData=false) static void testVITILcareContactPage() {
  Test.startTest();

    ENTPTestUtils.createCaseRootCauseCronicSetting();
        ENTPTestUtils.createExternalToInternalCustomSettings();
       MBRTestUtils.createParentToChildCustomSettings();
    
     VITILcareContactChangeCtlr ctlr = new VITILcareContactChangeCtlr();
    
    
     Test.stopTest();
}
     @isTest public static void testCreateNewCase() {
        MBRTestUtils.createExternalToInternalCustomSettings();
//        MBRTestUtils.createParentToChildCustomSettings();
//        MBRTestUtils.createTypeToFieldsCustomSettings();

        //System.runAs(u) { // sharing issue with this context

            PageReference pref = Page.VITILcareContactChange;
            pref.getParameters().put('category', 'CCICatRequest1');
            Test.setCurrentPage(pref);
        
            VITILcareContactChangeCtlr ctlr = new VITILcareContactChangeCtlr();
            
            ctlr.attachment.Name='Unit Test Attachment';
            ctlr.attachment.body=Blob.valueOf('Unit Test Attachment Body');

//System.debug('66: ctlr.createNewCase() + ' ctlr.createNewCase());        
//            System.assert(ctlr.createNewCase()ctlr.createNewCase() == null);

            ctlr.requestType = label.mbrSelectCategoryFirst;   
//            System.assert(ctlr.createNewCase() == null);
            
            ctlr.selectedCategory = 'Test';
            ctlr.requestType = label.mbrSelectCategoryFirst;  
//            System.assert(ctlr.createNewCase() == null); 
            
            ctlr.requestType = label.mbrSelectType;  
//            System.assert(ctlr.createNewCase() == null);


            ctlr.requestType = 'Voice - PRI';  
            ctlr.createNewCase();            //System.assert(ctlr.createNewCase() == null);

            ctlr.caseToInsert.recordtypeid = null;   
//            System.assert(ctlr.createNewCase() == null);

            ctlr.requestType = 'Mainstream/Data (i.e. T1, T3, DS0, etc)';
            ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId;   
            ctlr.createNewCase();
        //}
    }
      @isTest public static void testCreateParentCase() {
        MBRTestUtils.createExternalToInternalCustomSettings();

         VITILcareContactChangeCtlr ctlr = new  VITILcareContactChangeCtlr();

        ctlr.getRecordType('ENT Care Assure');
        ctlr.selectedCategory = 'Voice - PRI';
        ctlr.requestType = 'Voice - PRI';  
        ctlr.caseToInsert.Description = 'Test Description';
        ctlr.caseToInsert.Subject = 'Test Subject';
        ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId;   
        ctlr.createNewCase();

        List<Case> parentCases = [SELECT Id, CaseNumber, My_Business_Requests_Type__c FROM Case ORDER BY CreatedDate DESC];
        Case parentCase = null;
        if (parentCases.size() > 0) {
            parentCase = parentCases[0];
            parentCase.status = 'Closed';
            update parentCase;
        }

  ctlr.getParentCase();
          ctlr.getParentCaseType();
       ctlr.getParentCategory();
          ctlr.getCaseToInsert();
System.debug('parentCase: ' + parentCase);        
        // set pageReference and querystring params
        PageReference submitPageRef = Page.VITILcareContactChange;
        Test.setCurrentPage(submitPageRef);
       ApexPages.currentPage().getParameters().put('reopen', parentCase.CaseNumber);

        // instantiate a second controller for the child insert
       	 VITILcareContactChangeCtlr ctlr2 = new  VITILcareContactChangeCtlr();
        ctlr2.selectedCategory = 'Voice - PRI';
        ctlr2.requestType = 'Voice - PRI';
         ctlr2.caseToInsert.Description = 'Test Description';
        ctlr2.caseToInsert.Subject = 'Test Subject Child';
        ctlr2.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId;   
        ctlr2.createNewCase();
            
        Case childCase = [SELECT Id, Parent.Id, ParentId, CaseNumber FROM Case WHERE ParentId != null ORDER BY CreatedDate DESC LIMIT 1][0];
        System.assertEquals(parentCase.Id, childCase.Parent.Id);


    }
    
     @isTest
    public static void testPopulateDescrip(){
        MBRTestUtils.createExternalToInternalCustomSettings();
         Test.startTest();
        User u = MBRTestUtils.createENTPPortalUser('test-user-1', false, 'en_US');        
//        System.runAs(u) {
            VITILcareContactChangeCtlr ctlr = new VITILcareContactChangeCtlr();
            ctlr.requestType = 'Mainstream/Data (i.e. T1, T3, DS0, etc)';
            ctlr.selectedCategory = 'Enterprise';
            ctlr.caseToInsert.recordtypeid = Schema.SObjectType.Case.RecordTypeInfosByName.get('Generic').RecordTypeId; 
    
            ctlr.formWrapper.contactChangeInfo.requestPortal = 'Test';
            ctlr.formWrapper.contactChangeInfo.Firstname = 'Test 123';
            ctlr.formWrapper.contactChangeInfo.Lastname = 'Test';
            ctlr.formWrapper.contactChangeInfo.email = 'Test';
            ctlr.formWrapper.contactChangeInfo.phonenumber = 'Test';
            ctlr.formWrapper.contactChangeInfo.prefLanguage = 'Test';
            ctlr.formWrapper.contactChangeInfo.additionalNote = 'Test';
         
          
    
            ctlr.createNewCase();
      
        
            List<Case> cases = [SELECT Id, Description, Subject FROM Case];

Test.stopTest();       
    }
     @isTest public static void testUpdateCasePriorityToUrgent() {
        
        VITILcareContactChangeCtlr ctlr = new VITILcareContactChangeCtlr();

        String originValue = 'VITILcare';
      
        List<Case> cases = new List<Case>();
        cases.add( new Case(Subject='4', Status = 'New', Priority = 'Medium', Origin = originValue) ); // subject will be used for day of week
        cases.add( new Case(Subject='5', Status = 'New', Priority = 'Medium', Origin = originValue) );
        cases.add( new Case(Subject='6', Status = 'New', Priority = 'Medium', Origin = originValue) );
        cases.add( new Case(Subject='0', Status = 'New', Priority = 'Medium', Origin = originValue) );
        cases.add( new Case(Subject='0', Status = 'New', Priority = 'Urgent', Origin = originValue) );
        cases.add( new Case(Subject='0', Status = 'In Progress', Priority = 'Medium', Origin = originValue) );

        System.debug('akong: cases A: ' + cases);

        try {
            insert cases;
        } catch (Exception e) {
            System.debug('akong: case insert exception: ' + e);
        }

        Test.startTest();
        VITILcareContactChangeCtlr.updateCasePriorityToUrgent();
        Test.stopTest();

        List<Case> urgentCases = [SELECT Id, Priority FROM Case WHERE Priority = 'Urgent'];
      // System.assertEquals(1, urgentCases.size());
    }

}