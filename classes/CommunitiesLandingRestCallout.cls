@RestResource(urlMapping='/CommunitiesLandingRestCallout/*')
global without sharing class CommunitiesLandingRestCallout {
  @HttpPost
    global static String doPost(String roleId,String uuid,String cacheKey,String firstName,String lastName,String salesRepBusinessEmail,String salesRepPIN,String portalRoleName,String salesRepPreferredContactLanguageCode) {
        //String roleIdVal = req.params.get('roleId');
        RestRequest req = RestContext.request;
        
        RestResponse res = RestContext.response;
        
       // mylog__c l1=new mylog__c();
       // l1.logV__c='inside rest call method roleIdVal='+roleId+' cacheKey='+cacheKey;
        
       // insert l1;
		
		/*if(String.isNotBlank(roleId)){
		  updateRole(uuid, roleId);
		  return 'success';
		  
		}*/
        
        User user = [SELECT Id, UserName, FirstName, LastName, Email, Sales_Rep_ID__c, UserRoleId, Title, LanguageLocaleKey, LocaleSidKey FROM User WHERE FederationIdentifier = :uuid];        
        if (String.isNotBlank(firstName)){
            user.FirstName = firstName;
        } 
        if (String.isNotBlank(lastName)){
            user.LastName = lastName;
        }
        if (String.isNotBlank(salesRepBusinessEmail)){
            user.Email = salesRepBusinessEmail;
        }
        if (String.isNotBlank(salesRepPIN)){
            user.Sales_Rep_ID__c = salesRepPIN;
        }
        if (String.isNotBlank(portalRoleName)){
            user.Title = portalRoleName;
        }
        if (String.isNotBlank(salesRepPreferredContactLanguageCode)){
            String language = salesRepPreferredContactLanguageCode;
            if(language.equalsIgnoreCase('FR')){
                user.LanguageLocaleKey = 'fr';
                user.LocaleSidKey = 'fr_CA';
            } else {
                user.LanguageLocaleKey = 'en_US';
                user.LocaleSidKey = 'en_CA';
            }
        } else {
            user.LanguageLocaleKey = 'en_US';
            user.LocaleSidKey = 'en_CA';
        }
        
        update user;
        
        //updateRole(uuid, roleId);
        
        return 'success';
    }
    
    /*@TestVisible
    private static void updateRole(String uuid, String roleId) {
        system.debug('updating user role to -> ' + roleId);
        User user = [SELECT Id, UserRoleId FROM User WHERE FederationIdentifier = :uuid];
        user.UserRoleId = roleId;
        update(user);
    }   
*/

}