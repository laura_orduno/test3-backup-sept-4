/**
 * An apex page controller that exposes the site forgot password functionality
 */
public with sharing class MBRForgotPasswordCtlr {

	public String username {get; set;}   
    public String usernameError {get;set;}
    public String resetComplete {get;set;}
       
    public PageReference forgotPassword() {
        resetComplete = '';
        usernameError = '';
        List<user> cons = [select id from user where UserType = 'CspLitePortal' and username = :username and isactive=true];
        if(cons.size()<1) {
            usernameError = label.mbrInvalidUsername;
            return null;
        }
        
        boolean success = Site.forgotPassword(username);
        
        if (success) {
            resetComplete = label.mbrPasswordResetComplete;
        }
        else {
            usernameError = label.mbrInvalidUsername;
        }
        return null;
    }
     

}