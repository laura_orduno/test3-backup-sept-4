@isTest
private without sharing class SFDCUdateCaseHelper_test {

	@testSetup
	public static void setup() {
		Contact con = new Contact(LastName = 'Test', Email = 'Test@test.com');
		insert con;

		MBR_Case_Collaborator_Settings__c settings = MBR_Case_Collaborator_Settings__c.getOrgDefaults();
		settings.Dummy_Contact_id__c = con.Id;
		upsert settings;
	}

	@isTest(SeeAllData=false) static void testSFDCUpdateCaseHelper() {
		Test.startTest();
		trac_TriggerHandlerBase.blockTrigger = true;
		account a = new account(name = 'test acct');
		insert a;

		List<Case> testCases = ENTPTestUtils.createENTPCases(1, a.Id);

		List<Case> caseList1 = [SELECT createdby.firstname, createdby.lastname, Lynx_Ticket_Number__c, NotifyCustomer__c, NotifyCollaboratorString__c, OwnerId, Resolution_Details__c, My_Business_Requests_Type__c, ParentId, isClosed, Contact.FirstName, Contact.LastName, id,contact.name,subject,description,lastmodifieddate,case_resolution__c,createddate, caseNumber, recordType.name, account.name, type, status, priority, isEscalated, Escalation_History__c, (select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborator__r order by createddate desc), (select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate, ContentType from Attachments order by LastModifiedDate asc), (select createdby.firstname, createdby.lastname,Id,CommentBody,ParentId,CreatedDate,LastModifiedDate from casecomments where IsPublished = true order by CreatedDate asc), (select id, toaddress,fromaddress,fromname,ccaddress,messagedate,subject,htmlbody,textbody,hasattachment from emailmessages) FROM Case WHERE Id = :testCases[0].Id];
		Case cs1 = caseList1[0];

		SFDCUpdateCaseHelper.UpdateCase(cs1.caseNumber, 'closed', '22222222222', '', '');
		trac_TriggerHandlerBase.blockTrigger = false;
		Test.stopTest();
	}

	@isTest(SeeAllData=false) static void testSFDCUpdateCaseHelperTicketEventErr() {
		Test.startTest();
		trac_TriggerHandlerBase.blockTrigger = true;
		account a = new account(name = 'test acct');
		insert a;

		List<Case> testCases = ENTPTestUtils.createENTPCases(1, a.Id);

		List<Case> caseList1 = [SELECT createdby.firstname, createdby.lastname, Lynx_Ticket_Number__c, NotifyCustomer__c, NotifyCollaboratorString__c, OwnerId, Resolution_Details__c, My_Business_Requests_Type__c, ParentId, isClosed, Contact.FirstName, Contact.LastName, id,contact.name,subject,description,lastmodifieddate,case_resolution__c,createddate, caseNumber, recordType.name, account.name, type, status, priority, isEscalated, Escalation_History__c, (select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborator__r order by createddate desc), (select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate, ContentType from Attachments order by LastModifiedDate asc), (select createdby.firstname, createdby.lastname,Id,CommentBody,ParentId,CreatedDate,LastModifiedDate from casecomments where IsPublished = true order by CreatedDate asc), (select id, toaddress,fromaddress,fromname,ccaddress,messagedate,subject,htmlbody,textbody,hasattachment from emailmessages) FROM Case WHERE Id = :testCases[0].Id];
		Case cs1 = caseList1[0];

		SFDCUpdateCaseHelper.UpdateCase(cs1.caseNumber, 'In progress', '', 'This is an error', '');
		trac_TriggerHandlerBase.blockTrigger = false;
		Test.stopTest();
	}

	@isTest(SeeAllData=false) static void testSFDCUpdateCaseHelperTicketEventCom() {
		Test.startTest();
		trac_TriggerHandlerBase.blockTrigger = true;
		account a = new account(name = 'test acct');
		insert a;

		List<Case> testCases = ENTPTestUtils.createENTPCases(1, a.Id);

		List<Case> caseList1 = [SELECT createdby.firstname, createdby.lastname, Lynx_Ticket_Number__c, NotifyCustomer__c, NotifyCollaboratorString__c, OwnerId, Resolution_Details__c, My_Business_Requests_Type__c, ParentId, isClosed, Contact.FirstName, Contact.LastName, id,contact.name,subject,description,lastmodifieddate,case_resolution__c,createddate, caseNumber, recordType.name, account.name, type, status, priority, isEscalated, Escalation_History__c, (select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborator__r order by createddate desc), (select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate, ContentType from Attachments order by LastModifiedDate asc), (select createdby.firstname, createdby.lastname,Id,CommentBody,ParentId,CreatedDate,LastModifiedDate from casecomments where IsPublished = true order by CreatedDate asc), (select id, toaddress,fromaddress,fromname,ccaddress,messagedate,subject,htmlbody,textbody,hasattachment from emailmessages) FROM Case WHERE Id = :testCases[0].Id];
		Case cs1 = caseList1[0];
		ENTPTestUtils.createComment(testCases[0].Id, false, false);

		CaseComment fID = [Select id,CommentBody,Createdby.LastName,Createdby.firstname, CreatedDate from CaseComment where ParentId = :cs1.Id];
		ID CommentID = fID.Id;
		String CommentBodyGet = fID.CommentBody;
		SFDCUpdateCaseHelper.UpdateCase(cs1.caseNumber, 'In progress', '222222222', 'This is an error', CommentID);
		trac_TriggerHandlerBase.blockTrigger = false;
		Test.stopTest();
	}

	@isTest(SeeAllData=false) static void testSFDCUpdateCaseHelperTicketEventComNul() {
		Test.startTest();
		trac_TriggerHandlerBase.blockTrigger = true;
		account a = new account(name = 'test acct');
		insert a;

		List<Case> testCases = ENTPTestUtils.createENTPCases(1, a.Id);

		List<Case> caseList1 = [SELECT createdby.firstname, createdby.lastname, Lynx_Ticket_Number__c, NotifyCustomer__c, NotifyCollaboratorString__c, OwnerId, Resolution_Details__c, My_Business_Requests_Type__c, ParentId, isClosed, Contact.FirstName, Contact.LastName, id,contact.name,subject,description,lastmodifieddate,case_resolution__c,createddate, caseNumber, recordType.name, account.name, type, status, priority, isEscalated, Escalation_History__c, (select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborator__r order by createddate desc), (select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate, ContentType from Attachments order by LastModifiedDate asc), (select createdby.firstname, createdby.lastname,Id,CommentBody,ParentId,CreatedDate,LastModifiedDate from casecomments where IsPublished = true order by CreatedDate asc), (select id, toaddress,fromaddress,fromname,ccaddress,messagedate,subject,htmlbody,textbody,hasattachment from emailmessages) FROM Case WHERE Id = :testCases[0].Id];
		Case cs1 = caseList1[0];

		SFDCUpdateCaseHelper.UpdateCase(cs1.caseNumber, 'In progress', '222222222', 'This is an error', NULL);
		trac_TriggerHandlerBase.blockTrigger = false;
		Test.stopTest();
	}

	@isTest(SeeAllData=false) static void testSFDCUpdateCaseHelperTicketEventnocase() {
		Test.startTest();
		trac_TriggerHandlerBase.blockTrigger = true;
		account a = new account(name = 'test acct');
		insert a;

		List<Case> testCases = ENTPTestUtils.createENTPCases(1, a.Id);

		List<Case> caseList1 = [SELECT createdby.firstname, createdby.lastname, Lynx_Ticket_Number__c, NotifyCustomer__c, NotifyCollaboratorString__c, OwnerId, Resolution_Details__c, My_Business_Requests_Type__c, ParentId, isClosed, Contact.FirstName, Contact.LastName, id,contact.name,subject,description,lastmodifieddate,case_resolution__c,createddate, caseNumber, recordType.name, account.name, type, status, priority, isEscalated, Escalation_History__c, (select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborator__r order by createddate desc), (select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate, ContentType from Attachments order by LastModifiedDate asc), (select createdby.firstname, createdby.lastname,Id,CommentBody,ParentId,CreatedDate,LastModifiedDate from casecomments where IsPublished = true order by CreatedDate asc), (select id, toaddress,fromaddress,fromname,ccaddress,messagedate,subject,htmlbody,textbody,hasattachment from emailmessages) FROM Case WHERE Id = :testCases[0].Id];
		Case cs1 = caseList1[0];
		ENTPTestUtils.createComment(testCases[0].Id, false, false);

		CaseComment fID = [Select id,CommentBody,Createdby.LastName,Createdby.firstname, CreatedDate from CaseComment where ParentId = :cs1.Id];
		ID CommentID = fID.Id;
		String CommentBodyGet = fID.CommentBody;
		SFDCUpdateCaseHelper.UpdateCase('TELUS-SFDC', 'In progress', cs1.Lynx_Ticket_Number__c, 'This is an error', CommentID);

		trac_TriggerHandlerBase.blockTrigger = false;
		Test.stopTest();
	}

	@isTest(SeeAllData=false) static void testSFDCUpdateCaseHelperTicketEventnocomment() {
		Test.startTest();
		trac_TriggerHandlerBase.blockTrigger = true;
		account a = new account(name = 'test acct');
		insert a;

		List<Case> testCases = ENTPTestUtils.createENTPCases(1, a.Id);

		List<Case> caseList1 = [SELECT createdby.firstname, createdby.lastname, Lynx_Ticket_Number__c, NotifyCustomer__c, NotifyCollaboratorString__c, OwnerId, Resolution_Details__c, My_Business_Requests_Type__c, ParentId, isClosed, Contact.FirstName, Contact.LastName, id,contact.name,subject,description,lastmodifieddate,case_resolution__c,createddate, caseNumber, recordType.name, account.name, type, status, priority, isEscalated, Escalation_History__c, (select Active__c,Collaborator_Email__c, Collaborator_Name__c, Role__c from Collaborator__r order by createddate desc), (select createdby.firstname, createdby.lastname,Id,Name,Description,ParentId,CreatedDate,LastModifiedDate, ContentType from Attachments order by LastModifiedDate asc), (select createdby.firstname, createdby.lastname,Id,CommentBody,ParentId,CreatedDate,LastModifiedDate from casecomments where IsPublished = true order by CreatedDate asc), (select id, toaddress,fromaddress,fromname,ccaddress,messagedate,subject,htmlbody,textbody,hasattachment from emailmessages) FROM Case WHERE Id = :testCases[0].Id];
		Case cs1 = caseList1[0];
		ENTPTestUtils.createComment(testCases[0].Id, false, false);

		CaseComment fID = [Select id,CommentBody,Createdby.LastName,Createdby.firstname, CreatedDate from CaseComment where ParentId = :cs1.Id];
		ID CommentID = fID.Id;

		String CommentBodyGet = fID.CommentBody;
		//delete fId;

		SFDCUpdateCaseHelper.UpdateCase(cs1.caseNumber, 'In progress', cs1.Lynx_Ticket_Number__c, 'This is an error', '1234567');
		trac_TriggerHandlerBase.blockTrigger = false;
		Test.stopTest();
	}
}