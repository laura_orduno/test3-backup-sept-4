/**
 * Created by dpeterson on 2/5/2018.
 */

public abstract class Portal_GetHelpController {

	private String portal_Type_EN;
	private String portal_Type_FR;

	public Boolean singleQuestion { get; set; }
	public String questionId { get; set; }
	public String CatName { get; set; }
	public String userLanguage {
		get {
			return PortalUtils.getUserLanguage();
		}
		set;
	}
	public Map<Id, CCIGetHelp__c> QuestionsList { get; set; }

	// used for custom exception messages
	public transient String pageError { get; set; }
	public AggregateResult[] results;

	public Portal_GetHelpController(String s) {
		portal_Type_EN = s;
		portal_Type_FR = s + '-fr';

		singleQuestion = false;
		if (ApexPages.currentPage().getParameters().get('q') != null) {
			questionId = ApexPages.currentPage().getParameters().get('q');
			singleQuestion = true;
		}

	}

	public AggregateResult[] getResults() {
		if (userLanguage == 'en_US') {
			return [
					select Count(Id) aaa, Category__c bbb, name
					from CCIGetHelp__c
					where Published__c = true and Portal__c = :PORTAL_TYPE_EN
					GROUP BY name, Category__c
					ORDER BY name
			];
		} else {
			return [
					select Count(Id) aaa, Category__c bbb, name
					from CCIGetHelp__c
					where Published__c = true and Portal__c = :PORTAL_TYPE_FR
					GROUP BY name, Category__c
					ORDER BY name
			];
		}

	}

	public List<CCIGetHelp__c> getQuestions() {
		String portal_string;

		if (userLanguage == 'en_US') {
			portal_string = PORTAL_TYPE_EN;
		} else {
			portal_string = PORTAL_TYPE_FR;
		}

		String queryString = 'SELECT Id, Question__c, Category__c, Answer__c FROM CCIGetHelp__c ';
		if (String.isNotEmpty(questionId)) {
			queryString += ' WHERE Id = \'' + questionId + '\' Order By Name';
		} else {
			queryString += ' WHERE Published__c = true';

			if (String.isNotEmpty(portal_string)) {
				queryString += ' AND Portal__c INCLUDES (\'' + portal_string + '\')';
			}
			queryString += ' ORDER BY Name LIMIT 50';
		}

		List<CCIGetHelp__c> allQuestions = (List<CCIGetHelp__c>) database.query(queryString);

		return(allQuestions);
	}

	public void setResults(AggregateResult[] results) {
		this.results = results;
	}

	public CCIGetHelp__c selectedQuestion {
		get {
			String urlQuestionNumber = ApexPages.currentPage().getParameters().get('questionNumber');
			selectedQuestion = [SELECT Question__c, Id, Answer__c from CCIGetHelp__c where Id = :urlQuestionNumber LIMIT 1];
			return selectedQuestion;
		}
		set;
	}

	public String[] getCategories() {
		String[] CList = new List<String>();
		Schema.sObjectType objType = CCIGetHelp__c.getSObjectType();
		Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
		Map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
		List<Schema.PicklistEntry> values = fieldMap.get('Category__c').getDescribe().getPickListValues();
		for (Schema.PicklistEntry a : values) {
			String CatValueT = a.getValue();
			System.debug('Cat Value: ' + CatValueT);
			if (userLanguage == 'en_US') {
				if ((CatValueT != 'Submitting requests') && (CatValueT != 'Managing requests') && (CatValueT != 'Questions générales') && (CatValueT != 'Envoi de billets') && (CatValueT != 'Profil et paramètres') && (CatValueT != 'Gestion des billets')) {
					CList.add(a.getValue());
				}
			} else {
				if ((CatValueT != 'Submitting requests') && (CatValueT != 'Managing requests') && (CatValueT != 'General questions') && (CatValueT != 'Submitting tickets') && (CatValueT != 'Managing tickets') && (CatValueT != 'Profile and settings')) {
					CList.add(a.getValue());
				}
			}

		}
		return(CList);
	}
}