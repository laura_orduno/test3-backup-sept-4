/**
* Description: This is a common value object pattern, which will store trouble ticket details from multiple sources.
  All these attributes will have its getter and setter methods defined in it.  
* Author: Sandip Chaudhari (IBM) / Nitin Saxena (IBM)
* Date: 07-March-2016
* Project/Release: Trouble Ticket Aggregation
* Change Control Information:
* Date Who Reason for the change
*/
public class TicketVO{
    
    /* Class to hold the data received from various system. 
    * Also this class implements the Comparable interface to sort the data by modified date  
    */
    public static final Integer TIMEOUT_INT_SECS = 120;
    public static final string SRC_TTODS = 'TTODS';
    public static final string SRC_SRM = 'SRM';
    public static final string SRC_LavaStorm = 'LavaStorm';
    
    public class TicketData implements Comparable {
        // constructor to initiate the activity list
        public TicketData (){
            ttActivity = new List<TTActivity>();
            workLogList = new List<LavaStormWorkLogDetails>();
        }
        
        // Implement the compareTo() method
        public Integer compareTo(Object compareTo) {
            TicketData compareToEmp = (TicketData)compareTo;
            Integer returnValue = 0;
            if (sortOrder > compareToEmp.sortOrder){
                returnValue = 1;
            }else if(sortOrder < compareToEmp.sortOrder){
                returnValue = -1;
            }else{
                if (createDate < compareToEmp.createDate){
                    returnValue = 1;
                }else if(createDate > compareToEmp.createDate){
                    returnValue = -1;
                }
            }
            return returnValue;      
        }
        public string sourceSystem {get;set;} // Either SRM, TTODS or LavaStorm 
        public string sfId {get;set;} // For LavaStorm
        /* Common - TTODS was the starting point for the fields listed below 
        * The comments at the end of each field represent an example mapping from various ticketing systems
        * These mapping comments beside each field represents a scratch pad for the various source systems
        * For the actual mapping, look in the helper class that does the mapping
        */
        public List<TTActivity> ttActivity {get;set;}
        public List<LavaStormWorkLogDetails> workLogList {get;set;}
        public Integer sortOrder{get;set;}
        public String TELUSTicketId {get;set;} //TTODS, SRM-number_x, LAVAStorm - Trouble_Id__c
        public String RelatedTicketId {get;set;} // TTODS, SFDC-ParentId,Reference_Case_Nbr__c
        public String description {get;set;} // TTODS, SFDC-Description, LAVAStorm - Problem_Description_Details__c
        public String sourceTypeCode {get;set;} // TTODS, SRM-systemOriginId, WBS_Case_Origin__c
        public String ticketTypeCode {get;set;} // TTODS, SRM-typeCode, LAVAStorm - Type__c
        public String priorityCode {get;set;} // TTODS, SRM-priorityCode, LAVAStorm - Prioirity__c
        public String statusCode {get;set;} // TTODS, SRM-statusCode LAVAStorm - Status__c
        public String subStatusCode {get;set;} // TTODC,  
        public DateTime customerDueDate {get;set;} // TTODC, 
        public DateTime closeDate {get;set;} // TTODC, SRM-closureDateTime, LAVAStorm - Closed_Date__c
        public String closeDateStr{get;set;}
        public String troubleTypeCode {get;set;} // TTODS, SRM-typeCode, LAVAStorm - Type__c
        public DateTime createDate {get;set;} // TTODC, SRM-creationDateTime, LAVAStorm - Trouble_Date__c
        public String createDateStr{get;set;}
        public DateTime modifyDateTime {get;set;} // TTODC, SRM-statusDateTime
        public String createdByName {get;set;} // TTODC, SRM-representativeName, LAVAStorm - Submitter_Name__c
        public String assignToWorkgroupName {get;set;} // TTODC, SRM-representativeName, LAVAStorm - Assigned_Group__c
        public String assignToBIN {get;set;} // TTODC- controllingWorkGroupCode, SRM-controllingWorkGroupCode, LAVAStorm - Assigned_To_BIN__c
        public String comments {get;set;}  // TTODC, LAVAStorm - Short_Description__c
        public String toolTipComments {get;set;} // used to show tool tip on minipanel
        public String ticketCustomerContact {get;set;} // TTODC - store full name and contact number, SRM -
        public String customerAddress {get;set;}
        public String street{get;set;}
        public String city{get;set;}
        public String province{get;set;}
        public String country{get;set;}
        public String postalCode{get;set;}
        public String resoultionDetails{get;set;}
        public DateTime timeOfResoultion {get;set;}
        public string CaseOrigin {get;set;} //add by Rob 10/19/2017
        //End Common    
    
        //LavaStorm
        public String mobileNumber {get;set;}
        public String sim{get;set;}
        public String telusNumber {get;set;}
        public String serialNumber {get;set;}
        public String contactName {get;set;}
        public String emailAddress {get;set;}
        public String contactNumber {get;set;}
        public String technology {get;set;}
        public String banType {get;set;}
        public String banSubType {get;set;}
        public String banSegment {get;set;}
        public String mdsSupportLevel {get;set;}
        public String resolutionCode1 {get;set;}
        public String resolutionCode2 {get;set;}
        public String resolutionCode3 {get;set;}
        public String resolvedGroup {get;set;}
        public String resolvedBy {get;set;}
        public String submitterGroup {get;set;}
        public String statusReason {get;set;}
        public String equipmentTypeCode {get;set;}
        public String muleVendorName {get;set;}
        public String muleModel {get;set;}
        public String muleProductName {get;set;}
        public String requestorInfoStatus {get;set;}
        public DateTime WorkLogCreateDate {get;set;}
        public String WorkLogDetails {get;set;}
        public String WorkLogFullName {get;set;}
        public String WorkLogSummary {get;set;}
        public String WorkLogtype {get;set;}
                
        //End LavaStorm
        // TTODS related
        public String severityCode {get;set;}
        public DateTime eventStartDate {get;set;}
        public DateTime eventEndDate {get;set;}
        public String specialInstructionComment {get;set;}
        public String repeatFlag {get;set;}
        public String chronicFlag {get;set;}
        public String CRTCFlag {get;set;}
        public String WFMWoID {get;set;}
        public String ETTSFlag {get;set;}
        public String GPONFlag {get;set;}
        public String loopBondedFlag {get;set;}
        public DateTime appointmentStartDate {get;set;}
        public Decimal appointmentDuration {get;set;}
        public String lastDispatchSystem {get;set;}
        public String SLAFlag {get;set;}
        public String specialStudies {get;set;}
        public String SMCAlliance {get;set;}
        public String outageType {get;set;}
        public Decimal estimatedOutage {get;set;}
        public String currentEscalation {get;set;}
        public String mainPhoneNumber {get;set;}
        public String accessInformation {get;set;}
        public String dotNetCustomerUserID {get;set;}
        public String stationField {get;set;}
        public String modemType {get;set;}
        public String contactMode {get;set;}
        public String workforceRequired {get;set;}
        public DateTime nextEscalationDateTime {get;set;}
        public String NTMSReferredFlag {get;set;}
        public String troubleticketRelationTypeCode {get;set;}
        public String lineTestResult {get;set;}
        public String lineTestOhmsShort {get;set;}
        public String lineTestCapacitance {get;set;}
        public DateTime plannedStartDateTime {get;set;}
        public DateTime plannedEndDateTime {get;set;}
        public String controllingWorkGroupCode {get;set;}
        public String nonPubFlag {get;set;}
        public String level1Code {get;set;}
        public String level2Code {get;set;}
        public String level3Code {get;set;}
        public String lineOfBusinessCode {get;set;}
        public String conditionCode {get;set;}
        // End TTODS
    
        //SRM Related 
        public String finalServiceCode {get;set;}
        public String agencyCode {get;set;}
        public String proximityCode {get;set;}
        public String serviceClassificationCode {get;set;}
        public String customerName {get;set;}
        public String callCardModeCode {get;set;}
        public String customerPhoneNumber {get;set;}
        public String customerPhoneNumberExt {get;set;}
        public String npaNumber {get;set;}
        public String customerId {get;set;}
        public String customerUserId {get;set;}
        public String customerServiceIndicatorsText {get;set;}
        public String customerTypeText {get;set;}
        public String customerTypeCode {get;set;}
        public String customerAccountNumber {get;set;}
        public String customerBillingNumber {get;set;}
        public String productGroupNumber {get;set;}
        public String serviceTypeCode {get;set;}
        public String level5Code {get;set;}
        public String dispatchLevelCode {get;set;}
        public String responsibleGroupCode {get;set;}
        public String circuitNumber {get;set;}
        public String accountTypeCode {get;set;}
        // End SRM
        
       // Internal Case Related
        public String escalation{get;set;}
        public String milestoneStatus{get;set;}
       // end Case
    }
    // Class to show work log details
    public class LavaStormWorkLogDetails implements Comparable{
        public Integer compareTo(Object compareTo) {
            LavaStormWorkLogDetails compareToEmp = (LavaStormWorkLogDetails)compareTo;
            if (WorkLogCreateDate == compareToEmp.WorkLogCreateDate) return 0;
            if (WorkLogCreateDate > compareToEmp.WorkLogCreateDate) return 1;
            return -1;       
        }
        public DateTime WorkLogCreateDate {get;set;}
        public String WorkLogDetails {get;set;}
        public String WorkLogFullName {get;set;}
        public String WorkLogSummary {get;set;}
        public String WorkLogtype {get;set;}
        public String workLogId {get;set;}
    }
    // Class to collect Activity History for specfic ticket
    public class TTActivity implements Comparable{
        // Implement the compareTo() method
        public Integer compareTo(Object compareTo) {
            TTActivity compareToEmp = (TTActivity)compareTo;
            if (actvitiyLastUpdateTimeTTODS == compareToEmp.actvitiyLastUpdateTimeTTODS) return 0;
            if (actvitiyLastUpdateTimeTTODS < compareToEmp.actvitiyLastUpdateTimeTTODS) return 1;
            return -1;       
        } 
        public String TELUSTroubleTicketId {get;set;}
        public String activityTypeCode {get;set;}
        public String activityCategoryCode {get;set;}
        public String TELUSActivityId {get;set;}
        public String statusCode {get;set;}
        public String createdBy {get;set;}
        public String assignedTo {get;set;}
        public String resolutionCondition {get;set;}
        public String workforceRequired {get;set;}
        public Decimal workgroupTime {get;set;}
        public Decimal totalActivityTime {get;set;}
        public String CWCTimeType {get;set;}
        public Decimal CWCTotalTimeHours {get;set;}
        public Decimal CWCTotalTimeMinutes {get;set;}
        public Decimal CWCEquipmentCost {get;set;}
        public String assignToWorkgroup {get;set;}
        public String activityOwner {get;set;}
        public String activityComments {get;set;}
        public String resolutionCode1 {get;set;}
        public String resolutionCode2 {get;set;}
        public String resolutionCode3 {get;set;}
        public String notifyCustomerFlag {get;set;}
        public DateTime plannedStartDateTime {get;set;}
        public DateTime plannedCompleteDateTime {get;set;}
        public DateTime assignedDateTime {get;set;}
        public DateTime actualCompleteDateTime {get;set;}
        public String actualCompleteDateTimeStr {get;set;}
        public DateTime actualStartDateTime {get;set;}
        public DateTime closedDate {get;set;}
        public DateTime activityCreateDateTime {get;set;}
        public String externalTTSystemSequenceNumber {get;set;}
        public String activityIdTypeCode {get;set;}
        public String categoryCode {get;set;}
        public String classCode {get;set;}
        public String completedFlag {get;set;}
        public String escalationTypeCode {get;set;}
        public String overDueFlag {get;set;}
        public String alarmFlag {get;set;}
        public DateTime modifiedDateTime {get;set;}
        public DateTime actvitiyLastUpdateTimeTTODS {get;set;}
        public String actvitiyLastUpdateTimeTTODSStr {get;set;}
    }
    
     public class ServiceRequestAndResponseParameter{
         // Request PArameters
         public String accountId;  // Request Parameter for salesforce Id of account record
         public String TIDValue;  // Request Parameter
         public String RCIDValue; // Request Parameter
         public Integer recordLimits; // Request Parameter to set record limit to show on page
         public String callBackMethodName; // Request Parameter to send name of callback method for continuation framework
         public String source; // Request Parameter to send name of source system
         public String ticketIdToSearch; // Request Parameter to search individual ticket
         public Integer upToXLatestModified; // Request Parameter - Set number of records need to fetch from external service
         public Boolean isCallForLavaStormIndividualTT = false;  // Request Parameter for salesforce Id of account record
         
         //Output-Response attributes
         public transient List<TicketVO.TicketData> lstTickets = new List<TicketVO.TicketData>(); //Response Parameter
         public AsyncSmb_TTODS_troubleticket.TroubleTicketListFuture ttListFuture = new AsyncSmb_TTODS_troubleticket.TroubleTicketListFuture(); // response for TTODS
         public AsyncSRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsByRegionalCustomerIdResponse_elementFuture srmTTListFuture 
                     = new AsyncSRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsByRegionalCustomerIdResponse_elementFuture(); // response for SRM
         public AsyncSRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsByTicketNumberResponse_elementFuture srmTTDetailFuture 
                     = new AsyncSRM1_2_TQTroubleTicketSvcReqRes_v1.searchTroubleTicketsByTicketNumberResponse_elementFuture(); // response for SRM - Individual Ticket
         
     }    
}