@isTest 
private class OCOM_CustomEligibilityHookHelperTest {


    public void setupData(){
        
    
    }
    
    
    public static testmethod void checkoutReadinessOnOliUpdationTest(){
        Profile p = [select id from profile where name='Standard User'];
        User testUser = new User(alias = 'test123', email='ocom_test123qwert@telus.com', emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', localesidkey='en_US', profileid = p.Id, country='Canada', timezonesidkey='America/Los_Angeles', username='ocom_test123qwert@telus.com');
        insert testUser;
        
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select Account.Credit_Profile__c,AccountId, Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c,orderMgmtId__c FROM Order where id=:orderId];
        ordObj.orderMgmtId__c='OS1234567890';
        ordObj.status='Draft';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test address';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        ordObj.Sales_Representative__c=testUser.id;
        update ordObj;
        system.debug('submitOrderTest2::ordObj('+ordObj+')(1)');
        
        Account newAccount = new Account(Name='BillingTestAccount', CustProfId__c='123456');
        insert newAccount;
        system.debug('submitOrderTest2::newAccount('+newAccount+')');
        
        Product2 newProduct = new Product2(name='Test Product', productcode='TESTCODE', isActive=true, OCOM_Shippable__c='Yes');
        newProduct.vlocity_cmt__JSONAttribute__c = ' {"ETL":[{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t23000000L20fAAC","attributeid__c":"a7R23000000A72CEAS","attributecategoryid__c":"a7Q230000008TfKEAU","categorycode__c":"ETL","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-111","attributeconfigurable__c":true,"attributedisplaysequence__c":"1","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"NSG Product","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":true,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":true,"isquerydriven__c":false,"querylabel__c":null,"id":"a7P230000008W0mEAE","isrequired__c":false,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":null,"valuedatatype__c":"Text","valuedescription__c":null,"attributecloneable__c":false,"customconfiguitemplate__c":null,"categorydisplaysequence__c":0,"attributeRunTimeInfo":{"dataType":"Text","uiDisplayType":"Text"},"$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t23000000L20fAAC","attributeid__c":"a7R23000000A72DEAS","attributecategoryid__c":"a7Q230000008TfKEAU","categorycode__c":"ETL","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-113","attributeconfigurable__c":true,"attributedisplaysequence__c":"2","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Service ID","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":true,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":true,"isquerydriven__c":false,"querylabel__c":null,"id":"a7P230000008W0nEAE","isrequired__c":false,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":null,"valuedatatype__c":"Text","valuedescription__c":null,"attributecloneable__c":false,"customconfiguitemplate__c":null,"categorydisplaysequence__c":0,"attributeRunTimeInfo":{"dataType":"Text","uiDisplayType":"Text"},"$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t23000000L20fAAC","attributeid__c":"a7R23000000A72MEAS","attributecategoryid__c":"a7Q230000008TfKEAU","categorycode__c":"ETL","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-114","attributeconfigurable__c":true,"attributedisplaysequence__c":"3","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Service Order Type","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":true,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":true,"isquerydriven__c":false,"querylabel__c":null,"id":"a7P230000008W0oEAE","isrequired__c":false,"rulemessage__c":null,"uidisplaytype__c":"Dropdown","value__c":null,"valuedatatype__c":"Picklist","valuedescription__c":null,"attributecloneable__c":false,"customconfiguitemplate__c":null,"categorydisplaysequence__c":0,"attributeRunTimeInfo":{"dataType":"Picklist","uiDisplayType":"Dropdown","values":[{"id":"9145017269313790905","displayText":"Disconnect","value":"Disconnect"},{"id":"9145002414213768419","displayText":"Modify","value":"Modify"},{"id":"9145002403813768295","displayText":"New","value":"New"}],"default":[]},"$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t23000000L20fAAC","attributeid__c":"a7R23000000A72gEAC","attributecategoryid__c":"a7Q230000008TfKEAU","categorycode__c":"ETL","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-118","attributeconfigurable__c":true,"attributedisplaysequence__c":"6","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Enabled Services","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":true,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":true,"isquerydriven__c":false,"querylabel__c":null,"id":"a7P230000008W0rEAE","isrequired__c":false,"rulemessage__c":null,"uidisplaytype__c":"Dropdown","value__c":"IPSec VPN","valuedatatype__c":"Picklist","valuedescription__c":null,"attributecloneable__c":false,"customconfiguitemplate__c":null,"categorydisplaysequence__c":0,"attributeRunTimeInfo":{"dataType":"Picklist","uiDisplayType":"Dropdown","default":[{"id":"9145630288713423491","displayText":"IPSec VPN","value":"IPSec VPN"}],"values":[{"id":"9145269304413030450","displayText":"L3VPN","value":"L3VPN"},{"id":"9145269304413030449","displayText":"Managed Internet","value":"Managed Internet"},{"id":"9145630288713423491","displayText":"IPSec VPN","value":"IPSec VPN"},{"id":"9145630288713423490","displayText":"Managed Internet","value":"Managed Internet"}]},"$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t23000000L20fAAC","attributeid__c":"a7R23000000A72vEAC","attributecategoryid__c":"a7Q230000008TfKEAU","categorycode__c":"ETL","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-122","attributeconfigurable__c":true,"attributedisplaysequence__c":"8","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Number of Allowed Instances","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":true,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":true,"isquerydriven__c":false,"querylabel__c":null,"id":"a7P230000008W1GEAU","isrequired__c":false,"rulemessage__c":null,"uidisplaytype__c":"Text","value__c":null,"valuedatatype__c":"Number","valuedescription__c":null,"attributecloneable__c":false,"customconfiguitemplate__c":null,"categorydisplaysequence__c":0,"attributeRunTimeInfo":{"dataType":"Number","uiDisplayType":"Text"},"$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t23000000L20fAAC","attributeid__c":"a7R2300000004QREAY","attributecategoryid__c":"a7Q230000008TfKEAU","categorycode__c":"ETL","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-142","attributeconfigurable__c":true,"attributedisplaysequence__c":"14","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Topology Role(Hub,Spoke)","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":true,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":true,"isquerydriven__c":false,"querylabel__c":null,"id":"a7P230000004P0sEAE","isrequired__c":false,"rulemessage__c":null,"uidisplaytype__c":"Dropdown","value__c":"Hub","valuedatatype__c":"Picklist","valuedescription__c":null,"attributecloneable__c":true,"customconfiguitemplate__c":null,"categorydisplaysequence__c":0,"attributeRunTimeInfo":{"dataType":"Picklist","uiDisplayType":"Dropdown","default":[{"id":"9145279001813048083","displayText":"Hub","value":"Hub"}],"values":[{"id":"9145279001813048084","displayText":"Spoke","value":"Spoke"},{"id":"9145279001813048083","displayText":"Hub","value":"Hub"}]},"$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t23000000L20fAAC","attributeid__c":"a7R23000000A73yEAC","attributecategoryid__c":"a7Q230000008TfKEAU","categorycode__c":"ETL","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-136","attributeconfigurable__c":true,"attributedisplaysequence__c":"9","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Download Speed","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":true,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":true,"isquerydriven__c":false,"querylabel__c":null,"id":"a7P230000008W1LEAU","isrequired__c":false,"rulemessage__c":null,"uidisplaytype__c":"Dropdown","value__c":"10 Mbps","valuedatatype__c":"Picklist","valuedescription__c":null,"attributecloneable__c":false,"customconfiguitemplate__c":null,"categorydisplaysequence__c":0,"attributeRunTimeInfo":{"dataType":"Picklist","uiDisplayType":"Dropdown","default":[{"id":"9145969400013258026","displayText":"10 Mbps","value":"10 Mbps"}],"values":[{"id":"9145296262713058186","displayText":"300 Mbps","value":"300 Mbps"},{"id":"9145296262713058185","displayText":"100 Mbps","value":"100 Mbps"},{"id":"9145296262713058183","displayText":"30 Mbps","value":"30 Mbps"},{"id":"9145296262713058178","displayText":"25 Mbps","value":"25 Mbps"},{"id":"9145508890213276050","displayText":"15 Mbps","value":"15 Mbps"},{"id":"9145508890213276049","displayText":"30 Mbps","value":"30 Mbps"},{"id":"9145508890213276048","displayText":"100 Mbps","value":"100 Mbps"},{"id":"9145508890213276047","displayText":"300 Mbps","value":"300 Mbps"},{"id":"9145508890213276046","displayText":"25 Mbps","value":"25 Mbps"},{"id":"9145969400013258028","displayText":"55 Mbps","value":"55 Mbps"},{"id":"9145969400013258027","displayText":"15 Mbps","value":"15 Mbps"},{"id":"9145969400013258026","displayText":"10 Mbps","value":"10 Mbps"},{"id":"9145969400013258025","displayText":"20 Mbps","value":"20 Mbps"},{"id":"9145969400013258024","displayText":"30 Mbps","value":"30 Mbps"},{"id":"9145969400013258023","displayText":"75 Mbps","value":"75 Mbps"},{"id":"9145969400013258022","displayText":"100 Mbps","value":"100 Mbps"},{"id":"9145969400013258021","displayText":"5 Mbps","value":"5 Mbps"},{"id":"9145969400013258020","displayText":"300 Mbps","value":"300 Mbps"},{"id":"9145969400013258019","displayText":"50 Mbps","value":"50 Mbps"}]},"$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t23000000L20fAAC","attributeid__c":"a7R230000004DiUEAU","attributecategoryid__c":"a7Q230000008TfKEAU","categorycode__c":"ETL","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-141","attributeconfigurable__c":true,"attributedisplaysequence__c":"10","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"Network Role","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":true,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":true,"isquerydriven__c":false,"querylabel__c":null,"id":"a7P2300000008RIEAY","isrequired__c":false,"rulemessage__c":null,"uidisplaytype__c":"Dropdown","value__c":"Network Connection","valuedatatype__c":"Picklist","valuedescription__c":null,"attributecloneable__c":false,"customconfiguitemplate__c":null,"categorydisplaysequence__c":0,"attributeRunTimeInfo":{"dataType":"Picklist","uiDisplayType":"Dropdown","default":[{"id":"9145492910113250829","displayText":"Network Connection","value":"Network Connection"}],"values":[{"id":"9145492910113250830","displayText":"Network","value":"Network"},{"id":"9145492910113250829","displayText":"Network Connection","value":"Network Connection"},{"id":"9145492910113250828","displayText":"Network Element","value":"Network Element"}]},"$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t23000000L20fAAC","attributeid__c":"a7R2300000004QWEAY","attributecategoryid__c":"a7Q230000008TfKEAU","categorycode__c":"ETL","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-143","attributeconfigurable__c":true,"attributedisplaysequence__c":"11","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"VLAN","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":true,"valueinnumber__c":0,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":true,"isquerydriven__c":false,"querylabel__c":null,"id":"a7P230000004Oz7EAE","isrequired__c":false,"rulemessage__c":null,"uidisplaytype__c":"Single Value","value__c":"0","valuedatatype__c":"Number","valuedescription__c":null,"attributecloneable__c":true,"customconfiguitemplate__c":null,"categorydisplaysequence__c":0,"attributeRunTimeInfo":{"dataType":"Number","uiDisplayType":"Single Value","default":"0"},"$$AttributeDefinitionEnd$$":null},{"$$AttributeDefinitionStart$$":null,"objectid__c":"01t23000000L20fAAC","attributeid__c":"a7R2300000004QbEAI","attributecategoryid__c":"a7Q230000008TfKEAU","categorycode__c":"ETL","categoryname__c":"Characteristics","attributeuniquecode__c":"ATTRIBUTE-144","attributeconfigurable__c":true,"attributedisplaysequence__c":"12","attributefilterable__c":false,"isactive__c":true,"attributedisplayname__c":"encryption","displaysequence__c":null,"formatmask__c":null,"hasrule__c":false,"isconfigurable__c":true,"ishidden__c":true,"valueinnumber__c":null,"objecttype__c":"Product2","querycode__c":null,"isreadonly__c":true,"isquerydriven__c":false,"querylabel__c":null,"id":"a7P230000004OzpEAE","isrequired__c":false,"rulemessage__c":null,"uidisplaytype__c":"Dropdown","value__c":"ENABLED","valuedatatype__c":"Picklist","valuedescription__c":null,"attributecloneable__c":true,"customconfiguitemplate__c":null,"categorydisplaysequence__c":0,"attributeRunTimeInfo":{"dataType":"Picklist","uiDisplayType":"Dropdown","default":[{"id":"9145605079413388165","displayText":"ENABLED","value":"ENABLED"}],"values":[{"id":"9145605079413388166","displayText":"DISABLED","value":"DISABLED"},{"id":"9145605079413388165","displayText":"ENABLED","value":"ENABLED"}]},"$$AttributeDefinitionEnd$$":null}]} ';
        insert newProduct;

          
        Id pricebookId= Test.getStandardPricebookId();
        system.debug('submitOrderTest2::pricebookId('+pricebookId+')');
        
        PricebookEntry aPriceBookEntry = new PricebookEntry(product2id=newProduct.id, unitprice=1,  isactive=true, pricebook2id=pricebookId);
        insert aPriceBookEntry;
        system.debug('submitOrderTest2::aPriceBookEntry('+aPriceBookEntry+') id('+aPriceBookEntry.id+')');
        
        SMBCare_Address__c smbObj = TestDataCreationFactory.createSMBCareAddressOnAccount(newAccount);
        insert smbObj;
        
        ServiceAddressOrderEntry__c saoObj = new ServiceAddressOrderEntry__c ();
        saoObj.Address__c = smbObj.id ; 
        saoObj.isSiteReadyForCheckout__c  =  true;
        saoObj.Order__c = ordObj.Id;
        insert saoObj;
        
        
        OrderItem ordrItem0=new OrderItem();
        ordrItem0.OrderId=ordObj.id;
        ordrItem0.PricebookEntryId=aPriceBookEntry.id;
        ordrItem0.Quantity=1;
        ordrItem0.UnitPrice=100.00;
        ordrItem0.vlocity_cmt__LineNumber__c='0010';
        ordrItem0.vlocity_cmt__ProvisioningStatus__c=null;
        ordrItem0.orderMgmtId__c='OS1234567890';
        ordrItem0.vlocity_cmt__ServiceAccountId__c=newAccount.Id;
        ordrItem0.vlocity_cmt__BillingAccountId__c=newAccount.Id;
        ordrItem0.isConfigurationComplete__c =  true ; 
        ordrItem0.Service_Address__c = smbObj.Id ;
        insert ordrItem0 ;
        
        OrderItem ordrItem2=new OrderItem();
        ordrItem2.OrderId=ordObj.id;
        ordrItem2.PricebookEntryId=aPriceBookEntry.id;
        ordrItem2.Quantity=1;
        ordrItem2.UnitPrice=100.00;
        ordrItem2.vlocity_cmt__LineNumber__c='0010';
        ordrItem2.vlocity_cmt__ProvisioningStatus__c=null;
        ordrItem2.orderMgmtId__c='OS1234567890';
        ordrItem2.vlocity_cmt__ServiceAccountId__c=newAccount.Id;
        ordrItem2.vlocity_cmt__BillingAccountId__c=newAccount.Id;
        ordrItem2.isConfigurationComplete__c =  true ; 
        ordrItem2.Service_Address__c = smbObj.Id ;
        insert ordrItem2;
        
        OrderItem ordrItem1=new OrderItem();
        ordrItem1.OrderId=ordObj.id;
        ordrItem1.PricebookEntryId=aPriceBookEntry.id;
        ordrItem1.Quantity=1;
        ordrItem1.UnitPrice=100.00;
        ordrItem1.vlocity_cmt__LineNumber__c='0010';
        ordrItem1.vlocity_cmt__ProvisioningStatus__c=null;
        ordrItem1.orderMgmtId__c='OS1234567890';
        ordrItem1.vlocity_cmt__ServiceAccountId__c=newAccount.Id;
        ordrItem1.vlocity_cmt__BillingAccountId__c=newAccount.Id;
        ordrItem1.isConfigurationComplete__c =  false;
        ordrItem1.Service_Address__c = smbObj.Id ; 
        


        insert ordrItem1;
        
        List<Id> idList = new List<Id>();
        idList.add(ordrItem0.Id);
        idList.add(ordrItem1.Id);
        idList.add(ordrItem2.Id);
        List<OrderItem> oliList = [Select Id,  isConfigurationComplete__c , vlocity_cmt__JSONAttribute__c, OrderId, vlocity_cmt__ParentItemId__c, Service_Address__c from OrderItem where id IN: idList];
        Set<Id> oliIdSet = new Set<Id>();
        //oliIdSet.add(oliList[0].Id);
        OCOM_CustomEligibilityHookHelper.checkoutReadinessOnOliUpdation(oliList , oliIdSet , ordrItem1.Id);
        OCOM_CustomEligibilityHookHelper.checkoutReadinessOnOliUpdation(oliList , oliIdSet , ordrItem0.Id);
    }

}