/**
 *  MBRMyProfileCtlr_Test.cls
 *
 *  @description
 *      Test Class for the My Profile Controller in the My Business Requests Community (MBRMyProfileCtlr.cls). Copied, not created by dan.
 *      Modified by Alex Kong (2014-11-12)
 *
 *  @date - 03/01/2014
 *  @author - Dan Reich: Traction on Demand, Alex Kong (Traction on Demand)
 */
@isTest(seealldata=false)
private class MBRMyProfileCtlr_Test {
    
    @isTest
    public static void testMBRMyProfileCtlrLoggedInNonPortal() {
        profile p = [select id from profile where name = 'System Administrator' limit 1];
        user u = new User(alias = 'person', email='guest@testpkg.com',
                 emailencodingkey='UTF-8', firstname='Test', lastname='Person', languagelocalekey='en_US',
                 localesidkey='en_US', profileid = p.id,
                 timezonesidkey='America/Los_Angeles', username='guest@testpkg.com');    

        system.runAs(u) {
            MBRMyProfileCtlr controller = new MBRMyProfileCtlr();
            system.debug('user:'+Userinfo.getUserType());
            system.debug('user:'+Userinfo.getProfileId());
            system.debug('initCheck:'+controller.initCheck());
            //System.assertEquals(null, controller.initCheck());
            System.assertEquals(controller.accountDetail, null, 'Guest access check failed');
        }    
    }    
        
    @isTest
    public static void testMBRMyProfileCtlrLoggedInPortal1() {
        account a = new account(name = 'test acct');
        insert a;
        List<contact > cons= new List<contact >();
        contact c = new contact(email='test@test.com', lastname = 'test', accountid = a.id);
        cons.add(c);
        contact c2 = new contact(email='test2@test.com', lastname = 'test', accountid = a.id);
        cons.add(c2);
        insert cons;
        profile p = [select id from profile where name = 'Customer Community User' limit 1];
        List<user> users = new List<user>();

        User u = MBRTestUtils.createPortalUser('');

        system.runAs(u) {
            Test.setCurrentPage( Page.MBRYourProfile );
            MBRMyProfileCtlr controller = new MBRMyProfileCtlr();
            system.debug('user:'+Userinfo.getUserType());
            system.debug('user:'+Userinfo.getProfileId());
            system.debug('initCheck:'+controller.initCheck());
            System.assertEquals(null, controller.initCheck());
            //System.assertEquals('test@test.com', controller.username);
            controller.username = 'test@test.com10';
            System.assertEquals('test@test.com10', controller.username);
            controller.username = 'test2@test20.com';
            controller.updateUser();
            System.assertEquals(null, controller.usernameError);
            controller.username = 'guest2@testpkg.com';
            controller.updateUser();
            System.assertNotEquals('', controller.usernameError);
        }
    }

    @isTest
    public static void testMBRMyProfileCtlrLoggedInPortal2() {
        account a = new account(name = 'test acct');
        insert a;
        List<contact > cons= new List<contact >();
        contact c = new contact(email='test@test.com', lastname = 'test', accountid = a.id);
        cons.add(c);
        contact c2 = new contact(email='test2@test.com', lastname = 'test', accountid = a.id);
        cons.add(c2);
        insert cons;
        profile p = [select id from profile where name = 'Customer Community User' limit 1];
        List<user> users = new List<user>();

        User u = MBRTestUtils.createPortalUser('');

        system.runAs(u) {
            Test.setCurrentPage( Page.MBRYourProfile );
            MBRMyProfileCtlr controller = new MBRMyProfileCtlr();
            system.debug('user:'+Userinfo.getUserType());
            system.debug('user:'+Userinfo.getProfileId());
            system.debug('initCheck:'+controller.initCheck());
            controller.username = 'guest2@testpkg.com';
            controller.newPassword = '123';
            controller.updateContact();
            System.assertNotEquals('', controller.weakPassword);
            controller.newPassword = '123asdfASDF2@#$34';
            controller.updateContact();
            System.assertNotEquals('', controller.passwordNotMatch);
            controller.passwordNotMatch = '';
            controller.newPassword = '123asdfASDF2@#$34';
            controller.confirmPassword = '123asdfASDF2@#$34';            
            controller.updateContact();
            System.assertEquals('', controller.passwordNotMatch);
            controller.passwordNotMatch = '';
            controller.weakPassword= '';
            controller.newPassword = '123asdfASDF2@#$34';
            controller.confirmPassword = '123asdfASDF2@#$34';            
            controller.oldPassword = '1dfASDF2@#$34';                        
            controller.updateContact();
            System.assertNotEquals('', controller.oldPasswordError);

            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, 'You cannot reuse this old password' ) );
            controller.updateContact();
            System.assertEquals( Label.MBRCannotReusePassword, controller.weakPassword );
        }
    }

    @isTest
    public static void testUpdateContactInvalidOldEmailMessage()
    {
        User u = MBRTestUtils.createPortalUser('');

        system.runAs(u) {
            PageReference pageRef = Page.MBRMyProfile;
            Test.setCurrentPage(pageRef);

            MBRMyProfileCtlr controller = new MBRMyProfileCtlr();
            controller.initCheck();
            controller.oldPassword = '';
            controller.newPassword = '';
            controller.confirmPassword = '';

            String randomText = 'Your old password is invalid';
            //ApexPages.getMessages().clear();
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, randomText ) );
            controller.updateContact();
            System.assertEquals( label.mbrOldPasswordError, controller.oldPasswordError );
        }    
    }

    @isTest
    public static void testUpdateContactRandomErrorMessage()
    {
        User u = MBRTestUtils.createPortalUser('');

        system.runAs(u) {
            PageReference pageRef = Page.MBRMyProfile;
            Test.setCurrentPage(pageRef);

            MBRMyProfileCtlr controller = new MBRMyProfileCtlr();
            controller.initCheck();
            controller.oldPassword = '';
            controller.newPassword = '';
            controller.confirmPassword = '';

            String randomText = 'This is an additional exception!';
            //ApexPages.getMessages().clear();
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, randomText ) );
            controller.updateContact();
            System.assertEquals( randomText, controller.weakPassword );
        }    
    }

    @isTest
    public static void testUpdateInvalidEmail()
    {
        User u = MBRTestUtils.createPortalUser('');

        system.runAs(u) {
            PageReference pageRef = Page.MBRMyProfile;
            Test.setCurrentPage(pageRef);

            MBRMyProfileCtlr controller = new MBRMyProfileCtlr();
            controller.initCheck();
            controller.username = 'foo';
            
            controller.updateUser();

            System.assertEquals( controller.usernameError, label.mbrInvalidEmail );
            controller.usernameError = '';

            controller.updateContact();

            System.assertEquals( controller.usernameError, label.mbrInvalidEmail );
        }    
    }

    @isTest public static void testCancelUpdate() {
        
        MBRMyProfileCtlr controller = new MBRMyProfileCtlr();
        System.assert(controller.cancelUpdate().getUrl().containsIgnoreCase('profile'));
    }
    
    
    /* VITILcare portal */
    @isTest
    public static void VITILcare_testMBRMyProfileCtlrLoggedInNonPortal() {
        profile p = [select id from profile where name = 'System Administrator' limit 1];
        user u = new User(alias = 'person', email='guest@testpkg.com',
                 emailencodingkey='UTF-8', firstname='Test', lastname='Person', languagelocalekey='en_US',
                 localesidkey='en_US', profileid = p.id,
                 timezonesidkey='America/Los_Angeles', username='guest@testpkg.com');    

        system.runAs(u) {
            VITILcareMyProfileCtlr controller = new VITILcareMyProfileCtlr();
            system.debug('user:'+Userinfo.getUserType());
            system.debug('user:'+Userinfo.getProfileId());
            system.debug('initCheck:'+controller.initCheck());
            //System.assertEquals(null, controller.initCheck());
            System.assertEquals(controller.accountDetail, null, 'Guest access check failed');
        }    
    }    
        
    @isTest
    public static void VITILcare_testMBRMyProfileCtlrLoggedInPortal1() {
        account a = new account(name = 'test acct');
        insert a;
        List<contact > cons= new List<contact >();
        contact c = new contact(email='test@test.com', lastname = 'test', accountid = a.id);
        cons.add(c);
        contact c2 = new contact(email='test2@test.com', lastname = 'test', accountid = a.id);
        cons.add(c2);
        insert cons;
        profile p = [select id from profile where name = 'Customer Community User' limit 1];
        List<user> users = new List<user>();

        User u = MBRTestUtils.createPortalUser('');

        system.runAs(u) {
            Test.setCurrentPage( Page.VITILcareMyProfile );
            VITILcareMyProfileCtlr controller = new VITILcareMyProfileCtlr();
            system.debug('user:'+Userinfo.getUserType());
            system.debug('user:'+Userinfo.getProfileId());
            system.debug('initCheck:'+controller.initCheck());
            System.assertEquals(null, controller.initCheck());
            //System.assertEquals('test@test.com', controller.username);
            controller.username = 'test@test.com10';
            System.assertEquals('test@test.com10', controller.username);
            controller.username = 'test2@test20.com';
            controller.updateUser();
            //System.assertEquals(null, controller.usernameError);
            controller.username = 'guest2@testpkg.com';
            controller.updateUser();
            System.assertNotEquals('', controller.usernameError);
        }
    }

    @isTest
    public static void VITILcare_testMBRMyProfileCtlrLoggedInPortal2() {
        account a = new account(name = 'test acct');
        insert a;
        List<contact > cons= new List<contact >();
        contact c = new contact(email='test@test.com', lastname = 'test', accountid = a.id);
        cons.add(c);
        contact c2 = new contact(email='test2@test.com', lastname = 'test', accountid = a.id);
        cons.add(c2);
        insert cons;
        profile p = [select id from profile where name = 'Customer Community User' limit 1];
        List<user> users = new List<user>();

        User u = MBRTestUtils.createPortalUser('VITILcare');

        system.runAs(u) {
            Test.setCurrentPage( Page.VITILcareMyProfile );
            VITILcareMyProfileCtlr controller = new VITILcareMyProfileCtlr();
            system.debug('user:'+Userinfo.getUserType());
            system.debug('user:'+Userinfo.getProfileId());
            system.debug('initCheck:'+controller.initCheck());
            controller.username = 'guest2@testpkg.com';
            controller.newPassword = '123';
            controller.updateContact();
            System.assertNotEquals('', controller.weakPassword);
            controller.newPassword = '123asdfASDF2@#$34';
            controller.updateContact();
            System.assertNotEquals('', controller.passwordNotMatch);
            controller.passwordNotMatch = '';
            controller.newPassword = '123asdfASDF2@#$34';
            controller.confirmPassword = '123asdfASDF2@#$34';            
            controller.updateContact();
            System.assertEquals('', controller.passwordNotMatch);
            controller.passwordNotMatch = '';
            controller.weakPassword= '';
            controller.newPassword = '123asdfASDF2@#$34';
            controller.confirmPassword = '123asdfASDF2@#$34';            
            controller.oldPassword = '1dfASDF2@#$34';                        
            controller.updateContact();
            System.assertNotEquals('', controller.oldPasswordError);

            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, 'You cannot reuse this old password' ) );
            controller.updateContact();
            System.assertEquals( Label.MBRCannotReusePassword, controller.weakPassword );
        }
    }

    @isTest
    public static void VITILcare_testUpdateContactInvalidOldEmailMessage()
    {
        User u = MBRTestUtils.createPortalUser('');

        system.runAs(u) {
            PageReference pageRef = Page.VITILCareMyProfile;
            Test.setCurrentPage(pageRef);

            VITILcareMyProfileCtlr controller = new VITILcareMyProfileCtlr();
            controller.initCheck();
            controller.oldPassword = '';
            controller.newPassword = '';
            controller.confirmPassword = '';

            String randomText = 'Your old password is invalid';
            //ApexPages.getMessages().clear();
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, randomText ) );
            controller.updateContact();
            System.assertEquals( label.mbrOldPasswordError, controller.oldPasswordError );
        }    
    }

    @isTest
    public static void VITILcare_testUpdateContactRandomErrorMessage()
    {
        User u = MBRTestUtils.createPortalUser('');

        system.runAs(u) {
            PageReference pageRef = Page.VITILcareMyProfile;
            Test.setCurrentPage(pageRef);

            VITILcareMyProfileCtlr controller = new VITILcareMyProfileCtlr();
            controller.initCheck();
            controller.oldPassword = '';
            controller.newPassword = '';
            controller.confirmPassword = '';

            String randomText = 'This is an additional exception!';
            //ApexPages.getMessages().clear();
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, randomText ) );
            controller.updateContact();
            //System.assertEquals( randomText, controller.weakPassword );
        }    
    }

    @isTest
    public static void VITILcare_testUpdateInvalidEmail()
    {
        User u = MBRTestUtils.createPortalUser('');

        system.runAs(u) {
            PageReference pageRef = Page.VITILcareMyProfile;
            Test.setCurrentPage(pageRef);

            VITILcareMyProfileCtlr controller = new VITILcareMyProfileCtlr();
            controller.initCheck();
            controller.username = 'foo';
            
            controller.updateUser();

            System.assertEquals( controller.usernameError, label.mbrInvalidEmail );
            controller.usernameError = '';

            controller.updateContact();

            System.assertEquals( controller.usernameError, label.mbrInvalidEmail );
        }    
    }

    @isTest public static void VITILcare_testCancelUpdate() {
        
        VITILcareMyProfileCtlr controller = new VITILcareMyProfileCtlr();
        System.assert(controller.cancelUpdate().getUrl().containsIgnoreCase('profile'));
    }
}