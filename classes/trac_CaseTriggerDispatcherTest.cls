/**
*trac_CaseTriggerDispatcher
*@description Test Class to cover the sendeing of a case to RingCentral via Salseforce to salesforce
*@authoer David Barrera
*@Date 9/04/14
*/

@isTest
private class trac_CaseTriggerDispatcherTest {
     /*November 14 2014 - Commenting out asserts to allow for deployment.  Due to refactoring for
     redesign to use Process Builder and Flow test classes are now failing in production.  However, 
     need Flows to be activated before can send new test classes with asserts to production successfully */
     
    @isTest(SeeAllData=false)
    static  void testSendingCase() {
        // Insert some cases to work with.
        
        List<Case> testCases = new List<Case>();
        Set<String> caseIds = new Set<String>();
        Case testCase1 = new Case(Subject = 'Test Subject 1', Priority = 'Medium');
        //Case testCase2 = new Case(Subject = 'Test Subject 2', Priority = '1');
        //Case testCase3 = new Case(Subject = 'Test Subject 3', Priority = 'low');

        testCases.add(testCase1);
        //testCases.add(testCase2);
        //testCases.add(testCase3);

        for(Case aCase: testCases){
            caseIds.add(aCase.Id);
        }
        
        insert testCases;

        for(Case aCase: testCases){
            aCase.Type ='VoIP TN Resume';
            acase.Transfer_to_Queue__c = 'RingCentral Tier 2';
            aCase.BAN_Nbr__c='1234';
        }

        Dispatcher.ResetProcessCount('trac_CaseTriggerDispatcher_sendCase');
        Test.startTest();
        update testCases;
        Test.stopTest();
        
    }
    
    @isTest(SeeAllData=false)
    static void testCaseWithCaseComments(){

        
        Set<String> caseIds = new Set<String>();

        List<Case> testCases = new List<Case>();
        List<CaseComment> testCaseComments = new List<CaseComment>();
        List<Device__c> testDevices = new List<Device__c>();
        List<Attachment> testAttachments = new List<Attachment>();
        List<PartnerNetworkRecordConnection> casePnrcList = new List<PartnerNetworkRecordConnection>();
        List<PartnerNetworkRecordConnection> devicePnrcList =  new List<PartnerNetworkRecordConnection>();
        List<Vendor_Communication__c> newOwnerVendors = new List<Vendor_Communication__c>();
        Map<Id, Id> caseIdToPNRC = new Map<Id, Id>(); 

        Case testCase1 = new Case(Subject = 'Test Subject 1', Priority = 'Medium');
        //Case testCase2 = new Case(Subject = 'Test Subject 2', Priority = 'High');
        //Case testCase3 = new Case(Subject = 'Test Subject 3', Priority = 'low');

        testCases.add(testCase1);
        //testCases.add(testCase2);
        //testCases.add(testCase3);

        for(Case aCase: testCases){
            caseIds.add(aCase.Id);
        }
        
        insert testCases;
            //Insert one case comment that is published and one that isn't for two cases
        
        CaseComment testCaseComment1 = new CaseComment(CommentBody = 'Test number 1 for sure ',
                                                        ParentId = testCase1.Id, 
                                                        IsPublished = true);
        //CaseComment testCaseComment2 = new CaseComment(CommentBody = 'Test number 2 for real ',
        //                                              ParentId = testCase1.Id,
        //                                              IsPublished = false);
        //CaseComment testCaseComment3 = new CaseComment(CommentBody = 'Test number 3 of course ',
        //                                              ParentId = testCase2.Id, 
        //                                              IsPublished = false);
        //CaseComment testCaseComment4 = new CaseComment(CommentBody = 'Test number 4 but of course ',
        //                                              ParentId = testCase2.Id, 
        //                                              IsPublished = false);
        //CaseComment testCaseComment5 = new CaseComment(CommentBody = 'Test number 5 for sure ',
        //                                              ParentId = testCase3.Id, 
        //                                              IsPublished = false);

        testCaseComments.add(testCaseComment1);
        //testCaseComments.add(testCaseComment2);
        //testCaseComments.add(testCaseComment3);
        //testCaseComments.add(testCaseComment4);
        //testCaseComments.add(testCaseComment5);
        
        insert testCaseComments;



        //Insert a device for each case. 
        
            

        //Update the test cases to have there transfer queue set to 'ringCentral tier 2'
        for(Case aCase: testCases){
            //aCase.Vendor_Transfer_Type__c ='Warm Transfer';
            aCase.Type ='VoIP TN Resume';
            acase.Transfer_to_Queue__c = 'RingCentral Tier 2';
            aCase.BAN_Nbr__c='1234';
        }

        
        Test.startTest();
        update testCases;
        Test.stopTest();
        
        //query for the PNRC confirm that there are records made for the cases updated above.
        
        casePnrcList = getPNRCList(testCases);
        //System.Debug('This is the size of the pnrc: ' + casePnrcList.size());
        for (PartnerNetworkRecordConnection pnrc: casePnrcList){
            //System.Debug('Case ID of the local record in the pnrcList: ' + pnrc.LocalRecordId);   
            caseIdToPNRC.put(pnrc.LocalRecordId, pnrc.PartnerRecordId); 
        }


        System.assert(caseIdToPNRC.containsKey(testCases.get(0).Id));
        //System.assert(caseIdToPNRC.containsKey(testCases.get(1).Id));
        //System.assert(caseIdToPNRC.containsKey(testCases.get(2).Id));

        testDevices.add( new Device__C(Serial_Number__c='123',VoIP_Case__c = testCase1.Id));
        //testDevices.add( new Device__C(Serial_Number__c='3233', VoIP_Case__c = testCase2.Id));
        //testDevices.add( new Device__C(Serial_Number__c='14811',VoIP_Case__c = testCase3.Id));

        insert testDevices; 
        
        testAttachments.add( new Attachment(
			parentId = String.valueOf(testCase1.Id),
            body = Blob.valueOf( 'this is an attachment test' ),
			name = 'fake attachment'
		));
        
        insert testAttachments; 
        
        //Check that there are PNRC for the devices that should have been pushed as well.
        devicePnrcList = getPNRCList(testDevices);
        Map<Id,Id> deviceIdToPNRC = new Map<Id,Id>(); 

        for (PartnerNetworkRecordConnection pnrc: devicePnrcList){          
            deviceIdToPNRC.put(pnrc.LocalRecordId, pnrc.PartnerRecordId);   
        }
        
        System.Debug('The size of the deviceIdToPNRC: ' + deviceIdToPNRC.size());
        //System.assert(deviceIdToPNRC.containsKey(testDevices.get(0).Id));
        //System.assert(deviceIdToPNRC.containsKey(testDevices.get(1).Id));
        //System.assert(deviceIdToPNRC.containsKey(testDevices.get(2).Id));
        
        
        trac_S2S_connection_Helper.getRingCentralConnectionId('');
        trac_S2S_connection_Helper.getRingCentralConnectionId('test');      

    }
    
    @isTest(SeeAllData=false)
    static void testCaseDispatcherReceive() {

        Set<String> caseIds = new Set<String>();

        List<Case> testCases = new List<Case>();
        List<Account> testAccs = new List<Account>();
        List<Contact> testConts = new List<Contact>();
        List<CaseRecordType__c> testCaseRecordType = new List<CaseRecordType__c>();

        CaseRecordType__c crt1 = new CaseRecordType__c (Name='Test1',
                                                        Case_Type__c='test1',
                                                        Record_Type_Name__c='SMB Care TELUS Service');
        CaseRecordType__c crt2 = new CaseRecordType__c (Name='Test2',
                                                        Case_Type__c='test2',
                                                        Record_Type_Name__c='SMB Care TELUS Service');
        CaseRecordType__c crt3 = new CaseRecordType__c (Name='Test3',
                                                        Case_Type__c='test3',
                                                        Record_Type_Name__c='SMB Care TELUS Service');
        
        testCaseRecordType.add(crt1);
        testCaseRecordType.add(crt2);
        testCaseRecordType.add(crt3);

        insert testCaseRecordType;

        Id banRecordTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('BAN').RecordTypeId; 
        Id rcidRecorTypeId = Schema.SObjectType.Account.RecordTypeInfosByName.get('RCID').RecordTypeId; 
        //Insert Account with ban id that will be attached on case.,

        Account testRCIDAcc = new Account(Can__c='98974511',
                                        Name='Test Account Three',
                                        RecordTypeId=rcidRecorTypeId);

        insert testRCIDAcc;

        Account testBanAcc1 = new Account(Can__c='456789123',
                                            Name='Test Account One',
                                            RecordTypeId=banRecordTypeId,
                                            ParentId =testRCIDAcc.Id);
        Account testBanAcc2 = new Account(Can__c='471154',
                                            Name='Test Account Two',
                                            RecordTypeId=banRecordTypeId);
        
        testAccs.add(testBanAcc1);
        testAccs.add(testBanAcc2);
        insert testAccs;
        //Insert Contact that will have email, phone and last name that will be on Contact.email,phone,lastname
        Contact testCont1 = new Contact(Email='test@test.com',
                                        LastName='test',
                                        Phone='784511514');
        Contact testCont2 = new Contact(Email='JamesT@Starfleet.com',
                                        LastName='Kirk',
                                        Phone='1974');
        Contact testCont3 = new Contact(LastName='Kirk',
                                        Phone='1974');
        Contact testCont4 = new Contact(Email='donny@TMNT.com',
                                        LastName='ninja',
                                        Phone='265874445');
        Contact testCont5 = new Contact(Account = testRCIDAcc,
                                        Email='modest@mouse.com',
                                        LastName='nthemooninja',
                                        Phone='4814457842');

        testConts.add(testCont1);
        testConts.add(testCont2);
        testConts.add(testCont3);
        testConts.add(testCont4);
        testConts.add(testCont5);

        insert testConts;

        Case testCase1 = new Case(Subject = 'Test Subject 1', 
                                    Priority = 'Medium',
                                    Record_Type_Name__c='test1',
                                    BAN_Nbr__c='456789123',
                                    Contact_Email__c='test@test.com',
                                    Contact_Last_Name__c='test',
                                    Contact_Phone_Number2__c='784511514'
        );
        Case testCase2 = new Case(Subject = 'Test Subject 2',
                                    Priority = '1',Record_Type_Name__c='test2',
                                    BAN_Nbr__c='471154',
                                    Contact_Last_Name__c='Kirk',
                                    Contact_Phone_Number2__c='1974'
        );
        Case testCase3 = new Case(Subject = 'Test Subject 3',
                                    Priority = 'low',
                                    Record_Type_Name__c='test3',
                                    Contact_Email__c='donny@TMNT.com'
        );
        Case testCase4 = new Case(Subject = 'Test Subject 4',
                                    Priority = 'high',
                                    Record_Type_Name__c='test3',
                                    Contact_Email__c='modest@mouse.com',
                                    Contact_Last_Name__c='themoon',
                                    Contact_Phone_Number2__c='4814457842',
                                    Business_Name__c = 'Test Account Three'
        );

        testCases.add(testCase1);
        testCases.add(testCase2);
        testCases.add(testCase3);
        testCases.add(testCase4);

        for(Case aCase: testCases){
            caseIds.add(aCase.Id);
        }
        Test.startTest();
        insert testCases;
        Test.stopTest();

    }
    
    public static List<PartnerNetworkRecordConnection> getPNRCList(List<SObject> objList){
        List<PartnerNetworkRecordConnection> pnrcList = new List<PartnerNetworkRecordConnection>();
        Set<String> objIds = new Set<String>();
        //System.Debug('---- This is the objList: ' + objList);
        for(SObject obj: objList){
            objIds.add(obj.Id);
        }
        //System.Debug('---- This is the objList size: ' + objIds.size() );
        //System.Debug('---- This is the objIds: ' + objIds );
        if(objIds.size() > 0){
            pnrcList =[ 
                Select id,status,LocalRecordId,StartDate,PartnerRecordId
                From PartnerNetworkRecordConnection
                Where LocalRecordId in :objIds
            ];      
        }   
        //System.Debug('---this ist he pnrcList: ' + pnrcList);
        return pnrcList;

    }
}