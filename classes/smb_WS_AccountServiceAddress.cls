global class smb_WS_AccountServiceAddress {

   //Define an object in apex that is exposed in apex web service

    global class ServiceAddress {
        webservice SMBCare_Address__c ServiceAddress = new SMBCare_Address__c();        
   }
   

   webservice static list<ServiceAddress> getServiceAddressesForAccount(string accountID) 
   {
        list <id> lstAccID = new list<id>();
        list<ServiceAddress> lstServiceAddress = new list<ServiceAddress>();
        list<SMBCare_Address__c> lstSerAddress = new list<SMBCare_Address__c>();
        ServiceAddress sAddress = new ServiceAddress();
        try
        {
            //get ultimate parent and then childrens
            lstAccID = smb_AccountUtility.getAccountsInHierarchy(smb_AccountUtility.getUltimateParentId(accountID),null);
            if (lstAccID.size()>0)
            {
                /*lstSerAddress = [SELECT 
                                    Account__c,
                                    Name,
                                    Type__c,
                                    Street__c,
                                    Unit_Number__c,
                                    Street_Address__c,
                                    Block__c,
                                    City__c,
                                    State__c,
                                    Country__c
                                FROM
                                    SMBCare_Address__c
                                WHERE
                                    Account__c in :lstAccID
                                ];*/
                    lstSerAddress = getAllAddressFields(lstAccID);
            
            
            }
            if (lstSerAddress.size()>0)
            {
                for (SMBCare_Address__c serAdd :lstSerAddress )
                {
                    sAddress.ServiceAddress = serAdd;
                    lstServiceAddress.add(sAddress);
                }
                return lstServiceAddress;
                
            }
            else
            return null;
        }
        catch (Exception ex)
        {
            throw ex;      
        }
  }
  
  private static list<SMBCare_Address__c> getAllAddressFields(list<id> lstID)
  {
    /* Map<String, Schema.SObjectField> fldObjMap = schema.SObjectType.SMBCare_Address__c.fields.getMap();
    List<Schema.SObjectField> fldObjMapValues = fldObjMap.values();
    
    String theQuery = 'SELECT ';
    for(Schema.SObjectField s : fldObjMapValues)
    {
        String theLabel = s.getDescribe().getLabel(); // Perhaps store this in another map
        String theName = s.getDescribe().getName();
        //String theType = s.getDescribe().getType(); // Perhaps store this in another map

        // Continue building your dynamic query string
        theQuery += theName + ',';
    }

    // Trim last comma
    theQuery = theQuery.subString(0, theQuery.length() - 1);

    // Finalize query string
    theQuery += ' FROM SMBCare_Address__c WHERE Account__c in :lstID';
    system.debug('--------------Quesry--'+theQuery);
    // Make your dynamic call
    return Database.query(theQuery); */
    list<SMBCare_Address__c> resultAddress = new list<SMBCare_Address__c>();
    resultAddress = [Select Vector__c, Validation_Status__c, Unit_Number__c, Type__c, Township__c, SystemModstamp, Subdivision_ID__c, Street__c,
                    Street_Type_Code__c, Street_Direction_Code__c, Street_Address__c, Status__c, State__c, Service_Address_Valid__c, Section__c,
                    Rural_Route_Type_Code__c, Rural_Route_Number__c, Rate_Group__c, Rate_Band__c, Range__c, Quarter__c, Priority__c, Postal_Code__c,
                    Plan__c, Phone__c, PO_Box_Number__c, New_Address_Flag__c, Name, NPA_NXX__c, Meridian__c, M_P_Compass_Only__c, MCM_External_ID__c,
                    Lot__c, Legal_Land_Description__c, LastModifiedDate, LastModifiedById, IsDeleted, Id, Forbone__c,
                    FMS_Address_ID__c, Due_Date_Time__c, CurrencyIsoCode, CreatedDate, CreatedById, Country__c, ConnectionSentId, ConnectionReceivedId,
                    Class_of_Service__c, Civic_Number__c, City__c, Block__c, Approximation_Flag__c,Address_Subtype__c,Account__c From SMBCare_Address__c  WHERE Account__c in :lstID ORDER BY CreatedDate DESC LIMIT 50];
    
    return  resultAddress;                
  
  }

}