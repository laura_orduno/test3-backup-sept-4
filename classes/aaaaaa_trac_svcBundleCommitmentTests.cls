global without sharing class aaaaaa_trac_svcBundleCommitmentTests {   
    public static testMethod void createBundleCommitment() {
        Test.setMock(WebServiceMock.class, new CreateBundleCommitmentMockSuccess());
        svc_BundleCommitmentExtSvc_1.BundleCommitmentExtSvcPort port = new svc_BundleCommitmentExtSvc_1.BundleCommitmentExtSvcPort();
        
        //port.endpoint_x = 'http://office.tractionondemand.com:8080/mockEquipmentRepairManagementService_v1_0_SOAP';
        port.lenientParsing_x = true;
        
        String quoteRefNum = 'Q-34501';
        String bundleName = 'Business Anywhere Bundle - Single Line';
        String billingAccountNum = '100200300';
        svc_BundleCommitmentCommonType_v1.CustomerInfo customerInfo = new svc_BundleCommitmentCommonType_v1.CustomerInfo();
        customerInfo.companyName = 'Example Company';
        customerInfo.customer = new svc_EnterpriseCommonName_v1.IndividualName();
        customerInfo.customer.firstName = 'Alex';
        customerInfo.customer.lastName = 'Miller';

        
        customerInfo.address = new svc_TelusCommonAddressTypes_v5.Address();
        customerInfo.address.streetName = 'Clarke Street';
        customerInfo.address.streetNumber = '2328';
        customerInfo.address.unitName = '';
        customerInfo.address.municipalityName = 'Port Moody';
        customerInfo.address.provinceStateCode = 'BC';
        customerInfo.address.postalZipCode = 'V3H 1Y8';
        customerInfo.address.countryCode = 'CA';
        
        svc_BundleCommitmentCommonType_v1.WirelessOfferReference wor = new svc_BundleCommitmentCommonType_v1.WirelessOfferReference();
        wor.wirelessOfferCode = 'TESTOFFERCODE';
        wor.originalOffersAvailableNum = 2;
        
        svc_BundleCommitmentCommonType_v1.WirelessOfferReference[] wirelessOfferRefList = new List<svc_BundleCommitmentCommonType_v1.WirelessOfferReference>{wor};
        
        svc_BundleCommitmentExtSvcRequestRespons.CreateBundleCommitmentResponse result = port.createBundleCommitment(quoteRefNum, bundleName, billingAccountNum, customerInfo, wirelessOfferRefList);

        system.assert(result != null);

        system.debug(result);
    }
    
    global class CreateBundleCommitmentMockSuccess implements WebServiceMock {
        global void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
                          String requestName, String responseNS, String responseName, String responseType) {
        
            svc_BundleCommitmentExtSvcRequestRespons.CreateBundleCommitmentResponse resp =
            new svc_BundleCommitmentExtSvcRequestRespons.CreateBundleCommitmentResponse();
            
            resp.dateTimeStamp = Datetime.newInstance(2012,11,22,22,18,30);
            
            //resp.errorCode = null;
            resp.messageType = 'SUCCESS';
            resp.transactionId = '12359439123403';
            //resp.messageList = new List<svc_EnterpriseCommonTypes_v7.Message>();
            //resp.contextData = 'test';
            
            /* resp.validationMessage = new svc_EnterpriseCommonTypes_v7.ResponseMessage();
            resp.validationMessage.messageList = new List<svc_EnterpriseCommonTypes_v7.Message>();
            
            resp.validationStatus = 'FAILED';
            
            
            resp.validationMessage.errorCode = 'PRODUCT_NOT_FOUND';
            resp.validationMessage.messageType = 'ERROR';
            
            svc_EnterpriseCommonTypes_v7.Message message = new svc_EnterpriseCommonTypes_v7.Message();
            message.locale = 'EN';
            message.message = 'Product Info is Not Found.';
            
            resp.validationMessage.messageList.add(message); */
            
            response.put('response_x', resp);
        }
    }
}