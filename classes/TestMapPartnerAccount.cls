@isTest
private class TestMapPartnerAccount{
     static testmethod void testMappingTrigger(){
     // new Account created
     Account acc = new Account(Name= 'Text');
     insert acc;
     System.debug('######!!!!Account###'+acc);
     ////Account is updated as Partner Account 
     acc.IsPartner=true;
     update acc;
     System.debug('######!!!!Ispartner###'+acc);  
     //////////new Contact is created   
     Contact con = new Contact(
     LastName ='Test Contact',
     Phone='6049969449',
     AccountId=acc.Id
     );
     insert con;
     System.debug('######!!!!contact###'+con); 
     ///////Selected a profile Partner User 
     Profile p =[select id from Profile where name like '%Partner%' limit 1];  
     System.debug('######!!!!Profile###'+p);
     //////Created a Partner User
     User u = new User(
     LastName='TestNameXKCDTMPA',
     Alias='tstNXK',
     Email='testxkcdtmpa@yahoo.com',
     Username='partnerxkcdtmpa@yahoo.com',
     CommunityNickname='xkcdtmpa',
     emailencodingkey='UTF-8',
      phone = '6049969449',
     languagelocalekey='en_US',
     localesidkey='en_US',
     ContactId=con.Id,
     timezonesidkey='America/Los_Angeles',
     //UserRole='',
     profileId = p.Id
     );
     insert u;
     System.debug('######!!!!UserType###'+u.UserType);  
     //////////Created a new Account
     //Account accs = new Account(Name='Test Account');
     //insert accs;
     /////////////Created A lead
     Lead led = new Lead(Company='HDFC',LastName='Test',OwnerId=u.id);
     insert led;
     System.debug('######!!!!'+led);
     User urs=[select Contact.AccountId, UserType from User where id=:led.OwnerId];
     Lead lead =[select Partner_Account__c from Lead where id=:led.id];//
     ////////Check for Lead Partner Account= Owner.Contact.AccountId
     System.debug('######!!!!AccountId'+urs.Contact.AccountId);
     System.assertEquals('PowerPartner',urs.UserType);   
     System.assertEquals(urs.Contact.AccountId,lead.Partner_Account__c);  
     }
}