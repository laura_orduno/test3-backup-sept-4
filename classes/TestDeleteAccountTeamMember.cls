@isTest
private class TestDeleteAccountTeamMember{
     static testmethod void testCodeCoverage(){
         // created Account -Start
         Account account = new Account(Name= 'Test123');
         insert account;      
         //created Account -End

         User u = [SELECT Id FROM User WHERE IsActive = true AND Id != :UserInfo.getUserId() LIMIT 1];
         
         //Create Account Team- Start
         AccountTeamMember actMember1 = new AccountTeamMember(
             AccountId = account.Id,
             UserId = u.Id
         );
         insert actMember1;

         AccountTeamMember actMember2 = new AccountTeamMember(
             AccountId = account.Id,
             UserId = UserInfo.getUserId()
         );
         insert actMember2;
         //Create Account Team- End 
         
         //Test Case--User can delete by login if exist as Account Team Member 
         System.runAs(u){
            ApexPages.currentPage().getParameters().put('actId', account.Id);
            DeleteAccountTeamMember obj = new DeleteAccountTeamMember();
            obj.init();
            obj.cancel();            
            obj.deleteAccountTeamMember();
         } 
         DeleteAccountTeamMember obj1 = new DeleteAccountTeamMember();  
         obj1.init();
         obj1.cancel();
     }
}