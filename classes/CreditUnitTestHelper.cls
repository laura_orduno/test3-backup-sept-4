public class CreditUnitTestHelper {
    
    public static Account theAccount = getAccount();
    public static User theUser = createTheTestuser();
    
    public static Credit_Checklist__c getChecklist(){
        Credit_Checklist__c checklist = new Credit_Checklist__c();
        checklist.Credit_Assessment_Ref_No__c = getCar().id;
        insert checklist;
        return checklist;
    }
    
    
    public static Credit_Related_Info__c getCri(){
        Credit_Related_Info__c cri = new Credit_Related_Info__c();
        cri.Delete_Document__c = true;
        cri.Document_Internal_ID__C = 'a5Le000000009X5';
        insert cri;
        return cri;
    }

    
    public static Credit_Assessment__c getCar(){
        Credit_Assessment__c car = new Credit_Assessment__c();
        car.CAR_Account__c = theAccount.id;
        car.Customer_Segmentation_Code__c = 'SM';
        car.Birth_Date__c = Date.today();
        car.Driver_s_License__c = '123456';
        car.Drivers_License_province__c = 'British Columbia';
        car.Health_Card_No__c  = '123456';
        car.Passport__c  = '123456';
        car.Passport_Country__c = 'Canada';
        insert car;
        return car;
    }

    public static Document getDocument(){
        Document doc = new Document();
        doc.name = 'doc name';
        doc.Body = Blob.valueOf('Unit Test Document Body');
        doc.FolderId =UserInfo.getUserId();
        insert doc;
        return doc;
    }
    
    public static Credit_Partners__c getPartner(){
        Credit_Partners__c partner = new Credit_Partners__c();
        partner.SIN__c = '722343282';
        partner.Contact_No__c = '6048129539';
        partner.First_Name__c = 'firstname';
        partner.Last_Name__c = 'lastname';
        //partner.Corporate_Partner__c = getCreditProfile().id;
        insert partner;
        return partner;
    }

    public static Account getAccount(){      
        Account account = new Account();
        account.Name = 'acc name';
        account.CustProfId__c = '123456';  
        insert account;
        return account;
    }

    public static Opportunity  getOpportunity (){      
        Opportunity  opportunity = new Opportunity ();
        opportunity.Name = 'First Appointment';
        opportunity.StageName = 'First Appointment';
        opportunity.CloseDate = Date.today();
        insert opportunity;
        return opportunity;
    }
    
    public static Credit_Profile__c getCreditProfile(){
        Credit_Profile__c creditProfil = new Credit_Profile__c();     
        Account account = theAccount;
        creditProfil.Account__c = account.id;        
        creditProfil.City_Head_Office__c = 'head office';
        creditProfil.Consumer_profile_ID__c = '123457';   
        creditProfil.is_auto_update__c = true;     
        insert creditProfil;
        System.debug('... dyy ... creditProfil.id = ' + creditProfil.id );
        return creditProfil;
    }
    
    public static Credit_Profile__c getCreditProfile(String accId){
        Credit_Profile__c creditProfil = new Credit_Profile__c();  
        creditProfil.Account__c = accId;        
        creditProfil.City_Head_Office__c = 'head office';
        creditProfil.is_auto_update__c = true;
        insert creditProfil;
        System.debug('... dyy ... creditProfil.id = ' + creditProfil.id );
        return creditProfil;
    }
    
    
        
    public static User createTheTestuser(){
        User user = new User();
        user.Username = 'dyy.dyy@telus.com';
        user.LastName = 'X33X';  
        user.Email = 'dyy.dyy@telus.com';   
        user.CommunityNickname = 'dyy';   
        user.TimeZoneSidKey = 'Pacific/Kiritimati';   
        user.LocaleSidKey = UserInfo.getLocale();   
        user.EmailEncodingKey = 'ISO-8859-1';   
        user.ProfileId = UserInfo.getProfileId();   
        user.LanguageLocaleKey = UserInfo.getLanguage();   
        user.Alias = 'dyy';
        //insert user;
        return user;
    }
        
    public static User createTestuser(){
        return theUser;
    }
    
    public static User getTestuser(){
        return theUser;
    }
    
    
    public static Credit_Related_Product__c getCreditRelatedProduct(Credit_Assessment__c car){
        Credit_Related_Product__c product = new Credit_Related_Product__c();
        product.Credit_Assessment__c = car.id;
        insert product;
        return product;
    }
    
    
    public static FOBOChecklist__c getFOBOChecklist(Credit_Assessment__c car){
        FOBOChecklist__c product = new FOBOChecklist__c();
        product.Credit_Assessment__c = car.id;
        product.Opportunity__c = getOpportunity().id;
        //insert product;
        return product;
    }
    
    
    public static OpportunityLineItem getOpportunityLineItem(Credit_Assessment__c car){
        OpportunityLineItem product = new OpportunityLineItem();
        product.Credit_Assessment__c = car.id;
        //insert product;
        return product;
    }

    public static OrderItem getOrderLineItem(Credit_Assessment__c car){
        OrderItem product = new OrderItem();
        product.Credit_Assessment__c = car.id;
        //insert product;
        return product;
    }
}