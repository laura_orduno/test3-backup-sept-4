/**
 * Test class for S2C_OrderCreation
 *
 * @author Thomas Tran, Traction on Demand
 * @date 01-29-2015
 * @storynumber LP ID: 19901617
 */
@isTest(seeAllData=false)
private class S2C_OrderCreationTest {

  	private static testmethod void createOrderOpportunityWithNoProductItems() {
	    Account newAccount = S2C_TestUtility.createAccount('Test Account');
	    insert newAccount;

	    Opportunity newOpportunity = S2C_TestUtility.createOpportunity(newAccount.Id, 'Test Opportunity');
	    newOpportunity.CloseDate = Date.today();
	    newOpportunity.StageName = S2C_TestUtility.ORIGINATED_STAGE_NAME;
	    insert newOpportunity;

	    List<S2C_OrderCreation.OpportunityProductItemInfo> opiInfoList = new List<S2C_OrderCreation.OpportunityProductItemInfo>();

	    Test.startTest();

	    String resultString = S2C_OrderCreation.createOrder(newOpportunity.Id, JSON.serialize(opiInfoList));

	    Test.stopTest();

	    Opportunity salesOpportunity = getSalesOpportunity(newOpportunity.Id);
	    Opportunity orderOpportunity = S2C_TestUtility.getOrderOpportunity(newOpportunity.Id);

	    System.assertEquals('/' + orderOpportunity.Id, resultString);
	    System.assertEquals(salesOpportunity.AccountId, orderOpportunity.AccountId);
	    System.assertEquals(salesOpportunity.OwnerId, orderOpportunity.OwnerId);
	    System.assertEquals(salesOpportunity.Name, orderOpportunity.Name);
	    System.assertEquals(salesOpportunity.Id, orderOpportunity.Sales_Opportunity_Id__c);
	    System.assertEquals(S2C_TestUtility.ORIGINATED_STAGE_NAME, orderOpportunity.StageName);
	    System.assertEquals(S2C_TestUtility.COMPLEX_ORDER_TYPE, orderOpportunity.Type);
	    System.assertEquals(salesOpportunity.Id, orderOpportunity.Sales_Opportunity_Id__c);
	    System.assertEquals(null, orderOpportunity.Primary_Order_Contact__c);
	    System.assert((salesOpportunity.Opportunity_Product_Items__r).isEmpty());
	    System.assert((orderOpportunity.Service_Requests__r).isEmpty());
  	}

  	private static testmethod void createOrderOpportunityWithProductItems() {
	    User newUser = createUser('test001@test.ca.test');

  	System.runAs(newUser){
		      insert S2C_TestUtility.createOrderType();

		      Account newAccount = S2C_TestUtility.createAccount('Test Account');
		      insert newAccount;

		      Opportunity newOpportunity = S2C_TestUtility.createOpportunity(newAccount.Id, 'Test Opportunity');
		      newOpportunity.CloseDate = Date.today();
		      newOpportunity.StageName = S2C_TestUtility.ORIGINATED_STAGE_NAME;
		      newOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Revenue Based Opp').getRecordTypeId();
		      insert newOpportunity;

		      Contact newContact = S2C_TestUtility.createContact('Test');
		      insert newContact;

		      OpportunityContactRole newOpportunityContactRole = S2C_TestUtility.createOpportunityContactRole(newOpportunity.Id, newContact.Id, true);
		      insert newOpportunityContactRole;

		      Product2 newProduct2 = S2C_TestUtility.createProduct('Test Product', 'Test');
		      insert newProduct2;

		      Opp_Product_Item__c newOppProductItem1 = S2C_TestUtility.createOpportunityProductItem(newProduct2.Id, newOpportunity.Id);
		      insert newOppProductItem1;

		      SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct2.Product_Family__c);
		      insert newSRSPodsProduct;

		      S2C_OrderCreation.OpportunityProductItemInfo newOpiItem1 = new S2C_OrderCreation.OpportunityProductItemInfo(newOppProductItem1.Id, newOppProductItem1.Name, newOppProductItem1.Name,'Prequal', newSRSPodsProduct.Id, '12', Date.valueOf('2015-01-01'), 'true');
		      List<S2C_OrderCreation.OpportunityProductItemInfo> opiInfoList = new List<S2C_OrderCreation.OpportunityProductItemInfo>();
		      opiInfoList.add(newOpiItem1);

		      Test.startTest();

		      String resultString = S2C_OrderCreation.createOrder(newOpportunity.Id, JSON.serialize(opiInfoList));

		      Test.stopTest();

		      Opportunity salesOpportunity = getSalesOpportunity(newOpportunity.Id);
		      Opportunity orderOpportunity = S2C_TestUtility.getOrderOpportunity(newOpportunity.Id);
            
	          Opp_Product_Item__c newOppProductItemUpdate = newOppProductItem1;
              newOppProductItemUpdate.Contract_Length_Month__c = 24;
		      Update newOppProductItemUpdate;

		      System.assertEquals('/' + orderOpportunity.Id, resultString);
		      System.assertEquals(salesOpportunity.AccountId, orderOpportunity.AccountId);
		      System.assertEquals(salesOpportunity.OwnerId, orderOpportunity.OwnerId);
		      System.assertEquals(salesOpportunity.Name, orderOpportunity.Name);
		      System.assertEquals(salesOpportunity.Id, orderOpportunity.Sales_Opportunity_Id__c);
		      System.assertEquals(S2C_TestUtility.ORIGINATED_STAGE_NAME, orderOpportunity.StageName);
		      System.assertEquals(S2C_TestUtility.COMPLEX_ORDER_TYPE, orderOpportunity.Type);
		      System.assertEquals(salesOpportunity.Id, orderOpportunity.Sales_Opportunity_Id__c);
		      System.assertEquals(newContact.Id, orderOpportunity.Primary_Order_Contact__c);
		      System.assertEquals(1, (salesOpportunity.Opportunity_Product_Items__r).size());
		      System.assertEquals(1, (orderOpportunity.Service_Requests__r).size());
	    }

    
  }

  private static testmethod void createOrderOpportunityWithNoContactRole() {
    Account newAccount = S2C_TestUtility.createAccount('Test Account');
    insert newAccount;

    Opportunity newOpportunity = S2C_TestUtility.createOpportunity(newAccount.Id, 'Test Opportunity');
    newOpportunity.CloseDate = Date.today();
    newOpportunity.StageName = S2C_TestUtility.ORIGINATED_STAGE_NAME;
    insert newOpportunity;

    Contact newContact = S2C_TestUtility.createContact('Test');
    insert newContact;

    OpportunityContactRole newOpportunityContactRole = S2C_TestUtility.createOpportunityContactRole(newOpportunity.Id, newContact.Id, true);
    insert newOpportunityContactRole;

    List<S2C_OrderCreation.OpportunityProductItemInfo> opiInfoList = new List<S2C_OrderCreation.OpportunityProductItemInfo>();

    Test.startTest();

    String resultString = S2C_OrderCreation.createOrder(newOpportunity.Id, JSON.serialize(opiInfoList));

    Test.stopTest();

    Opportunity salesOpportunity = getSalesOpportunity(newOpportunity.Id);
    Opportunity orderOpportunity = S2C_TestUtility.getOrderOpportunity(newOpportunity.Id);

    System.assertEquals('/' + orderOpportunity.Id, resultString);
    System.assertEquals(salesOpportunity.AccountId, orderOpportunity.AccountId);
    System.assertEquals(salesOpportunity.OwnerId, orderOpportunity.OwnerId);
    System.assertEquals(salesOpportunity.Name, orderOpportunity.Name);
    System.assertEquals(salesOpportunity.Id, orderOpportunity.Sales_Opportunity_Id__c);
    System.assertEquals(S2C_TestUtility.ORIGINATED_STAGE_NAME, orderOpportunity.StageName);
    System.assertEquals(S2C_TestUtility.COMPLEX_ORDER_TYPE, orderOpportunity.Type);
    System.assertEquals(salesOpportunity.Id, orderOpportunity.Sales_Opportunity_Id__c);
    System.assertEquals(newContact.Id, orderOpportunity.Primary_Order_Contact__c);
    System.assert((salesOpportunity.Opportunity_Product_Items__r).isEmpty());
    System.assert((orderOpportunity.Service_Requests__r).isEmpty());
  }
  
  private static testmethod void createOrderOpportunityWithMove(){
  	User newUser = createUser('test001@test.ca.test');

  	System.runAs(newUser){
		      insert S2C_TestUtility.createOrderType();

		      Account newAccount = S2C_TestUtility.createAccount('Test Account');
		      insert newAccount;

		      Opportunity newOpportunity = S2C_TestUtility.createOpportunity(newAccount.Id, 'Test Opportunity');
		      newOpportunity.CloseDate = Date.today();
		      newOpportunity.StageName = S2C_TestUtility.ORIGINATED_STAGE_NAME;
		      newOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Revenue Based Opp').getRecordTypeId();
		      insert newOpportunity;

		      Contact newContact = S2C_TestUtility.createContact('Test');
		      insert newContact;

		      OpportunityContactRole newOpportunityContactRole = S2C_TestUtility.createOpportunityContactRole(newOpportunity.Id, newContact.Id, true);
		      insert newOpportunityContactRole;

		      Product2 newProduct2 = S2C_TestUtility.createProduct('Test Product', 'Test');
		      insert newProduct2;

		      Opp_Product_Item__c newOppProductItem1 = S2C_TestUtility.createOpportunityProductItem(newProduct2.Id, newOpportunity.Id);
		      insert newOppProductItem1;

		      SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct2.Product_Family__c);
		      insert newSRSPodsProduct;

		      S2C_OrderCreation.OpportunityProductItemInfo newOpiItem1 = new S2C_OrderCreation.OpportunityProductItemInfo(newOppProductItem1.Id, newOppProductItem1.Name, newOppProductItem1.Name, 'Move', newSRSPodsProduct.Id, '12', Date.valueOf('2015-01-01'), 'true');
		      List<S2C_OrderCreation.OpportunityProductItemInfo> opiInfoList = new List<S2C_OrderCreation.OpportunityProductItemInfo>();
		      opiInfoList.add(newOpiItem1);

		      Test.startTest();
 	
		      String resultString = S2C_OrderCreation.createOrder(newOpportunity.Id, JSON.serialize(opiInfoList));

		      Test.stopTest();

		      Opportunity salesOpportunity = getSalesOpportunity(newOpportunity.Id);
		      Opportunity orderOpportunity = S2C_TestUtility.getOrderOpportunity(newOpportunity.Id);
            
	          Opp_Product_Item__c newOppProductItemUpdate = newOppProductItem1;
              newOppProductItemUpdate.Contract_Length_Month__c = 24;
		      Update newOppProductItemUpdate;

		      System.assertEquals('/' + orderOpportunity.Id, resultString);
		      System.assertEquals(salesOpportunity.AccountId, orderOpportunity.AccountId);
		      System.assertEquals(salesOpportunity.OwnerId, orderOpportunity.OwnerId);
		      System.assertEquals(salesOpportunity.Name, orderOpportunity.Name);
		      System.assertEquals(salesOpportunity.Id, orderOpportunity.Sales_Opportunity_Id__c);
		      System.assertEquals(salesOpportunity.Id, orderOpportunity.Sales_Opportunity_Id__c);
		      System.assertEquals(newContact.Id, orderOpportunity.Primary_Order_Contact__c);
		      System.assertEquals(1, (salesOpportunity.Opportunity_Product_Items__r).size());
		      System.assertEquals(2, (orderOpportunity.Service_Requests__r).size());

	    }
  }

  private static testmethod void createOrderOpportunityWithMoveNOSR(){
  	User newUser = createUser('test001@test.ca.test');

  	System.runAs(newUser){
		      insert S2C_TestUtility.createOrderType();

		      Account newAccount = S2C_TestUtility.createAccount('Test Account');
		      insert newAccount;

		      Opportunity newOpportunity = S2C_TestUtility.createOpportunity(newAccount.Id, 'Test Opportunity');
		      newOpportunity.CloseDate = Date.today();
		      newOpportunity.StageName = S2C_TestUtility.ORIGINATED_STAGE_NAME;
		      newOpportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Revenue Based Opp').getRecordTypeId();
		      insert newOpportunity;

		      Contact newContact = S2C_TestUtility.createContact('Test');
		      insert newContact;

		      OpportunityContactRole newOpportunityContactRole = S2C_TestUtility.createOpportunityContactRole(newOpportunity.Id, newContact.Id, true);
		      insert newOpportunityContactRole;

		      Product2 newProduct2 = S2C_TestUtility.createProduct('Test Product', 'Test');
		      insert newProduct2;

		      Opp_Product_Item__c newOppProductItem1 = S2C_TestUtility.createOpportunityProductItem(newProduct2.Id, newOpportunity.Id);
		      insert newOppProductItem1;

		      SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct2.Product_Family__c);
		      insert newSRSPodsProduct;

		      S2C_OrderCreation.OpportunityProductItemInfo newOpiItem1 = new S2C_OrderCreation.OpportunityProductItemInfo(newOppProductItem1.Id, newOppProductItem1.Name, newOppProductItem1.Name, 'Move', newSRSPodsProduct.Id, '12', Date.valueOf('2015-01-01'), 'false');
		      List<S2C_OrderCreation.OpportunityProductItemInfo> opiInfoList = new List<S2C_OrderCreation.OpportunityProductItemInfo>();
		      opiInfoList.add(newOpiItem1);

		      Test.startTest();
 	
		      String resultString = S2C_OrderCreation.createOrder(newOpportunity.Id, JSON.serialize(opiInfoList));

		      Test.stopTest();

		      Opportunity salesOpportunity = getSalesOpportunity(newOpportunity.Id);
		      Opportunity orderOpportunity = S2C_TestUtility.getOrderOpportunity(newOpportunity.Id);
            
	          Opp_Product_Item__c newOppProductItemUpdate = newOppProductItem1;
              newOppProductItemUpdate.Contract_Length_Month__c = 24;
		      Update newOppProductItemUpdate;

		      System.assertEquals('/' + orderOpportunity.Id, resultString);
		      System.assertEquals(salesOpportunity.AccountId, orderOpportunity.AccountId);
		      System.assertEquals(salesOpportunity.OwnerId, orderOpportunity.OwnerId);
		      System.assertEquals(salesOpportunity.Name, orderOpportunity.Name);
		      System.assertEquals(salesOpportunity.Id, orderOpportunity.Sales_Opportunity_Id__c);
		      System.assertEquals(salesOpportunity.Id, orderOpportunity.Sales_Opportunity_Id__c);
		      System.assertEquals(newContact.Id, orderOpportunity.Primary_Order_Contact__c);
		      System.assertEquals(1, (salesOpportunity.Opportunity_Product_Items__r).size());
		      System.assertEquals(0, (orderOpportunity.Service_Requests__r).size());

	    }
  }

  /**
   * Retrieves the Sales Opportunity
   * @param  Id
   * @return Opportunity
   */
  private static Opportunity getSalesOpportunity(Id salesOpportunityId){
    Opportunity orderOpportunity = [
      SELECT AccountId, OwnerId, Name, 
        (SELECT Product_Family__c FROM Opportunity_Product_Items__r)
      FROM Opportunity
      WHERE Id = :salesOpportunityId
    ];

    return orderOpportunity;
  }

  public static User createUser(String email){
		User newUser;
		Id profileId;

		profileId = [
			SELECT Id 
			FROM Profile 
			WHERE Name='System Administrator'
			LIMIT 1
		].Id;

		newUser = new User(
			Alias = 'standt',
			Email=email, 
			EmailEncodingKey='UTF-8',
			LastName='Testing',
			LanguageLocaleKey='en_US', 
			LocaleSidKey='en_US',
			ProfileId = profileId, 
			TimeZoneSidKey='America/Los_Angeles',
			UserName=email
		);

		return newUser;
	}

}