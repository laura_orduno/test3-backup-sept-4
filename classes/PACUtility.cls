/******************************************************************************
# File..................: PACUtility
# Version...............: 1
# Created by............: Sandip Chaudhari (IBM)
# Description...........: This class used send address validation request to SACG team, 
                          also send email notification to the SACG team
# 1. Modified By -
# 2. Modified By -
******************************************************************************/


global class PACUtility{
    public List<selectOption> countryList {get;set;}
    public List<selectOption> usStateList {get;set;}
    public List<selectOption> canadaProvinceList {get;set;}
    
    public PACUtility(){
        countryList = new List<selectOption>();
        usStateList = new List<selectOption>();
        canadaProvinceList = new List<selectOption>();
    }
    
    /**
        Sort field to use in SelectOption i.e. Label or Value
    */
    public enum FieldToSort {
        Label, Value
    }
    
    /* Name - sendToSACG
    * Parameter - List<ServiceAddressData>
    * Description - This methods will create case and send emil to SACG team
    */
    public void sendToSACG(List<ServiceAddressData> saData){
        String errMessages = '';
        list<Messaging.Singleemailmessage> theEmailMessages = new list<Messaging.Singleemailmessage>();
        Set<String> addresses = new Set<String>();
        List<Case> caseList = new List<Case>();
        RecordType recType = [SELECT Id from Recordtype where sobjectType = 'Case' and developerName ='SMB_Care_SACG' and IsActive =true limit 1];
        User u = [select Id, Name from User where Id = :UserInfo.getUserId()];
        
        for(ServiceAddressData saObj: saData){
            if(addresses.contains(saObj.SACGAddress)){
                saObj.returnMessage = 'Duplicate in list of SACG function';
                saObj.sacgStatus = 'Duplicate';
            }else{
                addresses.add(saObj.SACGAddress);
            }
        }
        caseList = [Select Id, Service_Location_Address__c from case where Service_Location_Address__c IN: addresses AND RecordTypeId =: recType.Id];
        Set<String> addrSet = new Set<String>();
 
        if(caseList !=  null && caseList.size() > 0){
            for(Case caseObj: caseList){
                addrSet.add(caseObj.Service_Location_Address__c);
            }
            for(ServiceAddressData saObj: saData){
                if(addrSet.contains(saObj.SACGAddress)){
                    saObj.returnMessage = 'Case already exists for selected address';
                    saObj.sacgStatus = 'Duplicate';
                }
            }
        }
        
        for(ServiceAddressData saObj: saData){
            if(saObj.sacgStatus != 'Duplicate' && saObj.isSACG == true && saObj.SACGAddress != null && saObj.SACGAddress != ''){
                Case objCase = new Case();
                objCase.RecordTypeId = recType.Id;
                objCase.OwnerId = u.Id;
                objCase.Account_Name__c = saObj.accountId; // SR account name
                objCase.Service_Location_Address__c = saObj.SACGAddress;
                objCase.Description = saObj.notesForSACGTeam; // Notes for SACG team
                objCase.Subject = 'SACG Request';
                objCase.Priority = 'Medium';
                objCase.Origin = 'SACG Request';
                objCase.Status = 'Open';
                ObjCase.Notes__c = ''; //not known
                objCase.ContactId = saObj.contactId;  //SR primary contact
                objCase.Opportunity__c = saObj.opportunityId; // SR opp name
                objCase.Type ='SACG';
                objCase.Special_Instruction__c = '';
                if(saObj.isNewConstruction){
                    objCase.Special_Instruction__c = System.Label.PACNewConstruction;
                }else{
                    objCase.Special_Instruction__c = '';
                }
                if(saObj.isTempJobPhone){
                    if(objCase.Special_Instruction__c != ''){
                        objCase.Special_Instruction__c = objCase.Special_Instruction__c + ' and ' + System.Label.PACTempJobPhome;
                    }else{
                        objCase.Special_Instruction__c = System.Label.PACTempJobPhome;
                    }
                }
                caseList.add(objCase);
            }
        }
        
        try{
            Database.SaveResult[] insCaseList = Database.insert(caseList, false);
            List<Case> insertedCaseList = new List<Case>();
            List<Id> insertedCaseIds = new List<Id>();
            for (Database.SaveResult sr : insCaseList){
                if (sr.isSuccess()){
                    insertedCaseIds.add(sr.getId()); 
                }else{
                    for(Database.Error err : sr.getErrors()) {
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        errMessages = errMessages + ' -> ' + err.getStatusCode() + ': ' + err.getMessage() + ' ';
                    }
                }
    
            }
            insertedCaseList = [Select Id, Service_Location_Address__c, CaseNumber from case where Id IN: insertedCaseIds];
            Map<String, Case>  caseMap = new Map<String, Case>();
            if(insertedCaseList != null && insertedCaseList.size() > 0){
                map<String,SMB_SACGSettings__c> mapSACGSettings = SMB_SACGSettings__c.getAll();
                String strUsers = mapSACGSettings.get('ContactIds').Data__c;
                    
                for (Case csObj: insertedCaseList){
                    caseMap.put(csObj.Service_Location_Address__c, csObj);
                    if(strUsers.trim().length()>0){
                        string [] arrUsers = strUsers.split(';',0);
                        if(arrUsers.size()>0){
                           for (String str:arrUsers){
                                system.debug('***** Creating Email: ' + str);
                                theEmailMessages.add(CreateEmail(str, csObj.Id,mapSACGSettings.get('OrgWideEmailAddressId').Data__c,mapSACGSettings.get('TemplateId').Data__c)); 
                            }
                        }
                    }
                }
            }
            for(ServiceAddressData saObj: saData){
                if(saObj.sacgStatus != 'Duplicate'){
                    if(caseMap.containsKey(saObj.SACGAddress)){
                        Case tmpCaseObj = caseMap.get(saObj.SACGAddress);
                        if(tmpCaseObj != null){
                            saObj.caseId = tmpCaseObj.Id;
                            saObj.caseNumber = tmpCaseObj.CaseNumber;
                            saObj.sacgStatus = 'Success';
                            saObj.fullCaseURL = URL.getSalesforceBaseUrl().toExternalForm()+'/'+ tmpCaseObj.Id;
                            saObj.returnMessage = 'Case has been created, Case number is '+ tmpCaseObj.CaseNumber;
                        }
                    }else{
                        saObj.sacgStatus = 'Error';    
                        saObj.returnMessage = 'Error occurred, please contact system administrator. Errors - ' + errMessages  ;
                    }
                }
            }
        
            if (theEmailMessages != null && theEmailMessages.size() > 0){
                system.debug('>>>>> Sending Email:' + theEmailMessages);
                Messaging.sendEmail(theEmailMessages, false);                   
            }
        }catch(Exception ex){
            for(ServiceAddressData saObj: saData){
                saObj.sacgStatus = 'Exception';    
                saObj.returnMessage = 'Exception !!!, please contact system administrator - ' + ex;
            }
        }
        return;
    }
    
     /* Name - sendToSACG
    * Parameter - List<ServiceAddressData>
    * Description - This methods will create case and send emil to SACG team
    */
    public void sendToSACG(ServiceAddressData saData){
        List<ServiceAddressData> saData1 = new List<ServiceAddressData>();
        saData1.add(saData);
        sendToSACG(saData1);
        return;
        
    }
    
     /* Name - CreateEmail
    * Parameter - List<ServiceAddressData>
    * Description - This methods will send emil to SACG team
    */
    public static Messaging.Singleemailmessage CreateEmail  (string targetId, Id theCaseId, String orgwideId, string templateId)
     {
        Messaging.Singleemailmessage theEmailMessage = new Messaging.Singleemailmessage();
        theEmailMessage.setOrgWideEmailAddressId(orgwideId);
        theEmailMessage.setTemplateId(templateId);                                                
        theEmailMessage.setSaveAsActivity(true);                        
        theEmailMessage.setTargetObjectId(targetId);                      
        theEmailMessage.setWhatId(theCaseId);       
        return theEmailMessage;     
     }
    
   
   /* 
    * getCountries - Used to get list of countries and States, invoked from constructor of this class
                    - This method added as a part of PAC R2 to minimize the SOQL
    * Parameter - 
    * Return Type - void
    */
    public void getCountryStateList(){
        List<Country_Province_Codes__c> allData = Country_Province_Codes__c.getAll().values();
        List<selectOption> usStateListTmp = new List<selectOption>();
        List<selectOption> canadaProvinceListTmp = new List<selectOption>();
        usStateList.add(new SelectOption('', 'Select'));
        canadaProvinceList.add(new SelectOption('', 'Select'));
        if(allData != null){
            for (Country_Province_Codes__c obj: allData){
                if (obj.Type__c == 'Country'){
                    countryList.add(new SelectOption(obj.Name, obj.Description__c));
                }
                
                if (obj.Type__c == 'American State'){
                    usStateListTmp.add(new SelectOption(obj.Name, obj.Description__c));
                }
                
                if (obj.Type__c == 'Canadian Province'){
                    canadaProvinceListTmp.add(new SelectOption(obj.Name, obj.Description__c));
                }
            }
       }
       doSort(canadaProvinceListTmp, FieldToSort.Label);
       doSort(usStateListTmp, FieldToSort.Label);
       doSort(countryList, FieldToSort.Label);
       for(selectOption obj: usStateListTmp){
           usStateList.add(obj);
       }
       for(selectOption obj: canadaProvinceListTmp){
           canadaProvinceList.add(obj);
       }
       usStateList.add(new SelectOption(' ', ' '));
       canadaProvinceList.add(new SelectOption(' ', ' '));

    }
    
     /* Name - doSort
    * Parameter - List<Selectoption>, FieldToSort 
    * Description - This methods will sort select options list
    */ 
    public void doSort(List<Selectoption> opts, FieldToSort sortField) {
        
        Map<String, Selectoption> mapping = new Map<String, Selectoption>();
        // Suffix to avoid duplicate values like same labels or values are in inbound list 
        Integer suffix = 1;
        for (Selectoption opt : opts) {
            if (sortField == FieldToSort.Label) {
                mapping.put(    // Done this cryptic to save scriptlines, if this loop executes 10000 times
                                // it would every script statement would add 1, so 3 would lead to 30000.
                             (opt.getLabel() + suffix++), // Key using Label + Suffix Counter  
                             opt);   
            } else {
                mapping.put(    
                             (opt.getValue() + suffix++), // Key using Label + Suffix Counter  
                             opt);   
            }
        }
        
        List<String> sortKeys = new List<String>();
        sortKeys.addAll(mapping.keySet());
        sortKeys.sort();
        // clear the original collection to rebuilt it
        opts.clear();
        
        for (String key : sortKeys) {
            opts.add(mapping.get(key));
        }
    } 
    
    webservice static String getInstanceName(){
            String instanceName = [select InstanceName from Organization limit 1].InstanceName;
            return instanceName;
    }
}