public with sharing class smb_OutputAbbreviatedTextController {

	public string text {get;set;}
	
	public integer maxWidth {get;set;}
	
	public string abbreviatedText {
		get {
			if (text == null) return null;
			return text.abbreviate(maxWidth);
		}
	}

}