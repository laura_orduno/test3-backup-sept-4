@isTest
private class QuoteComponentParentTest {
	testMethod static void testQuoteComponentParent() {
      QuoteComponentController qcc = new QuoteComponentController();
      QuoteComponentParent qcp = new QuoteComponentParent();
      System.assertEquals(null, qcp.getComponentController());
      
      qcc.parentController = qcp;
      System.assertEquals(qcp, qcc.parentController);
      
      qcp.setComponentController(qcc);
      System.assertEquals(qcc, qcp.getComponentController());
      System.assertEquals(qcp, qcp.getThis());
  }
}