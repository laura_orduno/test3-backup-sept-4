/**
 * Unit tests for <code>LeadCompanyScrubber</code> class.
 * 
 * @author Max Rudman
 * @since 6/27/2010
 */
@isTest
private class LeadCompanyScrubberTests {
    testMethod static void testScrub() {
        setUp();
        
        Lead lead = new Lead(FirstName='Jon',LastName='Doe');
        lead.Company = 'University of ACME';
        insert lead;
        System.assertEquals('ACME', [SELECT ScrubbedCompany__c FROM Lead WHERE Id = :lead.Id].ScrubbedCompany__c);
        
        lead.Company = 'ACME Corp.';
        update lead;
        System.assertEquals('ACME', [SELECT ScrubbedCompany__c FROM Lead WHERE Id = :lead.Id].ScrubbedCompany__c);
    }
    
    private static void setUp() {
        ScrubRule__c r1 = new ScrubRule__c(Active__c=true,Name='Test1',Type__c='Prefix',Value__c='University of');
        ScrubRule__c r2 = new ScrubRule__c(Active__c=true,Name='Test2',Type__c='Suffix',Value__c='Corp.');
        insert new List<ScrubRule__c>{r1,r2};
    }
}