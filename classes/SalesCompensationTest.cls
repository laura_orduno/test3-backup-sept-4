/* 
    Added by Rajan T on 3-Aug-2015 for Code Coverge on 'CalculateBusinessHoursAges' Trigger 
    for Sales Compensation Auto-Population functionality (SIS-937) 
    */
@isTest
public class SalesCompensationTest{


    public static testMethod void testDepartment() {
        // Setup test data
        
        List<User> userList = [SELECT id, name, Channel_Callidus__c, state FROM User WHERE IsActive = true AND Channel_Callidus__c != null AND state != null AND profile.name = 'BCX General'];
        List<RecordType> rt = [Select Id from RecordType where Name = 'Sales Compensation Inquiries'];
        
        if(!userList.isEmpty()){
            User u = userList[0];
            System.runAs(u) {
                Test.Starttest();
                
                Case c = new Case();
                c.Status = 'Waiting on Vendor';
                c.RecordTypeId = rt[0].id;
                c.ownerId = u.id;
                
                insert c;
                
                Case updatedCase = [select id, dept__c, region__c from Case where Id=:c.Id];
                Test.Stoptest();
                
                System.assert(updatedCase.dept__c == u.Channel_Callidus__c);
                System.assert(updatedCase.region__c == u.state);
                
            }
        }
    }

}