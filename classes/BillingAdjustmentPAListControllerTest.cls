@isTest
public class BillingAdjustmentPAListControllerTest{

	static{
		BillingAdjustmentPAListControllerTest.loadProductApprovalThresholdSettings();
	}

    @isTest(SeeAllData=true)
    static void testParentAccountBAList(){
      Billing_Adjustment__c ba = BillingAdjustmentCommonTest.createBillingAdjustment(2000, 'Credit', 'THPS');	
      ApexPages.StandardController sc = new ApexPages.standardController(ba);
      BillingAdjustmentPAListController ctrl = new BillingAdjustmentPAListController(sc);
	  System.assertEquals(1, ctrl.baList.size(), 'Should return 1 record');
        
    }

    /**
     * loadProductApprovalThresholdSettings
     * @description Loads the threshold fields to search for in the query - Reason we have to check if the settings exist is becuase
     *              the current test class is set to see all data true.
     * @author Thomas Tran, Traction on Demand
     * @date 04-19-2016
     */
    private static void loadProductApprovalThresholdSettings(){
    	Map<String, Product_Approval_Threshold_Settings__c> currentPatSettings = Product_Approval_Threshold_Settings__c.getAll();

    	if(currentPatSettings.isEmpty()){
	        List<Product_Approval_Threshold_Settings__c> patSettings = new List<Product_Approval_Threshold_Settings__c>();

	        patSettings.add(new Product_Approval_Threshold_Settings__c(Name='Credit', Threshold_API_Name__c='Credit_Threshold__c'));
	        patSettings.add(new Product_Approval_Threshold_Settings__c(Name='Debit', Threshold_API_Name__c='Debit_Threshold__c'));
	        patSettings.add(new Product_Approval_Threshold_Settings__c(Name='TLC Charge', Threshold_API_Name__c='TLC_Charge_Threshold__c'));
	        patSettings.add(new Product_Approval_Threshold_Settings__c(Name='TLC Waive', Threshold_API_Name__c='TLC_Waive_Threshold__c'));

	        insert patSettings;
	    }

    }
}