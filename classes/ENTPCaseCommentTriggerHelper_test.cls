@isTest(SeeAllData=false)
public class ENTPCaseCommentTriggerHelper_test {

	@isTest public static void processCaseComments_test() {
		String userName = 'test-user-1';
		String lang = 'en_US';
		Account a = new Account(Name = 'Unit Test Account');
		a.strategic__c = true;
		insert a;
		Contact c = new Contact(Email = userName + '@unit-test.com', LastName = 'Test', AccountId = a.Id);
		insert c;
		Profile p = [SELECT Id FROM Profile WHERE Name = 'Customer Community User Custom' AND UserType = 'CSPLitePortal' LIMIT 1];
		User u = new User(Alias = 'TestUser', Email = userName + '@unit-test.com', ContactId = c.Id,
				EmailEncodingKey = 'UTF-8', FirstName = 'Unit', LastName = 'Testing', LanguageLocaleKey = lang,
				LocaleSidKey = lang, CommunityNickname = userName + 'UNTST', ProfileId = p.Id,
				TimeZoneSidKey = 'America/Los_Angeles',
				UserName = userName + '@unit-test.com');
		u.Customer_Portal_Role__C = 'tps';
		insert u;

		Test.startTest();
		System.runAs(u) {
			List<Case> testCases = ENTPTestUtils.createENTPCases(2, a.Id);
			//create a comment - not EMAIL Comment
			ENTPTestUtils.createComment(testCases[0].Id, false, false);
			//create a comment - EMAIL Comment
			ENTPTestUtils.createComment(testCases[0].Id, true, true);
			List<CaseComment> caseComments = [Select id,CommentBody,Createdby.LastName,Createdby.firstname, CreatedDate from CaseComment where ParentId = :testCases[0].Id];
			//Email message create
			// ENTPCaseCommentTriggerHelper ctlr = new ENTPCaseCommentTriggerHelper();
			Set<Id> caseCommentIds = new Set<Id>();
			for (CaseComment cc : caseComments) {
				caseCommentIds.add(cc.id);
			}
			trac_TriggerHandlerBase.blockTrigger = true;
			ENTPCaseCommentTriggerHelper.processCaseComments(caseCommentIds);
		}
		Test.stopTest();
	}
}