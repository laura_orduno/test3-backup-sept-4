public virtual with sharing class ENTPAuthCheckCtlr extends Portal_AuthCheckCtlr {

	public String CatListPull = '';

	public String AddFuncPull1 { get; set; }
	public String CustRole = '';
	public String CustRole1 { get; set; }
	public static PageReference loginPageReferenceEntp;
	public static String userLanguage {
		get {
			return PortalUtils.getUserLanguage();
		}
		set;
	}

	static {
		String orgId = UserInfo.getOrganizationId();
		String startUrl = ApexPages.currentPage().getUrl();
		loginPageReferenceEntp = new PageReference(startUrl + '/ENTP/login?so=' + orgId);
		loginPageReferenceEntp.getParameters().put('startUrl', startUrl.replace('/apex/', ''));
	}

	public ENTPAuthCheckCtlr() {
		super(loginPageReferenceEntp, Page.MBRNonPortalUsersError);
	}

	public PageReference initCheck() {
		PageReference resultPR = redirectIfGuest();
		if (resultPR != null) {
			return resultPR;
		}

		getTypeofCust();

		if (theUser != null) {
			PageReference resultPR2 = redirectIfNoAccount();
			if (resultPR2 != null) {
				return resultPR2;
			}
		}

		return null;
	}

	public String getLanguageLocaleKey() {
		return userLanguage;
	}

	public String getTypeofCust() {
		String Chat_Assign_group = '';
		if (theUser != null) {
			CustRole = theUser.Customer_Portal_Role__c;
			if (CustRole != '' && CustRole != null) {
				String[] CustRoleT = CustRole.split(';');
				CustRole1 = CustRoleT[0];

				List<ENTP_portal_additional_functions__c> ENTPPortalMap = [
						Select Name,Customer_Portal_Role__c,Additional_Function__c
						from ENTP_portal_additional_functions__c
						where Customer_Portal_Role__c = :CustRole
				];
				if (ENTPPortalMap.size() > 0) {
					CatListPull = ENTPPortalMap[0].Additional_Function__c;
					String[] CatListPullT = CatListPull.split(';');
					AddFuncPull1 = CatListPullT[0];
				}
			}
			List<account> Accounts = [SELECT Strategic__c, ID from Account where ID = :theUser.AccountId];
			//if (theUser.Account.Strategic__c == true) {
			if (!Accounts.isEmpty() && Accounts[0].Strategic__c == true) {
				if (userLanguage == 'en_US' || userLanguage == 'fr') {

					Chat_Assign_group = '1'; //English/CCL NCSC
				}
				/*if (userLanguage == 'fr') {

					Chat_Assign_group = '1'; //French/CCL NCSC

				}*/

			} else {

				if (userLanguage == 'en_US' || userLanguage == 'fr') {

					Chat_Assign_group = '0'; //English NCSC
				}
				/*if (userLanguage == 'fr') {

					Chat_Assign_group = '0'; //French NCSC

				}*/
			}
		}
		return Chat_Assign_group;
	}

}