@istest(seealldata=false)
class TestUnifiedSelfServeID{
    @testsetup
    static void setupTestData(){
        map<string,schema.recordtypeinfo> accountRecordTypeInfosMap=account.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        account rcidAccount=new account(name='Test RCID',language_preference__c='English',recordtypeid=accountRecordTypeInfosMap.get('RCID').getrecordtypeid());
        insert rcidAccount;
        account rcid1=new account(name='Test RCID1',language_preference__c='English',recordtypeid=accountRecordTypeInfosMap.get('RCID').getrecordtypeid());
        insert rcid1;
        account canAccount=new account(name='Test CAN',language_preference__c='English',parentid=rcidAccount.id,recordtypeid=accountRecordTypeInfosMap.get('CAN').getrecordtypeid());
        insert canAccount;
        account banAccount=new account(name='Test BAN',language_preference__c='English',parentid=rcidAccount.id,recordtypeid=accountRecordTypeInfosMap.get('BAN').getrecordtypeid());
        insert banAccount;
        contact newContact=new contact(firstname='Test',lastname='TestLastname',email='test.email@testemail.com',accountid=rcidAccount.id,self_serve_uuid__c='21EC2020-3AEA-1069',active__c=true);
        insert newContact;
        contact dupContact=new contact(firstname='Test',lastname='Duplastname',email='test.email@testemail.com',accountid=rcidAccount.id,self_serve_uuid__c='',active__c=true);
        insert dupContact;
        contact_linked_billing_account__c contactBillingAccount=new contact_linked_billing_account__c(active__c=true,billing_account__c=canAccount.id,contact__c=newContact.id,last_integration_status__c='Pending');
        insert contactBillingAccount;
        webservice_config__c associateService=new webservice_config__c(name='AssociateService',system_domain__c='Profile Management',client_cert_name__c='sfdc_bse',method__c='POST',timeout__c=120000,endpoint__c='https://webservice1.preprd.teluslabs.net/rest/v1-4/cmo/selfmgmt/profilemanagement_vs1/service-association');
        webservice_config__c disassociateService=new webservice_config__c(name='DisassociateService',system_domain__c='Profile Management',client_cert_name__c='sfdc_bse',method__c='POST',timeout__c=120000,endpoint__c='https://webservice1.preprd.teluslabs.net/rest/v1-4/cmo/selfmgmt/profilemanagement_vs1/service-disassociation');
        insert associateService;
        insert disassociateService;
    }
    @istest
    static void testViewUUIDPending(){
        contact_linked_billing_account__c contactBillingAccount=[select id,active__c,billing_account__c,contact__c,last_integration_status__c from contact_linked_billing_account__c limit 1];
        contact loadedContact=[select id from contact where id=:contactBillingAccount.contact__c limit 1];
        test.starttest();
        test.setcurrentpagereference(page.unifiedselfserveinfo);
        apexpages.standardcontroller standardController=new apexpages.standardcontroller(loadedContact);
        UnifiedSelfServeInfoExt controller=new UnifiedSelfServeInfoExt(standardController);
        controller.updateContactBillingAccount();
        test.stoptest();
    }
    @istest
    static void testViewUUIDComplete(){
        contact_linked_billing_account__c contactBillingAccount=[select id,active__c,billing_account__c,contact__c,last_integration_status__c from contact_linked_billing_account__c limit 1];
        contact loadedContact=[select id from contact where id=:contactBillingAccount.contact__c limit 1];
        contactBillingAccount.last_integration_status__c='Completed';
        update contactBillingAccount;
        test.starttest();
        test.setcurrentpagereference(page.unifiedselfserveinfo);
        apexpages.standardcontroller standardController=new apexpages.standardcontroller(loadedContact);
        UnifiedSelfServeInfoExt controller=new UnifiedSelfServeInfoExt(standardController);
        controller.updateContactBillingAccount();
        test.stoptest();
    }
    @istest
    static void testInsertUpdateDuplicateContactLinkedBillingAccount(){
        contact_linked_billing_account__c contactBillingAccount=[select id,active__c,billing_account__c,contact__c,last_integration_status__c from contact_linked_billing_account__c limit 1];
        contact_linked_billing_account__c cloneContactBillingAccount=contactBillingAccount.clone(false,true,false,false);
        contact loadedContact=[select id from contact where id=:contactBillingAccount.contact__c limit 1];
        test.starttest();
        try{
            staticresourcecalloutmock mock=new staticresourcecalloutmock();
            mock.setstaticresource('UUIDAssociationSuccessfulResponse');
            mock.setstatuscode(200);
            mock.setheader('Content-Type','application/json');
            test.setmock(httpcalloutmock.class,mock);
            cloneContactBillingAccount.last_integration_status__c='Completed';
            insert cloneContactBillingAccount;
            test.setcurrentpagereference(page.unifiedselfserveinfo);
            apexpages.standardcontroller standardController=new apexpages.standardcontroller(loadedContact);
            UnifiedSelfServeInfoExt controller=new UnifiedSelfServeInfoExt(standardController);
            controller.updateContactBillingAccount();
        }catch(dmlexception dmle){
            // Expected to have error on inserting same email address contact.
        }
        test.stoptest();
    }
    @istest
    static void testDisassociateBillingAccount(){
        contact_linked_billing_account__c contactBillingAccount=[select id,active__c,billing_account__c,contact__c,last_integration_status__c from contact_linked_billing_account__c limit 1];
        test.starttest();
        try{
            staticresourcecalloutmock mock=new staticresourcecalloutmock();
            mock.setstaticresource('UUIDDisassociationSuccessfulResponse');
            mock.setstatuscode(200);
            mock.setheader('Content-Type','application/json');
            test.setmock(httpcalloutmock.class,mock);
            test.setcurrentpagereference(page.ContactLinkedBillingAccount);
            apexpages.standardcontroller standardController=new apexpages.standardcontroller(contactBillingAccount);
            ContactLinkedBillingAccountExt controller=new ContactLinkedBillingAccountExt(standardController);
            controller.association.active__c=false;
            controller.doSave();
        }catch(dmlexception dmle){
            // Expected to have error on inserting same email address contact.
        }
        test.stoptest();
    }
    @istest
    static void testAssociateContactWithSameEmailContactLinkedBillingAccount(){
        contact_linked_billing_account__c contactBillingAccount=[select id,active__c,billing_account__c,contact__c,last_integration_status__c from contact_linked_billing_account__c limit 1];
        contact_linked_billing_account__c cloneContactBillingAccount=contactBillingAccount.clone(false,true,false,false);
        contact dupContact=[select id from contact where lastname='Duplastname' limit 1];
        test.starttest();
        try{
            staticresourcecalloutmock mock=new staticresourcecalloutmock();
            mock.setstaticresource('UUIDAssociationSuccessfulResponse');
            mock.setstatuscode(200);
            mock.setheader('Content-Type','application/json');
            test.setmock(httpcalloutmock.class,mock);
            test.setcurrentpagereference(page.ContactLinkedBillingAccount);
            apexpages.standardcontroller standardController=new apexpages.standardcontroller(cloneContactBillingAccount);
            ContactLinkedBillingAccountExt controller=new ContactLinkedBillingAccountExt(standardController);
            controller.association.contact__c=dupContact.id;
            controller.doSave();
        }catch(dmlexception dmle){
            // Expected to have error on inserting same email address contact.
        }
        test.stoptest();
    }
    @istest
    static void testAssociateExistingBillingAccountWithSameContactContactLinkedBillingAccount(){
        contact_linked_billing_account__c contactBillingAccount=[select id,active__c,billing_account__c,contact__c,last_integration_status__c from contact_linked_billing_account__c limit 1];
        contact_linked_billing_account__c cloneContactBillingAccount=contactBillingAccount.clone(false,true,false,false);
        test.starttest();
        try{
            staticresourcecalloutmock mock=new staticresourcecalloutmock();
            mock.setstaticresource('UUIDAssociationSuccessfulResponse');
            mock.setstatuscode(200);
            mock.setheader('Content-Type','application/json');
            test.setmock(httpcalloutmock.class,mock);
            cloneContactBillingAccount.last_integration_status__c='Completed';
            test.setcurrentpagereference(page.ContactLinkedBillingAccount);
            apexpages.standardcontroller standardController=new apexpages.standardcontroller(cloneContactBillingAccount);
            ContactLinkedBillingAccountExt controller=new ContactLinkedBillingAccountExt(standardController);
            controller.doSave();
        }catch(dmlexception dmle){
            // Expected to have error on inserting same email address contact.
        }
        test.stoptest();
    }
    @istest
    static void testAssociateDisassociatedBillingAccount(){
        contact_linked_billing_account__c contactBillingAccount=[select id,active__c,billing_account__c,contact__c,last_integration_status__c from contact_linked_billing_account__c limit 1];
        contact_linked_billing_account__c cloneContactBillingAccount=contactBillingAccount.clone(false,true,false,false);
        test.starttest();
        try{
            staticresourcecalloutmock mock=new staticresourcecalloutmock();
            mock.setstaticresource('UUIDAssociationSuccessfulResponse');
            mock.setstatuscode(200);
            mock.setheader('Content-Type','application/json');
            test.setmock(httpcalloutmock.class,mock);
            cloneContactBillingAccount.active__c=false;
            insert cloneContactBillingAccount;
            test.setcurrentpagereference(page.ContactLinkedBillingAccount);
            apexpages.standardcontroller standardController=new apexpages.standardcontroller(cloneContactBillingAccount);
            ContactLinkedBillingAccountExt controller=new ContactLinkedBillingAccountExt(standardController);
            controller.association.active__c=true;
            controller.doSave();
        }catch(dmlexception dmle){
            // Expected to have error on inserting same email address contact.
        }
        test.stoptest();
    }
    @istest
    static void testAssociateBANAccount(){
        contact_linked_billing_account__c contactBillingAccount=[select id,active__c,billing_account__c,contact__c,last_integration_status__c from contact_linked_billing_account__c limit 1];
        account banAccount=[select id from account where name='Test BAN' limit 1];
        contact_linked_billing_account__c cloneContactBillingAccount=contactBillingAccount.clone(false,true,false,false);
        test.starttest();
        try{
            staticresourcecalloutmock mock=new staticresourcecalloutmock();
            mock.setstaticresource('UUIDAssociationSuccessfulResponse');
            mock.setstatuscode(200);
            mock.setheader('Content-Type','application/json');
            test.setmock(httpcalloutmock.class,mock);
            test.setcurrentpagereference(page.ContactLinkedBillingAccount);
            apexpages.standardcontroller standardController=new apexpages.standardcontroller(cloneContactBillingAccount);
            ContactLinkedBillingAccountExt controller=new ContactLinkedBillingAccountExt(standardController);
            controller.association.billing_account__c=banAccount.id;
            controller.doSave();
        }catch(dmlexception dmle){
            // Expected to have error on inserting same email address contact.
        }
        test.stoptest();
    }
    @istest
    static void testAssociateNonPilotAccount(){
        contact_linked_billing_account__c contactBillingAccount=[select id,active__c,billing_account__c,contact__c,last_integration_status__c from contact_linked_billing_account__c limit 1];
        account rcid1=[select id from account where name='Test RCID1' limit 1];
        contact nonPilotContact=[select id,accountid from contact where lastname='TestLastname' limit 1];
        nonPilotContact.accountid=rcid1.id;
        update nonPilotContact;
        contact_linked_billing_account__c cloneContactBillingAccount=contactBillingAccount.clone(false,true,false,false);
        test.starttest();
        try{
            staticresourcecalloutmock mock=new staticresourcecalloutmock();
            mock.setstaticresource('UUIDAssociationSuccessfulResponse');
            mock.setstatuscode(200);
            mock.setheader('Content-Type','application/json');
            test.setmock(httpcalloutmock.class,mock);
            test.setcurrentpagereference(page.ContactLinkedBillingAccount);
            apexpages.standardcontroller standardController=new apexpages.standardcontroller(cloneContactBillingAccount);
            ContactLinkedBillingAccountExt controller=new ContactLinkedBillingAccountExt(standardController);
            controller.association.contact__c=nonPilotContact.id;
            controller.doSave();
        }catch(dmlexception dmle){
            // Expected to have error on inserting same email address contact.
        }
        test.stoptest();
    }
    @istest
    static void testAssociateBillingAccountSave(){
        contact_linked_billing_account__c contactBillingAccount=[select id,active__c,billing_account__c,contact__c,last_integration_status__c from contact_linked_billing_account__c limit 1];
        contact_linked_billing_account__c cloneContactBillingAccount=contactBillingAccount.clone(false,true,false,false);
        delete contactBillingAccount;
        test.starttest();
        try{
            staticresourcecalloutmock mock=new staticresourcecalloutmock();
            mock.setstaticresource('UUIDAssociationSuccessfulResponse');
            mock.setstatuscode(200);
            mock.setheader('Content-Type','application/json');
            test.setmock(httpcalloutmock.class,mock);
            test.setcurrentpagereference(page.ContactLinkedBillingAccount);
            apexpages.standardcontroller standardController=new apexpages.standardcontroller(cloneContactBillingAccount);
            ContactLinkedBillingAccountExt controller=new ContactLinkedBillingAccountExt(standardController);
            controller.doSave();
        }catch(dmlexception dmle){
            // Expected to have error on inserting same email address contact.
        }
        test.stoptest();
    }
    @istest
    static void testAssociateWebserviceExceptionFailure(){
        contact_linked_billing_account__c contactBillingAccount=[select id,active__c,billing_account__c,contact__c,last_integration_status__c from contact_linked_billing_account__c limit 1];
        contact_linked_billing_account__c cloneContactBillingAccount=contactBillingAccount.clone(false,true,false,false);
        webservice_config__c webConf=[select id from webservice_config__c where name='AssociateService' limit 1];
        delete contactBillingAccount;
        delete webConf;
        test.starttest();
        try{
            staticresourcecalloutmock mock=new staticresourcecalloutmock();
            mock.setstaticresource('UUIDAssociationSuccessfulResponse');
            mock.setstatuscode(200);
            mock.setheader('Content-Type','application/json');
            test.setmock(httpcalloutmock.class,mock);
            test.setcurrentpagereference(page.ContactLinkedBillingAccount);
            apexpages.standardcontroller standardController=new apexpages.standardcontroller(cloneContactBillingAccount);
            ContactLinkedBillingAccountExt controller=new ContactLinkedBillingAccountExt(standardController);
            controller.doSave();
        }catch(dmlexception dmle){
            // Expected to have error on inserting same email address contact.
        }
        test.stoptest();
    }
    @istest
    static void testDisassociateWebserviceExceptionFailure(){
        contact_linked_billing_account__c contactBillingAccount=[select id,active__c,billing_account__c,contact__c,last_integration_status__c from contact_linked_billing_account__c limit 1];
        webservice_config__c webConf=[select id from webservice_config__c where name='DisassociateService' limit 1];
        contact_linked_billing_account__c cloneContactBillingAccount=contactBillingAccount.clone(false,true,false,false);
        delete contactBillingAccount;
        delete webConf;
        test.starttest();
        try{
            staticresourcecalloutmock mock=new staticresourcecalloutmock();
            mock.setstaticresource('UUIDAssociationSuccessfulResponse');
            mock.setstatuscode(200);
            mock.setheader('Content-Type','application/json');
            test.setmock(httpcalloutmock.class,mock);
            test.setcurrentpagereference(page.ContactLinkedBillingAccount);
            apexpages.standardcontroller standardController=new apexpages.standardcontroller(cloneContactBillingAccount);
            ContactLinkedBillingAccountExt controller=new ContactLinkedBillingAccountExt(standardController);
            controller.doSave();
        }catch(dmlexception dmle){
            // Expected to have error on inserting same email address contact.
        }
        test.stoptest();
    }
    @istest
    static void testAssociateWebserviceCalloutFailure(){
        contact_linked_billing_account__c contactBillingAccount=[select id,active__c,billing_account__c,contact__c,last_integration_status__c from contact_linked_billing_account__c limit 1];
        contact_linked_billing_account__c cloneContactBillingAccount=contactBillingAccount.clone(false,true,false,false);
        webservice_config__c webConf=[select id,endpoint__c from webservice_config__c where name='AssociateService' limit 1];
        webConf.endpoint__c='https://testdomain.unabletoconnect.calloutexception';
        delete contactBillingAccount;
        update webConf;
        test.starttest();
        try{
            staticresourcecalloutmock mock=new staticresourcecalloutmock();
            mock.setstaticresource('UUIDAssociationSuccessfulResponse');
            mock.setstatuscode(200);
            mock.setheader('Content-Type','application/json');
            test.setmock(httpcalloutmock.class,mock);
            test.setcurrentpagereference(page.ContactLinkedBillingAccount);
            apexpages.standardcontroller standardController=new apexpages.standardcontroller(cloneContactBillingAccount);
            ContactLinkedBillingAccountExt controller=new ContactLinkedBillingAccountExt(standardController);
            controller.doSave();
        }catch(dmlexception dmle){
            // Expected to have error on inserting same email address contact.
        }
        test.stoptest();
    }
    @istest
    static void testDisassociateWebserviceCalloutFailure(){
        contact_linked_billing_account__c contactBillingAccount=[select id,active__c,billing_account__c,contact__c,last_integration_status__c from contact_linked_billing_account__c limit 1];
        webservice_config__c webConf=[select id,endpoint__c from webservice_config__c where name='DisassociateService' limit 1];
        webConf.endpoint__c='https://testdomain.unabletoconnect.calloutexception';
        contact_linked_billing_account__c cloneContactBillingAccount=contactBillingAccount.clone(false,true,false,false);
        delete contactBillingAccount;
        update webConf;
        test.starttest();
        try{
            staticresourcecalloutmock mock=new staticresourcecalloutmock();
            mock.setstaticresource('UUIDAssociationSuccessfulResponse');
            mock.setstatuscode(200);
            mock.setheader('Content-Type','application/json');
            test.setmock(httpcalloutmock.class,mock);
            test.setcurrentpagereference(page.ContactLinkedBillingAccount);
            apexpages.standardcontroller standardController=new apexpages.standardcontroller(cloneContactBillingAccount);
            ContactLinkedBillingAccountExt controller=new ContactLinkedBillingAccountExt(standardController);
            controller.doSave();
        }catch(dmlexception dmle){
            // Expected to have error on inserting same email address contact.
        }
        test.stoptest();
    }
}