public class ContractTriggerHelper {
    // initialize a variable to hold state  
    private static boolean alreadyModified = false;
    private static boolean alreadySubmittedToNc = false;
    
    // get the state
    public static boolean isAlreadyModified() {
        return alreadyModified;
    }
    public static boolean isAlreadySubmittedToNc() {
        return alreadySubmittedToNc;
    }
    public static void setAlreadyModified1() {
        alreadyModified = false;
    }

    // set this to true to keep track of and avoid recursive updates.  Generally set after first time through
    // a trigger.  We can access this in the trigger and avoid recursive updates...
    public static void setAlreadyModified() {
        alreadyModified = true;
    }
    public static void setAlreadySubmittedToNc() {
        alreadySubmittedToNc = true;
    }
    
    // for the list of contracts
    // populate id set for all contracts that are registered
    // // return set 
    public static Set<Id> getRegisteredContractIds(List<Contract> contracts){
        Set<Id> contractIds = new Set<Id>();
        
//        System.debug('getRegisteredContractIds, contracts: ' + contracts);
        
        for(Contract c : contracts){
	        System.debug('getRegisteredContractIds, c.Complex_Fulfillment_Contract__c: ' + c.Complex_Fulfillment_Contract__c);
	        System.debug('getRegisteredContractIds, c.Status: ' + c.Status);
            
            if(String.isNotBlank(c.Complex_Fulfillment_Contract__c) && c.Status == 'Contract Registered'){
                contractIds.add(c.id);
            }	             
        }
        
        System.debug('getRegisteredContractIds, contractIds: ' + contractIds);
        
        return contractIds;      
    }
   
    
    // for the set of contract ids provided, set all cooresponding 
    // vlocity_cmt__ContractLineItem__c records to inactive
    public static void updateVlocityContractLineItemStatus(Set<Id> contractIds){
        try{
    	    List<vlocity_cmt__ContractLineItem__c> items = [SELECT vlocity_cmt__status__c FROM vlocity_cmt__ContractLineItem__c WHERE Replaced_By_Contract__c IN :contractIds AND (Requirement_for_Termination__c=null or Requirement_for_Termination__c='')];
	            
            for(vlocity_cmt__ContractLineItem__c item : items){
                item.vlocity_cmt__status__c = 'Inactive';    
            }

            System.debug('updateVlocityContractLineItemStatus: ' + items);
            
            update items;
        }
        catch(QueryException e){
            System.debug('ContractTriggerHelper.updateStatus: ' + e.getMessage());
            
	        throw e; 
        }
        catch(DMLException e){
            System.debug('ContractTriggerHelper.updateStatus: ' + e.getMessage());

            throw e;             
        }
    }        
}