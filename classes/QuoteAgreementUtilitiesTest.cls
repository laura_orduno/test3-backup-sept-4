@isTest
private class QuoteAgreementUtilitiesTest {
    private static testMethod void testLoadAgreementsByWaybill() {
    	Agreement__c ag = insertTestAgreement();
    	
    	List<Agreement__c> agreements = QuoteAgreementUtilities.loadAgreementsByWaybill('testwbid');
    	
    	system.assertEquals(1,agreements.size());
    	system.assertEquals(ag.id,agreements[0].id);
    }
    
    private static testMethod void testLoadAgreementsByWaybillNull() {
    	String nullString;
    	List<String> nullList;
    	
    	List<Agreement__c> agreements = QuoteAgreementUtilities.loadAgreementsByWaybill(nullString);
    	
    	system.assertEquals(null, agreements);
    	
    	agreements = QuoteAgreementUtilities.loadAgreementsByWaybill(nullList);
    	
    	system.assertEquals(null, agreements);
    }
    
    private static testMethod void testLoadAgreement() {
    	Agreement__c ag = insertTestAgreement();
    	
    	Agreement__c result = QuoteAgreementUtilities.loadAgreement(ag.id);
    	
    	system.assertEquals(ag.id,result.id);
    }
    
    private static testMethod void testLoadAgreementNull() {
    	Agreement__c result = QuoteAgreementUtilities.loadAgreement(null);
    	
    	system.assertEquals(null, result);
    	
    	List<Agreement__c> results = QuoteAgreementUtilities.loadAgreements(null);
    	
    	system.assertEquals(null, results);
    }
    
    private static testMethod void testLoadAgreementsByQuote() {
    	Agreement__c ag = insertTestAgreement();
    	
    	List<Agreement__c> agreements = QuoteAgreementUtilities.loadAgreementsByQuote(ag.quote__c);
    	
    	system.assertEquals(1,agreements.size());
    	system.assertEquals(ag.id,agreements[0].id);
    }
    
    private static testMethod void testLoadAgreementsByQuoteWithReplacement() {
    	Agreement__c ag = insertTestAgreement();
    	
    	Agreement__c ag2 = new Agreement__c(
      	Recipient__c = ag.recipient__c, Recipient__r = ag.recipient__r, Type__c = 'Recombo', Waybill_Id__c = ag.waybill_id__c,
      	Status__c = 'Pending', Sent_Date__c = Datetime.now(), Expiry_Date__c = Date.today().addDays(2), Quote__c = ag.quote__c,
      	Quote__r = ag.quote__r
      );
      insert ag2;
    	
    	List<Agreement__c> agreements = QuoteAgreementUtilities.loadAgreementsByQuote(ag.quote__c, ag.id);
    	
    	system.assertEquals(1,agreements.size());
    	system.assertEquals(ag2.id,agreements[0].id);
    }
    
    private static testMethod void testLoadAgreementsByQuoteNull() {
    	
    	List<Agreement__c> agreements = QuoteAgreementUtilities.loadAgreementsByQuote(null);
    	
    	system.assertEquals(null, agreements);
    }
    
    private static testMethod void testLoadAgreementsByQuoteGroup() {
    	Agreement__c ag = insertTestAgreement();
    	
    	List<Agreement__c> agreements = QuoteAgreementUtilities.loadAgreementsByQuoteGroup(ag.quote_group__c);
    	
    	system.assertEquals(1,agreements.size());
    	system.assertEquals(ag.id,agreements[0].id);
    }
    
    private static testMethod void testLoadAgreementsByQuoteGroupWithReplacement() {
    	Agreement__c ag = insertTestAgreement();
    	
    	Agreement__c ag2 = new Agreement__c(
      	Recipient__c = ag.recipient__c, Recipient__r = ag.recipient__r, Type__c = 'Recombo', Waybill_Id__c = ag.waybill_id__c,
      	Status__c = 'Pending', Sent_Date__c = Datetime.now(), Expiry_Date__c = Date.today().addDays(2), Quote__c = ag.quote__c,
      	Quote__r = ag.quote__r, quote_group__c = ag.quote_group__c
      );
      insert ag2;
    	
    	List<Agreement__c> agreements = QuoteAgreementUtilities.loadAgreementsByQuoteGroup(ag.quote_group__c, ag.id);
    	
    	system.assertEquals(1,agreements.size());
    	system.assertEquals(ag2.id,agreements[0].id);
    }
    
    private static testMethod void testLoadAgreementsByQuoteGroupNull() {
    	
    	List<Agreement__c> agreements = QuoteAgreementUtilities.loadAgreementsByQuoteGroup(null);
    	
    	system.assertEquals(null, agreements);
    }
    
    private static testMethod void testProcessAcceptance() {
    	Agreement__c ag = insertTestAgreement();
    	
    	QuoteAgreementUtilities.processAcceptance(ag.id);
    	
    	String status = [SELECT status__c FROM Agreement__c WHERE Id = :ag.id].status__c;
    	system.assertEquals(QuoteAgreementStatus.SIGNED, status);

    	QuoteAgreementUtilities.processAcceptance(ag.id, true);
    }
    
    private static testMethod void testProcessAcceptanceInvalidId() {
    	Boolean caughtException = false;
    	
    	try {
    		QuoteAgreementUtilities.processAcceptance('ZZZZZZZZZZZZZZZZZZ');
    	} catch (Exception e) {
    		caughtException = true;
    		system.assertEquals('Invalid Agreement Id', e.getMessage());
    	}
    	
    	system.assert(caughtException);
    }
    
    private static testMethod void testProcessAcceptanceNoMatch() {
    	Boolean caughtException = false;
    	
    	try {
    		QuoteAgreementUtilities.processAcceptance(Agreement__c.SObjectType.getDescribe().getKeyPrefix() + 'ZZZZZZZZZZZZZZZ');
    	} catch (Exception e) {
    		caughtException = true;
    		system.assertEquals('Unable to load agreement', e.getMessage());
    	}
    	
    	system.assert(caughtException);
    }
    
    private static testMethod void testExpireOldQuoteAgreements() {
    	Agreement__c ag = insertTestAgreement();
    	
    	Boolean result = QuoteAgreementUtilities.expireOldQuoteAgreements(ag.quote__c, null);
    	
    	system.assert(result);
    	
    	QuoteAgreementUtilities.expireOldQuoteAgreementsLater(ag.quote__c, null);
    }
    
    private static testMethod void testExpireOldQuoteAgreementsNull() {
    	Boolean caughtException = false;
    	
    	try {
    		QuoteAgreementUtilities.expireOldQuoteAgreements(null, null);
    	} catch (Exception e) {
    		caughtException = true;
    		system.assertEquals('Missing quote id.', e.getMessage());
    	}

    	system.assert(caughtException);
    	
    	QuoteAgreementUtilities.expireOldQuoteAgreementsLater(null, null);
    }
    
    private static testMethod void testExpireOldQuoteAgreementsNoAgreements() {
    	Opportunity o = new Opportunity(name = 'test opp', closedate = Date.today(), stagename = 'test');
    	insert o;
    	
    	SBQQ__Quote__c q = new SBQQ__Quote__c(SBQQ__Opportunity__c = o.id);
    	insert q;
    	
    	Boolean result = QuoteAgreementUtilities.expireOldQuoteAgreements(q.id, null);
    	
    	system.assert(result);
    }
    
    private static testMethod void testExpireOldQuoteGroupAgreements() {
    	Agreement__c ag = insertTestAgreement();
    	
    	Boolean result = QuoteAgreementUtilities.expireOldQuoteGroupAgreements(ag.quote_group__c, null);
    	
    	system.assert(result);
    	
    	result = QuoteAgreementUtilities.expireOldQuoteGroupAgreements(ag.quote_group__c, null, null);
    	
    	system.assert(result);
    	
    	QuoteAgreementUtilities.expireOldQuoteGroupAgreementsLater(ag.quote_group__c, null);
    }
    
    private static testMethod void testExpireOldQuoteGroupAgreementsNull() {
    	Boolean caughtException = false;
    	
    	try {
    		QuoteAgreementUtilities.expireOldQuoteGroupAgreements(null, null, null);
    	} catch (Exception e) {
    		caughtException = true;
    		system.assertEquals('Missing quote group id.', e.getMessage());
    	}

    	system.assert(caughtException);
    	
    	QuoteAgreementUtilities.expireOldQuoteGroupAgreementsLater(null, null);
    }
    
    private static testMethod void testExpireOldQuoteGroupAgreementsNoAgreements() {
    	Opportunity o = new Opportunity(name = 'test opp', closedate = Date.today(), stagename = 'test');
    	insert o;
    	
    	Quote_Group__c qg = new Quote_Group__c();
    	insert qg;
    	
    	SBQQ__Quote__c q = new SBQQ__Quote__c(SBQQ__Opportunity__c = o.id, group__c = qg.id);
    	insert q;
    	
    	Boolean result = QuoteAgreementUtilities.expireOldQuoteGroupAgreements(qg.id, null);
    	
    	system.assert(result);
    }
    
    private static testMethod void testExpireAgreements() {
    	Agreement__c ag = insertTestAgreement();
    	
    	QuoteAgreementUtilities.expireAgreements(new Id[]{ag.quote_group__c}, null, null);
    	
    	QuoteAgreementUtilities.expireAgreementsLater(new Id[]{ag.quote_group__c});
    }
    
    private static testMethod void testExpireAgreementsNull() {   	
    	Boolean result = QuoteAgreementUtilities.expireAgreements(null, null, null);
    	
    	system.assert(result);
    	
    	QuoteAgreementUtilities.expireAgreementsLater(null);
    }
    
    
    @isTest
    private static Agreement__c insertTestAgreement() {
    	final string WAYBILL_ID = 'testwbid';
    	
    	Account a = new Account(Name = 'Test', Phone = '6049969449');
      insert a;
      
      Contact c = new Contact(FirstName = 'Alex', LastName = 'Miller', Phone = '6049969449', AccountId = a.id, Account = a);
      insert c;
      
      Opportunity o = new Opportunity(
    		name = 'test opp',
    		AccountId = a.id,
    		closedate = Date.today(),
    		stagename = 'test'
    	);
    	insert o;
    	
    	Quote_Group__c qg = new Quote_Group__c();
    	insert qg;
    	
    	SBQQ__Quote__c q = new SBQQ__Quote__c(SBQQ__Opportunity__c = o.id, SBQQ__Account__c = a.id, SBQQ__Account__r = a,
    	group__c = qg.id, group__r = qg);
    	insert q;
      
      Agreement__c ag = new Agreement__c(
      	Recipient__c = c.Id, Recipient__r = c, Type__c = 'NotRecombo', Waybill_Id__c = WAYBILL_ID, Status__c = 'Pending',
      	Sent_Date__c = Datetime.now(), Expiry_Date__c = Date.today().addDays(2), Quote__c = q.id, Quote__r = q,
      	Quote_Group__c = qg.id
      );
      insert ag;
    	
    	return ag;
    }
}