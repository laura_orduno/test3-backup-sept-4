/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class smb_test_AmendmentsCMSV3_WSDL {

static testMethod void smb_Amendments() {
     
    smb_Amendments amnd = new smb_Amendments();
    cmsv3_ContractManagementSvcTypes_v3.ContractAmendment amendment = new cmsv3_ContractManagementSvcTypes_v3.ContractAmendment();       
cmsv3_ContractManagementSvcTypes_v3 woncms = new cmsv3_ContractManagementSvcTypes_v3();

//amend.signator.title = 'Solution';
cmsv3_ContractManagementSvcTypes_v3.CustomerSignor customerSignor = new cmsv3_ContractManagementSvcTypes_v3.CustomerSignor();
customerSignor.firstName = 'Anthony';
customerSignor.lastName = 'Michael';
customerSignor.title ='Solution';
   
//amendment.customerSignor.firstName = 'Anthony';
//amendment.customerSignor.lastName = 'Michael';
//amendment.customerSignor.title ='Solution';
//amends.signator = 'SolutionAnthonyMichael';

if(null != customerSignor)
{
amnd.signator =  (customerSignor.title == 'Solution' ? customerSignor.title+'Solution':' ')                        
                        + (customerSignor.firstName == 'Anthony' ? customerSignor.firstName+'Anthony':' ')
                        +  (customerSignor.lastName == 'Michael' ? customerSignor.lastName:'Michael');
}

 String a = amnd.contractId;
String b = amnd.contractNum;
String c = amnd.contractAssociateNum;
String d = amnd.contractAmendmentNum;        
String l = amnd.signedDate;
String e = amnd.registeredDate;
String f = amnd.effectiveDate;
String g = amnd.expiryDate;
String h = amnd.contractType;
String i = amnd.contractStatus;
String j = amnd.contractTerm;
String k = amnd.signator;
Date chk = Date.today();
Date s = amnd.parseDate('2013-08-01 12:00:00');
System.debug(s);
if(s==null) {
System.debug (null);
}
/*
Date sal = amnd.parseDate('0000-00-00 00:00:00');
System.debug(sal);
if(sal==null) {
System.debug (null);
}*/
 String t = amnd.parseDateAndFormat('2013-08-01 12:00:00');
 System.debug(t);
 if(t==null) {
System.debug (null);
}
/*
String tal = amnd.parseDateAndFormat('0000-00-00 00:00:00');
 System.debug(tal);
 if(tal==null) {
System.debug (null);
}*/
 String z = amnd.parseDateAndFormat('2013-08-01 12:00:00');
 System.debug(z);
 if(z==null) {
System.debug (null);
}
/*
String zal = amnd.parseDateAndFormat('0000-00-00 00:00:00');
 System.debug(zal);
 if(zal==null) {
System.debug (null);
}*/

Decimal u = amnd.parseDecimal('1.2');
System.debug(u);
if(u==null) {
System.debug (null);
}
/*
Decimal ual = amnd.parseDecimal('0');
System.debug(ual);
if(ual==null) {
System.debug (null);
}*/

String v = amnd.zeroFill('11');
if(v==null) {
System.debug (null);
}

 if(v.length()==1) {
           System.debug('00'+v);
           }
        else if (v.length()==2){
        System.debug('0'+v);
        }
        else{
        System.debug(v);
        }
        
        String uv = amnd.zeroFill('4545');
if(uv==null) {
System.debug (null);
}

 if(uv.length()==1) {
           System.debug('00'+uv);
           }
        else if (uv.length()==2){
        System.debug('0'+uv);
        }
        else{
        System.debug(uv);
        }
        
        String xv = amnd.zeroFill('2');
if(xv==null) {
System.debug (null);
}

 if(xv.length()==1) {
           System.debug('00'+xv);
           }
        else if (xv.length()==2){
        System.debug('0'+xv);
        }
        else{
        System.debug(xv);
        }
        
        String av = amnd.zeroFill('null');
if(av==null) {
System.debug (null);
}

 if(av.length()==1) {
           System.debug('00'+av);
           }
        else if (av.length()==2){
        System.debug('0'+av);
        }
        else{
        System.debug(av);
        }

        if (null != amendment.customerSignor)
        {
            system.debug('------Amendment.customerSignor.firstName-----'+amendment.customerSignor.firstName);
            system.debug('------Amendment.customerSignor.firstName-----'+amendment.customerSignor.firstName != null ? amendment.customerSignor.firstName:'Anthony');
      //  amends.signator =  (Amendment.customerSignor.title != null ? Amendment.customerSignor.title+'Solution':' ')

                   
        }
        if (amendment.customerSignor != null)
        {
            system.debug('------Amendment.customerSignor.firstName-----'+amendment.customerSignor.firstName);
            system.debug('------Amendment.customerSignor.firstName-----'+amendment.customerSignor.firstName != null ? amendment.customerSignor.firstName:'Anthony');

      //  amends.signator =  (Amendment.customerSignor.title == 'Solution' ? Amendment.customerSignor.title+'Solution':' ')


        }

//Updated by Arvind
 //cmsv3_ContractManagementSvcTypes_v3.ContractAmendment amend = new cmsv3_ContractManagementSvcTypes_v3.ContractAmendment();
  cms3_1ContractManagementSvcTypes_v3.ContractAmendment amend = new cms3_1ContractManagementSvcTypes_v3.ContractAmendment();
 amend.contractId = '6756756';
amend.contractTermUnit = 'Sunday';
amend.contractTerm ='D';
String qun = amend.contractId;
String kin = amend.contractTerm;
System.assertEquals('Sunday',amend.contractTermUnit);
System.assertEquals('D',amend.contractTerm);
System.assertEquals('6756756',amend.contractId);
//cmsv3_ContractManagementSvcTypes_v3.CustomerSignor cussig = new cmsv3_ContractManagementSvcTypes_v3.CustomerSignor();
//Updated by Arvind
cms3_1ContractManagementSvcTypes_v3.CustomerSignor cussig = new cms3_1ContractManagementSvcTypes_v3.CustomerSignor();

smb_Amendments amends = new smb_Amendments(amend);

amends.contractId = '123456';
amends.contractAmendmentNum = '123';
amends.signedDate = '2013-08-01 12:00:00';
amends.registeredDate = '2013-08-01 12:00:00';
amends.effectiveDate = '2013-08-01 12:00:00';
amends.expiryDate = '2013-08-01 12:00:00';
amends.contractType = 'S';
amends.contractStatus = 'Fermé';
amends.contractTerm = (amend.contractTermUnit == 'Sunday'
                            ?kin + '(D)'
                            :amend.contractTerm);
 if(amend.contractTermUnit == 'Sunday'){
 amends.contractTerm = amend.contractTerm ;
 }                           
        
amends.signator = 'Anthony Michael';



System.assertEquals('123456',amends.contractId);
System.assertEquals('123',amends.contractAmendmentNum);
System.assertEquals('2013-08-01 12:00:00',amends.signedDate);
System.assertEquals('2013-08-01 12:00:00',amends.registeredDate);
System.assertEquals('2013-08-01 12:00:00',amends.effectiveDate);
System.assertEquals('2013-08-01 12:00:00',amends.expiryDate);
System.assertEquals('S',amends.contractType);
System.assertEquals('Fermé',amends.contractStatus);
System.assertEquals('D',amends.contractTerm);
System.assertEquals('Anthony Michael',amends.signator);

//if (amend.customerSignor.firstName != null && amend.customerSignor.lastName != null )
 //       {
 //amend.customerSignor.title ='Solution';
//amend.customerSignor.firstName = 'Anthony';
//amend.customerSignor.lastName = 'Michael';
           // system.debug('------amend.customerSignor.firstName-----'+amend.customerSignor.firstName);
           // system.debug('------amend.customerSignor.firstName-----'+amend.customerSignor.firstName != null ? amend.customerSignor.firstName:'');
      //  amends.signator = (amend.customerSignor.firstName != null ? amend.customerSignor.firstName+' ':' ')
      //                  +  (amend.customerSignor.lastName != null ? amend.customerSignor.lastName:'');
                        
                       //  amends.signator = amend.customerSignor.title + amend.customerSignor.firstName + amend.customerSignor.lastName;
                       
     //   }

// List<smb_ContractManagementSvcTypes_v3.ContractAmendment> contractAmendmentList = new List<smb_ContractManagementSvcTypes_v3.ContractAmendment>();
// List<smb_Amendments.contractAmendmentNum> empList = new List<smb_Amendments.contractAmendmentNum>();
// empLists.add('1');
 // empLists.add('2');
 //  empLists.add('3');
//empList.add(new amends.contractAmendmentNum('1'));
//empList.sort();

//System.debug(empList);


}


}