@istest(SeeAllData=false)
public class OCOM_NewOrderTeamMemberControllerTest
{
    @testSetup static void setupData()
    {
        Account acct = new Account(name='testAccout');
        insert acct;
        Order ord = new Order(name='testOrder',AccountId=acct.id,status='Draft',EffectiveDate=Date.today());
        insert ord;        
    }
    static testMethod void testNewTeamMember()
    {
        Order ordRec = [select id from Order limit 1];
        PageReference pageRef = Page.OCOM_NewOrderTeamMember;
        pageRef.getParameters().put('oid',ordRec.Id);
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller stdcon = new ApexPages.Standardcontroller(new Cloud_Enablement_Team_Member__c());
        OCOM_NewOrderTeamMemberController ctlrObj = new OCOM_NewOrderTeamMemberController(stdcon);
        ctlrObj.saveWorkReq();
        ctlrObj.getTmRec();
        ctlrObj.getOrderId();
        ctlrObj.cancelWorkReq();
    }
    static testMethod void testNewTeamMemberNullOrder()
    {
        PageReference pageRef = Page.OCOM_NewOrderTeamMember;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller stdcon = new ApexPages.Standardcontroller(new Cloud_Enablement_Team_Member__c());
        OCOM_NewOrderTeamMemberController ctlrObj = new OCOM_NewOrderTeamMemberController(stdcon);
        ctlrObj.saveWorkReq();
    }
}