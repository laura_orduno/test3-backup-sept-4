@isTest
public class AgreementCommentCtrlExtTest {
    
    @isTest
    public static void testNewCommentforAgreement(){
        TestDataHelper.testContractCreation();
        Subsidiary_Account__c subAcc = new  Subsidiary_Account__c(Contract__c=TestDataHelper.testContractObj.id);
        insert subAcc;  
        ApexPages.currentPage().getParameters().put('retURL','/'+TestDataHelper.testContractObj.id);  
        Agreement_Dealer__c dealer = new Agreement_Dealer__c(Exclusive_Dealer__c=true,Contract__c=TestDataHelper.testContractObj.id);
        ApexPages.StandardController sc = new ApexPages.standardController(dealer);
        AgreementCommentCtrlExt ctrl = new AgreementCommentCtrlExt(sc);
        ctrl.cancel(); 
        ctrl.save();
        ctrl.edit();
        ctrl.showPropergateList();  
        ctrl.propagate();
        ctrl.displayItems.get(0).selected = true;  
        ctrl.propagate();  
    }
    
    @isTest
    public static void testNewCommentforSubsidiary(){
        Subsidiary_Account__c subAcc = new  Subsidiary_Account__c();
        insert subAcc;  
        ApexPages.currentPage().getParameters().put('retURL','/'+subAcc.id);  
        Agreement_Dealer__c dealer= new Agreement_Dealer__c(Exclusive_Dealer__c=true,Subsidiary_Account__c=subAcc.id);    
        ApexPages.StandardController sc = new ApexPages.standardController(dealer);
        AgreementCommentCtrlExt ctrl = new AgreementCommentCtrlExt(sc);
        ctrl.save();
    }
    
    @isTest
    public static void testNewCommentforDealSupport(){
        TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();
        TestDataHelper.testContractCreation();
        Opportunity opp = new Opportunity();
        opp.Name = 'TEST OPP';
        opp.AccountId = TestDataHelper.testAccountObj.Id;
        opp.StageName = 'NewStage';
        Opp.CloseDate = Date.newInstance(2015,12,25);
        insert opp;
        map<string,schema.recordtypeinfo> recordTypeMap=offer_house_demand__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        Offer_House_Demand__c dealSupport = new Offer_House_Demand__c();
        dealSupport.recordtypeid=recordTypeMap.get('WLN Deal Support').getrecordtypeid();
        dealSupport.type_of_contract__c='CCA';
        dealSupport.OPPORTUNITY__c = opp.Id;
        dealSupport.TERM_OF_CONTRACT__c = 24;
        dealSupport.Type__c ='Current';
        dealSupport.Device_Term__c = 12;
        dealSupport.Status__c= 'Contract In Progress';
        dealSupport.signature_required__c='Yes';
        insert dealSupport;
        ApexPages.currentPage().getParameters().put('retURL','/'+dealSupport.id);  
        Agreement_Dealer__c dealer= new Agreement_Dealer__c(Exclusive_Dealer__c=true,Deal_Support__c=dealSupport.id);    
        ApexPages.StandardController sc = new ApexPages.standardController(dealer);
        AgreementCommentCtrlExt ctrl = new AgreementCommentCtrlExt(sc);
        ctrl.save();
    }
    
    
    @isTest
    public static void testErrors1(){
        
        TestDataHelper.testContractCreation();
        Subsidiary_Account__c subAcc = new  Subsidiary_Account__c(Contract__c=TestDataHelper.testContractObj.id);
        insert subAcc;  
        ApexPages.currentPage().getParameters().put('retURL', '/');  
        Agreement_Dealer__c dealer = new Agreement_Dealer__c(Contract__c=TestDataHelper.testContractObj.id);  
        ApexPages.StandardController sc = new ApexPages.standardController(dealer);
        AgreementCommentCtrlExt ctrl = new AgreementCommentCtrlExt(sc);
        
        ctrl.showPropergateList();
        ctrl.displayItems.get(0).selected = true;   
        ctrl.displayItems.add(ctrl.displayItems.get(0).clone());
        ctrl.propagate();  
    }
    
    @isTest
    public static void testErrors2() {
        Agreement_Dealer__c dealer = new Agreement_Dealer__c();   
        ApexPages.StandardController sc = new ApexPages.standardController(dealer);
        AgreementCommentCtrlExt ctrl = new AgreementCommentCtrlExt(sc);
    }
    
    
}