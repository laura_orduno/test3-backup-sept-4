global class VITILcare_TTODS_CalloutHelper {
    
    /* This method gets a Account TID and RCID values as input and return related Trouble Tickets*/
    public list<smb_TTODS_troubleticket.TroubleTicket> getTicketsByQueryCall(string TIDValue, string RCIDValue){
        return getTicketsByQueryCall('', TIDValue, RCIDValue);
    }
    
    public list<smb_TTODS_troubleticket.TroubleTicket> getTicketsByQueryCall(string ticketNum, string TIDValue, string RCIDValue){
        
        if(!Test.isRunningTest())
            if(string.isBlank(TIDValue) && string.isblank(RCIDValue))return null;
        
        //list of Trouble tickets data to be return
        list<smb_TTODS_troubleticket.TroubleTicket> lstTrobleTicket = new list<smb_TTODS_troubleticket.TroubleTicket>();
        
        //Web service primary class object
        smb_TTODS_queryticketservice.QueryTicketPort servicePort = new smb_TTODS_queryticketservice.QueryTicketPort();
        servicePort.endpoint_x = SMBCare_WebServices__c.getInstance('TTODS_EndPoint').Value__c;
     	//servicePort.endpoint_x = 'https://xmlgwy-pt1.telus.com:9030/pt01/SMO/ProblemMgmt/TroubleTicketODSQueryService_v1_5_vs0';
        servicePort = prepareCallout(servicePort);
        
        //servicePort.getTickets(requestSystemName, requestUserId, telusTicketId, telusTicketIds, externalTicketId, subscriptionId, customerId, includeOpenTickets, includeClosedTickets, upToXLatestModified, includeActivities);
        
        //requestSystemName is static as 'SFDC'
        string requestSystemName = 'SFDC';
        
        //requestUserId is TIDValue in method parameters
        string requestUserId = TIDValue;
        
        //telusTicketId
        smb_TTODS_troubleticket.ValueAndOperator ttValueAndOperator = new smb_TTODS_troubleticket.ValueAndOperator();
       // ttValueAndOperator.value = ticketNum;
        //ttValueAndOperator.operator = '=';
        //telusTicketIds
        smb_TTODS_troubleticket.ListAndOperator ttListAndOperator = new smb_TTODS_troubleticket.ListAndOperator();
        
        //externalTicketId
        smb_TTODS_troubleticket.ExternalTicketIdFilter ttExternalTicketIdFilter = new smb_TTODS_troubleticket.ExternalTicketIdFilter();
        
        //subscriptionId
        smb_TTODS_troubleticket.SubscriptionIdFilter ttSubscriptionIdFilter = new smb_TTODS_troubleticket.SubscriptionIdFilter();
        //Contact
        smb_TTODS_troubleticket.Contact ttContact = new smb_TTODS_troubleticket.Contact();
        //customerId
        smb_TTODS_troubleticket.CustomerIdFilter ttCustomerIdFilter = new smb_TTODS_troubleticket.CustomerIdFilter();
        ttCustomerIdFilter.value = RCIDValue;
        ttCustomerIdFilter.operator = '=';
        ttCustomerIdFilter.customerIdType = 'CSHM';
        
        //includeOpenTickets and includeClosedTickets
        smb_TTODS_troubleticket.TicketFilter ttTicketFilter = new smb_TTODS_troubleticket.TicketFilter();
        ttTicketFilter.toInclude = true;
        
        //upToXLatestModified
        integer upToXLatestModified = 200; 
        
        //includeActivities
        list<string> lstActivityTypeValues = new list<string>();
        list<string> lstActivityStateValues = new list<string>();
        
        smb_TTODS_troubleticket.ActivityFilter ttActivityFilter = new smb_TTODS_troubleticket.ActivityFilter();
        ttActivityFilter.toInclude = true;
        
        smb_TTODS_troubleticket.ActivityTypeListAndOperator ttActivityTypeListAndOperator = new smb_TTODS_troubleticket.ActivityTypeListAndOperator();
        lstActivityTypeValues.add('Resolution');
         lstActivityTypeValues.add('Notes');
        lstActivityTypeValues.add('Call - Inbound');
        lstActivityTypeValues.add('Notify Customer');
         lstActivityTypeValues.add('Route');
        lstActivityTypeValues.add('Test Remarks');
        ttActivityTypeListAndOperator.value = lstActivityTypeValues;
        ttActivityTypeListAndOperator.operator = 'IN';
        ttActivityFilter.activityTypes = ttActivityTypeListAndOperator;
        
            
        
/*     smb_TTODS_troubleticket.ActivityStateListAndOperator ttActivityStateListAndOperator = new smb_TTODS_troubleticket.ActivityStateListAndOperator();
        lstActivityStateValues.add('Done');
        ttActivityStateListAndOperator.value = lstActivityStateValues;
        ttActivityStateListAndOperator.operator = 'IN';
        ttActivityFilter.activityStates = ttActivityStateListAndOperator;
        
  */
        
        ttActivityFilter.upToXLatestModified = 200;     
        
        //Callout
        if(Test.isRunningTest())
        {
            smb_TTODS_troubleticket.TroubleTicket tt = new smb_TTODS_troubleticket.TroubleTicket();
            tt.TELUSTicketId = 'T1234567';
            tt.sourceTypeCode = 'CUSTOMER';
            tt.ticketTypeCode ='Ticket';
            tt.severityCode = '5 - MEDIUM';
            tt.priorityCode = 'In Service';
            tt.statusCode ='Open';
            tt.subStatusCode = 'Assigned';
            tt.customerDueDate = datetime.now().addDays(1);
            tt.createDate = datetime.now().addDays(-1);
            tt.closeDate = datetime.now().addDays(2);
            tt.createdByName = 'T010027';
            
            smb_TTODS_troubleticket.Contact con = new smb_TTODS_troubleticket.Contact();
            con.LastName = 'Test';
            con.FullName='Test';
            
            smb_TTODS_troubleticket.TelecommunicationsAddress telAddress = new smb_TTODS_troubleticket.TelecommunicationsAddress();
            telAddress.telephoneNumber = '1234567891';
            telAddress.telecommunicationsAddressUsageType ='CBR';
            
            con.telecommunicationsaddress = new list<smb_TTODS_troubleticket.TelecommunicationsAddress>();               
            con.telecommunicationsaddress.add(telAddress);
            
            tt.contact = new list<smb_TTODS_troubleticket.Contact>();
            tt.contact.add(con);
            
            lstTrobleTicket.add(tt);
        }
        else
        {
            lstTrobleTicket = servicePort.getTickets(requestSystemName, requestUserId,null,null,null,null, ttCustomerIdFilter, ttTicketFilter, ttTicketFilter, upToXLatestModified, ttActivityFilter);
        }
        return lstTrobleTicket;
    }
    
    /*Method to set Credentials in header of the request*/
    public smb_TTODS_queryticketservice.QueryTicketPort prepareCallout(smb_TTODS_queryticketservice.QueryTicketPort serport)
    {
        String creds;
        SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username');
        SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');
        if (String.isNotBlank(WSusername.Value__c)&& String.isNotBlank(wsPassword.Value__c))
            creds = WSusername.Value__c+':'+wsPassword.Value__c;
        String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(creds));
        
        serPort.inputHttpHeaders_x = new Map<String, String>();
        serPort.inputHttpHeaders_x.put('Authorization','Basic ' + encodedusernameandpassword );
        // Adding EM Header - START
                    
        smb_cust_telus_em_header_v1_new.emRoutingRulesType er = new smb_cust_telus_em_header_v1_new.emRoutingRulesType();
        er.transport = 'http';
        er.host = 'troubleticketsvc-st.tsl.telus.com';
        er.port = '80';
        er.uri='/troubleticketODS/assurance/webservice/QueryTicketPort';
        //er.envString = 'AT01'; To be left blank
        smb_cust_telus_em_header_v1_new.emHeaderType eh = new smb_cust_telus_em_header_v1_new.emHeaderType();
        eh.routingRules = er;
        serPort.emHeader = eh;
        // Adding EM Header - END

        
        
        return serPort; 
    }

}