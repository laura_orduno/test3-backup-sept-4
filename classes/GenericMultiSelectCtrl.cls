/**
 * Generic Controller Class for the To Provide Multiselect funtinality for When Creating a Junction Object Record.
 * Related Visual Page Name : GenericMultiSelectPage
 * 
 * This Implementaion will provide below functionality
 * 1)  Selectabel Diplay Records on the target Object with 
 * 
 * Usage:
 * Creare a Button ( Details and or List Item) or Using Following example Parameters
 * /apex/GenericMultiSelectPage?
 * sourceId={!Offer_House_Demand__c.Id}
 * &sfld=Deal_Support__c
 * &tfld=Account__c
 * &trgobj=Account
 * &dfsn=show_subs
 * &ffsn=filter
 * &junobj=Subsidiary_Account__c
 * &shrt=BAN,CAN,RCID
 * &datafilter=(parentid%3D%27{!Offer_House_Demand__c.Account_Id__c}%27%20or%20parentid%3D%27{!Offer_House_Demand__c.Account_CBUCID_Id__c}%27)
 * 
 * Required URL Parameters 
 * =======================
 * sourceId : Id of the source object which the related list is on
 * sfld : Source Field API name on the Junction Object
 * tfld : Traget Field API name on the Junction Object
 * trgobj : Target object which list of recodsto be shown to be selected
 * dfsn : Display Column Field Set Name for the Table Columns
 * junobj: Name of the Juntion/Link object which creates the M-M or 1-M relastionship
 * 
 * Optional  URL Parameter
 * =======================
 * ffsn : Filter Field Set for the filer field which display on top. If not defined this defaults to value passeed tp 'dfsn'
 * shrt : show Record Type Names, when profieded only show the profided value as a filter option for the Recod Type, 
 * 		  provide 'none' to hide the recodtype filter option. //TODO implement the hide when none is profided
 * datafilter : initial filter criteria for the records.Make sure to URLEncode the String
 * 
 * Advanced Parameter
 * ==================
 * cpfs : Copy fields Name, Field which values should be copied from Trget object to Junction Object. 
 * mrt : Valid values (true, false) Match the traget Object Record Recod Type to new Junction Object Record Type. Defualt value is false.  
 */ 



public class GenericMultiSelectCtrl {
	
    public static final Integer pageSize = 15;
    public List<ItemSelect> displayItems =null;
    private List<SelectOption> recordTypeOptions;
    
    public String selectedRecType {get;set;}
    public Map<id,sObject> selectedItemMap {get; set;}
    public List<sObject> selectedItemList;
    public List<sObject> savedItemList {get;set;}
    
    // Parameters Id
    public String sourceId {get;set;} //Id of the source object whcih master side Example Deal has many Rate Plans , rate Plan has many addons
    
    // Parameters Object Names
    public String targetObj {get;set;}
    public String juntionObj {get;set;}
    
    // Parameters FiledNames
    public String junSourceObjField {get;set;}
    public String juntargetObjField {get;set;}
    // Parameters Filter
    public String dataFilter {get;set;}
    private List<string> filter = new List<String>();
    
    public List<Schema.FieldSetMember> displayFieldset {get;set;} 
    public List<Schema.FieldSetMember> FilterFieldset {get;set;}
    
    public String displayFieldSetName {get;set;}
    public String filterFieldsetName {get;set;}
    public transient String showRecordTypes {get;set;}
    public String copyFiledSetName {get;set;}
    public String relatedFieldName {get;set;}
    
    public GenericPagination paginator {get;set;}
    
    public sObject targetObjInstance {get;set;} 
   
    private boolean matchRecType = false;
    
    public GenericMultiSelectCtrl(){
        try{
            init();
            initPagination();
        }catch(GenericPagination.PaginationException e){
            String errmsg= 'There might be an issue with SOQL. Please validate the datafilter parameter on the URL';
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL,errmsg);
            ApexPages.addMessage(msg);
        }catch(RequiredParameterException e){
            String errmsg= 'Required parameters sourceId, sfld, tfld, tarobj, junobj, dfsn can not be empty';
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL,errmsg);
            ApexPages.addMessage(msg);
        }    
    }
    
    private void init(){
        selectedItemList = new List<sObject>();
        selectedItemMap = new Map<id,sObject>();
        displayFieldset = new List<Schema.FieldSetMember>();
        FilterFieldset =  new List<Schema.FieldSetMember>();
        recordTypeOptions = new List<SelectOption>();
        savedItemList = new List<sObject>();
        
        //Required Parameters
        sourceId = ApexPages.currentPage().getParameters().get('sourceId');
        junSourceObjField = ApexPages.currentPage().getParameters().get('sfld');
        juntargetObjField = ApexPages.currentPage().getParameters().get('tfld');
        targetObj = ApexPages.currentPage().getParameters().get('trgobj');
        juntionObj = ApexPages.currentPage().getParameters().get('junobj');
        displayFieldSetName =  ApexPages.currentPage().getParameters().get('dfsn');
        if(sourceId==null || junSourceObjField==null || juntargetObjField==null || targetObj ==null || displayFieldSetName==null){
            throw new RequiredParameterException('Required parameters can not be empty');
        }
        
        //Optional parameters
        filterFieldsetName =  ApexPages.currentPage().getParameters().get('ffsn');
        showRecordTypes =  ApexPages.currentPage().getParameters().get('shrt');
        dataFilter = ApexPages.currentPage().getParameters().get('datafilter');
        copyFiledSetName = ApexPages.currentPage().getParameters().get('cpfs');
        String mRecType = ApexPages.currentPage().getParameters().get('mrt');
        
        matchRecType = Boolean.valueOf(mRecType == null?false:true);
        filterFieldsetName = filterFieldsetName == null?displayFieldSetName:filterFieldsetName;
        targetObjInstance  = (Sobject)Type.forName(targetObj).newInstance();
        initRecrodTypeOptions();
        displayFieldset = Util.getFieldSetFieldsObjs(targetObj, displayFieldSetName);
        FilterFieldset = Util.getFieldSetFieldsObjs(targetObj, filterFieldsetName);
    }
    
    private void initPagination(){
        System.debug(LoggingLevel.ERROR,'initPagination ');
        Set<String> queryFields = new Set<String>();
        queryFields.addAll(Util.getFieldSetFields(targetObj, displayFieldSetName));
        queryFields.addAll(new LIst<string>{'Name', 'Id'});
        
        if(dataFilter!=null){
           filter.add(dataFilter); 
        }
        String ids = getSaveIdList();
        if(ids!=null && ids.length()>0){
        	filter.add('id not in ('+ids+')');    
        }
        String query = Util.queryBuilder(targetObj,new List<String>(queryFields) , filter);
        System.debug('query ' +query);
        paginator = new GenericPagination(query,pageSize);
        
    }
    
    private String getSaveIdList(){
        List<String> idList =new List<String>();
        relatedFieldName = juntargetObjField.replace('__c', '__r.Name');
        String query = Util.queryBuilder(juntionObj,new List<String>{juntargetObjField,relatedFieldName}, new List<string>{junSourceObjField+'=\''+sourceId+'\''});
        savedItemList = Database.query(query);
        for(Sobject obj : savedItemList){
            if(obj.get(juntargetObjField)!=null){
                idList.add('\''+obj.get(juntargetObjField)+'\'');
            }
        }
        return String.join(idList,',');
    }
    private void initRecrodTypeOptions(){
        
        if(targetObj!=null){
            List<SelectOption> rtList = Util.getRecordTypesAsSelectOptions(targetObj);
            if(showRecordTypes!=null){
                Set<String> allowdRecTypes = new Set<String>(showRecordTypes.split(','));
                for(integer i=0;i<rtList.size();i++){
                    if(allowdRecTypes.contains(rtList[i].getlabel())){
                      recordTypeOptions.add(rtList[i]);
                    }
                }
            	targetObjInstance.put('RecordTypeId',Util.getRecordTypeIdByName(targetObjInstance, showRecordTypes.split(',')[0]));     
            }else{
                recordTypeOptions.addALL(rtList);
           }
            if(recordTypeOptions.size()>1){
                 recordTypeOptions.add(0,new SelectOption('ALL','ALL'));
            }
       }     
    }
    
    public List<ItemSelect> getdisplayItems(){
        displayItems = new List<ItemSelect>();
        if(paginator!=null){
            for(Sobject item : (List<Sobject>)paginator.getRecords()) {
                if(selectedItemMap.containsKey(item.id)){
                	displayItems.add(new ItemSelect(item,true));    
                }else{
                    displayItems.add(new ItemSelect(item,false));
                }
                
            }
         }   
        return displayItems;
    }
    
    public List<sObject> getSelectedItemList(){
        selectedItemList.clear();
        selectedItemList.addAll(selectedItemMap.values());
        return selectedItemList;
    }
    
    public List<SelectOption> getRecordTypeList(){
        return recordTypeOptions;
    } 
    
    private void processSelectedItems() {
        for (ItemSelect item : this.displayItems) {
            if (item.selected == true) {
                selectedItemMap.put(item.obj.id,item.obj);
            }else{
                selectedItemMap.remove(item.obj.id);
            }
        }
    }
  
    public PageReference addSelected() {
        processSelectedItems();
        return null;
    }
    
    public PageReference saveSelected() {
        linkandSave();
        PageReference pr =  new PageReference('/'+sourceId);
        pr.setRedirect(true);
        return pr;
    }
    
    public PageReference cancel() {
        return new PageReference('/'+sourceId);
    }
    
    public PageReference clearSelected() {
        for (ItemSelect item : this.displayItems) {
            item.selected = false;
        }
        this.selectedItemList.clear();
        this.selectedItemMap.clear();
        return null;
    }
    
    public void doFilter(){
        List<String> queryFields = new List<String>();
        List<String>  localFilter = new List<String>();
        localFilter.addAll(filter);
        queryFields.addAll(Util.getFieldSetFields(targetObj, displayFieldSetName));
        queryFields.addAll(new LIst<string>{'Name', 'Id'});
        for(Schema.FieldSetMember f :FilterFieldset){
            Object filterPlanType =(Object) targetObjInstance.get(f.getFieldPath());
            if(filterPlanType != null && 'PICKLIST'.equals(String.valueOf(f.getType()))){
               	localFilter.add(f.getFieldPath() +' =\'' + filterPlanType+ '\'');
            }else if(filterPlanType != null && 'STRING'.equals(String.valueOf(f.getType()))){
                localFilter.add(f.getFieldPath() +' Like \'%' + filterPlanType+ '%\'');
            }else if(filterPlanType!=null ){
                localFilter.add(f.getFieldPath() +' =' + filterPlanType);
            }    
        }
        if(selectedRecType !=null && !'ALL'.equals(selectedRecType)){
            localFilter.add('RecordTypeId = \''+ selectedRecType+'\'');
            targetObjInstance.put('RecordTypeId',selectedRecType); 
        }
        //add error handling
        String query = Util.queryBuilder(targetObj,queryFields ,localFilter);
        System.debug('Filter query:' + query); 
        paginator = new GenericPagination(query,pageSize);
            
    }
    
    private void linkandSave(){
        List<Sobject> itemstOInsert =new LIst<Sobject>();
        Map<id, Sobject> toInsertMap = new Map<id, Sobject>();
        Type t = Type.forName(juntionObj);
        for(Id targetId :selectedItemMap.keySet()){
            Sobject  obj = (Sobject)t.newInstance();
        	obj.put(junSourceObjField,sourceId);
            obj.put(junTargetObjField,targetId);
            itemstOInsert.add(obj);
            toInsertMap.put(targetId,obj);
        }
        if(copyFiledSetName!=null){
            List<String> fieldset  = Util.getFieldSetFields(juntionObj, copyFiledSetName);
            Set<id> selItemIdSet = selectedItemMap.keySet();
            List<String> filter = new List<String>{'id in :selItemIdSet'};
            if(matchRecType){
                fieldset.add('RecordTypeId');  
            }   
            String query = Util.queryBuilder(targetObj,fieldset ,filter);
            List<Sobject> recs =  Database.query(query);
            for(Sobject rps : recs){
                Sobject  obj =  toInsertMap.get(rps.id);
                for(String field :  fieldset){
                    obj.put(field, rps.get(field));
                }
                if(matchRecType){
                    String targetRecTypeName = Util.getRecordTypeNameById(rps,(String)rps.get('RecordTypeId'));
                    obj.put('RecordTypeId', Util.getRecordTypeIdByName(juntionObj,targetRecTypeName )); 
                }
            }
       }     
       insert toInsertMap.values();
    }
    
    public class ItemSelect{
        public sObject obj {get; set;}
        public boolean selected {get; set;}
       
        public ItemSelect(sObject obj,boolean sel) {
            this.obj = obj;
            selected = sel;
        }
    }
    
   public class RequiredParameterException extends Exception {
      
   }
   
}