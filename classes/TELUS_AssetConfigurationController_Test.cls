@isTest
public with sharing class TELUS_AssetConfigurationController_Test {
    public static List<Product2> productList;
    public static Account acc;
    public static List<RecordType> accntRecordTypeList;
    public static String recordTypeId;
    public static ID accId;
    public static List<Id> assetIds;
    public static List<String> productNames;
    public static List<PricebookEntry> stdPriceBookEntryList;
    public static Integer j;
    public static List<Asset> asstList;
    public static Object ob = 123;
    public static List<Asset> result;
    public static List<TELUS_LineItemWrapper> liwList;
    public static Map<String,Object> lineNumberAsset;
    
    public static testMethod void init()
    {
        result = new List<Asset>();
        liwList = new List<TELUS_LineItemWrapper>();
        lineNumberAsset = new Map<String,Object>();
        
        accntRecordTypeList = [select ID, DeveloperName, NamespacePrefix from RecordType where SobjectType = 'Account' AND NamespacePrefix = 'vlocity_cmt' and DeveloperName = 'Billing'];
        recordTypeId = accntRecordTypeList[0].ID;
        acc = new Account();
        acc.Name = 'TELUSTestServiceAccount1';
        acc.RecordTypeId = recordTypeId;
        insert acc;
        
        accId = acc.Id;        
        assetIds = new List<Id>();
                      
        productList = new List<Product2>();
        productNames = new List<String>();
            
        for (Integer i = 1; i <= 40; i++) {
          String name = 'Product' + i;
          Product2 product = new Product2(Name = name, Description = 'This is a Description' + i, vlocity_cmt__JSONAttribute__c = 'This is a JSON Attribute' + i);
          productList.add(product);
          productNames.add(name);
        }
        insert productList;
        productList = [Select Id, Name from Product2];
            
        //Associate Products with a Pricebook entry
        stdPriceBookEntryList = new List<PricebookEntry>();
        productList = new List<Product2>();
        productList = [Select Id, Name from Product2 where Name IN :productNames];
        j = 1;
        
        Pricebook2 standardBook =  new Pricebook2(Id = Test.getStandardPricebookId(), Name = 'TestPricebook', IsActive = true, vlocity_cmt__IsDefault__c = true);
        for (Product2 product : productList) {
          PricebookEntry standardPricebookEntry = new PricebookEntry(Pricebook2Id = standardBook.Id,
                  Product2Id = product.Id, UnitPrice = j * 2, vlocity_cmt__RecurringPrice__c = j * 17, IsActive = true, UseStandardPrice = false);
          stdPriceBookEntryList.add(standardPricebookEntry);
          j++;
        }
        insert stdPriceBookEntryList;
        
        Asset asset = new Asset();
        asset.Name = 'Factory Test asset';
        asset.AccountId = accId;
        asset.Product2Id = productList[0].Id;
        asset.vlocity_cmt__PricebookEntryId__c = stdPriceBookEntryList[0].Id;
        asset.Quantity = 1;
        asset.Price = 100.00;
        asset.vlocity_cmt__LineNumber__c = '0001';
        asset.vlocity_cmt__PricebookEntryId__c = stdPriceBookEntryList[0].Id;
        asset.vlocity_cmt__ProvisioningStatus__c = 'Active';
        asset.vlocity_cmt__JSONAttribute__c = 'This is a JSON Attribute';

        insert asset;
        
        asstList = [Select Name,AccountId,Product2Id,Quantity,Price from Asset];
        for(Asset asst : asstList){
            asst.vlocity_cmt__ServiceAccountId__c = accId;
            assetIds.add(asst.Id);
        }
        
        Asset ast = asstList[0].clone();
        asstList.add(ast);
        asstList[0].vlocity_cmt__LineNumber__c = '0001';
        asstList[1].vlocity_cmt__LineNumber__c = '0001.0001';
        asstList[0].vlocity_cmt__RootItemId__c = asstList[0].Id;
        asstList[1].vlocity_cmt__RootItemId__c = asstList[0].Id;

        upsert asstList;
        
        
    }
    private static testMethod void testAssetConfigurationAPI1(){
        init();
        
        TELUS_AssetConfigurationController assconf = new TELUS_AssetConfigurationController();
        List<Map<String, String>> columnsMapList = new List<Map<String, String>>();
        Map<String, String> columnsMap = new Map<String, String> {'id' => 'Name', 'name' => 'Name'};
        columnsMapList.add(columnsMap);        
        
        Test.startTest();  
        TELUS_AssetConfigurationController cont = new TELUS_AssetConfigurationController();
        TELUS_AssetConfigurationController.processPriceFields(ob);
        String res = TELUS_AssetConfigurationController.changeToOrderFromAsset(accId, assetIds, 'Service');
        
        /*res = AssetConfigurationController.changeToQuoteFromAsset(accId, assetIds, 'Service');*/
          
        List<Object> obj = TELUS_AssetConfigurationController.getAssetList(accId, null, 'Service');
        //List<Object> obj1 = TELUS_AssetConfigurationController.getAssetList(accId, null, 'Billing');
        System.assert(obj.size()>0);
        
        TELUS_AssetConfigurationController.columnsMap = columnsMapList;
        
        obj = TELUS_AssetConfigurationController.getAssets(accId, null);
        System.assert(obj.size()>0);
        
        obj = TELUS_AssetConfigurationController.getAssets(accId, null);
        System.assert(obj.size()>0);
        
        obj = TELUS_AssetConfigurationController.getAssets(accId, 'fieldSet');
        System.assert(obj.size()>0);
        
        /*Set<String> lab = new Set<String> {'Name', 'Asset.Product2Id'};
       
        List<Map<String, String>> columnsLabel = FieldSetService.getNameLabelList(lab,Asset.SObjectType);
        System.assert(columnsLabel != null); */
        
        TELUS_LineWrapper lineItem1 = new TELUS_LineWrapper(asstList[0]);
        TELUS_LineWrapper.sortBy = 'Descending';
        Integer compare1 = lineItem1.compareTo(new TELUS_LineWrapper(asstList[1]));
        
        
        TELUS_LineItemWrapper lineItemWrapper = new TELUS_LineItemWrapper(lineNumberAsset.put(asstList[0].vlocity_cmt__LineNumber__c,asstList[0]),'Descending');
        Integer compareResult = lineItemWrapper.compareTo(new TELUS_LineItemWrapper(asstList[1]));
        
        TELUS_LineItemWrapper lineItemWrapper1 = new TELUS_LineItemWrapper(lineNumberAsset.put(asstList[0].vlocity_cmt__LineNumber__c,asstList[0]),'');
        Integer compareResult1 = lineItemWrapper1.compareTo(new TELUS_LineItemWrapper(asstList[1]));
        Test.stopTest();
    }
    private static testMethod void testAssetConfigurationAPI2()
    {
        init();
        Test.startTest();
        
        //Error
        Object obj1 = asstList[0];
        TELUS_LineItemWrapper lineItemWrapper1 = new TELUS_LineItemWrapper(lineNumberAsset.put(asstList[0].vlocity_cmt__LineNumber__c,asstList[0]),'Descending');
        Integer compareResult1 = lineItemWrapper1.compareTo(new TELUS_LineItemWrapper((Object)lineNumberAsset.put(asstList[1].vlocity_cmt__LineNumber__c,obj1),'Descending'));
        
        
        TELUS_LineWrapper lineItem1 = new TELUS_LineWrapper(asstList[0]);
        TELUS_LineWrapper.sortBy = 'Ascending';
        Integer compare1 = lineItem1.compareTo(new TELUS_LineWrapper(asstList[1]));
        
        //48% coverage
        /*TELUS_LineItemWrapper lineItemWrapper1 = new TELUS_LineItemWrapper(lineNumberAsset.put(asstList[0].vlocity_cmt__LineNumber__c,asstList[0]),'Custom');
        Integer compareResult1 = lineItemWrapper1.compareTo(new TELUS_LineItemWrapper(asstList[1],'Custom'));*/
        
        /*TELUS_LineItemWrapper lineItemWrapper2 = new TELUS_LineItemWrapper(asstList[0]);
        Integer compareResult2 = lineItemWrapper2.compareTo(new TELUS_LineItemWrapper(asstList[1],'Custom'));*/
        
        for(Asset ast1 : asstList){
            TELUS_LineItemWrapper liw = new TELUS_LineItemWrapper(lineNumberAsset.put(ast1.vlocity_cmt__LineNumber__c,ast1),'Custom');
            liwList.add(liw);
        }
        liwList.sort();
        TELUS_LineItemWrapper.sortBy = 'Descending';
        liwList.sort();
        TELUS_LineItemWrapper.sortBy = 'Ascending';
        liwList.sort();
        for(TELUS_LineItemWrapper liw : liwList) {
            result.add((Asset)liw.sObjectItem);
        }
        
        Test.stopTest();
         
    }

}