public with sharing class ICInventoryLookupController{
  
  public List<IC_Inventory__c> results{get;set;} // search results
  public string recordType{get;set;} // Inventory RecordType Name
  public string selectedlocation {get;set;} 
  public List<SelectOption> locations {get;set;}
  public String searchDeviceType {get;set;}
  public String searchCity {get;set;}    
  public String deliveryMethod {get;set;} 
  
  public Map<String,String> RecordTypeMapping {get;set;}
  public Map<String,String> locationMap = new Map<String,String>();
  public Map<String,String> cityMap = new Map<String,String>();
  
    
  public ICInventoryLookupController(){
      initRecordTypeMapping();
      selectedlocation='';
      // get the record Type from request parameter
      Id recordTypeId = System.currentPageReference().getParameters().get('rt'); 
      RecordType RecType = [Select Id, Name From RecordType  Where SobjectType = 'IC_Solution__c' and Id =:recordTypeId];
      recordType = RecordTypeMapping.get(RecType.Name);
      runSearch();  
 }
    
 // performs the filter search 
 public PageReference Search() {
    runSearch();
    return null;
  }
 
 // prepare the query and update the result
 private void runSearch() {
     searchCity = searchCity == '' || searchCity == null ? '%':searchCity;
   
     String[] deleveryMethods;
     if(deliveryMethod == null || deliveryMethod ==''){
     	deleveryMethods = new String[]{'Customer Pick-up' ,'Courier to Customer'};
     }else{
         deleveryMethods = new String[]{deliveryMethod};
     }
     if(searchDeviceType==null || searchDeviceType==''){
        searchDeviceType ='%';
     }
     if(this.recordType=='Device'){
          results = [select id, name,  Device_Type__c, Comments__c, Device_Provider__r.Name, Loaned_Out__c, Device_Provider__r.Street__c, Device_Provider__r.City__c, Device_Provider__r.Province__c, Device_Provider__r.ICS_Delivery_Method__c, RecordType.Name from IC_Inventory__c
                                       Where RecordType.Name=:this.recordType and Device_Provider__r.City__c Like  :searchCity  and Status__c='Active' and Loaned_Out__c=false and Device_Type__c like :searchDeviceType and Device_Provider__r.ICS_Delivery_Method__c in :deleveryMethods];
  	     for(IC_Inventory__c result: results){
             cityMap.put(result.Device_Provider__r.City__c , result.Device_Provider__r.City__c);  
         }
     }
     if(this.recordType=='Internet Fax Account'){
         results = [select id, name, Fax_Number__c, Loaned_Out__c, Login_ID__c, RecordType.Name from IC_Inventory__c
                                       Where RecordType.Name=:this.recordType and Loaned_Out__c=false and  Status__c='Active'];
     }
     
} 
	/*
	 * Handels the mapping between IC_Solution RecordType and the IC_Inventory RecordType
	 */ 
    private void initRecordTypeMapping(){
        //<IC_Solution.RecortType.Name ,IC_Inventory.RecortType.Name>
        RecordTypeMapping = new Map<String,String>();
        RecordTypeMapping.put('Internet Fax','Internet Fax Account');
        RecordTypeMapping.put('Loaner Device','Device');
    }
    
 /*
  * used by the visualforce page to send the link to the right dom element
  */
  public string getFormTag() {
    return System.currentPageReference().getParameters().get('frm');
  }
 
 /*
  * used by the visualforce page to send the link to the right dom element for the text box
  */
  public string getTextBox() {
    return System.currentPageReference().getParameters().get('txt');
  } 
  
 /*
  *  To Check if the Currenty Invetory list is a Device or not. 
  *	 	
  */  
   public boolean getIsDevice(){
         if(this.recordType=='Device'){
            return true;
         }else{
            return false;
         }
   }
   
   /*
    *======================= Filter Dropdown get Methods====================== 
    */
   
    public List<SelectOption> getDeviceTypes(){
        List<SelectOption> diviceTypes = new List<SelectOption>();
        IC_Inventory__c obj = new IC_Inventory__c();
        diviceTypes.add(new SelectOption('','All'));
        List<String>  listValues =  Util.getPickListValues(obj,'Device_Type__c');
        for(String item : listValues){
            diviceTypes.add(new SelectOption(item,item));
        }
        return diviceTypes;
    }
    
    
    public List<SelectOption> getDeliveryMethods(){
        List<SelectOption> deliveryMethods = new List<SelectOption>();
        IC_Device_Provider__c obj = new IC_Device_Provider__c();
        deliveryMethods.add(new SelectOption('','All'));
        List<String>  listValues =  Util.getPickListValues(obj,'ICS_Delivery_Method__c');
        for(String item : listValues){
            deliveryMethods.add(new SelectOption(item,item));
        }
        return deliveryMethods;
    }
    
    
    public List<SelectOption> getCityList(){
        List<SelectOption> cityList = new List<SelectOption>();
        IC_Inventory__c obj = new IC_Inventory__c();
        cityList.add(new SelectOption('','All'));
        for(String item : cityMap.keySet()){
            cityList.add(new SelectOption(item,cityMap.get(item)));
        }
        return cityList;
    }
}