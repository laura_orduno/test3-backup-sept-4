/**
 * Wrapper around MetaDataUtils class that attempts to
 * use aliases for certain "standard" QuoteQuickly fields.
 * 
 * @author Max Rudman
 * @since 4/9/2011
 */
public class AliasedMetaDataUtils {
	private static final Map<String,Schema.SObjectField> LINE_STANDARD_FIELDS = new Map<String,Schema.SObjectField>{
		'quantity' => SBQQ__QuoteLine__c.SBQQ__Quantity__c,
		'net total' => SBQQ__QuoteLine__c.SBQQ__NetTotal__c,
		'customer total' => SBQQ__QuoteLine__c.SBQQ__CustomerTotal__c,
		'regular total' => SBQQ__QuoteLine__c.SBQQ__RegularTotal__c,
		'list total' => SBQQ__QuoteLine__c.SBQQ__ListTotal__c,
		'product family' => SBQQ__QuoteLine__c.SBQQ__ProductFamily__c,
		'product code' => SBQQ__QuoteLine__c.SBQQ__ProductCode__c,
		'product' => SBQQ__QuoteLine__c.SBQQ__Product__c,
		'markup (%)' => SBQQ__QuoteLine__c.SBQQ__MarkupRate__c,
		'markup (amt)' => SBQQ__QuoteLine__c.SBQQ__MarkupAmount__c,
		'discount (%)' => SBQQ__QuoteLine__c.SBQQ__Discount__c,
		'discount (amt)' => SBQQ__QuoteLine__c.SBQQ__AdditionalDiscountAmount__c,
		'subscription category' => SBQQ__QuoteLine__c.SBQQ__SubscriptionCategory__c
	};
	
	private static final Map<String,Schema.SObjectField> OPTION_STANDARD_FIELDS = new Map<String,Schema.SObjectField>{
		'quantity' => SBQQ__ProductOption__c.SBQQ__Quantity__c,
		'unit price' => SBQQ__ProductOption__c.SBQQ__UnitPrice__c,
		'product code' => SBQQ__ProductOption__c.SBQQ__ProductCode__c,
		'discount (%)' => SBQQ__ProductOption__c.SBQQ__Discount__c,
		'discount (amt)' => SBQQ__ProductOption__c.SBQQ__DiscountAmount__c
	};
	
	private static final Map<String,Schema.SObjectField> PRODUCT_STANDARD_FIELDS = new Map<String,Schema.SObjectField>{
		'product name' => Product2.Name,
		'product family' => Product2.Family,
		'product code' => Product2.ProductCode,
		'subscription category' => Product2.SBQQ__SubscriptionCategory__c
	};
	
	private static final Map<String,Schema.SObjectField> QUOTE_STANDARD_FIELDS = new Map<String,Schema.SObjectField>{
		'type' => SBQQ__Quote__c.SBQQ__Type__c,
		'status' => SBQQ__Quote__c.SBQQ__Status__c,
		'primary' => SBQQ__Quote__c.SBQQ__Primary__c,
		'net amount' => SBQQ__Quote__c.SBQQ__NetAmount__c,
		'total customer disc. amount' => SBQQ__Quote__c.SBQQ__TotalCustomerDiscountAmount__c,
		'addl. disc. amount' => SBQQ__Quote__c.SBQQ__AdditionalDiscountAmount__c,
		'bill to country' => SBQQ__Quote__c.SBQQ__BillingCountry__c,
		'ship to country' => SBQQ__Quote__c.SBQQ__ShippingCountry__c,
		'group line items' => SBQQ__Quote__c.SBQQ__LineItemsGrouped__c
	};
	
	private static final Map<String,AliasedObject> STANDARD_OBJECTS = new Map<String,AliasedObject>{
		'quote line' => new AliasedObject(SBQQ__QuoteLine__c.sObjectType, LINE_STANDARD_FIELDS),
		'product option' => new AliasedObject(SBQQ__ProductOption__c.sObjectType, OPTION_STANDARD_FIELDS),
		'product' => new AliasedObject(Product2.sObjectType, PRODUCT_STANDARD_FIELDS),
		'quote' => new AliasedObject(SBQQ__Quote__c.sObjectType, QUOTE_STANDARD_FIELDS)
	};
	
	public static Schema.SObjectType getType(String objectName) {
		if (objectName == null) {
			return null;
		}
		AliasedObject obj = STANDARD_OBJECTS.get(objectName.toLowerCase());
		if (obj != null) {
			return obj.soType;
		}
		return MetaDataUtils.getType(objectName);
	}
	
	public static Schema.SObjectField getField(String objectName, String fieldName) {
		if ((objectName == null) || (fieldName == null)) {
			return null;
		}
		AliasedObject obj = STANDARD_OBJECTS.get(objectName.toLowerCase());
		if (obj != null) {
			Schema.SObjectField field = obj.fields.get(fieldName.toLowerCase());
			if (field != null) {
				return field;
			}
			return MetaDataUtils.getField(obj.soType, fieldName);
		}
		return MetaDataUtils.getField(objectName, fieldName);
	}
	
	public static Schema.SObjectField getField(Schema.SObjectType soType, String fieldName) {
		if (fieldName == null) {
			return null;
		}
		for (AliasedObject obj : STANDARD_OBJECTS.values()) {
			if (obj.soType == soType) {
				Schema.SObjectField field = obj.fields.get(fieldName.toLowerCase());
				if (field != null) {
					return field;
				}
			}
		}
		return MetaDataUtils.getField(soType, fieldName);
	}
	
	public static void setFieldValue(SObject target, Schema.SObjectField field, Object value) {
		if (value == null) {target.put(String.valueOf(field), null);} else {target.put(String.valueOf(field), value);}
	}
	
	public static Object getFieldValue(SObject target, Schema.SObjectField field) {
		return target.get(String.valueOf(field));
	}
	
	public static Object getFieldValue(SObject target, String field) {
		return target.get(field);
	}
	
	public static void setFieldObject(SObject target, Schema.SObjectField field, SObject value) {
		target.putSObject(field.getDescribe().getRelationshipName(), value);
	}
	
	public static SObject getFieldObject(SObject target, Schema.SObjectField field) {
		return target.getSObject(field.getDescribe().getRelationshipName());
	}
	
	public static Schema.SObjectType determineQuoteType(Id id) {
		System.assert(id != null, 'Quote ID must not be null');
		if (String.valueOf(id).substring(0,3) == SBQQ__Quote__c.sObjectType.getDescribe().getKeyPrefix()) {
			return SBQQ__Quote__c.sObjectType;
		} else if (String.valueOf(id).substring(0,3) == SBQQ__WebQuote__c.sObjectType.getDescribe().getKeyPrefix()) {
			return SBQQ__WebQuote__c.sObjectType;
		}
		System.assert(false, 'Unsupported quote object: ' + id);
		return null;
	}
	
	public static Schema.SObjectType determineQuoteLineType(Id id) {
		Schema.SObjectType qt = determineQuoteType(id);
		if (qt == SBQQ__Quote__c.sObjectType) {
			return SBQQ__QuoteLine__c.sObjectType;
		} else if (qt == SBQQ__WebQuote__c.sObjectType) {
			return SBQQ__WebQuoteLine__c.sObjectType;
		}
		return null;
	}
	
	private class AliasedObject {
		private Schema.SObjectType soType;
		private Map<String,Schema.SObjectField> fields;
		
		private AliasedObject(Schema.SObjectType soType, Map<String,Schema.SObjectField> fields) {
			this.soType = soType;
			this.fields = fields;
		}
	}
	
}