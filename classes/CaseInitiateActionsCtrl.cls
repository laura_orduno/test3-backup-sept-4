public class CaseInitiateActionsCtrl {  
    
    public String outputString {get; set;} 

    public CaseInitiateActionsCtrl(ApexPages.StandardController controller) {
    
        String profileName = '';
        String userRoleName = '';
        User logUsers = [SELECT Id, UserRole.Name, Profile.Name 
                         FROM User 
                         WHERE 
                             id = :UserInfo.getUserId()]; 
        if(logUsers.UserRole.Name != null) {
            userRoleName = logUsers.UserRole.Name;
        }         
        profileName = logUsers.Profile.Name;              
        String caseId = ApexPages.currentPage().getParameters().get('Id');
        List<Case> caseList = [SELECT id, RecordType.Name, IsClosed
                               FROM Case
                               WHERE
                                   Id = :caseId];
        if(caseList.size() > 0) {
            Case initCase = caseList[0];
            if(initCase.RecordType.Name.startsWith('SMB Wireless Repair') && !initCase.IsClosed) {
                if(initCase.RecordType.Name == 'SMB Wireless Repair') {
                    outputString = '<div style="float: left; margin-right: 3px;">'+
                                        '<a href="#" onclick="'+Label.RepairJSStart+'/apex/RepairValidate?id='+initCase.id+ Label.RepairJSEnd+'"><img title="Validate" alt="Validate" src="'+Label.VALIDATE_IMAGE+'"></img></a>'+
                                    '</div>';                                 
                } else {
                    if(userRoleName.startsWith('# BSR') || 
                       userRoleName.startsWith('# CRS') || 
                       userRoleName.startsWith('# Contact Center') || 
                       userRoleName == '# SMB Care Rep' || 
                       userRoleName == '# SMB Care Manager' || 
                       userRoleName == '# National Contact Center & NAC Manager ' || 
                       userRoleName == '# System Admin' || 
                       userRoleName.startsWith('# WBS Support') || 
                       profileName.startsWith('Care') || 
                       profileName.startsWith('SMB Care')) {
                           if(initCase.RecordType.Name == 'SMB Wireless Repair in Progress') {
                               outputString = '<div style="float: left; margin-right: 3px;">'+
                                                  '<a href="/apex/RepairViewRedirect?id='+initCase.id+'" target="_blank"><img title="View In RTS" alt="View In RTS" src="'+Label.VIEW_IN_SRD_IMAGE+'"></img></a>'+
                                              '</div>'; 
                                                                                
                           } else {
                               if(!userRoleName.startsWith('# Contact Center') && userRoleName != '# National Contact Center & NAC Manager ') {
                                   outputString = '<div style="float: left; margin-right: 3px;">'+
                                                       '<a href="#" onclick="'+Label.RepairJSStart+'/apex/RepairCreate?id='+initCase.id+ Label.RepairJSEnd+'"><img title="Create Repair" alt="Create Repair" src="'+Label.CREATE_REPAIR_IMAGE+'"></img></a>'+
                                                  '</div>';                                  
                               } else {
                                   outputString = '';
                               }                           
                           }
                           outputString = outputString +
                                          '<div>'+
                                              '<a href="#" onclick="'+Label.RepairJSStart+'/apex/RepairUpdate?id='+initCase.id+ Label.RepairJSEnd+'"><img title="Get Repair Details" alt="Get Repair Details" src="'+Label.GET_REPAIR_DETAILS_IMAGE+'"></img></a>'+
                                          '</div>';                            
                            
                   } else {
                       outputString = '';
                   }   
                }            
            } else {
                outputString = '';
            }
        }
        
        System.debug('outputString: ' + outputString);
    }

}