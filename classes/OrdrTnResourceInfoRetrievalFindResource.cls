//Methods Included: findResource
// Primary Port Class Name: FindResourceSOAPBindingOSSKIDC	
public class OrdrTnResourceInfoRetrievalFindResource {
	public class FindResourceSOAPBinding {
		//public String endpoint_x = 'https://soa-mp-toll-pt.tsl.telus.com:443/RMO/InventoryMgmt/ResourceInformationRetrievalService_v1_1_vs0';
		public String endpoint_x = '';
		public Map<String,String> inputHttpHeaders_x;
		public Map<String,String> outputHttpHeaders_x;
		public String clientCertName_x;
		public String clientCert_x;
		public String clientCertPasswd_x;
		public Integer timeout_x;
		private String[] ns_map_type_info = new String[]{'http://www.ibm.com/telecom/common/schema/base/v3_0','TpCommonBaseV3','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/common/schema/Message','TpCommonMessage','http://www.ibm.com/telecom/inventory/schema/resource_configuration/v3_0','TpInventoryResourceConfigV3','http://www.ibm.com/telecom/common/schema/place/v3_0','TpCommonPlaceV3','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/FindResource','OrdrTnResourceInfoRetrievalFindResource','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/types/schema/FindResource','OrdrTnResourceInfoRetrievalConfiguration','http://www.ibm.com/telecom/common/schema/mtosi/v3_0','TpCommonMtosiV3','http://www.ibm.com/telecom/common/schema/urban_property_address/v3_0','TpCommonUrbanPropertyAddressV3','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/ResourceConfigurationMessage','OrdrTnResourceInfoRetrievalConfiguration'};
				
		public OrdrTnResourceInfoRetrievalConfiguration.ResourceConfigurationCollectionMessage findResource(TpCommonBaseV3.EntityWithSpecification[] sourceCriteria,TpCommonBaseV3.EntityWithSpecification[] targetCriteria) {
			TpCommonMessage.LookupRequestMessage request_x = new TpCommonMessage.LookupRequestMessage();
			OrdrTnResourceInfoRetrievalConfiguration.ResourceConfigurationCollectionMessage response_x;
			request_x.sourceCriteria = sourceCriteria;
			request_x.targetCriteria = targetCriteria;
			Map<String, OrdrTnResourceInfoRetrievalConfiguration.ResourceConfigurationCollectionMessage> response_map_x = new Map<String, OrdrTnResourceInfoRetrievalConfiguration.ResourceConfigurationCollectionMessage>();
			response_map_x.put('response_x', response_x);
             System.debug('print secondtime FindResourceWirelineEndpoint: '+ endpoint_x);

           
                WebServiceCallout.invoke(
				this,
				request_x,
				response_map_x,
				new String[]{endpoint_x,
				'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/FindResource/findResource',
				'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/types/schema/FindResource',
				'findResource',
				'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/types/schema/FindResource',
				'findResourceResponse',
				'OrdrTnResourceInfoRetrievalConfiguration.ResourceConfigurationCollectionMessage'}
			);
            
			
			response_x = response_map_x.get('response_x');
			return response_x;
		}
	}

}