public with sharing class trac_Related_History_Versions_Ext {
    
    private final Offer_House_Demand__c theOffer;
    public String systemURL {get; set;} // base URL of the current salesforce instance, used to launch new pages
    public Boolean isFrench {get; set;}
    
    public trac_Related_History_Versions_Ext(ApexPages.StandardController stdController) {
        this.theOffer = (Offer_House_Demand__c)stdController.getRecord();
        
        systemURL = URL.getSalesforceBaseUrl().getHost();
        initLanguage();
    }
    
    public List<Offer_House_Demand__c> getTheHistoryVersions() {
        
        List<Offer_House_Demand__c> historyVersions = [SELECT Id, Name, ACCOUNT__c, Type__c, Version_Number__c, CreatedDate,
                                                       OwnerId, LastModifiedById, LastModifiedBy.Name, Owner.Name, ACCOUNT__r.Name,
                                                       Offer_House_Demand__r.ACCOUNT__c, Offer_House_Demand__r.ACCOUNT__r.Name
                                                       FROM Offer_House_Demand__c
                                                       WHERE Offer_House_Demand__c = :theOffer.Id
                                                       AND Type__c = 'Version'
                                                       ORDER BY CreatedDate DESC];
        return historyVersions;
    }
    
    public void initLanguage() {
        isFrench = !(UserInfo.getLanguage() == 'en_US');
    }
    
    public static testMethod void testMyController() {
        
        // User must have a profile and a role
        Id PROFILEID = [SELECT id FROM Profile WHERE name='Sales Manager / Director / MD' LIMIT 1].id;
        
        // Get a user to test
        User tester = [SELECT Id FROM User WHERE isActive = true AND ProfileId = :PROFILEID LIMIT 1];
        
        // Create Offer
        Offer_House_Demand__c o = new Offer_House_Demand__c();
        map<string,schema.recordtypeinfo> recordTypeMap=offer_house_demand__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        o.recordtypeid=recordTypeMap.get('WLN Deal Support').getrecordtypeid();
        o.type_of_contract__c='CCA';
        o.signature_required__c='Yes';
        system.runAs(tester) {  insert o; }
        
        
        ApexPages.StandardController sc = new ApexPages.standardController(o);
        
        // create an instance of the controller
        trac_Related_History_Versions_Ext myPageCon = new trac_Related_History_Versions_Ext(sc);
        myPageCon.getTheHistoryVersions();
        
    }
}