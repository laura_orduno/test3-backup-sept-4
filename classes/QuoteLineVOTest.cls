@isTest
private class QuoteLineVOTest {
	private static testMethod void testQuoteLineVO() {
    	final String PRODUCT_NAME = 'prodname';
    	final Integer QUANTITY = 2;
    	
    	Opportunity o = new Opportunity(name = 'test', closedate = Date.today(), stagename = 'test');
    	insert o;
    	
    	SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c = o.id);
    	insert quote;
    	
    	Product2 p = new Product2(name = PRODUCT_NAME);
    	insert p;
    	
    	SBQQ__QuoteLine__c line = new SBQQ__QuoteLine__c(
    		SBQQ__Quote__c = quote.id,
    		SBQQ__Product__c = p.id,
    		SBQQ__Quantity__c = QUANTITY
    	);
    	insert line;
    	
    	QuoteLineVO qlv = new QuoteLineVO(line);
    	
    	List<QuoteLineVO> wrappedLines = qlv.getAll();
    	
    	List<QuoteLineVO.ConfigurationField> configFields = qlv.getConfigurationFields();
    	
    	String configLink = qlv.getConfigureLink();
    	String completeLink = qlv.getQuoteCompleteInformationLink();
    }
    
    private static testMethod void testConfigurationField() {
    	QuoteLineVO.ConfigurationField cf = new QuoteLineVO.ConfigurationField('test', 'test');
    	
    	system.assertEquals('test', cf.label);
    	system.assertEquals('test', cf.value);
    }
}