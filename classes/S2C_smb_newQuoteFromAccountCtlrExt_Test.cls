/**
 * S2C_smb_newQuoteFromAccountCtlrExt_Test
 * @description Test class for S2C_smb_newQuoteFromAccountCtlrExt
 * @author Thomas Tran, Traction on Demand
 * @date 07-22-2015
 */
@isTest(seeAllData=false)
private class S2C_smb_newQuoteFromAccountCtlrExt_Test {
	@isTest static void paramater_assignments() {
		Account newAccount = new Account(Name = 'New Account 001');
		insert newAccount;

		Contact newContact = new Contact(FirstName = 'First Contact 001', LastName = 'Last Contact 001');
		insert newContact;
		PageReference refPage = S2C_smb_newQuoteFromAccountCtlrExt.launchWizard(1000, newAccount, newContact.Id, 'Complex Order', 'Originated', newAccount.Name + ' - COMPLEX ORDER');

		String oppAmount = refPage.getParameters().get('OPP_AMOUNT');
		String accountId = refPage.getParameters().get('OPP_ACCOUNT_ID');
		String oppType = refPage.getParameters().get('OPP_TYPE');
		String oppStageName = refPage.getParameters().get('OPP_STAGE_NAME');
		String oppName = refPage.getParameters().get('OPP_NAME');
		String contactId = refPage.getParameters().get('CONTACT_ID');     

		System.assertEquals(1000, Integer.valueOf(oppAmount));
		System.assertEquals(newAccount.Id, accountId);
		System.assertEquals(newContact.Id, contactId);		
		System.assertEquals('Complex Order', oppType);
		System.assertEquals('Originated', oppStageName);
		System.assertEquals(newAccount.Name + ' - COMPLEX ORDER - MOVE',oppName);

	}

	@isTest static void paramater_assignments_no_oppname() {
		Account newAccount = new Account(Name = 'New Account 001');
		insert newAccount;

		Contact newContact = new Contact(FirstName = 'First Contact 001', LastName = 'Last Contact 001');
		insert newContact;

		Test.startTest();

		PageReference refPage = S2C_smb_newQuoteFromAccountCtlrExt.launchWizard(1000, newAccount, newContact.Id, 'Complex Order', 'Originated', '');

		Test.stopTest();

		String oppAmount = refPage.getParameters().get('OPP_AMOUNT');
		String accountId = refPage.getParameters().get('OPP_ACCOUNT_ID');
		String oppType = refPage.getParameters().get('OPP_TYPE');
		String oppStageName = refPage.getParameters().get('OPP_STAGE_NAME');
		String oppName = refPage.getParameters().get('OPP_NAME');
		String contactId = refPage.getParameters().get('CONTACT_ID');

		System.assertEquals(1000, Integer.valueOf(oppAmount));
		System.assertEquals(newAccount.Id, accountId);
		System.assertEquals(newContact.Id, contactId);		
		System.assertEquals('Complex Order', oppType);
		System.assertEquals('Originated', oppStageName);
		System.assert(String.isNotBlank(oppName));

	}
}