@isTest
public class ContractHistoryComponentCntrlTest {

    @isTest
    public static void testgetObjectHistory(){
        
        Agreement_Contact__c a  = new Agreement_Contact__c(Authorized_Administrator__c=true);
        insert a;
        a.Authorized_Administrator__c = false;
        update a;
        ContractHistoryComponentCntrl ctrl = new ContractHistoryComponentCntrl();
        ctrl.objName = 'Agreement_Contact__c';
        ctrl.ObjectId = a.id;
      //  ctrl.listEntityHistory = getHistoryList();
        List<ContractHistoryComponentCntrl.ObjectHistory> recs = ctrl.ObjectHistories;
        
       ContractHistoryComponentCntrl.ObjectHistory hisObj = new ContractHistoryComponentCntrl.ObjectHistory(getHistoryList().get(0));
       String f = hisObj.ActionFormat;
        
    }
    
    private static  List<sObject> getHistoryList(){
       List<sObject> li= new List<Sobject>();
        Sobject  obj = new Agreement_Contact__History();
        obj.put('Field','Owner');
       li.add(obj);
        return li;
    }
    
    
}