/**
 * Implementation of <code>SelectOptionModel</code> that sources options from
 * value sof specified field.
 * 
 * @author Max Rudman
 * @since 9/10/2009
 */
public class PicklistValuesOptionModel implements SelectOptionModel {
	private List<SelectOption> options;
	
	public PicklistValuesOptionModel(Schema.SObjectField field, Set<String> exclusions) {
		this(field.getDescribe().getPicklistValues(), exclusions);
	}
	
	public PicklistValuesOptionModel(Schema.SObjectField field) {
		this(field.getDescribe().getPicklistValues());
	}
	
	public PicklistValuesOptionModel(List<Schema.PicklistEntry> picklistValues) {
		this(picklistValues, null);
	}
	
	public PicklistValuesOptionModel(List<Schema.PicklistEntry> picklistValues, Set<String> exclusions) {
		if (exclusions == null) {
			exclusions = new Set<String>();
		}
		options = new List<SelectOption>();
		for (Schema.PicklistEntry entry : picklistValues) {
			if (!exclusions.contains(entry.getValue())) {
				options.add(new SelectOption(entry.getValue(),entry.getLabel()));
			}
		}
	}

	/**
	 * Returns Select Options generated by this model.
	 * 
	 * @return List of <code>SelectOption</code> objects.
	 */
	public List<SelectOption> getOptions() {
		return options;
	}
}