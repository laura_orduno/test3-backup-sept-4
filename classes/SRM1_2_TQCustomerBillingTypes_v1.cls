//Generated by wsdl2apex

public class SRM1_2_TQCustomerBillingTypes_v1 {
    public class BillingAddress {
        public String unformattedBillingAddressText;
        public String streetName;
        public String streetType;
        public String direction;
        public String civicNumber;
        public String suffix;
        public String cityName;
        public String provinceCode;
        public String postalCode;
        public String countryName;
        public String locationTypeCode;
        public String locationNumber;
        public String installationTypeCode;
        public String installationName;
        public String deliveryNumber;
        public String deliveryModeCode;
        public String zipCode;
        private String[] unformattedBillingAddressText_type_info = new String[]{'unformattedBillingAddressText','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] streetName_type_info = new String[]{'streetName','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] streetType_type_info = new String[]{'streetType','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] direction_type_info = new String[]{'direction','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] civicNumber_type_info = new String[]{'civicNumber','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] suffix_type_info = new String[]{'suffix','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] cityName_type_info = new String[]{'cityName','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'1','1','false'};
        private String[] provinceCode_type_info = new String[]{'provinceCode','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] postalCode_type_info = new String[]{'postalCode','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] countryName_type_info = new String[]{'countryName','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] locationTypeCode_type_info = new String[]{'locationTypeCode','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] locationNumber_type_info = new String[]{'locationNumber','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] installationTypeCode_type_info = new String[]{'installationTypeCode','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] installationName_type_info = new String[]{'installationName','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] deliveryNumber_type_info = new String[]{'deliveryNumber','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] deliveryModeCode_type_info = new String[]{'deliveryModeCode','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] zipCode_type_info = new String[]{'zipCode','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1','false','false'};
        private String[] field_order_type_info = new String[]{'unformattedBillingAddressText','streetName','streetType','direction','civicNumber','suffix','cityName','provinceCode','postalCode','countryName','locationTypeCode','locationNumber','installationTypeCode','installationName','deliveryNumber','deliveryModeCode','zipCode'};
    }
    public class Payment {
        public Long paymentId;
        public String customerAccountNumber;
        public Double paymentAmount;
        public Date paymentDate;
        public String paymentTypeCode;
        public Boolean paymentBilledInd;
        public Date billDate;
        public Long batchNumber;
        public Date paymentProcessedDate;
        public String receiptNumber;
        public Boolean paymentProcessedInd;
        private String[] paymentId_type_info = new String[]{'paymentId','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'1','1','false'};
        private String[] customerAccountNumber_type_info = new String[]{'customerAccountNumber','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'1','1','false'};
        private String[] paymentAmount_type_info = new String[]{'paymentAmount','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'1','1','false'};
        private String[] paymentDate_type_info = new String[]{'paymentDate','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'1','1','false'};
        private String[] paymentTypeCode_type_info = new String[]{'paymentTypeCode','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] paymentBilledInd_type_info = new String[]{'paymentBilledInd','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] billDate_type_info = new String[]{'billDate','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'1','1','false'};
        private String[] batchNumber_type_info = new String[]{'batchNumber','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] paymentProcessedDate_type_info = new String[]{'paymentProcessedDate','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] receiptNumber_type_info = new String[]{'receiptNumber','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] paymentProcessedInd_type_info = new String[]{'paymentProcessedInd','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1','false','false'};
        private String[] field_order_type_info = new String[]{'paymentId','customerAccountNumber','paymentAmount','paymentDate','paymentTypeCode','paymentBilledInd','billDate','batchNumber','paymentProcessedDate','receiptNumber','paymentProcessedInd'};
    }
    public class BillingSummary {
        public String customerAccountNumber;
        public String billingTelephoneNumber;
        public Date billingDate;
        public Double previousBillTotalDueAmount;
        public Double billPeriodDueAmount;
        public Double billTotalDueAmount;
        public Date paymentDueDate;
        public String paymentReferenceNumber;
        private String[] customerAccountNumber_type_info = new String[]{'customerAccountNumber','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'1','1','false'};
        private String[] billingTelephoneNumber_type_info = new String[]{'billingTelephoneNumber','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'1','1','false'};
        private String[] billingDate_type_info = new String[]{'billingDate','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'1','1','false'};
        private String[] previousBillTotalDueAmount_type_info = new String[]{'previousBillTotalDueAmount','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'1','1','false'};
        private String[] billPeriodDueAmount_type_info = new String[]{'billPeriodDueAmount','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'1','1','false'};
        private String[] billTotalDueAmount_type_info = new String[]{'billTotalDueAmount','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'1','1','false'};
        private String[] paymentDueDate_type_info = new String[]{'paymentDueDate','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'1','1','false'};
        private String[] paymentReferenceNumber_type_info = new String[]{'paymentReferenceNumber','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1','false','false'};
        private String[] field_order_type_info = new String[]{'customerAccountNumber','billingTelephoneNumber','billingDate','previousBillTotalDueAmount','billPeriodDueAmount','billTotalDueAmount','paymentDueDate','paymentReferenceNumber'};
    }
    public class HistoricalUsageStatistics {
        public Double averageMonthlyFeesLast3Months;
        public Double averageLongDistanceBilledAmountLast3Months;
        public Integer averageLongDistanceUsageMinutesLast3Months;
        private String[] averageMonthlyFeesLast3Months_type_info = new String[]{'averageMonthlyFeesLast3Months','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'1','1','false'};
        private String[] averageLongDistanceBilledAmountLast3Months_type_info = new String[]{'averageLongDistanceBilledAmountLast3Months','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'1','1','false'};
        private String[] averageLongDistanceUsageMinutesLast3Months_type_info = new String[]{'averageLongDistanceUsageMinutesLast3Months','http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://xmlschema.tmi.telus.com/xsd/Customer/CustomerBill/TQCustomerBillingTypes_v1','false','false'};
        private String[] field_order_type_info = new String[]{'averageMonthlyFeesLast3Months','averageLongDistanceBilledAmountLast3Months','averageLongDistanceUsageMinutesLast3Months'};
    }
}