public class CreditProfileCalloutUpdateHelper {
    
    
    private static final Integer WS_TIMEOUT_MS = 120000;
    
    /*------------------------------------------------------------------------*/    
    public static void prepareCallout(CreditProfileService.BusinessCreditProfileMgmtService_v1_0_SOAP servicePort){
        
        System.debug('... dyy ... CreditProfileCalloutUpdateHelper... void prepareCallout ...');
        
        if ( !Test.isRunningTest() ){
            SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username');
            SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');
            String creds;
            if (String.isNotBlank(WSusername.Value__c) && String.isNotBlank(wsPassword.Value__c)) {
                creds = WSusername.Value__c + ':' + wsPassword.Value__c;
            }
            String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(creds));
            servicePort.inputHttpHeaders_x = new Map<String, String>();
            servicePort.inputHttpHeaders_x.put('Authorization', 'Basic ' + encodedusernameandpassword);
            servicePort.timeout_x = WS_TIMEOUT_MS;
        }
    }
    /*------------------------------------------------------------------------*/
    
    
    public static Credit_Profile__c getCreditProfile(ID profileId){
        
        Credit_Profile__c profile = new Credit_Profile__c();
        
        try {
            profile = [select id,name
                       ,Account__r.CustProfId__c
                       ,Alternate_Registered_Name__c
                       ,Application_province__c
                       ,Birth_Date__c
                       ,Business_Anywhere_Monthly_Spend_Limit__c
                       ,City_Head_Office__c
                       ,City_Residential__c
                       ,Consumer_profile_ID__c
                       ,Corporate_Registry_Status__c
                       ,Country_Head_Office__c
                       ,Country_Residential__c
                       ,Credit_Bureau_Check_Accepted__c
                       ,Customer_Website__c
                       ,Delinquent_Accounts__c
                       ,Deposit_Customer__c
                       ,Driver_s_License__c
                       ,Drivers_License_province__c
                       ,DUNS_No_Secondary__c
                       ,Health_Card_No__c
                       ,Last_Validation_Date__c
                       ,Legal_Entity_Type__c
                       ,Legal_Name_On_File__c
                       ,Legal_Name_Should_Be__c
                       ,Mandatory_Assessment__c
                       ,Passport__c
                       ,Passport_Country__c
                       ,Postal_Code_Head_Office__c
                       ,Postal_Code_Residential__c
                       ,Province_State_Head_Office__c
                       ,Province_State_Residential__c
                       ,Quebec_Health_Name__c
                       ,Registration_Date__c
                       ,Registration_Jurisdiction__c
                       ,Registration_Jurisdiction_Country__c
                       ,Registration_No__c
                       ,Risk_Indicator__c
                       ,SIN__c
                       ,Street_Address_Head_Office__c
                       ,Street_Address_Residential__c
                       ,Valid_Legal_Name__c
                       ,Provincial_ID__c
                       ,Provincial_ID_Province__c
                       ,Credit_Reference_TL__c
                       ,Other_Approved_Business_Names__c
                       from Credit_Profile__c where id = :profileId
                      ];
        }catch (Exception e){
            System.debug('Error occured retrieving profile for Id = ' + profileId 
                         + ', ' + e);
        }
        
        return profile;
    }
    
    
    
    
    public static void populateIndividualCreditProfile(
        CreditProfileServiceTypes.IndividualCreditProfile individualCreditProfile
        , Credit_Profile__c creditProfil){
            
            CreditProfileServiceTypes.CreditCheckAddress 
                creditAddress = new CreditProfileServiceTypes.CreditCheckAddress();            
            CreditProfileServiceTypes.IndividualCreditIdentification 
                creditIdentification = new CreditProfileServiceTypes.IndividualCreditIdentification();            
            CreditProfileServiceTypes.IndividualPersonalInfo 
                personalInfo = new CreditProfileServiceTypes.IndividualPersonalInfo();
            
            CreditProfileServiceTypes.DriverLicense driverLicense = new CreditProfileServiceTypes.DriverLicense();
            CreditProfileServiceTypes.HealthCard healthCard = new CreditProfileServiceTypes.HealthCard();
            CreditProfileServiceTypes.Passport passport = new CreditProfileServiceTypes.Passport();
            CreditProfileServiceTypes.ProvincialIdCard provincialIdCard = new CreditProfileServiceTypes.ProvincialIdCard();
             
            
            personalInfo.creditCheckConsentCd = CreditReferenceCode.getSingleLetterCode(creditProfil.Credit_Bureau_Check_Accepted__c);
            
            System.debug('... dyy ... String.isNotBlank(creditProfil.Street_Address_Residential__c) = ' + String.isNotBlank(creditProfil.Street_Address_Residential__c));
            System.debug('... dyy ... Y.equalsignorecase(personalInfo.creditCheckConsentCd) = ' + 'Y'.equalsignorecase(personalInfo.creditCheckConsentCd));
            if(String.isNotBlank(creditProfil.Street_Address_Residential__c)
               && String.isNotBlank(creditProfil.City_Residential__c)
               && String.isNotBlank(creditProfil.Country_Residential__c)
               && 'Y'.equalsignorecase(personalInfo.creditCheckConsentCd))
                individualCreditProfile.creditAddress = creditAddress;
            
            if(String.isNotBlank(creditProfil.Driver_s_License__c)
               || String.isNotBlank(creditProfil.Health_Card_No__c)
               || String.isNotBlank(creditProfil.Passport__c)
               || String.isNotBlank(creditProfil.Provincial_ID__c)
              ) individualCreditProfile.creditIdentification = creditIdentification;
            
            if(creditProfil.Birth_Date__c!=null)
                individualCreditProfile.personalInfo = personalInfo;
            
            
            System.debug('... dyy ... creditProfil.Driver_s_License__c = ' + creditProfil.Driver_s_License__c);
            System.debug('... dyy ... creditProfil.Drivers_License_province__c = ' + creditProfil.Drivers_License_province__c);
            
            if(String.isNotBlank(creditProfil.Driver_s_License__c))
                creditIdentification.driverLicense = driverLicense;            
            if(String.isNotBlank(creditProfil.Health_Card_No__c))
                creditIdentification.healthCard = healthCard;
            if(String.isNotBlank(creditProfil.Passport__c))
                creditIdentification.passport = passport;   
            if(String.isNotBlank(creditProfil.Provincial_ID__c))
                creditIdentification.provincialIdCard = provincialIdCard;
            
            System.debug('... dyy ... creditProfil.Provincial_ID__c = ' + creditProfil.Provincial_ID__c);
            
            
            creditIdentification.sin = creditProfil.SIN__c;
            
            individualCreditProfile.applicationProvinceCd = CreditReferenceCode.getJurisdictionProvinceCd(creditProfil.Application_province__c) ;
            individualCreditProfile.commentTxt = 'NA';     
            
            personalInfo.birthDate = creditProfil.Birth_Date__c;

            creditAddress.addressLineOneTxt = creditProfil.Street_Address_Residential__c;
            creditAddress.cityName = creditProfil.City_Residential__c;
            creditAddress.provinceCd = CreditReferenceCode.getJurisdictionProvinceCd(creditProfil.Province_State_Residential__c);
            creditAddress.countryCd = CreditReferenceCode.getJurisdictionCountryCd(creditProfil.Country_Residential__c) ;
            creditAddress.postalCd = creditProfil.Postal_Code_Residential__c;

            driverLicense.driverLicenseNum = creditProfil.Driver_s_License__c;
            driverLicense.provinceCd = CreditReferenceCode.getJurisdictionProvinceCd(creditProfil.Drivers_License_province__c);
            
            healthCard.healthCardNum = creditProfil.Health_Card_No__c;
            
            passport.passportNum = creditProfil.Passport__c;
            passport.countryCd = CreditReferenceCode.getJurisdictionCountryCd(creditProfil.Passport_Country__c);
            
            provincialIdCard.provincialIdNum = creditProfil.Provincial_ID__c;
            provincialIdCard.provinceCd = CreditReferenceCode.getJurisdictionProvinceCd(creditProfil.Provincial_ID_Province__c);            
            
        }
    
    
    
    public static void populateUpdatedBusinessCreditProfile(
        CreditProfileServiceTypes.BusinessCreditProfileBase updatedBusinessCreditProfile
        , Credit_Profile__c creditProfil){
            
            
            CreditProfileServiceTypes.CreditCheckAddress businessCreditCheckAddress 
                = new CreditProfileServiceTypes.CreditCheckAddress ();
            CreditProfileServiceTypes.RegistrationIncorporation registrationIncorporation   
                =new CreditProfileServiceTypes.RegistrationIncorporation ();
            
            if(String.isNotBlank(creditProfil.Street_Address_Head_Office__c)
               && String.isNotBlank(creditProfil.City_Head_Office__c)
               && String.isNotBlank(creditProfil.Country_Head_Office__c)) 
                updatedBusinessCreditProfile.businessCreditCheckAddress = businessCreditCheckAddress;
            
            updatedBusinessCreditProfile.registrationIncorporation = registrationIncorporation;
            
            mapCreditCheckAddress(businessCreditCheckAddress, creditProfil);            
            mapRegistrationIncorporation(registrationIncorporation, creditProfil);
            
            //updatedBusinessCreditProfile.businessCreditProfileId = Long.valueof(creditProfil.id);
            
            if(creditProfil.Deposit_Customer__c!=null)
                updatedBusinessCreditProfile.existingDepositInd = ('Yes'==creditProfil.Deposit_Customer__c);
            else 
                updatedBusinessCreditProfile.existingDepositInd = false;
            
            if(creditProfil.Valid_Legal_Name__c!=null)
                updatedBusinessCreditProfile.validLegalNameOnFileInd = ('Yes'==creditProfil.Valid_Legal_Name__c);
            else 
                updatedBusinessCreditProfile.validLegalNameOnFileInd = false;
            
            updatedBusinessCreditProfile.legalEntityTypeCd 
                = CreditReferenceCode.getLegalEntityTypeCd(
                    creditProfil.Legal_Entity_Type__c); //.substringBefore('(').trim());
            
            updatedBusinessCreditProfile.alternateRegisteredName = creditProfil.Alternate_Registered_Name__c;
            updatedBusinessCreditProfile.companyWebsiteUrl = creditProfil.Customer_Website__c;
            updatedBusinessCreditProfile.corporateRegistryStatusCd = CreditReferenceCode.getCorporateRegistryStatus(creditProfil.Corporate_Registry_Status__c);
            updatedBusinessCreditProfile.correctedLegalName = creditProfil.Legal_Name_Should_Be__c;
            updatedBusinessCreditProfile.creditRiskIndicatorCd = CreditReferenceCode.getCreditRiskIndicatorCd(creditProfil.Risk_Indicator__c);
            updatedBusinessCreditProfile.delinquencyInd = creditProfil.Delinquent_Accounts__c;
            updatedBusinessCreditProfile.legalNameLastValidationCheckDate = creditProfil.Last_Validation_Date__c;
            updatedBusinessCreditProfile.mandatoryAssessmentRequiredInd = creditProfil.Mandatory_Assessment__c;
            updatedBusinessCreditProfile.quebecHealthName = creditProfil.Quebec_Health_Name__c;
            updatedBusinessCreditProfile.secondaryDunsNum = creditProfil.DUNS_No_Secondary__c;
            
            updatedBusinessCreditProfile.creditReferenceTierLimitNum = creditProfil.Credit_Reference_TL__c;
            
            updatedBusinessCreditProfile.otherApprovedBusinessNameList 
                = CreditWebServiceClientUtil.getListOfStringsPerLinebreak(creditProfil.Other_Approved_Business_Names__c);
            
            /*
            updatedBusinessCreditProfile.creditRiskIndicatorLastUpdatedDate = null;
            updatedBusinessCreditProfile.creditCommentList = null;*/

            
        }
    
    
    public static void mapRegistrationIncorporation(
        CreditProfileServiceTypes.RegistrationIncorporation registrationIncorporation
        , Credit_Profile__c creditProfil){
            registrationIncorporation.registrationIncorporationNum = creditProfil.Registration_No__c;
            registrationIncorporation.registrationDate = creditProfil.Registration_Date__c;
            registrationIncorporation.jurisdictionProvinceCd 
                = CreditReferenceCode.getJurisdictionProvinceCd(creditProfil.Registration_Jurisdiction__c);
            registrationIncorporation.jurisdictionCountryCd 
                = CreditReferenceCode.getJurisdictionCountryCd(creditProfil.Registration_Jurisdiction_Country__c);
        }
    
    public static void mapCreditCheckAddress(
        CreditProfileServiceTypes.CreditCheckAddress businessCreditCheckAddress
        , Credit_Profile__c creditProfil){
            businessCreditCheckAddress.addressLineOneTxt = creditProfil.Street_Address_Head_Office__c;
            businessCreditCheckAddress.cityName = creditProfil.City_Head_Office__c;
            businessCreditCheckAddress.provinceCd 
                = CreditReferenceCode.getJurisdictionProvinceCd(creditProfil.Province_State_Head_Office__c);
            businessCreditCheckAddress.postalCd = creditProfil.Postal_Code_Head_Office__c;
            businessCreditCheckAddress.countryCd 
                = CreditReferenceCode.getJurisdictionCountryCd(creditProfil.Country_Head_Office__c);
        }
    
    
    
    public static Long getBusinessCustomerId(Credit_Profile__c creditProfil){
        Long businessCustomerId;
        if((creditProfil.Account__r!=null)
           && (creditProfil.Account__r.CustProfId__c!=null))
            businessCustomerId = Long.valueof(creditProfil.Account__r.CustProfId__c);
        System.debug('... dyy ... businessCustomerId = ' + businessCustomerId); 
        return businessCustomerId;       
    }
    
    
    public static Long getIndividualCustomerId(Credit_Profile__c creditProfil){
        Long individualCustomerId;
        if(creditProfil.Consumer_profile_ID__c!=null)
            individualCustomerId = Long.valueof(creditProfil.Consumer_profile_ID__c);
        System.debug('... dyy ... individualCustomerId = ' + individualCustomerId); 
        return individualCustomerId;
    }
    
    /*-------------------------------------------------*/
    
      
    
    
    public static void populateBusinessCreditReportRequest(
        CreditProfileServiceTypes.BusinessCreditReportRequest businessCreditReportRequest
        , Document document
        , String documentTypeCd
        , String documentSourceCd
    ){ 
        
        businessCreditReportRequest.documentName = 
            document.name.substringBeforeLast('.') 
            + '.' 
            + document.name.substringAfterLast('.').toLowerCase();
        System.debug('... dyy ... document.Name = ' + document.Name);

        businessCreditReportRequest.creditReportContent = EncodingUtil.base64Encode(document.Body);
        businessCreditReportRequest.documentTypeCd = documentTypeCd;
        businessCreditReportRequest.documentSourceCd = documentSourceCd;
        businessCreditReportRequest.userId = UserInfo.getUserId();
        businessCreditReportRequest.appId = '6161';
    }
    
    /*------------------------------------------------*/
    
    
    public static void deleteCreditProfileFromSFDC(ID cpId){
        try{
            Credit_Profile__c cp = new Credit_Profile__c();
            cp.id = cpId;
            delete cp;
        }catch(Exception e){
            System.debug('... dyy ... error deleteCreditProfileFromSFDC ... ' + e.getMessage());
        }
    }
    
    
}