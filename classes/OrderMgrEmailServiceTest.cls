@isTest(SeeAllData=false)
private class OrderMgrEmailServiceTest {
    @testSetup static void setupData() {
        Account acct = new Account(name='testAccout');
        insert acct;
        Order ord = new Order(name='testOrder',AccountId=acct.id,status='Draft',EffectiveDate=Date.today());
        insert ord;        
    }
    static testmethod void testNoOrder() {
        test.startTest();
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        email.fromAddress = 'test@test.com';
        email.toAddresses = new List<String>{'test2@test2.com'};
        email.fromName = 'test';
        email.subject = 'test: ';
        email.plainTextBody = 'email body';
        
        system.debug('email='+email);
        
        OrderMgrEmailService srv = new OrderMgrEmailService();
        srv.processEmail(email);
        
        test.stopTest();
    }
    static testmethod void testTeamMember() {
        test.startTest();
        Order savedOrd = [select id,name,ordernumber from Order limit 1];
        system.debug('savedOrd.ordernumber='+savedOrd.ordernumber);
        system.debug('savedOrd='+savedOrd);
        
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        email.fromAddress = 'test@test.com';
        email.toAddresses = new List<String>{'test2@test2.com'};
        email.fromName = 'test';
        email.subject = 'test: ' + savedOrd.ordernumber;
        email.plainTextBody = 'email body';
        
        system.debug('email='+email);
        
        OrderMgrEmailService srv = new OrderMgrEmailService();
        srv.processEmail(email);
        
        test.stopTest();
    }
    static testmethod void testGoodOrder() {
        test.startTest();
        Order savedOrd = [select id,name,ordernumber from Order limit 1];
        system.debug('savedOrd.ordernumber='+savedOrd.ordernumber);
        system.debug('savedOrd='+savedOrd);
        
        User usrObj = [select id,name,license_type__c from User where license_type__c = 'Salesforce' limit 1];
        Cloud_Enablement_Team_Member__c teamMember = new Cloud_Enablement_Team_Member__c();

        if(usrObj != null) {
            teamMember.Manager__c = usrObj.id;
            teamMember.Order__c = savedOrd.id;
            teamMember.Team_Member__c = usrObj.id;
            teamMember.Team_Name__c = 'Order Manager';
            insert teamMember;
        }
        
        //Messaging.Inboundenvelope envelope;
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        email.fromAddress = 'test@test.com';
        email.toAddresses = new List<String>{'test2@test2.com'};
        email.fromName = 'test';
        email.subject = 'test: ' + savedOrd.ordernumber;
        email.plainTextBody = 'email body';
        
        Messaging.InboundEmail.Header hdr = new Messaging.InboundEmail.Header();
        hdr.name = 'Date';
        hdr.value = String.valueOf(DateTime.now());
        email.Headers = new Messaging.InboundEmail.Header[] {hdr};
        
        String str = '1234567890abcdefg';
        Messaging.Inboundemail.BinaryAttachment bAttachment = new Messaging.Inboundemail.BinaryAttachment();
        bAttachment.fileName = 'binaryfile';
        bAttachment.body = Blob.valueOf(str);
        bAttachment.mimeTypeSubType = 'plain/txt';
        email.binaryAttachments = new List<Messaging.Inboundemail.BinaryAttachment>();
        email.binaryAttachments.add(bAttachment);
        
        Messaging.Inboundemail.TextAttachment tAttachment = new Messaging.Inboundemail.TextAttachment();
        tAttachment.fileName = 'textfile';
        tAttachment.body = str;
        tAttachment.mimeTypeSubType = 'plain/txt';
        email.TextAttachments = new Messaging.Inboundemail.TextAttachment[] {tAttachment};
        
        system.debug('email='+email);

        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        env.fromAddress = 'test@test.com';

        OrderMgrEmailService srv = new OrderMgrEmailService();
        //srv.processEmail(email);
        srv.handleInboundEmail(email,env);
        
        test.stopTest();
    }
}