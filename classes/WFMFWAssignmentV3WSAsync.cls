//Generated by wsdl2apex

public class WFMFWAssignmentV3WSAsync {
    public class FieldWorkAssignmentMgmtServicePortAsync {
        
        public String endpoint_x = 'https://webservice1.preprd.teluslabs.net/RMO/ProcessMgmt/FieldWorkAssignmentMgmtService_v3_0_vs1';
        public Map<String,String> inputHttpHeaders_x;
        public String clientCertName_x;
        public Integer timeout_x;
        
        private String[] ns_map_type_info = new String[]{ 
            'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/Exceptions_v3', 'WFMAssignmentExceptionV3',
            'http://xmlschema.tmi.telus.com/srv/RMO/ProcessMgmt/FieldWorkAssignmentMgmtServiceRequestResponse_v3', 'WFMFWAssignmentSvcReqResV3', 
            'http://xmlschema.tmi.telus.com/xsd/Resource/Resource/WorkForceManagementOrderTypes_v3', 'WFMOrderTypesV3', 
            'http://xmlschema.tmi.telus.com/xsd/Enterprise/BaseTypes/EnterpriseCommonTypes_v9', 'WFMAssignEnterpriseCommonTypesV3', 
            'http://telus.com/wsdl/RMO/ProcessMgmt/FieldWorkAssignmentMgmtService_3', 'WFMFWAssignmentV3WS'
         };
             
        public WFMFWAssignmentSvcReqResV3Async.SearchWorkOrderListResponseFuture beginSearchWorkOrderList(System.Continuation continuation,WFMOrderTypesV3.InputHeader inputHeader,WFMOrderTypesV3.WorkOrderIdent[] workorderIdentList) {
            WFMFWAssignmentSvcReqResV3.SearchWorkOrderList request_x = new WFMFWAssignmentSvcReqResV3.SearchWorkOrderList();
            request_x.inputHeader = inputHeader;
            request_x.workorderIdentList = workorderIdentList;
            return (WFMFWAssignmentSvcReqResV3Async.SearchWorkOrderListResponseFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              WFMFWAssignmentSvcReqResV3Async.SearchWorkOrderListResponseFuture.class,
              continuation,
              new String[]{endpoint_x,
              'searchWorkOrderList',
              'http://xmlschema.tmi.telus.com/srv/RMO/ProcessMgmt/FieldWorkAssignmentMgmtServiceRequestResponse_v3',
              'searchWorkOrderList',
              'http://xmlschema.tmi.telus.com/srv/RMO/ProcessMgmt/FieldWorkAssignmentMgmtServiceRequestResponse_v3',
              'searchWorkOrderListResponse',
              'WFMFWAssignmentSvcReqResV3.SearchWorkOrderListResponse'}
            );
        }
        public WFMFWAssignmentSvcReqResV3Async.UpdateWorkOrderResponseFuture beginUpdateWorkOrder(System.Continuation continuation,WFMOrderTypesV3.InputHeader inputHeader,WFMOrderTypesV3.WorkOrder workOrder) {
            WFMFWAssignmentSvcReqResV3.UpdateWorkOrder request_x = new WFMFWAssignmentSvcReqResV3.UpdateWorkOrder();
            request_x.inputHeader = inputHeader;
            request_x.workOrder = workOrder;
            return (WFMFWAssignmentSvcReqResV3Async.UpdateWorkOrderResponseFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              WFMFWAssignmentSvcReqResV3Async.UpdateWorkOrderResponseFuture.class,
              continuation,
              new String[]{endpoint_x,
              'updateWorkOrder',
              'http://xmlschema.tmi.telus.com/srv/RMO/ProcessMgmt/FieldWorkAssignmentMgmtServiceRequestResponse_v3',
              'updateWorkOrder',
              'http://xmlschema.tmi.telus.com/srv/RMO/ProcessMgmt/FieldWorkAssignmentMgmtServiceRequestResponse_v3',
              'updateWorkOrderResponse',
              'WFMFWAssignmentSvcReqResV3.UpdateWorkOrderResponse'}
            );
        }
        public WFMFWAssignmentSvcReqResV3Async.GetWorkOrderResponseFuture beginGetWorkOrder(System.Continuation continuation,WFMOrderTypesV3.InputHeader inputHeader,String workOrderId) {
            WFMFWAssignmentSvcReqResV3.GetWorkOrder request_x = new WFMFWAssignmentSvcReqResV3.GetWorkOrder();
            request_x.inputHeader = inputHeader;
            request_x.workOrderId = workOrderId;
            return (WFMFWAssignmentSvcReqResV3Async.GetWorkOrderResponseFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              WFMFWAssignmentSvcReqResV3Async.GetWorkOrderResponseFuture.class,
              continuation,
              new String[]{endpoint_x,
              'getWorkOrder',
              'http://xmlschema.tmi.telus.com/srv/RMO/ProcessMgmt/FieldWorkAssignmentMgmtServiceRequestResponse_v3',
              'getWorkOrder',
              'http://xmlschema.tmi.telus.com/srv/RMO/ProcessMgmt/FieldWorkAssignmentMgmtServiceRequestResponse_v3',
              'getWorkOrderResponse',
              'WFMFWAssignmentSvcReqResV3.GetWorkOrderResponse'}
            );
        }
        public WFMFWAssignmentSvcReqResV3Async.CancelWorkOrderResponseFuture beginCancelWorkOrder(System.Continuation continuation,WFMOrderTypesV3.InputHeader inputHeader,WFMOrderTypesV3.WorkOrder workOrder) {
            WFMFWAssignmentSvcReqResV3.CancelWorkOrder request_x = new WFMFWAssignmentSvcReqResV3.CancelWorkOrder();
            request_x.inputHeader = inputHeader;
            request_x.workOrder = workOrder;
            return (WFMFWAssignmentSvcReqResV3Async.CancelWorkOrderResponseFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              WFMFWAssignmentSvcReqResV3Async.CancelWorkOrderResponseFuture.class,
              continuation,
              new String[]{endpoint_x,
              'cancelWorkOrder',
              'http://xmlschema.tmi.telus.com/srv/RMO/ProcessMgmt/FieldWorkAssignmentMgmtServiceRequestResponse_v3',
              'cancelWorkOrder',
              'http://xmlschema.tmi.telus.com/srv/RMO/ProcessMgmt/FieldWorkAssignmentMgmtServiceRequestResponse_v3',
              'cancelWorkOrderResponse',
              'WFMFWAssignmentSvcReqResV3.CancelWorkOrderResponse'}
            );
        }
        public WFMFWAssignmentSvcReqResV3Async.CreateWorkOrderResponseFuture beginCreateWorkOrder(System.Continuation continuation,WFMOrderTypesV3.InputHeader inputHeader,WFMOrderTypesV3.WorkOrder workOrder) {
            WFMFWAssignmentSvcReqResV3.CreateWorkOrder request_x = new WFMFWAssignmentSvcReqResV3.CreateWorkOrder();
            request_x.inputHeader = inputHeader;
            request_x.workOrder = workOrder;
            return (WFMFWAssignmentSvcReqResV3Async.CreateWorkOrderResponseFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              WFMFWAssignmentSvcReqResV3Async.CreateWorkOrderResponseFuture.class,
              continuation,
              new String[]{endpoint_x,
              'createWorkOrder',
              'http://xmlschema.tmi.telus.com/srv/RMO/ProcessMgmt/FieldWorkAssignmentMgmtServiceRequestResponse_v3',
              'createWorkOrder',
              'http://xmlschema.tmi.telus.com/srv/RMO/ProcessMgmt/FieldWorkAssignmentMgmtServiceRequestResponse_v3',
              'createWorkOrderResponse',
              'WFMFWAssignmentSvcReqResV3.CreateWorkOrderResponse'}
            );
        }
    }
}