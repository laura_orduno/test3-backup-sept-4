@isTest
public class CreditSrchUpdPartnerControllerTest
{
    private static CreditSearchUpdPartnerProfileController controller;

    @isTest(SeeAllData=false)
    public static void getCurrentCreditProfile()
    {
        initialize();

        Test.startTest();
        controller.getCurrentCreditProfile();
        Test.stopTest();
    }

    @isTest(SeeAllData=false)
    public static void getDisplayProfileForColumnSelection()
    {
        initialize();

        Test.startTest();
        controller.getDisplayProfileForColumnSelection();
        Test.stopTest();
    }

    @isTest(SeeAllData=false)
    public static void getSearchParam()
    {
        initialize();

        Test.startTest();
        controller.getSearchParam();
        Test.stopTest();
    }

    @isTest
    public static void getCurrentCreditPartner()
    {
        initializeBasicController();

        Test.startTest();
        controller.getCurrentCreditPartner();
        Test.stopTest();
    }

    @isTest(SeeAllData=false)
    public static void search()
    {
        initializeWithCorporatePartner();

        CreditSearchUpdPartnerProfileController.CreditProfileSearchParam userSearchParam = new CreditSearchUpdPartnerProfileController.CreditProfileSearchParam();
        userSearchParam.dunsNum = '100';

        controller.userSearchParam = userSearchParam;

        Test.startTest();

        controller.search();

        Test.stopTest();
    }

    @isTest(SeeAllData=false)
    public static void searchIndividualPartner()
    {
        initialize();

        CreditSearchUpdPartnerProfileController.CreditProfileSearchParam userSearchParam = new CreditSearchUpdPartnerProfileController.CreditProfileSearchParam();
        userSearchParam.driversLicenseNum = 'DL987321';
        userSearchParam.driversLicenseProvince = 'BC';
        userSearchParam.healthCardNum = 'H321654';

        controller.userSearchParam = userSearchParam;

        ApexPages.currentPage().getParameters().put('inputType', 'IND');

        Test.startTest();
        controller.search();
        Test.stopTest();
    }

    @isTest(SeeAllData=false)
    public static void selectProfileForUpdate()
    {
        initialize();

        controller.creditProfileForUpdateId = '100';

        CreditSearchUpdPartnerProfileController.CreditProfileSearchParam userSearchParam = new CreditSearchUpdPartnerProfileController.CreditProfileSearchParam();
        userSearchParam.dunsNum = '100';
        userSearchParam.driversLicenseNum = 'DL987321';
        userSearchParam.driversLicenseProvince = 'BC';

        controller.userSearchParam = userSearchParam;

        Test.startTest();

        controller.search();

        controller.selectProfileForUpdate();

        Test.stopTest();
    }

    @isTest(SeeAllData=false)
    public static void selectProfileForUpdateForIndividualPartner()
    {
        initialize();

        controller.creditProfileForUpdateId = '100';

        CreditSearchUpdPartnerProfileController.CreditProfileSearchParam userSearchParam = new CreditSearchUpdPartnerProfileController.CreditProfileSearchParam();
        userSearchParam.driversLicenseNum = 'DL987321';
        userSearchParam.driversLicenseProvince = 'BC';
        userSearchParam.passportNum = '456123';
        userSearchParam.passportCountry = 'CAN';
        userSearchParam.healthCardNum = 'H321654';

        controller.userSearchParam = userSearchParam;

        Test.startTest();

        controller.search();

        controller.selectProfileForUpdate();

        Test.stopTest();
    }

    @isTest(SeeAllData=false)
    public static void selectProfileForUpdateForCorporatePartner()
    {
        initializeWithCorporatePartner();

        controller.creditProfileForUpdateId = '100';

        CreditSearchUpdPartnerProfileController.CreditProfileSearchParam userSearchParam = new CreditSearchUpdPartnerProfileController.CreditProfileSearchParam();
        userSearchParam.rcid = '0000003245';
        userSearchParam.registrationNum = '123456789';
        userSearchParam.legalName = 'Banner Industries';

        controller.userSearchParam = userSearchParam;

        Test.startTest();

        controller.search();

        controller.selectProfileForUpdate();

        Test.stopTest();
    }

    @isTest(SeeAllData=false)
    public static void previewCreateCreditProfile()
    {
        initialize();

        Test.startTest();

        controller.previewCreateCreditProfile();

        Test.stopTest();
    }

    @isTest(SeeAllData=false)
    public static void executeUpdateForCorporatePartner()
    {
        initializeWithCorporatePartner();

        ApexPages.currentPage().getParameters().put('inputType', 'CORP');

        CreditSearchUpdPartnerProfileController.CreditProfileSearchParam userSearchParam = new CreditSearchUpdPartnerProfileController.CreditProfileSearchParam();
        userSearchParam.rcid = '0000003245';
        userSearchParam.registrationNum = '123456789';
        userSearchParam.legalName = 'Banner Industries';

        controller.userSearchParam = userSearchParam;

        Test.startTest();

        List<CreditSearchUpdPartnerProfileController.DisplayCreditProfile> matchingCreditProfiles = new List<CreditSearchUpdPartnerProfileController.DisplayCreditProfile>();

        CreditSearchUpdPartnerProfileController.DisplayCreditProfile displayCreditProfile = new CreditSearchUpdPartnerProfileController.DisplayCreditProfile();
        displayCreditProfile.key = '100';
        displayCreditProfile.rcid = '0000003245';
        displayCreditProfile.registrationNum = '123456789';
        displayCreditProfile.legalName = 'Banner Industries';
        displayCreditProfile.creditProfileInstance = [SELECT Id, RCID__c, DUNS_No__c, Registration_No__c, Registration_Date__c, Registration_Jurisdiction__c, Street_Address_Head_Office__c, City_Head_Office__c, Province_State_Head_Office__c FROM Credit_Profile__c WHERE Name = 'UnitTest'];

        matchingCreditProfiles.add(displayCreditProfile);

        controller.matchingCreditProfiles = matchingCreditProfiles;

        controller.creditProfileForUpdateId = displayCreditProfile.key;
        controller.selectProfileForUpdate();

        controller.columnToSave = 'REGISTRATION_NUM';
        controller.toggleColumnToSave();

        controller.columnToSave = 'REGISTRATION_DATE';
        controller.toggleColumnToSave();

        controller.columnToSave = 'REGISTRATION_JURISDICTION';
        controller.toggleColumnToSave();

        controller.columnToSave = 'HEAD_OFFICE_ADDRESS';
        controller.toggleColumnToSave();

        controller.columnToSave = 'CITY';
        controller.toggleColumnToSave();

        controller.columnToSave = 'PROVINCE';
        controller.toggleColumnToSave();

        controller.executeUpdate();

        Test.stopTest();
    }

    @isTest
    public static void executeUpdateForIndividualPartner()
    {
        Test.startTest();

        initializeWithIndividualPartner();

        CreditSearchUpdPartnerProfileController.CreditProfileSearchParam userSearchParam = new CreditSearchUpdPartnerProfileController.CreditProfileSearchParam();
        userSearchParam.sin = '046454286';
        userSearchParam.provincialIdNum = '3333';
        userSearchParam.provincialIdProvince = 'BC';

        controller.userSearchParam = userSearchParam;

        List<CreditSearchUpdPartnerProfileController.DisplayCreditProfile> matchingCreditProfiles = new List<CreditSearchUpdPartnerProfileController.DisplayCreditProfile>();

        CreditSearchUpdPartnerProfileController.DisplayCreditProfile displayCreditProfile = new CreditSearchUpdPartnerProfileController.DisplayCreditProfile();
        displayCreditProfile.key = '100';
        displayCreditProfile.sin = '046454286';
        displayCreditProfile.provincialIdNum = '3333';
        displayCreditProfile.provincialIdProvince = 'BC';
        displayCreditProfile.creditProfileInstance = [SELECT Id, First_Name__C, Last_Name__c, Birth_Date__c, SIN__c, Driver_s_License__C, Drivers_License_province__c, Provincial_ID__c, Provincial_ID_Province__c, Passport__c, Passport_Country__c, Health_Card_No__c FROM Credit_Profile__c WHERE Name = 'UnitTest'];

        matchingCreditProfiles.add(displayCreditProfile);

        controller.matchingCreditProfiles = matchingCreditProfiles;

        controller.creditProfileForUpdateId = displayCreditProfile.key;
        controller.selectProfileForUpdate();

        controller.columnToSave = 'SIN';
        controller.toggleColumnToSave();

        controller.columnToSave = 'BIRTHDATE';
        controller.toggleColumnToSave();

        controller.columnToSave = 'DRIVERS_LICENSE';
        controller.toggleColumnToSave();

        controller.columnToSave = 'PROVINCIAL_ID';
        controller.toggleColumnToSave();

        controller.columnToSave = 'PASSPORT';
        controller.toggleColumnToSave();

        controller.columnToSave = 'HEALTHCARD';
        controller.toggleColumnToSave();

        controller.executeUpdate();

        Test.stopTest();
    }

    @isTest
    public static void executeUpdateForIndividualPartnerWithMatchInSdfc()
    {
        Test.startTest();

        initializeWithIndividualPartner();

        CreditSearchUpdPartnerProfileController.CreditProfileSearchParam userSearchParam = new CreditSearchUpdPartnerProfileController.CreditProfileSearchParam();
        userSearchParam.sin = '046454286';
        userSearchParam.provincialIdNum = '3333';
        userSearchParam.provincialIdProvince = 'BC';

        controller.userSearchParam = userSearchParam;

        List<CreditSearchUpdPartnerProfileController.DisplayCreditProfile> matchingCreditProfiles = new List<CreditSearchUpdPartnerProfileController.DisplayCreditProfile>();

        CreditSearchUpdPartnerProfileController.DisplayCreditProfile displayCreditProfile = new CreditSearchUpdPartnerProfileController.DisplayCreditProfile();
        displayCreditProfile.key = '100';
        displayCreditProfile.sin = '046454286';
        displayCreditProfile.provincialIdNum = '3333';
        displayCreditProfile.provincialIdProvince = 'BC';
        displayCreditProfile.consumerProfileId = '7';
        displayCreditProfile.creditProfileInstance = [SELECT Id, First_Name__C, Last_Name__c, Birth_Date__c, SIN__c, Driver_s_License__C, Drivers_License_province__c, Provincial_ID__c, Provincial_ID_Province__c, Passport__c, Passport_Country__c, Health_Card_No__c FROM Credit_Profile__c WHERE Name = 'UnitTest'];

        matchingCreditProfiles.add(displayCreditProfile);

        controller.matchingCreditProfiles = matchingCreditProfiles;

        controller.creditProfileForUpdateId = displayCreditProfile.key;
        controller.selectProfileForUpdate();

        controller.columnToSave = 'SIN';
        controller.toggleColumnToSave();

        controller.columnToSave = 'BIRTHDATE';
        controller.toggleColumnToSave();

        controller.columnToSave = 'DRIVERS_LICENSE';
        controller.toggleColumnToSave();

        controller.columnToSave = 'PROVINCIAL_ID';
        controller.toggleColumnToSave();

        controller.columnToSave = 'PASSPORT';
        controller.toggleColumnToSave();

        controller.columnToSave = 'HEALTHCARD';
        controller.toggleColumnToSave();

        controller.executeUpdate();

        Test.stopTest();
    }

    @isTest
    public static void executeUpdateForIndividualPartnerWithMatchInSdfc2()
    {
        Test.startTest();

        initializeWithIndividualPartner();

        CreditSearchUpdPartnerProfileController.CreditProfileSearchParam userSearchParam = new CreditSearchUpdPartnerProfileController.CreditProfileSearchParam();
        userSearchParam.sin = '046454286';
        userSearchParam.provincialIdNum = '3333';
        userSearchParam.provincialIdProvince = 'BC';

        controller.userSearchParam = userSearchParam;

        List<CreditSearchUpdPartnerProfileController.DisplayCreditProfile> matchingCreditProfiles = new List<CreditSearchUpdPartnerProfileController.DisplayCreditProfile>();

        CreditSearchUpdPartnerProfileController.DisplayCreditProfile displayCreditProfile = new CreditSearchUpdPartnerProfileController.DisplayCreditProfile();
        displayCreditProfile.key = '100';
        displayCreditProfile.sin = '046454286';
        displayCreditProfile.provincialIdNum = '3333';
        displayCreditProfile.provincialIdProvince = 'BC';
        displayCreditProfile.consumerProfileId = '7';
        displayCreditProfile.creditProfileInstance = [SELECT Id, First_Name__C, Last_Name__c, Birth_Date__c, SIN__c, Driver_s_License__C, Drivers_License_province__c, Provincial_ID__c, Provincial_ID_Province__c, Passport__c, Passport_Country__c, Health_Card_No__c FROM Credit_Profile__c WHERE Name = 'UnitTest'];

        matchingCreditProfiles.add(displayCreditProfile);

        controller.matchingCreditProfiles = matchingCreditProfiles;

        controller.creditProfileForUpdateId = displayCreditProfile.key;
        controller.selectProfileForUpdate();

        controller.columnToSave = 'SIN_2';
        controller.toggleColumnToSave();

        controller.columnToSave = 'BIRTHDATE_2';
        controller.toggleColumnToSave();

        controller.columnToSave = 'DRIVERS_LICENSE_2';
        controller.toggleColumnToSave();

        controller.columnToSave = 'PROVINCIAL_ID_2';
        controller.toggleColumnToSave();

        controller.columnToSave = 'PASSPORT_2';
        controller.toggleColumnToSave();

        controller.columnToSave = 'HEALTHCARD_2';
        controller.toggleColumnToSave();

        controller.executeUpdate();

        Test.stopTest();
    }

    @isTest(SeeAllData=false)
    public static void cancelUpdate()
    {
        initialize();

        Test.startTest();
        controller.cancelUpdate();
        Test.stopTest();
    }

    @isTest(SeeAllData=false)
    public static void toggleColumnToSave()
    {
        initialize();

        controller.isShowSelectedCorporatePartnerProfile = true;
        controller.originalProfileForColumnSelection = new CreditSearchUpdPartnerProfileController.DisplayCreditProfile();

        Test.startTest();

        controller.columnToSave = 'REGISTRATION_NUM';
        controller.toggleColumnToSave();

        controller.columnToSave = 'REGISTRATION_DATE';
        controller.toggleColumnToSave();

        controller.columnToSave = 'REGISTRATION_JURISDICTION';
        controller.toggleColumnToSave();

        controller.columnToSave = 'HEAD_OFFICE_ADDRESS';
        controller.toggleColumnToSave();

        controller.columnToSave = 'CITY';
        controller.toggleColumnToSave();

        controller.columnToSave = 'PROVINCE';
        controller.toggleColumnToSave();

        controller.columnToSave = 'SIN';
        controller.toggleColumnToSave();

        controller.columnToSave = 'BIRTHDATE';
        controller.toggleColumnToSave();

        controller.columnToSave = 'DRIVERS_LICENSE';
        controller.toggleColumnToSave();

        controller.columnToSave = 'PROVINCIAL_ID';
        controller.toggleColumnToSave();

        controller.columnToSave = 'PASSPORT';
        controller.toggleColumnToSave();

        controller.columnToSave = 'HEALTHCARD';
        controller.toggleColumnToSave();

        controller.columnToSave = 'SIN_2';
        controller.toggleColumnToSave();

        controller.columnToSave = 'BIRTHDATE_2';
        controller.toggleColumnToSave();

        controller.columnToSave = 'DRIVERS_LICENSE_2';
        controller.toggleColumnToSave();

        controller.columnToSave = 'PROVINCIAL_ID_2';
        controller.toggleColumnToSave();

        controller.columnToSave = 'PASSPORT_2';
        controller.toggleColumnToSave();

        controller.columnToSave = 'HEALTHCARD_2';
        controller.toggleColumnToSave();

        Test.stopTest();
    }

    @isTest(SeeAllData=false)
    public static void executeCreateProfile()
    {
        initialize();

        Test.startTest();

        controller.executeCreateProfile();

        Test.stopTest();
    }

    public static void executeCreateProfileNoValues()
    {
        initializeBasicController();

        ApexPages.currentPage().getParameters().put('inputType', 'IND');

        Test.startTest();

        controller.executeCreateProfile();

        Test.stopTest();
    }

    @isTest(SeeAllData=false)
    public static void cancelCreateProfile()
    {
        initialize();

        Test.startTest();

        controller.cancelCreateProfile();

        Test.stopTest();
    }

    private static void initializeWithCorporatePartner()
    {
        Test.setMock(WebServiceMock.class, new BusinessCreditProfileMgmtServiceMock());

        setupTestDataCorporatePartner();

        String Id = [Select Id From Credit_Partners__c Where Legal_Name__c='Banner Industries'].Id;

        Account a = new Account(name='Tester');
        insert a;

        ApexPages.currentPage().getParameters().put('inputID', Id);
        ApexPages.currentPage().getParameters().put('inputType', 'CORP');

        controller = new CreditSearchUpdPartnerProfileController(new ApexPages.StandardController(a));
    }

    private static void initializeWithIndividualPartner()
    {
        Test.setMock(WebServiceMock.class, new BusinessCreditProfileMgmtServiceMock());

        setupTestDataIndividualPartner();

        String Id = [Select Id From Credit_Partners__c Where Legal_Name__c='Banner Industries'].Id;

        Account a = new Account(name='Tester');
        insert a;

        ApexPages.currentPage().getParameters().put('inputID', Id);
        ApexPages.currentPage().getParameters().put('inputType', 'IND');

        controller = new CreditSearchUpdPartnerProfileController(new ApexPages.StandardController(a));
    }

    private static void initialize()
    {
        Test.setMock(WebServiceMock.class, new BusinessCreditProfileMgmtServiceMock());

        setupTestData();

        String Id = [Select Id From Credit_Partners__c Where Legal_Name__c='Banner Industries'].Id;

        Account a = new Account(name='Tester');
        insert a;

        ApexPages.currentPage().getParameters().put('inputID', Id);
        ApexPages.currentPage().getParameters().put('inputType', 'IND');

        controller = new CreditSearchUpdPartnerProfileController(new ApexPages.StandardController(a));
    }

    private static void initializeBasicController()
    {
        Account a = new Account(name='Tester');
        insert a;

        controller = new CreditSearchUpdPartnerProfileController(new ApexPages.StandardController(a));
    }

    private static void setupTestDataCorporatePartner()
    {
        Contact contact = new Contact();
        contact.FirstName = 'Unit';
        contact.LastName = 'Test';
        contact.Email = 'noreply@telus.com';

        insert contact;

        Credit_Profile__c profile = new Credit_Profile__c();

        profile.Name = 'UnitTest';
        profile.First_Name__C = 'Unit';
        profile.Last_Name__c = 'Test';
        profile.Street_Address_Head_Office__c = '8888 Georgia';
        profile.City_Head_Office__c = 'Vancouver';
        profile.Province_State_Head_Office__c = 'British Columbia';
        profile.City_Residential__c = 'Burnaby';
        profile.SIN__c = '046454286';
        profile.Driver_s_License__C = 'DL987321';
        profile.Drivers_License_province__c = 'BC';
        profile.Provincial_ID__c = '3333';
        profile.Provincial_ID_Province__c = 'BC';
        profile.Passport__c = '123456789';
        profile.Passport_Country__c = 'Canada';
        profile.Health_Card_No__c = '123456789';
        profile.Consumer_profile_ID__c = '7';
        profile.Sole_Proprietor_Contact__c = contact.id;
        profile.RecordTypeId = [Select id From RecordType where sObjectType = 'Credit_Profile__c' and name ='Partnership'].id;

        insert profile;

        Credit_Partners__c partner = new Credit_Partners__c();
        partner.Legal_Name__c = 'Banner Industries';
        partner.Registration_No__c = '123456789';
        partner.Registration_Date__c = Date.newInstance(2000, 1, 1);
        partner.Registration_Jurisdiction__c = 'British Columbia';
        partner.Street_Address_Head_Office__c = '8888 Georgia';
        partner.City_Head_Office__c = 'Vancouver';
        partner.Province_State_Head_Office__c = 'British Columbia';
        partner.City_Residential__c = 'Burnaby';
        partner.Province_State_Residential__c = 'British Columbia';
        partner.Postal_Code_Residential__c = 'V5S 3Z6';
        partner.Country_Residential__c = 'Canada';
        partner.DUNS_No__c = '100';
        partner.RCID__c = '200';
        partner.Credit_Bureau_Check_Accepted__c = 'No';
        partner.Application_province__c = 'British Columbia';
        partner.Corporate_Partner__c = profile.id;
        partner.RecordTypeId = [Select id From RecordType where sObjectType = 'Credit_Partners__C' and developerName ='Corporate_Partner'].id;

        insert partner;
    }

    private static void setupTestDataIndividualPartner()
    {
        Contact contact = new Contact();
        contact.FirstName = 'Unit';
        contact.LastName = 'Test';
        contact.Email = 'noreply@telus.com';

        insert contact;

        Credit_Profile__c profile = new Credit_Profile__c();

        profile.Name = 'UnitTest';
        profile.First_Name__C = 'Unit';
        profile.Last_Name__c = 'Test';
        profile.Birth_Date__c = Date.newInstance(2000, 1, 1);
        profile.SIN__c = '046454286';
        profile.Driver_s_License__C = 'DL987321';
        profile.Drivers_License_province__c = 'BC';
        profile.Provincial_ID__c = '3333';
        profile.Provincial_ID_Province__c = 'BC';
        profile.Passport__c = '123456789';
        profile.Passport_Country__c = 'Canada';
        profile.Health_Card_No__c = '123456789';
        profile.Consumer_profile_ID__c = '7';
        profile.Sole_Proprietor_Contact__c = contact.id;
        profile.RecordTypeId = [Select id From RecordType where sObjectType = 'Credit_Profile__c' and name ='Individual'].id;

        insert profile;

        Credit_Partners__c partner = new Credit_Partners__c();
        partner.Legal_Name__c = 'Banner Industries';
        partner.First_Name__c = 'Bruce';
        partner.Last_Name__c = 'Banner';
        partner.Registration_No__c = '123456789';
        partner.Registration_Date__c = null;
        partner.Registration_Jurisdiction__c = 'British Columbia';
        partner.Street_Address_Head_Office__c = '8888 Georgia';
        partner.City_Head_Office__c = 'Vancouver';
        partner.Province_State_Head_Office__c = 'British Columbia';
        partner.City_Residential__c = 'Burnaby';
        partner.Province_State_Residential__c = 'British Columbia';
        partner.Postal_Code_Residential__c = 'V5S 3Z6';
        partner.Country_Residential__c = 'Canada';
        partner.SIN__c = '046454286';
        partner.Birth_Date__c = null;
        partner.Driver_s_License__c = 'DL987321';
        partner.Drivers_License_Province__c = 'British Columbia';
        partner.Provincial_ID__c = '3333';
        partner.Provincial_ID_Province__c = 'British Columbia';
        partner.Passport__c = '123456789';
        partner.Passport_Country__c = 'Canada';
        partner.Health_Card_No__c = 'H321654';
        partner.Credit_Bureau_Check_Accepted__c = 'Yes';
        partner.Application_province__c = 'British Columbia';
        partner.Mobile__c = '6046041111';
        partner.Contact_No__c = '6046042222';
        partner.Individual_Partner__c = profile.id;
        partner.RecordTypeId = [Select id From RecordType where sObjectType = 'Credit_Partners__C' and developerName ='Individual_Partner'].id;

        insert partner;
    }

    private static void setupTestData()
    {
        Credit_Partners__c partner = new Credit_Partners__c();
        partner.Legal_Name__c = 'Banner Industries';
        partner.First_Name__c = 'Bruce';
        partner.Last_Name__c = 'Banner';
        partner.Registration_No__c = '123456789';
        partner.Registration_Date__c = null;
        partner.Registration_Jurisdiction__c = 'British Columbia';
        partner.Street_Address_Head_Office__c = '8888 Georgia';
        partner.City_Head_Office__c = 'Vancouver';
        partner.Province_State_Head_Office__c = 'British Columbia';
        partner.City_Residential__c = 'Burnaby';
        partner.Province_State_Residential__c = 'British Columbia';
        partner.Postal_Code_Residential__c = 'V5S 3Z6';
        partner.Country_Residential__c = 'Canada';
        partner.SIN__c = '046454286';
        partner.Birth_Date__c = null;
        partner.Driver_s_License__c = 'DL987321';
        partner.Drivers_License_Province__c = 'British Columbia';
        partner.Provincial_ID__c = '3333';
        partner.Provincial_ID_Province__c = 'British Columbia';
        partner.Passport__c = '123456789';
        partner.Passport_Country__c = 'Canada';
        partner.Health_Card_No__c = 'H321654';
        partner.Credit_Bureau_Check_Accepted__c = 'Yes';
        partner.Application_province__c = 'British Columbia';
        partner.Mobile__c = '6046041113';
        partner.Contact_No__c = '6046042223';
        partner.RecordTypeId = [Select id From RecordType where sObjectType = 'Credit_Partners__C' and developerName ='Individual_Partner'].id;

        insert partner;

        Contact contact = new Contact();
        contact.FirstName = 'Unit';
        contact.LastName = 'Test';
        contact.Email = 'noreply@telus.com';

        insert contact;

        Credit_Profile__c profile = new Credit_Profile__c();

        profile.Name = 'UnitTest';
        profile.First_Name__C = 'Unit';
        profile.Last_Name__c = 'Test';
        profile.SIN__c = '046454286';
        profile.Driver_s_License__C = 'DL987321';
        profile.Drivers_License_province__c = 'BC';
        profile.Provincial_ID__c = '3333';
        profile.Provincial_ID_Province__c = 'BC';
        profile.Passport__c = '123456789';
        profile.Passport_Country__c = 'Canada';
        profile.Health_Card_No__c = '123456789';
        profile.Consumer_profile_ID__c = '7';
        profile.Sole_Proprietor_Contact__c = contact.id;
        profile.RecordTypeId = [Select id From RecordType where sObjectType = 'Credit_Profile__c' and name ='Individual'].id;

        insert profile;
    }
}