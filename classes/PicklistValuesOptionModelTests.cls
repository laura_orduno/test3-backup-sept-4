/**
 * Test suite for PicklistValuesOptionModel.
 * 
 * @author Max Rudman
 * @since 9/10/2009
 */
@isTest
private class PicklistValuesOptionModelTests {
	testMethod static void testNoExclusions() {
		String firstValue = Account.sObjectType.fields.Type.getDescribe().getPicklistValues().get(0).getValue();
		PicklistValuesOptionModel target = new PicklistValuesOptionModel(Account.sObjectType.fields.Type);
		System.assertEquals(Account.sObjectType.fields.Type.getDescribe().getPicklistValues().size(), target.getOptions().size());
		System.assertEquals(firstValue, target.getOptions().get(0).getValue());
	}
	
	testMethod static void testWithExclusions() {
		String firstValue = Account.sObjectType.fields.Type.getDescribe().getPicklistValues().get(0).getValue();
		PicklistValuesOptionModel target = new PicklistValuesOptionModel(Account.sObjectType.fields.Type, new Set<String>{firstValue});
		System.assertEquals(Account.sObjectType.fields.Type.getDescribe().getPicklistValues().size() - 1, target.getOptions().size());
		if (target.getOptions().size() > 0) {
			System.assert(firstValue != target.getOptions().get(0).getValue());
		}
	}
}