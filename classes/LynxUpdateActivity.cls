public class LynxUpdateActivity {

	public class sendRequest {
		public LynxUpdateActivity.sendRequest SRequest(string caseNumber, ID CaseId, string description, string customerActivityId, string TELUSTicketId, String ContWorkGroup) {
			// Create the request envelope
			DOM.Document doc = new DOM.Document();
			//String endpoint = 'https://soa-mp-toll-st01.tsl.telus.com:443/SMO/ProblemMgmt/LynxTroubleTicketMgmtService_v1_0_vs0';
			//String endpoint = 'https://xmlgwy-pt1.telus.com:9030/st01/SMO/ProblemMgmt/LynxTroubleTicketMgmtService_v1_0_vs0';
			String endpoint = '';

			// TODO: Re-leverage custom setting for endpoint, no hard-coding
			if (!Test.isRunningTest()) {
				endpoint = SMBCare_WebServices__c.getInstance('TTODS_Endpoint_Update_Lynx').Value__c;
			} else {
				endpoint = 'https://xmlgwy.telus.com:9030/SMO/ProblemMgmt/LynxTroubleTicketMgmtService_v1_0_vs0';
			}

			//String endpoint = 'https://xmlgwy.telus.com:9030/SMO/ProblemMgmt/LynxTroubleTicketMgmtService_v1_0_vs0';
			////////////
			String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
			string ebon = 'http://ebonding.telus.com';
			string ass = 'http://assurance.ebonding.telus.com';
			datetime creationDateTime = datetime.now();
			date mydate = creationDateTime.date();
			datetime getmytime = datetime.now();
			time mytime = getmytime.time();
			String StatusCode = '';
			string AssnTWorkGroup = '';
			String plannedStartDateTime = '';
			string testTime = string.valueOf(mydate) + 'T' + string.valueOf(mytime);
			string createdate = string.valueOfGmt(creationDateTime);
			string logicalId = Label.ENTPLogicalID;
			string messageId = logicalId + creationDateTime;
			String encodemessageId = EncodingUtil.base64Encode(Blob.valueOf(messageId));
			if (ContWorkGroup != '' && ContWorkGroup != null && ContWorkGroup != 'NBA - Service Desk Partner') {
				//StatusCode = Label.ENTPStatusCodeSub;
				/*RB-Must be hard coded so French version will not show up, causes errors in Lynx*/
				StatusCode = 'Submit';
				AssnTWorkGroup = '';
				plannedStartDateTime = '';
			} else {
				//StatusCode = Label.ENTPStatusCodeUnassigned;
				/*RB-Must be hard coded so French version will not show up, causes errors in Lynx*/
				StatusCode = 'Unassigned';
				AssnTWorkGroup = Label.ENTPAssignedToWorkGrp;
				plannedStartDateTime = testTime;

			}
			dom.XmlNode envelope
					= doc.createRootElement('Envelope', soapNS, 'soapenv');
			envelope.setNamespace('ebon', ebon);
			envelope.setNamespace('ass', ass);

			dom.XmlNode header = envelope.addChildElement('Header', soapNS, null);
			dom.XmlNode eBondingheader = header.addChildElement('eBondingHeader', ebon, 'ebon');
			dom.XmlNode sender = eBondingheader.addChildElement('sender', ebon, null);
			sender.addChildElement('logicalId', ebon, null).
					addTextNode(logicalId);
			sender.addChildElement('component', ebon, null).
					addTextNode('');
			sender.addChildElement('referenceId', ebon, null).
					addTextNode('');
			sender.addChildElement('confirmation', ebon, null).
					addTextNode('OnError');
			eBondingheader.addChildElement('creationDateTime', ebon, null).
					addTextNode(testTime);
			eBondingheader.addChildElement('messageId', ebon, null).
					addTextNode(encodemessageId);
			dom.XmlNode credentials = eBondingheader.addChildElement('credentials', ebon, null);
			credentials.addChildElement('userName', ebon, null).
					addTextNode('');
			credentials.addChildElement('password', ebon, null).
					addTextNode('');

			dom.XmlNode businessServiceRequest = eBondingheader.addChildElement('businessServiceRequest', ebon, null);
			businessServiceRequest.addChildElement('service', ebon, null).
					addTextNode('AssuranceService');
			businessServiceRequest.addChildElement('operation', ebon, null).
					addTextNode('AddTicketActivity');
			dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
			dom.XmlNode AddTicketActivity = body.addChildElement('AddTicketActivity', ass, 'ass');
			AddTicketActivity.addChildElement('activityTypeCode', ass, null).
					addTextNode('Alert');
			AddTicketActivity.addChildElement('activityCategoryCode', ass, null).
					addTextNode('3rd Party Info Added');
			AddTicketActivity.addChildElement('customerActivityId', ass, null).
					addTextNode(customerActivityId);
			AddTicketActivity.addChildElement('statusCode', ass, null).
					addTextNode(StatusCode);
			AddTicketActivity.addChildElement('assignToWorkgroup', ass, null).
					addTextNode(AssnTWorkGroup);
			AddTicketActivity.addChildElement('activityComments', ass, null).
					addTextNode(description);
			AddTicketActivity.addChildElement('plannedStartDateTime', ass, null).
					addTextNode(plannedStartDateTime);
			AddTicketActivity.addChildElement('customerTicketId', ass, null).
					addTextNode(caseNumber);
			AddTicketActivity.addChildElement('TELUSTicketId', ass, null).
					addTextNode(TELUSTicketId);

			System.debug(doc.toXmlString());

			// TODO: add exception handling

			// Send the request
			HttpRequest req = new HttpRequest();
			req.setMethod('POST');
			req.setEndpoint(endpoint);
			// TODO: specify a timeout of 2 minutes
			// I.E. req.setTimeout(milliseconds);

			String creds = '';
			if (!Test.isRunningTest()) {
				SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username');
				SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');
				if (String.isNotBlank(wsUsername.Value__c) && String.isNotBlank(wsPassword.Value__c))
					creds = wsUsername.Value__c + ':' + wsPassword.Value__c;
			} else {
				creds = 'APP_SFDC:soaorgid';
			}

			String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(creds));

			//Blob headerValue = Blob.valueOf(encodedusernameandpassword);
			String authorizationHeader = 'BASIC ' + encodedusernameandpassword;
			// EncodingUtil.base64Encode(headerValue);
			req.setHeader('Authorization', authorizationHeader);
			req.setHeader('Content-Type', 'text/xml');

			req.setBodyDocument(doc);

			// TODO: Determine the level of detail webservice is returning in regards to errors.

			Http http = new Http();
			HttpResponse res = new HttpResponse();
			if (!Test.isRunningTest()) {
				res = http.send(req);
			} else {
				res = ENTPUtils.mockWebserviceResponse(ENTPUtils.LYNX_TICKET_SEARCH_XML_RESPONSE);
			}
			//System.assertEquals(500, res.getStatusCode());

			System.debug('GetBody line 110:' + res.getBody());
			// System.assertEquals(200, res.getStatusCode());

			dom.Document resDoc = res.getBodyDocument();
			XmlStreamReader reader = res.getXmlStreamReader();

			// Read through the XML
			String OKTicketId = '';
			while (reader.hasNext()) {
				System.debug('Event Type:' + reader.getEventType());
				if (reader.getEventType() == XmlTag.START_ELEMENT) {
					System.debug('getLocalName: ' + reader.getLocalName());
					if ('OK' == (reader.getLocalName())) {
						boolean isSafeToGetNextXmlElement = true;
						while (isSafeToGetNextXmlElement) {

							if (reader.getEventType() == XmlTag.END_ELEMENT) {
								OKTicketId = reader.getLocalName();
								System.debug('OK Ticket Element Line 181 :' + OKTicketId);
								break;
							} else if (reader.getEventType() == XmlTag.CHARACTERS) {
								OKTicketId = reader.getText();
								System.debug('OK Ticket Element Line 185 :' + OKTicketId);
							}
							// Always use hasNext() before calling next() to confirm
							// that we have not reached the end of the stream

							if (reader.hasNext()) {

								reader.next();

							} else {

								isSafeToGetNextXmlElement = false;
								break;
							}
						}

					}
				}
				reader.next();
			}
			if (OKTicketId == null || OKTicketId == '') {
				try {
					Ticket_Event__c NewEvent = new Ticket_Event__c(
							Case_Number__c = caseNumber,
							Event_Type__c = 'New Activity',
							Status__c = 'New',
							Case_Comment_Id__c = customerActivityId,
							Case_Comment__c = description,
							Failure_Reason__c = 'Lynx did not accept activity create',
							Lynx_Ticket_Number__c = TELUSTicketId,
							Case_Id__c = CaseId
					);
					System.debug('LynxUpdateActivity->UpdateActivity, CaseNumber(' + caseNumber + ')');
					insert NewEvent;

				} catch (DmlException e) {
					System.debug('LLynxUpdateActivity->UpdateActivity, exception: ' + e.getMessage());
				}

			}

			envelope = resDoc.getRootElement();
			//  String troubleTick = 'http://www.telus.com/schema/servicestatus/troubleticket';
			//String wsa = 'http://schemas.xmlsoap.org/soap/envelope/';
			//Dom.XMLNode TroubleTicket = resDoc.getRootElement();
			// dom.XmlNode header1 = envelope.getChildElement('Header', soapNS);
			// String TELUSTicketId = TroubleTicket.getChildElement('TELUSTicketId',null).getText();
			//System.debug('TELUSTicketId: ' + TELUSTicketId);
			System.debug(resDoc.toXmlString());
			System.debug('Envelope=:' + envelope);
			//  System.debug('TroubleTicket=:' + TroubleTicket);
			//System.debug(header1);
			//System.debug(testTime);

			return null;
		}

	}

}