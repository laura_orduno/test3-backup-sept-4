/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DatahugCompanyV1 extends datahugapps.BaseController {
    @RemoteAction
    global static Map<String,String> AddContactRoleToOpportunity(String parameterList) {
        return null;
    }
    global static Map<String,String> AddContactRoleToOpportunity(String referrer, String contactId, String emailAddress, String opportuntityId, String jobTitle, String fullName, String pageId, String role, String referrerOption) {
        return null;
    }
    @RemoteAction
    global static Boolean changeContactRole(String parameterList) {
        return null;
    }
    @RemoteAction
    global static Boolean checkNewContactId(String paramList) {
        return null;
    }
    @RemoteAction
    global static datahugapps.ContactResult.CreateContactResult createContactOrLead(String paramList) {
        return null;
    }
    global static Map<String,String> createContact(String paramList) {
        return null;
    }
    global static Map<String,String> createContact(String referrer, String email, String accountId, String jobTitle, String fullName, String pageId, String referrerOption) {
        return null;
    }
    global static String followCompany(String referrer, String companyId) {
        return null;
    }
    global static String followContact(String referrer, String contactId, String accountId, String contactPageId) {
        return null;
    }
    @RemoteAction
    global static datahugapps.ActivityStreamResult getActivity(String paramList) {
        return null;
    }
    @RemoteAction
    global static datahugapps.EmailContentResult getActivityContent(String paramList) {
        return null;
    }
    global static datahugapps.AnalyticsResult getAnalytics(String paramList) {
        return null;
    }
    @RemoteAction
    global static datahugapps.AnalyticsResult getAnalyticsForDateRange(String paramList) {
        return null;
    }
    global static Map<String,Object> getAnalytics(String referrer, String pageId, String domainUrl) {
        return null;
    }
    @RemoteAction
    global static datahugapps.ColleagueHoveroverResult getColleagueHoverovers(String paramList) {
        return null;
    }
    global static datahugapps.ColleagueHoveroverResult getColleagueHoverovers(String referrer, String contactIds) {
        return null;
    }
    global static datahugapps.CompanyResult getCompany(String referrer, String pageId, String accountId, String domainUrl) {
        return null;
    }
    global static datahugapps.ActivityStreamResult getCompanyActivity(String referrer, String domainUrl, String accountOppId) {
        return null;
    }
    global static datahugapps.CalendarHistoryResult getCompanyCalendar(String referrer, String domainUrl, String accountOppId) {
        return null;
    }
    global static datahugapps.CalendarHistoryResult getCompanyHistory(String referrer, String domainUrl, String accountOppId) {
        return null;
    }
    @RemoteAction
    global static datahugapps.ContactRowResult getConnections(String paramList) {
        return null;
    }
    global static datahugapps.ContactRowResult getConnections(String referrer, String pageId, String domainUrl, String connectionType, String sortField, Boolean sortAsc) {
        return null;
    }
    @RemoteAction
    global static List<datahugapps.CompanyResult.DropdownEntry> getContactRolesForOpportunity() {
        return null;
    }
    global static datahugapps.ContactRowResult getContacts(String referrer, String pageId, String domainUrl, String connectionType, String sortField, Boolean sortAsc, String searchTerm) {
        return null;
    }
    @RemoteAction
    global static datahugapps.CompanyResult getHeaderInfo(String paramList) {
        return null;
    }
    @RemoteAction
    global static datahugapps.LeadOverviewResult getLeadOverview(String paramList) {
        return null;
    }
    @RemoteAction
    global static datahugapps.CompanyResult getOpportunityOverview(String paramList) {
        return null;
    }
    @RemoteAction
    global static String markContactPrivate(String paramList) {
        return null;
    }
    global static String markContactPrivate(String referrer, String contactId, String accountId, String contactPageId) {
        return null;
    }
    @RemoteAction
    global static String markContactPublic(String paramList) {
        return null;
    }
    global static String markContactPublic(String referrer, String contactId, String accountId, String contactPageId) {
        return null;
    }
    @RemoteAction
    global static void postError(String paramList) {

    }
    global static void postError(String referrer, String log) {

    }
    @RemoteAction
    global static void postLog(String paramList) {

    }
    global static void postLog(String referrer, String log) {

    }
    @RemoteAction
    global static void postMetric(String paramList) {

    }
    global static void postMetric(String referrer, String action) {

    }
    @RemoteAction
    global static void recordNewContactAdding(String paramList) {

    }
    @RemoteAction
    global static Boolean sendColleagueMessage(String paramList) {
        return null;
    }
    global static Boolean sendColleagueMessage(String referrer, String linkUrl, String selectedColleagueId, String contactName, String message) {
        return null;
    }
    @RemoteAction
    global static void setTourAsTaken(String paramList) {

    }
    @RemoteAction
    global static datahugapps.ContactRowResult showMoreContacts(String paramList) {
        return null;
    }
    global static datahugapps.ContactRowResult showMoreContacts(String referrer, String pageId, String path) {
        return null;
    }
    global static String unfollowCompany(String referrer, String companyId) {
        return null;
    }
    global static String unfollowContact(String referrer, String contactId, String accountId, String contactPageId) {
        return null;
    }
}
