public class BillingAdjustmentTriggerHandler{

    public static void handleBeforeInsert(List<Billing_Adjustment__c> balist){
        process(baList,true);
        
    }
        
    public static void handleBeforeUpdate(List<Billing_Adjustment__c> balist){
        BillingAdjustmentDataHelper.oldMap = Trigger.OldMap;
        process(baList,false);
    }
    
    public static void handleAfterUpdate(List<Billing_Adjustment__c> balist){
        applyApexSharing(balist);
    }
    
    public static void handleAfterInsert(List<Billing_Adjustment__c> balist){
        applyApexSharing(balist);
    }
    
    private static void process(List<Billing_Adjustment__c> baList, Boolean restValues){
        for(Billing_Adjustment__c ba : baList){
            BillingAdjustmentDataHelper.addData(ba);
            if(restValues){
                resetValues(ba);
            }
        }
        BillingAdjustmentDataHelper.loadProductApprovalThresholds(baList);
        BillingAdjustmentDataHelper.loadData();
        handleCommonUpdates(baList);
    }
     
    public static void handleCommonUpdates(List<Billing_Adjustment__c> balist){
        List<Approval_Threshold__c> individualThreshold = retrieveIndividualThresholds(balist);

        for(Billing_Adjustment__c ba : baList){
            try{
                if(!BillingAdjustmentDataHelper.isReminderNotificationUpdate(ba)){
          system.debug(LoggingLevel.INFO, 'Approval Status Start ' + ba.Approval_Status__c);
                        
                    BillingAdjustmentHandlerHelper.setReasonCode(ba);
                    BillingAdjustmentHandlerHelper.setParentAccount(ba);
                    BillingAdjustmentHandlerHelper.setRequiredReleaseCode(ba, individualThreshold);
                    BillingAdjustmentHandlerHelper.setInitailNextApprover(ba);
                    BillingAdjustmentHandlerHelper.setEmailNotifySalesPrimeUsers(ba);
                    BillingAdjustmentHandlerHelper.setControllerApprovers(ba);
                    BillingAdjustmentHandlerHelper.setPMRequired(ba);
                    updateBillingTeamEmail(ba);
                    if(BillingAdjustmentDataHelper.isApprovalProcessUpdate(ba)){
                         Billing_Adjustment_Approval_Flow__c flowMatrix = Billing_Adjustment_Approval_Flow__c.getValues(ba.Transaction_Type__c);
                         if(flowMatrix !=null){      
                            if(flowMatrix.Relase_Code_Base_Approval__c==true){
                                ba.Approver_Notifications__c =0;
                                ba.Notified_Next_Approver__c=false;
                                if(ba.Approval_Status__c != 'Pending Queue'){
                                  BillingAdjustmentHandlerHelper.handleApproval(ba);
                                }    
                            }
                        }
                      
                    }
                    BillingAdjustmentHandlerHelper.setNextApproverEmail(ba);
                    BillingAdjustmentHandlerHelper.setPMApproval(ba);
                    BillingAdjustmentHandlerHelper.restoreOriginalOwner(ba);
                }

             }catch(BillingAdjustmentHandlerHelper.BillingAdjustmentException e){
            
             }catch(Exception e){
              ba.addError('Unexpected Error Occurd' + ' '+ e.getMessage()+ '->' + e.getStackTraceString() );                   
             }    
        system.debug(LoggingLevel.INFO, 'Approval Status End ' + ba.Approval_Status__c);
        }

        
    }

    /**
     * retrieveIndividualThresholds
     * @description Retrieves the individual thresholds of the next approver
     * @author Thomas Tran, Traction on Demand
     * @date FEB 18, 2016
     */
    private static List<Approval_Threshold__c> retrieveIndividualThresholds(List<Billing_Adjustment__c> baList){
        Set<Id> nextApproverId = new Set<Id>();
        Set<String> transactionType = new Set<String>();
        Set<String> businessUnit = new Set<String>();
        //Id individualThresholdApprovalId = Schema.SObjectType.Approval_Threshold__c.getRecordTypeInfosByName().get('Individual Threshold').getRecordTypeId();

         for(Billing_Adjustment__c ba : baList){

            if(Trigger.isBefore && Trigger.isInsert){
              if(ba.First_Approver__c =='Adjustment Requester'){
                  nextApproverId.add(ba.Requested_By__c);
              }else if(ba.First_Approver__c =='Other Manager'){
                  nextApproverId.add(ba.Other_Approval_Manager__c);
              }else{
                  User usrMgr = BillingAdjustmentDataHelper.getManger(ba,ba.OwnerId);  
                  if(ba.Approval_Status__c!='Approved'){
                      if(usrMgr != null){
                          nextApproverId.add(usrMgr.id);
                      }
                  }
              }
            } else{
              nextApproverId.add(ba.Next_Approver__c);
            }

            transactionType.add(ba.Transaction_Type__c);
            businessUnit.add(ba.Business_Unit__c);
        }

        List<Approval_Threshold__c> individualThreshold = [SELECT Business_Unit__c, Transaction_Type__c, Individual_User__c, Individual_User_Release_Code__c, Individual_Threshold__c
                                                            FROM Approval_Threshold__c
                                                            WHERE Individual_User__c IN :nextApproverId AND Transaction_Type__c = :transactionType
                                                                    AND Business_Unit__c = :businessUnit AND RecordTypeId IN (SELECT Id FROM RecordType WHERE SObjectType = 'Approval_Threshold__c'
                                                                      AND DeveloperName = 'Individual_Threshold')];

        return individualThreshold;
    }
    
    private static void resetValues(Billing_Adjustment__c ba){
        
        system.debug('reset value trigger');
        
        ba.Approval_Step_Number__c = 0;
        ba.Approval_Status__c = null;
        ba.Status__c = 'Draft';
        ba.Controller_Approved__c=false;
        ba.Controller_Approval_Required__c =false;
        
        ba.Corporate_Controller_Approved__c =false;
        ba.Corporate_Controller_Approval_Required__c =false;
        
        
        ba.VP_Controller_Approved__c = false;
        ba.VP_Controller_Approval_Required__c = false;
        
        ba.Product_Manager_Approved__c = false;
        
        ba.Approver_Notifications__c =0;
        ba.Last_Approver_User__c=null;
        ba.Last_Approval_Date__c=null;
        ba.Original_RecordType__c=null;
        ba.Sales_VP_Engaged__c =false;
        ba.Approver_Release_Code__c =null;
        ba.Next_Hierarchy_Approver__c =null;
        ba.Closed_Status__c ='';
        
    }
    
    private static void updateBillingTeamEmail(Billing_Adjustment__c ba){
        if(BillingAdjustmentDataHelper.hasUpdated(ba,'Billing_Team__c')){
            String billingTeam = ba.Billing_Team__c;
            if(billingTeam.length()>35){
               billingTeam = billingTeam.substring(0,34);
            }
           BillingAdjustmentEmails__c billingTeamEmails = BillingAdjustmentEmails__c.getValues(billingTeam);
            if(billingTeamEmails!=null){
              ba.Billing_Team_Email__c = billingTeamEmails.Email__c;    
            }
            
        }
    }
    
    private static Billing_Adjustment__Share shareWithNextApprover(Billing_Adjustment__c ba){
        if(BillingAdjustmentDataHelper.hasUpdated(ba, 'Next_Approver__c') ){
              Id recordId =ba.id;
              Billing_Adjustment__Share baShare  = new Billing_Adjustment__Share();
              baShare.ParentId = recordId;
              baShare.UserOrGroupId = ba.Next_Approver__c;

              baShare.RowCause = Schema.Billing_Adjustment__Share.RowCause.Manual;
              return baShare;
        }else{
            return null;
        }
        
    }
    
    private static Billing_Adjustment__Share shareWithAccountPrime(Billing_Adjustment__c ba){
        if(BillingAdjustmentDataHelper.hasUpdated(ba, 'Account__c') ){
              Id recordId =ba.id;
              Billing_Adjustment__Share baShare  = new Billing_Adjustment__Share();
              baShare.ParentId = recordId;
              baShare.UserOrGroupId = BillingAdjustmentDataHelper.getSalesAssimnetUserIdByRole(ba, 'ACCTPRIME');
              baShare.AccessLevel = 'Edit';
              baShare.RowCause = Schema.Billing_Adjustment__Share.RowCause.Manual;
              return baShare;
        }else{
            return null;
        }
        
    }
    
    public static void applyApexSharing(List<Billing_Adjustment__c> baList){
       List<SObject> shareOBjList = new List<SObject>();
        for(Billing_Adjustment__c ba : baList){
            SObject  obj = shareWithNextApprover(ba);
            if(obj!=null)
                shareOBjList.add(obj);
            Sobject accPrimeShare = shareWithAccountPrime(ba);
            if(accPrimeShare!=null)
                shareOBjList.add(accPrimeShare);
        }
        if(shareOBjList.size()>0){
           Database.insert(shareOBjList,false); 
        }
            
    }
    
    
}