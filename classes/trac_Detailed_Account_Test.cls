@isTest(seeAllData = false)
public with sharing class trac_Detailed_Account_Test {
	public static testMethod void test_Trac_Detailed_Account(){
		Account a = new Account(name = 'test account', 
									Dealer_Account__c = false,
									Client_s_information_verified_on__c = System.now().date(),
									//LastModifiedDate = System.now().addDays(-1),
									Wireline_Escalation_Timestamp__c = System.now());
		insert a;
		
		trac_Detailed_Account testDetailedAccount = new trac_Detailed_Account(a.Id);
		testDetailedAccount.record = null;
		testDetailedAccount.getAccount();		
	}
}