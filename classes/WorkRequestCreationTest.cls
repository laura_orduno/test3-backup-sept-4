@isTest
private class WorkRequestCreationTest {
    @isTest
    private static void testInvokeMethod() { 
        Map<String, Object> output   = new Map<String,Object>();
        Map<String, Object> options  = new Map<String,object>(); 
        Map<String, Object> input    = new Map<String,Object>();
        Map<String, String> inputval = new Map<String,String>(); 
        Test.startTest(); 
        
        Pricebook2 standardBook =  new Pricebook2(Id = Test.getStandardPricebookId(), Name = 'TestPricebook', IsActive = true);
        Product2 product1 = new Product2(Name='TELUS Public Wi-Fi',ProductCode='PROD-002', Description='This is a test JSON Product 1 Generator',isActive = true,orderMgmtId__c='9143367435013077174' ,OCOM_Bookable__c='Yes');
        insert product1;
        Product2 product3 = new Product2(Name='TELUS Connectivity',ProductCode='PROD-004', Description='This is a test JSON Product 2',  IsActive = true,orderMgmtId__c='9143863905013553519');
        product3.vlocity_cmt__JSONAttribute__c = '{"TELUSCHAR": [{"$$AttributeDefinitionStart$$": null, "objectid__c": "01t40000004X6uZAAS", "attributeid__c": "a7R220000004CUaEAM", "attributecategoryid__c": "a7Q2200000000UBEAY", "categorycode__c": "TELUSCHAR", "categoryname__c": "Characteristics", "attributeuniquecode__c": "ATTRIBUTE-096", "attributeconfigurable__c": true, "attributedisplaysequence__c": "18", "attributefilterable__c": false, "isactive__c": true, "attributedisplayname__c": "Offering ID", "displaysequence__c": null, "formatmask__c": null, "hasrule__c": false, "isconfigurable__c": true, "ishidden__c": true, "valueinnumber__c": null, "objecttype__c": "Product2", "querycode__c": null, "isreadonly__c": true, "isquerydriven__c": false, "querylabel__c": null, "id": "a7P2200000001GmEAI", "isrequired__c": true, "rulemessage__c": null, "uidisplaytype__c": "Text", "value__c": "Connectivity", "valuedatatype__c": "Text", "valuedescription__c": null, "attributecloneable__c": false, "customconfiguitemplate__c": null, "categorydisplaysequence__c": 1, "attributeRunTimeInfo": {"dataType": "Text", "uiDisplayType": "Text", "default": "Connectivity"}, "$$AttributeDefinitionEnd$$": null }, {"$$AttributeDefinitionStart$$": null, "objectid__c": "01t40000004X6uZAAS", "attributeid__c": "a7R220000004CW2EAM", "attributecategoryid__c": "a7Q2200000000UBEAY", "categorycode__c": "TELUSCHAR", "categoryname__c": "Characteristics", "attributeuniquecode__c": "ATTRIBUTE-117", "attributeconfigurable__c": true, "attributedisplaysequence__c": "7", "attributefilterable__c": false, "isactive__c": true, "attributedisplayname__c": "Service Designation", "displaysequence__c": null, "formatmask__c": null, "hasrule__c": false, "isconfigurable__c": true, "ishidden__c": false, "valueinnumber__c": null, "objecttype__c": "Product2", "querycode__c": null, "isreadonly__c": false, "isquerydriven__c": false, "querylabel__c": null, "id": "a7P2200000001GGEAY", "isrequired__c": true, "rulemessage__c": null, "uidisplaytype__c": "Dropdown", "value__c": "Regular Consumer", "valuedatatype__c": "Picklist", "valuedescription__c": null, "attributecloneable__c": false, "customconfiguitemplate__c": null, "categorydisplaysequence__c": 1, "attributeRunTimeInfo": {"dataType": "Picklist", "uiDisplayType": "Dropdown", "default": [{"value": "Regular Consumer", "displayText": "Regular Consumer", "id": "9141251142313189262"}], "values": [{"value": "Public Wi-fi", "displayText": "Public Wi-fi", "id": "9143429886113166279"}, {"value": "Regular Consumer", "displayText": "Regular Consumer", "id": "9141251142313189262"}] }, "$$AttributeDefinitionEnd$$": null }, {"$$AttributeDefinitionStart$$": null, "objectid__c": "01t40000004X6uZAAS", "attributeid__c": "a7R220000004CWqEAM", "attributecategoryid__c": "a7Q2200000000UBEAY", "categorycode__c": "TELUSCHAR", "categoryname__c": "Characteristics", "attributeuniquecode__c": "ATTRIBUTE-127", "attributeconfigurable__c": true, "attributedisplaysequence__c": "11", "attributefilterable__c": false, "isactive__c": true, "attributedisplayname__c": "Special Project Code", "displaysequence__c": null, "formatmask__c": null, "hasrule__c": false, "isconfigurable__c": true, "ishidden__c": true, "valueinnumber__c": null, "objecttype__c": "Product2", "querycode__c": null, "isreadonly__c": true, "isquerydriven__c": false, "querylabel__c": null, "id": "a7P220000008YZREA2", "isrequired__c": false, "rulemessage__c": null, "uidisplaytype__c": "Text", "value__c": null, "valuedatatype__c": "Text", "valuedescription__c": null, "attributecloneable__c": false, "customconfiguitemplate__c": null, "categorydisplaysequence__c": 1, "attributeRunTimeInfo": {"dataType": "Text", "uiDisplayType": "Text"}, "$$AttributeDefinitionEnd$$": null }, {"$$AttributeDefinitionStart$$": null, "objectid__c": "01t40000004X6uZAAS", "attributeid__c": "a7R220000004CX5EAM", "attributecategoryid__c": "a7Q2200000000UBEAY", "categorycode__c": "TELUSCHAR", "categoryname__c": "Characteristics", "attributeuniquecode__c": "ATTRIBUTE-131", "attributeconfigurable__c": true, "attributedisplaysequence__c": "14", "attributefilterable__c": false, "isactive__c": true, "attributedisplayname__c": "Provisioning Indicator", "displaysequence__c": null, "formatmask__c": null, "hasrule__c": false, "isconfigurable__c": true, "ishidden__c": true, "valueinnumber__c": null, "objecttype__c": "Product2", "querycode__c": null, "isreadonly__c": true, "isquerydriven__c": false, "querylabel__c": null, "id": "a7P2200000001GHEAY", "isrequired__c": true, "rulemessage__c": null, "uidisplaytype__c": "Dropdown", "value__c": "Copper", "valuedatatype__c": "Picklist", "valuedescription__c": null, "attributecloneable__c": false, "customconfiguitemplate__c": null, "categorydisplaysequence__c": 1, "attributeRunTimeInfo": {"dataType": "Picklist", "uiDisplayType": "Dropdown", "default": [{"value": "Copper", "displayText": "Copper", "id": "9143397710313150194"}], "values": [{"value": "Fibre", "displayText": "Fibre", "id": "9143397710313150193"}, {"value": "Copper", "displayText": "Copper", "id": "9143397710313150194"}] }, "$$AttributeDefinitionEnd$$": null }, {"$$AttributeDefinitionStart$$": null, "objectid__c": "01t40000004X6uZAAS", "attributeid__c": "a7R220000004EXcEAM", "attributecategoryid__c": "a7Q2200000000UBEAY", "categorycode__c": "TELUSCHAR", "categoryname__c": "Characteristics", "attributeuniquecode__c": "ATTRIBUTE-454", "attributeconfigurable__c": true, "attributedisplaysequence__c": "20", "attributefilterable__c": false, "isactive__c": true, "attributedisplayname__c": "Connectivity ID", "displaysequence__c": null, "formatmask__c": null, "hasrule__c": false, "isconfigurable__c": true, "ishidden__c": false, "valueinnumber__c": null, "objecttype__c": "Product2", "querycode__c": null, "isreadonly__c": true, "isquerydriven__c": false, "querylabel__c": null, "id": "a7P2200000013tGEAQ", "isrequired__c": false, "rulemessage__c": null, "uidisplaytype__c": "Text", "value__c": null, "valuedatatype__c": "Text", "valuedescription__c": null, "attributecloneable__c": true, "customconfiguitemplate__c": null, "categorydisplaysequence__c": 1, "attributeRunTimeInfo": {"dataType": "Text", "uiDisplayType": "Text"}, "$$AttributeDefinitionEnd$$": null }, {"$$AttributeDefinitionStart$$": null, "objectid__c": "01t40000004X6uZAAS", "attributeid__c": "a7R220000004Ec3EAE", "attributecategoryid__c": "a7Q2200000000UBEAY", "categorycode__c": "TELUSCHAR", "categoryname__c": "Characteristics", "attributeuniquecode__c": "ATTRIBUTE-511", "attributeconfigurable__c": true, "attributedisplaysequence__c": "21", "attributefilterable__c": false, "isactive__c": true, "attributedisplayname__c": "Technology Type", "displaysequence__c": null, "formatmask__c": null, "hasrule__c": false, "isconfigurable__c": true, "ishidden__c": false, "valueinnumber__c": null, "objecttype__c": "Product2", "querycode__c": null, "isreadonly__c": false, "isquerydriven__c": false, "querylabel__c": null, "id": "a7P2200000016UYEAY", "isrequired__c": true, "rulemessage__c": null, "uidisplaytype__c": "Dropdown", "value__c": "Copper", "valuedatatype__c": "Picklist", "valuedescription__c": null, "attributecloneable__c": true, "customconfiguitemplate__c": null, "categorydisplaysequence__c": 1, "attributeRunTimeInfo": {"dataType": "Picklist", "uiDisplayType": "Dropdown", "default": [{"value": "Copper", "displayText": "Copper", "id": "9146035525513915783"}], "values": [{"value": "Copper", "displayText": "Copper", "id": "9146035525513915783"}, {"value": "Fibre", "displayText": "Fibre", "id": "9146035525513915782"}] }, "$$AttributeDefinitionEnd$$": null }] }';
        insert product3; 
        Pricebook2 customPriceBook =  new Pricebook2( Name = 'OCOM PriceBook', IsActive = true);
        insert customPriceBook; 
        PricebookEntry telus_conn_Std = new PricebookEntry(Pricebook2Id = standardBook.Id,
                                                           Product2Id = product3.Id, UnitPrice = 10, vlocity_cmt__RecurringPrice__c = 5, IsActive = true, UseStandardPrice = false);
        insert telus_conn_Std;
        PricebookEntry telus_conn_custom = new PricebookEntry(Pricebook2Id = customPriceBook.Id,
                                                              Product2Id = product3.Id, UnitPrice = 2, Vlocity_cmt__RecurringPrice__c = 17, IsActive = true, UseStandardPrice = false);
        insert telus_conn_custom;
        
        PricebookEntry pbwifi_std = new PricebookEntry(Pricebook2Id = standardBook.Id,
                                                       Product2Id = product1.Id, UnitPrice = 10, vlocity_cmt__RecurringPrice__c = 5, IsActive = true, UseStandardPrice = false);
        
        insert pbwifi_std;
        PricebookEntry pbwifi_custom = new PricebookEntry(Pricebook2Id = customPriceBook.Id,
                                                          Product2Id = product1.Id, UnitPrice = 20, Vlocity_cmt__RecurringPrice__c = 17, IsActive = true, UseStandardPrice = false);
        insert pbwifi_custom;
        
        Account testAccount = new Account(Name = 'Test Account');
        insert testAccount;
        
        SMBCare_Address__c address = new SMBCare_Address__c(Account__c=testAccount.Id,
                                                            address_type__c= OrdrConstants.ADDRESS_TYPE_STANDARD, Suite_Number__c='10T2', 
                                                            Street_Number__c='3777', Street_Name__c='Kingsway', 
                                                            Street_Type_Code__c='ST', City__c='Burnaby', Province__c='BC', 
                                                            Postal_Code__c='V5H 3Z7', FMS_Address_ID__c='1919282', SRS_Service_Location__c='3243244',Location_Id__c='1234',Municipality_Name__c='Test muncipality',Maximum_Speed__c='50 Mbps');
        insert address;
        
        Order testOrder = new Order(Name = 'Test Order', Service_Address__c=address.Id,AccountId = testAccount.Id , EffectiveDate = System.today(), status = 'Draft', Pricebook2Id =  customPriceBook.Id);
        insert testOrder;
        
        
        Id contractReqRecId = Contracts__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('eContract').getRecordTypeId();
        Id contractRecId = Contract.SObjectType.getDescribe().getRecordTypeInfosByName().get('Corporate Wireless').getRecordTypeId();
        Id contractLineRecordId= vlocity_cmt__ContractLineItem__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('eContract Line Item').getRecordTypeId();
        Id rcidAccntRecordTypeId=Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        
        Account acct = new Account(Name='testacct', Phone='9222022768', RecordTypeId=rcidAccntRecordTypeId);
        insert acct;
        Contact testContactObj = new Contact(firstname = 'TestConFirstName', LastName = 'TestConLastname', accountId = acct.Id , 
                                             Phone = '123456' , fax = '1234567', Email = 'test@gmail.com');
        insert testContactObj;
        Opportunity opp = new Opportunity(AccountId=acct.id, Name='testopp', CloseDate=system.today(),Probability=0,StageName='test stage');
        insert opp;        
        Contracts__c ConReq= new Contracts__c(RecordTypeId=contractReqRecId,Contract_Type__c='New',Opportunity__c=opp.id );
        insert ConReq;   
        Contract testContractObj = new Contract(Complex_Fulfillment_Contract__c=ConReq.id,Status='Draft',Contract_Type__c='Amendment',
                                                AccountId=acct.id,ContractTerm=12,StartDate=system.today(),
                                                Customer_Signor__c=testContactObj.id,TELUS_Signor__c=userinfo.getuserid(), Agreement_Type__c = 'Amendment',vlocity_cmt__OrderId__c = testOrder.Id);
        insert testContractObj;
        
        inputval.put('ContextId',testOrder.id);
        inputval.put('ContractId',testContractObj.id);
        inputval.put('ContractRequestID',ConReq.id);
        input.put('workRequestParam',inputVal);

        WorkRequestCreation wqc = new WorkRequestCreation(); 
        wqc.invokeMethod('createWorkRequest',input,output,options);
        wqc.invokeMethod('GetWorkRequestsOfOrder',input,output,options);
        Group ownerGroup = [select Id from group where DeveloperName = 'SCA_Support' limit 1];  
        
        Profile p = [select id from profile where name='Standard User'];
        User testUser = new User(alias = 'test123', email='ocom_test123qwert@telus.com', emailencodingkey='UTF-8', 
                                 languagelocalekey='en_US', localesidkey='en_US', profileid = p.Id, 
                                 country='Canada', timezonesidkey='America/Los_Angeles', 
                                 username='ocom_test123qwert@telus.com',lastname=ownerGroup.id);
        insert testUser;
        User u = [Select id, Name from User LIMIT 1];        
        wqc.createTaskNotify(input,output,options); 
        
        list<work_request__C> wlst = [select id,contract__c from work_request__C ]; 
        list<work_request__C> updatewlst  = new list<work_request__C>(); 
        for(work_request__C w : wlst){
            w.status__c = 'Closed';
            w.contract__c = ConReq.id;
            updatewlst.add(w);
        }
        update updatewlst; 
        
        wqc.invokeMethod('createWorkRequest',input,output,options);
        
        Test.stopTest(); 
        
    }
}