/**
 * Base class for validators that use the Product Rule object.
 * 
 * @author Max Rudman
 * @since 4/9/2011
 */
public abstract class AbstractProductRuleValidator extends RuleProcessorSupport {
    public Map<Id,SBQQ__SummaryVariable__c> referencedVariables {get; private set;}
    
    protected List<SBQQ__ProductRule__c> activeRules;
    protected Boolean initialized = false;
    protected SummaryVariableCalculator variableCalculator;
    protected Set<String> testedFields = new Set<String>();
    protected SObject quote;
    
    public AbstractProductRuleValidator(SObject quote) {
    	this.quote = quote;
    }
    
    protected virtual void init() {
        Set<Id> referencedVariableIds = new Set<Id>();
        activeRules = 
            [SELECT Name, SBQQ__ConditionsMet__c, SBQQ__ErrorMessage__c,
                (SELECT SBQQ__TestedVariable__c, SBQQ__TestedField__c, SBQQ__Operator__c, SBQQ__FilterType__c, SBQQ__FilterVariable__c, SBQQ__FilterValue__c FROM SBQQ__ErrorConditions__r),
                (SELECT SBQQ__Product__c, SBQQ__FilterField__c, SBQQ__Operator__c, SBQQ__FilterValue__c, SBQQ__Required__c FROM SBQQ__Actions__r)
             FROM SBQQ__ProductRule__c WHERE Id IN :loadRuleIds()];
        for (SBQQ__ProductRule__c rule : activeRules) {
            for (SBQQ__ErrorCondition__c cond : rule.SBQQ__ErrorConditions__r) {
            	if (cond.SBQQ__TestedVariable__c != null) {
            		referencedVariableIds.add(cond.SBQQ__TestedVariable__c);
            	} else if (cond.SBQQ__TestedField__c != null) {
            		Schema.SObjectField tfield = AliasedMetaDataUtils.getField(quote.getSObjectType(), cond.SBQQ__TestedField__c);
            		if (tfield != null) {
            			testedFields.add(tfield.getDescribe().getName());
            		}
            	}
                if (cond.SBQQ__FilterVariable__c != null) {
                    referencedVariableIds.add(cond.SBQQ__FilterVariable__c);
                }
            }
        }
        
        referencedVariables = new Map<Id,SBQQ__SummaryVariable__c>(loadVariables(referencedVariableIds));
        quote = loadQuote();
        variableCalculator = new SummaryVariableCalculator(referencedVariables.values());
        variableCalculator.calculate(quote, loadTargetRecords());
    }
    
    private SBQQ__SummaryVariable__c[] loadVariables(Set<Id> varIds) {
    	if (!varIds.isEmpty()) {
    		SummaryVariableDAO dao = new SummaryVariableDAO();
    		return dao.loadByIds(varIds);
    	}
    	return new SBQQ__SummaryVariable__c[]{};
    }
    
    protected Boolean isRuleSatisfied(SBQQ__ProductRule__c rule) {
        Boolean errors = false;
        for (SBQQ__ErrorCondition__c cond : rule.SBQQ__ErrorConditions__r) {
            Object testedValue = null;
            if (cond.SBQQ__TestedField__c != null) {
            	Schema.SObjectField tfield = AliasedMetaDataUtils.getField(SBQQ__Quote__c.sObjectType, cond.SBQQ__TestedField__c);
            	if (tfield == null) {
            		continue;
            	}
            	testedValue = quote.get(tfield.getDescribe().getName());
            } else if (cond.SBQQ__TestedVariable__c != null) {
            	testedValue = getCombinedValue(cond.SBQQ__TestedVariable__c);
            } else {
            	continue;
            }
            Object filterValue = cond.SBQQ__FilterValue__c;
            if (cond.SBQQ__FilterType__c == 'Variable') {
                filterValue = getCombinedValue(cond.SBQQ__FilterVariable__c);
            }
            Operators.Logical op = Operators.getInstance(cond.SBQQ__Operator__c);
            if (op == null) {
                throw new ConfigurationException('Invalid operator: ' + cond.SBQQ__Operator__c + ' [' + cond.Id + ']');
            }
            Boolean result = op.evaluate(testedValue, filterValue);
            if (!result && (rule.SBQQ__ConditionsMet__c == 'All')) {
                // One false error condition in "AND" rule means the rule is satisfied
                return true;
            } else if ((result) && (rule.SBQQ__ConditionsMet__c == 'Any')) {
                // One true error condition in "OR" rule means the rule is unsatisfied
                return false;
            } else {
                errors = errors || result;
            }
        }
        return !errors;
    }
    
    public Decimal getCombinedValue(Id varId) {
        Decimal value = variableCalculator.getResult(varId);
        SBQQ__SummaryVariable__c var = referencedVariables.get(varId);
        if ((var != null) && (var.SBQQ__CombineWith__c != null)) {
            value = QuoteUtils.addAsZero(value, getCombinedValue(var.SBQQ__CombineWith__c));
        }
        return value;
    }
    
    public String[] validate() {
        if (!initialized) {
            init();
        }
        
        List<String> messages = new List<String>();
        for (SBQQ__ProductRule__c rule : activeRules) {
            if (!isRuleSatisfied(rule)) {
                messages.add(rule.SBQQ__ErrorMessage__c);
            }
        }
        
        return messages;
    }
    
    public abstract Set<Id> loadRuleIds();
    
    public abstract List<SObject> loadTargetRecords();
    
    public virtual SObject loadQuote() {
        return new SBQQ__Quote__c();
    }
    
    public class ConfigurationException extends Exception {}
}