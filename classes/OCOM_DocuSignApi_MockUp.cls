@isTest
global class OCOM_DocuSignApi_MockUp implements WebServiceMock {
global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
               DocuSignAPI.EnvelopeStatus Obj = new DocuSignAPI.EnvelopeStatus();
               Obj.EnvelopeID = '8fd115ee-43c2-480a-bcd3-008910ec4988';
               DocuSignAPI.CreateAndSendEnvelopeResponse_element MainObj = new DocuSignAPI.CreateAndSendEnvelopeResponse_element();
               MainObj.CreateAndSendEnvelopeResult = Obj;
               response.put('response_x', MainObj);
           }
}