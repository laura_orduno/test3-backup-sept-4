/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class TM_UploadVideoToS3 {
    global TM_UploadVideoToS3() {

    }
    global static List<LRN.UserProgramController.AutoCompleteWrapper> accountLookupSearchNew(String param) {
        return null;
    }
    global static List<Account> accountLookupSearch(String param) {
        return null;
    }
    global static List<LRN.UserProgramController.AutoCompleteWrapper> getUsersNew(String name) {
        return null;
    }
    global static List<User> getUsers(String name) {
        return null;
    }
    global static List<LRN.UserProgramController.AutoCompleteWrapper> opportunityLookupSearchNew(String param) {
        return null;
    }
    global static List<Opportunity> opportunityLookupSearch(String param) {
        return null;
    }
    global static void uploadFile(String attachmentBody, String fileName) {

    }
}
