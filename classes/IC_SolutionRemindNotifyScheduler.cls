global class IC_SolutionRemindNotifyScheduler implements Schedulable{

    
    
    public static String CRON_EXP = '0 58 9 * * ?';
    // Dumber of days which the records has not been changed 
    public static String FOLLOWUP_DAY ='3';
    
     global void execute(SchedulableContext ctx) {
       String query = 'Select id, name, RecordType.Name, Inventory_Item__r.Device_Provider__r.ICS_Notification_Email__c from IC_Solution__c where Status__c = :status and RecordType.Name= :recodTypeName and CreatedDate < LAST_N_DAYS:'+ FOLLOWUP_DAY;
       Database.executeBatch(new IC_SolutionOustandingNotifyBatch(query,'New','Loaner Device'), 5);
     }

}