@isTest
public class AgreementAccountHierarchyControllerTest {

    
    static testMethod void test_pageSize() {
        System.debug('Starting PageSize()');
        smb_Application_Settings__c setting = smb_Application_Settings__c.getInstance('NumAccountsToPreloadInHierarchy');
        
        if (setting == null || setting.value__c == null) {
            system.assert(AgreementAccountHierarchyController.pageSize == AgreementAccountHierarchyController.DEFAULT_PAGE_SIZE);
        }
        else {
            system.assert(AgreementAccountHierarchyController.pageSize == integer.valueOf(setting.value__c));
        }
		System.debug('End PageSize()');        
    }
    
    static List<Account> readyAccountHierarchy() {
        Map<string, Id> recordTypes = new Map<string, Id>();
        Map<string,schema.RecordtypeInfo> recordTypeInfoMap=Account.sobjecttype.getDescribe().getRecordTypeInfosByName();
        
        for (Schema.RecordTypeInfo recordTypeInfo:recordTypeInfoMap.values()) {
        
            if ((recordTypeInfo.getName().equalsignorecase('CBUCID'))
            || (recordTypeInfo.getName().equalsignorecase('RCID'))
            || (recordTypeInfo.getName().equalsignorecase('CAN')))
           {
                                        
            recordTypes.put(recordTypeInfo.getName(), recordTypeInfo.getRecordTypeId());                               
            }
        }                                      
        List<Account> results = new List<Account>();
        
        Account parent = new Account (
            Name = 'CBUCID',
            RecordTypeId = recordTypes.get('CBUCID'),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            CBUCID__c = '123456'
        );
        
        insert parent;
        
        results.add(parent);
        
        Account child = new Account (
            Name = 'RCID',
            parentId = parent.Id,
            RecordTypeId = recordTypes.get('RCID'),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            RCID__c = '987654'
        );
        
        insert child;
        
        results.add(child);
        
        Account grandChild = new Account (
            Name = 'BAN',
            parentId = child.Id,
            RecordTypeId = recordTypes.get('CAN'),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            CAN__c = '5577993311',
            Billing_System_ID__c='101'
        );
        
        insert grandChild;
        
        results.add(grandChild);
        return results;
        
    }
    
    static testMethod void test_readyAccountHierarchy() {
        System.debug('Starting readyAccountHierarchy()');
        readyAccountHierarchy();
        
        List<Account> accounts = [SELECT Id 
                                  FROM Account];
                                  
        System.assert(Accounts.size() == 3);
        System.debug('End readyAccountHierarchy()');
    }
    
    static testMethod void test_getRootNode_CBUCID() {
        System.debug('Starting getRootNode_CBUCID()');
        List<Account> testAccounts = readyAccountHierarchy();
        
        Id CBUCIDAccountId = testAccounts.get(0).Id;
        Id RcidAccountId = testAccounts.get(1).Id;
        
        AgreementAccountHierarchyController.AccountNode result = AgreementAccountHierarchyController.getRootNode(CBUCIDAccountId);
    }
    
    static testMethod void test_getRootNode_RCID() {
        System.debug('Starting getRootNode_RCID()');
        List<Account> testAccounts = readyAccountHierarchy();
        
        Id RcidAccountId = testAccounts.get(0).Id;
        Id BANAccountId = testAccounts.get(1).Id;
        
        AgreementAccountHierarchyController.AccountNode result = AgreementAccountHierarchyController.getRootNode(RcidAccountId);
    }
    
    static testMethod void test_getChildNodes_RCID() {
        System.debug('Starting test_getChildNodes_RCID()');
        List<Account> testAccounts = readyAccountHierarchy();
        
        Id CBUCIDAccountId = testAccounts.get(0).Id;
        Id RcidAccountId = testAccounts.get(1).Id;
        Id CanAccountId = testAccounts.get(2).Id;
        
        List<AgreementAccountHierarchyController.AccountNode> results = AgreementAccountHierarchyController.getChildNodes(RcidAccountId,0);
        System.debug('End test_getChildNodes_RCID()');
        
    }
   
}