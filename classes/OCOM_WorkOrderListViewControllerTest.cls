/*
    *******************************************************************************************************************************
    Class Name:     OCOM_WorkOrderListViewControllerTest 
    Purpose:        Test code for OCOM_WorkOrderListViewController class        
    Created by:     Umesh Atry - TechMahindra        16-Feb-2017        
    Modified by:    Umesh Atry - TechMahindra        11-March-2017 (Added new condition for On_site value for Dispatch Status pick list & change value "En-Route" to "En_Route" & change value "COMPLETED" to "COMPLETE")
                    
    *******************************************************************************************************************************
*/

@isTest
private class OCOM_WorkOrderListViewControllerTest {
     
    @IsTest
    private static void TestWorkOrderPagination(){ 
        
        Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();

        Account RCIDacc = new Account(name='debs',recordtypeid=recRCIDTypeId);
        Account seracc = new Account(name='123 street',recordtypeid=recSerTypeId,parentid=RCIDacc.id);   
            
        List<Account> acclist= new List<Account>();          
        acclist.add(RCIDacc);        
        acclist.add(seracc );
        insert acclist;
        
        Order ord = new order(Ban__c='draft',accountId=seracc.id,effectivedate=system.today(),status='Not Submitted'); 
        insert ord;        
                
        List<Work_Order__c> work_order_list = new  List<Work_Order__c>();
        
		Work_Order__c wkOrder_1 = new Work_Order__c(Scheduled_Due_Date_Location__c='2016-03-11 08:00:00', status__c = Label.OCOM_WFMReserved,
                                                        Install_Type__c ='FieldWork', Onsite_Contact_HomePhone__c = '647.981.9999', 
                                                        Onsite_Contact_Name__c = 'Test is a test', Onsite_Contact_Email__c = 'test@testcompany.ca', 
                                                        Onsite_Contact_CellPhone__c = '416.999.8888', Scheduled_Datetime__c=System.now().AddDays(1),
                                                        WFM_Number__c='1234567', Dispatch_Status__c = 'OPEN', Time_Slot__c='0800-1200', Order__c = ord.Id);
                                                        
        Work_Order__c wkOrder_2 = new Work_Order__c(Scheduled_Due_Date_Location__c='2016-03-11 08:00:00', status__c = Label.OCOM_WFMReserved,
                                                        Install_Type__c ='FieldWork', Onsite_Contact_HomePhone__c = '647.981.9999',Scheduled_Datetime__c=System.now().AddDays(2), 
                                                        Onsite_Contact_Name__c = 'Test is a test', Onsite_Contact_Email__c = 'test@testcompany.ca', 
                                                        WFM_Number__c='1234567', Dispatch_Status__c = 'TENTATIVE', Time_Slot__c='0800-1200', Order__c = ord.Id); 
                                                        
        Work_Order__c wkOrder_3 = new Work_Order__c(Scheduled_Due_Date_Location__c='2016-03-11 08:00:00', status__c = Label.OCOM_WFMReserved,
                                                        Install_Type__c ='FieldWork', Onsite_Contact_HomePhone__c = '647.981.9999', 
                                                        Onsite_Contact_Name__c = 'Test is a test', Onsite_Contact_Email__c = 'test@testcompany.ca',Scheduled_Datetime__c=System.now().AddDays(4), 
                                                        WFM_Number__c='1234568',Dispatch_Status__c = 'DISPATCHED', Time_Slot__c='1200-1600', Order__c = ord.Id); 
														
		Work_Order__c wkOrder_4 = new Work_Order__c(Scheduled_Due_Date_Location__c='2016-03-11 08:00:00', status__c = Label.OCOM_WFMReserved,
                                                        Install_Type__c ='FieldWork', Onsite_Contact_HomePhone__c = '647.981.9999', 
                                                        Onsite_Contact_Name__c = 'Test is a test', Onsite_Contact_Email__c = 'test@testcompany.ca',Scheduled_Datetime__c=System.now().AddDays(4), 
                                                        WFM_Number__c='1234568',Dispatch_Status__c = 'ACCEPTED', Time_Slot__c='1200-1600', Order__c = ord.Id);                           
		
		Work_Order__c wkOrder_5 = new Work_Order__c(Scheduled_Due_Date_Location__c='2016-03-11 08:00:00', status__c = Label.OCOM_WFMReserved,
                                                        Install_Type__c ='FieldWork', Onsite_Contact_HomePhone__c = '647.981.9999', 
                                                        Onsite_Contact_Name__c = 'Test is a test', Onsite_Contact_Email__c = 'test@testcompany.ca',Scheduled_Datetime__c=System.now().AddDays(4), 
                                                        WFM_Number__c='1234568',Dispatch_Status__c = 'REJECTED', Time_Slot__c='1200-1600', Order__c = ord.Id);                
		
		Work_Order__c wkOrder_6 = new Work_Order__c(Scheduled_Due_Date_Location__c='2016-03-11 08:00:00', status__c = Label.OCOM_WFMReserved,
                                                        Install_Type__c ='FieldWork', Onsite_Contact_HomePhone__c = '647.981.9999', 
                                                        Onsite_Contact_Name__c = 'Test is a test', Onsite_Contact_Email__c = 'test@testcompany.ca',Scheduled_Datetime__c=System.now().AddDays(4), 
                                                        WFM_Number__c='1238111',Dispatch_Status__c = 'COMPLETE', Time_Slot__c='1200-1600', Order__c = ord.Id);
		
		Work_Order__c wkOrder_7 = new Work_Order__c(Scheduled_Due_Date_Location__c='2016-03-11 08:00:00', status__c = Label.OCOM_WFMReserved,
                                                        Install_Type__c ='FieldWork', Onsite_Contact_HomePhone__c = '647.981.9999', 
                                                        Onsite_Contact_Name__c = 'Test is a test', Onsite_Contact_Email__c = 'test@testcompany.ca',Scheduled_Datetime__c=System.now().AddDays(4), 
                                                        WFM_Number__c='1231368',Dispatch_Status__c = 'INCOMPLETE', Time_Slot__c='1200-1600', Order__c = ord.Id);
		
		Work_Order__c wkOrder_8 = new Work_Order__c(Scheduled_Due_Date_Location__c='2016-03-11 08:00:00', status__c = Label.OCOM_WFMReserved,
                                                        Install_Type__c ='FieldWork', Onsite_Contact_HomePhone__c = '647.981.9999', 
                                                        Onsite_Contact_Name__c = 'Test is a test', Onsite_Contact_Email__c = 'test@testcompany.ca',Scheduled_Datetime__c=System.now().AddDays(4), 
                                                        WFM_Number__c='1266568',Dispatch_Status__c = 'CANCELLED', Time_Slot__c='1200-1600', Order__c = ord.Id);                                                                  
		
		Work_Order__c wkOrder_9 = new Work_Order__c(Scheduled_Due_Date_Location__c='2016-03-11 08:00:00', status__c = Label.OCOM_WFMReserved,
                                                        Install_Type__c ='FieldWork', Onsite_Contact_HomePhone__c = '647.981.9999', 
                                                        Onsite_Contact_Name__c = 'Test is a test', Onsite_Contact_Email__c = 'test@testcompany.ca',Scheduled_Datetime__c=System.now().AddDays(4).AddDays(4), 
                                                        WFM_Number__c='1225568',Dispatch_Status__c = 'EN_ROUTE', Time_Slot__c='1200-1600', Order__c = ord.Id);                           
														
		// for testing open status if schedule date is greater then todays date											
		Work_Order__c wkOrder_10 = new Work_Order__c(Scheduled_Due_Date_Location__c='2016-03-11 08:00:00', status__c = Label.OCOM_WFMReserved,
                                                        Install_Type__c ='FieldWork', Onsite_Contact_HomePhone__c = '647.981.9999', 
                                                        Onsite_Contact_Name__c = 'Test is a test', Onsite_Contact_Email__c = 'test@testcompany.ca',Scheduled_Datetime__c=System.now().AddDays(2), 
                                                        WFM_Number__c='1237768',Dispatch_Status__c = 'OPEN', Time_Slot__c='1200-1600', Order__c = ord.Id);
														
		// for testing open Status if schedule date is same as today date									
		Work_Order__c wkOrder_11 = new Work_Order__c(Scheduled_Due_Date_Location__c='2016-03-11 08:00:00', status__c = Label.OCOM_WFMReserved,
                                                        Install_Type__c ='FieldWork', Onsite_Contact_HomePhone__c = '647.981.9999', 
                                                        Onsite_Contact_Name__c = 'Test is a test', Onsite_Contact_Email__c = 'test@testcompany.ca',Scheduled_Datetime__c=System.now(), 
                                                        WFM_Number__c='1235368',Dispatch_Status__c = 'OPEN', Time_Slot__c='1200-1600', Order__c = ord.Id);                           
														
		// for testing TEntative status if schedule date is greater than 1 days
		Work_Order__c wkOrder_12 = new Work_Order__c(Scheduled_Due_Date_Location__c='2016-03-11 08:00:00', status__c = Label.OCOM_WFMReserved,
                                                        Install_Type__c ='FieldWork', Onsite_Contact_HomePhone__c = '647.981.9999', 
                                                        Onsite_Contact_Name__c = 'Test is a test', Onsite_Contact_Email__c = 'test@testcompany.ca',Scheduled_Datetime__c=System.now().AddDays(9), 
                                                        WFM_Number__c='1234568',Dispatch_Status__c = 'TENTATIVE', Time_Slot__c='1200-1600', Order__c = ord.Id);
														
		// for testing TEntative status	if schedule date is same as today date											
		Work_Order__c wkOrder_13 = new Work_Order__c(Scheduled_Due_Date_Location__c='2016-03-11 08:00:00', status__c = Label.OCOM_WFMReserved,
                                                        Install_Type__c ='FieldWork', Onsite_Contact_HomePhone__c = '647.981.9999', 
                                                        Onsite_Contact_Name__c = 'Test is a test', Onsite_Contact_Email__c = 'test@testcompany.ca',Scheduled_Datetime__c=System.now().AddDays(1), 
                                                        WFM_Number__c='12332168',Dispatch_Status__c = 'TENTATIVE', Time_Slot__c='1200-1600', Order__c = ord.Id);                          
														
		Work_Order__c wkOrder_14 = new Work_Order__c(Scheduled_Due_Date_Location__c='2016-03-11 08:00:00', status__c = Label.OCOM_WFMReserved,
                                                        Install_Type__c ='FieldWork', Onsite_Contact_HomePhone__c = '647.981.9999', 
                                                        Onsite_Contact_Name__c = 'Test is a test', Onsite_Contact_Email__c = 'test@testcompany.ca',Scheduled_Datetime__c=System.now(), 
                                                        WFM_Number__c='12332168',Dispatch_Status__c = 'TENTATIVE', Time_Slot__c='1200-1600', Order__c = ord.Id); 
														
		Work_Order__c wkOrder_15 = new Work_Order__c(Scheduled_Due_Date_Location__c='2016-03-11 08:00:00', status__c = Label.OCOM_WFMReserved,
                                                        Install_Type__c ='FieldWork', Onsite_Contact_HomePhone__c = '647.981.9999', 
                                                        Onsite_Contact_Name__c = 'Test is a test', Onsite_Contact_Email__c = 'test@testcompany.ca',Scheduled_Datetime__c=System.now().AddDays(4), 
                                                        WFM_Number__c='1266568',Dispatch_Status__c = 'On_Site', Time_Slot__c='1200-1600', Order__c = ord.Id);
		
		
		work_order_list.add(wkOrder_1);work_order_list.add(wkOrder_2);work_order_list.add(wkOrder_3);
		work_order_list.add(wkOrder_4);work_order_list.add(wkOrder_5);work_order_list.add(wkOrder_6);
		work_order_list.add(wkOrder_7);work_order_list.add(wkOrder_8);work_order_list.add(wkOrder_9);
		work_order_list.add(wkOrder_10);work_order_list.add(wkOrder_11);work_order_list.add(wkOrder_12);
		work_order_list.add(wkOrder_13);work_order_list.add(wkOrder_14);work_order_list.add(wkOrder_15);
		
        insert work_order_list;       
		
        work_order_list = [SELECT ID,Scheduled_Datetime__c from Work_Order__c];        
        System.assertEquals(15, work_order_list.size());
        
        PageReference pageRef = Page.OCOM_WorkOrderListViewPage;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('OrderID', ord.id );
        
        Test.startTest();
        OCOM_WorkOrderListViewController myPageExt = new OCOM_WorkOrderListViewController();
		
        myPageExt.getWorkOrderList();
        Integer totalNoDays = myPageExt.findNoOfWorkingDays(Date.Today(), work_order_list.get(0).Scheduled_Datetime__c.Date());
        system.debug('TottalNoDays = '+totalNoDays);
        myPageExt.currentPage = 1;
        myPageExt.gotoPage();        
        
        myPageExt.Next();
        myPageExt.First();
        myPageExt.Last();
        myPageExt.Previous();
        myPageExt.Cancel();
        
        Integer cp = myPageExt.currentPage;
        
        boolean hn = myPageExt.hasNext;        
		System.assertEquals(false, hn); 
		
        boolean hp = myPageExt.hasPrevious;       
		System.assertEquals(false, hp); 
		
        integer pn = myPageExt.pageNumber;       
		System.assertEquals(1, pn); 
		
        boolean isRecordExist = myPageExt.hasWorkOrders;
		System.assertEquals(true, isRecordExist);         
        
        System.assertEquals(15, myPageExt.workOrderList.size()); 
        myPageExt.deleteWorkOrderId = work_order_list.get(2).id;
        myPageExt.deleteWorkOrder();
        myPageExt.reset();
        
        myPageExt.searchText = '1234';        
        myPageExt.Search();
        
        boolean isRecordNotExist = myPageExt.hasWorkOrders;
		System.assertEquals(true, isRecordNotExist); 
		
        Test.stopTest(); 
    }
}