@isTest
public class OCOM_SaveAndSendOrderPdfControllerTest 
{
    @testSetup
    static void testDataSetup() 
    {
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
    }
    
    @isTest
    private static void OCOM_SaveAndSendOrderPdfControllerConstTest() 
    {
        Order ordObj = [SELECT Id FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        if (ordObj != null) 
        {
            Test.startTest();
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ordObj);
            ApexPages.currentPage().getParameters().put('Id',ordObj.id);
            OCOM_SaveAndSendOrderPdfController cont = new OCOM_SaveAndSendOrderPdfController(sc);
            //cont.pdfHelper.pdfType = Label.Review_Label;
            cont.callSendEmail();
            OCOM_SaveAndSendOrderPdfController pcont = new OCOM_SaveAndSendOrderPdfController(sc,'French');
            Test.stopTest();
            //system.assert(billingAddrCtrl.isCarRequired, true);
        }
    }
    
    
    @isTest
    private static void OCOM_SaveAndSendOrderPdfHelperTest1() 
    {
        Order ordObj = [SELECT Id FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        if (ordObj != null) 
        {
            
            Contact cnt = new Contact(Email ='test@gmail.com', LastName = 'TestLer',Language_Preference__c = 'French');
            insert cnt;
            
            // save a template to SMB_QuoteEmailTemplates__c
            SMB_QuoteEmailTemplates__c smbTemp = new SMB_QuoteEmailTemplates__c(Name = 'Default Quote PDF',TemplateName__c = 'Default Quote PDF');
            insert smbTemp; 
            
            Blob cont = Blob.valueOf('FOR UNIT TEST');
            Attachment att = new Attachment(Name='OR0000128-Review-V1.pdf', parentId = ordObj.id, body = cont);
            insert att;
            
            Test.startTest();
            ordObj.CustomerAuthorizedByID = cnt.id;
            ordObj.CustomerAuthorizedBy = cnt;
            ordObj.CustomerAuthorizedBy.Language_Preference__c = 'English';
            update ordObj;
            OCOM_SaveAndSendOrderPdfHelper helper = new OCOM_SaveAndSendOrderPdfHelper(ordObj.id);
            helper.emailAddress ='test@gmail.com';
            helper.CCEmails = 'test1@gmail.com;test2@gmail.com';
            helper.PdfType =Label.Review_Label;
            helper.choosenLanguage = Label.en_US_Label;
            helper.sendEmail();
            helper.sendEmailWithLang('French');
            Test.stopTest();
            //system.assert(billingAddrCtrl.isCarRequired, true);
        }
    }
    
    @isTest
    private static void OCOM_SaveAndSendOrderPdfHelperTest2() 
    {
        Order ordObj = [SELECT Id FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        if (ordObj != null) 
        {
            
            Contact cnt = new Contact(Email ='test@gmail.com', LastName = 'TestLer',Language_Preference__c = 'English');
            insert cnt;
            
            // save a template to SMB_QuoteEmailTemplates__c
            SMB_QuoteEmailTemplates__c smbTemp = new SMB_QuoteEmailTemplates__c(Name = 'Default Quote PDF',TemplateName__c = 'Default Quote PDF');
            insert smbTemp; 
            
            Blob cont = Blob.valueOf('FOR UNIT TEST');
            Attachment att = new Attachment(Name='OR0000128-Review-V1.pdf', parentId = ordObj.id, body = cont);
            insert att;
            
            Test.startTest();
            ordObj.CustomerAuthorizedByID = cnt.id;
            ordObj.CustomerAuthorizedBy = cnt;
            ordObj.CustomerAuthorizedBy.Language_Preference__c = 'English';
            ordobj.Status = 'Draft';
            ordobj.ordermgmtid__c=null;
            update ordObj;
            OCOM_SaveAndSendOrderPdfHelper helper = new OCOM_SaveAndSendOrderPdfHelper(ordObj.id);
            helper.emailAddress ='test@gmail.com';
            helper.CCEmails = 'test1@gmail.com;test2@gmail.com';
            helper.PdfType ='Confirmation';
            helper.choosenLanguage = Label.fr_Label;
            helper.sendEmail();
            helper.sendEmailWithLang('English');
            helper.SavePDF();
            helper.choosenLanguage = Label.fr_Label;
            helper.SavePDF();
            Test.stopTest();
            //system.assert(billingAddrCtrl.isCarRequired, true);
        }
    }
    
    
    @isTest
    private static void OCOM_SaveAndSendOrderPdfHelperTest3() 
    {
        Order ordObj = [SELECT Id FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        if (ordObj != null) 
        {
            
            Contact cnt = new Contact(Email ='test@gmail.com', LastName = 'TestLer',Language_Preference__c = 'French');
            insert cnt;
            
            // save a template to SMB_QuoteEmailTemplates__c
            //SMB_QuoteEmailTemplates__c smbTemp = new SMB_QuoteEmailTemplates__c(Name = 'Default Quote PDF',TemplateName__c = 'Default Quote PDF');
            //insert smbTemp; 
            
            Blob cont = Blob.valueOf('FOR UNIT TEST');
            Attachment att = new Attachment(Name='OR0000128-Review-V1.pdf', parentId = ordObj.id, body = cont);
            insert att;
            
            Test.startTest();
            ordObj.CustomerAuthorizedByID = cnt.id;
            //ordObj.CustomerAuthorizedBy = cnt;
            //ordObj.CustomerAuthorizedBy.Language_Preference__c = 'English';
            update ordObj;
            OCOM_SaveAndSendOrderPdfHelper helper = new OCOM_SaveAndSendOrderPdfHelper(ordObj.id);
            helper.emailAddress ='test@gmail.com';
            helper.CCEmails = 'test1@gmail.com;test2@gmail.com';
            helper.PdfType ='Review';
            helper.choosenLanguage = Label.fr_Label;
            helper.sendEmail();
            helper.sendEmailWithLang('French');
            helper.openModal();
            helper.hideModal();
            Test.stopTest();
            //system.assert(billingAddrCtrl.isCarRequired, true);
        }
    }
    
    
    @isTest
    private static void OCOM_SaveAndSendOrderPdfHelperTest4() 
    {
        Order ordObj = [SELECT Id FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
        if (ordObj != null) 
        {
            
            Contact cnt = new Contact(Email ='test@gmail.com', LastName = 'TestLer',Language_Preference__c ='English');
            insert cnt;
            
            // save a template to SMB_QuoteEmailTemplates__c
            SMB_QuoteEmailTemplates__c smbTemp = new SMB_QuoteEmailTemplates__c(Name = 'Default Quote PDF',TemplateName__c = 'Default Quote PDF');
            insert smbTemp; 
            
            Blob cont = Blob.valueOf('FOR UNIT TEST');
            Attachment att = new Attachment(Name='OR0000128-Review-V1.pdf', parentId = ordObj.id, body = cont);
            insert att;
            Test.startTest();
            OCOM_SaveAndSendOrderPdfHelper helper = new OCOM_SaveAndSendOrderPdfHelper(ordObj.id);
            ordObj.CustomerAuthorizedByID = cnt.Id;
            //ordObj.CustomerAuthorizedBy = cnt;
            //ordObj.CustomerAuthorizedBy.Language_Preference__c = 'English';
            update ordObj;
            helper.emailAddress ='mks@gmail.com';
            helper.CCEmails = 'test1@gmail.com;test2@gmail.com';
            helper.PdfType = Label.Review_Label ;
            helper.choosenLanguage = Label.en_US_Label;
            helper.sendEmail();
            helper.sendEmailWithLang('English');
            Test.stopTest();
            //system.assert(billingAddrCtrl.isCarRequired, true);
        }
    }
    
    @isTest
    private static void OCOM_SaveAndSendOrderPdfHelperTest5() 
    {
        Order ordObj = [SELECT Id FROM Order WHERE orderMgmtId__c='1234567890' LIMIT 1];
          ordObj.CustomerAuthorizedByID = null;
         update ordObj;
        if (ordObj != null) 
        {
            
            Contact cnt = new Contact(Email ='test@gmail.com', LastName = 'TestLer',Language_Preference__c = 'French');
            insert cnt;
            
            // save a template to SMB_QuoteEmailTemplates__c
            SMB_QuoteEmailTemplates__c smbTemp = new SMB_QuoteEmailTemplates__c(Name = 'Default Quote PDF',TemplateName__c = 'Default Quote PDF');
            insert smbTemp; 
            
            Blob cont = Blob.valueOf('FOR UNIT TEST');
            Attachment att = new Attachment(Name='OR0000128-Review-V1.pdf', parentId = ordObj.id, body = cont);
            insert att;
            
            Test.startTest();
            OCOM_SaveAndSendOrderPdfHelper helper = new OCOM_SaveAndSendOrderPdfHelper(ordObj.id);
          
            //ordObj.CustomerAuthorizedBy = cnt;
            //ordObj.CustomerAuthorizedBy.Language_Preference__c = 'English';
            ordobj.Status = 'Submitted';
            update ordObj;
            helper.emailAddress =null;
            helper.CCEmails = 'test1@gmail.com;test2@gmail.com';
            helper.PdfType =null;
            helper.sendEmail();
            helper.choosenLanguage = Label.en_US_Label;
            helper.sendEmailWithLang('French');
            helper.SavePDF();
            Test.stopTest();
            //system.assert(billingAddrCtrl.isCarRequired, true);
        }
    }
    
    
  
}