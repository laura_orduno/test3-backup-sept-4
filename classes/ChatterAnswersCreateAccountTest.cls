@isTest
private class ChatterAnswersCreateAccountTest {
	static testMethod void validateAccountCreation() {
		String firstName = '';
		
		String lastName = '';
		
		Profile[] p = [SELECT Id FROM Profile WHERE UserType = 'Standard'];
		User[] user = [SELECT Id, Firstname, Lastname FROM User WHERE IsActive = true and ProfileId =: p[0].Id];
		// We cannot create account without a user.
		if (user.size() == 0) { return; }
		firstName = user[0].FirstName;
		
		if(firstName != null){
        firstName = firstName.toLowerCase();
        //system.debug('firstName:='+firstName);
    }
		lastName = user[0].LastName;
		
		if(lastName != null){
		lastName = lastName.toLowerCase();
		//system.debug('lastName:='+lastName);	
		}
        
		String userId = user[0].Id;
		String accountId = new ChatterAnswersRegistration().createAccount(firstName, lastName, userId);
		Account account = [SELECT name, ownerId from Account where Id =: accountId];
        string accountName = account.name;
        if(accountName != null){
         accountName = accountName.toLowerCase();	
        }
       
       // system.debug('accountName:='+accountName);
        System.assertEquals(firstName + ' ' + lastName, accountName);
        System.assertEquals(userId, account.ownerId);
		
		
		//System.assertEquals(firstName + ' ' + lastName, account.name);
       
		
	}
}