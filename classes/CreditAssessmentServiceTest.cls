@isTest
public class CreditAssessmentServiceTest {
    
    public static CreditAssessmentService.BusinessCreditAssessmentService_v1_0_SOAP
        servicePort 
        = new CreditAssessmentService.BusinessCreditAssessmentService_v1_0_SOAP();
    
    public static CreditAssessmentServiceMock
        mockclass
        = new CreditAssessmentServiceMock();
    
    @isTest
    public static void ping(){
        Test.setMock(WebServiceMock.class, mockclass);
        Test.startTest();
        servicePort.ping();        
        Test.StopTest();    
    }
    
    @isTest
    public static void getBusinessCreditAssessmentList(){
        Test.setMock(WebServiceMock.class, mockclass);
        Test.startTest();
        Long[] creditAssessmentIdList;
        CreditAssessmentBusinessTypes.CreditAuditInfo auditInfo;                
        servicePort.getBusinessCreditAssessmentList(creditAssessmentIdList
                                                    ,auditInfo
                                                   );        
        Test.StopTest();    
    }
    
    
    
    @isTest
    public static void attachCreditReportWithCarId(){
        Test.setMock(WebServiceMock.class, mockclass);
        Test.startTest();   
        String externalCarId;
        Long businessCustomerId;
        CreditAssessmentBusinessTypes.BusinessCreditReportRequest businessCreditReportRequest;
        CreditAssessmentBusinessTypes.CreditAuditInfo auditInfo;
        servicePort.attachCreditReportWithCarId(externalCarId
                                       ,businessCustomerId
                                       ,businessCreditReportRequest
                                       ,auditInfo
                                      );        
        Test.StopTest();    
    }
    
    @isTest
    public static void attachCreditReport(){
        Test.setMock(WebServiceMock.class, mockclass);
        Test.startTest();      
        Long businessCustomerId;
        CreditAssessmentBusinessTypes.BusinessCreditReportRequest businessCreditReportRequest;
        CreditAssessmentBusinessTypes.CreditAuditInfo auditInfo;
        servicePort.attachCreditReport(businessCustomerId
                                       ,businessCreditReportRequest
                                       ,auditInfo
                                      );        
        Test.StopTest();    
    }
    
    @isTest
    public static void getCreditReport(){
        Test.setMock(WebServiceMock.class, mockclass);
        Test.startTest(); 
        Long creditReportId;
        CreditAssessmentBusinessTypes.CreditAuditInfo auditInfo;
        servicePort.getCreditReport(creditReportId
                                    ,auditInfo
                                   );        
        Test.StopTest();    
    }
    
    @isTest
    public static void getCreditReportList(){
        Test.setMock(WebServiceMock.class, mockclass);
        Test.startTest(); 
        Long[] creditReportIdList;
        CreditAssessmentBusinessTypes.CreditAuditInfo auditInfo;            
        servicePort.getCreditReportList(creditReportIdList
                                        ,auditInfo
                                       );        
        Test.StopTest();    
    }
    
    @isTest
    public static void performAutoUpdateBusinessCreditProfile(){
        Test.setMock(WebServiceMock.class, mockclass);
        Test.startTest(); 
        String externalCARId;
        CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfile_carBusinessProfile_element carBusinessProfile;
        CreditAssessmentBusinessTypes.CreditAuditInfo auditInfo;
        servicePort.performAutoUpdateBusinessCreditProfile(externalCARId
                                                           ,carBusinessProfile
                                                           ,auditInfo
                                                          );        
        Test.StopTest();    
    }
    
    @isTest
    public static void performBusinessCreditAssessment(){
        Test.setMock(WebServiceMock.class, mockclass);
        Test.startTest(); 
        String externalCARId;
        CreditAssessmentBusinessTypes.BusinessProfile BusinessProfile;
        CreditAssessmentRequestResponse.performBusinessCreditAssessment_orderData_element orderData;
        CreditAssessmentBusinessTypes.CreditAuditInfo auditInfo;
        servicePort.performBusinessCreditAssessment(externalCARId
                                                    ,BusinessProfile
                                                    ,orderData
                                                    ,auditInfo
                                                   );        
        Test.StopTest();    
    }
    
    @isTest
    public static void removeCreditReport(){
        Test.setMock(WebServiceMock.class, mockclass);
        Test.startTest(); 
        String creditReportId;
        CreditAssessmentBusinessTypes.CreditAuditInfo auditInfo;            
        servicePort.removeCreditReport(creditReportId
                                       ,auditInfo
                                      );        
        Test.StopTest();    
    }
    
    
}