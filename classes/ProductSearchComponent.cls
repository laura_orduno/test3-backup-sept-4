public class ProductSearchComponent {

    public static final Set<String> FIELDS_TO_RETRIEVE = new Set<String> {'Id', 'Lead_Time__c', 'Name', 'BPC_1__c', 'BPC_2__c',
         'BPC_3__c', 'Family', 'Product_Family__c', 'ProductCode'};
         
    public static final String ORDER_BY =  ' order by BPC_1__c, BPC_2__c, BPC_3__c, Product_Family__c';
    
    public String searchText {get; set;}
    public String searchOption {get; set;}

    public ProductSearchComponent() {
        searchOption = '';
    }
    
    public List<Product2> searchProducts() {
        
        String criteria = '';
        if (searchOption.equals('Search All') && !Util.isBlank(searchText) ) {
            criteria += ' where  ( BPC_1__c like \'%' + searchText + '%\' or BPC_2__c like \'%' + searchText +'%\' or BPC_3__c like \'%' + searchText + '%\' or Product_Family__c like \'%' + searchText +   '%\' ) And IsActive = true';
        }else if (!searchOption.equals('Search All') && ! Util.isBlank(searchText)) {
            criteria += ' where ' + searchOption + ' like \'%' + searchText + '%\' And IsActive = true';
        } else {
            criteria += ' where IsActive = true';
        }
        System.debug('criteria = ' + criteria);
        String query = 'Select ' + Util.join(FIELDS_TO_RETRIEVE,',',null) + ' from Product2 ' + criteria + ORDER_BY + ' limit 1000';
        System.debug('query = ' + query);

        return Database.query(query);
    }
    
}