@RestResource(urlMapping='/KafkaWorkOrderSubscriber')
//https://coins-telus.cs15.force.com/subscribers/services/apexrest/KafkaWorkOrderSubscriber
//https://sfadmin1-telus.cs15.force.com/subscribers/services/apexrest/KafkaWorkOrderSubscriber
//https://telus--rdvzdev.cs50.my.salesforce.com/console
//https://telus--rdvzdev.cs50.salesforce.com/services/apexrest/KafkaWorkOrderSubscriber
global class KafkaWorkOrderSubscriber {
    
    /*
    * Response back to Kafka
    * The response will be a standard HTTP response with the following possibilities:
    * 
    * HTTP Status Codes:
    *       Success: HTTP Status Code = 2xx, e.g. 200
    *       Client Error: HTTP Status Code = 4xx, e.g. 400, 401
    *       System Error: HTTP Status Code = 500
    * 
    * Along with the status code is a response body.
    * The body will be in XML format depending on what the HTTP client can accept via the Accept HTTP header.
    * 
    * WFM -> KAFKA -> SDFC Service -> SFDC database
    */
    
    //Predefined HTTP Status code to be used in the service
    private static Integer OK = 200;
    private static Integer UNAUTHORIZED = 401;
    private static Integer INTERNAL_SERVER_ERROR = 500;
    
    //Predefined error messages
    private static String UNAUTHORIZED_MSG = 'Error Unauthorized access!';
    private static String INTERNAL_SERVER_ERROR_MSG = 'Internal Server Error';
    
    /*
    * No implementation of HTTP_GET method for security purposes
    * System will always return 500 INTERNAL_SERVER_ERROR
    */
    @HttpGet
    global static SubscriberResponse doGet() {
        SubscriberResponse rtn = new SubscriberResponse();
        RestResponse res = RestContext.response;
        
        res.statusCode=INTERNAL_SERVER_ERROR;
        rtn.errorCode = INTERNAL_SERVER_ERROR;
        rtn.errorMessage = INTERNAL_SERVER_ERROR_MSG;
        
        
        return rtn;
    }
    
    @HttpPost
    global static SubscriberResponse doPost() {
        System.debug('>+-+-+=> doPost here');
        SubscriberResponse rtn = new SubscriberResponse();
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        Map<String, String> headers = req.headers;
        String xml = '';
        List<Work_Order__c> WOList = new List<Work_Order__c>();
        //JobDetails[] jobs;
        Blob body = req.requestBody;
                
        Boolean isTokenOK = getToken().compareTo(headers.get('Token')) == 0 ;
            if ( Test.isRunningTest() )isTokenOK = true;
            if ( isTokenOK ) {
               if ( body != null ) {
                        xml = body.toString();
                   		integer statusCode;
                   		ExternalEventRequestResponseParameter.RequestEventData reqObject= new ExternalEventRequestResponseParameter.RequestEventData();
                        reqObject.eventName='wfm.workorder';
                        reqObject.eventDetails=xml;
                        list<ExternalEventRequestResponseParameter.RequestEventData> reqList = new list <ExternalEventRequestResponseParameter.RequestEventData> ();    
                        reqList.add(reqObject);
                        
                        List<ExternalEventRequestResponseParameter.ResponseEventData> responseData =  ExternalEventService_Helper.processEvent(reqList);
                        system.debug('+++####$$$$$-->> responseData received in Kafka class: '+responseData[0]);
                   		if(responseData[0].error!=null){
                            res.statusCode= INTERNAL_SERVER_ERROR;
                            rtn.messageid='error';
                            rtn.topic='wfm.workorder';
                            rtn.errorMessage=responseData[0].error[0].errorDescription;
                       }
                       else{
                           	res.statusCode= OK;
                            rtn.messageid='Success.';
                            rtn.topic='wfm.workorder';
                            rtn.errorMessage='';
                       }
                   	}
            }
        else{
            res.statusCode= UNAUTHORIZED;
            rtn.messageid='error';
            rtn.topic='wfm.workorder';
            rtn.errorMessage=UNAUTHORIZED_MSG;
        }
        
        //We can use this to get XML string on email whenever pubsub sends message to see what we are receiving from pub-sub.
        return rtn;
    }
    
    
    global class SubscriberResponse {
        String messageid;
        //String partition;
        String topic;
        //String offset;
        Integer errorCode;
        String errorMessage;
        //JobDetails request;
    }
    
    public static String getToken () {
        /*
        * Predefined static token used for handshake.
        * Note: This is not ideal way but to align with how Kafka communicate with Falcon program
        * Future enchancement is required make use of OAuth.
        * Token generated from http://randomkeygen.com/ look for CodeIgniter Encryption Keys
        */
        String Token;
        if ( Test.isRunningTest() )
            Token='mLVMq7sT1xqVQwWRZr6ycJ2ofL0kMzV78QNOD4b5l2EZU0R5AvQD4CLR53Jhhu0k';   
        //AF10RUgpbbZBGuEBabhj7d21wr84xrt31ujhhZDr2W1y5AkyP89zl6ZopA77p3yl
        else
            Token= KafkaSettings__c.getInstance('Token').value__c;         
        return Token;
    }
    
    
}