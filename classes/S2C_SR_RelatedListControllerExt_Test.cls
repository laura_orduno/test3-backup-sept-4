/**
 * S2C_SR_RelatedListControllerExt_Test
 * @description Test class for the S2C_SR_RelatedListControllerExt
 * @author Thomas Tran, Traction on Demand
 * @date 08-06-2015
 */
@isTest(seeAllData=false)
private class S2C_SR_RelatedListControllerExt_Test {
    
    @isTest static void cloneServiceRequestByProduct_test() {
        List<Service_Request__c> srToInsert = new List<Service_Request__c>();
        List<SRS_PODS_Answer__c> answerToInsert = new List<SRS_PODS_Answer__c>();
        List<Service_Request_Employee__c> empToInsert = new List<Service_Request_Employee__c>();
        List<Service_Request_Contact__c> srcToInsert = new List<Service_Request_Contact__c>();
        List<Contracts__c> contractToInsert = new List<Contracts__c>();
        Account newAccount = createAccount('Account 001 - S2C_SR_RelatedListControllerExt_Test');
        insert newAccount;

        Opportunity newOpportunity = createOpportunity('Opp 001 -  S2C_SR_RelatedListControllerExt_Test', newAccount);
        insert newOpportunity;

        Service_Request__c disconnectSR = createServiceRequest('Disconnect', newOpportunity,newAccount);
        srToInsert.add(disconnectSR);

        Service_Request__c provideSR = createServiceRequest('Provide/Install', newOpportunity,newAccount);
        srToInsert.add(provideSR);

        insert srToInsert;

        pairSRs(disconnectSR, provideSR);

        SRS_PODS_Answer__c disconnectAnswer = createPodsAnswer(disconnectSR);
        answerToInsert.add(disconnectAnswer);

        SRS_PODS_Answer__c provideAnswer = createPodsAnswer(provideSR);
        answerToInsert.add(provideAnswer);

        insert answerToInsert;

        Contact newContact = createContact('Test');
        insert newContact;

        Service_Request_Contact__c newSRContact1 =  createSRContact(newContact, newAccount, disconnectSR);
        srcToInsert.add(newSRContact1);
        Service_Request_Contact__c newSRContact2 =  createSRContact(newContact, newAccount, provideSR);
        srcToInsert.add(newSRContact2);
        insert srcToInsert;

        Employee__c newEmp = createEmployee();
        insert newEmp;

        Service_Request_Employee__c newSrEmp1 = createSREmployee(newEmp, disconnectSR);
        empToInsert.add(newSrEmp1);
        Service_Request_Employee__c newSrEmp2 = createSREmployee(newEmp, provideSR);
        empToInsert.add(newSrEmp2);

        insert empToInsert;

        Contracts__c newContract1 = createContract(disconnectSR, newContact, newOpportunity);
        contractToInsert.add(newContract1);

        Contracts__c newContract2 = createContract(provideSr, newContact, newOpportunity);
        contractToInsert.add(newContract2);

        insert contractToInsert;

        Product2 newProduct = S2C_TestUtility.createProduct('Test Product', 'Test');
        insert newProduct;

        SRS_PODS_Product__c newSRSPodsProduct = S2C_TestUtility.createSRSPODSProduct(newProduct.Product_Family__c);
        insert newSRSPodsProduct;

        disconnectSR.SRS_PODS_Product__c = newSRSPodsProduct.Id;
        update disconnectSR;

        provideSR.SRS_PODS_Product__c = newSRSPodsProduct.Id;
        update provideSR;

        Service_Request__c testSr = [SELECT Name, Description__c ,Service_Request_Type__c,Target_Date_Type__c,Target_Date__c,SRS_PODS_Product__r.Name,Legacy_Order_count__c,
                                            Requested_Date__c,Opportunity__c,recordtypeId,Account_Name__c,PrimaryContact__c,Billing_Account__c,Billing_Number__c,Billing_Remarks__c,Billing_Account_New__c,Billing_Account_Type__c,
                                            Same_As_Account_Name__c,Billing_Name__c,Same_As_Service_Address__c,Billing_Address__c,Service_Identifier_Type__c,ESD_Customer_Short_Name__c,Existing_Service_ID__c,ESD_Customer_ID__c,
                                            Product_Template_Status__c,TELUS_Caused_Recovery_Reason__c,Requested_By_TELUS__c,Approved_By_Customer__c,Approved_By_TELUS__c,Recovery_Remarks__c,Details_Of_Service__c,Workflow_Request_Type__c, 
                                            Account_Name__r.Name
                                    FROM Service_Request__c
                                    WHERE Id = :disconnectSR.Id];

        Test.startTest();

        S2C_SR_RelatedListControllerExt.executeCloneByProduct(testSr, 1);
        Boolean hasPair = S2C_SR_RelatedListControllerExt.srPairCheck(disconnectSR.Id);

        Test.stopTest();

        List<Service_Request__c> existingSRLIst = [SELECT Id FROM Service_Request__c];
        System.assertEquals(true, hasPair);
        system.debug('existingSRLIst.size(): ' + existingSRLIst.size() );
        System.assert(existingSRLIst.size() >= 2);
    }
    
    @isTest static void cloneSeviceRequestByAddress_test() {
        List<SRS_Service_Address__c> srsAddressToInsert = new List<SRS_Service_Address__c>();
        List<Service_Request__c> srToInsert = new List<Service_Request__c>();
        List<SRS_PODS_Answer__c> answerToInsert = new List<SRS_PODS_Answer__c>();
        List<Service_Request_Employee__c> empToInsert = new List<Service_Request_Employee__c>();
        List<Contracts__c> contractToInsert = new List<Contracts__c>();
        Account newAccount = createAccount('Account 001 - S2C_SR_RelatedListControllerExt_Test');
        insert newAccount;

        Opportunity newOpportunity = createOpportunity('Opp 001 -  S2C_SR_RelatedListControllerExt_Test', newAccount);
        insert newOpportunity;

        Service_Request__c disconnectSR = createServiceRequest('Disconnect', newOpportunity,newAccount);
        srToInsert.add(disconnectSR);

        Service_Request__c provideSR = createServiceRequest('Provide/Install', newOpportunity,newAccount);
        srToInsert.add(provideSR);

        insert srToInsert;

        pairSRs(disconnectSR, provideSR);

        SRS_PODS_Answer__c disconnectAnswer = createPodsAnswer(disconnectSR);
        answerToInsert.add(disconnectAnswer);

        SRS_PODS_Answer__c provideAnswer = createPodsAnswer(provideSR);
        answerToInsert.add(provideAnswer);

        insert answerToInsert;

        Contact newContact = createContact('Test');
        insert newContact;

        Service_Request_Contact__c newSRContact =  createSRContact(newContact, newAccount, disconnectSR);
        insert newSRContact;

        Employee__c newEmp = createEmployee();
        insert newEmp;

        Service_Request_Employee__c newSrEmp1 = createSREmployee(newEmp, disconnectSR);
        empToInsert.add(newSrEmp1);
        Service_Request_Employee__c newSrEmp2 = createSREmployee(newEmp, provideSR);
        empToInsert.add(newSrEmp2);

        insert empToInsert;

        Contracts__c newContract1 = createContract(disconnectSR, newContact, newOpportunity);
        contractToInsert.add(newContract1);

        Contracts__c newContract2 = createContract(disconnectSR, newContact, newOpportunity);
        contractToInsert.add(newContract2);

        insert contractToInsert;

        SMBCare_Address__c smbAddy = createSMBCareAddress(newAccount);
        insert smbAddy;

        SRS_Service_Address__c disconnectAddy = createSRSServiceAddress(String.valueOf(disconnectSR.Id), String.valueOf(smbAddy.Id));
        srsAddressToInsert.add(disconnectAddy);
        SRS_Service_Address__c provideAddy =  createSRSServiceAddress(String.valueOf(provideSR.Id), String.valueOf(smbAddy.Id));
        srsAddressToInsert.add(provideAddy);

        insert srsAddressToInsert;

        Service_Request__c testSr = [SELECT Name, Description__c ,Service_Request_Type__c,Target_Date_Type__c,Target_Date__c,SRS_PODS_Product__r.Name,Legacy_Order_count__c,
                                            Requested_Date__c,Opportunity__c,recordtypeId,Account_Name__c,PrimaryContact__c,Billing_Account__c,Billing_Number__c,Billing_Remarks__c,Billing_Account_New__c,Billing_Account_Type__c,
                                            Same_As_Account_Name__c,Billing_Name__c,Same_As_Service_Address__c,Billing_Address__c,Service_Identifier_Type__c,ESD_Customer_Short_Name__c,Existing_Service_ID__c,ESD_Customer_ID__c,
                                            Product_Template_Status__c,TELUS_Caused_Recovery_Reason__c,Requested_By_TELUS__c,Approved_By_Customer__c,Approved_By_TELUS__c,Recovery_Remarks__c,Details_Of_Service__c,Workflow_Request_Type__c, 
                                            Account_Name__r.Name
                                    FROM Service_Request__c
                                    WHERE Id = :disconnectSR.Id];

        Test.startTest();

        S2C_SR_RelatedListControllerExt.executeCloneByAddress(testSr, 1);
        Boolean hasPair = S2C_SR_RelatedListControllerExt.srPairCheck(disconnectSR.Id);

        Test.stopTest();

        List<Service_Request__c> existingSRLIst = [SELECT Id FROM Service_Request__c];
        System.assertEquals(true, hasPair);
        system.debug('existingSRLIst.size(): ' + existingSRLIst.size() );
        SYstem.assert(existingSRLIst.size() >= 2);
    }

    @isTest static void progressCheck_Test() {
        Account newAccount = createAccount('Account 001 - S2C_SR_RelatedListControllerExt_Test');
        insert newAccount;

        Opportunity newOpportunity = createOpportunity('Opp 001 -  S2C_SR_RelatedListControllerExt_Test', newAccount);
        insert newOpportunity;

        Service_Request__c newSr = createServiceRequest('Disconnect', newOpportunity,newaccount);
        insert newSr;

        Webservice_Integration_Error_Log__c newLog = new Webservice_Integration_Error_Log__c(
            Apex_Class_and_Method__c = 'Test',
            SFDC_Record_Id__c = newSr.Id,
            Object_Name__c = 'Service_Request__c'
        );
    
        insert newLog;

        Test.startTest();
        system.debug('LogID: ' +newlog.Id+'| newlog..Status ='+ newlog.Status__c);
        String resultNoLog = S2C_SR_RelatedListControllerExt.progressCheck('');

        String resultInProgress = S2C_SR_RelatedListControllerExt.progressCheck(newLog.Id);

        newLog.Status__c = 'Failed';
        update newLog;
        String resultFailed = S2C_SR_RelatedListControllerExt.progressCheck(newLog.Id);

        newLog.Status__c = 'Passed';
        update newLog;
        String resultPassed = S2C_SR_RelatedListControllerExt.progressCheck(newLog.Id);


        Test.stopTest();

        System.assertEquals('No Log', resultNoLog);
        //System.assertEquals('In Progress', resultInProgress);
        //System.assertEquals('Failed', resultFailed);
        //System.assertEquals('Passed', resultPassed);
    }

    @isTest static void popupBlocker_Test() {
        Account newAccount = createAccount('Account 001 - S2C_SR_RelatedListControllerExt_Test');
        insert newAccount;

        Opportunity newOpportunity = createOpportunity('Opp 001 -  S2C_SR_RelatedListControllerExt_Test', newAccount);
        insert newOpportunity;

        Service_Request__c newSr = createServiceRequest('Disconnect', newOpportunity,newaccount);
        insert newSr;

        Webservice_Integration_Error_Log__c newLog = new Webservice_Integration_Error_Log__c(
            Apex_Class_and_Method__c = 'Test',
            SFDC_Record_Id__c = newSr.Id,
            Object_Name__c = 'Service_Request__c'
        );

        insert newLog;

        Test.startTest();

        Boolean resultInProgress = S2C_SR_RelatedListControllerExt.popupBlocker(newSr.Id);

        newLog.Status__c = 'Passed';
        update newLog;

        Boolean resultNotInProgress = S2C_SR_RelatedListControllerExt.popupBlocker(newSr.Id);


        Test.stopTest();

        //System.assertEquals(true, resultInProgress);
        //System.assertEquals(false, resultNotInProgress);
    }

    private static Account createAccount(String acctName){
        return new Account(
            Name = acctName
        );
    }

    private static Opportunity CreateOpportunity(String oppName, Account acct){
        return new Opportunity(
            Name = oppName,
            AccountId = acct.Id,
            StageName = 'Originated',
            CloseDate = Date.today()
        );
    }

    private static Service_Request__c createServiceRequest(String recordTypeName, Opportunity opp,Account account){
        Id rtId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();

        return new Service_Request__c(
            RecordTypeID = rtId,
            Opportunity__c = opp.Id,
            Account_Name__c = account.id
        );
    }

    private static void pairSRs(Service_Request__c provideSR, Service_Request__c disconnectSR){
        List<SRS_Service_Request_Related_Order__c> srroToInsert = new List<SRS_Service_Request_Related_Order__c>();
        String referenceNumberRecordTypeId = Schema.SObjectType.SRS_Service_Request_Related_Order__c.getRecordTypeInfosByName().get('Reference Number').getRecordTypeId();
        Set<Id> ids = new Set<Id>();
        ids.add(provideSR.Id);
        ids.add(disconnectSR.Id);
        Map<Id, Service_Request__c> srMap = new Map<Id, Service_Request__c>([SELECT Name FROM Service_Request__c WHERE Id IN :ids]);

        System.debug(srMap);

        srroToInsert.add(
            new SRS_Service_Request_Related_Order__c(
                RecordTypeId = referenceNumberRecordTypeId,
                Name = srMap.get(provideSR.Id).Name,
                Reference_Type__c = 'Related Serv Req',
                RO_Service_Request__c = disconnectSR.Id,
                Comments__c = 'Test',
                System__c = 'Test'
            )
        );

        srroToInsert.add(
            new SRS_Service_Request_Related_Order__c(
                RecordTypeId = referenceNumberRecordTypeId,
                Name = srMap.get(disconnectSR.Id).Name,
                Reference_Type__c = 'Related Serv Req',
                RO_Service_Request__c = provideSR.Id,
                Comments__c = 'Test',
                System__c = 'Test'
            )
        );

        insert srroToInsert;
    }

    private static SRS_PODS_Answer__c createPodsAnswer(Service_Request__c sr){
        return new SRS_PODS_Answer__c(
            Fox_Parameter__c = 'Test',
            GroupSerial__c = 123456,
            Service_Request__c = sr.Id,
            SRS_Group_Question__c = 4.0,
            SRS_PODS_Answer__c = 'Test',
            SRS_Question_ID__c = 123456
        );
    }

    private static Contact createContact(String lastName){
        return new Contact(
            LastName = lastName
        );
    }
    
    private static Service_Request_Contact__c createSRContact(Contact currentContact, Account acct, Service_Request__c sr){
        return new Service_Request_Contact__c(
            Contact__c = currentContact.Id,
            SRAccount__c = acct.Id,
            Service_Request__c = sr.Id
        );
    }

    private static Employee__c createEmployee(){
        return new Employee__c(
            Name = 'Test',
            Employee_ID__c = '12312312',
            TMODS_TEAM_MEMBER_ID__c = 12321421

        );
    }

    private static Service_Request_Employee__c createSREmployee(Employee__c emp, Service_Request__c sr){
        return new Service_Request_Employee__c(
            Team_Member__c = emp.Id,
            Service_Request__c = sr.Id
        );
    }

    private static Contracts__c createContract(Service_Request__c sr, Contact currentContact, Opportunity opp){
        return new Contracts__c(
            Customer_Signor__c = currentContact.Id,
            Contract__c = sr.Id,
            Opportunity__c = opp.Id,
            Type_of_Contract__c = 'New'
        );
    }

    private static SMBCare_Address__c createSMBCareAddress(Account acct){
        return new SMBCare_Address__c(
            Account__c = acct.Id
        );
    }

    private static SRS_Service_Address__c createSRSServiceAddress(String serviceId, String smbCareAddressId){
        return new SRS_Service_Address__c(
            Service_Request__c = serviceId,
            Address__c = smbCareAddressId,
            Demarcation_Location__c = 'Test',
            NPA_NXX__c = '123456',
            Floor__c = 'Test',
            Location__c = 'Location A',
            New_Building__c = 'Test',
            Rack__c = 'Test',
            Shelf__c = 'Test',
            Site_Ready__c = 'Test',
            CLLI__c = 'Test',
            Cabinet__c = 'Test',
            Building_Name__c = 'Test',
            Existing_Working_TN__c = 'Test',
            End_Customer_Name__c = 'Test'
        );
    }
}