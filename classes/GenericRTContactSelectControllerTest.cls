@IsTest
private class GenericRTContactSelectControllerTest
{
    private static List<RecordType> accountRecordTypes = [select Id, Name from RecordType where SObjectType = 'Account'];
    private static Map<String,id> recordTypeMap = new Map<String,id>();
    private static Account acc = null;
    static {
         for(RecordType art : accountRecordTypes){
            recordTypeMap.put(art.Name, art.id);
         }
        acc = createAccount();  
        ApexPages.currentPage().getParameters().put('aid',acc.id);
        ApexPages.currentPage().getParameters().put('targetObjType','Billing_Adjustment__c');
        ApexPages.currentPage().getParameters().put('backurl','https://cs15.salesforce.com/');
     }
    
   @isTest 
   public static void testQueryCustomObjects(){
		Test.setMock(HttpCalloutMock.class, new ToolingAPILightMock(testObjectResponse,testFieldResponse, 200));
        Test.startTest();
        GenericRTContactSelectController c  = new GenericRTContactSelectController();
        System.assertNotEquals(null, c.getRecordTypesList(),'Check Recortype lits is not null');
        Test.stopTest();
        System.assertEquals('Billing Adjustment',c.getTragetObjectName(),'Test getTragetObjectName');
    }
    
    @isTest 
    public static void testShowandClosePopupandGoBack(){
       GenericRTContactSelectController c  = new GenericRTContactSelectController();
       c.showPopup();
       System.assertNotEquals(null, c.new_contact,'Contact should not be null after showpopup');
       c.closePopup();
       System.assertEquals(null, c.new_contact,'Contact should  be null after closepopup');
       System.assertNotEquals(null, c.goback(),'Contact should  be null after closepopup'); 
       c.rcidAccount=null;
       c.showPopup();
       System.assertEquals(c.account.id, c.new_contact.AccountId,'Contact account id should be current account id when no parent account for the provided account'); 
    }
    
    @isTest 
    public static void testcontinueToNext(){
       Test.startTest();
       GenericRTContactSelectController c1  = new GenericRTContactSelectController();
       c1.continueToNext();
       GenericRTContactSelectController c2  = new GenericRTContactSelectController(); 
       ContactSelectFieldMaping__c customSetting = new ContactSelectFieldMaping__c(Name=c2.targetObjType);
       customSetting.Account_Field_Id__c ='Acc Field Id';
       customSetting.Contact_Field_Id__c ='Cont Filed Id';
       insert customSetting;
       c2.initCustFieldIdMap(c2.targetObjType, c2.filedNames); 
       c2.selectedCaseRecordType='Select';
       c2.continueToNext();
       c2.selectedCaseRecordType='RecordType'; 
       c2.selectedContactId=null; 
       c2.continueToNext();
       c2.selectedContactId='Lori Welland : 003e000000HYrjRAAT'; 
       c2.continueToNext(); 
       Test.stopTest(); 
        
    }
   @isTest 
    public static void testinitCustFieldIdMap(){
  		Test.startTest();
        GenericRTContactSelectController c  = new GenericRTContactSelectController();
        ContactSelectFieldMaping__c customSetting = new ContactSelectFieldMaping__c(Name=c.targetObjType);
        customSetting.Account_Field_Id__c ='Acc Field Id';
        customSetting.Contact_Field_Id__c ='Cont Filed Id';
        insert customSetting;
        c.initCustFieldIdMap(c.targetObjType, c.filedNames);
        System.assertEquals(customSetting.Account_Field_Id__c,c.cobjFiedlIdMap.get('Account'),'test account field');
        System.assertEquals(customSetting.Contact_Field_Id__c,c.cobjFiedlIdMap.get('Contact'),'test contact field' );
   		Test.stopTest();
    }
    @isTest 
    public static void testNewContact(){
       GenericRTContactSelectController c  = new GenericRTContactSelectController();
       c.showPopup();
        c.new_contact.LastName = 'Test';
        c.new_contact.FirstName = 'Test';
        c.new_contact.Phone = '4567897788';
       c.saveContact();       
    }
    
    @isTest 
    public static void gettargetObjectLabel(){
       GenericRTContactSelectController c  = new GenericRTContactSelectController();
       System.assertEquals('Billing Adjustment',c.gettargetObjectLabel());
        
              
    }

   //********* MOCK Object ***********
	public class ToolingAPILightMock implements HttpCalloutMock {
		private String testObjectResponse;
        private String testFieldResponse;
		private Integer testStatusCode;

		public ToolingAPILightMock(String testObjectResponse, string testFieldResponse, Integer testStatusCode){
			this.testObjectResponse = testObjectResponse;
            this.testFieldResponse = testFieldResponse;
			this.testStatusCode = testStatusCode;
		}

		public HTTPResponse respond(HTTPRequest req) {
         	HttpResponse res = new HttpResponse();
            String a = 'a';
            if(req.toString().contains('CustomField')){
                 res.setBody(testFieldResponse); 
            }else{
                 res.setBody(testObjectResponse);
            }
            res.setStatusCode(testStatusCode);
			return res;
		}
	}

	private static String testObjectResponse =
		'{' +
		  '"size" : 1,' +
		  '"totalSize" : 1,' +
		  '"done" : true,' +
		  '"records" : [ {' +
		    '"attributes" : {' +
		      '"type" : "CustomObject",' +
		      '"url" : "/services/data/v29.0/tooling/sobjects/CustomObject/01IG00000021cXoMAI"' +
		    '},' +
		    '"Id" : "01IG00000021cXoMAI",' +
		    '"DeveloperName" : "Test"' +
		  '} ],' +
		  '"queryLocator" : null,' +
		  '"entityTypeName" : "CustomEntityDefinition"' +
		'}';

	private static String testFieldResponse =
		'{' +
		  '"size" : 1,' +
		  '"totalSize" : 1,' +
		  '"done" : true,' +
		  '"queryLocator" : null,' +
		  '"records" : [ {' +
		    '"attributes" : {' +
		      '"type" : "CustomField",' +
		      '"url" : "/services/data/v29.0/tooling/sobjects/CustomField/00NG0000009Y0I9MAK"' +
		    '},' +
		    '"DeveloperName" : "A_Number",' +
		    '"Id" : "00NG0000009Y0I9MAK",' +
		    '"FullName" : "01IG00000021cXo.A_Number__c",' +
		    '"TableEnumOrId" : "01IG00000021cXoMAI",' +
		    '"NamespacePrefix" : null' +
		  '},' +
          '{' +
		    '"attributes" : {' +
		      '"type" : "CustomField",' +
		      '"url" : "/services/data/v29.0/tooling/sobjects/CustomField/00NG0000009Y0I9MAK"' +
		    '},' +
		    '"DeveloperName" : "A_Number",' +
		    '"Id" : "00NG0000009Y0I9MAK",' +
		    '"FullName" : "01IG00000021cXo.A_Number__c",' +
		    '"TableEnumOrId" : "01IG00000021cXoMAI",' +
		    '"NamespacePrefix" : null' +
		  '}],' +
		  '"entityTypeName" : "CustomFieldDefinition"' +
		'}';

	private static String testErrorResponse =
		'[{' +
		  '"errorCode" : "INVALID_FIELD",' +
		  '"message" : "message"' +
		'}]';

	private static String testApexClassMemberQueryResponse = '{'
    	+ '"size": 1,' 
    	+ '"totalSize": 1,'
    	+ '"done": true,'
    	+ '"records": [{'
        + '"attributes": {'
        + '    "type": "ApexClassMember",'
        + '    "url": "/services/data/v28.0/tooling/sobjects/ApexClassMember/400G00000005IaoIAE"'
        + '},'
        + '"Id": "400G00000005IaoIAE",'
        + '"Body": "body",'
        + '"Content": "content",'
        + '"ContentEntityId": "01pG0000003ZjfTIAS",'
        + '"LastSyncDate": "2014-01-28T14:51:03.000+0000",'
        + '"Metadata": {'
        + '    "apiVersion": 28.0,'
        + '    "packageVersions": null,'
        + '    "status": "Active",'
        + '    "module": null,'
        + '    "urls": null,'
        + '    "fullName": null'
        + '},'
        + '"MetadataContainerId": "1drG0000000EKF0IAO",'
        + '"SymbolTable": {'
        + '    "tableDeclaration": {'
        + '        "modifiers": [],'
        + '        "name": "ContactExt",'
        + '        "location": {'
        + '            "column": 27,'
        + '            "line": 1'
        + '        },'
        + '        "type": "ContactExt",'
        + '        "references": []'
        + '    },'
        + '    "variables": [{'
        + '        "modifiers": [],'
        + '        "name": "stdController",'
        + '        "location": {'
        + '            "column": 52,'
        + '            "line": 9'
        + '        },'
        + '        "type": "StandardController",'
        + '        "references": [{'
        + '            "column": 30,'
        + '            "line": 10'
        + '        }, {'
        + '            "column": 35,'
        + '            "line": 11'
        + '        }]'
        + '    }],'
        + '    "externalReferences": [],'
        + '    "innerClasses": [],'
        + '    "name": "ContactExt",'
        + '    "constructors": [{'
        + '        "parameters": [{'
        + '            "name": "stdController",'
        + '            "type": "StandardController"'
        + '        }],'
        + '        "visibility": "PUBLIC",'
        + '        "modifiers": [],'
        + '        "name": "ContactExt",'
        + '        "location": {'
        + '            "column": 12,'
        + '            "line": 9'
        + '        },'
        + '        "type": null,'
        + '        "references": []'
        + '    }],'
        + '    "key": "01pG0000003ZjfT",'
        + '    "methods": [{'
        + '        "returnType": "PageReference",'
        + '        "parameters": [],'
        + '        "visibility": "PUBLIC",'
        + '        "modifiers": [],'
        + '        "name": "saveContact",'
        + '        "location": {'
        + '            "column": 26,'
        + '            "line": 14'
        + '        },'
        + '        "type": null,'
        + '        "references": []'
        + '    }, {'
        + '        "returnType": "PageReference",'
        + '        "parameters": [],'
        + '        "visibility": "PUBLIC",'
        + '        "modifiers": [],'
        + '        "name": "cancelChanges",'
        + '        "location": {'
        + '            "column": 26,'
        + '            "line": 19'
        + '        },'
        + '        "type": null,'
        + '        "references": []'
        + '    }],'
        + '    "properties": [{'
        + '        "visibility": "PRIVATE",'
        + '        "modifiers": [],'
        + '        "name": "myContact",'
        + '        "location": {'
        + '            "column": 18,'
        + '            "line": 3'
        + '        },'
        + '        "type": "Contact",'
        + '        "references": [{'
        + '            "column": 14,'
        + '            "line": 11'
        + '        }, {'
        + '            "column": 16,'
        + '            "line": 15'
        + '        }]'
        + '    }, {'
        + '        "visibility": "PRIVATE",'
        + '        "modifiers": [],'
        + '        "name": "stdController",'
        + '        "location": {'
        + '            "column": 42,'
        + '            "line": 4'
        + '        },'
        + '        "type": "StandardController",'
        + '        "references": [{'
        + '            "column": 14,'
        + '            "line": 10'
        + '        }]'
        + '    }],'
        + '    "id": "01pG0000003ZjfT",'
        + '    "namespace": "timesheet"'
        + '}'
        + '}],'
        + '"queryLocator": null,'
        + '"entityTypeName": "ApexClassMember"'
        + '}';
    
   /*
	* Create Basic Parent Account
	*/
    public static Account createParentAccount() { 
        System.debug('createParentAccount - Start');
        Account parentAccount = new Account(Name = 'Test Account-Parent');
        parentAccount.Phone='6047894561';
        parentAccount.RecordTypeId = recordTypeMap.get('RCID');
        insert parentAccount;
        System.debug('Parent Account - ' + parentAccount);
        System.debug('createParentAccount - End');
        return parentAccount;
	}
   /*
	* Create Basic Account (Which has a Parent Account)
	*/
    public static Account createAccount() { 
        System.debug('createAccount - Start');
        Account parentAccout = createParentAccount();
        Account account = new Account(Name = 'Test Account');
        account.RecordTypeId = recordTypeMap.get('CAN');
        account.Phone='6047897777';
        account.parentId = parentAccout.id;
        insert account;
        System.debug('Account - ' + account + 'Account RecordType Id ' + account.RecordTypeId);
        Account a = [Select Name, RecordType.Name from account where id = :account.id];
        System.debug('After insert Account - ' + a + 'Account RecordType Id ' + a.RecordTypeId);
        System.debug('createAccount - End');
        return account;
	}
    /**
     * Create Basic Contact
     */
    public static Contact createContact(id accountId){
        System.debug('createContact - Start');
        Contact contact = new Contact();
        contact.AccountId = accountId;
        contact.FirstName ='Tin';
        contact.LastName ='Tin';
        insert contact;
        System.debug('contact - '+ contact);
        System.debug('createContact - End');
        return contact;
    }
    
}