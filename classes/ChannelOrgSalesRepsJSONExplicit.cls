public class ChannelOrgSalesRepsJSONExplicit {    
    public String firstName {get;set;}
    public String lastName {get;set;}
    public String salesRepPin {get;set;}
    public List<String> salesRepCategoryKeys {get;set;}
        
    public static List<ChannelOrgSalesRepsJSONExplicit> parse(String json) {       
        return (List<ChannelOrgSalesRepsJSONExplicit>) System.JSON.deserialize(json, List<ChannelOrgSalesRepsJSONExplicit>.class);
    }
}