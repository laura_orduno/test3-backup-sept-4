@isTest(seealldata=true)
private class voc_conciergeControllerTest{
    
    static testMethod void testMethodForCodeCoverage(){
        
        List<VOC_Survey_Type__c> voctypes = new List<VOC_Survey_Type__c>();
        VOC_Survey_Type__c voctype1 = new VOC_Survey_Type__c(Requires_Fielded__c = false, Survey_URL__c = 'test', Type__c= 1);
        vocTypes.add(voctype1);
        VOC_Survey_Type__c voctype2 = new VOC_Survey_Type__c(Requires_Fielded__c = true, Survey_URL__c = 'test', Type__c= 2);        
        vocTypes.add(voctype2);
        insert vocTypes;
        
        Schema.DescribeSObjectResult atoken = Schema.sObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recordTypeInfoByName = atoken.getRecordTypeInfosByName();        
        
        List<Account> accountsToInsert = new List<Account>();
        Account rcidacct = new Account(Name='rcid',recordtypeid=recordTypeInfoByName .get('RCID').getRecordTypeId());
        accountsToInsert.add(rcidacct);
        Account rcidacct2 = new Account(Name='dummy',recordtypeid=recordTypeInfoByName .get('RCID').getRecordTypeId());
        accountsToInsert.add(rcidacct2);        
        insert accountsToInsert;

        VOC_Dummy_Account__c vocCStemp = VOC_Dummy_Account__c.getvalues('dummy');
        if(vocCStemp ==null) {
            VOC_Dummy_Account__c vocCS = new VOC_Dummy_Account__c();
            vocCS.name = 'dummy';
            vocCS.Account_Id__c = rcidacct2.id;
            insert vocCS;
        } else {
            vocCStemp.Account_Id__c = rcidacct2.id;
            update vocCStemp;
        }
        
        List<Contact> cons = new List<Contact>();
        Contact rcidcon1 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first',email='test@test.com');
        Contact rcidcon2 = new Contact(LastName='last',accountid=rcidacct.id);
        Contact rcidcon3 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first');                
        cons.add(rcidcon1);
        cons.add(rcidcon2);
        cons.add(rcidcon3);
        insert cons;
        
        List<VOC_Survey__c> vocs = new List<VOC_Survey__c>();
        VOC_Survey__c voc3 = new VOC_Survey__c(
                                    Account_No__c='test',
                                    Agent_ID__c='T876522',
                                    Business_Name__c='test',
                                    Case_Owner__c='test',
                                    Case_Subject__c='test',
                                    Customer_Email__c='test4@test.com',
                                    Customer_First_Name__c='first4',
                                    Customer_Last_Name__c='last4',
                                    Customer_Phone__c='4555555555',
                                    Custom_ID__c='testtest992',
                                    Interaction_Date__c=Date.today(),
                                    Location__c='test',
                                    RCID__c='122323',
                                    Reference__c='test',
                                    Ticket__c='test',
                                    Survey_Type__c=1,
                                    update_manager_emails__c=true,
                                    manager_callback__c=true,
                                    sent__c=true
                                );
        vocs.add(voc3);
        insert vocs;
        
        voc_conciergeController vocc = new voc_conciergeController();
        PageReference ref =new PageReference('http://test.com');
        Test.setCurrentPage(ref);
        ApexPages.currentPage().getParameters().put('vocid',voc3.id);
        vocc.start();
        vocc.complete();
    }
    
    static testMethod void testMethodForMgr(){
        
        List<VOC_Survey_Type__c> voctypes = new List<VOC_Survey_Type__c>();
        VOC_Survey_Type__c voctype1 = new VOC_Survey_Type__c(Requires_Fielded__c = false, Survey_URL__c = 'test', Type__c= 1);
        vocTypes.add(voctype1);
        VOC_Survey_Type__c voctype2 = new VOC_Survey_Type__c(Requires_Fielded__c = true, Survey_URL__c = 'test', Type__c= 2);        
        vocTypes.add(voctype2);
        insert vocTypes;
        
        Schema.DescribeSObjectResult atoken = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recordTypeInfoByName = atoken.getRecordTypeInfosByName();
        
        List<Account> accountsToInsert = new List<Account>();
        Account rcidacct = new Account(Name='rcid',recordtypeid=recordTypeInfoByName.get('RCID').getRecordTypeId());
        accountsToInsert.add(rcidacct);
        Account rcidacct2 = new Account(Name='dummy',recordtypeid=recordTypeInfoByName.get('RCID').getRecordTypeId());
        accountsToInsert.add(rcidacct2);        
        insert accountsToInsert;

        VOC_Dummy_Account__c vocCStemp = VOC_Dummy_Account__c.getvalues('dummy');
        if(vocCStemp ==null) {
            VOC_Dummy_Account__c vocCS = new VOC_Dummy_Account__c();
            vocCS.name = 'dummy';
            vocCS.Account_Id__c = rcidacct2.id;
            insert vocCS;
        } else {
            vocCStemp.Account_Id__c = rcidacct2.id;
            update vocCStemp;
        }
        
        List<Contact> cons = new List<Contact>();
        Contact rcidcon1 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first',email='test@test.com');
        Contact rcidcon2 = new Contact(LastName='last',accountid=rcidacct.id);
        Contact rcidcon3 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first');                
        cons.add(rcidcon1);
        cons.add(rcidcon2);
        cons.add(rcidcon3);
        insert cons;
        
        List<VOC_Survey__c> vocs = new List<VOC_Survey__c>();
        VOC_Survey__c voc3 = new VOC_Survey__c(
                                    Account_No__c='test',
                                    Agent_ID__c='T876522',
                                    Business_Name__c='test',
                                    Case_Owner__c='test',
                                    Case_Subject__c='test',
                                    Customer_Email__c='test4@test.com',
                                    Customer_First_Name__c='first4',
                                    Customer_Last_Name__c='last4',
                                    Customer_Phone__c='4555555555',
                                    Custom_ID__c='testtest992',
                                    Interaction_Date__c=Date.today(),
                                    Location__c='test',
                                    RCID__c='122323',
                                    Reference__c='test',
                                    Ticket__c='test',
                                    Survey_Type__c=1,
                                    update_manager_emails__c=true,
                                    manager_callback__c=true,
                                    sent__c=true
                                );
        vocs.add(voc3);
        insert vocs;
        
        voc_conciergeController vocc = new voc_conciergeController();
        PageReference ref =new PageReference('http://test.com');
        Test.setCurrentPage(ref);
        ApexPages.currentPage().getParameters().put('vocid',voc3.id);
        ApexPages.currentPage().getParameters().put('mgr','1');
        vocc.start();
        vocc.complete();
    }
    
    static testMethod void testMethodForLang(){
        
        List<VOC_Survey_Type__c> voctypes = new List<VOC_Survey_Type__c>();
        VOC_Survey_Type__c voctype1 = new VOC_Survey_Type__c(Requires_Fielded__c = false, Survey_URL__c = 'test', Type__c= 1);
        vocTypes.add(voctype1);
        VOC_Survey_Type__c voctype2 = new VOC_Survey_Type__c(Requires_Fielded__c = true, Survey_URL__c = 'test', Type__c= 2);        
        vocTypes.add(voctype2);
        insert vocTypes;
        
        Schema.DescribeSObjectResult atoken = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recordTypeInfoByName = atoken.getRecordTypeInfosByName();
                
        List<Account> accountsToInsert = new List<Account>();
        Account rcidacct = new Account(Name='rcid',recordtypeid=recordTypeInfoByName.get('RCID').getRecordTypeId());
        accountsToInsert.add(rcidacct);
        Account rcidacct2 = new Account(Name='dummy',recordtypeid=recordTypeInfoByName.get('RCID').getRecordTypeId());
        accountsToInsert.add(rcidacct2);        
        insert accountsToInsert;

        VOC_Dummy_Account__c vocCStemp = VOC_Dummy_Account__c.getvalues('dummy');
        if(vocCStemp ==null) {
            VOC_Dummy_Account__c vocCS = new VOC_Dummy_Account__c();
            vocCS.name = 'dummy';
            vocCS.Account_Id__c = rcidacct2.id;
            insert vocCS;
        } else {
            vocCStemp.Account_Id__c = rcidacct2.id;
            update vocCStemp;
        }
        
        List<Contact> cons = new List<Contact>();
        Contact rcidcon1 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first',email='test@test.com');
        Contact rcidcon2 = new Contact(LastName='last',accountid=rcidacct.id);
        Contact rcidcon3 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first');                
        cons.add(rcidcon1);
        cons.add(rcidcon2);
        cons.add(rcidcon3);
        insert cons;
        
        List<VOC_Survey__c> vocs = new List<VOC_Survey__c>();
        VOC_Survey__c voc3 = new VOC_Survey__c(
                                    Account_No__c='test',
                                    Agent_ID__c='T876522',
                                    Business_Name__c='test',
                                    Case_Owner__c='test',
                                    Case_Subject__c='test',
                                    Customer_Email__c='test4@test.com',
                                    Customer_First_Name__c='first4',
                                    Customer_Last_Name__c='last4',
                                    Customer_Phone__c='4555555555',
                                    Custom_ID__c='testtest992',
                                    Interaction_Date__c=Date.today(),
                                    Location__c='test',
                                    RCID__c='122323',
                                    Reference__c='test',
                                    Ticket__c='test',
                                    Survey_Type__c=1,
                                    update_manager_emails__c=true,
                                    manager_callback__c=true,
                                    sent__c=true
                                );
        vocs.add(voc3);
        insert vocs;
        
        voc_conciergeController vocc = new voc_conciergeController();
        PageReference ref =new PageReference('http://test.com');
        Test.setCurrentPage(ref);
        ApexPages.currentPage().getParameters().put('vocid',voc3.id);
        ApexPages.currentPage().getParameters().put('mgr','1');
        ApexPages.currentPage().getParameters().put('lang','fr');
        vocc.start();
        vocc.complete();
    }
    
    static testMethod void testMethodForLangNoMgr(){
        
        List<VOC_Survey_Type__c> voctypes = new List<VOC_Survey_Type__c>();
        VOC_Survey_Type__c voctype1 = new VOC_Survey_Type__c(Requires_Fielded__c = false, Survey_URL__c = 'test', Type__c= 1);
        vocTypes.add(voctype1);
        VOC_Survey_Type__c voctype2 = new VOC_Survey_Type__c(Requires_Fielded__c = true, Survey_URL__c = 'test', Type__c= 2);        
        vocTypes.add(voctype2);
        insert vocTypes;
        
        Schema.DescribeSObjectResult atoken = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recordTypeInfoByName = atoken.getRecordTypeInfosByName();
                
        List<Account> accountsToInsert = new List<Account>();
        Account rcidacct = new Account(Name='rcid',recordtypeid=recordTypeInfoByName.get('RCID').getRecordTypeId());
        accountsToInsert.add(rcidacct);
        Account rcidacct2 = new Account(Name='dummy',recordtypeid=recordTypeInfoByName.get('RCID').getRecordTypeId());
        accountsToInsert.add(rcidacct2);        
        insert accountsToInsert;

        VOC_Dummy_Account__c vocCStemp = VOC_Dummy_Account__c.getvalues('dummy');
        if(vocCStemp ==null) {
            VOC_Dummy_Account__c vocCS = new VOC_Dummy_Account__c();
            vocCS.name = 'dummy';
            vocCS.Account_Id__c = rcidacct2.id;
            insert vocCS;
        } else {
            vocCStemp.Account_Id__c = rcidacct2.id;
            update vocCStemp;
        }
        
        List<Contact> cons = new List<Contact>();
        Contact rcidcon1 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first',email='test@test.com');
        Contact rcidcon2 = new Contact(LastName='last',accountid=rcidacct.id);
        Contact rcidcon3 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first');                
        cons.add(rcidcon1);
        cons.add(rcidcon2);
        cons.add(rcidcon3);
        insert cons;
        
        List<VOC_Survey__c> vocs = new List<VOC_Survey__c>();
        VOC_Survey__c voc3 = new VOC_Survey__c(
                                    Account_No__c='test',
                                    Agent_ID__c='T876522',
                                    Business_Name__c='test',
                                    Case_Owner__c='test',
                                    Case_Subject__c='test',
                                    Customer_Email__c='test4@test.com',
                                    Customer_First_Name__c='first4',
                                    Customer_Last_Name__c='last4',
                                    Customer_Phone__c='4555555555',
                                    Custom_ID__c='testtest992',
                                    Interaction_Date__c=Date.today(),
                                    Location__c='test',
                                    RCID__c='122323',
                                    Reference__c='test',
                                    Ticket__c='test',
                                    Survey_Type__c=1,
                                    update_manager_emails__c=true,
                                    manager_callback__c=true,
                                    sent__c=true
                                );
        vocs.add(voc3);
        insert vocs;
        
        voc_conciergeController vocc = new voc_conciergeController();
        PageReference ref =new PageReference('http://test.com');
        Test.setCurrentPage(ref);
        ApexPages.currentPage().getParameters().put('vocid',voc3.id);
        ApexPages.currentPage().getParameters().put('lang','fr');
        vocc.start();
        vocc.complete();
    }
    
    static testMethod void testMethodForFielded(){
        
        List<VOC_Survey_Type__c> voctypes = new List<VOC_Survey_Type__c>();
        VOC_Survey_Type__c voctype1 = new VOC_Survey_Type__c(Requires_Fielded__c = false, Survey_URL__c = 'test', Type__c= 1);
        vocTypes.add(voctype1);
        VOC_Survey_Type__c voctype2 = new VOC_Survey_Type__c(Requires_Fielded__c = true, Survey_URL__c = 'test', Type__c= 2);        
        vocTypes.add(voctype2);
        insert vocTypes;
        
        Schema.DescribeSObjectResult atoken = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recordTypeInfoByName = atoken.getRecordTypeInfosByName();
                
        List<Account> accountsToInsert = new List<Account>();
        Account rcidacct = new Account(Name='rcid',recordtypeid=recordTypeInfoByName.get('RCID').getRecordTypeId());
        accountsToInsert.add(rcidacct);
        Account rcidacct2 = new Account(Name='dummy',recordtypeid=recordTypeInfoByName.get('RCID').getRecordTypeId());
        accountsToInsert.add(rcidacct2);        
        insert accountsToInsert;

        VOC_Dummy_Account__c vocCStemp = VOC_Dummy_Account__c.getvalues('dummy');
        if(vocCStemp ==null) {
            VOC_Dummy_Account__c vocCS = new VOC_Dummy_Account__c();
            vocCS.name = 'dummy';
            vocCS.Account_Id__c = rcidacct2.id;
            insert vocCS;
        } else {
            vocCStemp.Account_Id__c = rcidacct2.id;
            update vocCStemp;
        }
        
        List<Contact> cons = new List<Contact>();
        Contact rcidcon1 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first',email='test@test.com');
        Contact rcidcon2 = new Contact(LastName='last',accountid=rcidacct.id);
        Contact rcidcon3 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first');                
        cons.add(rcidcon1);
        cons.add(rcidcon2);
        cons.add(rcidcon3);
        insert cons;
        
        List<VOC_Survey__c> vocs = new List<VOC_Survey__c>();
        VOC_Survey__c voc3 = new VOC_Survey__c(
                                    Account_No__c='test',
                                    Agent_ID__c='T876522',
                                    Business_Name__c='test',
                                    Case_Owner__c='test',
                                    Case_Subject__c='test',
                                    Customer_Email__c='test4@test.com',
                                    Customer_First_Name__c='first4',
                                    Customer_Last_Name__c='last4',
                                    Customer_Phone__c='4555555555',
                                    Custom_ID__c='testtest992',
                                    Interaction_Date__c=Date.today(),
                                    Location__c='test',
                                    RCID__c='122323',
                                    Reference__c='test',
                                    Ticket__c='test',
                                    Survey_Type__c=2,
                                    update_manager_emails__c=true,
                                    manager_callback__c=true,
                                    sent__c=true
                                );
        vocs.add(voc3);
        insert vocs;
        
        voc_conciergeController vocc = new voc_conciergeController();
        PageReference ref =new PageReference('http://test.com');
        Test.setCurrentPage(ref);
        ApexPages.currentPage().getParameters().put('vocid',voc3.id);
        ApexPages.currentPage().getParameters().put('lang','fr');
        vocc.start();
        vocc.complete();
    }
    
    static testMethod void testMethodForCompleteClicked(){
        
        List<VOC_Survey_Type__c> voctypes = new List<VOC_Survey_Type__c>();
        VOC_Survey_Type__c voctype1 = new VOC_Survey_Type__c(Requires_Fielded__c = false, Survey_URL__c = 'test', Type__c= 1);
        vocTypes.add(voctype1);
        VOC_Survey_Type__c voctype2 = new VOC_Survey_Type__c(Requires_Fielded__c = true, Survey_URL__c = 'test', Type__c= 2);        
        vocTypes.add(voctype2);
        insert vocTypes;
        
        Schema.DescribeSObjectResult atoken = Schema.SObjectType.Account;
        Map<String, Schema.RecordTypeInfo> recordTypeInfoByName = atoken.getRecordTypeInfosByName();
                
        List<Account> accountsToInsert = new List<Account>();
        Account rcidacct = new Account(Name='rcid',recordtypeid=recordTypeInfoByName.get('RCID').getRecordTypeId());
        accountsToInsert.add(rcidacct);
        Account rcidacct2 = new Account(Name='dummy',recordtypeid=recordTypeInfoByName.get('RCID').getRecordTypeId());
        accountsToInsert.add(rcidacct2);        
        insert accountsToInsert;

        VOC_Dummy_Account__c vocCStemp = VOC_Dummy_Account__c.getvalues('dummy');
        if(vocCStemp ==null) {
            VOC_Dummy_Account__c vocCS = new VOC_Dummy_Account__c();
            vocCS.name = 'dummy';
            vocCS.Account_Id__c = rcidacct2.id;
            insert vocCS;
        } else {
            vocCStemp.Account_Id__c = rcidacct2.id;
            update vocCStemp;
        }
        
        List<Contact> cons = new List<Contact>();
        Contact rcidcon1 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first',email='test@test.com');
        Contact rcidcon2 = new Contact(LastName='last',accountid=rcidacct.id);
        Contact rcidcon3 = new Contact(LastName='last',accountid=rcidacct.id,firstname='first');                
        cons.add(rcidcon1);
        cons.add(rcidcon2);
        cons.add(rcidcon3);
        insert cons;
        
        List<VOC_Survey__c> vocs = new List<VOC_Survey__c>();
        VOC_Survey__c voc3 = new VOC_Survey__c(
                                    Account_No__c='test',
                                    Agent_ID__c='T876522',
                                    Business_Name__c='test',
                                    Case_Owner__c='test',
                                    Case_Subject__c='test',
                                    Customer_Email__c='test4@test.com',
                                    Customer_First_Name__c='first4',
                                    Customer_Last_Name__c='last4',
                                    Customer_Phone__c='4555555555',
                                    Custom_ID__c='testtest992',
                                    Interaction_Date__c=Date.today(),
                                    Location__c='test',
                                    RCID__c='122323',
                                    Reference__c='test',
                                    Ticket__c='test',
                                    Survey_Type__c=2,
                                    update_manager_emails__c=true,
                                    manager_callback__c=true,
                                    sent__c=true
                                );
        vocs.add(voc3);
        insert vocs;
        
        voc_conciergeController vocc = new voc_conciergeController();
        PageReference ref =new PageReference('http://test.com');
        Test.setCurrentPage(ref);
        ApexPages.currentPage().getParameters().put('vocid',voc3.id);
        ApexPages.currentPage().getParameters().put('lang','fr');
        vocc.complete();
    }
}