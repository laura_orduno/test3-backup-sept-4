@isTest
public class ContractEnvelopeTriggerHandlerTest {
      private static Id createData(){  
        Id contractReqRecId = Contracts__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('eContract').getRecordTypeId();
        Id contractRecId = Contract.SObjectType.getDescribe().getRecordTypeInfosByName().get('Corporate Wireless').getRecordTypeId();
		Id vlContractRecId = Contract.SObjectType.getDescribe().getRecordTypeInfosByName().get('Vlocity Contracts').getRecordTypeId();
		Id baContractRecId = Contract.SObjectType.getDescribe().getRecordTypeInfosByName().get('Business Anywhere').getRecordTypeId();
		
		
        Id contractLineRecordId= vlocity_cmt__ContractLineItem__c.SObjectType.getDescribe().getRecordTypeInfosByName().get('eContract Line Item').getRecordTypeId();
        Id rcidAccntRecordTypeId=Account.SObjectType.getDescribe().getRecordTypeInfosByName().get('RCID').getRecordTypeId();
        Account acct = new Account(Name='testacct', Phone='9222022768', RecordTypeId=rcidAccntRecordTypeId);
        insert acct;
        Contact testContactObj = new Contact(firstname = 'TestConFirstName', LastName = 'TestConLastname', accountId = acct.Id , 
                                             Phone = '123456' , fax = '1234567', Email = 'test@gmail.com');
        insert testContactObj;
        Opportunity opp = new Opportunity(AccountId=acct.id, Name='testopp', CloseDate=system.today(),Probability=0,StageName='test stage');
        insert opp;        
        Contracts__c ConReq= new Contracts__c(RecordTypeId=contractReqRecId,Contract_Type__c='New',Opportunity__c=opp.id );
        insert ConReq;   
        Contract testContractObj = new Contract(Complex_Fulfillment_Contract__c=ConReq.id,Status='Draft',Contract_Type__c='Amendment',
                                                AccountId=acct.id,ContractTerm=12,StartDate=system.today(),
                                                Customer_Signor__c=testContactObj.id,TELUS_Signor__c=userinfo.getuserid(), Agreement_Type__c = 'Amendment',Effective_Date__c=Datetime.now());
        insert testContractObj;
		
        
        Product2 testProductObj = new Product2(Name = 'Laptop X200', 
                                               Family = 'Hardware' ,vlocity_cmt__TrackAsAgreement__c = TRUE, Sellable__c = true);
        insert testProductObj;
        List<vlocity_cmt__ContractLineItem__c> testContractLineItemList =new List<vlocity_cmt__ContractLineItem__c>();
        vlocity_cmt__ContractLineItem__c cli1 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=9,TELUS_Printed_Product_Name__c = testProductObj.Name,vlocity_cmt__Product2Id__c=testProductObj.id,vlocity_cmt__ContractId__c=testContractObj.id,vlocity_cmt__OneTimeTotal__c=12,name='test0',vlocity_cmt__OneTimeCharge__c=2,vlocity_cmt__Quantity__c=12,vlocity_cmt__LineNumber__c = '0001');
        testContractLineItemList.add(cli1);
        
        vlocity_cmt__ContractLineItem__c cli2 = new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=6,vlocity_cmt__Product2Id__c=testProductObj.id,vlocity_cmt__ContractId__c=testContractObj.id,vlocity_cmt__OneTimeTotal__c=6,name='test1',vlocity_cmt__OneTimeCharge__c=3,vlocity_cmt__Quantity__c=12,TELUS_Printed_Product_Name__c = testProductObj.Name,vlocity_cmt__LineNumber__c = '0001.0001');
        
        testContractLineItemList.add(cli2);
        vlocity_cmt__ContractLineItem__c cli3  = new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=3,vlocity_cmt__Product2Id__c=testProductObj.id,vlocity_cmt__ContractId__c=testContractObj.id,vlocity_cmt__OneTimeTotal__c=5,name='test2',vlocity_cmt__OneTimeCharge__c=4,vlocity_cmt__Quantity__c=12,TELUS_Printed_Product_Name__c = testProductObj.Name,vlocity_cmt__LineNumber__c = '0002');
        testContractLineItemList.add(cli3);
        
        vlocity_cmt__ContractLineItem__c cli4 =  new vlocity_cmt__ContractLineItem__c(vlocity_cmt__RecurringTotal__c=3,vlocity_cmt__Product2Id__c=testProductObj.id,vlocity_cmt__ContractId__c=testContractObj.id,vlocity_cmt__OneTimeTotal__c=5,name='test3',vlocity_cmt__OneTimeCharge__c=4,vlocity_cmt__Quantity__c=12,TELUS_Printed_Product_Name__c = testProductObj.Name,vlocity_cmt__LineNumber__c = '0002.0001');
        
        testContractLineItemList.add(cli4);
        insert  testContractLineItemList;
        
       Contract testContractObj1 = new Contract(Complex_Fulfillment_Contract__c=ConReq.id,Status='Draft',Contract_Type__c='Amendment',
                                                AccountId=acct.id,ContractTerm=12,StartDate=system.today(),
                                                Customer_Signor__c=testContactObj.id,TELUS_Signor__c=userinfo.getuserid(), Agreement_Type__c = 'Amendment',Effective_Date__c=Datetime.now());
        insert testContractObj1;
        
		//testContractObj.status='Draft';
		update testContractObj;
		
        testContractObj.InitialContract__c=String.valueOf(testContractObj1.id).substring(0,7);       
		testContractObj.status='Contract Registered';
		testContractObj.vlocity_cmt__OriginalContractId__c=testContractObj1.id;
		update testContractObj;
		testContractObj.recordtypeid=contractRecId;
		testContractObj.InitialContract__c=null;
		testContractObj.Contract_Type__c=null;
		update testContractObj;
		
		testContractObj.recordtypeid=vlContractRecId;
		update testContractObj;
		
		testContractObj.recordtypeid=baContractRecId;
		update testContractObj;
		return testContractObj.id;
		
    }
    @isTest
    private static void test1(){
        Id contractId=createData();
        Test.startTest();
        vlocity_cmt__ContractVersion__c testContractVersionObj = new vlocity_cmt__ContractVersion__c(Name = 'Test', vlocity_cmt__ContractId__c = contractId,vlocity_cmt__Status__c = 'Active');
            insert testContractVersionObj;
        vlocity_cmt__ContractEnvelope__c env=new vlocity_cmt__ContractEnvelope__c();
        env.vlocity_cmt__ContractVersionId__c=testContractVersionObj.id;
        env.vlocity_cmt__ContractId__c=contractId;
        insert env;
        env.vlocity_cmt__DocumentName__c='test name';
        update env;
        Test.stopTest();
    }

}