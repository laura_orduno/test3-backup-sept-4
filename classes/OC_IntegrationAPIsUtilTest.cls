@isTest
private class OC_IntegrationAPIsUtilTest {
    
    private static void createTestData(){
        System.runAs(new User(Id = Userinfo.getUserId())) {
            OrdrTestDataFactory.createOrderingWSCustomSettings('RateGroupInfoEndpoint', 'https://test.test.com');
            OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://test.test.com');
            OrdrTestDataFactory.createOrderingWSCustomSettings('QueryResourceWirelineEndpoint', 'https://test.test.com');
            OrdrTestDataFactory.createOrderingWSCustomSettings('ClearanceCheckEndpoint', 'https://test.test.com');
            
        }
    }
    
    @isTest
    private static void getProductsTechnicalAvailabilityTest(){
        createTestData();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        OC_IntegrationAPIsUtil oc=new OC_IntegrationAPIsUtil();
        String methodName='getProductsTechnicalAvailability';
        Map<String, Object> input=new Map<String, Object>();
        input.put('buildingNumber','buildingNumber'); 
        input.put('coid','coid');
        input.put('fmsId','fmsId');
        input.put('fmsId','fmsId');
        input.put('locationId','locationId');
        input.put('municipalityName','municipalityName');
        input.put('npa_nxx','npa_nxx');   
        input.put('postalCode','postalCode');
        input.put('province','province');      
        input.put('street','street');
        input.put('streetDirection','streetDirection');
        input.put('streetName','streetName');
        input.put('streetName_fms','streetName_fms');
        input.put('streetTypePrefix','streetTypePrefix');
        input.put('streetTypeSuffix','streetTypeSuffix');
        input.put('suiteNumber','suiteNumber');
        Map<String, Object> output=new Map<String, Object>();
        Map<String, Object> options=new Map<String, Object>();
        oc.invokeMethod(methodName,input,output, options);
        Test.stopTest();
        
    }
    @isTest
    private static void getRateGroupInfoTest(){
        createTestData();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        OC_IntegrationAPIsUtil oc=new OC_IntegrationAPIsUtil();
        String methodName='getRateGroupInfo';
        Map<String, Object> input=new Map<String, Object>();
        input.put('npa_nxx','416111'); 
        
        Map<String, Object> output=new Map<String, Object>();
        Map<String, Object> options=new Map<String, Object>();
        oc.invokeMethod(methodName,input,output, options);
        Test.stopTest();
    }
    @isTest
    private static void getGponDropStatusTest(){
        createTestData();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        OC_IntegrationAPIsUtil oc=new OC_IntegrationAPIsUtil();
        String methodName='getGponDropStatus';
        Map<String, Object> input=new Map<String, Object>();
        input.put('locationId','416111'); 
        
        Map<String, Object> output=new Map<String, Object>();
        Map<String, Object> options=new Map<String, Object>();
        oc.invokeMethod(methodName,input,output, options);
        Test.stopTest();
    }
    @isTest
    private static void getServicePathTest(){
        createTestData();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        OC_IntegrationAPIsUtil oc=new OC_IntegrationAPIsUtil();
        String methodName='getServicePath';
        Map<String, Object> input=new Map<String, Object>();
        input.put('fmsId','416111'); 
        input.put('municipalityName','416111'); 
        input.put('province','416111'); 
        input.put('coid','416111'); 
        
        Map<String, Object> output=new Map<String, Object>();
        Map<String, Object> options=new Map<String, Object>();
        oc.invokeMethod(methodName,input,output, options);
        Test.stopTest();
    }
    @isTest
    private static void getPortabilityCheckTest(){
        createTestData();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        OC_IntegrationAPIsUtil oc=new OC_IntegrationAPIsUtil();
        String methodName='getPortabilityCheck';
        Map<String, Object> input=new Map<String, Object>();
        input.put('phoneNumber','1111111111');
        input.put('buildingNumber','buildingNumber');
        input.put('coid','coid');
        input.put('fmsId','fmsId');
        input.put('locationId','locationId');
        input.put('municipalityName','municipalityName');
        input.put('npa_nxx','npa_nxx');
        input.put('postalCode','postalCode');
        input.put('province','province');
        input.put('street','street');
        input.put('streetDirection','streetDirection');
        input.put('streetName','streetName');
        input.put('streetName_fms','streetName_fms');
        input.put('streetTypePrefix','streetTypePrefix');
        input.put('streetTypeSuffix','streetTypeSuffix');
        input.put('suiteNumber','suiteNumber');
        
        Map<String, Object> output=new Map<String, Object>();
        Map<String, Object> options=new Map<String, Object>();
        oc.invokeMethod(methodName,input,output, options);
        Test.stopTest();
    }
}