public with sharing class s2c_Sales_MyCasesController {
    public list<case> myCases{get;set;}
    public s2c_Sales_MyCasesController(){
        init();
    }
    void init(){
        string query='select id,casenumber,subject,createddate,lastmodifieddate,status,recordtypeid,accountid,contactid,ownerid,account.name,contact.name,owner.name from case where isclosed=false and createdbyid=\''+userinfo.getuserid()+'\' order by createddate limit 20';
        myCases=database.query(query);
    }
}