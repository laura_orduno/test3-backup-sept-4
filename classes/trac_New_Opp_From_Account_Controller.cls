/*
    New Opp From Account Controller
    trac_New_Opp_From_Account_Controller.cls
    Part of the TELUS Usability II Projects
    
    Author: Alex Miller @ Traction <amiller@tractionondemand.com>
    TELUS Sponsor: Lenora Ruggieri <lenora.ruggieri@telus.com>
    Created: July, 2012
*/
public with sharing class trac_New_Opp_From_Account_Controller {
    public class NewOppException extends Exception {}

    public Boolean loaded {get; private set;}
    public Account account {get; private set;}
    public Contact[] contacts {get; private set;}
    public Integer contactsCount {get {if(contacts == null){return 0;} return contacts.size();}}
    
    /* Input Variables */
    public String searchTerm {get; set;}
    public Id selectedContactId {get; set;}
    
    public Boolean showNewContactForm {get; private set;}
    public Contact new_contact {get; set;}
    public Id mostRecentlyCreatedContactId {get; private set;}

    public trac_New_Opp_From_Account_Controller() {}
    
    public void onLoad() {
        Savepoint sp = database.setSavepoint();
        try {
            loaded = false;
            showNewContactForm = false;
            if (ApexPages.currentPage() == null || ApexPages.currentPage().getParameters() == null) { clear(); return; }
            String aid = ApexPages.currentPage().getParameters().get('aid');
            if (aid == null || aid == '' || aid.substring(0, 3) != '001') { clear(); return; }
            Id xid = (Id) aid;
            account = [Select Name, Owner.Name,ParentId From Account Where Id = :xid Limit 1];
            if (account == null) { clear(); throw new NewOppException('Unable to load account'); return; }

            loadContacts();
            if (contacts == null || contacts.size() == 0) { showNewContactForm = true; }
            
            new_contact = new Contact(AccountId = account.Id);

            loaded = true;
        } catch (Exception e) { 
            //system.debug('Caught Exception: ' + ex.getMessage() + ' on line ' + ex.getLineNumber() + '\n stack:' + ex.getStackTraceString());
            clear(); Database.rollback(sp); QuoteUtilities.log(e); 
        }
    }
    
    private void loadContacts() {
        Set<Id> associatedIds = new Set<Id>();
        List<Id> queryIds = new List<Id>() ;
        List<Id> recTypeId = new List<Id>();
        Map<Id,Account> associatedAccounts = new Map<Id,Account>();

        recTypeId.add( Schema.SObjectType.Account.RecordTypeInfosByName.get('CAN').RecordTypeId );
        recTypeId.add( Schema.SObjectType.Account.RecordTypeInfosByName.get('BAN').RecordTypeId );

        if(account != null){
            queryIds.add(account.Id); 
        }
        if( account != null && account.ParentId != null ){
            queryIds.add( account.ParentId );
        }

        //Grab the associated accounts ( the CBUCID Account of the RCID, and all the BAN and CANs from this account) .
        if( queryIds.size() > 0 ){
            associatedAccounts = new Map<Id,Account>( [SELECT Id,ParentId,Name 
                                                        FROM Account 
                                                        Where ParentId in :queryIds
                                                        OR Id in :queryIds
                                                        OR ( ParentId = :account.Id 
                                                            AND RecordTypeId in :recTypeId) limit 48000
                                                          ]);
        }

        //System.Debug('These are the Accounts: ' + associatedAccounts );
        
        if(associatedAccounts.size() > 0 ){
            associatedIds = associatedAccounts.keySet();
            contacts = [SELECT Name, Title, Email, Active__c ,Account.RecordTypeId
                            FROM Contact 
                            WHERE AccountId IN :associatedIds
                            ORDER BY Active__c DESC, Account.RecordType.Name DESC,CreatedDate DESC 
                            LIMIT 1000
            ]; 
        }
    }
    
    public PageReference toggleNewContactForm() {
        try {
            showNewContactForm = !showNewContactForm;
        } catch (Exception e) { clear(); QuoteUtilities.log(e); }
        return null;
    }
    
    public void onSearch() {
        try {
            if (searchTerm == null || searchTerm == '') { loadContacts(); return; }
            //searchTerm = '*' + searchTerm + '*';
            List<List<sObject>> searchResult = [FIND :searchTerm IN ALL FIELDS RETURNING Contact (name, email, title, Active__c,Account.RecordTypeId Where AccountId = :account.Id)];
            contacts = ((List<Contact>)searchResult[0]);
        } catch (Exception e) { QuoteUtilities.log(e); } return;
    }
    
    
    public PageReference createNewContact() {
            //insert new_contact;
            ApexPages.standardController sc = new ApexPages.standardController(new_contact);
            sc.save();
            if (sc.getId() == null) { return null; }
            new_contact = [Select FirstName, LastName, Name, Email, Title, Phone From Contact Where Id = :sc.getId()];
            if (new_contact.Id != null) {
                showNewContactForm = false;
                mostRecentlyCreatedContactId = new_contact.id;
                new_contact = new Contact(AccountId = account.id);
                loadContacts();
            }
            return null;
    }
    
    public PageReference onSelectContact() {
        try {
            if (selectedContactId == null) { throw new NewOppException('You must select a valid contact to create an opportunity.'); return null;}
            PageReference pr = new PageReference('/006/e');
            pr.getParameters().put('retURL', '%2F'+selectedContactId);
            pr.getParameters().put('accid', account.Id);
            pr.getParameters().put('conid', selectedContactId);
            pr.getParameters().put('ent', 'Opportunity');
            pr.setRedirect(true);
            return pr;
        } catch (Exception e) { QuoteUtilities.log(e); } return null;
    }
    
    private void clear() {
        account = null;
        contacts = null;
        new_contact = null;
    }
    public String recordTypeName(String id){
        return Schema.SObjectType.Account.RecordTypeInfosByName.get(':id').RecordTypeId;
    }
    
    /* Test Methods! */
    
    private void addETP(String e) {
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e));
    }
    
    private static testMethod void testWithNoContacts() {
        trac_New_Opp_From_Account_Controller tno = new trac_New_Opp_From_Account_Controller();
        Account a = new Account(Name = 'This is a test account');
        insert a;
        ApexPages.currentPage().getParameters().clear();
        ApexPages.currentPage().getParameters().put('aid', a.id);
        tno.onLoad();
        system.assertEquals(true, tno.showNewContactForm);
        system.assertEquals(0, tno.contactsCount);
        system.assertEquals(null, tno.toggleNewContactForm());
        system.assertEquals(false, tno.showNewContactForm);
    }
    
    private static testMethod void testAddContact() {
        trac_New_Opp_From_Account_Controller tno = new trac_New_Opp_From_Account_Controller();
        Account a = new Account(Name = 'This is a test account');
        insert a;
        ApexPages.currentPage().getParameters().clear();
        ApexPages.currentPage().getParameters().put('aid', a.id);
        tno.onLoad();
        system.assertEquals(true, tno.showNewContactForm);
        system.assertEquals(0, tno.contactsCount);
        
        tno.new_contact.FirstName = 'Alex';
        tno.new_contact.LastName = 'Miller';
        tno.new_contact.Title = 'Developer';
        tno.new_contact.Phone = '6049969449';
        tno.new_contact.Email = 'amiller@tractionondemand.com';
        tno.createNewContact();
        system.assertNotEquals(0, tno.contactsCount);
        system.assertEquals(tno.contacts[0].Id, tno.mostRecentlyCreatedContactId);
    }
    
    private static testMethod void testWithContact() {
        trac_New_Opp_From_Account_Controller tno = new trac_New_Opp_From_Account_Controller();
        Account a = new Account(Name = 'This is a test account');
        insert a;
        Contact c = new Contact(FirstName = 'Alex', LastName = 'Miller', Email = 'amiller@tractionondemand.com', Phone = '6049969449', Title = 'Developer', AccountId = a.Id);
        insert c;
        ApexPages.currentPage().getParameters().clear();
        ApexPages.currentPage().getParameters().put('aid', a.id);
        tno.onLoad();
        system.assertEquals(false, tno.showNewContactForm);
        system.assertEquals(1, tno.contactsCount);
    }
    
    private static testMethod void testSearch() {
        // Really ought to be called 'pretendToTestSearch'
        trac_New_Opp_From_Account_Controller tno = new trac_New_Opp_From_Account_Controller();
        Account a = new Account(Name = 'This is a test account');
        insert a;
        Contact c = new Contact(FirstName = 'Alex', LastName = 'Miller', Email = 'amiller@tractionondemand.com', Phone = '6049969449', Title = 'Developer', AccountId = a.Id);
        insert c;
        ApexPages.currentPage().getParameters().clear();
        ApexPages.currentPage().getParameters().put('aid', a.id);
        tno.onLoad();
        tno.searchTerm = 'miller';
        tno.onSearch();
        // Remember boys and girls, no SOSL in test classes!
        // system.assertEquals(1, tno.contactsCount);
    }
    
    private static testMethod void testChooseContact() {
        trac_New_Opp_From_Account_Controller tno = new trac_New_Opp_From_Account_Controller();
        Account a = new Account(Name = 'This is a test account');
        insert a;
        Contact c = new Contact(FirstName = 'Alex', LastName = 'Miller', Email = 'amiller@tractionondemand.com', Phone = '6049969449', Title = 'Developer', AccountId = a.Id);
        insert c;
        ApexPages.currentPage().getParameters().clear();
        ApexPages.currentPage().getParameters().put('aid', a.id);
        tno.onLoad();
        system.assertEquals(1, tno.contactsCount);
        tno.selectedContactId = tno.contacts[0].Id;
        PageReference pr = new PageReference('/006/e');
        pr.getParameters().put('retURL', '%2F'+tno.contacts[0].Id);
        pr.getParameters().put('accid', a.Id);
        pr.getParameters().put('conid', tno.contacts[0].Id);
        pr.getParameters().put('ent', 'Opportunity');
        pr.setRedirect(true);
        system.assertequals(pr.getUrl(), tno.onSelectContact().getUrl());
    }
    
    private static testMethod void testToggleForm() {
        trac_New_Opp_From_Account_Controller tno = new trac_New_Opp_From_Account_Controller();
        Account a = new Account(Name = 'This is a test account');
        insert a;
        Contact c = new Contact(FirstName = 'Alex', LastName = 'Miller', Email = 'amiller@tractionondemand.com', Phone = '6049969449', Title = 'Developer', AccountId = a.Id);
        insert c;
        ApexPages.currentPage().getParameters().clear();
        ApexPages.currentPage().getParameters().put('aid', a.id);
        tno.onLoad();
        system.assertEquals(1, tno.contactsCount);
        tno.showNewContactForm = null;
        tno.toggleNewContactForm();
    }
    private static testMethod void testNoAccount() {
        trac_New_Opp_From_Account_Controller tno = new trac_New_Opp_From_Account_Controller();
        tno.onLoad();
        system.assertEquals(false, tno.loaded);
    }
    private static testMethod void testNonExistentAccount() {
        trac_New_Opp_From_Account_Controller tno = new trac_New_Opp_From_Account_Controller();
        ApexPages.currentPage().getParameters().put('aid', '0014000000by1yA');
        tno.onLoad();
        system.assertEquals(false, tno.loaded);
    }
}