/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class OC_StandaloneCheckServicesTest {

    private static smbcare_address__c address3;
    private static Account RCIDacc;
    private static Contact Cont;
    private static Order Od ;
    private static Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
    private static Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();   

    static {
          // TO DO: implement unit test
        //Data Setup             
        Id recSerTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Service').getRecordTypeId();
        Id recRCIDTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId();        
          
        Account billAcc = new Account(name='Service Account',recordtypeid=recSerTypeId); 
        RCIDacc = new Account(name='RCID Account',recordtypeid=recRCIDTypeId, Billing_Account_ID__c= billAcc.Id);
        Account seracc = new Account(name='Service Account',recordtypeid=recSerTypeId,parentid=RCIDacc.id, Billing_Account_ID__c= billAcc.Id);           
          
        List<Account> acclist= new List<Account>();          
        acclist.add(RCIDacc);
        acclist.add(seracc );
        insert acclist;
          
        smbcare_address__c address= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N3',FMS_Address_ID__c='987650',account__c=RCIDacc.id); 
        address3= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N3',FMS_Address_ID__c='987652',account__c=RCIDacc.id,service_account_Id__c=seracc.id); 
        address3.Block__c = 'Block 1';
        address3.Building_Number__c = 'Building 1';
        address3.Street_Number__c = '25';
        address3.Street_Name__c = ' York Street';
        address3.Street__c = ' York Street';
        address3.Status__c = 'valid';
        address3.Suite_Number__c = '121';
        address3.Province__c = 'BC';
        
        smbcare_address__c address2= new smbcare_address__c( Unit_Number__c='121', Street_Address__c='25 york street', City__c='toronto',State__c='ontorio', Postal_Code__c='M1t3 N5',FMS_Address_ID__c='987651',account__c=RCIDacc.id); 
        List<smbcare_address__c> smbCarelist= new List<smbcare_address__c>(); 
        smbCarelist.add(address);
        smbCarelist.add(address2);
        smbCarelist.add(address3);
        insert smbCarelist;

        address3 = [Select Id,Unit_Number__c,Street_Address__c,City__c,State__c,Postal_Code__c,FMS_Address_ID__c,account__c,Account__r.name, Account__r.Billing_Account_Name__c,Account__r.Billing_Account_ID__c,Account__r.Account_Open_Since__c,Service_Account_Id__c    From smbcare_address__c WHERE Block__c = 'Block 1' AND Building_Number__c=  'Building 1' LIMIT 1];
        
        ServiceAddressData SData = new ServiceAddressData();
        SData.isManualCapture=true;
        SData.fmsId='987652';
        
        Cont = new Contact (FirstName ='FirstName', LastName='LastName', AccountId=seracc.Id, Email='Abc@abc.com');
        insert Cont;
        
        Contract Ct = new Contract(AccountId = RCIDacc.Id, Status = 'Draft',StartDate = Date.Today(), ContractTerm = 12);
        Insert Ct;
        
        Id standardPriceBookId = Test.getStandardPriceBookId();
        
        SMBCare_Address__c smbCareAddr = new SMBCare_Address__c(Account__c=RCIDacc.Id, Postal_Code__c='M1T3N3', Street_Number__c='Street 1', Street_Name__c='Street Name 1', Province__c='ONT', Suite_Number__c='1001', COID__c='COID 1', Building_Number__c='Building 1', Country__c='CAN', FMS_Address_ID__c='FMSID 121', Status__c='Valid');
        insert smbCareAddr;
        
        Opportunity Opp= new Opportunity(Name='Test Opp',StageName='Discovery',CloseDate=System.today().addDays(5));
        insert Opp; 
        
        Quote Qt= new Quote(Name='Test Quote11',opportunityid=opp.id);
        insert Qt;
        
        Od = new Order(Name = 'ORD-000001', Shipping_Address__c='2010 100 AV NW EDMONTON AB T5A3M1', Service_Address_Text__c='2010 100 AV NW EDMONTON AB T5A3M1' , FMS_Address_ID__c='FMSID 121', ShippingStreet='2010 100 AV', ShippingCity='EDMONTON', BAN__c='Ban 21' , PriceBook2Id =standardPriceBookId  , AccountId = RCIDacc.Id, EffectiveDate = Date.Today(), ContractId = Ct.Id, Status = 'Draft', vlocity_cmt__QuoteId__c=Qt.Id);
        Od.Service_Address__c = address3.Id;
        Insert Od;
        
        Product2 Pd = new Product2(Name='Porduct 1',isActive=true);
        insert Pd;

       
/*        PricebookEntry Pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=pd.Id, UnitPrice=99, isActive=true);
        insert Pbe;

        OrderItem ordPd = new OrderItem(PriceBookEntryId=Pbe.Id, OrderId=Od.Id, Quantity=1, UnitPrice=99);
        insert ordPd;
        
        Case Cse = new Case(AccountId=seracc.Id, ContactId=Cont.Id, Description='Test Description', Status='New');
        Insert Cse;*/
        
        List<Country_Province_Codes__c> cntPrvnList = new List<Country_Province_Codes__c>();
        
        Country_Province_Codes__c CntryProv = new Country_Province_Codes__c(Name='AA', Type__c='American State', is_ILEC__c=false,Description__c='Armed Forces Americas');
        
        Country_Province_Codes__c CntryProv1 = new Country_Province_Codes__c(Name=' AK', Type__c='American State', is_ILEC__c=false,Description__c='Alaska');
        
        cntPrvnList.add(CntryProv);
        cntPrvnList.add(CntryProv1);
        
        Insert cntPrvnList;
    }
    
    @IsTest
    private static void OC_StandaloneCheckServices_UnitTest() {
        
        Test.startTest();
            PageReference PageRef = Page.OC_StandaloneCheckServices;
            PageRef.getparameters().put('rcid', RCIDacc.Id);
            PageRef.getparameters().put('addressType', 'MOVE_OUT');
            PageRef.getparameters().put('parentOrderId', Od.Id);
            PageRef.getparameters().put('phoneNumber', '6041231212');
            PageRef.getparameters().put('smbCareAddrId', address3.Id);
            Test.setCurrentPage(PageRef);  
            OC_StandaloneCheckServices ScNewController =  new  OC_StandaloneCheckServices();
            //ScNewController.serviceAccount = seracc;
            OC_StandaloneCheckServices.ProductCategory product = new OC_StandaloneCheckServices.ProductCategory();
            product.internet_type = 'FIFA GPON';
            product.type = 'INTERNET';
            product.availability = 'available';
            OC_StandaloneCheckServices.Characteristic charac = new OC_StandaloneCheckServices.Characteristic();
            charac.name = 'test';
            charac.value = '10';
            charac.displaySequence = 1;
            OC_StandaloneCheckServices.Characteristic charac2 = new OC_StandaloneCheckServices.Characteristic();
            charac2.name = 'test2';
            charac2.value = '11';
            charac2.displaySequence = 2;

            product.characteristics = New List<OC_StandaloneCheckServices.Characteristic>{charac, charac2};
            product.characteristics.sort();

            PageReference pr = ScNewController.getSAData();
            ScNewController.ProcessChange();
            ScNewController.LoadAccounts();
            pr = ScNewController.updateSAData();
            //List<Object> as = ScNewController.getAccountList();
            List<SelectOption> options = ScNewController.getOrderPathValues();
            options = ScNewController.getOrderPathValues();
            options = ScNewController.getExistingServicesValues();
            options = ScNewController.getcustomerLocationValues();
            options = ScNewController.getTypeOfOrderValues();
            //String where = ScNewController.GetWhereClause();
            String abds = OC_StandaloneCheckServices.GetAddressSearchString(null);
            String getWhere = ScNewController.GetWhereClause();

            //
            //
             List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
            Id serviceAddressId = addresses[0].Id;
            OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
              
                
            OrdrTestDataFactory.createOrderingWSCustomSettings('ProductEligibilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/CMO/OrderMgmt/PerformCustomerOrderFeasibility_v2_0_vs0');
            Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
            //TpFulfillmentCustomerOrderV3.CustomerOrder response = OrdrProductEligibilityWsCallout.performCustomerOrderFeasibility(inputMap);

            pr = ScNewController.init();
            pr = ScNewController.RedirectToNextPage();

            PageRef.getparameters().put('smbCareAddrId', serviceAddressId);
            ScNewController.SAData = new ServiceAddressData();
            ScNewController.SAData.province='BC';
            ScNewController.SAData.suiteNumber='12';
            ScNewController.SAData.buildingNumber='03';
            ScNewController.SAData.street='Test st';
            ScNewController.SAData.streetDirectionCode='AB';
            ScNewController.SAData.city='Burnaby';
            ScNewController.SAData.country='Canada';
            ScNewController.SAData.city='Burnaby';
            ScNewController.SAData.postalCode='V3M5V1';
            ScNewController.SAData.isManualCapture = True;        
            ScNewController.dropExceptionCode = '';
            ScNewController.showErrorMsg = false;
            //ScNewController.productCategoriesTop = new List<ProductCategory>();
            ScNewController.typeOfOrder = 'Standard';
            ScNewController.orderPath = '';
            ScNewController.customerLocation = '';
            ScNewController.errorMsg = '';
            ScNewController.maxSpeed = '';
            ScNewController.portabilityCheckMessage = '';
            ScNewController.portabilityCheckFailure = false;
            ScNewController.portabilityCheckReturned = false;
            ScNewController.addressOrigin = '';
            ScNewController.dropInfo = '';
            Boolean geshowErrorMsg = ScNewController.showErrorMsg;
            OC_StandaloneCheckServices.SampleResultInfo sampleInfo = new  OC_StandaloneCheckServices.SampleResultInfo();
            OC_StandaloneCheckServices.ProductCategory productCats =  new OC_StandaloneCheckServices.ProductCategory();
            productCats.characteristics = new List<OC_StandaloneCheckServices.characteristic>();
            productCats.type = 'type';
            productCats.availability = 'available';
            productCats.icon = 'Icon';
            productCats.internet_type = 'Fiber';

            String dropI = ScNewController.dropInfo;
            String currentTr = ScNewController.currentTransportTypeCode;
            String gPonBTC = ScNewController.gponBuildTypeCode;
            String provisioning = ScNewController.provisioningSystemCode;
            OC_StandaloneCheckServices.AccountSummary accS = new OC_StandaloneCheckServices.AccountSummary(address3);
            ScNewController.dropFacilityCode = '';
            ScNewController.dropTypeCode = '';
            ScNewController.dropLength = '';
            ScNewController.currentTransportTypeCode = '';
            ScNewController.futureTransportTypeCode = '';
            ScNewController.futurePlantReadyDate = '';
            ScNewController.futureTransportRemarkTypeCode = '';
            ScNewController.gponBuildTypeCode = '';
            ScNewController.provisioningSystemCode = '';
            ScNewController.ratingNpaNxx = '';
            ScNewController.COID = '';
            List<OC_StandaloneCheckServices.AccountSummary> accSummList = ScNewController.getAccountList();
            OC_SelectAndNewAddressController.isILEC('AB');

            pr = ScNewController.initPortabilityCheck();
            pr = ScNewController.callIntegrationUtil();
            pr = ScNewController.RedirectToNextPage();

        Test.StopTest();   
   
    }

    @IsTest
    private static void OC_StandaloneCheckServices_InitPortabilityErrorMsg() {

        Test.startTest();
            PageReference PageRef = Page.OC_StandaloneCheckServices;
            PageRef.getparameters().put('phoneNumber', '');
            Test.setCurrentPage(PageRef);  
            OC_StandaloneCheckServices ScNewController =  new  OC_StandaloneCheckServices();

            PageReference pr = ScNewController.initPortabilityCheck();
            System.assertEquals(pr, null, 'Returns null in case of Expected Error.');
            pr = ScNewController.RedirectToNextPage();
        Test.StopTest();   

        System.assertNotEquals(pr, null, 'Returns null in case of Expected Error.');
    }

    @IsTest
    private static void OC_StandaloneCheckServices_ComplexOrder() {

        Test.startTest();
            PageReference PageRef = Page.OC_StandaloneCheckServices;
            PageRef.getparameters().put('rcid', RCIDacc.Id);
            PageRef.getparameters().put('addressType', 'MOVE_OUT');
            PageRef.getparameters().put('parentOrderId', Od.Id);
            PageRef.getparameters().put('phoneNumber', '6041231212');
            PageRef.getparameters().put('smbCareAddrId', address3.Id);
            Test.setCurrentPage(PageRef);  

            OC_StandaloneCheckServices ScNewController =  new  OC_StandaloneCheckServices();
            ScNewController.orderPath = 'Complex';
            PageReference pr = ScNewController.callIntegrationUtil();
            pr = ScNewController.RedirectToNextPage();

        Test.StopTest();   

        System.assertNotEquals(null, pr, 'Returns null in case of Expected Error.');
    }


    @IsTest
    private static void OC_StandaloneCheckServices_Products() {
        System.runAs(new User(Id = Userinfo.getUserId())) {
            OrdrTestDataFactory.createOrderingWSCustomSettings('RateGroupInfoEndpoint', 'https://test.test.com');
            OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://test.test.com');
            OrdrTestDataFactory.createOrderingWSCustomSettings('QueryResourceWirelineEndpoint', 'https://test.test.com');
            OrdrTestDataFactory.createOrderingWSCustomSettings('ClearanceCheckEndpoint', 'https://test.test.com');
            
        }
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
        OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
            
        Test.startTest();
            PageReference PageRef = Page.OC_StandaloneCheckServices;
            PageRef.getparameters().put('rcid', RCIDacc.Id);
            PageRef.getparameters().put('addressType', 'MOVE_OUT');
            PageRef.getparameters().put('parentOrderId', Od.Id);
            PageRef.getparameters().put('phoneNumber', '6041231212');
            PageRef.getparameters().put('smbCareAddrId', serviceAddressId);
            Test.setCurrentPage(PageRef);  
            Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());


            OC_StandaloneCheckServices ScNewController =  new  OC_StandaloneCheckServices();
            ScNewController.orderPath = 'Complex';
            PageReference pr = ScNewController.callIntegrationUtil();
            System.assertEquals( null,pr, 'Returns null in case of Expected Error.');
            ScNewController.LoadAccounts();
        Test.StopTest();   

    }

    @IsTest
    private static void OC_StandaloneCheckServices_loadAccounts() {
        
        List<SMBCare_Address__c> addresses = OrdrTestDataFactory.createAddresses();
        Id serviceAddressId = addresses[0].Id;
        OrdrTestDataFactory.createOrderingWSCustomSettings('TechnicalAvailabilityEndpoint', 'https://xmlgwy-pt1.telus.com:9030/virt/RMO/ResourceMgmt/CheckGenericTechnicalAvailabilitySvc_v2_2_vs1');
            
        Test.startTest();
            PageReference PageRef = Page.OC_StandaloneCheckServices;
            PageRef.getparameters().put('rcid', RCIDacc.Id);
            PageRef.getparameters().put('addressType', 'MOVE_OUT');
            PageRef.getparameters().put('parentOrderId', Od.Id);
            PageRef.getparameters().put('phoneNumber', '6041231212');
            PageRef.getparameters().put('smbCareAddrId', serviceAddressId);
            Test.setCurrentPage(PageRef);  
            Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());

            OC_StandaloneCheckServices ScNewController =  new  OC_StandaloneCheckServices();
            ServiceAddressData saData = new ServiceAddressData();
            saData.address = ' York Street' ; 
            saData.city = 'Toronto' ; 
            saData.province = 'Ontario'; 
            saData.postalCode = 'a1a 1a1' ; 
            saData.country = 'Canada' ; 
            saData.searchString = 'York'; 
            saData.fmsId = '987652';
            saData.isManualCapture  =  true ; 
            saData.buildingNumber =  'Building 1'; 
            saData.suiteNumber = '121';

            ScNewController.SAData = saData;
            ScNewController.LoadAccounts();

            saData = new ServiceAddressData();
            saData.address = ' York Street' ; 
            saData.city = 'Toronto' ; 
            saData.province = 'Ontario'; 
            saData.postalCode = 'a1a 1a1' ; 
            saData.country = 'Canada' ; 
            saData.searchString = 'York'; 
            saData.fmsId = '987652';
            saData.isManualCapture  =  true ; 
            saData.buildingNumber =  'Building 1'; 
            saData.streetTypeSuffix = 'StreetSuffix' ; 
            saData.municipalityNameLPDS = 'testMunicipality' ; 
            saData.dropFacilityCode = 'testCode' ; 
            saData.dropTypeCode = 'testCodeType';
            saData.dropLength = 'testDropLength'; 
            saData.dropExceptionCode = 'exceptionCode'; 
            saData.currentTransportTypeCode = 'testTransportType';
            saData.futureTransportTypeCode = 'testFutureTransport'; 
            saData.futurePlantReadyDate = 'testPlantReady'; 
            saData.futureTransportRemarkTypeCode = 'testTransportCode'; 
            saData.gponBuildTypeCode = 'testgponBuildType'; 
            saData.provisioningSystemCode = 'testProvisioningCode';
            saData.street = ' York Street';
            saData.streetTypeCode='Street';
            saData.streetName = ' York Street';
            saData.streetName_fms = 'York Street';
            saData.streetDirectionCode = 'East';
            saData.suiteNumber = '121';

            ScNewController.SAData = saData;
            ScNewController.LoadAccounts();
            ScNewController.GetWhereClause();
            PageReference pr = ScNewController.initPortabilityCheck();

        Test.StopTest();   

        System.assertEquals( null,pr, 'Returns null in case of Expected Error.');
    }
  
}