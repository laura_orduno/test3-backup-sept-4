@isTest
private class ActivateOrderController_vlocity_cmtTest {

static testMethod void TestCase_1() {
        PageReference pageref = Page.ActivateOrder_vlocity_cmt;
        Test.setCurrentPage(pageref);

        Test.startTest();

        TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();

        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;

		Contract testContract_1  = new Contract(Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.Id,ContractTerm=12,StartDate=system.today(),vlocity_cmt__OpportunityId__c = testOpp_1.Id);
    	insert testContract_1;

        Blob b = Blob.valueOf('Test Data');  
	    Attachment attachment = new Attachment();  
	    attachment.ParentId = testContract_1.Id;  
	    attachment.Name = 'Test Attachment for Parent';  
	    attachment.Body = b;  
	    insert(attachment);  
    
        vlocity_cmt__ContractVersion__c testContractVersion = new vlocity_cmt__ContractVersion__c(Name='Test Version 1', vlocity_cmt__ContractId__c = testContract_1.Id);
        insert testContractVersion;

        
        pageref.getparameters().put('id', testContract_1.Id);
        ApexPages.StandardController sc_1 = new ApexPages.StandardController(testContract_1);
        ActivateOrderController_vlocity_cmt contToTest_1 = new ActivateOrderController_vlocity_cmt(sc_1);
    
        List<ActivateOrderController_vlocity_cmt.AttachmentWrapper> attachmentWrapperList =  new List<ActivateOrderController_vlocity_cmt.AttachmentWrapper>();
        attachmentWrapperList.add(new ActivateOrderController_vlocity_cmt.AttachmentWrapper(attachment));
        contToTest_1.attachmentWrapperList = attachmentWrapperList;
        contToTest_1.activateContract();

        Test.stopTest();


}
    
    static testMethod void TestCase_2() {
        PageReference pageref = Page.ActivateOrder_vlocity_cmt;
        Test.setCurrentPage(pageref);

        Test.startTest();

        TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();

        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;

		Contract testContract_1  = new Contract(Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.Id,ContractTerm=12,StartDate=system.today(),vlocity_cmt__OpportunityId__c = testOpp_1.Id);
    	insert testContract_1;

        Blob b = Blob.valueOf('Test Data');  
	    Attachment attachment = new Attachment();  
	    attachment.ParentId = testContract_1.Id;  
	    attachment.Name = 'Test Attachment for Parent';  
	    attachment.Body = b;  
	    insert(attachment);  
    
        vlocity_cmt__ContractVersion__c testContractVersion = new vlocity_cmt__ContractVersion__c(Name='Test Version 1', vlocity_cmt__ContractId__c = testContract_1.Id);
        insert testContractVersion;
        
        Blob b2 = Blob.valueOf('Test Data');  
	    Attachment attachment2 = new Attachment();  
	    attachment2.ParentId = testContractVersion.Id;  
	    attachment2.Name = 'Test Attachment for Parent';  
	    attachment2.Body = b2;  
	    insert(attachment2);  

        
        pageref.getparameters().put('id', testContract_1.Id);
        ApexPages.StandardController sc_1 = new ApexPages.StandardController(testContract_1);
        ActivateOrderController_vlocity_cmt contToTest_1 = new ActivateOrderController_vlocity_cmt(sc_1);
    
        List<ActivateOrderController_vlocity_cmt.AttachmentWrapper> attachmentWrapperList =  new List<ActivateOrderController_vlocity_cmt.AttachmentWrapper>();
        attachmentWrapperList.add(new ActivateOrderController_vlocity_cmt.AttachmentWrapper(attachment));
        attachmentWrapperList[0].selectedVar = true;
        attachmentWrapperList[0].attachementType = 'Contract';
        contToTest_1.attachmentWrapperList = attachmentWrapperList;
        contToTest_1.activateContract();

        Test.stopTest();


}

   static testMethod void TestCase_3() {
        PageReference pageref = Page.ActivateOrder_vlocity_cmt;
        Test.setCurrentPage(pageref);

        Test.startTest();

        TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();

        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;

		Contract testContract_1  = new Contract(Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.Id,ContractTerm=12,StartDate=system.today(),vlocity_cmt__OpportunityId__c = testOpp_1.Id);
    	insert testContract_1;

        Blob b = Blob.valueOf('Test Data');  
	    Attachment attachment = new Attachment();  
	    attachment.ParentId = testContract_1.Id;  
	    attachment.Name = 'Test Attachment for Parent';  
	    attachment.Body = b;  
	    insert(attachment);  
    
        vlocity_cmt__ContractVersion__c testContractVersion = new vlocity_cmt__ContractVersion__c(Name='Test Version 1', vlocity_cmt__ContractId__c = testContract_1.Id);
        insert testContractVersion;

        
        pageref.getparameters().put('id', testContract_1.Id);
        ApexPages.StandardController sc_1 = new ApexPages.StandardController(testContract_1);
        ActivateOrderController_vlocity_cmt contToTest_1 = new ActivateOrderController_vlocity_cmt(sc_1);
    
        List<ActivateOrderController_vlocity_cmt.AttachmentWrapper> attachmentWrapperList =  new List<ActivateOrderController_vlocity_cmt.AttachmentWrapper>();
        attachmentWrapperList.add(new ActivateOrderController_vlocity_cmt.AttachmentWrapper(attachment));
        attachmentWrapperList.add(new ActivateOrderController_vlocity_cmt.AttachmentWrapper(attachment));
        attachmentWrapperList[0].selectedVar = true;
        attachmentWrapperList[1].selectedVar = true;
        contToTest_1.attachmentWrapperList = attachmentWrapperList;
        contToTest_1.activateContract();

        Test.stopTest();


}
    static testMethod void TestCase_4() {
        PageReference pageref = Page.ActivateOrder_vlocity_cmt;
        Test.setCurrentPage(pageref);

        Test.startTest();

        TestDataHelper.testAccountCreation();
        TestDataHelper.testContactCreation();

        Opportunity testOpp_1   = smb_test_utility.createOpportunity('SMB_Care_Opportunity', Null, Null, false);
        testOpp_1.Type = 'All Other Orders';
        insert testOpp_1;

Contract testContract_1  = new Contract(Contract_Type__c='Amendment',AccountId=TestDataHelper.testAccountObj.Id,ContractTerm=12,StartDate=system.today(),vlocity_cmt__OpportunityId__c = testOpp_1.Id);
    insert testContract_1;

        vlocity_cmt__ContractVersion__c testContractVersion = new vlocity_cmt__ContractVersion__c(Name='Test Version 1', vlocity_cmt__ContractId__c = testContract_1.Id);
        insert testContractVersion;

        
        pageref.getparameters().put('id', testContract_1.Id);
        ApexPages.StandardController sc_1 = new ApexPages.StandardController(testContract_1);
        ActivateOrderController_vlocity_cmt contToTest_1 = new ActivateOrderController_vlocity_cmt(sc_1);
        contToTest_1.contractObj  = null;
        contToTest_1.activateContract();

        Test.stopTest();


}

     
}