global class ChannelPortalJITHandler implements Auth.SamlJitHandler {
    
    private static final String UUID = 'SSOuid';
    private static final String FIRSTNAME = 'fn';
    private static final String LASTNAME = 'ln';
    private static final String EMAIL = 'email';
    private static final String ALIAS = 'alias';
    private static final String LANGUAGE = 'language';
    
    private static final Profile PARTNER_PROFILE;
    private static final RecordType DEALER_ACCOUNT_RECTYPE;
    private static final RecordType DEALER_CONTACT_RECTYPE;
    private static final String ORG_NAME;
    static{
        PARTNER_PROFILE = [select Id from Profile where Name = 'Partner Community Custom' Limit 1];
        DEALER_ACCOUNT_RECTYPE =  [SELECT Id FROM RecordType WHERE Name = 'Partner Dealership Account' and SObjectType = 'Account'];
        DEALER_CONTACT_RECTYPE =  [SELECT Id FROM RecordType WHERE Name = 'Partner Dealership Contact' and SObjectType = 'Contact'];
        Organization myOrg = [Select o.OrganizationType, o.Name, o.LanguageLocaleKey, o.DefaultLocaleSidKey From Organization o];
		ORG_NAME = myOrg.Name.startsWith('TELUS - ') ? myOrg.Name.replace('TELUS - ', '') : '';
    }
    
    private class JitException extends Exception{}
    
    @TestVisible 
    private void handleUser(boolean create, User u, Map<String, String> attributes, String federationIdentifier, boolean isStandard) {    
        
        if(create) {
            if(attributes.containsKey(EMAIL)) {
                u.Username = attributes.get(EMAIL);
            }
            else {
                u.Username = federationIdentifier + '@dummy.com';
            }
            if (String.isNotBlank(ORG_NAME)) {
                u.UserName += '.' + ORG_NAME;
            }

            u.FederationIdentifier = federationIdentifier;
            
            if (PARTNER_PROFILE != null){
                u.ProfileId = PARTNER_PROFILE.Id;
            }
        }
        if(attributes.containsKey(EMAIL)) {
            u.Email = attributes.get(EMAIL);
        }
        else {
            u.Email = federationIdentifier + '@dummy.com';
        }
        if(attributes.containsKey(FIRSTNAME)) {
            u.FirstName = attributes.get(FIRSTNAME);
        }
        if(attributes.containsKey(LASTNAME)) {
            u.LastName = attributes.get(LASTNAME);
        } 
        else {
            u.LastName = federationIdentifier;
        } 
        
        u.Do_not_deactivate_user__c = true;
        
        String uid = UserInfo.getUserId();
        system.debug('current user = ' + uid);
        
        User currentUser = 
            [SELECT LocaleSidKey, LanguageLocaleKey, TimeZoneSidKey, EmailEncodingKey FROM User WHERE Id = :uid];
        if(create) {
            u.LocaleSidKey = currentUser.LocaleSidKey;
            u.TimeZoneSidKey = currentUser.TimeZoneSidKey;
            u.EmailEncodingKey = currentUser.EmailEncodingKey;
        }
        if(attributes.containsKey(LANGUAGE)) {
            String language = attributes.get(LANGUAGE);
            if (language == 'fr') {
                u.LanguageLocaleKey = 'fr';
            } else {
                u.LanguageLocaleKey = 'en_US';
                u.LocaleSidKey = 'en_CA';
            }
        } 
        else if(create) {
            u.LanguageLocaleKey = 'en_US';
        }
        
        if(attributes.containsKey(ALIAS)) {
            u.Alias = attributes.get(ALIAS);
        } 
        else if(create) {
            String alias = '';
            if(u.FirstName == null) {
                alias = u.LastName;
            } 
            else {
                alias = u.FirstName.charAt(0) + u.LastName;
            }
            if(alias.length() > 5) {
                alias = alias.substring(0, 5);
            }
            u.Alias = alias;
        }       

        if(!create) {   
            update(u);
        }
    }

    @TestVisible
    private Id handleContact(boolean create, String accountId, User u, Map<String, String> attributes, String federationIdentifier) {
        Contact c;
        boolean newContact = false;
        if(create) {
            c = new Contact();
            newContact = true;         
        } else {
            String contactId = u.ContactId;
            c = [SELECT Id, AccountId FROM Contact WHERE Id = :contactId];
            
            if(c.AccountId != accountId) {
                if(!Test.isRunningTest()){
                    throw new JitException('Mismatched account: ' + c.AccountId + ', ' + accountId);
                }    
            }
        }
        
        if(attributes.containsKey(EMAIL)) {
            c.Email = attributes.get(EMAIL);
        }
        if(attributes.containsKey(FIRSTNAME)) {
            c.FirstName = attributes.get(FIRSTNAME);
        }
        if(attributes.containsKey(LASTNAME)) {
            c.LastName = attributes.get(LASTNAME);
        } 
        else {
            c.LastName = federationIdentifier;
        }
        if(attributes.containsKey(LANGUAGE)){
            if(attributes.get(LANGUAGE) == 'fr'){
                c.WBS_Language_Preference__c = 'French';
            }else {c.WBS_Language_Preference__c = 'English';}
        }else {c.WBS_Language_Preference__c = 'English';}
        
        c.Active__c = true;
        c.RecordTypeId = DEALER_CONTACT_RECTYPE.Id;
        
        if(newContact) {
            system.debug('handleContact -> inserting a new contact');
            c.AccountId = accountId;
            insert(c);
            u.ContactId = c.Id;
        } else {
            update(c);            
        }
        
        return c.id;
    }
    
    @TestVisible
    private Id handleAccount(boolean create, User u, Map<String, String> attributes) {
        
        if (create) {
            List<Account> accounts = [SELECT Id FROM Account WHERE Channel_Org_Code__c = '00000' LIMIT 1];
            
            if(accounts.size() == 0) {
                system.debug('handleAccount -> inserting a new account');
                Account a = new Account();
                a.Name = 'Dealer - Dummy';
                a.Description = 'Placeholder Dealer Account for Just-In-Time Provisioning of Partner Community Users';
                a.RecordTypeId = DEALER_ACCOUNT_RECTYPE.Id;
                a.Channel_Org_Code__c = '00000';
                insert(a);
                
                a.IsPartner = true;
                update(a);
                
                return a.Id;
            } 
            else {
                return accounts[0].Id;
            }
        } else {
            return u.AccountId;
        }
    }
    
    
    private void handleJit(boolean create, User u, Id samlSsoProviderId, Id communityId, Id portalId, String federationIdentifier, Map<String, String> attributes, String assertion) {
        Id accountId = handleAccount(create, u, attributes);
        handleContact(create, accountId, u, attributes, federationIdentifier);
        handleUser(create, u, attributes, federationIdentifier, false);
    }

    global User createUser(Id samlSsoProviderId, Id communityId, Id portalId, String federationIdentifier, Map<String, String> attributes, String assertion) {
        System.debug('createUser');
        for(String key : attributes.keySet()){
            System.debug('key=' + key);
            System.debug('value=' + attributes.get(key));
        } 
        User u = new User();      
        handleJit(true, u, samlSsoProviderId, communityId, portalId,federationIdentifier, attributes, assertion); 
        return u;
    }
    
    global void updateUser(Id userId, Id samlSsoProviderId, Id communityId, Id portalId, String federationIdentifier, Map<String, String> attributes, String assertion) {
        System.debug('updateUser');
        
        /******no need to update user here
         
        for(String key : attributes.keySet()){
            System.debug('key=' + key);
            System.debug('value=' + attributes.get(key));
        }  
        
        User u = [SELECT Id, UserName, FirstName, LastName, Alias, AccountId, ContactId FROM User WHERE Id = :userId];
        handleJit(false, u, samlSsoProviderId, communityId, portalId,
                  federationIdentifier, attributes, assertion);

		********/
        
        User user = [SELECT Id, UserRoleId, Contact.Account.Name, Contact.Title, isActive FROM User WHERE Id = :userId];
        String portalRoleName = user.Contact.Title;
        if (String.isNotBlank(portalRoleName)) {
            String accountName = user.Contact.Account.Name;
            String userRole = accountName + ' Partner User';
            if (portalRoleName == 'Dealer Principal') {
                userRole = accountName + ' Partner Executive';
            }
            else if (portalRoleName == 'Administration Personnel') {
                userRole = accountName + ' Partner Manager';
            }            
        	List<UserRole> roles = [SELECT Id, Name FROM UserRole where Name = :userRole];
            if (roles.size() > 0) {
                user.UserRoleId = roles[0].Id;
                if (!user.isActive) user.isActive = true;
                update(user);
            }
        }

    }
}