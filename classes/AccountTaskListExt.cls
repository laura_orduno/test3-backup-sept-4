public class AccountTaskListExt{
    public id accountId{get;set;}
    public Boolean AllowTaskAndDiaryCreation{get;set;}
    
    public account account{get;set;}
    public apexpages.standardcontroller controller{get;set;}
    public list<task> tasks{get;set;}
    public list<String> searchSets{get;set;}
    public integer numTasks{get;set;}
    public integer numTasksPerPage{get;set;}
    public integer numPages{get;set;}

    public string searchText{get;set;}
    public Map<string, string> searchColumns{get;set;}

    public string subjectSearchText{get;set;}
    public string summarySearchText{get;set;}
    
    public string sortColumn{get;set;}
    public string sortDirection{get;set;}
    public integer currentPage{get;set;}
    public string sortArrow{get;set;}
    public list<integer> pageList{get;set;}
    public integer offset{get;set;}
    integer queryLimit{get;set;}
    
    public AccountTaskListExt(apexpages.standardcontroller controller){
        this.controller=controller;
        this.accountId=controller.getid();
		if (!Test.isRunningTest()) controller.addfields(new list<string>{'id','name'}); // still covered
        this.account=(account)controller.getrecord();
		AllowTaskAndDiaryCreation=true;
    	sortDirection='desc';
        sortColumn='createddate';
        currentPage=1;
        queryLimit=300;
        offset=0;
        searchSets = new List<String>{'default'};
        searchColumns = new Map<String, String>{
            'General' => '',
            'Subject' => '',
            'Summary__c' => '',
            'WhoId' => '',
			'WhatId' => '',
            'TaskSubtype' => '',
            'ActivityDate' => '',
            'Activity_Type__c' => '',
            'Assigned_To_Owner__c' => '',
            'LastModifiedDate' => '',
            'CreatedDate' => ''
        };
        
        runQuery();
    }
    
    public pagereference searchTasks(){
System.debug('searchTasks: ' + searchText);
        // clear
        clearSearchColumns();

        if(string.isblank(searchText)){
            runQuery();
        }else{
            list<task> allFound=new list<task>();
            allFound.addall(findTasksByAllFields(searchText));
            allFound.addall(queryTasksRelatedFields(searchText));
System.debug('allFound.size(): ' + allFound.size());
            taskMashing(allFound, null, null);
        }
        return null;
    }
    public pagereference searchTasksByColumn(){
System.debug('searchTasksByColumn: ' + searchText);
        // clear general search text
        searchText = '';
        
        // search columns that do not require the SOSL query to be filtered
        Set<String> exemptSearchColumns = new Set<String>{'CreatedDate'};

        String column = ApexPages.CurrentPage().getParameters().get('column');
        
        if(String.isBlank(column)){
System.debug('column is blank');
			clearSearchColumns();
    		runQuery();
			
			return null;            
        }
        
        String search = exemptSearchColumns.contains(column) ? '' : ApexPages.CurrentPage().getParameters().get('search');
        String filterSearch = ApexPages.CurrentPage().getParameters().get('search');
        list<task> allFound=new list<task>();
        allFound.addall(findTasksByAllFields(search));
        allFound.addall(queryTasksRelatedFields(search));
        taskMashing(allFound, column, filterSearch);
        
        return null;
    }
    
    list<task> findTasksByAllFields(String sSearch){
        list<task> foundTasks = null;
        if(String.isEmpty(sSearch)){
            runQuery();
            foundTasks = tasks;
        }
        else{
//            string searchQuery='FIND \'' + sSearch + '*\' IN ALL FIELDS RETURNING task(id where whatid=:accountId and isclosed=true order by '+sortColumn+' '+sortDirection+' nulls last LIMIT 200)';
//            string searchQuery='FIND \'' + sSearch + '*\' IN ALL FIELDS RETURNING task(id order by '+sortColumn+' '+sortDirection+' nulls last LIMIT 200)';
            string searchQuery='FIND \'' + sSearch + '*\' IN ALL FIELDS RETURNING task(id where accountid=:accountId order by '+sortColumn+' '+sortDirection+' nulls last LIMIT ' +  queryLimit + ')';
    		System.debug('searchQuery: ' + searchQuery);
            
            list<list<SObject>>searchList = search.query(searchQuery);
            foundTasks=(list<task>)searchList[0];            
        }
        
        return foundTasks;
    }
    list<task> queryTasksRelatedFields(String search){
System.debug('queryTasksRelatedFields: ' + search);        
        string query='select '+getTaskFieldsQueryString()+' from task where accountId=\''+controller.getid()+'\' and ( who.name like \'%'+string.escapesinglequotes(search)+'%\' or what.name like \'%'+string.escapesinglequotes(search)+'%\') and isclosed=true order by '+sortColumn+' '+sortDirection+' nulls last limit '+string.valueof(queryLimit);
//        string query='select '+getTaskFieldsQueryString()+' from task where ( who.name like \'%'+string.escapesinglequotes(search)+'%\' or what.name like \'%'+string.escapesinglequotes(search)+'%\') and isclosed=true order by '+sortColumn+' '+sortDirection+' nulls last limit '+string.valueof(queryLimit);
System.debug('queryTasksRelatedFields, query: ' + query);        
        list<task> foundTasks=database.query(query);
System.debug('queryTasksRelatedFields, foundTasks: ' + foundTasks);        
        return foundTasks;
    }
    
    void clearSearchColumns(){
        // clear 	
        for(String key : searchColumns.keySet()){
            searchColumns.put(key, '');
        }
    }    
        
    private Map<Id, Contact> getContactMap(List<Task> taskList){
        Set<Id> taskIds = new Set<Id>();
        
        for(Task t : taskList){
            taskIds.add(t.whoId);
        }
        
        return new Map<Id, Contact>([SELECT id, name FROM Contact WHERE id in :taskIds]);
    }
    
    private String getTaskColumnField(Task t, Map<Id, Contact> contactMap, String column){
System.debug('getTaskColumnField: ' + column);
        Object o = t.get(column);
        
        String sFld = '';      
        
        if(column == 'WhoId'){
System.debug('contactMap: ' + contactMap);
           	Contact c = contactMap.get(t.whoId);
            sFld = c != null ? c.name : '';
        }
        else{
            if(o instanceof DateTime){
                DateTime dtFld = (DateTime)t.get(column);
                sFld = dtFld.format();                
            }
            else{
                sFld = (String)t.get(column);                
            }                  
        }
        
        return sFld;
    }
        
    list<task> taskFilterByColumn(list<task> foundTasks, String column, String filterValue){
        list<task> filteredTasks = new List<task>();	
System.debug('foundTasks: ' + foundTasks);

        // clear
        clearSearchColumns();
    	// set
        searchColumns.put(column, filterValue);
System.debug('taskFilterByColumn: ' + column + ', is empty: ' + String.isEmpty(column));

        Map<Id, Contact> contactMap = getContactMap(foundTasks);
        if(!String.isEmpty(column)){
            // check matches for column
            for(Task t : foundTasks){
                if(column == 'General'){
                    filteredTasks.add(t);                      
                }
                else{
                    String sFld = getTaskColumnField(t, contactMap, column);
                    
        System.debug('taskFilterByColumn, ' + column + ': ' + sFld + ', match ' + filterValue);            
                        
                    if(sFld != null && sFld.containsIgnoreCase(filterValue)){
                        filteredTasks.add(t);      
                        System.debug('taskFilterByColumn: Yes');            
                    }                
                }
            }            
        }
        
        return filteredTasks;
    }
    
    void taskMashing(list<task> newTasks, String filterColumn, String filterValue){
System.debug('newTasks: ' + newTasks);
    	String taskFields = 'Subject, Summary__c, WhoId, WhatId, TaskSubtype, ActivityDate, Assigned_To_Owner__c, CreatedDate, LastModifiedDate';
//        string query = 'select ' + taskFields + ' from task where whatid=\'' + controller.getid() + '\' and id=:newTasks order by '+sortColumn+' '+sortDirection+' nulls last limit '+string.valueof(queryLimit);
        string query = 'select ' + taskFields + ' from task where id=:newTasks order by '+sortColumn+' '+sortDirection+' nulls last limit '+string.valueof(queryLimit);

System.debug('taskMashing: ' + query);
        tasks=database.query(query);
System.debug('tasks: ' + tasks);
           
        if(filterColumn != null && filterValue != null){
	        tasks = taskFilterByColumn(tasks, filterColumn, filterValue);        
        }
        
        numTasks=tasks.size();
    }
    string getTaskFieldsQueryString(){
        list<string> taskFieldString=new list<string>();
        schema.fieldset taskFieldSet=task.sobjecttype.getdescribe().fieldsets.getmap().get('AccountActivityHistory');
        list<schema.fieldsetmember> taskFieldSetMembers=taskFieldSet.getfields();
        for(schema.fieldsetmember member:taskFieldSetMembers){
System.debug('getTaskFieldsQueryString: ' + member.getfieldpath());
            taskFieldString.add(member.getfieldpath());
        }

        return string.join(taskFieldString,',');
    }
    public pagereference clear(){
        searchText='';
        runQuery();
        return null;
    }
    public void runQuery(){
        sortArrow=(sortDirection=='asc'?'▲':'▼');
//        numTasks=[select count() from task];
        numTasks=[select count() from task where accountId=:controller.getid()];
System.debug('runQuery, numTasks: ' + numTasks);        
        if(numTasks>0){
            numTasksPerPage=50;
            if(numTasks>2000){
                numTasks=2000;
            }
            numPages=numTasks/numTasksPerPage;
            numPages=(numPages==0&&numTasks>0?1:numPages);
            pageList=new list<integer>();
            for(integer i=1;i<=numPages;i++){
                pageList.add(i);
            }
            offset=(currentPage==1?0:(currentPage-1)*numTasksPerPage);
            string query='select '+getTaskFieldsQueryString()+' from task where accountId=\''+controller.getid()+'\' and isclosed=true order by '+sortColumn+' '+sortDirection+' nulls last limit '+string.valueof(queryLimit);
//            string query='select '+getTaskFieldsQueryString()+' from task order by '+sortColumn+' '+sortDirection+' nulls last limit '+string.valueof(queryLimit);
            system.debug('Query:'+query);
            tasks=database.query(query);
        }
    }
    public pagereference toggleSort(){
        sortDirection=(sortDirection=='asc'?'desc':'asc');
        runQuery();
        return null;
    }
    public void skipTo(){
    	runQuery();
    }
    /*
    class SortableTask implements comparable{
        task myTask{get;set;}
        string sortColumn{get;set;}
        string sortDirection{get;set;}
        public SortableTask(task myTask,string sortColumn,string sortDirection){
            this.myTask=myTask;
            this.sortColumn=sortColumn;
            this.sortDirection=sortDirection;
        }
        public integer compareTo(object obj){
            task compareToTask=(task)obj;
            if(myTask.get(sortColumn)==null&&compareToTask.get(sortColumn)==null){
                return 0;
            }else if(myTask.get(sortColumn)!=null&&compareToTask.get(sortColumn)==null){
                return (sortDirection.equalsignorecase('asc')?1:-1);
            }else if(myTask.get(sortColumn)!=null&&compareToTask.get(sortColumn)!=null){
                if(myTask.get(sortColumn)>compareToTask.get(sortColumn)){
                    return (sortDirection.equalsignorecase('asc')?1:-1);
                }else if(myTask.get(sortColumn)<compareToTask.get(sortColumn)){
                    return (sortDirection.equalsignorecase('asc')?-1:1);
                }else{
                    return 0;
                }
            }
        }
    }*/
}