/**
 * LimitedViewCaseWrapper.cls - global class required for custom case related list in 
 *								CC visibility project limited view vf 'page layout'
 * @description - 	required to facilitate the ability to sort the list of LimitedViewCaseWrappers
 *					for display on the page. Sorts by latest case number.
 *
 * @author Ryan Draper, Traction on Demand
 * @date 2013-08-12
 */
global class LimitedViewCaseWrapper implements comparable{
		public Datetime createdDateTime {get;set;}
    	public String createdDate {get;set;}
    	public String caseNumber {get;set;}
    	public String caseStatus {get;set;}
    	public String caseSubject {get;set;}
    	
    	public LimitedViewCaseWrapper(Case limitedCase){
    		createdDateTime = limitedCase.CreatedDate;
    		createdDate = String.valueOf(DateTime.valueOf(limitedCase.CreatedDate).format());
    		caseNumber = limitedCase.CaseNumber;
    		caseStatus = limitedCase.Status;
    		caseSubject = limitedCase.Subject;
    	}
    	
    	global Integer compareTo(Object compareTo){
    		LimitedViewCaseWrapper cw = (LimitedViewCaseWrapper)compareTo;
    		if(createdDateTime == cw.createdDateTime) return 0;
    		if(createdDateTime < cw.createdDateTime) return 1;
    		return -1;
    	}
    }