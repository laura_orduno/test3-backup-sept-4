@isTest
private class trac_TaskTest {

    static testMethod void taskTest() {

        User u1 = [SELECT Id FROM User WHERE IsActive = true AND Profile.Name = 'Sales Prime / ASR / Coordinator' LIMIT 1];
        User u2 = [SELECT Id FROM User WHERE IsActive = true AND LastName like '%Integration%' LIMIT 1];
        
        Contact myCon = new Contact();
        
        myCon.FirstName = 'Test';
        myCon.LastName = 'Test';
        insert myCon;
        
        ContactVO myVO = new ContactVO(myCon);
        
        string sGet;
        boolean bGet;
        Id idGet;
        
        myVO.FirstName = 'Test';
        myVO.LastName = 'Test';
        myVO.Title = 'Test';
        myVO.Role = 'Test';
        myVO.Department = 'Test';
        myVO.TitleCategory = 'Test';
        myVO.Language = 'Test';
        myVO.ContactMethod = 'Test';
        myVO.Phone = '111-111-1111';
        myVO.OtherPhone = '111-111-1111';
        myVO.Fax = '111-111-1111';
        myVO.MobilePhone = '111-111-1111';
        myVO.EMail = 'test@test.com';
        myVO.MailingStreet = 'Street';
        myVO.MailingCity = 'City';
        myVO.MailingProvince = 'Province';
        myVO.MailingPostalCode = 'PostalCode';
        myVO.MailingCountry = 'Country';
        myVO.OptIn = true;

        sGet = myVO.FirstName;
        sGet = myVO.LastName;
        sGet = myVO.Title;
        sGet = myVO.Role;
        sGet = myVO.Department;
        sGet = myVO.TitleCategory;
        sGet = myVO.Language;
        sGet = myVO.ContactMethod;
        sGet = myVO.Phone;
        sGet = myVO.OtherPhone;
        sGet = myVO.Fax;
        sGet = myVO.MobilePhone;
        sGet = myVO.EMail;
        sGet = myVO.MailingStreet;
        sGet = myVO.MailingCity;
        sGet = myVO.MailingProvince;
        sGet = myVO.MailingPostalCode;
        sGet = myVO.MailingCountry;
        bGet = myVO.OptIn;
        
        Account myAcc = new Account();
        
        myAcc.Name = 'Test';
        myAcc.RCID__c = 'TEST';
        insert myAcc;
        
        System.RunAs(u2) {
            myAcc.OwnerId = u2.Id;
            update myAcc;
        }
        trac_Task_Assigner tta2 = new trac_Task_Assigner(new ApexPages.StandardController(new Task() ));
        tta2.autoRun();
        
        Task t = new Task(OwnerId = u1.Id, Subject = 'Other', WhatId = myAcc.id);
        insert t;
        t.OwnerId = u2.Id;
        update t;
        ApexPages.currentPage().getParameters().put('id', t.id);
        trac_Task_Assigner tta = new trac_Task_Assigner(new ApexPages.StandardController(new Task() ));
        tta.autoRun();
        Task t2 = [SELECT Id, OwnerId, Status FROM Task WHERE Id = :t.id];
        System.assertEquals(t2.OwnerId, UserInfo.getUserId());
        t2.Status = 'Completed';
        update t2;
    }
}