//
// NHP Email Service
//
global class OrderMgrEmailService implements Messaging.InboundEmailHandler {
    
    public static String NHPTaskRecordType;
    public static String NHPWorkRequestRecordType;
    public static String NHPDefaultOwner;
    public static String NHPOwnerGroup;
    public static String NHPOrderManagerRole;
    public static String taskOwnerId;
    public static String groupQueueId;
    
    public static RecordType taskRecordType;
    public static RecordType workRequestRecordType;
    
    static {
        initialize();
    }
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,  Messaging.Inboundenvelope envelope) {
        return processEmail(email);
    }
    
    global Messaging.InboundEmailResult processEmail(Messaging.InboundEmail email) {
        Order ordObj;
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        system.debug('OrderMgrEmailService:received email:'+result);
        system.debug('OrderMgrEmailService:email.subject:'+email.subject);
        system.debug('OrderMgrEmailService:email.plainTextBody:'+email.plainTextBody);

        try {
            String orderNumber;
            if(email.subject.containsIgnoreCase('OR-'))
                orderNumber = getOrderNumber(email.subject);
            else if(email.plainTextBody.containsIgnoreCase('OR-'))
                orderNumber = getOrderNumber(email.plainTextBody);
            try {
                if(String.isNotBlank(orderNumber)) 
                    ordObj = [select Id,Name,OrderNumber,AccountId from Order where OrderNumber like :orderNumber];
            }
            catch(Exception e) {}
            
            if(ordObj == null) {
                createWorkRequest(email,ordObj);
            }
            else {
                // if there is an Order Manager associated with this Order,
                // - save email as a note attachment
                // - create task to notify OM
                Cloud_Enablement_Team_Member__c teamMember;
                system.debug('NHPOrderManagerRole='+NHPOrderManagerRole);
                system.debug('ordObj.Id='+ordObj.Id);

                try {
                    teamMember = [select id,Team_Member__c,Team_Name__c,Order__c
                                      from Cloud_Enablement_Team_Member__c
                                      where Order__c = :ordObj.Id and Team_Name__c =: NHPOrderManagerRole limit 1];
                }
                catch(Exception e) {
                    teamMember = null;
                }
                
                if(teamMember != null) {
                    createTask(email,ordObj,teamMember);
                }
                else {
                    createWorkRequest(email,ordObj);
                }
                createNote(email,ordObj);
                saveAttachments(email,ordObj.Id);
            }
            
            result.success = true;
            //result.message = ('Email processed successfully');
        }
        catch (Exception e) {
            result.success = false;
            //result.message = 'Error received while processing email: ' + e.getMessage();
        }
        
        return result;
    }

    public void createNote(Messaging.InboundEmail email, Order ordObj) {
        Note note = new Note();
        note.Title = email.fromName + ' : ' + email.subject;
        note.Body = email.plainTextBody;
        note.ParentId = ordObj.Id;
        system.debug('OrderMgrEmailService:Created note for email.subject:'+email.subject
                     +':note:'+note);
        insert note;
    }

    public void createTask(Messaging.InboundEmail email, Order ordObj,Cloud_Enablement_Team_Member__c teamMember) {
        Task taskRec = new Task();
        if(taskRecordType != null)
            taskRec.RecordTypeId = taskRecordType.Id;
        String emailTime = '';
        if(email.headers != null) {
            for(Messaging.InboundEmail.Header hitem : email.headers) {
                if(String.isNotBlank(hitem.name) && hitem.name == 'Date') {
                    emailTime = hitem.value;
                    break;
                }
            }
        }
        taskRec.Priority = 'Normal';
        taskRec.Activity_Type__c = 'Email Inbound';
        taskRec.Status = 'Not Started';
        taskRec.WhatId = ordObj.Id;
        taskrec.Subject = 'NHP Email: ' + email.fromAddress + ': ' + emailTime;
        taskrec.Description = 'From: ' + email.fromName + ' (' + DateTime.now() + ')\n'
            + ' Subject: ' + email.subject
            + email.plainTextBody;
        Id tskOwnId = teamMember.Team_Member__c != null
            ? teamMember.Team_Member__c : taskOwnerId;
        taskRec.OwnerId = tskOwnId;
        Date dt = Date.today();
        taskRec.ActivityDate = dt.addDays(1);
        system.debug('OrderMgrEmailService:createTask:Created task for email.subject:' + email.subject + ':taskRec:'+taskRec);
        insert taskRec;
    }

    public void createWorkRequest(Messaging.InboundEmail email, Order ordObj) {
        // - create a work request for case assessment
        // - "email to be assigned to an Order Manager"
        Work_Request__c wreq = new Work_Request__c();
        wreq.OwnerId = groupQueueId;
        if(ordObj != null) {
            if(ordObj.AccountId != null)
                wreq.Account__c = ordObj.AccountId;
            wreq.Order__c = ordObj.Id;
        }
        wreq.Subject__c = email.subject;
        wreq.Description__c = 'From: ' + email.fromName + ' (' + DateTime.now() + ')\n' +
            'Subject: ' + email.subject + '\n\nBody:' + email.plainTextBody;
        
        wreq.Transfer_to_Queue__c = NHPOwnerGroup;
        wreq.Type__c = System.Label.OCOM_Work_Request_Type; // 'NHP Support'
        if(workRequestRecordType != null)
            wreq.RecordTypeId = workRequestRecordType.id;
        insert wreq;
        system.debug('createWorkRequest:Created wreq:'+wreq);

        saveAttachments(email,wreq.Id);
    }
    
    public void saveAttachments(Messaging.InboundEmail email, Id objId) {
        try{
            for (Messaging.Inboundemail.TextAttachment  tAttachment :email.textAttachments) {
                Attachment attachment = new Attachment();
                
                attachment.Name = tAttachment.fileName;
                attachment.Body = Blob.valueOf(tAttachment.body);
                attachment.ParentId = objId;
                insert attachment;
            }
        }
        catch(Exception e) {}
        try {
            for (Messaging.Inboundemail.BinaryAttachment  bAttachment :email.binaryAttachments) {
                Attachment attachment = new Attachment();
                
                attachment.Name = bAttachment.fileName;
                attachment.Body = bAttachment.body;
                attachment.ParentId = objId;
                insert attachment;
            }
        }
        catch(Exception e){}
    }
    
    public String getOrderNumber(String textContent) {
        String orderMarker = 'OR-';
        String result;
        Integer startIdx = textContent.indexOfIgnoreCase(orderMarker);
        if(startIdx >= 0) {
            startIdx += 3;
            if(startIdx < textContent.length()) {
                String secondPart = textContent.substring(startIdx);
                if(String.isNotBlank(secondPart)) {
                    String[] token = secondPart.split('\\W\\s*|_');
                    if(token.size() > 0)
                        result = orderMarker + token.get(0);
                }
            }
        }
        return result;
    }

    public static void initialize(){
        system.debug('initialize():intializing class');
        
        try{
            for(NHP_Setting__mdt itm: [select DeveloperName , value__c from NHP_Setting__mdt]){
                if(itm.DeveloperName.equalsIgnoreCase('Task_Record_Type'))
                    NHPTaskRecordType = itm.value__c;
                else if(itm.DeveloperName.equalsIgnoreCase('Work_Request_Record_Type'))
                    NHPWorkRequestRecordType = itm.value__c;
                else if(itm.DeveloperName.equalsIgnoreCase('Default_WorkReq_Owner'))
                    NHPDefaultOwner = itm.value__c;
                else if(itm.DeveloperName.equalsIgnoreCase('Group_Queue'))
                    NHPOwnerGroup = itm.value__c;
                else if(itm.DeveloperName.equalsIgnoreCase('Order_Manager_Role'))
                    NHPOrderManagerRole = itm.value__c;
                
                system.debug('initialize():Loading Custom Metadata Types: '+ itm.DeveloperName  + ' = '+itm.value__c);
            }
        }
        catch (Exception e) {
            System.debug('Initialize member data: Query custom metadata types: received exception: ' + e.getMessage());
        }
        try{
            taskRecordType = [select Id, DeveloperName from RecordType where SObjectType = 'Task' and DeveloperName = : NHPTaskRecordType and IsActive = true limit 1];
        }
        catch (Exception e) {
            System.debug('Initialize member data: Query Task record type: received exception: ' + e.getMessage());
        }
        try{
            workRequestRecordType = [select Id, DeveloperName from RecordType where SObjectType = 'Work_Request__c' and DeveloperName = : NHPWorkRequestRecordType and IsActive = true limit 1];
        }
        catch (Exception e) {
            System.debug('Initialize member data: Query Work Request record type: received exception: ' + e.getMessage());
        }
            
        User ownerDefault = new User();
        Group ownerGroup = new Group();

        try{
            ownerDefault = [select Id from User where name = : NHPDefaultOwner limit 1];
            if(ownerDefault != null){
                taskOwnerId = ownerDefault.Id;
            }
            else{
                system.debug('Cannot find user ID for NHPDefaultOwner: '+NHPDefaultOwner);
                taskOwnerId = id.valueof('');
            }
        }
        catch (Exception e) {
            System.debug('Initialize member data: Query Task Owner: received exception: ' + e.getMessage());
        }

        try{
            ownerGroup = [select Id from group where DeveloperName = : NHPOwnerGroup limit 1];
            if(ownerGroup != null){
                groupQueueId = ownerGroup.Id;
            }
            else{
                system.debug('Cannot find group ID for NHPOwnerGroup: '+NHPOwnerGroup);
                if(ownerDefault != null)
                    groupQueueId = ownerDefault.Id;
            }
        }
        catch (Exception e) {
            System.debug('Initialize member data: Query Group/Queue ID: received exception: ' + e.getMessage());
        }
    }
}