/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * Name - GlobysBillAnalyzerHistoryChart_Test
 * 1. Created By - Sandip - 16 Aug 2016
 * 2. Modified By 
 */
 
@isTest
public class GlobysBillAnalyzerHistoryChart_Test implements HttpCalloutMock{
    private HttpResponse resp;
    
    public GlobysBillAnalyzerHistoryChart_Test(String testBody, Integer statusCode, String status) {
        
        resp = new HttpResponse();
        resp.setBody(testBody);
        resp.setStatusCode(statusCode);
        resp.setStatus(status);
    }
    
    public HTTPResponse respond(HTTPRequest req) {
        return resp;
    }
    
    public static void createConfig(){
        List<GlobysConfig__c> custObjList = new List<GlobysConfig__c>();
        GlobysConfig__c obj1 = new GlobysConfig__c (Name = 'GlobysPortalURL', value__c = '  https://telus1015.uat.globysonline.com/auth/saml/SingleSignOn?account=');
        custObjList.add(obj1);
        GlobysConfig__c obj2 = new GlobysConfig__c (Name = 'ReportClass', value__c = 'Summary');
        custObjList.add(obj2);
        GlobysConfig__c obj3 = new GlobysConfig__c (Name = 'ReportId', value__c = '12345');
        custObjList.add(obj3);
        GlobysConfig__c obj4 = new GlobysConfig__c (Name = 'TokenURL', value__c = 'https://telus1015.uat.globysonline.com/auth/oidc/connect/token');
        custObjList.add(obj4);
        GlobysConfig__c obj5 = new GlobysConfig__c (Name = 'ReportURL', value__c = 'https://telus1015.uat.globysonline.com/reports/v1/execute');
        custObjList.add(obj5);
        Database.insert(custObjList, false);
    }
    public static testMethod void testGlobysCBUCID () {
        createConfig();
        Account cbucid = new Account(
            Name = 'RCID Test',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CBUCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            CBUCID_Parent_Id__c = 'xxx1212_xx33'
        );
        insert cbucid; 
        
        String testBody = '{"columns":[{"access_token":"test", "id": "2","text": "Owner","justify": "Left","sortable": true},{"id": "3","text": "Balance","justify": "Right","sortable": true}],"sort": [{"id": "3","direction": "Desc"}],'+
                        '"rows": [{"data": ["January","David",{"c": "USD","v": 35.403}],"metadata": {"accountId": 53}}],"groups": [{"id": "1","text": "Month","justify": "Left","sortable": false}],'+
                        '"start": 50,"rowCount": 25,"totalRows": 154,"metadata": {"currencyConverted": true}}';


        test.startTest();
        GlobysBillAnalyzerHistoryChartController gobj = new GlobysBillAnalyzerHistoryChartController();
        HttpCalloutMock mock = new GlobysBillAnalyzerHistoryChart_Test(testBody,200,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        //String cbucidno = GlobysBillAnalyzerHistoryChartController.getcbucidNumber(cbucid.Id,'CBUCID');
        GlobysBillAnalyzerHistoryChartController.initChartReportService(cbucid.Id,'CBUCID');
        test.stopTest();
    }
    
    public static testMethod void testGlobysRCID () {
        createConfig();
        Account cbucid = new Account(
            Name = 'RCID Test',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CBUCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            CBUCID_Parent_Id__c = 'xxx1212_xx33'
        );
        insert cbucid; 
        
        Account rcid = new Account(
            Name = 'RCID Test',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            RCID__c = '987654_xx',
            parentId = cbucid.Id
        );
        insert rcid; 
        
        String testBody = '{"columns":[{"id": "2","text": "Owner","justify": "Left","sortable": true},{"id": "3","text": "Balance","justify": "Right","sortable": true}],"sort": [{"id": "3","direction": "Desc"}],'+
                        '"rows": [{"data": ["January","David",{"c": "USD","v": 35.403}],"metadata": {"accountId": 53}}],"groups": [{"id": "1","text": "Month","justify": "Left","sortable": false}],'+
                        '"start": 50,"rowCount": 25,"totalRows": 154,"metadata": {"currencyConverted": true}}';


        test.startTest();
        HttpCalloutMock mock = new GlobysBillAnalyzerHistoryChart_Test(testBody,200,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        //String cbucidno = GlobysBillAnalyzerHistoryChartController.getcbucidNumber(rcid.Id,'RCID');
        GlobysBillAnalyzerHistoryChartController.initChartReportService(rcid.Id,'RCID');
        test.stopTest();
    }
    
    public static testMethod void testGlobysCAN () {
        createConfig();
        Account cbucid = new Account(
            Name = 'RCID Test',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CBUCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            CBUCID_Parent_Id__c = 'xxx1212_xx33'
        );
        insert cbucid; 
        
        Account rcid = new Account(
            Name = 'RCID Test',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            RCID__c = '987654_xx',
            parentId = cbucid.Id
        );
        insert rcid; 
        
        Account canID = new Account(
            Name = 'RCID Test',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CAN').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            BAN_CAN__c = '987654test_xx',
            parentId = rcid.Id
        );
        insert canID; 
        
        String testBody = '{"columns":[{"id": "2","text": "Owner","justify": "Left","sortable": true},{"id": "3","text": "Balance","justify": "Right","sortable": true}],"sort": [{"id": "3","direction": "Desc"}],'+
                        '"rows": [{"data": ["January","David",{"c": "USD","v": 35.403}],"metadata": {"accountId": 53}}],"groups": [{"id": "1","text": "Month","justify": "Left","sortable": false}],'+
                        '"start": 50,"rowCount": 25,"totalRows": 154,"metadata": {"currencyConverted": true}}';


        test.startTest();
        HttpCalloutMock mock = new GlobysBillAnalyzerHistoryChart_Test(testBody,200,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        //String cbucidno = GlobysBillAnalyzerHistoryChartController.getcbucidNumber(canId.Id,'CAN');
        GlobysBillAnalyzerHistoryChartController.initChartReportService(canId.Id,'CAN');
        test.stopTest();
    }
    
    public static testMethod void testGlobysCBUCIDBadReq () {
        createConfig();
        Account cbucid = new Account(
            Name = 'RCID Test',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CBUCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            CBUCID_Parent_Id__c = 'xxx1212_xx33'
        );
        insert cbucid; 
        
        String testBody = '{"message":"test msg"}';


        test.startTest();
        GlobysBillAnalyzerHistoryChartController gobj = new GlobysBillAnalyzerHistoryChartController();
        HttpCalloutMock mock = new GlobysBillAnalyzerHistoryChart_Test(testBody,500,'Error');
        Test.setMock(HttpCalloutMock.class, mock);
        //String cbucidno = GlobysBillAnalyzerHistoryChartController.getcbucidNumber(cbucid.Id,'CBUCID');
        GlobysBillAnalyzerHistoryChartController.initChartReportService(cbucid.Id,'CBUCID');
        test.stopTest();
    }
    
    public static testMethod void testGlobysException () {
        createConfig();
        Account cbucid = new Account(
            Name = 'RCID Test',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('CBUCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            CBUCID_Parent_Id__c = 'xxx1212_xx33'
        );
        insert cbucid; 
        
        String testBody = 'test invalid json';


        test.startTest();
        GlobysBillAnalyzerHistoryChartController gobj = new GlobysBillAnalyzerHistoryChartController();
        HttpCalloutMock mock = new GlobysBillAnalyzerHistoryChart_Test(testBody,200,'OK');
        Test.setMock(HttpCalloutMock.class, mock);
        //String cbucidno = GlobysBillAnalyzerHistoryChartController.getcbucidNumber(cbucid.Id,'CBUCID');
        GlobysBillAnalyzerHistoryChartController.initChartReportService(cbucid.Id,'CBUCID');
        test.stopTest();
    }
    
}