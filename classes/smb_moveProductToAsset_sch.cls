/*
###########################################################################
# File..................: smb_moveProductToAsset_sch
# Version...............: 26
# Created by............: Vinay Sharma
# Created Date..........: 19-Mar-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This is a scheduler class which runs at a specific interval 
to collect quotes which are modified, inserted after last run date of this schedule job
#
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
###########################################################################
*/
global class smb_moveProductToAsset_sch implements Schedulable
{
    global void execute(SchedulableContext SC)
    {
        
        AggregateResult lastRunDateAssetJob = [Select MAX(CreatedDate) CDate From AsyncApexJob a Where a.ApexClass.Name = 'smb_moveProductToAsset_Batch'];
        
        Datetime lastRun = (Datetime) lastRunDateAssetJob.get('CDate');

        String query;
        
        query  = 'select id,'+
                        'CreatedById,'+
                        'CreatedDate,'+
                        'LastModifiedById,'+
                        'LastModifiedDate,'+
                        'SBQQ__NetTotal__c,'+
                        'SBQQ__Quantity__c,'+
                        'SBQQ__EndDate__c,'+
                        'SBQQ__Product__c,'+ 
                        
                        'SBQQ__Product__r.name,'+
                        'SBQQ__Product__r.CurrencyIsoCode,'+
                        'SBQQ__Product__r.Description,'+
                        
                        'SBQQ__RequiredBy__c,'+
                        'SBQQ__RequiredBy__r.name, '+
                        'SBQQ__RequiredBy__r.SBQQ__Product__c,'+
                        
                        'SBQQ__Quote__r.id,'+
                        'SBQQ__Quote__r.SBQQ__Account__c,'+
                        'SBQQ__Quote__r.SBQQ__PrimaryContact__c,'+ 
                        'SBQQ__Quote__r.SBQQ__Status__c,'+
                        'SBQQ__Quote__r.SBQQ__EndDate__c,'+
                        'SBQQ__Quote__r.LastModifiedDate,'+
                        'SBQQ__Quote__r.Completed__c,'+
                        'SBQQ__Quote__r.SBQQ__Opportunity__r.Web_Account__r.RCID__c'+' '+
                        'FROM SBQQ__QuoteLine__c '+' '+
                        'WHERE SBQQ__Quote__r.Completed__c = true'+' ';
                        
                        if(null !=lastRun)
                        { 
                            string formatLastRun = lastRun.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
                            query = query+'AND LastModifiedDate > ' + formatLastRun;
                        }                   
                         
        system.debug('---query---'+query); 
        
        //execute batch
        smb_moveProductToAsset_Batch runBatch = new smb_moveProductToAsset_Batch(query);   
        database.executebatch(runBatch,200);

    }
}