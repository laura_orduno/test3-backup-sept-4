/**
* @author Robert Brekke - robert.brekke@telus.com
* @description Unit Test for VTILcareNavCtlr.cls
*/
@isTest(SeeAllData=false)
public class VITILcareNavCtlr_Test {
@isTest static void testCatListPull() {
   //System.runAs(MBRTestUtils.createPortalUser('')) {
       Account a = new Account(Name = 'Unit Test Account');
      
        insert a;
     //  Account a = [Select id from Account where Name = 'Unit Test Account' LIMIT 1];
        system.debug('Account Select = :' + a);
       // system.debug('a.id = :' + a.id);
        Contact c = new Contact(Email = 'test@unit-test.com', LastName = 'Test', AccountId = a.Id);
        insert c;
        Account_Configuration__c ac = new Account_Configuration__c(Zenoss_URL__c = 'Test@test.com', Account__c = a.Id);
        system.debug('Account Conifg = :' + ac);
        insert ac;
        Profile p = [SELECT Id FROM Profile WHERE Name='Customer Community User Custom' AND UserType='CSPLitePortal' LIMIT 1]; 

//      UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];

        User u = new User(Alias = 'TestUser', Email='Test@unit-test.com', ContactId=c.Id, 
                            EmailEncodingKey='UTF-8', FirstName='Unit', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', CommunityNickname='UNTST', /*UserRoleId = portalRole.Id,*/ ProfileId=p.Id, 
                            TimeZoneSidKey='America/Los_Angeles', 
                            UserName='Test@unit-test.com');

        u.Customer_Portal_Role__C = 'tq';
        
        insert u;
       
    System.runAs(u) {
     
    VITILcareNavCtlr ctlr = new VITILcareNavCtlr();
  }
       
}
}