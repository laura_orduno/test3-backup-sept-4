@isTest(SeeAllData=true)
public class CloseBillingDateCheckExtensionTest {
	testMethod static void testCloseBillingDateCheckExtension1() {
        Account acc = TestUtil.createAccount();
        Opportunity opp = TestUtil.createOpportunity(acc.Id);
        Opportunity_Solution__c solution = TestUtil.createOpportunitySolution(opp.Id);
        List<Opp_Product_Item__c> items = TestUtil.createPLIList(opp.Id);
        
        PageReference pageRef = Page.CloseBillingDateCheck;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('solutionId', solution.id);
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        CloseBillingDateCheckExtension c = new CloseBillingDateCheckExtension(sc);
        
        c.getOppProductItems();
        c.errorLevel = 'WARNING';
        c.messageName = 'Test';
        c.showMessage();
        c.saveOpp();
        
    }
    
    testMethod static void testCloseBillingDateCheckExtension2() {
        Account acc = TestUtil.createAccount();
        Opportunity opp = TestUtil.createOpportunity(acc.Id);
        Opportunity_Solution__c solution = TestUtil.createOpportunitySolution(opp.Id);
        List<Opp_Product_Item__c> items = TestUtil.createPLIList(opp.Id);
        
        PageReference pageRef = Page.CloseBillingDateCheck;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('solutionId', solution.id);
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        CloseBillingDateCheckExtension c = new CloseBillingDateCheckExtension(sc);
        
        c.getOppProductItems();
        
        c.inlineSave();
        
    }
    
    testMethod static void testCloseBillingDateCheckExtension3() {
        Account acc = TestUtil.createAccount();
        Opportunity opp = TestUtil.createOpportunity(acc.Id);
        Opportunity_Solution__c solution = TestUtil.createOpportunitySolution(opp.Id);
        List<Opp_Product_Item__c> items = TestUtil.createPLIList(opp.Id);
        
        PageReference pageRef = Page.CloseBillingDateCheck;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('solutionId', solution.id);
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        CloseBillingDateCheckExtension c = new CloseBillingDateCheckExtension(sc);
        
        c.getOppProductItems();
        
        c.saveDates();
        
    }
    
    testMethod static void testCloseBillingDateCheckExtension4() {
        Account acc = TestUtil.createAccount();
        Opportunity opp = TestUtil.createOpportunity(acc.Id);
        Opportunity_Solution__c solution = TestUtil.createOpportunitySolution(opp.Id);
        List<Opp_Product_Item__c> items = TestUtil.createPLIList(opp.Id);
        
        PageReference pageRef = Page.CloseBillingDateCheck;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('solutionId', solution.id);
        ApexPages.currentPage().getParameters().put('retURL', '/'+opp.id);
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        CloseBillingDateCheckExtension c = new CloseBillingDateCheckExtension(sc);
        
        c.getOppProductItems();
        
        c.saveDates();
        
    }
}