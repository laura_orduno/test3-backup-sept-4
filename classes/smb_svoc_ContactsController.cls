public with sharing class smb_svoc_ContactsController {
	
	public List<Contact> Contacts {get;set;}
	public Id accountId {get;set;}
	
	public smb_svoc_ContactsController() {
		initialise();
	}
	
	private void initialise() {
		initialiseAccountId();
		initialiseContacts();
	}
	
	private void initialiseAccountId() {
		Id acctId = ApexPages.currentPage().getParameters().get('accountId');
		if (acctId == null) {
			throw new smb_ParameterNotSpecifiedException('accountId must be passed in the query string');
		}
		accountId = acctId;
	}
	
	private void initialiseContacts() {
		Contacts = [SELECT Id, Name, contact.Email, contact.Phone, 
						   Account.Name, Account.BAN_CAN__c, Account.CBUCID_RCID__c,
						   Account.RecordType.Name
					FROM Contact
					WHERE AccountId IN :smb_AccountUtility.getAccountsInHierarchy(accountId) limit 200];
	}

}