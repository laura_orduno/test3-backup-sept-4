public with sharing class Quote_PAC_ServiceAddress_Controller {
public boolean showOrhide {get;set;}
public SMBCare_Address__c smbCareAddr {get; set;}
public ServiceAddressData SAData  { get; set; }
//public ServiceAddressData SAData1  { get; set; }
public AddressData PAData  { get; set; }
public quote Qttoupdate    { get; set; }
ApexPages.StandardController stdCtrl;
   
    public Quote_PAC_ServiceAddress_Controller (){    
       system.debug('###-in Const 1');
        SAData  = new ServiceAddressData();
        //SAData1  = new ServiceAddressData();
        PAData  = new AddressData();
        SAData.address='test for UseAs';
        SAData.province='AB';
        SAData.city='geor';
        SAData.postalCode='XJDI';
        showOrhide=false;
        PAData.isCityRequired = true;
        smbCareAddr = new SMBCare_Address__c();
        smbCareAddr.Address_Type__c = 'Standard'; // Service Address Type defaults to Standard.
        smbCareAddr.Status__c='Valid';
        smbCareAddr.recordtypeid = Schema.SObjectType.SMBCare_Address__c.getRecordTypeInfosByName().get('Address').getRecordTypeId();       
    }
    
   public Quote_PAC_ServiceAddress_Controller(ApexPages.StandardController controller){
       system.debug('###-in Const 2');
        stdCtrl = controller;
        Qttoupdate = (quote ) controller.getRecord();  
        system.debug('###-Qttoupdate in Const 2: ' + Qttoupdate );
        SAData  = new ServiceAddressData();
        SAData.isPostalCodeRequired = true;
        SAData.isSearchStringRequired = true;
        PAData  = new AddressData();
        SAData.address='test for UseAs';
        SAData.province='AB';
        SAData.city='geor';
        SAData.postalCode='XJDI';
        showOrhide=false;
        PAData.isCityRequired = true;
        smbCareAddr = new SMBCare_Address__c();
        smbCareAddr.Address_Type__c = 'Standard'; // Service Address Type defaults to Standard.
        smbCareAddr.Status__c='Valid';
        smbCareAddr.recordtypeid = Schema.SObjectType.SMBCare_Address__c.getRecordTypeInfosByName().get('Address').getRecordTypeId();
        
        system.debug('###-smbCareAddr in Const 2: ' + smbCareAddr);
   }
    
  
        public Pagereference Save(){
            system.debug('###-in Save1: ' + Qttoupdate);
             quote qttoupdate1=[select id,Service_Address__c,account.id from quote where id=:Qttoupdate.id];
              system.debug('###-in Save2: ' + qttoupdate1);
              system.debug('###-in Save2: ' + SAData);
            List<SMBCare_Address__c> existingAddress=[select id from SMBCare_Address__c where FMS_Address_ID__c=:SAData.fmsId and  Account__c =:Qttoupdate1.account.id limit 1];
            if(!(existingAddress.size()>0)){
                system.debug('###-in Save if, exixting:'+existingAddress.size());
                smbCareAddr.Postal_Code__c = SAData.postalCode;
                smbCareAddr.Block__c= SAData.buildingNumber;
                smbCareAddr.city__c= SAData.city;
                smbCareAddr.Street__c= SAData.street;
                smbCareAddr.Province__c=   SAData.province;
                smbCareAddr.Country__c= SAData.country;
                smbCareAddr.FMS_Address_ID__c= SAData.fmsId;
                smbCareAddr.Status__c ='Valid';
                system.debug('#####'+smbCareAddr);
                system.debug('#####SAData##3'+SAData);
                smbCareAddr.Account__c =Qttoupdate1.account.id;
                insert smbCareAddr;
               Qttoupdate1.Service_Address__c=smbCareAddr.id;
            }else{
                system.debug('###-in Save else');
                Qttoupdate1.Service_Address__c=existingAddress[0].id;
            }
            
            Qttoupdate1.Service_Address1__c=SAData.address;
            try{            
                 update Qttoupdate1;
                 system.debug('@@@@india'+Qttoupdate1);
                 Pagereference pref= new pagereference('/apex/Quote_PAC_ServiceAddress?id='+Qttoupdate1.id);                         
                 return pref; 
                }
                catch(exception e)
                {
                 return null;
                } 
           
        }  
  public PageReference doDeviceDetection() {

        PageReference result = null;

        Boolean isMobileDevice = false;
        String userAgent = ApexPages.currentPage().getHeaders().get('USER-AGENT');

        if( (userAgent.contains('iPhone')) || (userAgent.contains('iPad')) || (userAgent.contains('Android')) ) {
            isMobileDevice = true;
        }

        if( isMobileDevice ) {
           result = stdCtrl.view();
           result.getParameters().put('sfdc.override', '1');

        }

        return result;   
    }  
    public void showPanel()
    {showOrhide=true;}
     
}