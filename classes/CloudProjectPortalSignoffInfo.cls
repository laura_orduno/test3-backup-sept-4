public without sharing class CloudProjectPortalSignoffInfo {
    public class CloudProjectPortalException extends Exception {}

    public Boolean loaded {get; private set;}
    public Boolean canComplete {get; private set;}

    private Boolean isTask {get; set;}
    private Cloud_Project_Sign_Off__c task {get; set;}
    private Note newNote {get; set;}
    private SMET_Requirement__c kd {get; set;}

    public List<Attachment> attachs {get; set;}
    public List<Note> notes {get; set;}
    public Set<Id> userIds = new Set<Id>();
    Map<id,User> idToUser;
    
    public CloudProjectPortalSignoffInfo() {
        
    }
    public void onLoad() {
        Savepoint sp = database.setSavepoint();
        try {
            loaded = false;
            if (ApexPages.currentPage() == null || ApexPages.currentPage().getParameters() == null) { clear(); return; }
            String oid = ApexPages.currentPage().getParameters().get('oid');
            String tok = ApexPages.currentPage().getParameters().get('tok');
            if (tok == null || oid == null /*|| oid.length() != 18 */|| oid.substring(0, 3) != 'a2x') { clear(); return; }
            Id xid = (Id) oid;
            newNote = new Note();
            task = [Select Project_Name__c, Role__c, Status__c, Cloud_Project_Key_Deliverable__c, KD_Name__c, Asignee_Name__c, Assignee_Email__c, Token__c, CreatedBy.Name From Cloud_Project_Sign_Off__c Where Id = :xid Limit 1];
            if (task == null) { clear(); throw new CloudProjectPortalException('Unable to load task'); return; }
            if (task.Token__c != tok) { clear(); throw new CloudProjectPortalException('Unable to load task.'); return; }
            attachs = [select Id,Name,Body,BodyLength,Description,ParentId,CreatedDate,LastModifiedDate from Attachment where ParentId = :task.Cloud_Project_Key_Deliverable__c order by LastModifiedDate desc];
            notes = [select ownerid,Id,title,body,ParentId,CreatedDate,LastModifiedDate from Note where ParentId = :task.id order by LastModifiedDate desc];
            for(Note tempNote:notes){
            	if(tempNote.ownerid != null) userIds.add(tempNote.ownerid);
            }
            idToUser = new Map<id,User>([Select Name From User Where Id IN :userIds]);
            if (task.Cloud_Project_Key_Deliverable__c != null) {
                kd = [Select Name From SMET_Requirement__c Where Id = :task.Cloud_Project_Key_Deliverable__c];
            }
                        
            canComplete = (task.status__c != 'Completed');   
            loaded = true;
        } catch (Exception e) { Database.rollback(sp); QuoteUtilities.log(e); }
    }
    
    public List<Attachment> getAttachmentList(){
        return attachs;
    }

    public List<Note> getNoteList(){
    	if(notes==null) return notes;
    	for(Note temp:notes){
    		String tempName = idToUser.get(temp.ownerid).Name;
    		if(idToUser.get(temp.ownerid).Name.contains('Guest User')) tempName = getRelatedToName();
    		temp.body = 'Comment from '+tempName;
    	}
        return notes;
    }
        
    public PageReference onComplete() {
        try {
            if (task == null) { throw new CloudProjectPortalException('No task loaded'); return null;}
            task.status__c = 'Completed';
            update task;
            newNote.parentId = task.id;
            newNote.body = 'Comment from '+getRelatedToName();
            newNote.isPrivate=false;
            if(newNote.title != null && newNote.title != '') insert newNote;
            PageReference pr = ApexPages.currentPage();
            pr.setRedirect(true);
            return pr;
        } catch (Exception e) { QuoteUtilities.log(e); } return null;
    }
    
    public PageReference addComment() {
        try {
            if (task == null) { throw new CloudProjectPortalException('No task loaded'); return null;}
            newNote.parentId = task.id;
            newNote.body = 'Comment from '+getRelatedToName();
            newNote.isPrivate=false;
            if(newNote.title != null && newNote.title != '') insert newNote;
            PageReference pr = ApexPages.currentPage();
            pr.setRedirect(true);
            return pr;
        } catch (Exception e) { QuoteUtilities.log(e); } return null;
    }
    
    public Note getNewNote(){
    	return newNote;
    }
    
    public string getSubject() {
        if (task == null) {return '';}
        String subject = '';
        if (task.KD_Name__c != null && task.Project_Name__c != null) {
            subject += task.KD_Name__c + ' - ' + task.Project_Name__c;
        }
        String rt = getRelatedToName();
        if (rt != null && rt != '') {
            if (subject == '') {
                subject = 'Regarding ' + rt;
            }
        }
        return subject;
    }
    
    public string getRelatedTo() {
        if (task == null) {return '';}
        String subject = '';
        if (task.KD_Name__c != null && task.Project_Name__c != null) {
            subject += task.KD_Name__c + ' - ' + task.Project_Name__c;
        }
        return subject;
    }

    public string getRole() {
        if (task == null) {return '';}
        String subject = '';
        if (task.role__c != null) {
            subject += task.role__c;
        }
        return subject;
    }
    
    public string getRelatedToName() {
        if (task == null) {return '';}
        String subject = '';
        if (task.Asignee_Name__c != null) {
            subject = task.Asignee_Name__c;
        }
        return subject;
    }
    
    public string getRelatedToEmail() {
        if (task == null) {return '';}
        String subject = '';
        if (task.Assignee_Email__c != null) {
            subject = task.Assignee_Email__c;
        }
        return subject;
    }
    
    public string getStatus() {
        if (task == null) {return '';}
        return task.Status__c;
    }
    
    public string getCreatedBy() {
        if (task == null) { return ''; }
        return task.CreatedBy.Name;
    }
    
    private void clear() {
        isTask = null;
        task = null;
    }
    
    private void addETP(String e) {
        ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.Error, e));
    }
}