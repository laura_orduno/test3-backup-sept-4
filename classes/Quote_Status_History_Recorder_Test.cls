@isTest
private class Quote_Status_History_Recorder_Test {
	
    static testMethod void myUnitTest() {
        Opportunity o = new Opportunity(name = 'test', stagename = 'test', closedate = Date.today());
        insert o;
        
        SBQQ__Quote__c q = new SBQQ__Quote__c(SBQQ__Opportunity__c = o.id);
        insert q;
        system.assertEquals(0, [SELECT count() FROM Quote_Status_History__c]);
        
        update new SBQQ__Quote__c(id = q.id, SBQQ__Status__c = QuoteStatus.Signed_In_Person);
        system.assertEquals(1, [SELECT count() FROM Quote_Status_History__c]);
        
        update new SBQQ__Quote__c(id = q.id, SBQQ__Status__c = QuoteStatus.Provisioning_In_Progress);
        system.assertEquals(2, [SELECT count() FROM Quote_Status_History__c]);
        
        update new SBQQ__Quote__c(id = q.id, SBQQ__Status__c = QuoteStatus.Wireline_Order_Entry_Complete);
        system.assertEquals(3, [SELECT count() FROM Quote_Status_History__c]);
        
        update new SBQQ__Quote__c(id = q.id, SBQQ__Status__c = QuoteStatus.Wireless_Provisioning_Complete);
        system.assertEquals(4, [SELECT count() FROM Quote_Status_History__c]);
        
        update new SBQQ__Quote__c(id = q.id, SBQQ__Status__c = QuoteStatus.Provisioning_Complete);
        system.assertEquals(5, [SELECT count() FROM Quote_Status_History__c]);
    }
}