global class CreditAssessmentCallout {
    
    private static final Integer WS_TIMEOUT_MS = 120000;
    
    /*------------------------------------------------------------------------*/
    
    public static CreditAssessmentService.BusinessCreditAssessmentService_v1_0_SOAP getServicePort(){   
        CreditAssessmentService.BusinessCreditAssessmentService_v1_0_SOAP 
            servicePort = new CreditAssessmentService.BusinessCreditAssessmentService_v1_0_SOAP();   
        if ( !Test.isRunningTest() ){            
            SMBCare_WebServices__c wsUsername = SMBCare_WebServices__c.getValues('username');
            SMBCare_WebServices__c wsPassword = SMBCare_WebServices__c.getValues('password');
            System.debug('... dyy ... username = ' + wsUsername.value__c);
            System.debug('... dyy ... password = ' + wsPassword.value__c);
            String creds;
            if (String.isNotBlank(WSusername.Value__c)&& String.isNotBlank(wsPassword.Value__c))
                creds = WSusername.Value__c+':'+wsPassword.Value__c;
            String encodedusernameandpassword = EncodingUtil.base64Encode(Blob.valueOf(creds));
            servicePort.inputHttpHeaders_x = new Map<String, String>();
            servicePort.inputHttpHeaders_x.put('Authorization','Basic ' + encodedusernameandpassword );
            servicePort.timeout_x = WS_TIMEOUT_MS;
        } 
        return servicePort;   
    }
    
    /*------------------------------------------------------------------------*/
    
    /*------------- Ping starts ------------------------------------------------*/
    
    webservice static void ping(){    
        try {
            CreditAssessmentService.BusinessCreditAssessmentService_v1_0_SOAP 
                servicePort = getServicePort();
            servicePort.Ping();  
            
            if ( Test.isRunningTest() ) throw new CalloutException();
        } catch(CalloutException e){
            System.debug(' ... dyy ... e.getMessage = ' + e.getMessage()); 
            System.debug(' ... dyy ... e.getCause = ' + e.getCause()); 
            System.debug(' ... dyy ... e.getStackTraceString = ' + e.getStackTraceString()); 
        }           
    }    
    
    /*------------- Ping ends ------------------------------------------------*/
    

    
    public static Boolean isClickedTooManyTimesTooFast(Credit_Assessment__c car){
        Boolean isClickedTooManyTimes = false;
        if ( !Test.isRunningTest() )
            if(Datetime.now()<car.LastModifiedDate.addSeconds(3) &&!'Draft'.equalsIgnoreCase(car.CAR_Status__c) &&!'Ready to Amend'.equalsIgnoreCase(car.CAR_Status__c)) 
            isClickedTooManyTimes = true; 
        return isClickedTooManyTimes;
    }
    
    public static Boolean isCarValidToSubmit(
        Credit_Assessment__c car
        , CreditAssessmentBusinessTypes.BusinessProfile businessProfile
    ){
        Boolean isCarProfileExist = (car.Credit_Profile__r!=null)&&(car.Credit_Profile__r.id!=null); 
        Boolean isPartnerProfileExist = 
            (businessProfile!=null)
            &&(businessProfile.associationsProfile!=null)
            &&(businessProfile.associationsProfile.partnerBusinessProfileList!=null)
            &&(businessProfile.associationsProfile.partnerBusinessProfileList.size()>0)
            &&(businessProfile.associationsProfile.partnerBusinessProfileList[0]!=null)
            &&(businessProfile.associationsProfile.partnerBusinessProfileList[0].partnerBusinessCreditProfile!=null)
            &&(businessProfile.associationsProfile.partnerBusinessProfileList[0].partnerBusinessCreditProfile.validLegalNameOnFileInd!=null); 
        Boolean isCarValidToSubmit = isCarProfileExist||isPartnerProfileExist;
        System.debug(' ... dyy ... isCarProfileExist = ' + isCarProfileExist); 
        System.debug(' ... dyy ... isPartnerProfileExist = ' + isPartnerProfileExist); 
        System.debug(' ... dyy ... isCarValidToSubmit = ' + isCarValidToSubmit); 
        return isCarValidToSubmit;
    }
    
    webservice static String performBusinessCreditAssessment(ID CAR_ID){
        
        
        System.debug(' ... dyy ... performBusinessCreditAssessment for = ' + CAR_ID); 
        
        String carMessage;
        Credit_Assessment__c car = CreditAssessmentCalloutHelper.getCar(CAR_ID);         
        
        if (!Test.isRunningTest() && isClickedTooManyTimesTooFast(car)) 
            return null;
        
        CreditAssessmentCalloutHelper.setCarStatusBeforeCallWebservice(car);  
        String externalCARId = CreditAssessmentCalloutHelper.getExternalCARId(car);
        CreditAssessmentRequestResponse.BusinessCreditAssessmentResult response;   
        
        
        CreditAssessmentBusinessTypes.BusinessProfile 
            businessProfile = getBusinessProfile(car);
        
        /*------- dyy check for valid to submit starts ----------------------------*/
        if(!Test.isRunningTest() && !isCarValidToSubmit(car, businessProfile)){
            carMessage = 'Profile_Missing';            
            car.CAR_Code__c = 'System outage';
            update car;
            System.debug(' ... dyy ... carMessage = ' + carMessage); 
            return carMessage;
        }
        /*------- dyy check for valid to submit ends ----------------------------*/
        
        
        CreditAssessmentRequestResponse.performBusinessCreditAssessment_orderData_element 
            orderData = getOrderData(car);
        
        CreditAssessmentBusinessTypes.CreditAuditInfo 
            auditInfo = CreditWebServiceClientUtil.getAssessmentAuditInfo();        
        
        String creditID = externalCARId;
        String creditObject = 'Credit_Assessment__c';
        String operationOrMethod = 'CreditAssessmentCallout.performBusinessCreditAssessment';
        String payload = 'PAYLOAD: externalCARId = ' + externalCARId
            + ', ' + businessProfile
            + ', ' + orderData
            + ', ' + auditInfo
            ;  
        String result; 
        Integer retryCount;
        String retryStatus;
        String status;
        String createdById = auditInfo.userId;
        DateTime createdDate = Datetime.now();
        
        
        try {
            CreditAssessmentService.BusinessCreditAssessmentService_v1_0_SOAP 
                servicePort = getServicePort();  
            
            if ( !Test.isRunningTest() ) {
                response = servicePort.PerformBusinessCreditAssessment(externalCARId, businessProfile, orderData, auditInfo);                 
                CreditAssessmentCalloutHelper.savetoCreditCondition(response,car);  
                result= response.assessmentResult.code;                
            }
            
            car.OwnerId = CreditWebServiceClientUtil.getQueueId('Credit Assessment Team');
            
            status = 'Success';
            
            CreditWebServiceClientUtil.writeToCreditRequestHistory(
                creditID
                , creditObject
                , operationOrMethod
                , payload
                , result
                , retryCount
                , retryStatus
                , status
                , createdById
                , createdDate);
            
            if ( Test.isRunningTest() ) throw new CalloutException();
        }catch(CalloutException e){
            
            status = 'Error';
            result = e.getTypeName() 
                + e.getCause() 
                + e.getLineNumber() 
                + e.getMessage() 
                + e.getStackTraceString();
            
            CreditWebServiceClientUtil.writeToCreditRequestHistory(
                creditID
                , creditObject
                , operationOrMethod
                , payload
                , result
                , retryCount
                , retryStatus
                , status
                , createdById
                , createdDate);
            
            CreditWebServiceClientUtil.writeToWebserviceIntegrationErrorLogWithPayload(
                creditID
                , creditObject
                , operationOrMethod
                , 'BusinessCreditAssessmentService_v1_0_SOAP.PerformBusinessCreditAssessment'
                , payload
                , e);
            
            System.debug(' ... dyy ... e.getMessage = ' + e.getMessage()); 
            System.debug(' ... dyy ... e.getCause = ' + e.getCause()); 
            System.debug(' ... dyy ... e.getStackTraceString = ' + e.getStackTraceString()); 
        }  
        
        if(response!=null) {   
            if(Label.Assessment_Result_Approved==response.assessmentResult.code)
                performAutoUpdateBusinessCreditProfileProxy(CAR_ID);             
            
            carMessage = CreditAssessmentCalloutHelper.setCarStatusAfterCallWebserviceSuccessful(car,response);  
            if (car.Number_of_Times_Submitted__c==null) car.Number_of_Times_Submitted__c=1;
            else car.Number_of_Times_Submitted__c++;       
            
            car.Submitted_Date__c = DateTime.now();
            
        } else {
            CreditAssessmentCalloutHelper.setCarStatusAfterCallWebserviceFailed(car);
        }                
        
        update car;
        
        if(String.isBlank(carMessage)){
            carMessage='Error';
            if('Queued'.equalsIgnoreCase(car.CAR_Status__c)) carMessage='Queued'; 
            else if('Completed'.equalsIgnoreCase(car.CAR_Status__c)) carMessage='Completed'; 
        }
        
        System.debug('... dyy ... carMessage = ' + carMessage);
        
        return carMessage;
    }
    
    public static CreditAssessmentBusinessTypes.BusinessProfile getBusinessProfile(
        Credit_Assessment__c car){        
            CreditAssessmentBusinessTypes.BusinessProfile 
                businessProfile = new CreditAssessmentBusinessTypes.BusinessProfile();     
            CreditAssessmentBusinessTypes.BusinessCustomerProfile 
                businessCustomerProfile = new CreditAssessmentBusinessTypes.BusinessCustomerProfile();          
            CreditAssessmentBusinessTypes.BusinessCreditProfileBase 
                businessCreditProfile = new CreditAssessmentBusinessTypes.BusinessCreditProfileBase(); 
            CreditAssessmentBusinessTypes.BusinessProfile_associationsProfile_element 
                associationsProfile = new CreditAssessmentBusinessTypes.BusinessProfile_associationsProfile_element();        
            businessProfile.businessCustomerProfile = businessCustomerProfile;     
            businessProfile.businessCreditProfile = businessCreditProfile;     
            CreditAssessmentCalloutHelper.populateBusinessCustomerProfile(businessCustomerProfile, car);
            CreditAssessmentCalloutHelper.populateBusinessCreditProfileBase(businessCreditProfile, car); 
            
            businessProfile.associationsProfile = null;
            
            if('Sole Proprietor (PR)'.equalsignorecase(car.Legal_Entity_Type__c)) {                
                businessProfile.associationsProfile = associationsProfile;     
                CreditAssessmentCalloutHelper.populateAssociationsProfileForProprietor(associationsProfile, car);
            }
            
            if('General Partnership (PT)'.equalsignorecase(car.Legal_Entity_Type__c)
               ||'Limited Partnership (LP)'.equalsignorecase(car.Legal_Entity_Type__c)
               ||'Limited Liability Partnership (LLP)'.equalsignorecase(car.Legal_Entity_Type__c)
              ){              
                  businessProfile.associationsProfile = associationsProfile;     
                  CreditAssessmentCalloutHelper.populateAssociationsProfileForPartner(associationsProfile, car);  
              } 
            
            return businessProfile;
        }
    
    public static CreditAssessmentRequestResponse.performBusinessCreditAssessment_orderData_element getOrderData(
        Credit_Assessment__c car){           
            CreditAssessmentRequestResponse.performBusinessCreditAssessment_orderData_element 
                orderData = new CreditAssessmentRequestResponse.performBusinessCreditAssessment_orderData_element();
            CreditAssessmentCalloutHelper.populateOrderData_element(orderData, car);  
            return orderData;    
        }
    
    
    
    @future(callout=true)
    public static void performAutoUpdateBusinessCreditProfileProxy(ID CAR_ID){
        System.debug(' ... dyy ... performAutoUpdateBusinessCreditProfileProxy ... ');         
        System.debug('... dyy ... CAR_ID = ' + CAR_ID);        
        Credit_Assessment__c car = CreditAssessmentCalloutHelper.getCar(CAR_ID);   
        
        performAutoUpdateBusinessCreditProfile(
            car
            , getBusinessProfile(car)
            , CreditWebServiceClientUtil.getAssessmentAuditInfo());
    }
    
    public static Boolean performAutoUpdateBusinessCreditProfile(
        Credit_Assessment__c car
        ,CreditAssessmentBusinessTypes.BusinessProfile businessProfile
        ,CreditAssessmentBusinessTypes.CreditAuditInfo auditInfo){   
            
            System.debug(' ... dyy ... performAutoUpdateBusinessCreditProfile ... '); 
            
            Boolean callSuccessful = false;
            CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfile_carBusinessProfile_element 
                carBusinessProfile 
                = new CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfile_carBusinessProfile_element();  
            carBusinessProfile.businessCustomerProfile = businessProfile.businessCustomerProfile;
            carBusinessProfile.updatedBusinessCreditProfile = businessProfile.businessCreditProfile;
            
            carBusinessProfile.updatedIndvidualProprietorProfile=null;
            System.debug(' ... dyy ... car.Legal_Entity_Type__c = ' + car.Legal_Entity_Type__c); 
            if('Sole Proprietor (PR)'.equalsignorecase(car.Legal_Entity_Type__c))
                carBusinessProfile.updatedIndvidualProprietorProfile = getUpdatedIndvidualProprietorProfile(businessProfile);
            
            String externalCARId = CreditAssessmentCalloutHelper.getExternalCARId(car);
            
            String creditID = externalCARId;
            String creditObject = 'Credit_Assessment__c';
            String operationOrMethod = 'CreditAssessmentCallout.performAutoUpdateBusinessCreditProfile';
            String payload = 'PAYLOAD: externalCARId = ' + externalCARId
                + ', ' + carBusinessProfile
                + ', ' + auditInfo
                ;  
            String result; 
            Integer retryCount;
            String retryStatus;
            String status;
            String createdById = auditInfo.userId;
            DateTime createdDate = Datetime.now();
            
            
            
            try {
                CreditAssessmentService.BusinessCreditAssessmentService_v1_0_SOAP 
                    servicePort = getServicePort();
                
                
                CreditAssessmentRequestResponse.AutoUpdateBusinessCreditProfileResult  
                    response 
                    = servicePort.performAutoUpdateBusinessCreditProfile(
                        externalCARId
                        , carBusinessProfile
                        , auditInfo);  
                
                Boolean autosaveSuccessful = false;
                if(response!=null){                    
                    autosaveSuccessful = CreditAssessmentCalloutHelper.saveAutoUpdateBusinessCreditProfileResult(response, car);
                }
                    
                callSuccessful = true;
                
                status = 'Success';
                result = 'AutoUpdateBusinessCreditProfile Successful';
                
                if(!autosaveSuccessful) {
                    result += ', saveAutoUpdateBusinessCreditProfileResult operation failed';
                }
                
                CreditWebServiceClientUtil.writeToCreditRequestHistory(
                    creditID
                    , creditObject
                    , operationOrMethod
                    , payload
                    , result
                    , retryCount
                    , retryStatus
                    , status
                    , createdById
                    , createdDate);
                
                if ( Test.isRunningTest() ) throw new CalloutException();
            }catch(CalloutException e){
                
                CreditWebServiceClientUtil.logCalloutErrors(
                    creditID
                    , creditObject
                    , operationOrMethod
                    , payload
                    , result
                    , retryCount
                    , retryStatus
                    , status
                    , createdById
                    , createdDate
                    , e);
                
            }         
            return callSuccessful;
        } 
    
    
    
    public static CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfile_carBusinessProfile_updatedIndvidualProprietorProfile_element 
        getUpdatedIndvidualProprietorProfile(CreditAssessmentBusinessTypes.BusinessProfile businessProfile){
            
            CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfile_carBusinessProfile_updatedIndvidualProprietorProfile_element 
                updatedIndvidualProprietorProfile = new CreditAssessmentRequestResponse.performAutoUpdateBusinessCreditProfile_carBusinessProfile_updatedIndvidualProprietorProfile_element ();
            CreditAssessmentBusinessTypes.IndividualCustomerProfile 
                individualCustomer = new CreditAssessmentBusinessTypes.IndividualCustomerProfile();
            CreditAssessmentBusinessTypes.IndividualCreditProfileBase 
                individualCreditProfile = new CreditAssessmentBusinessTypes.IndividualCreditProfileBase();
            updatedIndvidualProprietorProfile.individualCustomer = individualCustomer;
            updatedIndvidualProprietorProfile.individualCreditProfile = individualCreditProfile;
            
            CreditAssessmentBusinessTypes.BusinessProfile_associationsProfile_proprietorProfile_element 
                proprietorProfile;
            if (businessProfile!=null &&businessProfile.associationsProfile!=null){
                proprietorProfile= businessProfile.associationsProfile.proprietorProfile;
            }
            
            if(proprietorProfile!=null){
                updatedIndvidualProprietorProfile.individualCustomer = proprietorProfile.proprietorCustomerProfile;
                updatedIndvidualProprietorProfile.individualCreditProfile = proprietorProfile.proprietorCreditProfile;
            }
            
            return updatedIndvidualProprietorProfile;
        }
    
    
    
    webservice static String attachCreditReportWithCarId(
        String externalCarId
        , Long businessCustomerId
        , Document document
        , String documentTypeCd
        , String documentSourceCd
    ){
        
        String creditReportId;
        
        CreditAssessmentBusinessTypes.BusinessCreditReportRequest businessCreditReportRequest
            = new CreditAssessmentBusinessTypes.businessCreditReportRequest(); 
        CreditAssessmentCalloutHelper.populateBusinessCreditReportRequest(
            businessCreditReportRequest
            , document
            , documentTypeCd
            , documentSourceCd);
        
        CreditAssessmentBusinessTypes.CreditAuditInfo auditInfo 
            = CreditWebServiceClientUtil.getAssessmentAuditInfo();
        
        String payload;
        
        try {
            CreditAssessmentService.BusinessCreditAssessmentService_v1_0_SOAP 
                servicePort = getServicePort();
            
            payload = 'PAYLOAD: externalCarId = ' + externalCarId
                + ', businessCustomerId = ' + businessCustomerId
                + ', ' + auditInfo
                ;
            
            creditReportId = servicePort.attachCreditReportWithCarId(externalCarId, businessCustomerId, businessCreditReportRequest, auditInfo);
            
            System.debug(' ... dyy ... creditReportId = ' + creditReportId);
            
            if ( Test.isRunningTest() ) throw new CalloutException();
        } catch(CalloutException e){
            CreditWebServiceClientUtil.writeToWebserviceIntegrationErrorLogWithPayload(
                creditReportId
                , 'Credit_Related_Info__c'
                , 'CreditAssessmentCallout.attachCreditReport'
                , 'BusinessCreditAssessmentService_v1_0_SOAP.attachCreditReport'
                , payload
                , e);
            
            System.debug(' ... dyy ... e.getMessage = ' + e.getMessage()); 
            System.debug(' ... dyy ... e.getCause = ' + e.getCause()); 
            System.debug(' ... dyy ... e.getStackTraceString = ' + e.getStackTraceString()); 
        } 
        return creditReportId;
    }
    
    webservice static String attachCreditReport(
        Long businessCustomerId
        , Document document
        , String documentTypeCd
        , String documentSourceCd
    ){
        
        String creditReportId;
        
        CreditAssessmentBusinessTypes.BusinessCreditReportRequest businessCreditReportRequest
            = new CreditAssessmentBusinessTypes.businessCreditReportRequest(); 
        CreditAssessmentCalloutHelper.populateBusinessCreditReportRequest(
            businessCreditReportRequest
            , document
            , documentTypeCd
            , documentSourceCd);
        
        CreditAssessmentBusinessTypes.CreditAuditInfo auditInfo 
            = CreditWebServiceClientUtil.getAssessmentAuditInfo();
        
        String payload;
        
        try {
            CreditAssessmentService.BusinessCreditAssessmentService_v1_0_SOAP 
                servicePort = getServicePort();
            
            payload = 'PAYLOAD: businessCustomerId = ' + businessCustomerId
                + ', ' + auditInfo
                ;
            
            creditReportId = servicePort.attachCreditReport(businessCustomerId, businessCreditReportRequest, auditInfo);
            
            System.debug(' ... dyy ... creditReportId = ' + creditReportId);
            
            if ( Test.isRunningTest() ) throw new CalloutException();
        } catch(CalloutException e){
            CreditWebServiceClientUtil.writeToWebserviceIntegrationErrorLogWithPayload(
                creditReportId
                , 'Credit_Related_Info__c'
                , 'CreditAssessmentCallout.attachCreditReport'
                , 'BusinessCreditAssessmentService_v1_0_SOAP.attachCreditReport'
                , payload
                , e);
            
            System.debug(' ... dyy ... e.getMessage = ' + e.getMessage()); 
            System.debug(' ... dyy ... e.getCause = ' + e.getCause()); 
            System.debug(' ... dyy ... e.getStackTraceString = ' + e.getStackTraceString()); 
        } 
        return creditReportId;
    }
    
    webservice static Document getCreditReport(Long creditReportId){
        
        Document doc = new Document();
        
        CreditAssessmentBusinessTypes.CreditAuditInfo 
            auditInfo = CreditWebServiceClientUtil.getAssessmentAuditInfo();
        
        String payload;              
        
        try {
            CreditAssessmentService.BusinessCreditAssessmentService_v1_0_SOAP 
                servicePort = getServicePort();
            
            payload = 'PAYLOAD: creditReportId = ' + creditReportId
                + ', ' + auditInfo
                ;
            
            CreditAssessmentBusinessTypes.BusinessCreditReportFullDetails response
                = servicePort.getCreditReport(
                    creditReportId
                    ,auditInfo);   
            
            String documentId = response.documentId;
            String documentName = response.documentName;
            String creditReportContent = response.creditReportContent;
            doc.Name = documentName;
            doc.Body = EncodingUtil.base64Decode(creditReportContent);   
            System.debug(' ... dyy ... documentId = ' + documentId);
            System.debug(' ... dyy ... documentName = ' + documentName);
            
            if ( Test.isRunningTest() ) throw new CalloutException();
        } catch(CalloutException e) {
            CreditWebServiceClientUtil.writeToWebserviceIntegrationErrorLogWithPayload(
                String.valueof(creditReportId)
                , 'Credit_Related_Info__c'
                , 'CreditAssessmentCallout.getCreditReport'
                , 'BusinessCreditAssessmentService_v1_0_SOAP.getCreditReport'
                , payload
                , e);            
            System.debug(' ... dyy ... e.getMessage = ' + e.getMessage()); 
            System.debug(' ... dyy ... e.getCause = ' + e.getCause()); 
            System.debug(' ... dyy ... e.getStackTraceString = ' + e.getStackTraceString()); 
        } 
        return doc;
    }
    
    
    webservice static void removeCreditReport(String creditReportId){
        
        CreditAssessmentBusinessTypes.CreditAuditInfo
            auditInfo = CreditWebServiceClientUtil.getAssessmentAuditInfo();
        
        String payload;              
        
        try {
            CreditAssessmentService.BusinessCreditAssessmentService_v1_0_SOAP 
                servicePort = getServicePort();
            
            servicePort.removeCreditReport(creditReportId,auditInfo);
            
            payload = 'PAYLOAD: creditReportId = ' + creditReportId
                + ', ' + auditInfo
                ;
            
            if ( Test.isRunningTest() ) throw new CalloutException();
        } catch(CalloutException e) {
            CreditWebServiceClientUtil.writeToWebserviceIntegrationErrorLogWithPayload(
                String.valueof(creditReportId)
                , 'Credit_Related_Info__c'
                , 'CreditAssessmentCallout.removeCreditReport'
                , 'BusinessCreditAssessmentService_v1_0_SOAP.getCreditReport'
                , payload
                , e);            
            System.debug(' ... dyy ... e.getMessage = ' + e.getMessage()); 
            System.debug(' ... dyy ... e.getCause = ' + e.getCause()); 
            System.debug(' ... dyy ... e.getStackTraceString = ' + e.getStackTraceString()); 
        } 
    }
     
    /*------- QC 52180, 52183 fix begins --------------------------*/    
    webservice static void refreshProducts(ID CAR_ID){
        system.debug('CreditAssessmentCallout::refreshProducts() start');
        if(CAR_ID!=null){
            Credit_Related_Product__c[] allCarProducts = CreditAssessmentCalloutHelper.getAllCarProducts(CAR_ID);
            CreditAssessmentCalloutHelper.refreshOpportunitProduct(CAR_ID,allCarProducts);
            CreditAssessmentCalloutHelper.refreshFOBOChecklistTotally(CAR_ID,allCarProducts);
            CreditAssessmentCalloutHelper.refreshOrderProduct(CAR_ID,allCarProducts);
        }
        system.debug('CreditAssessmentCallout::refreshProducts() end');
    }
    /*------- QC 52180, 52183 fix ends --------------------------*/
    
    /*------- CreditCancelCarProxy ------------------------------*/
    
    
    @invocablemethod
    global static void cancelCarFromOrderProxy(List<ID> orderIds){   
        Boolean isCancellingCar = false;
        ID theOrderId;
        List<Opportunity> orders;
        if(orderIds!=null&&orderIds.size()!=0){        
            for (ID orderId:orderIds){    
                System.debug('... dyy ... orderId = ' + orderId);
                orders = getAllRelatedOrdersToCancel(orderId);
                if(!isRelatedOrderStillActive(orders)) {
                    theOrderId = orderId;
                    isCancellingCar = true;
                    break;
                }
            }             
        } 
        if(isCancellingCar) cancelCar(theOrderId, orders);   
    }
    
    public static List<Opportunity> getAllRelatedOrdersToCancel(ID orderId){   
        try{            
            List<Opportunity> orders = [
                select id,name
                ,Related_Orders__c
                ,Credit_Assessment__c
                ,Credit_Assessment__r.id
                ,StageName
                from Opportunity
                where id = :orderId
                or Related_Orders__c = :orderId
            ];  
            
            if(orders!=null
               &&orders.size()>0
              ){
                  ID relatedOrderId;
                  for (Opportunity order:orders){
                      if(order.Related_Orders__c!=null){
                          relatedOrderId = order.Related_Orders__c;
                          break;
                      }                      
                  }
                  try{
                      List<Opportunity> relatedOrders = [
                          select id,name
                          ,Related_Orders__c
                          ,Credit_Assessment__c
                          ,Credit_Assessment__r.id
                          ,StageName
                          from Opportunity
                          where 
                          id = :relatedOrderId
                      ];                    
                      orders.addAll(relatedOrders);                
                  }catch(Exception e){                      
                  }
            }
            return orders;
            
        }catch(Exception e){
            return null;
        }
    }
    
    public static Boolean isRelatedOrderStillActive(List<Opportunity> orders){
        Boolean isRelatedOrderStillActive = false;
        for (Opportunity order:orders){
            if(String.isNotBlank(order.StageName)
               &&!order.StageName.containsIgnoreCase('Cancelled')
               &&!order.StageName.containsIgnoreCase('Completed')
               &&!order.StageName.containsIgnoreCase('Declined')
               &&!order.StageName.containsIgnoreCase('Expired')
               &&!order.StageName.containsIgnoreCase('Lost')
               &&!order.StageName.containsIgnoreCase('Terminated')
               &&!order.StageName.containsIgnoreCase('Won')
              ){
                  isRelatedOrderStillActive = true;
                  break;
              }
        }
        return isRelatedOrderStillActive;
    }
    
    public static void cancelCar(
        Id orderId
        ,List<Opportunity> orders)
    {
        Boolean isUpdatingCar = false;
        Credit_Assessment__c car = new Credit_Assessment__c();
        for(Opportunity order:orders){
            if(orderId==order.id &&order.Credit_Assessment__c!=null){
                   car.id = order.Credit_Assessment__c;
                   car.CAR_Status__c = 'Cancelled';
                   car.CAR_Code__c = 'Cancel order received: TELUS initiated';
                   isUpdatingCar = true;
                   break;
               }
        }
        if(isUpdatingCar) update car;
    }
    
}