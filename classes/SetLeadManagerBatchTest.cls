@isTest
private class SetLeadManagerBatchTest {
	static testmethod void testBatch(){
   		Set<Id> userIds = new Set<Id> {Userinfo.getUserId()};
   		Test.startTest();
   		SetLeadManagerBatch obj = new SetLeadManagerBatch(); 
		obj.query = 'select id, manager_1__c, manager_2__c, manager_3__c, manager_4__c, ownerid from Lead where isConverted = false and ownerid in '+ RoleHierarchy.buildInExpression(userIds);
        Database.executeBatch(obj);
        Test.stopTest();
   }
}