@isTest
private class CustomGetUpdateContractListImplTest {

     static testmethod void UpdateContractListImplTest1(){
         TestDataHelper.testContractCreationWithContractRequest();
         TestDataHelper.testContractLineItemListWithProductCreation();
         Test.startTest();
         Id Id1 = null;
         
         Map<String,Object> outMap=new Map<String,Object>();
         if(TestDataHelper.testContractRequest != null){
             Apexpages.currentPage().getparameters().put('id',TestDataHelper.testContractRequest.id);
             CustomGetUpdateContractListImpl GetUpdateContractListImplData = new CustomGetUpdateContractListImpl();
             boolean blnSuccess=GetUpdateContractListImplData.invokeMethod('getUpdateContractList',new Map<String,Object>{'contextObjId' => TestDataHelper.testContractRequest.id},outMap,new Map<String,Object>());
             system.assertEquals(outMap!=null && outMap.size() > 0, true);
             system.assertEquals(blnSuccess, true);
             //boolean blnSuccess1 =GetUpdateContractListImplData.invokeMethod('getUpdateContractList',new Map<String,Object>{'contextObjId' => Id1},outMap,new Map<String,Object>());
         }
         
         Test.stopTest();
     }
    static testmethod void UpdateContractListImplTest2(){
        TestDataHelper.testContractCreationwithDealSupport();
        TestDataHelper.testContractLineItemListWithProductCreation();
       
        Test.startTest();
        Id Id1 = null;
        
        Map<String,Object> outMap=new Map<String,Object>();
        if(TestDataHelper.testDealSupport != null){
            Apexpages.currentPage().getparameters().put('id',TestDataHelper.testDealSupport.id);
            CustomGetUpdateContractListImpl GetUpdateContractListImplData = new CustomGetUpdateContractListImpl();
            boolean blnSuccess=GetUpdateContractListImplData.invokeMethod('getUpdateContractList',new Map<String,Object>{'contextObjId' => TestDataHelper.testDealSupport.id},outMap,new Map<String,Object>());
            system.assertEquals(outMap!=null && outMap.size() > 0, true);
            system.assertEquals(blnSuccess, true);
            //boolean blnSuccess1 =GetUpdateContractListImplData.invokeMethod('getUpdateContractList',new Map<String,Object>{'contextObjId' => Id1},outMap,new Map<String,Object>());
        }
        
        Test.stopTest();
    }
    static testmethod void UpdateContractListImplTest3(){
        TestDataHelper.testContractCreationwithOrder();
        TestDataHelper.testContractLineItemListWithProductCreation();
       
        Test.startTest();
        Id Id1 = null;
        
        Map<String,Object> outMap=new Map<String,Object>();
        if(TestDataHelper.orderObject  != null){
            Apexpages.currentPage().getparameters().put('id',TestDataHelper.orderObject.id);
            CustomGetUpdateContractListImpl GetUpdateContractListImplData = new CustomGetUpdateContractListImpl();
            boolean blnSuccess=GetUpdateContractListImplData.invokeMethod('getUpdateContractList',new Map<String,Object>{'contextObjId' => TestDataHelper.orderObject.id},outMap,new Map<String,Object>());
            system.assertEquals(outMap!=null && outMap.size() > 0, true);
            system.assertEquals(blnSuccess, true);
            //boolean blnSuccess1 =GetUpdateContractListImplData.invokeMethod('getUpdateContractList',new Map<String,Object>{'contextObjId' => Id1},outMap,new Map<String,Object>());
        }
        
        Test.stopTest();
    }
    
    
}