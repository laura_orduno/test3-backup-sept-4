/*
###########################################################################
# File..................: smb_OpportunityProductHelper
# Version...............: 1
# Created by............: Pallavi Mittal
# Created Date..........: 24-oct-2013
# Last Modified by......: 
# Last Modified Date....: 19-Nov-2013
# Description...........: This Helper class contains the methods used to update the Opportunity Products 
# Change Log:               
############################################################################
*/

public class smb_OpportunityProductHelper {
    
     public static void createCase(list<OpportunityLineItem> listOppProductLines, Boolean isUpdate, map<Id,OpportunityLineItem> mapOldOL){
     
         System.debug('OPP PRODUCT TRIGGER FLAG->'+isUpdate+'<-');
         
        Map<String,OpportunityLineItem > mapOLI=new Map<String,OpportunityLineItem >();
        set<Id> setOppIds = new set<Id>();  
        list<Messaging.Singleemailmessage> theEmailMessages = new list<Messaging.Singleemailmessage>();     
        map<Id, Opportunity> mapOpportunity;
        map<Id, Contact> mapContact;
        String tempKey;
        Case objCase;
        list<OpportunityLineItem> listProcessOL = new list<OpportunityLineItem>();
        list<Case> listCases = new list<Case>();
        RecordType recType = [SELECT Id from Recordtype where sobjectType = 'Case' and developerName ='SMB_Care_SACG' and IsActive =true limit 1];
        for (OpportunityLineItem objOL:listOppProductLines){
           tempKey=objOL.Service_Street__c+objOL.Service_City__c+objOL.Service_State__c+objOL.Service_Postal_Code__c;
           tempKey=tempKey.replace('null','');
                    
           if(objOL.New_Service_Address_Flag__c  && tempKey.length() > 0 && (!isUpdate || !mapOldOL.get(objOL.Id).New_Service_Address_Flag__c ))
             {
                        tempKey = tempKey.toUpperCase();
                        System.debug('tempKey->'+tempKey);
                        if(mapOLI.containsKey(objOL.OpportunityId+'-'+tempKey)==false)
                        {
                            setOppIds.add(objOL.OpportunityId);                        
                            mapOLI.put(objOL.OpportunityId+'-'+tempKey,objOL);
                        }
            }
        }
        System.debug('===========setOppIds' + setOppIds);

        if(mapOLI.size()>0){
            list<Case> lstCases = [Select Id,Opportunity__c,SMB_Case_Service_Add_Key__c, Service_Location_Address__c from Case where Opportunity__c IN:setOppIds];
            
            if(lstCases.size()>0){
                for(Case c:lstCases){
                    mapOLI.remove(c.Opportunity__c+'-'+c.SMB_Case_Service_Add_Key__c.toUpperCase());
                }
            }
        }
        if(setOppIds.size()>0 && mapOLI.size()>0)
        {
            mapOpportunity= new map<Id,Opportunity>([Select Id, OwnerId,Primary_Order_Contact__c,Account.Segment__c,Account.Name from  Opportunity where Id In: setOppIds]);
            set<Id> theContactIds = new set<Id>();
           
            for(Opportunity optty : mapOpportunity.values())
            {
                theContactIds.add(optty.Primary_Order_Contact__c);
            }
            mapContact = new map<Id,Contact>([SELECT Id, FirstName, LastName From Contact WHERE Id in:theContactIds]);
        
            for(OpportunityLineItem oPL:mapOLI.values())
            {
                        objCase = new Case();
                        objCase.RecordTypeId = recType.Id;
                        objCase.OwnerId = mapOpportunity.get(oPL.OpportunityId).OwnerId;
                        objCase.Account_Name__c =  mapOpportunity.get(oPL.OpportunityId).Account.Name;
    
                        String tempServiceLocationAddress=oPL.Service_Street__c+oPL.Service_City__c+oPL.Service_State__c+oPL.Service_Postal_Code__c;
                        tempServiceLocationAddress=tempServiceLocationAddress.replace('null','');

                        // SWF 12-19-13 commented out to change to use new address field.
                        // tempServiceLocationAddress=tempServiceLocationAddress.replace('null','');
                        //objCase.Service_Location_Address__c = tempServiceLocationAddress;
                        objCase.Service_Location_Address__c = oPL.SACG_Address__c;
                        objCase.SMB_Case_Service_Add_Key__c=tempServiceLocationAddress;
                        // SWF 12-19-13 Added description being set to Note field.
                        objCase.Description = oPL.SACG_Note__c;
                        
                        objCase.Subject = 'SACG Request';
                        objCase.Priority = 'Medium';
                        objCase.Origin = 'SACG Request';
                        objCase.Status = 'Open';
                        ObjCase.Notes__c = mapOpportunity.get(oPL.OpportunityId).Account.Segment__c;
                       // objCase.Comments = oPL.Notes__c;
                        objCase.ContactId = mapOpportunity.get(oPL.OpportunityId).Primary_Order_Contact__c;
                        //  SWF 11-10-13 Added addtional fields ********
                        objCase.Opportunity__c = oPL.OpportunityId;
                        objCase.Type ='SACG';
                        Contact theContact = mapContact.get(objCase.ContactId);
                        if (theContact != null)
                        {
                            objCase.Contact_First_Name__c = theContact.FirstName;
                            objCase.Contact_Last_Name__c = theContact.LastName;
                        }
                        // *********************************************                                                                        
                        listCases.add(objCase);
               } 
               if(listCases.size()>0)
               {
                        insert listCases;
                        
                    map<String,SMB_SACGSettings__c> mapSACGSettings = SMB_SACGSettings__c.getAll();
                    String strUsers = mapSACGSettings.get('ContactIds').Data__c;
                    if(strUsers.trim().length()>0){
                    
                    string [] arrUsers = strUsers.split(';',0);
                      if(arrUsers.size()>0){
                        for (Case theCase: listCases)
                        {
                            for (String str:arrUsers)
                            {
                                system.debug('***** Creating Email: ' + str);
                                theEmailMessages.add(CreateEmail(str, theCase.Id,mapSACGSettings.get('OrgWideEmailAddressId').Data__c,mapSACGSettings.get('TemplateId').Data__c));
                            }
                        }
                        
                        if (theEmailMessages != null && theEmailMessages.size() > 0)
                        {
                            system.debug('***** Sending Email:' + theEmailMessages);
                            Messaging.sendEmail(theEmailMessages, false);                   
                        }
                    }
                }
                      
              }
           }
    
      
    
     }
     
     public static Messaging.Singleemailmessage CreateEmail  (string targetId, Id theCaseId, String orgwideId, string templateId)
     {
        Messaging.Singleemailmessage theEmailMessage = new Messaging.Singleemailmessage();
        theEmailMessage.setOrgWideEmailAddressId(orgwideId);
        theEmailMessage.setTemplateId(templateId);                                                
        theEmailMessage.setSaveAsActivity(true);                        
        theEmailMessage.setTargetObjectId(targetId);                      
        theEmailMessage.setWhatId(theCaseId);       
        return theEmailMessage;     
     }
   

    
    //PK: Added 03/03 to update Opportunity.Street__c with latest service address from OpportunityLineItem to Opportunity : might be removed
    public static void updateOpportunitySvcAddr(list<OpportunityLineItem> listOLI, map<id,OpportunityLineItem> mapOldOli)
    {
        set<id> setOpptIds = new set<id>();
        
        if(mapOldOli != null)
        {
            // Update Scenario
            for(OpportunityLineItem oli:listOLI)
            { 
                if(mapOldOli.containsKey(oli.Id))
                {
                    if(oli.Service_Address_Formula__c != mapOldOli.get(oli.Id).Service_Address_Formula__c)
                         setOpptIds.add(oli.OpportunityId);
                }
                else
                    setOpptIds.add(oli.OpportunityId);
            }   
        }
        else
        {
            // Insert Scenario
            for(OpportunityLineItem o:listOLI)
            { 
                setOpptIds.add(o.OpportunityId);
            }   
        }
        
        if(setOpptIds.size() > 0)
        {
            list<RecordType> smbCareRT = new list<RecordType>([SELECT id, name FROM RecordType WHERE name = 'SMB Care Opportunity' AND sObjectType = 'Opportunity' AND isActive = true]);
            map<string,string> opptRecTypes = new map<string,string>();
                        
            for(RecordType rt:smbCareRT){
                opptRecTypes.put(rt.name,rt.id);
            }
            
            list<Opportunity> lstOpportunity = [Select Id, Street__c, (select id, Service_Address_Formula__c 
                                    from OpportunityLineItems where Service_Address_Formula__c != null 
                                    order by LastModifiedDate desc limit 1) from Opportunity where Id IN:setOpptIds and Opportunity.RecordTypeId IN: opptRecTypes.values()];
            
    
            list<Opportunity> updateOppt = new list<Opportunity>();
            for(Opportunity oppt:lstOpportunity)
            {
                //select service address on OLI not blank and most recent date
                if(oppt.OpportunityLineItems != null && oppt.OpportunityLineItems.size()>0 )
                {
                    if(oppt.Street__c != oppt.OpportunityLineItems[0].Service_Address_Formula__c)
                    {
                        oppt.Street__c = oppt.OpportunityLineItems[0].Service_Address_Formula__c;
                        updateOppt.add(oppt);
                    }       
                }                                               
            }
            if(updateOppt.size()>0){                
                upsert updateOppt;  
            }   
        }
    }
   
  
    /*
    * Update All_necessary_Due_Dates_booked__c field and set it as True on Opportunity when the Internet and Voice Products (OLIs) are same as the Work Orders assoicated  
    * with those OLIs
    */
    public static void updateOpportunityForNecessaryDueDates(list<OpportunityLineItem> listOppProductLines,List<Opportunity> lstOpp)
    {
        Set<Id> setOppId=new Set<Id>();
        Set<Id> setOppId2=new Set<Id>();
        if(null!=listOppProductLines && listOppProductLines.size()>0)
        {
            for(OpportunityLineItem OLI:listOppProductLines)
            {
                setOppId.add(OLI.Opportunityid);
            }
        }
        
        if(null!=lstOpp && lstOpp.size()>0)
        {
            for(Opportunity opp:lstOpp)
            {
                setOppId2.add(opp.id);
            }
        }
        
        System.debug('setOppId->'+setOppId);
        List<Opportunitylineitem> lstVoiceInternet=new List<Opportunitylineitem>();
        if(null!=listOppProductLines && listOppProductLines.size()>0)
            lstVoiceInternet= [SELECT Opportunity.id,Product_Name__c,Work_Order__c,work_order__r.status__c,OLI_Key__c FROM Opportunitylineitem where Prod_Type__c in ('Voice','Internet','VOICE','INTERNET') AND Opportunity.id in:setOppId and Element_Type__c != 'Over line Product'  order by Opportunity.id, Work_Order__c desc ];
        else
            lstVoiceInternet= [SELECT Opportunity.id,Product_Name__c,Work_Order__c,work_order__r.status__c,OLI_Key__c FROM Opportunitylineitem where Prod_Type__c in ('Voice','Internet','VOICE','INTERNET') AND Opportunity.id in:setOppId2 and Element_Type__c != 'Over line Product'  order by Opportunity.id, Work_Order__c desc ];
        Map<String,Set<String>> mapCheckforWO=new Map<String,Set<String>>();
        for(Opportunitylineitem OLI:lstVoiceInternet)
        {
                        if(mapCheckforWO.containsKey(OLI.Opportunity.id)==false)
                        {
                            mapCheckforWO.put(OLI.Opportunity.id,new Set<String>());
                        }
                            
                        if(null==OLI.Work_Order__c || OLI.work_order__r.status__c=='Cancelled')
                        {
                            //mapCheckforWO.get(OLI.Opportunity.id).add(OLI.Product_Name__c);
                            mapCheckforWO.get(OLI.Opportunity.id).add(OLI.OLI_Key__c);
                        }
                        //else if(mapCheckforWO.get(OLI.Opportunity.id).contains(OLI.Product_Name__c))
                        else if(mapCheckforWO.get(OLI.Opportunity.id).contains(OLI.OLI_Key__c))
                        {
                            //mapCheckforWO.get(OLI.Opportunity.id).remove(OLI.Product_Name__c);
                        	mapCheckforWO.get(OLI.Opportunity.id).remove(OLI.OLI_Key__c);
                        }
                        
                        
        }
        List<Opportunity> lstOppToUpdate=new List<Opportunity>(); 
        for(String OppId:mapCheckforWO.keySet())
        {
        lstOppToUpdate.add(new Opportunity(Id=OppId, All_necessary_Due_Dates_booked__c=mapCheckforWO.get(OppId).size()==0));
        }
       update lstOppToUpdate;
        
    }
    
    public static List<OpportunityLineItem> restrictDeletionUpdation(List<OpportunityLineItem> lstOLI)
    {
        Id profileId=UserInfo.getProfileId();
        Map<Id,Profile> mapProfile=new Map<Id,Profile>([Select id from Profile where name like '%Care%']);
        
        if(mapProfile.containsKey(profileId))
        {
            for(OpportunityLineItem lineItem:lstOLI){
                if(string.isnotblank(lineItem.fobo_request_type__c)&&!lineItem.fobo_request_type__c.equalsignorecase(label.aoo_deletable)){
                    lineItem.addError('You do not have rights to Delete Opportunity Line Items');   
                }
            } 
        }
        return lstOLI;
    }
}