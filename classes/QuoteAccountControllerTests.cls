@isTest
private class QuoteAccountControllerTests {
    private static Web_Account__c account;
    private static Address__c location;
    private static SBQQ__Quote__c quote;
    
    private testMethod static void testInitWithNoAcct() {
        account = new Web_Account__c(Name='UnitTest');
        insert account;
        
        location = new Address__c(Web_Account__c=account.Id,Special_Location__c='Test');
        location.Rate_Band__c = 'F';
        location.State_Province__c = 'BC';
        insert location;
        
        Opportunity opp = new Opportunity(Name='Test',CloseDate=System.today(),StageName='Prospecting');
        insert opp;
        
        quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
        quote.Rate_Band__c = 'F';
        quote.Province__c = 'BC';
        insert quote;
        
        Product2 p = new Product2(Name='Test',IsActive=true);
        insert p;
        
        SBQQ__QuoteLine__c line = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,SBQQ__Product__c=p.Id,SBQQ__Bundle__c=true);
        insert line;
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteAccountController target = new QuoteAccountController();
        
        system.assertEquals(null,target.selectedAccount.id);
        
        // simulate search
        target.searchTerm = 'UnitTest';
        target.onSearch();
        system.assertEquals(1,target.accounts.size());
        
        //simulate selection of account and location
        target.selectedAccountId = target.accounts[0].id;
        target.onSelectAccount();
        
        target.selectedLocationId = location.id;
        
    } 
    
    testMethod static void testSaveAndContinue() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteAccountController target = new QuoteAccountController();
        
        system.assert(target.ownsThisAccount);
        
        target.selectedAccount.Name = 'Test';
        target.selectedAccount.BAN_Type__c = 'Sole Proprietorship';
        target.selectedAccount.PCI_Home_Address_Postal_Code__c = 'V4N 0Z5';
        target.selectedAccount.PCI_Home_Address_Province__c = 'BC';
        target.selectedAccount.PCI_Home_Address_Street_Name__c = 'Spenser Court';
        target.selectedAccount.PCI_Home_Address_Civic_Number__c = '15078';
        target.selectedAccount.PCI_First_Name__c = 'Alex';
        target.selectedAccount.PCI_Last_Name__c = 'Miller';
        
        PageReference pref = target.onSaveAndContinue();
        
        System.assert(pref == null, 'Errors:' + ApexPages.getMessages());
        System.assert(target.getLocationReturnURL() != null);
        System.assert(target.getPreviousPageURL() != null);
    }
    
    testMethod static void testSaveAndContinueTwo() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteAccountController target = new QuoteAccountController();
        target.selectedAccount.Name = 'Test';
        target.selectedAccount.BAN_Type__c = 'Sole Proprietorship';
        target.selectedAccount.PCI_Home_Address_Postal_Code__c = 'V4N 0Z5';
        target.selectedAccount.PCI_Home_Address_Province__c = 'BC';
        target.selectedAccount.PCI_Home_Address_Street_Name__c = 'Spenser Court';
        target.selectedAccount.PCI_Home_Address_Civic_Number__c = '15078';
        target.selectedAccount.PCI_First_Name__c = 'Alex';
        target.selectedAccount.PCI_Last_Name__c = 'Miller';
        target.selectedAccount.PCI_Social_Insurance_Number__c = '123 456 789';
        PageReference pref = target.onSaveAndContinue();
        System.assert(pref == null, 'Errors:' + ApexPages.getMessages());
        System.assert(target.getLocationReturnURL() != null);
        System.assert(target.getPreviousPageURL() != null);
    }
    
    testMethod static void testSaveAndContinueThree() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteAccountController target = new QuoteAccountController();
        target.selectedAccount.Name = 'Test';
        target.selectedAccount.BAN_Type__c = 'Sole Proprietorship';
        target.selectedAccount.PCI_Home_Address_Postal_Code__c = 'V4N 0Z5';
        target.selectedAccount.PCI_Home_Address_Province__c = 'BC';
        target.selectedAccount.PCI_Home_Address_Street_Name__c = 'Spenser Court';
        target.selectedAccount.PCI_Home_Address_Civic_Number__c = '15078';
        target.selectedAccount.PCI_First_Name__c = 'Alex';
        target.selectedAccount.PCI_Last_Name__c = 'Miller';
        target.selectedAccount.PCI_Social_Insurance_Number__c = null;
        target.selectedAccount.PCI_Drivers_License_Number__c = '123456';
        PageReference pref = target.onSaveAndContinue();
        System.assert(pref == null, 'Errors:' + ApexPages.getMessages());
        System.assert(target.getLocationReturnURL() != null);
        System.assert(target.getPreviousPageURL() != null);
    }
    testMethod static void testSaveAndContinueFour() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteAccountController target = new QuoteAccountController();
        target.selectedAccount.Name = 'Test';
        target.selectedAccount.BAN_Type__c = 'Sole Proprietorship';
        target.selectedAccount.PCI_Home_Address_Postal_Code__c = 'V4N 0Z5';
        target.selectedAccount.PCI_Home_Address_Province__c = 'BC';
        target.selectedAccount.PCI_Home_Address_Street_Name__c = 'Spenser Court';
        target.selectedAccount.PCI_Home_Address_Civic_Number__c = '15078';
        target.selectedAccount.PCI_First_Name__c = 'Alex';
        target.selectedAccount.PCI_Last_Name__c = 'Miller';
        target.selectedAccount.PCI_Drivers_License_Number__c = null;
        target.selectedAccount.PCI_Drivers_License_Expiry_Date__c = '11/11/2096';
        PageReference pref = target.onSaveAndContinue();
        System.assert(pref == null, 'Errors:' + ApexPages.getMessages());
        System.assert(target.getLocationReturnURL() != null);
        System.assert(target.getPreviousPageURL() != null);
    }
    testMethod static void testSaveAndContinueFive() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteAccountController target = new QuoteAccountController();
        target.selectedAccount.Name = 'Test';
        target.selectedAccount.BAN_Type__c = 'Sole Proprietorship';
        target.selectedAccount.PCI_Home_Address_Postal_Code__c = 'V4N 0Z5';
        target.selectedAccount.PCI_Home_Address_Province__c = 'BC';
        target.selectedAccount.PCI_Home_Address_Street_Name__c = 'Spenser Court';
        target.selectedAccount.PCI_Home_Address_Civic_Number__c = '15078';
        target.selectedAccount.PCI_First_Name__c = 'Alex';
        target.selectedAccount.PCI_Last_Name__c = 'Miller';
        target.selectedAccount.PCI_Drivers_License_Number__c = '123456';
        target.selectedAccount.PCI_Drivers_License_Expiry_Date__c = '11/11/2096';
        PageReference pref = target.onSaveAndContinue();
        
        System.assert(pref == null, 'Errors:' + ApexPages.getMessages());
        System.assert(target.getLocationReturnURL() != null);
        System.assert(target.getPreviousPageURL() != null);
    }
    
    
    testMethod static void testSaveAndContinueNoErrors() {
        setUp(); 
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteAccountController target = new QuoteAccountController();
        
        system.assert(target.ownsThisAccount);
        
        target.selectedAccount.Name = 'Test';
        target.selectedAccount.BAN_Type__c = 'Sole Proprietorship';
        target.selectedAccount.PCI_Home_Address_Postal_Code__c = 'V4N 0Z5';
        target.selectedAccount.PCI_Home_Address_Province__c = 'BC';
        target.selectedAccount.PCI_Home_Address_Street_Name__c = 'Spenser Court';
        target.selectedAccount.PCI_Home_Address_Civic_Number__c = '15078';
        target.selectedAccount.PCI_Home_Address_City__c = 'Langley';
        target.selectedAccount.PCI_First_Name__c = 'Alex';
        target.selectedAccount.PCI_Last_Name__c = 'Miller';
        target.selectedAccount.PCI_Drivers_License_Number__c = '1234325';
        target.selectedAccount.PCI_Drivers_License_Expiry_Date__c = '11/11/2096';
        target.selectedAccount.PCI_Drivers_License_Expiry_Date_UI__c = Date.newInstance(2096,11,11);
        
        target.newLocation.City__c = 'Vancouver';
                target.newLocation.Street__c = 'Broadway';
        target.newLocation.Postal_Code__c = 'V1V 1V1';
        target.newLocation.Street_Number__c = '1234';
        target.newLocation.Special_Location__c = 'test';
        
        PageReference pref = target.onSaveAndContinue();
        
        System.assert(pref != null, 'Errors:' + ApexPages.getMessages());
        System.assert(target.getLocationReturnURL() != null);
        System.assert(target.getPreviousPageURL() != null);
        
        target.onViewPersonalInformation();
        target.getShowViewPersonalInformationButton();
    }
    

    testMethod static void testCancel() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteAccountController target = new QuoteAccountController();
        target.selectedAccount.Name = 'Test';
        PageReference pref = target.onCancel();
        System.assert(pref != null, 'Errors:' + ApexPages.getMessages());
        System.assert(target.getLocationReturnURL() != null);
        System.assert(target.getPreviousPageURL() != null);
    }
    
    testMethod static void testSelectAndContinue() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteAccountController target = new QuoteAccountController();
        target.selectedAccountId = account.Id;
        target.selectedLocationId = location.Id;
        target.selectedAccount.Name = 'Test';
        PageReference pref = target.onSelectAndContinue();
        System.assert(pref != null, 'Errors:' + ApexPages.getMessages());
    }
    
    testMethod static void testSave() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('aid', account.Id);
        ApexPages.currentPage().getParameters().put('retURL','/testing');
        QuoteAccountController target = new QuoteAccountController();
        PageReference pref = target.onSave();
        System.assert(pref != null, 'Errors:' + ApexPages.getMessages());
        system.assert(pref.getUrl().contains('/testing'),'URL should contain /testing, contains: ' + pref.getUrl());
    }
    
    testMethod static void testSearch() {
        setUp();
        
        ApexPages.currentPage().getParameters().put('aid', account.Id);
        QuoteAccountController target = new QuoteAccountController();
        
        // valid search
        target.searchTerm = 'UnitTest';
        PageReference pref = target.onSearch();
        System.assertEquals(1, target.accounts.size());
        
        // invalid search
        target.searchTerm = '';
        target.onSearch();
        system.assertEquals('You must enter a search term',ApexPages.getMessages()[0].getDetail());
    }
    
    testMethod static void testUtilMethods() {
        setUp();
        ApexPages.currentPage().getParameters().put('retURL','/testing');
        ApexPages.currentPage().getParameters().put('qid', quote.Id);
        QuoteAccountController target = new QuoteAccountController();
        target.onInit();
        
        PageReference ref = target.onCancel();
        system.assert(ref.getUrl().contains('/testing'),'URL should contain /testing, contains: ' + ref.getUrl());
    }
    
    testMethod static void testExceptionHandling() {
        QuoteAccountController target = new QuoteAccountController();
        target.onInit();
        target.onViewPersonalInformation();
        target.onSearch();
        target.getPreviousPageURL();
        target.getLocationReturnURL();
        target.onSearch();
        target.onSave();
        target.onSaveAndContinue();
        target.onSelectAndContinue();
        target.getShowViewPersonalInformationButton();
        
    }
    
    @isTest
    private static void setUp() {
        Product2 p = new Product2(Name='Test',IsActive=true);
        insert p;
        
        insert new Quote_Tool_Encryption_Settings__c(
            Algorithm_Name__c = 'AES256',
            Private_Key__c = 'fRUmE9eDre6UteSeWRuvaphespa78swecruCHUTeQEs='
        );
        
        account = new Web_Account__c(Name='UnitTest');
        insert account;
        
        location = new Address__c(Web_Account__c=account.Id,Special_Location__c='Test');
        location.Rate_Band__c = 'F';
        location.State_Province__c = 'BC';
        insert location;
        
        Opportunity opp = new Opportunity(Name='Test',Web_Account__c=account.Id,CloseDate=System.today(),StageName='Prospecting');
        insert opp;
        
        quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
        quote.Rate_Band__c = 'F';
        quote.Province__c = 'BC';
        quote.Install_Location__c = location.id;
        insert quote;
        
        SBQQ__QuoteLine__c line = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,SBQQ__Product__c=p.Id,SBQQ__Bundle__c=true);
        insert line;
    }
}