public class OrdrConstants 
{    
    public static final String SDF_CERT_NAME =  'sdfcommon';
    public static final String CREDENTIAL_USERNAME =  'UserName';
    public static final String CREDENTIAL_PASSWORD = 'Password';
    public static final Integer WS_TIMEOUT_MS = 120000;
    public static final String AUTHORIZATION = 'Authorization';
    public static final String BASIC ='Basic ';
 
    public static final String SPECIFICATION_NAME = 'NetCracker'; 
    public static final String SPECIFICATION_CATEGORY = 'Customer Order';
    
    public static final String SPECIFICATION_TYPE_X = 'Fulfillment';
    public static final String SPECIFICATION_TYPE_PORTABILITY = 'Portability Check';
    public static final String SPECIFICATION_TYPE_AVAILABILITY = 'Availability and Feasibility';
    
    public static final String SPECIFICATION_CATEGORY_LOGICAL_RESOURCE = 'Logical Resource';
    public static final String SPECIFICATION_CATEGORY_PRODUCT_CONFIG = 'Product Inventory';
    public static final String SPECIFICATION_TYPE_PRODUCT_CONFIG = 'Inventory Lookup';
    public static final String USER_ID = 'userId';
    
    public static final String WIRELESS = 'Wireless';
    public static final String WIRELINE = 'Wireline';
    
    //BPM-TP CharacteristicValue/Name key defined in IA
    public static final String STATUS = 'status';   
    public static final String SALES_ORDER_KEY = 'salesOrderKey';
    
    public static final String CHARACTERISTIC_TYPE = 'type';
    public static final String CHARACTERISTIC_TYPE_VALUE = 'availability';
    public static final String DISTRIBUTION_CHANNEL_KEY = 'distributionChannelKey';
    public static final String CUSTOMER_CATEGORY_KEY = 'customerCategoryKey';
    public static final String ID = 'Id';
    public static final String COID = 'coid';
    public static final String FMS_ID = 'fmsId';
    public static final String LOCATION_ID = 'locationId';
    public static final String LOCATION_KEY = 'locationKey';
    public static final String IS_ILEC = 'isIlecAddress';
    public static final String ADDR_PROVINCE = 'province';
    public static final String ADDR_PROVINCE_CODE = 'provinceCode';
    public static final String ADDR_MUNICIPALITY = 'municipalityName';
    public static final String ADDR_POSTAL_CODE = 'postalZipCode';
    public static final String ADDR_STREET_DIRECTION = 'streetDirectionCode';
    public static final String ADDR_STREET_TYPE = 'streetTypeCode';
    public static final String ADDR_STREET_NAME = 'streetName';
    public static final String ADDR_STREET_NUMBER = 'streetNumber';
    public static final String ADDR_UNIT_NUMBER = 'unitNumber';
    public static final String ADDR_QUARTER = 'quarter';
    public static final String ADDR_SECTION = 'section';
    public static final String ADDR_TOWNSHIP = 'township';
    public static final String ADDR_RANGE = 'range';
    public static final String ADDR_MERIDIAN = 'meridian';
    public static final String ADDR_LOT = 'lot';
    public static final String ADDR_BLOCK = 'block';
    public static final String ADDR_PLAN = 'plan';
    // BP1-336 added rateband and forborne
    public static final String ADDR_RATE_BAND = 'rateband';
    public static final String ADDR_FORBORNE = 'forborne';
    public static final String ADDR_ISFIFA = 'isFIFA';
    public static final String ADDR_ISDROPREADY = 'isDropReady';
    
    public static final String ADDRESS_TYPE = 'addressType';
    public static final String ADDRESS_TYPE_STANDARD = 'Standard';
    public static final String ADDRESS_TYPE_LOT_BLOCK_PLAN = 'LotBlockPlan';
    public static final String ADDRESS_TYPE_LEGAL_LAND_DESC = 'LegalLandDescription';
    public static final String ADDRESS_TYPE_TOWN_RANGE_ROAD_HWY = 'TownshipRangeRoadHigway';

    public static final String CUSTOMER_ID = 'customerId';
    public static final String CUSTOMER_PROFILE_ID = 'customerProfileId';
    public static final String PRODUCT_INSTANCE_ID = 'businessProductInstanceId';
    public static final String INSTANCE_TYPE = 'Business Product Instance';

    //CGTA
    public static final String TN = 'phoneNumber';
    public static final String PRODUCT_TYPE = 'productType';
    public static final String BRAND = 'Brand';
    public static final String REQUEST_DATE = 'requestDate';
    
    public static final String EXTERNAL_SYSTEM_NAME_NC = 'NC';    
    public static final String OBJECT_NAME = 'Order';
    
    //for WFM  
    public static final String  WFM_CLASSIFICATION_CODE = 'ORDER';
    //its value is defined in custom settings. by default, it's 6161.
    public static final String SYSTEM_SOURCE_CODE_KEY = 'SFDC_SystemId';
    
    //for endpoint value starting with xmlgwy. OrdrProductEligibilityWsCallout and OrdrSubmitCustomerOrderWsCallout should use this constant as well
     public static final String ENDPOINT_XML_GATEWAY_SYMBOL = 'https://xmlgwy';
    
    public static final String NEW_ORDER_ACTION_ADD='Add';
    public static final String ACTION_ADD='Add';
    public static final String ACTION_MODIFY='Modify';
    public static final String ACTION_DELETE='Delete';
    public static final String ACTION_NONE='-';
    //Arpit : 10-March-2017 : Disconnect ACD Status 
    public static final String ACTION_DISCONNECT='Disconnect';
    public static final String AMEND_STATUS_ADD='Add';
    public static final String AMEND_STATUS_DELETE='Deleted';
    public static final String AMEND_STATUS_CHANGE='Change';
    
    public static final String MACD_STATUS_NEW='New';
    public static final String MACD_STATUS_DELETED='Deleted';
    public static final String MACD_STATUS_CHANGED='Changed';
    public static final String MACD_STATUS_ACTIVE='Active';
    public static final String MACD_STATUS_NONE='None';
     //Arpit : 10-March-2017 : Disconnect ACD Status 
    public static final String MACD_STATUS_DISCONNECT='Disconnected';
   
    public static final String PROMOTION='Promotion';
    public static final String TELEPHONE_NUMBER='Telephone Number';
    public static final String TOLLFREE_TELEPHONE_NUMBER='Toll-Free Number';
    public static final String OI_COMPLETE_SUBSTATUS='completed';

    //public static final String NC_ORDR_INITIATING_STATUS='Initiating';
    public static final String NC_ORDR_ENTERING_STATUS='Entering';
    public static final String NC_ORDR_SUBMITTING_STATUS='Submitting';
    public static final String NC_ORDR_PROCESSING_STATUS='Processing';
    public static final String NC_ORDR_PROCESSED_STATUS='Processed';
    public static final String NC_ORDR_ERROR_STATUS='Error';
    public static final String NC_ORDR_PONR_STATUS='PONR is passed';
  
    //public static final String ORDR_INITIATING_STATUS='Initiating';
    public static final String ORDR_ENTERING_STATUS='Entering';
    public static final String ORDR_SUBMITTING_STATUS='Submitting';
    public static final String ORDR_SUBMITTED_STATUS='Submitted';
    public static final String ORDR_INPROGRESS_STATUS='In Progress';
    public static final String ORDR_COMPLETED_STATUS='Completed';
    public static final String ORDR_ERROR_STATUS='Error';
    public static final String ORDR_PENDING='Pending'; // Added as per BMPF - 20
    public static final String ORDR_NOT_SUBMITTED='Not Submitted'; // Added as per BMPF - 20
    
    public static final String ORDR_PONR_STATUS='PONR is passed';
    public static final String ORDR_PROCESSED_STATUS='Processed';
    public static final String ORDR_SUSPENDED_STATUS='Suspended';
    public static final String ORDR_SUPERSEDED_STATUS='Superseded';
    
    public static final String ORDR_CANCELLED_STATUS='Cancelled';
    public static final String ORDR_CANCEL_TIMEOUT_ERROR='ordCancelTimeOutError';

    public static final String OI_PROCESSING_STATUS='Processing';
    public static final String OI_COMPLETED_STATUS='Completed';
    public static final String OI_SUSPENDED_STATUS='Suspended';
    public static final String OI_SUPERSEDED_STATUS='Superseded';
    public static final String OI_CANCELLED_STATUS='Cancelled';
    public static final String OI_PROCESSED_STATUS='Processed';
    public static final String OI_ENTERING_STATUS='Entering';
	public static final String OI_PONR_STATUS='PONR is passed'; // Defect# 10493 - Added this line
    public static final String OI_COMPLETED_SUBSTATUS='Order Entry is Complete';	
    public static final String OI_NONDOABLE_SUBSTATUS='Non-Doable';

    public static final String INTERNET_PRODUCT_TYPE_NAME='TELUS Internet Product';
    public static final String WIRELESS_PRODUCT_TYPE_NAME='TELUS Wireless Product';
    public static final Set<String> errorLineItemSubStatusSet=new Set<String>();
    public static final Set<String> warnLineItemSubStatusSet=new Set<String>();
    public static final Set<String> ignoreCharNameSetForBeforeAndAfterView=new Set<String>();
    static {
         errorLineItemSubStatusSet.add('Re-Issue Agreement');
        errorLineItemSubStatusSet.add('Non-Doable: Re-issue Agreement');
        errorLineItemSubStatusSet.add('Not Qualified');
        errorLineItemSubStatusSet.add('Non-Doable: Port Not found');
        errorLineItemSubStatusSet.add('Port Not found');        
        warnLineItemSubStatusSet.add('Hold: Customer Requested');
        warnLineItemSubStatusSet.add('Hold: Credit Approval');
        warnLineItemSubStatusSet.add('Hold: Back Office');
        warnLineItemSubStatusSet.add('Missing: Order Details');
        warnLineItemSubStatusSet.add('Missing: Product Error');
       // warnLineItemSubStatusSet.add('Follow-up: Back Office');
       // warnLineItemSubStatusSet.add('Follow-up: Customer Required');
       // warnLineItemSubStatusSet.add('Follow-up: Porting Approval');
        
        ignoreCharNameSetForBeforeAndAfterView.add('Additional Access Information for Technician');
        ignoreCharNameSetForBeforeAndAfterView.add('Booking ID');
        ignoreCharNameSetForBeforeAndAfterView.add('City of use');
        ignoreCharNameSetForBeforeAndAfterView.add('Contact Name');
        ignoreCharNameSetForBeforeAndAfterView.add('Contact Telephone Number');
        ignoreCharNameSetForBeforeAndAfterView.add('Estimated Work Time');
        ignoreCharNameSetForBeforeAndAfterView.add('Line Item External ID');
        ignoreCharNameSetForBeforeAndAfterView.add('Planned End Date/Time');
        ignoreCharNameSetForBeforeAndAfterView.add('Planned Start Date/Time');
        ignoreCharNameSetForBeforeAndAfterView.add('Province of use');
        ignoreCharNameSetForBeforeAndAfterView.add('Recommended Install Type (RIT)');
        ignoreCharNameSetForBeforeAndAfterView.add('Requested Due Date');
        ignoreCharNameSetForBeforeAndAfterView.add('Shipping Address Characteristic');
        ignoreCharNameSetForBeforeAndAfterView.add('Shipping Address');
        ignoreCharNameSetForBeforeAndAfterView.add('installRemarks');
        ignoreCharNameSetForBeforeAndAfterView.add('onsiteContact');
        ignoreCharNameSetForBeforeAndAfterView.add('onsiteContactInfo');
        ignoreCharNameSetForBeforeAndAfterView.add('onsiteEmail');
        ignoreCharNameSetForBeforeAndAfterView.add('onsitePhone');
        
    }
    
    public static final String ORDR_SUB_SPECIFICATION_NAME='Name';
    public static final String ORDR_SUB_SPECIFICATION_TYPE='Type';
    public static final String ORDR_SUB_SPECIFICATION_CATEGORY='Category';
    public static final String ORDR_SUB_REQUEST_TYPE_KEY='requestTypeKey';
    public static final String ORDR_SUB_REQUESTED_COMPLETION_DATE='requestedCompletionDate';
    public static final String ORDR_SUB_CSR_ID='csrId';
    public static final String ORDR_SUB_CSR_NAME='csrName';
    public static final String ORDR_SUB_CSR_EMAIL='csrEmail';
    public static final String ORDR_SUB_RUSH_FLAG='rushFlag';
    public static final String ORDR_SUB_SUMMARY='summary';
    public static final String ORDR_SUB_EXTERNAL_ID='externalId';
    public static final String ORDR_SUB_CAR_REFERENCE_NUMBER='carReferenceNumber';
    public static final String ORDR_SUB_RCID='rcid';
    public static final String ORDR_SUB_CAR_URL='carUrl';
    public static final String ORDR_SUB_EXTERNAL_ID_URL='externalIdUrl';
    public static final String ORDR_SUB_QUOTE_ID='quoteId';
    public static final String ORDR_SUB_QUOTE_URL='quoteUrl';
    public static final String ORDR_SUB_CREDIT_REQUEST_STATUS='creditRequestStatus';
    public static final String ORDR_SUB_ORDER_REMARKS_FOR_ROSTERER='orderRemarksForRosterer';
    public static final String ORDR_SUB_SALES_AGENT_KEY='salesAgentKey';
    public static final String ORDR_SUB_INTERACTION_ROLE='interactionRole';
    public static final String ORDR_SUB_SALES_AGENT_TYPE_KEY='salesAgentTypeKey';
    public static final String ORDR_SUB_SALES_AGENT_TEAM='salesAgentTeam';
    public static final String ORDR_SUB_SALES_AGENT_NAME='salesAgentName';
    public static final String ORDR_SUB_SALES_AGENT_ID='salesAgentId';
    public static final String ORDR_SUB_SALES_AGENT_EMAIL='salesAgentEmail';
    public static final String ORDR_SUB_SALES_AGENT_CPMS_ID='salesAgentCpmsId';
    public static final String ORDR_SUB_SALES_AGENT_DEALER_CODE='salesAgentDealerCode';
    public static final String ORDR_SUB_ACTION='action';
    public static final String ORDR_SUB_OFFERING_KEY='offeringKey';
    public static final String ORDR_SUB_APID='apId';
    public static final String ORDR_SUB_CROSS_REFERENCE='crossReference';
    public static final String ORDR_SUB_LOCATION_KEY='locationKey';
    public static final String ORDR_SUB_CUSTOMER_ACCOUNT_KEY='customerAccountKey';
    public static final String ORDR_SUB_BILLING_ACCOUNT_KEY='billingAccountKey';
    public static final String ORDR_SUB_PROMOTION_INSTANCE_KEY='promotionInstanceKey';
    public static final String ORDR_SUB_PROMOTION_KEY='promotionKey';
    public static final String ORDR_SUB_VALUE='value';
    public static final String ORDR_SUB_START_DATE='startDate';
    public static final String ORDR_SUB_CUSTOMER_ACCOUNT_TYPE_KEY='customerAccountTypeKey';
    public static final String ORDR_SUB_DISTRIBUTION_CHANNEL='distributionChannel';
    public static final String ORDR_SUB_PRIMARY_CONTACT='primaryContact';
    public static final String ORDR_SUB_LEGAL_ADDRESS='legalAddress';
    public static final String ORDR_SUB_LEGAL_NAME='legalName';
    public static final String ORDR_SUB_OPERATING_NAME='operatingName';
    public static final String ORDR_SUB_DEFAULT_BILLING_ACCOUNT='defaultBillingAccount';
    public static final String ORDR_SUB_ORGANIZATION_TYPE='organizationType';
    public static final String ORDR_SUB_REGISTRATION_NUMBER='registrationNumber';
    public static final String ORDR_SUB_REGISTRATION_DATE='registrationDate';
    public static final String ORDR_SUB_JURISDICTION='jurisdiction';
    public static final String ORDR_SUB_LOCATION_TYPE_KEY='locationTypeKey';
    public static final String ORDR_SUB_PROVINCE_CODE='provinceCode';
    public static final String ORDR_SUB_MUNICIPALITY_NAME='municipalityName';
    public static final String ORDR_SUB_POSTAL_CODE='postalCode';
    public static final String ORDR_SUB_STREET_DIRECTION_CODE='streetDirectionCode';
    public static final String ORDR_SUB_STREET_TYPE_CODE='streetTypeCode';
    public static final String ORDR_SUB_STREET_NAME='streetName';
    public static final String ORDR_SUB_STREET_NUMBER='streetNumber';
    public static final String ORDR_SUB_UNIT_NUMBER='unitNumber';
    public static final String ORDR_SUB_QUARTER='quarter';
    public static final String ORDR_SUB_SECTION='section';
    public static final String ORDR_SUB_TOWNSHIP='township';
    public static final String ORDR_SUB_RANGE='range';
    public static final String ORDR_SUB_MERIDIAN='meridian';
    public static final String ORDR_SUB_LOT='lot';
    public static final String ORDR_SUB_BLOCK='block';
    public static final String ORDR_SUB_PLAN='plan';
    public static final String ORDR_SUB_FMS_ID='fmsId';
    public static final String ORDR_SUB_LOCATION_ID='locationId';
    public static final String ORDR_SUB_CUSTOMER_CONTACT_KEY='customerContactKey';
    public static final String ORDR_SUB_CONTACT_TYPE_KEY='contactTypeKey';
    public static final String ORDR_SUB_CONTACT_KEY='contactKey';
    public static final String ORDR_SUB_FIRST_NAME='firstName';
    public static final String ORDR_SUB_MIDDLE_NAME='middleName';
    public static final String ORDR_SUB_LAST_NAME='lastName';
    public static final String ORDR_SUB_TITLE='title';
    public static final String ORDR_SUB_JOB_TITLE='jobTitle';
    public static final String ORDR_SUB_PRIMARY_PHONE='primaryPhone';
    public static final String ORDR_SUB_FAX='fax';
    public static final String ORDR_SUB_EMAIL='email';
    public static final String ORDR_SUB_BILLING_ACCOUNT_TYPE_KEY='billingAccountTypeKey';
    public static final String ORDR_SUB_BILLING_ACCOUNT_NUMBER='billingAccountNumber';
    public static final String ORDR_SUB_RENDERED_ADDRESS='renderedAddress';
    public static final String ORDR_SUB_SALES_ORDER_KEY='salesOrderKey';
    public static final String ORDR_SUB_IS_ILEC_ADDRESS='isIlecAddress';
    public static final String ORDR_SUB_DROPFACILITYCODE ='dropFacilityCode';
    public static final String ORDR_SUB_DROPTYPECODE ='dropTypeCode';
    public static final String ORDR_SUB_DROPLENGTH='dropLength';
    public static final String ORDR_SUB_DROPEXCEPTIONCODE='dropExceptionCode';
    public static final String ORDR_SUB_CURRENTTRANSPORTTYPECODE='currentTransportTypeCode';
    public static final String ORDR_SUB_FUTURETRANSPORTTYPECODE='futureTransportTypeCode';
    public static final String ORDR_SUB_FUTUREPLANTREADYDATE='futurePlantReadyDate';
    public static final String ORDR_SUB_FUTURETRANSPORTREMARKTYPECODE='futureTransportRemarkTypeCode';
	public static final String ORDR_SUB_BILLING_SYSTEM_ID='billingSystemID'; //Defect#10112 Modified
    public static final String ORDR_SUB_DEDICATED_CUSTOMER_CONTACT='customerOrderContact';
    public static final String ORDR_SUB_CUSTOMER_SOLUTION_KEY='customerSolutionKey';    
    public static final String ORDR_SUB_CUSTOMER_SOLUTION_TYPE_KEY='customerSolutionTypeKey';
    public static final String ORDR_SUB_CUSTOMER_SOLUTION_TYPE='CustomerSolutionType';
    public static final String ORDR_SUB_CUSTOMER_SOLUTION_INTERACTION_VAL='CustomerSolution';
    public static final String ORDR_SUB_CUSTOMER_SOLUTION_SPEC_TYPE='CustomerSolution';
    //Defect#10451 Begin
    public static final String ORDR_SUB_AGREEMENT_NAME='Agreement Name';
    public static final String ORDR_SUB_AGREEMENT_TYPE='Agreement Type';
    public static final String ORDR_SUB_AGREEMENT_URL='Agreement URL';
    public static final String ORDR_SUB_DISTRIBUTION_CHANNEL_VAL='Business CSR';
    public static final String ORDR_SUB_SALESAGENT_OUTLETID='salesAgentOutletId';
    //Defect#10451 End
    public static final String NO_SPARE_TN='NoSpareTN';
    public static final String NO_SPARE_TN_STATUS='NoSpareTN';
    
    public static final String NC_EXTERNAL_ID='Line Item External ID';
    public static final String SWITCH_CODE='001';
    public static final String ORDRTN_TELEPHONE_NUMBER='Telephone Number';
    public static final String ORDRTN_LOGICAL_RESOURCE='Logical Resource';
    public static final String ORDRTN_WIRELINE='Wireline';
    public static final String ORDRTN_ACTION='action';
    public static final String ORDRTN_UNRESERVE='unreserve';
    public static final String ORDRTN_RELEASE='release';
    public static final String ORDRTN_USERID='userId'; 
    public static final String ORDRTN_SFDC='SFDCTN';
    //Start : WFM Constants
    public static final String WFM_INSTALL_REQUEST = 'I';
    public static final String WFM_DISCONNECT_REQUEST = 'D';
    public static final String WFM_BONDED_COPPER_TECHNOLOGYCODE ='COPPER_BONDED';
    public static final String WFM_OLI_PROVISIONING_STATUS_DELETED='DELETED';
    //End : WFM Constants
    public static final String SDF_SUPPORT_MSG='This exception type has been found to originate from SDF.  Contact the SDF support prime to investigate.';
    public static final String SDF_INTERNAL_SERVER_ERR='Internal Server Error (from client)';
    //Start : Service Charges Offers 
    public static final Set<String> serviceChargesOffersSet = new Set<String>();
    static {
         serviceChargesOffersSet.add('Field Connection Charge');
         serviceChargesOffersSet.add('9145067738513804148'); //NC Id for Field Connection Charge
         serviceChargesOffersSet.add('Office Connection Charge');
         serviceChargesOffersSet.add('9145068321313804466'); //NC Id for Field Connection Charge
         serviceChargesOffersSet.add('Waive field connection charges');
         serviceChargesOffersSet.add('9145358943913721953'); //NC Id for Waive field connection charges
         serviceChargesOffersSet.add('Waive data processing charges');
         serviceChargesOffersSet.add('9145359110013721989'); //NC Id for Waive installation charges
         serviceChargesOffersSet.add('Waive installation charges');
         serviceChargesOffersSet.add('9144057201313865268'); //NC Id for Waive installation charges
         serviceChargesOffersSet.add('Waive office connection charges');
         serviceChargesOffersSet.add('9145359110013721971'); //NC Id for Waive office connection charges
         serviceChargesOffersSet.add('Data Processing Charge');
         serviceChargesOffersSet.add('9145068565413804585'); //NC Id for Data Processing Charge
         serviceChargesOffersSet.add('HSIA Premise Visit'); 
         serviceChargesOffersSet.add('9143951521813684126'); //NC Id for HSIA Premise Visit
         serviceChargesOffersSet.add('9141399100613369027'); //NC Id for HSIA Premise Visit
         serviceChargesOffersSet.add('TV Premise Visit');
         serviceChargesOffersSet.add('9146509148313153436'); //NC Id for TV Premise Visit
         serviceChargesOffersSet.add('Voice Premise Visit');
         serviceChargesOffersSet.add('9146110563413797508'); //NC Id for Voice Premise Visit
         serviceChargesOffersSet.add('Premise Visit');
         serviceChargesOffersSet.add('9145604819113128388'); //NC Id for Premise Visit
         serviceChargesOffersSet.add('Wi-Fi Premise Visit');
         serviceChargesOffersSet.add('9145874267913217154'); //NC Id for Wi-Fi Premise Visit
         serviceChargesOffersSet.add('Call Guardian Service Charge');
         serviceChargesOffersSet.add('9145333170613653265'); //NC Id for Call Guardian Service Charge
    }
    //End   : Service Charges Offers 
    
    //Start : includeCharaceristicInReviewOrder 
     public static final Set<String> includeCharacteristicInHierarachySet = new set<String>{'Telephone Number','Telephone Number Setup'};  
     public static final String CHAR_DISPLAY_ADDRESS = 'Display Address'; 
     public static final String CHAR_NO_CHANGES = 'No Changes'; 
     public static final String CHAR_NON_POST = 'Non-Post'; 
     public static final String CHAR_NON_PUB = 'Non-Pub'; 
     public static final String CHAR_LISTING_OPTION = 'Listing Option'; 
    //End : includeCharaceristicInReviewOrder  
    
    //Start : Wireless Devices 
    public static final Set<String> wirelessOffersSet = new Set<String>();
    static {
        wirelessOffersSet.add('Wireless Device Product Component');
        wirelessOffersSet.add('9145419241113909328');
    }
    //End : Wireless Devices
    public static final String RELATIONSHIP_TYPE_PROMOTION ='Promotion';
   
    //Start : Non-Doable Task  
    public static final String NONDOABLE_TASK_SUBJECT = 'OCOM Order Request Non-Doable';
    public static final String NONDOABLE_TASK_STATUS = 'Not Started';
    public static final String NONDOABLE_TASK_PRIORITY = 'High';
    public static final String NONDOABLE_TASK_CURRENCY = 'CAD';
    //End : Non-Doable Task 
   
    public static final String INTEGRATION_GROUP_NAME='OCOM_Integration_Users';
    
    //Start : Shippable Offers Characteristics
    public static final String CHAR_SUBSCRIBER_FIRST_NAME = 'Subscriber First Name';
    public static final String CHAR_SUBSCRIBER_LAST_NAME = 'Subscriber Last Name';
    public static final String CHAR_SHIPPING_REQUIRED = 'Shipping Required';
    public static final String CHAR_SHIPPING_ADDRESS_CHARACTERISTIC = 'Shipping Address Characteristic';
    public static final String CHAR_SHIPPING_ADDRESS = 'Shipping Address';
    public static final String CHAR_SHIPPING_CONTACT_NAME = 'Contact Name';
    public static final String CHAR_SHIPPING_CONTACT_EMAIL = 'Contact Email';
    public static final String CHAR_SHIPPING_CONTACT_NUMBER = 'Contact Number';
    public static final String CHAR_CITY_OF_USE = 'City of use';
    public static final String CHAR_PROVINCE_OF_USE = 'Province of use';
    //End : Shippable Offers Characteristics
    //Start :Contract Term Characteristics
    public static final String CHAR_CONTRACT_TERM = 'Contract Term';
    //End :Contract Term Characteristics
    public static final String NC_FIELDWORKVALUE = 'Fielded';
    public static final String CHAR_VALUE_NO = 'No';
    public static final String CHAR_VALUE_YES = 'Yes';
    public static final String CHAR_REQUESTED_DATE = 'Requested Due Date';
    public static final String WFM_DISCONNECT_PROVISIONING_STATUS = 'DELETE';
    public static final String CONTRACT_TERM_CHAR='Contract Term';
    
   //Start: BB-209 - Added by Aditya Jamwal(IBM) 31 May,2017 
    public static final Map<String, String> GTACategoryMap;
    static {
        GTACategoryMap = new Map<String, String>();
        GTACategoryMap.put('TELUS Internet Product', 'INTERNET');
        GTACategoryMap.put('TELUS Office Phone Product', 'VOICE');
        GTACategoryMap.put('TELUS Wireless Product', 'MOBILE');
        GTACategoryMap.put('TELUS TV Product', 'TV');
        GTACategoryMap.put('TELUS TV Product (v2)', 'TV');
        GTACategoryMap.put('TELUS SWIFT Product', 'WiFi');
        GTACategoryMap.put('TELUS Public Wi-Fi Product', 'WiFi');
    }
    public static final integer noOfCalloutAllowedforGTAOnCheckAddress = 20;
	//End: BB-209 - Added by Aditya Jamwal(IBM) 31 May,2017 
	
	//Start: BB-131 - Added by Aditya Jamwal(IBM) 31 May,2017 
	//BA -> Billing Account
	public static final String BA_SPECIFICATION_NAME = 'Billing Account Management'; 
    public static final String BA_SPECIFICATION_CATEGORY = 'Customer Account';    
    public static final String BA_SPECIFICATION_TYPE = 'Billing';
    public static final String BA_ORIGINATOR_APPLICATION_ID = 'originatorApplicationId';
    
    public static final String BA_CUSTOMER_ACCOUNT_BILLING_LEGAL_NAME = 'businessLegalName';
    public static final String BA_CUSTOMER_ACCOUNT_CUSTOMER_PROFILE_ID = 'customerProfileID';
    public static final String BA_CUSTOMER_ACCOUNT_CBUCID = 'customerBusinessUnitCustomerId';
    public static final String BA_CUSTOMER_ACCOUNT_CSR_ID = 'customerServiceRepresentativeId';
    public static final String BA_CUSTOMER_ACCOUNT_TAX_PROVINCE = 'taxProvince';
    public static final String BA_CUSTOMER_ACCOUNT_CREDIT_CLASS = 'creditClass';
    public static final String BA_CUSTOMER_ACCOUNT_DISPLAY_LANGUAGE = 'displayLanguage';
    
    public static final String BA_CUSTOMER_ACCOUNT_CORE_ACCOUNT_PREFIX = 'coreAccountPrefix';
    public static final String BA_CUSTOMER_ACCOUNT_BUSINESS_DOMAIN = 'businessDomain';
    public static final String BA_CUSTOMER_ACCOUNT_BUSINESS_SUB_DOMAIN = 'businessSubDomain';
    public static final String BA_CUSTOMER_ACCOUNT_RESPONSIBLE_GROUP = 'responsibleGroup';
    public static final String BA_CUSTOMER_ACCOUNT_PROVINCIALTAX_APPLIES_IND = 'provincialTaxAppliesInd';
    public static final String BA_CUSTOMER_ACCOUNT_BILLING_PERIOD_NUMBER = 'billingPeriodNumber';
    public static final String BA_CUSTOMER_ACCOUNT_FEDERALTAX_APPLIESIND = 'federalTaxAppliesInd';
	public static final String BA_CUSTOMER_ACCOUNT_SERVICE_TYPE_CODE = 'serviceTypeCode';
    public static final String BA_CUSTOMER_ACCOUNT_TYPE_OF_SERVICE = 'typeofService';
    public static final String BA_CUSTOMER_ACCOUNT_SERVICE_REQUESTED = 'serviceRequested';
	public static final String BA_CUSTOMER_ACCOUNT_CONTRACT_LENGTH_IN_MONTHS = 'contractLengthInMonths';
	public static final String BA_CUSTOMER_ACCOUNT_START_DATE = 'startDate';
    public static final String BA_CUSTOMER_ACCOUNT_BILL_LANGUAGE = 'billLanguage';
	public static final String BA_CUSTOMER_ACCOUNT_VALIDATE_ADDRESS = 'validateAddress';
    public static final String BA_CUSTOMER_ACCOUNT_TYPE_OF_SERVICE_NAAS = 'NAAS';
    
    //Contact Info
    public static final String BA_CONTACT_FIRSTNAME = 'contactFirstName';
    public static final String BA_CONTACT_LASTNAME = 'contactLastName';
    public static final String BA_CONTACT_EMAIL = 'contactEmail';
    public static final String BA_CONTACT_PHONE_NUMBER = 'contactPhoneNumber';
    
    //Billing Address Info
    public static final String BA_ADDRESS_SUITE_NUMBER = 'suiteNumber';
    public static final String BA_ADDRESS_HOUSE_NUMBER = 'houseNumber';
    public static final String BA_ADDRESS_STREET_NAME = 'streetName';
    public static final String BA_ADDRESS_STREET_DIRECTION = 'streetDirection';
    public static final String BA_ADDRESS_STREET_TYPE = 'streetType';
    public static final String BA_ADDRESS_CITY = 'city';
    public static final String BA_ADDRESS_PROVINCE = 'province';
    public static final String BA_ADDRESS_POSTAL_CODE = 'postalcode';
    
    //Billing Account Creation Response Charsteristic Info
    public static final String BA_CUSTOMER_ACCOUNT_NUMBER = 'customerAccountNumber';
    public static final String BA_BILLING_SYSTEM_ID = 'billingSystemId';
    public static final String BA_BTN = 'billingTelephoneNumber';
    
   //End: BB-131 - Added by Aditya Jamwal(IBM) 31 May,2017 
    public static final String NHP_PROD_SPEC_NAME='Partner Internet Product';
    // Start Order 2.0 
    public static final String ORDR_ACTIVATED_STATUS='Activated';    
    public static final String ORDER_CANCEL_TYPE ='Cancel Entire Order';
    public static final String ORDER_CANCEL_ACTIVITIES = 'N/A';
    public static final String CAR_CANCEL_STATUS ='Cancelled';
    public static final String CAR_CANCEL_CODE ='Cancel order received: TELUS Initiated';
    public static final String ORDER_TYPE_NEW_SERVICES  ='New Services';
    public static final String ORDER_TYPE_MOVE  ='Move';
    public static final String NO_ORDERID_PROVIDED_TO_UPDATE_ORDER = Label.OC_No_Order_Id_is_provided_to_update_this_order;
    public static final String NO_ORDERID_PROVIDED_TO_CANCEL_ORDER = Label.OC_No_OrderId_is_found_to_initiated_cancel_request;
    public static final String NO_ORDER_FOUND = Label.OC_No_Order_is_found;
    public static final String NO_ORDER_FOUND_TO_DELETE = Label.OC_No_order_is_found_to_delete;
    public static final String ORDER_CANCELLATION_REQUEST_FAILED = Label.OC_Order_Cancellation_Request_Is_Failed;
    public static final String ORDER_STATUS_PONR_PASSED = Label.OC_Order_has_PONR_Passed_Cannot_be_Cancelled;
    public static final String CUSTOMER_ORDER_RECORDTYPEID_NOT_FOUND = Label.OC_Customer_Order_Record_Type_Id_is_not_found;
    public static final String CUSTOMER_SOLUTION_RECORDTYPEID_NOT_FOUND = Label.OC_Customer_Solution_Record_Type_Id_is_not_found;
    public static final String NO_CUSTOMER_ORDERS_FOUND_TO_CREATE = Label.OC_No_child_order_found_to_create_customer_orders;
    
    //BSBD_RTA-1337 missing distrubution channel for dealer during order submit
    public static final String BUSINESS_COPPER_ORDER='Business Copper Order Entry';
    public static final String DEALER_CHANNEL='Dealer Channel';
    // End Order 2.0 
}