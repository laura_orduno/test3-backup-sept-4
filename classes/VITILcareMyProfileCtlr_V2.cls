public with sharing class VITILcareMyProfileCtlr_V2 {
    
    public transient String pageError {get; set;}
    public transient String generalError {get; set;}
    public transient String profileUpdateComplete {get; set;}
    public transient String usernameError {get; set;}
    public transient String oldPasswordError {get; set;}
    public transient String weakPassword {get; set;}
    public transient String passwordNotMatch {get; set;}    
    public transient String[] errors {get; private set;}
    public Account accountDetail {get; set;}
    public Contact contactDetail {get; set;}
    public String editMode {get; set;}
    public String oldPassword {get; set;}
    public String newPassword {get; set;}
    public String confirmPassword {get; set;}
    public String username {
        get {
            return contactDetail.email;
        }
        set {
            contactDetail.email = value;
        }
    }
    public STATIC String EMAIL_PATTERN = '^[_A-Za-z0-9-&\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';
        
    public PageReference initCheck()
    {
        try
        {
            String userAccountId;
            String userContactId;
            //userAccountId = (String)UserUtil.CurrentUser.AccountId;
            //userContactId = (String)UserUtil.CurrentUser.ContactId;
            List<User> users = [SELECT Id, AccountId, ContactId FROM User WHERE Id = :UserInfo.getUserId()];
            if (users.size() > 0) {
                userAccountId = users[0].AccountId;
                userContactId = users[0].ContactId;
            }
            accountDetail = [SELECT name FROM Account WHERE id = :userAccountId LIMIT 1];
            contactDetail = [SELECT Preferred_Contact_Method__c, Email, FirstName, LastName, mailingstreet, mailingcity, mailingstate, mailingcountry, mailingpostalcode, MobilePhone, Phone, id, Title FROM Contact WHERE id = :userContactId LIMIT 1];
            editMode = ApexPages.currentPage().getParameters().get('m');
            
            if(editMode == 'completed')
            {
            	profileUpdateComplete = label.mbrProfileUpdated;        
            }
        }
        catch(Exception e)
        {
            accountDetail = null;
            contactDetail = null;
            system.debug(e);
        }
        
        return null;
    }
        
    public void updateUser()
    {
        PageReference myProfilePage = Page.MBRMyProfile;
        myProfilePage.setRedirect(true);

        User currentUser = [SELECT Id, Name, UserName, Email, Alias, AccountId, ContactId FROM User WHERE Id = :UserInfo.getUserId()];
        
        if(username != null && !username.equals(currentUser.username)) {
            try 
            {
                if(!Pattern.matches(MBRMyProfileCtlr.EMAIL_PATTERN, username)) {
                //if(!trac_PageUtils.isValidEmail(username)) {
                    usernameError = label.mbrInvalidEmail;
                	return;
                }
                List<User> users = [select email, username from user where username = :username and UserType = 'CspLitePortal' and IsActive = true];
                if(users.size()>0) {
                    usernameError = label.mbrDuplicateUsername;
                	return;
                }
                currentUser.username = username;
                currentUser.email = username;
                update currentUser;
                
                return;
            }
            catch(Exception e) 
            {
                usernameError = e.getMessage();
                return;
            }
        }
        
		return;
    }

    
//    public void updateContact()
    public PageReference updateContact()
    {       
//        System.debug('MBRMyProfileCtlr updateContact accountDetail: ' + accountDetail);

        User currentUser = [SELECT Id, Name, UserName, Email, Alias, AccountId, ContactId, FirstName, LastName FROM User WHERE Id = :UserInfo.getUserId()];
        
        if(contactDetail==null) return null;
        
        if(username != null) {
            if(!Pattern.matches(MBRMyProfileCtlr.EMAIL_PATTERN, username)) {
            //if (!trac_PageUtils.isValidEmail(username)) {
                usernameError = label.mbrInvalidEmail;
                return null;
            }
        }


        Database.Saveresult sr = Database.update(contactDetail, false);
        currentUser.FirstName = contactDetail.FirstName;
        currentUser.LastName = contactDetail.LastName;
        update currentUser;
        
        System.debug(sr);
        
        if (!sr.isSuccess()) {
            String exceptionString = '';
            
            for (Database.Error error : sr.getErrors()) {
                exceptionString = error.getMessage();
            }
            
            generalError = label.mbrProfileUpdateError;
            return null;
        }
        
        PageReference temp;
        
        if(editMode == 'password'){
            system.debug('MBRMyProfileCtlr updateContact oldPassword: '+oldPassword);
            system.debug('MBRMyProfileCtlr updateContact newPassword: '+newPassword);
            system.debug('MBRMyProfileCtlr updateContact confirmPassword: '+confirmPassword);                
            if(oldPassword != '' || newPassword != '' || confirmPassword != '') {
                if(!MBRChangePasswordCtlr.isStrongPassword(newPassword)) {
                    weakPassword = label.mbrWeakPasswordError;
                    system.debug('MBRMyProfileCtlr updateContact weakPassword: '+weakPassword);
                    system.debug('MBRMyProfileCtlr updateContact isStrongPassword: '+newPassword+':'+confirmPassword);
                    return null;
                }
        
                if (newPassword != confirmPassword) {
                    passwordNotMatch = label.mbrPasswordDoNotMatchError;
                    system.debug('MBRMyProfileCtlr updateContact isvalidpassword: '+newPassword+':'+confirmPassword);
                    return null;
                }
        
                system.debug('MBRMyProfileCtlr updateContact Userinfo.getUserName(): '+Userinfo.getUserName());
                system.debug('MBRMyProfileCtlr updateContact Userinfo.getUserEmail(): '+Userinfo.getUserEmail());        
                temp = Site.changePassword(newPassword, confirmPassword, oldPassword);
                System.debug('MBRMyProfileCtlr updateContact temp changePasswordResult: '+temp);
            }
    	}
    
        //if (!ApexPages.getMessages().isEmpty()) {
        if (ApexPages.hasMessages()) {
            // Hack to get around a weird problem where pageMessages component
            // does not render messages generated by Site.changePassword method.
            errors = new String[]{};
            for (ApexPages.Message msg : ApexPages.getMessages()) {
                errors.add(msg.getDetail());
            }
        }
                
        if(temp==null && errors!=null){
            if(errors[0].contains('You cannot reuse this old password')){
                weakPassword = label.mbrCannotReusePassword;
            }
            else if(errors[0].contains('Your old password is invalid')){
                oldPasswordError = label.mbrOldPasswordError;
            }
            else {
                weakPassword = errors[0];
            }
        }
        else {
            profileUpdateComplete = label.mbrProfileUpdated;        
            PageReference pref = Page.MBRMyProfileV2;
            pref.getParameters().put('m', 'completed');
            if (editMode != null && editMode.length() > 0) return pref;
        }
        
        return null;
    }   
    
    public PageReference cancelUpdate(){
    	return Page.MBRMyProfileV2;
    }    
    
    
}