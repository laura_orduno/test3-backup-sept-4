@isTest
public class ReasonCodeValidatorControllerTest{
	
    @isTest
    public static void testupdateContactSelectFiledIds(){
       
       Test.setMock(HttpCalloutMock.class, new ToolingAPILightMock(testObjectResponse,testFieldResponse, 200));
       Test.startTest(); 
       ReasonCodeValidatorController  validator = new ReasonCodeValidatorController();
       validator.updateContactSelectFiledIds();
       Test.stopTest(); 
        
    }
    
    @isTest
    public static void testUpdateApprovalFlowSettings(){
       ReasonCodeValidatorController  validator = new ReasonCodeValidatorController();
       validator.updateApprovalFlowSettings();  
    }
    
     @isTest
    public static void testGetMethods(){
       BillingAdjustmentCommonTest.createReasonCodeData();
       ReasonCodeValidatorController  validator = new ReasonCodeValidatorController();
       validator.getResult();
       validator.getResultMap();  
    }
    
     //********* MOCK Object ***********
	public class ToolingAPILightMock implements HttpCalloutMock {
		private String testObjectResponse;
        private String testFieldResponse;
		private Integer testStatusCode;

		public ToolingAPILightMock(String testObjectResponse, string testFieldResponse, Integer testStatusCode){
			this.testObjectResponse = testObjectResponse;
            this.testFieldResponse = testFieldResponse;
			this.testStatusCode = testStatusCode;
		}

		public HTTPResponse respond(HTTPRequest req) {
         	HttpResponse res = new HttpResponse();
            String a = 'a';
            if(req.toString().contains('CustomField')){
                 res.setBody(testFieldResponse); 
            }else{
                 res.setBody(testObjectResponse);
            }
            res.setStatusCode(testStatusCode);
			return res;
		}
	}

	private static String testObjectResponse =
		'{' +
		  '"size" : 1,' +
		  '"totalSize" : 1,' +
		  '"done" : true,' +
		  '"records" : [ {' +
		    '"attributes" : {' +
		      '"type" : "CustomObject",' +
		      '"url" : "/services/data/v29.0/tooling/sobjects/CustomObject/01IG00000021cXoMAI"' +
		    '},' +
		    '"Id" : "01IG00000021cXoMAI",' +
		    '"DeveloperName" : "Billing_Adjustment__c"' +
		  '} ],' +
		  '"queryLocator" : null,' +
		  '"entityTypeName" : "CustomEntityDefinition"' +
		'}';

	private static String testFieldResponse =
		'{' +
		  '"size" : 1,' +
		  '"totalSize" : 1,' +
		  '"done" : true,' +
		  '"queryLocator" : null,' +
		  '"records" : [ {' +
		    '"attributes" : {' +
		      '"type" : "CustomField",' +
		      '"url" : "/services/data/v29.0/tooling/sobjects/CustomField/00NG0000009Y0I9MAK"' +
		    '},' +
		    '"DeveloperName" : "Account",' +
		    '"Id" : "00NG0000009Y0I9MAK",' +
		    '"FullName" : "01IG00000021cXo.Account",' +
		    '"TableEnumOrId" : "01IG00000021cXoMAI",' +
		    '"NamespacePrefix" : null' +
		  '},' +
          '{' +
		    '"attributes" : {' +
		      '"type" : "CustomField",' +
		      '"url" : "/services/data/v29.0/tooling/sobjects/CustomField/00NG0000009Y0I9MAK"' +
		    '},' +
		    '"DeveloperName" : "Contact",' +
		    '"Id" : "00NG0000009Y0I9MAK",' +
		    '"FullName" : "01IG00000021cXo.Contact",' +
		    '"TableEnumOrId" : "01IG00000021cXoMAI",' +
		    '"NamespacePrefix" : null' +
		  '}],' +
		  '"entityTypeName" : "CustomFieldDefinition"' +
		'}';

	private static String testErrorResponse =
		'[{' +
		  '"errorCode" : "INVALID_FIELD",' +
		  '"message" : "message"' +
		'}]';

	private static String testApexClassMemberQueryResponse = '{'
    	+ '"size": 1,' 
    	+ '"totalSize": 1,'
    	+ '"done": true,'
    	+ '"records": [{'
        + '"attributes": {'
        + '    "type": "ApexClassMember",'
        + '    "url": "/services/data/v28.0/tooling/sobjects/ApexClassMember/400G00000005IaoIAE"'
        + '},'
        + '"Id": "400G00000005IaoIAE",'
        + '"Body": "body",'
        + '"Content": "content",'
        + '"ContentEntityId": "01pG0000003ZjfTIAS",'
        + '"LastSyncDate": "2014-01-28T14:51:03.000+0000",'
        + '"Metadata": {'
        + '    "apiVersion": 28.0,'
        + '    "packageVersions": null,'
        + '    "status": "Active",'
        + '    "module": null,'
        + '    "urls": null,'
        + '    "fullName": null'
        + '},'
        + '"MetadataContainerId": "1drG0000000EKF0IAO",'
        + '"SymbolTable": {'
        + '    "tableDeclaration": {'
        + '        "modifiers": [],'
        + '        "name": "ContactExt",'
        + '        "location": {'
        + '            "column": 27,'
        + '            "line": 1'
        + '        },'
        + '        "type": "ContactExt",'
        + '        "references": []'
        + '    },'
        + '    "variables": [{'
        + '        "modifiers": [],'
        + '        "name": "stdController",'
        + '        "location": {'
        + '            "column": 52,'
        + '            "line": 9'
        + '        },'
        + '        "type": "StandardController",'
        + '        "references": [{'
        + '            "column": 30,'
        + '            "line": 10'
        + '        }, {'
        + '            "column": 35,'
        + '            "line": 11'
        + '        }]'
        + '    }],'
        + '    "externalReferences": [],'
        + '    "innerClasses": [],'
        + '    "name": "ContactExt",'
        + '    "constructors": [{'
        + '        "parameters": [{'
        + '            "name": "stdController",'
        + '            "type": "StandardController"'
        + '        }],'
        + '        "visibility": "PUBLIC",'
        + '        "modifiers": [],'
        + '        "name": "ContactExt",'
        + '        "location": {'
        + '            "column": 12,'
        + '            "line": 9'
        + '        },'
        + '        "type": null,'
        + '        "references": []'
        + '    }],'
        + '    "key": "01pG0000003ZjfT",'
        + '    "methods": [{'
        + '        "returnType": "PageReference",'
        + '        "parameters": [],'
        + '        "visibility": "PUBLIC",'
        + '        "modifiers": [],'
        + '        "name": "saveContact",'
        + '        "location": {'
        + '            "column": 26,'
        + '            "line": 14'
        + '        },'
        + '        "type": null,'
        + '        "references": []'
        + '    }, {'
        + '        "returnType": "PageReference",'
        + '        "parameters": [],'
        + '        "visibility": "PUBLIC",'
        + '        "modifiers": [],'
        + '        "name": "cancelChanges",'
        + '        "location": {'
        + '            "column": 26,'
        + '            "line": 19'
        + '        },'
        + '        "type": null,'
        + '        "references": []'
        + '    }],'
        + '    "properties": [{'
        + '        "visibility": "PRIVATE",'
        + '        "modifiers": [],'
        + '        "name": "myContact",'
        + '        "location": {'
        + '            "column": 18,'
        + '            "line": 3'
        + '        },'
        + '        "type": "Contact",'
        + '        "references": [{'
        + '            "column": 14,'
        + '            "line": 11'
        + '        }, {'
        + '            "column": 16,'
        + '            "line": 15'
        + '        }]'
        + '    }, {'
        + '        "visibility": "PRIVATE",'
        + '        "modifiers": [],'
        + '        "name": "stdController",'
        + '        "location": {'
        + '            "column": 42,'
        + '            "line": 4'
        + '        },'
        + '        "type": "StandardController",'
        + '        "references": [{'
        + '            "column": 14,'
        + '            "line": 10'
        + '        }]'
        + '    }],'
        + '    "id": "01pG0000003ZjfT",'
        + '    "namespace": "timesheet"'
        + '}'
        + '}],'
        + '"queryLocator": null,'
        + '"entityTypeName": "ApexClassMember"'
        + '}';
  
}