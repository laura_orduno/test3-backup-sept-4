public with sharing class EmailSubscriptionController {
    
    private Id contactId;
    
    public Boolean refreshParent {get; set;}
    public String parentTarget {get; set;}
    
    public EmailSubscriptionController(ApexPages.standardController sc) {
        contactId = sc.getId();
        refreshParent = false;
        parentTarget = sc.view().getUrl();
    }
    
    public PageReference doSubscribe() {
        Contact c = new Contact(Id = contactId, SubscribeContact__c = true, Subscribe_Request_Date__c = Date.today());
        update c;
        refreshParent = true;
        return null;
    }

    public PageReference doCancelSubscribe() {
        Contact c = new Contact(Id = contactId, SubscribeContact__c = false, Subscribe_Request_Date__c = null);
        update c;
        refreshParent = true;
        return null;
    }

    public PageReference doUnsubscribe() {
        Contact c = new Contact(Id = contactId, UnsubscribeContact__c = true, Unsubscribe_Request_Date__c = Date.today());
        update c;
        refreshParent = true;
        return null;
    }

      public PageReference doCancelUnSubscribe() {
        Contact c = new Contact(Id = contactId, UnsubscribeContact__c = false, Unsubscribe_Request_Date__c = null);
        update c;
        refreshParent = true;
        return null;
    }  

}