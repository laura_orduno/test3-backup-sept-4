global class RestApi_MyAccountHelper_GetCaseDetails {
    public static final string INTERNAL_USER = 'Telus Agent';
    public static final string EMAIL2CASE_PROFILE = RestApi_MyAccountHelper_Utility.EMAIL2CASE_PROFILE;
    public static final string INTEGRATION_PROFILE = RestApi_MyAccountHelper_Utility.INTEGRATION_PROFILE;
    
    public class GetCaseDetailsRequestObject extends RestApi_MyAccountHelper.RequestObject{
        public Id caseId = null;
        public String uuid = '';
        public List<String> billing;
    }
    // end GetCaseDetailsRequestObject classes

    // We may have to add this if we need to provide further context around the CaseComment
	public class CaseCommentResponse{
		public Boolean agent = false;
		public CaseComment caseComment = null;
    }
    // GetCaseDetailsResponse   
    // derived child class to act as the JSON response
    global class GetCaseDetailsResponse extends RestApi_MyAccountHelper.Response{
        public Case caseDetails = null;
        public List<CaseCommentResponse> caseComments = null;
        public List<Attachment> attachments = null;
        String sbsFlag = null;

        public GetCaseDetailsResponse(GetCaseDetailsRequestObject request){
            super(request);
            
            if(request.caseId == null){
                throw new xException('No caseId was sent in the request');                
            }

            if(request.billing == null){
                throw new xException('No Account IDs sent in the request');                
            }    

            //need to figure out what the request param will be for uuid
            User u = RestApi_MyAccountHelper_Utility.getUser(request.uuid);

            // if user not found
            if(u == null){
                throw new xException('User not found for uuid: ' + request.uuid);                
            }

            if(u.ContactId == null){
                throw new xException('No contact associated with user: ' + request.uuid);
            }

            if (!u.Contact.MASR_Enabled__c) {
                throw new xException('User is not MASR Enabled.');
            }

            Contact c = RestApi_MyAccountHelper_Utility.getContact(u.contactId);

            if(c == null){
                throw new xException('No contact associated with user: ' + request.uuid);
            }

            if(c.MASR_Enabled__c){
	            caseDetails = RestApi_MyAccountHelper_Utility.getCaseDetailsForUser( request.caseId, c.Id, request.billing );
	            if( caseDetails == null )
	            	throw new xException('Not authorized to update the Case');
	            	
	            caseDetails.Status = RestApi_MyAccountHelper_Utility.translateStatus(caseDetails.Status);
	            caseComments = getCaseComments(request.caseId);
	            attachments = getAttachments(request.caseId);
            	sbsFlag = 'Yes';
        	} else {
        		sbsFlag = 'No';
        	}

        }    

        // We may have to adjust this to return the custom wrapper class if further detail is needed
        private List<CaseCommentResponse> getCaseComments(Id caseId){

            try{                
                List<CaseComment> comments = [
                	SELECT 	Id, Commentbody, CreatedDate, CreatedBy.ProfileId 
                    FROM 	CaseComment 
                    WHERE 	IsPublished = true 
                    AND 	ParentId = :caseId
                ];
                List<CaseCommentResponse> crs = new List<CaseCommentResponse>();
                for(CaseComment c : comments){
                    CaseCommentResponse cr = new CaseCommentResponse();
                    cr.agent = c.CreatedBy.ProfileId != INTEGRATION_PROFILE && c.CreatedBy.ProfileId != EMAIL2CASE_PROFILE;
                    cr.caseComment = c;
                    crs.add(cr);
                }

                return crs;
            }catch(QueryException e){
                // cannot throw execption if null as that is valid.
                //throw new xException('Case: ' + e.getMessage());

                return null;  
            }catch(Exception e){
                throw new xException('Case Comments: ' + e.getMessage());                            
            }

        }

        private List<Attachment> getAttachments(Id caseId){

            try{                
                return [
                	SELECT 	Id, Name, CreatedDate
                    FROM 	Attachment 
                    WHERE 	ParentId = :caseId
                ];
            }
            catch(QueryException e){
                // cannot throw execption if null as that is valid.
                //throw new xException('Case: ' + e.getMessage());

                return null;  
            }

            catch(Exception e){
                throw new xException('Attachments: ' + e.getMessage());                            
            }
        }

    } 

    // object to be returned as JSON response   
    public GetCaseDetailsResponse response = null;
    
    public RestApi_MyAccountHelper_GetCaseDetails(String requestString){

        // convert request to object 
        GetCaseDetailsRequestObject getCaseDetailsRequest = (GetCaseDetailsRequestObject)JSON.deserialize(requestString, GetCaseDetailsRequestObject.class);                    
        response = new GetCaseDetailsResponse(getCaseDetailsRequest);    

    } 
    
}