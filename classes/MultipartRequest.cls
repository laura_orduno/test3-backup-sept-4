public class MultipartRequest {
	private final List<Part> parts = new List<Part>();
	private String boundary;
	private String body;
	
	public void addPart(Part part) {
		this.parts.add(part);		
	}
	
	public void prepare() {
		createBoundary();
		createBody();	
	}
	
	public String getBody() {
		return body;
	}
	
   	private void createBoundary() {
       	String suffix = String.valueOf(System.now().getTime());
       	boundary = '';
       	for (Integer i = 0; i < 32 - suffix.length(); i++) {
           	boundary += '-';
       	}
       	boundary += suffix;
   	}	
    
   	private void createBody() {
   		body = '';
   		for (Part part: parts) {
			body += '--' + boundary + '\r\n';
			body += 'Content-Disposition: ' + part.getContentDisposition();
			Map<String, String> attributes = part.getContentDispositionAttributes();
			for (String key : attributes.keySet()) {
				String value = attributes.get(key);
				body += '; ' + key + '="' + value + '"'; 
			}
			body += '\r\n';
			String contentType = part.getContentType();
			if (contentType != null) {
				body += ('Content-Type: ' + contentType + '\r\n');
			}
			String contentTransferEncoding = part.getContentTransferEncoding();
			if (contentTransferEncoding != null) {
				body += (+ 'Content-Transfer-Encoding: ' + contentTransferEncoding + '\r\n');
			}
			body += '\r\n';
			body += part.getContent();
			body += '\r\n';
   		}
   		body += '\r\n--' + boundary + '--\r\n';
   	}

	public String getBoundary() {
		return boundary;
	}
				
	
	public class Part {
		private String contentDisposition;
		private final Map<String, String> contentDispositionAttributes = new Map<String, String>();
		private String contentType;
		private String contentTransferEncoding;
		private String content;
		
		public void setContentDisposition(String contentDisposition) {
			this.contentDisposition = contentDisposition;
		}
		
		public String getContentDisposition() {
			return contentDisposition;
		}
		
		public void addContentDispositionAttribute(String name, String value) {
			this.contentDispositionAttributes.put(name, value);
		}
		
		public Map<String, String> getContentDispositionAttributes() {
			return contentDispositionAttributes;
		}
		
		public void setContentType(String contentType) {
			this.contentType = contentType;
		}
		
		public String getContentType() {
			return contentType;
		}
		
		public void setContentTransferEncoding(String contentTransferEncoding) {
			this.contentTransferEncoding = contentTransferEncoding;
		}
		
		public String getContentTransferEncoding() {
			return contentTransferEncoding;
		}
		
		public void setContent(String content) {
			this.content = content;
		}
		
		public String getContent() {
			return content;
		}
		
	}	

}