@isTest
private class VlProductEligibilityImplTest {
    @isTest
    private static void testGetEligibleProductsFromOrder_Standard() {
        VlProductEligibilityImpl productEligibility = new VlProductEligibilityImpl();
 
        List<Order> orders = OrdrTestDataFactory.createOrdersWithServiceAddress();
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        //orders[0] has Standard address
        List<SObject> sObjList = productEligibility.getEligibleProducts(orders[0], null);
        Test.stopTest();
    } 

    @isTest
    private static void testInvokeMethodFromQuote_LotBlockPlan() {
        VlProductEligibilityImpl productEligibility = new VlProductEligibilityImpl();
 
        List<Quote> quotes = OrdrTestDataFactory.createQuotesWithServiceAddress();
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        //quotes[1] has LotBlockPlan address
        List<SObject> sObjList = productEligibility.getEligibleProducts(quotes[1], null);
        Test.stopTest();
    }    
    
    @isTest
    private static void testInvokeMethodFromOrder_LegalLandDesc() {
        VlProductEligibilityImpl productEligibility = new VlProductEligibilityImpl();
 
        List<Order> orders = OrdrTestDataFactory.createOrdersWithServiceAddress();
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        //orders[2] has LegalLandDesc address
        List<SObject> sObjList = productEligibility.getEligibleProducts(orders[2], null);
        Test.stopTest();
    }
    
    @isTest
    private static void testInvokeMethodFromQuote_TownshipRangeRoadHwy() {
        VlProductEligibilityImpl productEligibility = new VlProductEligibilityImpl();
 
        List<Quote> quotes = OrdrTestDataFactory.createQuotesWithServiceAddress();
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        //quotes[3] has TownshipRangeRoadHwy address
        List<SObject> sObjList = productEligibility.getEligibleProducts(quotes[3], null);
        Test.stopTest();
    }  
    
    @isTest
    private static void testInvokeMethod_Exception() {
        VlProductEligibilityImpl productEligibility = new VlProductEligibilityImpl(); 

        Test.setMock(WebServiceMock.class, new OrdrWsTpMockImpl());
        try {
            List<SObject> sObjList = productEligibility.getEligibleProducts(null, null);
            //System.assert(false, 'Exception expected');
        }
        catch(Exception e) {
            System.debug(e.getTypeName() + ': ' + e.getMessage());
        }
        
    }     

}