/**
 * @author Steve Doucette, Traction on Demand 
 * @date 5/28/2018
 */
public with sharing class PortalEntryController {
	public PortalEntryController() {
		// Set user language to what is passed via URL Param
		String lang = ApexPages.currentPage().getParameters().get('lang');
		PortalUtils.setUserLanguage(lang);
	}

	public PageReference redirectToSSO() {
		String url = URL.getCurrentRequestUrl().toExternalForm().substringBefore('/apex')
				+ '/login?so=' + UserInfo.getOrganizationId();
		return new PageReference(url);
	}
}