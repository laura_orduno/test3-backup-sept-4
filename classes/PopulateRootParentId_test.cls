@isTest
public class PopulateRootParentId_test {
    
    

    @isTest
    Static void testMethod_two(){
        Map<string, Id> recordTypes = new Map<string, Id>();
        Map<string,schema.RecordtypeInfo> recordTypeInfoMap=Account.sobjecttype.getDescribe().getRecordTypeInfosByName();
        
        for (Schema.RecordTypeInfo recordTypeInfo:recordTypeInfoMap.values()) {
        
            if ((recordTypeInfo.getName().equalsignorecase('CBUCID'))
            || (recordTypeInfo.getName().equalsignorecase('RCID'))
            || (recordTypeInfo.getName().equalsignorecase('CAN'))
            || (recordTypeInfo.getName().equalsignorecase('BAN'))) //added for ICCD changes
           {
                                        
            recordTypes.put(recordTypeInfo.getName(), recordTypeInfo.getRecordTypeId());                               
            }
        }   
        account acc = new account(name='Test Acc'); 
        acc.RecordTypeId = recordTypes.get('RCID');
       // acc.RecordType.DeveloperName = 'RCID'; 
        acc.InActive__c = false;   
        insert acc; 
        
        Product2 prod = new Product2(Name = 'Disconnect Services',Family = 'Hardware',OCOM_Bookable__c='Yes');
        insert prod;
        List<Asset> assetList =new List<Asset>();
        for(integer i=0;i<200;i++){
            Asset testAsset = new Asset();
            testAsset.AccountId=acc.Id;
            testAsset.Name = 'test';
            testAsset.Product2Id = prod.Id;
            if(i>150){
                testAsset.vlocity_cmt__LineNumber__c = '0001.0003';
            }
            else{
                testAsset.vlocity_cmt__LineNumber__c = '0001';
            }
            testAsset.vlocity_cmt__rootItemId__c='';
            testAsset.orderMgmt_BPI_Id__c='100';
            testAsset.vlocity_cmt__ServiceAccountId__c = acc.Id;
            assetList.add(testAsset);
        }
        insert assetList;
        system.debug('AssetList'+assetList.size());
        List<Asset> testAssets=[SELECT Id,vlocity_cmt__rootItemId__c,orderMgmt_BPI_Id__c,vlocity_cmt__LineNumber__c,
                         Product2Id,AccountId,Name from Asset];
        Test.startTest();
        Database.executeBatch(new Prod_PopulateRootParentId());
        Test.stopTest();
    }
}