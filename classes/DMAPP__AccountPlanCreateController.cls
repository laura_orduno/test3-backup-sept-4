/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class AccountPlanCreateController extends DMAPP.TASBaseController {
    @RemoteAction
    global static Object createChatterGroup(CollaborationGroup chattergroup) {
        return null;
    }
    @RemoteAction
    global static String deletePlan(String planId, String path) {
        return null;
    }
    @RemoteAction
    global static DMAPP.AccountPlanCreateController.ChatterPostDetails getChatterPostDetails(String planId) {
        return null;
    }
    @RemoteAction
    global static Object getPlanSettings(String id) {
        return null;
    }
    @RemoteAction
    global static Object postToGroupsFeed(String chatterGroupId, String msg) {
        return null;
    }
    @RemoteAction
    global static Object postToUsersFeed(String msg) {
        return null;
    }
    @RemoteAction
    global static DMAPP.OpportunityMapAdminRestResourceDelegate.PutResult putPlanSettings(String planid, DMAPP.OpportunityMapAdminRestResourceDelegate.PutParam param) {
        return null;
    }
    @RemoteAction
    global static Object searchForAccounts(String term, String ownerid, String accountsIdsToFilterBy, Integer hiddenAccountsCountFromParent) {
        return null;
    }
    @RemoteAction
    global static Object searchForUsers(String name, String title, String extra, String theType, Boolean reverse) {
        return null;
    }
global class ChatterPostDetails {
    global ChatterPostDetails(DMAPP__DM_Account_Plan__c plan, List<CollaborationGroupFeed> feed) {

    }
}
}
