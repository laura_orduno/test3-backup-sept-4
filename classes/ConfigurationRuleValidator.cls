/**
 * Implementation of ProductRuleValidator that validates correct configuration of bundles.
 * 
 * @author Max Rudman
 * @since 4/9/2011
 */
public class ConfigurationRuleValidator extends AbstractProductRuleValidator {
	private Id productId;
	private SBQQ__ProductOption__c[] options;
	
	public ConfigurationRuleValidator(Id productId, List<SBQQ__ProductOption__c> options) {
		super(new SBQQ__Quote__c());
		this.productId = productId;
		this.options = options;
	}
	
	public override Set<Id> loadRuleIds() {
		Set<Id> ids = new Set<Id>();
		for (SBQQ__ConfigurationRule__c configRule : 
			[SELECT SBQQ__ProductRule__c FROM SBQQ__ConfigurationRule__c 
			 WHERE SBQQ__Product__c = :productId AND SBQQ__Active__c = true]) {
			ids.add(configRule.SBQQ__ProductRule__c);
		}
		return ids;
	}
	
	public override List<SObject> loadTargetRecords() {
		return options;
	}
}