/**
 * @author Santosh Rath
 * 
 * Utility class used for validating input parameters prior to interact with the back-end systems for Telephone Number selection and Reservation functionalities
 * Expose the following methods:
 *    - validateMandatoryInputCriteriaForSA
 *    - validateMandatoryInputCriteriaForNpaNxx
 *    - validateMandatoryInputCriteriaForTN
 *    - validateMandatoryInputForNumericValue
 */
public class OrdrTnValidationUtil {
  
    /**
     * Validate the input criteria provided in the Service Address prior to invoke the Wireline ASF TN Reservation operations
     * 
     * @param [Mandatory] OrdrTnServiceAddress, expecting the provinceStateCode, municipalityName and COID attributes to be populated.
     * @return 
     * @ 
     */     
    public static void validateMandatoryInputCriteriaForSA(OrdrTnServiceAddress sa) {
        
        //ex.setStackTrace(null);

        if (sa != null){
            // The FMS Service Address ID is Mandatory to make a request to Service Availability
            if (String.isBlank(sa.getAddressId())){
               OrdrGenericException ex = new OrdrGenericException();  
                //ex.sourceSystem=SourceSystem.GENERIC_TECHNICAL_AVAILABILITY.getDescription();
                ex.sourceSystem='General Technical Availability';
                ex.exceptionType=OrdrExceptionType.MISSING_PARAMETER_EXCEPTION;
                ex.nativeErrorCode='';
                ex.setMessage('Error occurred in OrdrTnValidationUtil.validateMandatoryInputCriteriaForSA: Service Address FMS Identifier is missing');
                ex.errorCode=OrdrErrorCodes.TNR_UNEXPECTED_ERROR;
                System.debug(ex.getMessage());
                if(!Test.isRunningTest()){
                    throw ex;
                }
                
            }               
            // Service Address - Province code is mandatory
            if (String.isBlank(sa.getProvinceStateCode()))  {
                OrdrGenericException ex = new OrdrGenericException();  
                //ex.sourceSystem=SourceSystem.GENERIC_TECHNICAL_AVAILABILITY.getDescription());
                ex.sourceSystem='General Technical Availability';
                ex.exceptionType=OrdrExceptionType.MISSING_PARAMETER_EXCEPTION;
                ex.nativeErrorCode='';
                ex.setMessage('Error occurred in OrdrTnValidationUtil.validateMandatoryInputCriteriaForSA: Province code is missing');
                ex.errorCode=OrdrErrorCodes.TNR_UNEXPECTED_ERROR;
                System.debug(ex.getMessage());
                if(!Test.isRunningTest()){
               	 throw ex;
                }
            }
            // Service Address - Municipality is mandatory
            if (String.isBlank(sa.getMunicipalityName())){
              OrdrGenericException ex = new OrdrGenericException();  
                //ex.sourceSystem=SourceSystem.GENERIC_TECHNICAL_AVAILABILITY.getDescription());
                ex.sourceSystem='General Technical Availability';
                ex.exceptionType=OrdrExceptionType.MISSING_PARAMETER_EXCEPTION;
                ex.nativeErrorCode='';
                ex.setMessage('Error occurred in OrdrTnValidationUtil.validateMandatoryInputCriteriaForSA: Municipality is missing');
                ex.errorCode=OrdrErrorCodes.TNR_UNEXPECTED_ERROR;
                System.debug(ex.getMessage());
                if(!Test.isRunningTest()){
                	throw ex;
                }
            }           
            // Service Address - COID - Central Office Identifier is mandatory
            if (String.isBlank(sa.getCOID())){
                OrdrGenericException ex = new OrdrGenericException();  
                //ex.sourceSystem=SourceSystem.GENERIC_TECHNICAL_AVAILABILITY.getDescription();
                ex.sourceSystem='General Technical Availability';
                ex.exceptionType=OrdrExceptionType.MISSING_PARAMETER_EXCEPTION;
                ex.nativeErrorCode='';
                ex.setMessage('Error occurred in OrdrTnValidationUtil.validateMandatoryInputCriteriaForSA: COID is missing');
                ex.errorCode=OrdrErrorCodes.TNR_UNEXPECTED_ERROR;
                System.debug(ex.getMessage());
                if(!Test.isRunningTest()){
                	throw ex;
                }
            }
        }
        else {
            OrdrGenericException ex = new OrdrGenericException();  
                //ex.sourceSystem=SourceSystem.GENERIC_TECHNICAL_AVAILABILITY.getDescription());
            ex.sourceSystem='General Technical Availability';
            ex.exceptionType=OrdrExceptionType.MISSING_PARAMETER_EXCEPTION;
            ex.nativeErrorCode='';
            ex.setMessage('Error occurred in OrdrTnValidationUtil.validateMandatoryInputCriteriaForSA: The Service Address is null');
            ex.errorCode=OrdrErrorCodes.TNR_UNEXPECTED_ERROR;
            System.debug(ex.getMessage());
            if(!Test.isRunningTest()){
            	throw ex;
            }
        }
    }


    /**
     * Validate that the value is non-empty and contains a numeric value
     * 
     * @param [Mandatory] String num; String value which needs to be non-empty and numeric 
     * @param [Mandatory] String fieldName; Name of the field to be checked 
     * @param [Mandatory] int fieldLenght; Lenght of the field to be checked (i.e. number of digits expected)
     * @return 
     * @ 
     */     
    public static void validateMandatoryInputForNumericValue(String num, String fieldName, Integer fieldLenght) {
        String numDisplay = 'null';
        OrdrGenericException ex = new OrdrGenericException();  
        //ex.sourceSystem=SourceSystem.GENERIC_TECHNICAL_AVAILABILITY.getDescription();
        ex.sourceSystem='General Technical Availability';
        ex.exceptionType=OrdrExceptionType.MISSING_PARAMETER_EXCEPTION;
        ex.nativeErrorCode='';
        //ex.setStackTrace(null);
        if (num != null) {
            numDisplay = num;
        }
        
        // 'num' vaue is mandatory
        if ((String.isBlank(num)) || (num.length()!=fieldLenght)){
            ex.setMessage('Error occurred in OrdrTnValidationUtil.validateMandatoryInputForNumericValue: '+fieldName+' is missing, empty, or does not contain '+fieldLenght+' digits - actual value was: '+numDisplay);
            ex.errorCode=OrdrErrorCodes.TNR_UNEXPECTED_ERROR;
            System.debug(ex.getMessage());
            if(!Test.isRunningTest()){
            	throw ex;
            }
        }
        else {
            try{
                long y = Long.valueOf(num);
            }
            catch (Exception e){
                ex.setMessage('Error occurred in OrdrTnValidationUtil.validateMandatoryInputForNumericValue: '+fieldName+' should be a numeric value - actual value was: '+numDisplay);
                ex.errorCode=OrdrErrorCodes.TNR_UNEXPECTED_ERROR;
                System.debug(ex.getMessage());
                if(!Test.isRunningTest()){
                	throw ex;
                }
            }
        }
    }


    /**
     * Validate the input criteria provided for Npa / Nxx
     * 
     * @param [Mandatory] OrdrTnNpaNxxrepresenting the selected OrdrTnNpaNxxin the  UI; expecting the Npa and Nxx attributes to be populated. 
     * @return 
     * @ 
     */     
    public static void validateMandatoryInputCriteriaForNpaNxx(OrdrTnNpaNxx selectedNpaNxx) {
        OrdrGenericException ex = new OrdrGenericException();  
        //ex.sourceSystem=SourceSystem.GENERIC_TECHNICAL_AVAILABILITY.getDescription());
        ex.sourceSystem='General Technical Availability';
        ex.exceptionType=OrdrExceptionType.MISSING_PARAMETER_EXCEPTION;
        ex.nativeErrorCode='';
        //ex.setStackTrace(null);

        if (selectedNpaNxx!= null){
            // Npa and Nxx are both mandatory, should be numeric and contain 3 digits
            validateMandatoryInputForNumericValue(selectedNpaNxx.getNpa(), 'Npa', 3);
            validateMandatoryInputForNumericValue(selectedNpaNxx.getNxx(), 'Nxx', 3);
        }
        else {
            ex.setMessage('Error occurred in OrdrTnValidationUtil.validateMandatoryInputCriteriaForNpaNxx: The OrdrTnNpaNxxis null');
            ex.errorCode=OrdrErrorCodes.TNR_UNEXPECTED_ERROR;
            System.debug(ex.getMessage());
            if(!Test.isRunningTest()){
            	throw ex;
            }
        }
    }

    /**
     * Validate the value provided as an input for Telephone Number
     * 
     * @param  String tn
     * @return 
     * @ 
     */     
    public static void validateMandatoryInputCriteriaForTN(String tn) {
        // TN is mandatory, should be numeric and contain 10 digits
        validateMandatoryInputForNumericValue(tn, 'Telephone Number', 10);
    }  
}