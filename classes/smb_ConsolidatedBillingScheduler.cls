global class smb_ConsolidatedBillingScheduler implements Schedulable {

	global void execute(SchedulableContext sc) {
      smb_ConsolidatedBillingHierarchy b = new smb_ConsolidatedBillingHierarchy(); 
      database.executebatch(b);
   }


}