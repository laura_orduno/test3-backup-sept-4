public with sharing class trac_ConvertToCloudProjectButtonCtlr {
    private Id requestId;
    
    public trac_ConvertToCloudProjectButtonCtlr() {
        requestId = System.currentPageReference().getParameters().get('id') ;
    }
    
    public PageReference doIt() {   
        SMET_Request__c request = [SELECT 
        								OwnerId, 
        								What_is_your_request__c, 
        								Alignment_with_all_impacted_groups__c, 
        								Submitted_by__r.Name, 
        								Ideal_Completion_Date__c, Funding__c, 
        								Impact_to_Business_Units__c,
        								Impacts_to_Business_Units__c, 
        								Project_Sponsor__c, 
        								L2R_Driver__c, 
        								L2R_Sub_Driver__c, 
        								Subject__c, 
        								Priority__c, 
        								SMET_Requirement__c, 
        								Description__c,
        								Business_Impact__c,
        								Alignment_across_all_Impacted_Groups__c
                                   FROM SMET_Request__c
                                   WHERE id = :requestId LIMIT 1];
        
        String priority; // priority picklist values are different on the two objects...
        if(request.Priority__c == 'Urgent' || request.Priority__c == 'High') {
            priority = 'Critical';
        } else {
            priority = 'Standard';
        }
                
        //SMET_Request__c.Impacts_to_Business_Units__c
        
        SMET_Project__c project = new SMET_Project__c(name = request.Subject__c,
                                                        Project_Sponsor__c = request.Project_Sponsor__c,
                                                        status__c = 'New',
                                                        Priority__c = priority,
                                                        Project_Overview__c = request.What_is_your_request__c,
                                                        OwnerId = request.OwnerId,
                                                        Business_Unit_Effected__c  = request.Impacts_to_Business_Units__c,
                                                        /*Alignment_across_all_Impacted_Groups__c = request.Alignment_with_all_impacted_groups__c,*/
                                                        Alignment_across_all_Impacted_Groups__c = request.Alignment_across_all_Impacted_Groups__c,
                                                        Business_Impact__c = request.Business_Impact__c,
                                                        //Business_Unit_Effected__c = request.Impacts_to_Business_Units__c,
                                                        L2R_Driver__c = request.L2R_Driver__c,
                                                        L2R_SubDriver__c = request.L2R_Sub_Driver__c,
                                                        Funding__c = request.Funding__c,
                                                        Planned_Release_Date__c = request.Ideal_Completion_Date__c,
                                                        Requester__c = request.Submitted_by__r.Name,
                                                        Milestone__c = 'Assessing'
                                                        );
		
		
		
		// assign record type
		RecordType rType = [SELECT Id FROM RecordType WHERE Name = 'Stakeholder Project' LIMIT 1];
		 
		if (rType != null) {
			project.RecordTypeId = rType.Id;
		}
		
                                                        
        insert project;

        /*SMET_Requirement__c requirement = new SMET_Requirement__c(name = request.subject__c,
                                                                  status__c = 'New',
                                                                  priority__c = priority,
                                                                  requirement_detail__c =
                                                                  request.description__c,
                                                                  OwnerId = request.OwnerId);
        insert requirement;
        
        request.SMET_Requirement__c = requirement.id; */
        update request;
        
        return new PageReference('/' + project.id);
    }
    
    static testMethod void testConverter() {
        // Retrieve two profiles, for the standard user and the system   
    
      // administrator, then populate a map with them.
      Map<String,ID> profiles = new Map<String,ID>();
      List<Profile> ps = [select id, name from Profile where name = 
         'Standard User' or name = 'System Administrator'];
      for(Profile p : ps){
         profiles.put(p.name, p.id);
      }

      // Create the users to be used in this test.  
      // First make a new user.

      User standard = new User(alias = 'standt', 
      email='standarduser123123@testorg.com', 
      emailencodingkey='UTF-8',
      phone = '6049969449',
      lastname='Testing', languagelocalekey='en_US', 
      localesidkey='en_US', 
      profileid = profiles.get('Standard User'), 
      timezonesidkey='America/Los_Angeles', 
      username='standarduser123123@testorg.com');

      insert standard;

      // Then instantiate a user from an existing profile  
    

      User admin = new User(alias = 'admint1', 
      email='adminuser123123@testorg.com', 
      emailencodingkey='UTF-8', 
      lastname='TestingAdmin', languagelocalekey='en_US', 
      localesidkey='en_US', 
      profileid = profiles.get('System Administrator'), 
      timezonesidkey='America/Los_Angeles', 
      phone = '6049969449',
      username='adminuser123123@testorg.com');

      insert admin;
      system.runas(admin) {
        SMET_Request__c req = new SMET_Request__c(Subject__c = 'Test Subject', Priority__c = 'Medium', What_is_your_request__c = 'Test description', Ownerid = userinfo.getuserid());
        insert req;
        System.currentPageReference().getParameters().put('id', req.Id);
        trac_ConvertToCloudProjectButtonCtlr ctlr = new trac_ConvertToCloudProjectButtonCtlr();
        PageReference p = ctlr.doit();
        System.assertNotEquals(p, null);
      }
      system.runas(standard) {  
        SMET_Request__c req2 = new SMET_Request__c(Subject__c = 'Test Subject', Priority__c = 'High', What_is_your_request__c = 'Test description', Ownerid = userinfo.getuserid());
        insert req2;
        System.currentPageReference().getParameters().put('id', req2.Id);
        trac_ConvertToCloudProjectButtonCtlr ctlr2 = new trac_ConvertToCloudProjectButtonCtlr();
        PageReference p2 = ctlr2.doit();
        System.assertNotEquals(p2, null);
      }
    }

}