@isTest
public class QuoteCustomConfigurationServiceTest {
    private static Web_Account__c account;
    private static Contact contact;
    private static SBQQ__Quote__c quote;
    private static AdditionalInformation__c addl;
    private static SBQQ__QuoteLine__c line;

    testMethod static void test() {
        setUp();
        
        QuoteCustomConfigurationService.getRequiredOrdinalByStatus(QuoteStatus.SENT_FOR_CREDIT_CHECK);
        QuoteCustomConfigurationService.isRequiredAdditionalInformationFilled(quote.Id, QuoteStatus.SENT_FOR_CREDIT_CHECK);
        List<SBQQ__QuoteLine__c> quoteLines = QuoteCustomConfigurationService.loadQuoteLines(quote.Id);
        QuoteCustomConfigurationService.buildQuoteConfigFieldSetModelByQuoteLine(QuoteStatus.SENT_FOR_CREDIT_CHECK, new AdditionalInformation__c[]{addl}, new SBQQ__QuoteLine__c[]{line});
        for (QuoteConfigFieldVO qcfv : QuoteCustomConfigurationService.listQuoteConfigFieldsByProductCategory('Wireless')) {
            QuoteCustomConfigurationService.isFieldValueValid(addl, qcfv, 1, 'Wireless');
            QuoteCustomConfigurationService.isFieldValueValid(addl, qcfv, 1, 'Office Phone');
            QuoteCustomConfigurationService.isFieldValueValid(addl, qcfv, 1, 'Existing Device');
        }
        for (QuoteConfigFieldVO qcfv : QuoteCustomConfigurationService.listQuoteConfigFieldsByProductCategory('Office Phone')) {
            QuoteCustomConfigurationService.isFieldValueValid(addl, qcfv, 1, 'Wireless');
            QuoteCustomConfigurationService.isFieldValueValid(addl, qcfv, 1, 'Office Phone');
            QuoteCustomConfigurationService.isFieldValueValid(addl, qcfv, 1, 'Existing Device');
        }
        for (QuoteConfigFieldVO qcfv : QuoteCustomConfigurationService.listQuoteConfigFieldsByProductCategory('Existing Device')) {
            QuoteCustomConfigurationService.isFieldValueValid(addl, qcfv, 1, 'Wireless');
            QuoteCustomConfigurationService.isFieldValueValid(addl, qcfv, 1, 'Office Phone');
            QuoteCustomConfigurationService.isFieldValueValid(addl, qcfv, 1, 'Existing Device');
        }
    }

    private static void setUp() {
        account = new Web_Account__c(Name='UnitTest', phone__c = '6049969449');
        insert account;
        
        contact = new Contact(LastName='Test',FirstName='Max',Web_Account__c=account.Id, phone = '6049969449');
        insert contact;
        
        Opportunity opp = new Opportunity(Name='Test',Web_Account__c=account.Id,CloseDate=System.today(),StageName='Prospecting');
        insert opp;
        
        quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
        quote.Rate_Band__c = 'F';
        quote.Province__c = 'BC';
        insert quote;

        line = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,SBQQ__Bundle__c=true);
        insert line;
        
        addl = new AdditionalInformation__c(Quote__c=quote.Id, Quote_Line__c=line.Id);
        insert addl;
    }

}