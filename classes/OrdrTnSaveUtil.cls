/**
* @author Santosh Rath
* 
* Utility class used for interacting with the Salesforce Object through a few APIs:
* - for registering newly reserved TNs 
* - and sourcing from Salesforce the already reserved TNs list while initializing the configurator
* Expose the following methods:
*    - saveReservedTN
*    - getAssignedTNs
*    - getUnassignedTNs
*/
public class OrdrTnSaveUtil {
   
	private static Map<String,List<TELUS_TN_RESERVATION__c>> assignedTnrMap= new Map<String,List<TELUS_TN_RESERVATION__c>>();
	private static Map<String,List<TELUS_TN_RESERVATION__c>> unAssignedTnrMap= new Map<String,List<TELUS_TN_RESERVATION__c>>();
    public static final String TOBE_UPDATED='TBU';
    public OrdrTnSaveUtil() {
    }	
    
    public static boolean saveReservedTN(String orderId,String orderItemId,OrdrTnServiceAddress serviceAddress, List<OrdrTnLineNumber> successfulyReservedTnList, OrdrTnReservationType reservationType,OrdrTnAssignedProductType assignedProductType,OrdrTnLineNumberStatus reservedStatus,String userId) {
        
        boolean result=false;
        
        // Invoke the saveReservedTN API to persist in the Salesforce Object, the TNs that have been successfully reserved in the backend
        System.debug('OrdrTnSaveUtil.saveReservedTN invoking the saveReservedTN API to insert the Reserved TN in the Salesforce Object');
       // List<OrdrTnReservationData> tnReservationDataList=generateTnReservationDataList(orderId,orderItemId,serviceAddress, successfulyReservedTnList, reservationType, assignedProductType,reservedStatus);
        // boolean result = dbApi.saveReservedTN(tnReservationDataList);
         List<RecordType> cacheRecordTypeList=[select Id from RecordType where Name = 'TnCache' and SobjectType = 'TELUS_TN_RESERVATION__c'];
		 List<RecordType> usedRecordTypeList=[select Id from RecordType where Name = 'TnUsed' and SobjectType = 'TELUS_TN_RESERVATION__c'];
		List<String> tnList=new List<String>();
		for(OrdrTnLineNumber lineNoObj:successfulyReservedTnList){
			tnList.add(lineNoObj.getTenDigitsTn());
		}
       
		String key = OrdrTnCacheUtil.getKeyForOrdrTnReservationCaching(serviceAddress, userId, reservationType);
		List<TELUS_TN_RESERVATION__c> tnObjectList=[select id,	Address__c,product_type__c,Is_TN_in_Use__c,Order_Id__c,order_Item_id__c,status__c,Telephone_Number__c,recordTypeid from TELUS_TN_RESERVATION__c where Address__c=:serviceAddress.getSalesforceAddressId() and Telephone_Number__c in :tnList];
        for(TELUS_TN_RESERVATION__c tnrObjectToUpdate:tnObjectList){
		if(String.isNotBlank(orderItemId)){
			tnrObjectToUpdate.Is_TN_in_Use__c='Y';
			tnrObjectToUpdate.order_Item_id__c=orderItemId;
			tnrObjectToUpdate.recordTypeid=usedRecordTypeList.get(0).id;
			tnrObjectToUpdate.status__c=String.valueOf(OrdrTnLineNumberStatus.RESERVED_ASSIGNED);
		} else {
			tnrObjectToUpdate.Is_TN_in_Use__c='N';
			tnrObjectToUpdate.recordTypeid=cacheRecordTypeList.get(0).id;
			tnrObjectToUpdate.status__c=String.valueOf(OrdrTnLineNumberStatus.RESERVED_UNASSIGNED);
		}
		if(String.isNotBlank(String.valueof(reservedStatus)) && (!String.valueof(reservedStatus).equals(String.valueOf(OrdrTnLineNumberStatus.RESERVED_ASSIGNED)) && !String.valueof(reservedStatus).equals(String.valueOf(OrdrTnLineNumberStatus.RESERVED_UNASSIGNED)))){
			tnrObjectToUpdate.status__c=String.valueOf(reservedStatus);
		}
		 tnrObjectToUpdate.Order_Id__c=orderId;
		 tnrObjectToUpdate.product_type__c=String.valueOf(assignedProductType);
		}
		if(tnObjectList.size()>0){
			update tnObjectList;
		}
		
        System.debug('OrdrTnSaveUtil.saveReservedTN : saveReservedTN API successful? '+result);
        return true;
    }
    
    public static boolean modifyReservedTN(String orderId,String orderItemId,OrdrTnServiceAddress serviceAddress,String oldTn, OrdrTnLineNumberStatus reservedStatus,OrdrTnReservationType reservationType,String userId) {
        
        boolean result=false;
        // Invoke the saveReservedTN API to persist in the Salesforce Object, the TNs that have been successfully reserved in the backend
        System.debug('OrdrTnSaveUtil.modifyReservedTN invoking the saveReservedTN API to insert the Reserved TN in the Salesforce Object');
        String key = OrdrTnCacheUtil.getKeyForOrdrTnReservationCaching(serviceAddress, userId, reservationType);
        // boolean result = dbApi.saveReservedTN(tnReservationDataList);
         List<RecordType> cacheRecordTypeList=[select Id from RecordType where Name = 'TnCache' and SobjectType = 'TELUS_TN_RESERVATION__c'];
		 List<RecordType> usedRecordTypeList=[select Id from RecordType where Name = 'TnUsed' and SobjectType = 'TELUS_TN_RESERVATION__c'];

        List<TELUS_TN_RESERVATION__c> tnrListToSave=[select 	Address__c,status__c,recordtypeid,order_Item_id__c from TELUS_TN_RESERVATION__c where Order_Id__c=:orderId and order_Item_id__c=:orderItemId and Telephone_Number__c=:oldTn and CacheKey__c=:key] ; 
        for (TELUS_TN_RESERVATION__c reserveData:tnrListToSave){
            reserveData.status__c=String.valueOf(reservedStatus);
            reserveData.order_Item_id__c=null;
            reserveData.recordtypeid=cacheRecordTypeList.get(0).id;            
           
        }
        update tnrListToSave;
        System.debug('OrdrTnSaveUtil.saveReservedTN : saveReservedTN API successful? '+result);
        return true;
    }
    
    public static List<OrdrTnLineNumber> getAssignedTNs(String orderId,String orderItemId, OrdrTnReservationType reservationType,OrdrTnAssignedProductType assignedProductType,OrdrTnLineNumberStatus lineNumStatus) {
        List<OrdrTnLineNumber> result = new List<OrdrTnLineNumber>();

		List<TELUS_TN_RESERVATION__c> tnrObjectList=null;
		if(assignedTnrMap.get(orderId)!=null){
			tnrObjectList=assignedTnrMap.get(orderId);
		} else {
			tnrObjectList=[select COID__c, FMS_Id__c, Is_TN_in_Use__c, Non_Native_COID__c, Order_Id__c, order_Item_id__c, Product_Type__c, Province__c, Reservation_Type__c, status__c, Switch_Number__c, Telephone_Number__c from 
                                                     TELUS_TN_RESERVATION__c where Order_Id__c=:orderId and status__c =:String.valueOf(lineNumStatus)]; 
			assignedTnrMap.put(orderId,tnrObjectList);
		}

       
        
        List<OrdrTnReservationData> assignedTnsList=new List<OrdrTnReservationData>(); 
        for (TELUS_TN_RESERVATION__c tnrObject:tnrObjectList){
          
            OrdrTnReservationData assignedTn=new OrdrTnReservationData();
            assignedTn.setExtnCoId(tnrObject.COID__c);
            assignedTn.setExtnServiceAddressId(tnrObject.FMS_Id__c);
            assignedTn.setIsInUse(tnrObject.Is_TN_in_Use__c);
            assignedTn.setNonNativeCoId(tnrObject.Non_Native_COID__c);
            assignedTn.setOrderId(tnrObject.Order_Id__c);
            assignedTn.setOrderItemId(tnrObject.order_Item_id__c);
            assignedTn.setProductType(tnrObject.Product_Type__c);
            assignedTn.setProv(tnrObject.Province__c);
            assignedTn.setReservationType(tnrObject.Reservation_Type__c);
            assignedTn.setStatus(tnrObject.status__c);
            assignedTn.setSwitchNumber(tnrObject.Switch_Number__c);
            assignedTn.setTns(tnrObject.Telephone_Number__c);
            //toSaveTnr.TN_Reservation_Key__c=reserveData.();
            assignedTnsList.add(assignedTn);
        }
        
        
        result = processTNReservationDataList(assignedTnsList,OrdrTnLineNumberStatus.RESERVED_ASSIGNED);
        //System.debug('OrdrTnSaveUtil.getAssignedTNs : getAssignedTNs API returned '+result.size()+' Assigned TNs for the following criteria: '+sa.toString());
        return result;
    }
    
    public static List<OrdrTnLineNumber> getUnassignedTNs(String orderId,String orderItemId,OrdrTnServiceAddress serviceAddress, OrdrTnReservationType reservationType,OrdrTnAssignedProductType assignedProductType,OrdrTnLineNumberStatus reservedStatus) {
        List<OrdrTnLineNumber> result = new List<OrdrTnLineNumber>();
        OrdrTnServiceAddress sa = (OrdrTnServiceAddress) serviceAddress;
        // Invoke the getUnassignedTNs API to source from the Salesforce Object, the already reserved TNs, not yet assigned
        System.debug('OrdrTnSaveUtil.getUnassignedTNs invoking the getUnassignedTNs API with the following criteria: '+sa.toString());
        //result = processTNReservationDataList(dbApi.getUnassignedTNs(generateTnReservationData(serviceAddress, null, null, null, null, false)), OrdrTnLineNumberStatus.RESERVED_UNASSIGNED);
        OrdrTnReservationData tnrInputData=generateTnReservationData(orderId,orderItemId,serviceAddress, null, null, null, null,assignedProductType,reservedStatus, false);
     	List<TELUS_TN_RESERVATION__c> tnrObjectList=null;
		if(unAssignedTnrMap.get(orderId)!=null){
			tnrObjectList=unAssignedTnrMap.get(orderId);
		} else {
			tnrObjectList=tnrObjectList=[select COID__c, FMS_Id__c, Is_TN_in_Use__c, Non_Native_COID__c, Order_Id__c, order_Item_id__c, Product_Type__c, Province__c, Reservation_Type__c, status__c, Switch_Number__c, Telephone_Number__c from 
                                                     TELUS_TN_RESERVATION__c where Order_Id__c=:tnrInputData.getOrderId() and address__c=:serviceAddress.getSalesforceAddressId() and status__c =:String.valueOf(reservedStatus)]; 
        	unAssignedTnrMap.put(orderId,tnrObjectList);
		}

        List<OrdrTnReservationData> assignedTnsList=new List<OrdrTnReservationData>(); 
        for (TELUS_TN_RESERVATION__c tnrObject:tnrObjectList){
            if(String.isNotBlank(orderItemId)){
                if(!(orderItemId.equals(tnrObject.order_Item_id__c))){
                    continue;
                }
            }
			if(reservationType!=null && String.isNotBlank(String.valueOf(reservationType))){
                if(!(String.valueOf(reservationType).equals(tnrObject.Reservation_Type__c))){
                    continue;
                }
            }
		
            OrdrTnReservationData assignedTn=new OrdrTnReservationData();
            assignedTn.setExtnCoId(tnrObject.COID__c);
            assignedTn.setExtnServiceAddressId(tnrObject.FMS_Id__c);
            assignedTn.setIsInUse(tnrObject.Is_TN_in_Use__c);
            assignedTn.setNonNativeCoId(tnrObject.Non_Native_COID__c);
            assignedTn.setOrderId(tnrObject.Order_Id__c);
            assignedTn.setOrderItemId(tnrObject.order_Item_id__c);
            assignedTn.setProductType(tnrObject.Product_Type__c);
            assignedTn.setProv(tnrObject.Province__c);
            assignedTn.setReservationType(tnrObject.Reservation_Type__c);
            assignedTn.setStatus(tnrObject.status__c);
            assignedTn.setSwitchNumber(tnrObject.Switch_Number__c);
            assignedTn.setTns(tnrObject.Telephone_Number__c);
            //toSaveTnr.TN_Reservation_Key__c=reserveData.();
            assignedTnsList.add(assignedTn);
        }
        result = processTNReservationDataList(assignedTnsList,OrdrTnLineNumberStatus.RESERVED_UNASSIGNED);
        System.debug('OrdrTnSaveUtil.getUnassignedTNs : getUnassignedTNs API returned '+result.size()+' Unassigned TNs for the following criteria: '+sa.toString());
        return result;
    }
    
    
 @TestVisible  private static List<OrdrTnLineNumber> processTNReservationDataList(List<OrdrTnReservationData> returnedTns, OrdrTnLineNumberStatus status) {
        System.debug('OrdrTnSaveUtil.processTNReservationDataList - starts');		
        List<OrdrTnLineNumber> result = new List<OrdrTnLineNumber>();
        Map<String, OrdrTnLineNumber> hm = new Map<String, OrdrTnLineNumber>();
        if (returnedTns != null) {
			for(OrdrTnReservationData returnedTnsDataObj:returnedTns){
				 String servingCoid = returnedTnsDataObj.getNonNativeCoId();
                    String switchNumber = returnedTnsDataObj.getSwitchNumber();
                    boolean nativeNpaNxxInd = false;
                    if ((servingCoid == null) || (String.isBlank(servingCoid)) || (servingCoid.trim().equalsIgnoreCase('null'))) {
                        servingCoid = returnedTnsDataObj.getExtnCoId();
                        nativeNpaNxxInd = true;
                    }
                    if ((switchNumber == null) || (String.isBlank(switchNumber)) || (switchNumber.trim().equalsIgnoreCase('null'))) {
                        switchNumber = null;
                    }
                    if ((returnedTnsDataObj.getTns() != null) && (returnedTnsDataObj.getTns().length() == 10)) {
                        if (!hm.containsKey(returnedTnsDataObj.getTns())) {
							OrdrTnReservationType ordTnResType=convertOrdrTnReservationTypeStringToEnum (returnedTnsDataObj.getReservationType());
							OrdrTnAssignedProductType ordrTnAssignProdType=convertOrdrTnAssignedProductTypeStringToEnum (returnedTnsDataObj.getProductType());
							OrdrTnLineNumber ordLineNumObj=new OrdrTnLineNumber(returnedTnsDataObj.getTns(),servingCoid, switchNumber, nativeNpaNxxInd, ordTnResType, status, ordrTnAssignProdType);
                            hm.put(returnedTnsDataObj.getTns(), ordLineNumObj);
                        }
                    }
			}
		}
		result=hm.values();
		
       
        System.debug('OrdrTnSaveUtil.processTNReservationDataList - ends');		
        return result;
    }
    
    /**
* Instantiate a list of structure TNReservationData to pass as an input to:
* - the TNRservationUtil.saveReservedTN for persisting the newly Reserved TNs in the Salesforce Object
* @return List<TNReservationData>
*/
    
  @TestVisible  private static List<OrdrTnReservationData> generateTnReservationDataList(String orderId,String orderItemId,OrdrTnServiceAddress serviceAddress, List<OrdrTnLineNumber> successfulyReservedTnList, OrdrTnReservationType reservationType,	OrdrTnAssignedProductType assignedProductType,OrdrTnLineNumberStatus reservedStatus) {
        List<OrdrTnReservationData> dataList = new List<OrdrTnReservationData>();		
        if (successfulyReservedTnList != null) {
            for (integer i=0; i<successfulyReservedTnList.size(); i++) {
                if (successfulyReservedTnList.get(i) != null) {
                    dataList.add(generateTnReservationData(orderId,orderItemId,serviceAddress, 
                                                           successfulyReservedTnList.get(i).getServingCoid(), 
                                                           successfulyReservedTnList.get(i).getSwitchNumber(), 
                                                           successfulyReservedTnList.get(i).getTenDigitsTn(), 
                                                           reservationType, assignedProductType,reservedStatus,true));
                }
            }
        }
        return dataList;
    }
    
    /**
* Instantiate a structure TNReservationData to pass as an input to:
* - the TNRservationUtil.saveReservedTN for persisting the newly Reserved TNs in the Salesforce Object
* - the TNRservationUtil.getAssignedTNs & getUnassignedTNs for querying the Reserved TNs list from the Salesforce Object
* @return TNReservationData
*/
    
 @TestVisible   private static OrdrTnReservationData generateTnReservationData(String orderId,String orderItemId,OrdrTnServiceAddress serviceAddress, String servingCoid,String switchNumber, String tn, OrdrTnReservationType reservationType,OrdrTnAssignedProductType assignedProductType,OrdrTnLineNumberStatus reservedStatus,Boolean isForSaving	) {
        System.debug('OrdrTnSaveUtil.generateTnReservationData - starts');		
        String nonNativeCoId = null;
        if (String.isNotBlank(servingCoid)) {
            if (!servingCoid.trim().equalsIgnoreCase(serviceAddress.getCOID())) {
                nonNativeCoId = servingCoid;
            }
        }
        
        OrdrTnReservationData tnReservationData = new OrdrTnReservationData();
        //tnReservationData.setOpportunityId(OrdrTnReservedPoolUtil.getCurrentCpqOpportunityKey());
        tnReservationData.setOrderId(orderId);
        //tnReservationData.setCustomerId(OrdrTnReservedPoolUtil.getCurrentCustomerId());
        tnReservationData.setExtnServiceAddressId(serviceAddress.getAddressId()); // Salesforce Service Address Identifier (not the FMS ID) is expected here
        tnReservationData.setExtnFmsServiceAddressId(serviceAddress.getAddressId()); // FMS Service Address Identifier is expected here
        tnReservationData.setSwitchNumber(switchNumber);
        tnReservationData.setExtnCoId(serviceAddress.getCOID());
        /*if (isForSaving || String.isBlank(orderItemId)) {
            tnReservationData.setOrderItemId(TOBE_UPDATED);
        } else {
            tnReservationData.setOrderItemId(orderItemId);
        }*/
		tnReservationData.setOrderItemId(orderItemId);
        tnReservationData.setProv(serviceAddress.getProvinceStateCode());
        tnReservationData.setIsPrimaryQuote(null);
        tnReservationData.setProductType(String.valueOf(assignedProductType));
        tnReservationData.setNonNativeCoId(nonNativeCoId);
        tnReservationData.setTns(tn);
        if (reservationType != null) {
            tnReservationData.setReservationType(String.valueOf(reservationType));
        }
        else {
            tnReservationData.setReservationType(null);
        }
        if(reservedStatus!=null){
            tnReservationData.setStatus(String.valueOf(reservedStatus));
        } else {
            tnReservationData.setStatus(null);
        }
        
        
        //System.debug('OrdrTnSaveUtil.generateTnReservationData - ends : tnReservationData ('+String.valueOf(reservationType)+') '+tn+' for Saving '+isForSaving+' for OrderHeaderKey '+tnReservationData.getOrderId()+' - OpportunityKey '+tnReservationData.getOpportunityKey());		
        return tnReservationData;
        
    }
    
    public static  OrdrTnAssignedProductType convertOrdrTnAssignedProductTypeStringToEnum (String productType) {
        OrdrTnAssignedProductType assignedProductType = null;
        if (String.isNotBlank(productType)) {
            try {
                
                if (productType==String.valueOf(OrdrTnAssignedProductType.ADSL))
                    assignedProductType =  OrdrTnAssignedProductType.ADSL;
                else if (productType==String.valueOf(OrdrTnAssignedProductType.ML))
                    assignedProductType =  OrdrTnAssignedProductType.ML;
                else if (productType==String.valueOf(OrdrTnAssignedProductType.OL))
                    assignedProductType =  OrdrTnAssignedProductType.OL;
                else if (productType==String.valueOf(OrdrTnAssignedProductType.SL))
                    assignedProductType =  OrdrTnAssignedProductType.SL;
                else if (productType==String.valueOf(OrdrTnAssignedProductType.TF))
                    assignedProductType =  OrdrTnAssignedProductType.TF;
                else if (productType==String.valueOf(OrdrTnAssignedProductType.WLS))
                    assignedProductType =  OrdrTnAssignedProductType.WLS;
            }
            catch (Exception e) {
               System.debug(e);
                System.debug(e.getMessage());
                System.debug(e.getStackTraceString());
            }
        }
        return assignedProductType;
    }
    
    public static OrdrTnReservationType convertOrdrTnReservationTypeStringToEnum (String reservationType) {
        OrdrTnReservationType numberReservationType = null;
        if (String.isNotBlank(reservationType)) {
            try {
                //numberReservationType = OrdrTnReservationType.valueOf(reservationType);
                if (reservationType==String.valueOf(OrdrTnReservationType.TOLLFREE))
                    numberReservationType = OrdrTnReservationType.TOLLFREE;
                else if (reservationType==String.valueOf(OrdrTnReservationType.WLN))
                    numberReservationType = OrdrTnReservationType.WLN;
                else if (reservationType==String.valueOf(OrdrTnReservationType.WLS))
                    numberReservationType = OrdrTnReservationType.WLS;
            }
            catch (Exception e) {
                System.debug(e);
                System.debug(e.getMessage());
                System.debug(e.getStackTraceString());
            }
        }
        return numberReservationType;
    }
    
 
}