/**
 * Created by dpeterson on 1/18/2018.
 * This class can be used as a utility class to query data in a non-repetitive way. If a query needs to be called from multiple helper classes in the same context then only 1 query will be requested.
 */

public with sharing class CacheQuerySelector {

    private static Set<String> CLOSEDCASESSTATUSES;

    /**
     * Get the closed statuses (because at the point of this trigger Case.IsClosed won't be set yet
     * @author  Dane Peterson, Traction on Demand
     * @date  2018-01-18
     * @return A set of strings that represent closed statuses
     */

    public static Set<String> getClosedCaseStatuses() {
        if(CLOSEDCASESSTATUSES == null) {
            CLOSEDCASESSTATUSES = new Set<String>();

            for (CaseStatus status : [Select MasterLabel From CaseStatus where IsClosed = true]) {
                CLOSEDCASESSTATUSES.add(status.MasterLabel);
            }
        }

        return CLOSEDCASESSTATUSES;
    }

}