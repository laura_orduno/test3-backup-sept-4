public without sharing class QuoteProvisionRequestController {
    public class QPRException extends Exception {}
    public QuoteVO quote {get; private set;}
    public Web_Account__c account {get; private set;}
    public List<QuoteLineVO> products {get; private set;}
    public String qid {get; set {this.qid = value; init();}}
    
    public QuoteProvisionRequestController() {
        init();
    }
    
    public void init() {
        Id quoteId = null;
        
        if (ApexPages.currentPage() != null && ApexPages.currentPage().getParameters() != null && ApexPages.currentPage().getParameters().get('qid') != null) {
            quoteId = ApexPages.currentPage().getParameters().get('qid');
        }
        if (quoteId == null && qid != null) {
            Quote_BSO__c qbso = [Select Name From Quote_BSO__c Where Id = :qid Limit 1];
            if (qbso != null) {
                quoteId = (Id) qbso.Name;
            }
        }
        if (quoteId == null) {
            return;
            //throw new QPRException('Quote Id is null :/');
        }
        quote = new QuoteVO(QuoteUtilities.loadQuote(quoteId));
        products = new List<QuoteLineVO>();
        for (SBQQ__QuoteLine__c line : QuotePortalUtils.loadQuoteLines(quoteId, true)) {
            products.add(new QuoteLineVO(line));
        }
        products = QuotePortalUtils.sortLines(products);
        
        Id accountId = quote.record.SBQQ__Opportunity__r.Web_Account__c;
        account = QuotePortalUtils.loadAccountById(accountId);
    }
    
}