public class OrdrTnResourceInfoRetrievalConfiguration {
	public class ResourceConfigurationCollectionMessage {
		public TpCommonMtosiV3.Header_T header;
		public TpInventoryResourceConfigV3.LogicalResource[] logicalResource;
		public TpInventoryResourceConfigV3.PhysicalResource[] physicalResource;
		public TpInventoryResourceConfigV3.Resource[] logicalResourceOrPhysicalResource;


		private String[] header_type_info = new String[]{'header','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/ResourceConfigurationMessage','Header_T','0','1','false'};
		private String[] logicalResource_type_info = new String[]{'logicalResource','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/ResourceConfigurationMessage','LogicalResource','0','1','false'};
		private String[] physicalResource_type_info = new String[]{'physicalResource','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/ResourceConfigurationMessage','PhycalResource','0','1','false'};
		private String[] logicalResourceOrPhysicalResource_type_info = new String[]{'logicalResourceOrPhysicalResource','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/ResourceConfigurationMessage','Resource','0','1','false'};

		private String[] apex_schema_type_info = new String[]{'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/ResourceConfigurationMessage','false','false'};
		private String[] field_order_type_info = new String[]{'header','logicalResource','physicalResource','logicalResourceOrPhysicalResource'};
	}
	public class ResourceConfigurationMessage {
		public TpCommonMtosiV3.Header_T header;
		private String[] header_type_info = new String[]{'header','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/ResourceConfigurationMessage','Header_T','0','1','false'};
		private String[] apex_schema_type_info = new String[]{'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/ResourceConfigurationMessage','false','false'};
		private String[] field_order_type_info = new String[]{'header'};
	}
	public class SiteCollectionMessage {
		public TpCommonMtosiV3.Header_T header;
		private String[] header_type_info = new String[]{'header','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/ResourceConfigurationMessage','Header_T','0','1','false'};
		public TpCommonPlaceV3.Site[] site;
		private String[] site_type_info = new String[]{'site','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/ResourceConfigurationMessage','Site','0','-1','false'};
		private String[] apex_schema_type_info = new String[]{'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/ResourceConfigurationMessage','false','false'};
		private String[] field_order_type_info = new String[]{'header','site'};
	}
	public class SiteMessage {
		public TpCommonMtosiV3.Header_T header;
		private String[] header_type_info = new String[]{'header','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/ResourceConfigurationMessage','Header_T','0','1','false'};
		public TpCommonPlaceV3.Site site;
		private String[] site_type_info = new String[]{'site','http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/ResourceConfigurationMessage','Site','1','1','false'};
		private String[] apex_schema_type_info = new String[]{'http://www.ibm.com/xmlns/prod/websphere/fabric/2009/12/telecom/operations/inventory/schema/ResourceConfigurationMessage','false','false'};
		private String[] field_order_type_info = new String[]{'header','site'};
	}
}