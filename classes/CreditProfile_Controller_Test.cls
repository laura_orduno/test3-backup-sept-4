@isTest
private class CreditProfile_Controller_Test {
    
        
    @isTest
    public static void cpId() {  
        
        Test.startTest();  
        
        Test.setCurrentPageReference(new PageReference('dyy test page'));  
        
        Account acc = CreditUnitTestHelper.getAccount();  
        Credit_Profile__c cp = CreditUnitTestHelper.getCreditProfile();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(acc);            
        CreditProfile_Controller controller = new CreditProfile_Controller();         
        
        
        CreditUnitTestHelper.createTestuser();         
        User testUser = CreditUnitTestHelper.getTestuser();
        System.debug('... dyy ... testuser = ' + testUser.Name);
        
        
        
        System.runAs(testUser){              
            controller.cpId = cp.id;         
            controller.queryString = 'select'; 
            controller.cp = cp;
            cp = controller.cp;
        }
        
        Test.StopTest();    
    }
    

    /*
    private static testMethod void testCreditProfile () {
        Test.setCurrentPage(Page.CreditProfile);
        CreditProfile_Controller CP = new CreditProfile_Controller();
        System.debug('Insert New Account');
        Account acc= new Account(
            Name = 'RCID Test',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            RCID__c = '987654'
        );

        insert acc; 
        //insert new Credit_ID_Mapping__c(Name='Credit Product', Field_ID__c='00Ne0000001JO6F');
		Credit_Profile__c creditProfile=new Credit_Profile__c(name='CP-RCID',
                                                           account__c=acc.id
        );
        insert creditProfile;
        acc.Credit_Profile__c=creditProfile.id;
        update creditProfile;
        
        acc = [select Id, Name, credit_profile__c from account where id=:acc.id];
        
        CP.cpID = acc.Credit_Profile__c;
        System.assertEquals(creditProfile.name, CP.cp.name);
    }*/
    
    
}