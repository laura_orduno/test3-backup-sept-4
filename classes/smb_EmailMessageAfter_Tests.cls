@isTest
private class smb_EmailMessageAfter_Tests {

	@isTest(SeeAllData=true)
    static void testInsertion() {
        
        RecordType rt = [SELECT Id 
        				 FROM RecordType 
        				 WHERE DeveloperName = 'SMB_Care_Email_to_Case'
        							AND
        					   SobjectType = 'Case'];
        
		Case caseRecord = new Case (
			RecordTypeId = rt.Id,
			Subject = 'Test Case',
			SuppliedEmail = 'dchoate@astadia.com',
			SuppliedName = 'Derek Choate',
			Description = 'Test Description'
		);
		
		insert caseRecord;
		
		caseRecord.Status = 'Waiting on Customer';
		
		update caseRecord;
		
		EmailMessage message = new EmailMessage (
			ParentId = caseRecord.Id,
			FromAddress = caseRecord.SuppliedEmail,
			Incoming = true,
			TextBody = 'Test'
		);
		
		insert message;
		
		Case updatedCase = [SELECT Id, Status FROM Case WHERE Id = :caseRecord.Id];
		
		System.assertEquals(updatedCase.Status, 'Customer Response Received');

    }
}