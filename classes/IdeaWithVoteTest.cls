@isTest
private class IdeaWithVoteTest {

    private static testMethod void testIdeaWithVote() {
    	final Id COMMUNITY_ID = [SELECT Id FROM Community WHERE IsActive = true LIMIT 1].id;
    	final Id PROFILE_ID = [SELECT Id FROM Profile WHERE name = 'Standard User' LIMIT 1].id;
    	
    	User u = new User(
    		username = 'VMDFJREFSRI@TRACTIONSM.COM',
      	email = 'test@example.com',
	      title = 'test',
	      lastname = 'test',
	      alias = 'test',
	      TimezoneSIDKey = 'America/Los_Angeles',
	      LocaleSIDKey = 'en_US',
	      EmailEncodingKey = 'UTF-8',
	      ProfileId = PROFILE_ID,
	      LanguageLocaleKey = 'en_US'
	    );
      insert u;
    	
    	Idea i = new Idea(title = 'test', CommunityId = COMMUNITY_ID);
    	
    	system.runas(u) {
    		insert i;
    	}
    	
    	Vote v = new Vote(ParentId = i.id, Type = 'Up');
    	insert v;
    	
    	// Test CTOR
    	
    	IdeaWithVote iwv = new IdeaWithVote(i, v, true);
    	system.assertEquals(i, iwv.idea);
    	system.assertEquals(v, iwv.vote);
    	system.assert(iwv.read);
    	
    	
    	// Test Static Methods
    	
    	List<IdeaWithVote> resultsByIdea = IdeaWithVote.getIdeasWithVotes(new Idea[] {i});
    	system.assertEquals(1, resultsByIdea.size());
    	system.assertEquals('Up',resultsByIdea[0].getHowUserVoted());
    	
    	List<IdeaWithVote> resultsByVote = IdeaWithVote.getIdeasWithVotes(new Vote[] {v});
    	system.assertEquals(1, resultsByVote.size());
    	system.assertEquals('Up',resultsByVote[0].getHowUserVoted());
    	
    }
}