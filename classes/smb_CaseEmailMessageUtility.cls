global class smb_CaseEmailMessageUtility {

	global static void updateCaseStatusOnIncomingEmails(Map<Id, List<EmailMessage>> incomingCaseEmailsMappedToCaseId) {
		
		List<Case> cases = [SELECT Id, RecordType.DeveloperName, Status, Waiting_on_TELUS_Internal_Description__c
							FROM Case
							WHERE Id IN :incomingCaseEmailsMappedToCaseId.keySet()
								AND RecordTypeId != null];
		
		List<Case> casesToUpdate = new List<Case>();
						
		for (Case caseRecord : cases) {
			
			if (caseRecord.RecordType.DeveloperName.startsWith('SMB_Care') &&
				caseRecord.Status == 'Waiting on Customer') {
				caseRecord.Status = 'Customer Response Received';
				//caseRecord.Waiting_on_TELUS_Internal_Description__c = 'Customer responded to case via email. Waiting for a response from TELUS.';
				casesToUpdate.add(caseRecord);
			}
			
		}
		
		if (casesToUpdate.size() > 0) {
			Database.SaveResult[] results = Database.update(casesToUpdate, false);
			
			List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
			
			for (Database.Saveresult result : results) {
				if (!result.isSuccess()) {
					Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
					mail.setToAddresses(new string[] {'dchoate@astadia.com'});
					mail.setReplyTo('dchoate@astadia.com');
					mail.setSenderDisplayName('Email Message Trigger');
					mail.setSubject('Error occured while updating the following case : ' + result.getId());
					
					List<string> errors = new List<string>();
					for (Database.Error err : result.getErrors()) {
						errors.add(err.getMessage());
					}
					mail.setPlainTextBody(string.join(errors, '\n'));
					
					mails.add(mail);
				}
			}
			
			if (mails.size() > 0) {
				Messaging.sendEmail(mails);
			}
		}
		
	}

}