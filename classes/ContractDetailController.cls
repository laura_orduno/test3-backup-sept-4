/******************************************************************************
# File..................: ContractDetailController
# Version...............: 1
# Created by............: Sandip Chaudhari (IBM)
# Description...........: Class is to Create, Validate, Save Billing Address for Service Request. 
                          This class is main controller for detail page. Created as a part of PAC R2
******************************************************************************/

Public class ContractDetailController{
    public AddressData PAData  { get; set; }
    contract saObj;
    public String isError{ get; set; }
    
    public ContractDetailController(ApexPages.StandardController controller){
        isError = 'onLoad';
        PAData = new AddressData();
        PAData.isAddressLine1Required = true;
        PAData.isProvinceRequired = true;
        PAData.isCityRequired = true;
        saObj = (contract)controller.getRecord();
        saObj = [SELECT Id, billingaddress, billingstreet,billingcity,billingstate,billingcountry,billingpostalcode,Billing_Address_Status__c
                        FROM contract
                        WHERE Id=: saObj.Id];
        if(saObj != null){
            PAData.searchString = saObj.billingaddress.getstreet()+''+saObj.billingaddress.getcity()+''+saObj.billingaddress.getstate()+''+saObj.billingaddress.getcountry()+''+saObj.billingaddress.getpostalcode();
            if(saObj.Billing_Address_Status__c == System.Label.PACInValidCaptureNewAddrStatus){
                PAData.isManualCapture = true;
            }else{
                PAData.isManualCapture = false;
            }
            
            //Split the address devided by comma and assigned to respective vaiables for AddressData class
            if(saObj.billingstreet != null){
                integer i=0;
                string[] addrPart = saObj.billingstreet.split(',');
                if(addrPart != null && addrPart.size() > 0){
                    if(addrPart.size() > i){
                        PAData.addressLine1 = addrPart[i];
                       // PAData.onLoadSearchAddress = addrPart[i];
                        i++;
                    }
                    if(addrPart.size() > i){
                        if(addrPart[i] != null){
                            PAData.city  = addrPart[i].trim();
                            i++;
                        }
                    }
                    if(addrPart.size() > i){
                        if(addrPart[i] != null){
                            PAData.canadaProvince = addrPart[i].trim();
                            PAData.usState = addrPart[i].trim();
                            PAData.province = addrPart[i].trim();
                        }
                        i++;
                    }
                    if(addrPart.size() > i){
                        if(addrPart[i] != null){
                            PAData.country = addrPart[i].trim();
                        }
                        i++;
                    }
                    
                    if(addrPart.size() > i){
                        if(addrPart[i] != null){
                            PAData.postalCode = addrPart[i].trim();
                        }
                    }
                }
            }
        }else{
            PAData.country='Canada';
        }
    }
    
     /**
    * Name - mapAddress
    * Description - Update Billing address in existing Billing_Address__c field
    * Param - 
    * Return type - void
    **/
    
    public pageReference updateBillingAddress (){
        mapAddress();
        try{
            isError = 'inSave';
            update saObj;
        }catch(Exception ex){
            isError = 'InError';
            ApexPages.addMessages(ex) ;
        }
        //PageReference redirectPage = new PageReference('/apex/ServiceRequestDetail?id='+ saObj.Id +'&sfdc.override=1');
        //redirectPage.setRedirect(true);
        //return redirectPage;
        return null;
    }
    
    /**
    * Name - mapAddress
    * Description - Map Billing address with existing Billing_Address__c field
    * Param - 
    * Return type - void
    **/
    public void mapAddress(){
        system.debug('#### City' + PAData.city);
        system.debug('#### postalCode' + PAData.postalCode);
        //String address = '';
        if(PAData.addressLine1 != null)
            saObj.billingstreet  = PAData.addressLine1.trim();
        saObj.billingcity=PAData.city;
        saObj.billingstate=PAData.province;
        saObj.billingcountry=PAData.country;
        saObj.billingpostalcode=PAData.postalCode;
        //saObj.billingaddress = address;
        saObj.Billing_Address_Status__c = PAData.captureNewAddrStatus;
    }
}