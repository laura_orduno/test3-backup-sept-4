/**
 * trac_Detailed_Account.cls - Data Access class for Customer Central Visibility Project
 * @description -   Class used to get and set all information for the current account being viewed
 *                  by portal users through DealerAccountView.page
 *
 * @author Ryan Draper, Traction on Demand; Merisha Shim, Traction on Demand
 * @date 2013-08-12, 2014-02-04
 */
public without sharing class trac_Detailed_Account {
    public Account  record {get;set;}///???
    public String   accountImg {get{return record.RT__c;}}
    public String   id {get{return record.Id;}}
    public String   name {get{return record.Name;}set{
                        if(value == null || value == ''){
                            record.Name = record.Name;
                            return;
                        }else{
                            record.Name = value;}
                        }   
                    }
    public String   accountStatus {get{return record.Account_Status__c;}set{record.Account_Status__c = value;}}
    public String   accountClassification{get{return record.Account_Classification__c;}}
    public Id       ownerId{get{return record.Ownerid;}}
    public Id       ownerAccountId {get{return record.owner.AccountId;}}
    public String   ownerName{get{return record.Owner.Name;}}
    public String   ownerEmail{get{return record.Owner.Email;}}
    public Id       ownerUserRoleId{get{return record.Owner.UserRoleId;}}
    public Id       recordTypeId{get;set;}
    public String   recordTypeName{get{return record.RecordType.Name;}}
    public String   ban{get{return record.BAN__c;}set{record.BAN__c = value;}}
    public Boolean  busExBonusEligible{get{return record.BusEx_Bonus_Eligible__c;}set{record.BusEx_Bonus_Eligible__c = value;}}
    public String   cbucidFormula{get{return record.CBUCID_formula__c;}}
    public String   cbucidDUNS{get{return record.CBUCID_DUNS__c;}}
    public String   cbuCd{get{return record.CBU_CD__c;}}
    public String   cbuName{get{return record.CBU_Name__c;}}
    
    public Decimal  companysTotalAnnualSalesPerDB{get{return record.Company_s_Total_Annual_Sales_Per_D_B__c;}}
    public Id       createdById{get{return record.CreatedById;}}
    public String   createdByName{get{return record.CreatedBy.Name;}} 
    public String   custCentralDUNS{get{return record.CustCentral_DUNS__c;}set{record.CustCentral_DUNS__c = value;}}
    public decimal  dBOfLocations{get{return record.D_B_of_Locations__c;}}
    public Boolean  dealerAccount{get{return record.Dealer_Account__c;}set{record.Dealer_Account__c = value;}}
    public Boolean  dntc{get{return record.DNTC__c;}}
    public String   duns{get{return record.DUNS__c;}}
    public String   fax{get{return record.Fax;}set {record.Fax = value;}}
    public Boolean  inactive{get{return record.Inactive__c;}set{record.Inactive__c = value;}}
    public Date     clientsInformationVerifiedOn{get{return record.Client_s_information_verified_on__c;}set{record.Client_s_information_verified_on__c = value;}}
    public Id       informationVerifiedBy{get{return record.Information_verified_by__c;}set{record.Information_verified_by__c = value;}}
    public String   informationVerifiedByName{get{return record.Information_verified_by__r.Name;} }
    
    public String   languagePreference{get{return record.Language_Preference__c;}set{record.Language_Preference__c = value;}}
    public Id       lastModifiedById{get{return record.LastModifiedById;}}
    public String   lastModifiedByName{get{return record.LastModifiedBy.Name;}}
    public String   legalBusinessName{get{return record.Legal_Business_Name__c;}set{record.Legal_Business_Name__c = value;}}
    public Id       subSegmentCode{get{return record.Sub_Segment_Code__c;}} 
    public String   marketingSubSegmentDescription{get{return record.Marketing_Sub_Segment_Description__c;}}
    public decimal  mikeBlackberry{get{return record.MIKE_BLACKBERRY__c;}}
    public decimal  mikeBlackberryCbucid{get{return record.MIKE_BLACKBERRY_CBUCID__c;}}
    public decimal  mikeOther{get{return record.MIKE_OTHER__c;}}
    public decimal  mikePda{get{return record.MIKE_PDA__c;}}
    public decimal  mikeVoiceOnly{get{return record.MIKE_VOICE_ONLY__c;}}
    
    public String   naics2Description{get{return record.NAICS_2_Description__c;}}
    public String   naics4Description{get{return record.NAICS_4_Description__c;}}
    public String   naics6Description{get{return record.NAICS_6_Description__c;}}
    public decimal  nbrOfEmployeesDB{get{return record.Nbr_of_Employees_D_B__c;}}
    public decimal  numberOfEmployeesTotal{get{return record.Number_of_Employees_Total__c;}}
    public decimal  numberOfEmployeesTotalCbucid{get{return record.Number_of_Employees_Total_CBUCID__c;}}
    public Id       parentId{get{return record.ParentId;}set{record.ParentId = value;}}
    public String   parentName{get{return record.Parent.Name;}set{record.Parent.Name = value;}}
    public decimal  pcsBlackberry{get{return record.PCS_BLACKBERRY__c;}}
    public decimal  pcsIphone{get{return record.PCS_iPHONE__c;}}
    public decimal  pcsMikeAircard{get{return record.PCS_MIKE_AIRCARD__c;}}
    public decimal  pcsOther{get{return record.PCS_OTHER__c;}}
    public decimal  pcsPda{get{return record.PCS_PDA__c;}}
    public decimal  pcsVoiceOnly{get{return record.PCS_VOICE_ONLY__c;}}
    public String   phone{get{return record.Phone;}set{record.Phone = value;}}
    public String   rcid{get{return record.RCID__c;}}
    public String   rcidName{get{return record.RCID_Name__c;}}
    
    public String   teamTelusId{get{return record.Team_TELUS_ID__c;}set{record.Team_TELUS_ID__c = value;}}
    public Decimal  totalTelusBilledRevenue2011{get{return record.Total_TELUS_Billed_Revenue_2011__c;}}
    public Decimal  totalTelusBilledRevenueYtd{get{return record.Total_TELUS_Billed_Revenue_YTD__c;}}
    public Decimal  totalTelusWirelessRevenue2011{get{return record.Total_TELUS_Wireless_Revenue_2011__c;}}
    public Decimal  totalTelusWirelessRevenueYtd{get{return record.Total_TELUS_Wireless_Revenue_YTD__c;}}
    public Decimal  totalTelusWirelineRevenue2011{get{return record.Total_TELUS_Wireline_Revenue_2011__c;}}
    public Decimal  totalTelusWirelineRevenueYtd{get{return record.Total_TELUS_Wireline_Revenue_YTD__c;}}
    public String   website{get{return record.Website;}set{record.Website = value;}}
    public String   accountClassificationCbucid{get{return record.Account_Classification_CBUCID__c;}}
    public String   cbuNameCbucid{get{return record.CBU_Name_CBUCID__c;}}
    public Decimal  companysTotalAnnualSalesCbucid{get{return record.Company_s_Total_Annual_Sales_CBUCID__c;}}
    public decimal  dbOfLocationsCbucid{get{return record.D_B_of_Locations_CBUCID__c;}}
    
    public String   l2rSurveySentMonth{get{return record.L2R_Survey_Sent_Month__c;}set{record.L2R_Survey_Sent_Month__c = value;}}
    public decimal  mikeOtherCbucid{get{return record.MIKE_OTHER_CBUCID__c;}}
    public decimal  mikePdaCbucid{get{return record.MIKE_PDA_CBUCID__c;}}
    public decimal  mikeVoiceOnlyCbucid{get{return record.MIKE_VOICE_ONLY_CBUCID__c;}}
    
    public String   naics2DescriptionCbucid{get{return record.NAICS_2_Description_CBUCID__c;}}
    public String   naics4DescriptionCbucid{get{return record.NAICS_4_Description_CBUCID__c;}}
    public String   naics6DescriptionCbucid{get{return record.NAICS_6_Description_CBUCID__c;}}
    public Id       naics6Code{get{return record.NAICS_6_Code__c;}}
    public decimal  pcsBlackberryCbucid{get{return record.PCS_BLACKBERRY_CBUCID__c;}}
    public decimal  pcsIphoneCbucid{get{return record.PCS_iPHONE_CBUCID__c;}}
    public decimal  pcsMikeAircardCbucid{get{return record.PCS_MIKE_AIRCARD_CBUCID__c;}}
    public decimal  pcsOtherCbucid{get{return record.PCS_OTHER_CBUCID__c;}}
    public decimal  pcsPdaCbucid{get{return record.PCS_PDA_CBUCID__c;}}
    public decimal  pcsVoiceOnlyCbucid{get{return record.PCS_VOICE_ONLY_CBUCID__c;}}
    
    public String   stratVerticalDescriptionCbucid{get{return record.Strat_Vertical_Description_CBUCID__c;}}
    public String   subSegmentDescriptionCbucid{get{return record.Sub_Segment_Description_CBUCID__c;}}
    public Decimal  totalTelusBilledRevenue2011Cbucid{get{return record.Total_TELUS_Billed_Revenue_2011_CBUCID__c;}}
    public Decimal  totalTelusBilledRevenueYtdCbucid{get{return record.Total_TELUS_Billed_Revenue_YTD_CBUCID__c;}}
    public Decimal  totalTelusWirelineRevenueYtdCbucid{get{return record.Total_TELUS_Wireline_Revenue_YTD_CBUCID__c;}} 
    public Decimal  totalTelusWirelessRevenue2011Cbucid{get{return record.Total_TELUS_Wireless_Revenue_2011_CBUCID__c;}}
    public Decimal  totalTelusWirelessRevenueYtdCbucid{get{return record.Total_TELUS_Wireless_Revenue_YTD_CBUCID__c;}}
    public Decimal  totalTelusWirelineRevenue2011Cbucid{get{return record.Total_TELUS_Wireline_Revenue_2011_CBUCID__c;}}
    public String   shippingCity{get{return record.ShippingCity;}set{record.ShippingCity = value;}}
    public String   shippingCountry{get{return record.ShippingCountry;}set{record.ShippingCountry = value;}}
    public String   shippingPostalCode{get{return record.ShippingPostalCode;}set{record.ShippingPostalCode = value;}}
    public String   shippingState{get{return record.ShippingState;}set{record.ShippingState = value;}}
    public String   shippingStreet{get{return record.ShippingStreet;}set{record.ShippingStreet = value;}}
    public String   billingCountry{get{return record.BillingCountry;}set{record.BillingCountry = value;}}
    public String   billingCity{get{return record.BillingCity;}set{record.BillingCity = value;}}
    public String   billingPostalCode{get{return record.BillingPostalCode;}set{record.BillingPostalCode = value;}}
    public String   billingState{get{return record.BillingState;}set{record.BillingState = value;}}
    public String   billingStreet{get{return record.BillingStreet;}set{record.BillingStreet = value;}}
    public String   comments{get{return record.COMMENTS__c;}set{record.COMMENTS__c = value;}}
    public String   telusBusinessUnit{get{return record.Telus_Business_Unit__c;}set{record.Telus_Business_Unit__c = value;}}
    public String   description{get{return record.Description;}set{record.Description = value;}}
    public String   directFulfillDealer{get{return record.Direct_Fulfill_Dealer__c;}set{record.Direct_Fulfill_Dealer__c = value;}}
    public String   formattedDateVerified {get;set;}
    public DateTime wirelineEscalationTimestamp {get{return record.Wireline_Escalation_Timestamp__c;}set{record.Wireline_Escalation_Timestamp__c = value;}}
    public DateTime lastModifiedDate {get{return record.LastModifiedDate;}}
    public DateTime createdDate {get{return record.CreatedDate;}}
    public String   lastModifiedDateFormatted {get;set;}
    public String   createdDateFormatted {get;set;}
    public String   escalationCaseNumber {get{return record.Escalation_Case_Number__c;}set{record.Escalation_Case_Number__c = value;}}
    //Merisha added Marketing Sub Segment A Name field
    public String   subSegmentCodeName { get {return record.Sub_Segment_Code__r.Name;} set{record.Sub_Segment_Code__r.Name = value;}}

    //Constructor
    public trac_Detailed_Account(Id accId){
        this.record = loadAccount(accId);
        if(clientsInformationVerifiedOn != null){
             formattedDateVerified = clientsInformationVerifiedOn.format();
        }else{
            formattedDateVerified = '';
        }       
        
        if(lastModifiedDate != null){ 
            lastModifiedDateFormatted = lastModifiedDate.format();
        }else{
            lastModifiedDateFormatted ='';
        }
        
        createdDateFormatted = createdDate.format();
        /*
        if(createdDate != null) {
            createdDateFormatted = createdDate.format();
        }else{
            createdDateFormatted = '';
        }
        */
    }
    
    //Method to retrieve Account information
    public Account loadAccount(Id accId){
                                        //RCID Fields and general account fields
        Account theAccount = [  SELECT  Name, Id, Owner.Name, Account_Classification__c, Ownerid, RecordTypeId, Account_Status__c, 
                                        BAN__c, BusEx_Bonus_Eligible__c,CBUCID_formula__c, CBUCID_DUNS__c, CBU_CD__c, Parent.Name, 
                                        CBU_Name__c, Client_s_information_verified_on__c, CreatedBy.Name, 
                                        Company_s_Total_Annual_Sales_Per_D_B__c, CreatedById,CustCentral_DUNS__c, LastModifiedBy.Name,
                                        D_B_of_Locations__c, Dealer_Account__c, DNTC__c, Fax, Inactive__c,Information_verified_by__c,
                                        Language_Preference__c, LastModifiedById,
                                        Legal_Business_Name__c, Sub_Segment_Code__c, Marketing_Sub_Segment_Description__c, MIKE_BLACKBERRY__c,
                                        MIKE_OTHER__c, MIKE_PDA__c, MIKE_VOICE_ONLY__c, NAICS_2_Description__c,
                                        NAICS_4_Description__c, NAICS_6_Description__c,  Nbr_of_Employees_D_B__c, Number_of_Employees_Total__c,
                                        ParentId, PCS_BLACKBERRY__c, PCS_iPHONE__c, PCS_MIKE_AIRCARD__c, PCS_OTHER__c, PCS_PDA__c, PCS_VOICE_ONLY__c,
                                        Phone, RCID__c, RCID_Name__c, Team_TELUS_ID__c, Total_TELUS_Billed_Revenue_2011__c,
                                        Total_TELUS_Billed_Revenue_YTD__c, Total_TELUS_Wireless_Revenue_2011__c, Total_TELUS_Wireless_Revenue_YTD__c,
                                        Total_TELUS_Wireline_Revenue_2011__c, Total_TELUS_Wireline_Revenue_YTD__c, Website, Direct_Fulfill_Dealer__c,
                                        Information_verified_by__r.Name, owner.UserRoleId, owner.AccountId, owner.Email, CreatedDate, LastModifiedDate,
                                        //CBUCID fields and any additional general account fields
                                        Account_Classification_CBUCID__c, CBU_Name_CBUCID__c, Company_s_Total_Annual_Sales_CBUCID__c,
                                        D_B_of_Locations_CBUCID__c, 
                                        L2R_Survey_Sent_Month__c, MIKE_OTHER_CBUCID__c, MIKE_PDA_CBUCID__c, MIKE_VOICE_ONLY_CBUCID__c,
                                        NAICS_2_Description_CBUCID__c, NAICS_4_Code_Description_CBUCID__c,
                                        NAICS_6_Code__c, PCS_BLACKBERRY_CBUCID__c, PCS_iPHONE_CBUCID__c, PCS_MIKE_AIRCARD_CBUCID__c,
                                        PCS_OTHER_CBUCID__c, PCS_PDA_CBUCID__c, PCS_VOICE_ONLY_CBUCID__c, 
                                        Strat_Vertical_Description_CBUCID__c, Sub_Segment_Description_CBUCID__c, Total_TELUS_Billed_Revenue_2011_CBUCID__c,
                                        Total_TELUS_Billed_Revenue_YTD_CBUCID__c, Total_TELUS_Wireless_Revenue_2011_CBUCID__c, 
                                        Total_TELUS_Wireless_Revenue_YTD_CBUCID__c, ShippingCity, ShippingCountry, ShippingPostalCode,
                                        ShippingState, ShippingStreet, BillingCity, BillingCountry, BillingPostalCode, BillingState, BillingStreet,
                                        NAICS_4_Description_CBUCID__c, NAICS_6_Description_CBUCID__c, Number_of_Employees_Total_CBUCID__c,
                                        MIKE_BLACKBERRY_CBUCID__c, Total_TELUS_Wireline_Revenue_YTD_CBUCID__c,
                                        Total_TELUS_Wireline_Revenue_2011_CBUCID__c,
                                        //specific to customer central page layout
                                        COMMENTS__c, Telus_Business_Unit__c, Description, RT__c, DUNS__c ,
                                        //request ownership  wireline escalation fields
                                        Wireline_Escalation_Timestamp__c, RecordType.Name, Escalation_Case_Number__c,
                                        //Merisha added this
                                        Sub_Segment_Code__r.Name
                                FROM Account
                                WHERE Id = :accId LIMIT 1];
        return theAccount;
    }
    
    //accessor for the Account record
    public Account getAccount(){
        if (this.record != null){
            return this.record;
        } else{
            return null;
        }
    }
    
}