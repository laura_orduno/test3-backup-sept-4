public class CloudProjectTeamCapacity{
	private Set<String> validCloudProjectStages;
    
    // class constructor
    public CloudProjectTeamCapacity()
    {       
        // populate valid stages
        validCloudProjectStages = 
            new Set<String>{'Not Started (approved & awaiting kick-off)', 'Initiate (BRD)', 'In Progress', 'Testing (sandbox deployment, quality assurance)'};                                  
    }   
                
    public CloudProjectCapacityData getCapacityData()
    {
        CloudProjectCapacityData data = new CloudProjectCapacityData();

        data.projects = new Map<Id, CloudProjectData>();
            
        for(SMET_Project__c project : [SELECT id, name, project_type__c, evolution_roadmap__c, milestone__c, planned_start_date__c, planned_release_date__c FROM SMET_Project__c WHERE 
                milestone__c in :validCloudProjectStages AND (evolution_roadmap__c = 'Yes' OR evolution_roadmap__c = 'No')]) 
        {
            CloudProjectData cpData = new CloudProjectData();
            
            cpData.projectId = project.id;
            cpData.projectName = project.name;
            cpData.projectType = project.project_type__c;
   
            // US date format mm/dd/yyyy	
            cpData.startDate = project.planned_start_date__c.month() + '/' + project.planned_start_date__c.day() + '/' + project.planned_start_date__c.year();
            cpData.endDate = project.planned_release_date__c.month() + '/' + project.planned_release_date__c.day() + '/' + project.planned_release_date__c.year();
            cpData.isEvolution = project.evolution_roadmap__c;
            cpData.milestone = project.milestone__c;
            
            data.projects.put(project.id, cpData);
        }
        
    	data.members = 
            new Map<Id, Cloud_Enablement_Team_Member__c>([SELECT team_member__r.id, team_member__r.name, team_name__c FROM Cloud_Enablement_Team_Member__c]);
        
        data.resourcedMembers = [SELECT Team_Member__r.id, Team_Member__r.name, Cloud_Enablement_Project__r.id, of_workload_for_this_project__c, role__c FROM Cloud_Enablement_Project_Team__c];            
        
        return data;
    }
    
	public static void clearTeams()
    {    
        List<Cloud_Enablement_Team_Member__c> membersToDelete = [
            SELECT id, team_member__c, manager__c, team_name__c FROM Cloud_Enablement_Team_Member__c
        ]; 

/*        
for(Cloud_Enablement_Team_Member__c m : membersToDelete)
{
System.debug('delete member: ' + m.team_member__c);
    
}
*/        
        delete membersToDelete;
    }
    
	public static List<Cloud_Enablement_Team_Member__c> populateTeam(String teamName, String managerName, Set<String> userNameSet)
    {
        List<Cloud_Enablement_Team_Member__c> result = new List<Cloud_Enablement_Team_Member__c>();
        
        try
        {
            // DEV TEAM
            User manager = [
                SELECT id, name FROM User WHERE name = :managerName LIMIT 1
            ];      
            
            Set<String> duplicates = new Set<String>();
            try
            {
                for(User u : [SELECT id, name FROM User WHERE name in :userNameSet])
                {
                    Cloud_Enablement_Team_Member__c teamMember = new Cloud_Enablement_Team_Member__c();
                    teamMember.team_name__c = teamName;
                    if(manager != null)
                    {
                        teamMember.manager__c = manager.id;
                    }    
                    teamMember.team_member__c = u.id;
                    
                    if(!duplicates.contains(u.name) && userNameSet.contains(u.name))
                    {   
System.debug('member added: ' + u.name + '(' + u.id + ')');                        
                        duplicates.add(u.name);
                        result.add(teamMember);
                    }    
                    else
                    {
                        
                    }
                }    
            }
            catch(Exception e)
            {
                System.debug('Error: ' + e.getMessage());
            }            
            duplicates.clear();
        }
        catch(Exception e)
        {
            System.debug('Error: ' + e.getMessage());
        }            
                
        return result;
    }   
    
	public static void populateTeams()
    {    
        List<Cloud_Enablement_Team_Member__c> teamMembers = new List<Cloud_Enablement_Team_Member__c>();

		teamMembers.addAll
            (
            	populateTeam('Dev and Release Mgmt', 'Cindy Nancoo', 
                	 new Set<String>{'Cindy Nancoo', 'Franklin Huang', 'Satish Sekar', 'Jimmy Hsiao', 'Andy Leung', 'Steve Rabouin', 'Travis Cote', 'Robert Brekke', 'Bogdan Enache', 'Feral Rizvi', 'Veneranda Giarrusso', 'Steve Yeung'})
            );
		teamMembers.addAll
            (
            	populateTeam('Support', 'Lenora Ruggieri', 
                	 new Set<String>{'Lenora Ruggieri', 'Allan Roszmann', 'Afzal Makeen', 'Trisha Rakochy', 'Isabelle Decarie', 'Brian Linhares'})
            );
		teamMembers.addAll
            (
            	populateTeam('Data, Training, Comms', 'Glenn Peters', 
                	 new Set<String>{'Glenn Peters', 'Derek Wile', 'Hailey Van Wyk', 'Raj Bhatnagar', 'Fabio Aversa', 'Bonnie Fong', 'Nika Pedersen'})
            );
		teamMembers.addAll
            (
            	populateTeam('Evolution', 'Steve Rhodes', 
                	 new Set<String>{'Steve Rhodes', 'Rita Fang', 'Dawn Underwood', 'Gord Fleming', 'Leah Ho', 'Michelle Remy', 'Deb Sankey', 'Cathy Pearce', 'Julia Garner', 'Chris Ball'})
            );
		teamMembers.addAll
            (
            	populateTeam('Enablement and PMO', 'Sarah Hamilton', 
                	 new Set<String>{'Sarah Hamilton', 'Keith Simpson', 'Kathy MacKenzie', 'Debbie Munro'})
            );
		teamMembers.addAll
            (
            	populateTeam('Back Office Strategy', 'Zoran Krunic', 
                	 new Set<String>{'Zoran Krunic', 'Sheldon Pimenta', 'Eduardo Aguirre', 'Darren Morgan', 'Dora Tsang'})
            );
		teamMembers.addAll
            (
            	populateTeam('CRM Labs', 'Yen Li Chong', 
                	 new Set<String>{'Yen Li Chong', 'Johnson Wong'})
            );
        
        insert teamMembers;        
    }
}