@isTest
private class trac_SMETProjectQuickUpdate_Test {

    static testMethod void quickAddTest() {
        SMET_Project__c p = new SMET_Project__c(name = 'test', Impact__c = 5, Complexity__c = 5);
        insert p;
        
        p = [SELECT new_update__c FROM SMET_Project__c where id = :p.id];
        p.new_update__c = 'test update';
        update p;
        
        p = [SELECT new_update__c FROM SMET_Project__c where id = :p.id];
        System.assertEquals(null,p.new_update__c); // confirm field was cleared
        
        List<SMET_Update__c> updates = [SELECT update_note__c FROM SMET_Update__c WHERE SMET_Project__c = :p.id];
        System.assertEquals('test update',updates[0].update_note__c);
    }
}