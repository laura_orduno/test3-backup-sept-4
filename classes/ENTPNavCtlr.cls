public class ENTPNavCtlr {
	String userAccountId;
	public string CatListPull = '';
	public string AddFuncPull = '';
	public string[] AddFuncPullT;
	public string AddFuncPull1 { get; set; }
//     public string CatListPull1 {get;set;}
	public string CustRole = '';
	public string CustRole1 { get; set; }
	public String combinedStrings {
		get;
		set;
	}

	public List<String> strings {
		get {
			if (combinedStrings == null) {
				return new List<String>();
			}

			return combinedStrings.split(',');
		}
	}

	public String userLanguage {
		get{
			return PortalUtils.getUserLanguage();
		}
		set;
	}
/*****    
*/
	public ENTPNavCtlr() {
		String userAccountId;
		userAccountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;
		// string TelusProducts;
		string additionalFunc;
		system.debug('userAccountId: ' + userAccountId);
		List<User> TelusProducts = [SELECT Customer_Portal_Role__C FROM User WHERE id = :UserInfo.getUserId()];
		//system.debug('Customer_Portal_Role__C: ' + TelusProducts);

		CustRole = TelusProducts[0].Customer_Portal_Role__C;
		if (CustRole != '' && CustRole != null) {
			string[] CustRoleT = CustRole.split(';');
			CustRole1 = CustRoleT[0];
			//return CustRole1;
			system.debug('Customer Role: ' + CustRole1);

			List<ENTP_portal_additional_functions__c> ENTPPortalMap = [Select Name,Customer_Portal_Role__c,Additional_Function__c from ENTP_portal_additional_functions__c where Customer_Portal_Role__c = :CustRole];
			if (ENTPPortalMap.size() > 0) {
				CatListPull = ENTPPortalMap[0].Additional_Function__c;
				String[] CatListPullT = CatListPull.split(';');
				AddFuncPull1 = CatListPullT[0];
				system.debug('CatListPullT: ' + CatListPullT);
			}
		}
	}

	public void setUserLanguage() {
		PortalUtils.setUserLanguage();
	}
}