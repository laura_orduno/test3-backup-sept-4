public with sharing class AddressValidationException extends Exception{
	
	public List<string> messages {get;set;}
	
	public AddressValidationException(List<string> messages) {
		this.messages = messages;
		this.setMessage('');
	}
}