global with sharing class NAAS_AdditionalDetailsCheck implements vlocity_cmt.VlocityOpenInterface2 {
    public String OderId;
    public String errorMsgs; 
    public boolean isRemoteCall = false;
    public string sErrorMessage = '';
    public Map<String, String> errors = new Map<String, String>();
    global NAAS_AdditionalDetailsCheck(){}
    
    global Boolean invokeMethod(String methodName, Map<String, Object> input, Map<String, Object> output, Map<String, Object> options) {
        OderId = (String)input.get('ContextId');
        System.debug('Validate Additional Details $$ Order ID $$ ===========> '+OderId);
        
        if (methodName.indexOf('ValidateAndAmend')!=-1) {
            validateCreditAssessment(input, output, options);
            validateBillingAddress(input, output, options);
            validateShippingAddress(input, output, options);
            //ADHARSH: COMMENTING OUT CONTRACT RELATED CODE FOR PILOT A LAUNCH
            /*if(String.isBlank(errorMsgs)){
                amendContracts(input,output,options);
            }*/
            //ADHARSH: END
        }
        if(errors != null && errors.size()>0)
        {
            output.put('vlcValidationErrors', errors);
        }
        return true;
    }
    
    private List<OrderItem> getOffers(string orderId){
        List<OrderItem> items = [Select Id, OrderId,pricebookentry.product2.OCOM_Shippable__c,Shipping_Address__c,Billing_Address__c,vlocity_cmt__BillingAccountId__c,Billing_Account__c,OfferName__c, order.ServiceAddressSummary__c,vlocity_cmt__ParentItemId__c,Order.status, 
                             Pricebookentry.Product2.Family,
                             PriceBookEntry.product2.Sellable__c,
                             Pricebookentry.Product2.vlocity_cmt__TrackAsAgreement__c,
                             Pricebookentry.Product2.Name,amendStatus__c,
                             vlocity_cmt__LineNumber__c,Shipping_Address_Contact__c
         from OrderItem where orderId=:OderId];
       
        return items;
    }
    
    private void validateCreditAssessment(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> options){
        Map<String, Object> creditOutputMap = new Map<String, Object>();
        OCOM_CreditAssessment_Helper.checkCredit(OderId, creditOutputMap);
        system.debug('OCOM_AdditionalDetailsCheck::validateCreditAssessment() creditOutputMap (' + creditOutputMap + ')');
        String creditFailureMessage = (String)creditOutputMap.get('validationFailureMessage');
        //if (String.isNotBlank(creditFailureMessage)) {
            //addErrorMsg(creditFailureMessage);
            //errors.put('UIErrorCredit', 'Credit Check is missing.');
        //}
        // Thomas OCOM-1233 Start
        if (creditOutputMap.get('noCreditProfile')==true) {
            errors.put('UIErrorCredit', system.Label.OCOM_Credit_Profile_Is_Missing);
        } else if (creditOutputMap.get('isCarRequired')==true && creditOutputMap.get('currentCARId')==null) {
            errors.put('UIErrorCredit', system.Label.OCOM_Credit_Check_Is_Missing);
        } else if (creditOutputMap.get('moreThan10PercentChange')==true) {
            errors.put('UIErrorCredit', system.Label.OCOM_CAR_Amend_Required);
        }
        // Thomas OCOM-1233 End
    }
    
    private void validateShippingAddress(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> options){
        List<OrderItem> items = getOffers(OderId);
        boolean missing = false;
        if(items !=null && items.size()>0){
            for(orderItem o:items){
                if(o.pricebookentry.product2.OCOM_Shippable__c == 'Yes' && String.isBlank(o.Shipping_Address__c) ){
                      errors.put('UIErrorShipping',Label.OCOM_OLIShippingAddressValidationmsg +' '+ o.OfferName__c);
                      missing = true;
                }
                if(o.pricebookentry.product2.OCOM_Shippable__c == 'Yes' && String.isBlank(o.Shipping_Address_Contact__c)){
                       errors.put('UIErrorShipping',label.OCOM_OLIShippingAddrContactNameValidationmsg +' '+ o.OfferName__c);
                       missing = true;
                }
            }
        }
        if(missing)
            errors.put('UIErrorShipping', 'Shipping addresses and contacts are missing.');
    }    

    private void validateBillingAddress(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> options) {
        // List<OrderItem> items = [Select Id, OrderId, Billing_Address__c,vlocity_cmt__BillingAccountId__c,Billing_Account__c, OCOMProductName__c, order.ServiceAddressSummary__c from OrderItem where orderId=:OderId and vlocity_cmt__ParentItemId__c = null];
        System.debug('Validate Additional Details $$ Order ID $$ ===========> '+OderId);
        List<OrderItem> items = getOffers(OderId);
        boolean missing = false;
        if (items !=null && items.size()>0) {
            for (OrderItem item :items) {
                if(String.isBlank(item.Billing_Account__c) && String.isBlank(item.vlocity_cmt__ParentItemId__c)) {
                    //addErrorMsg('Billing Address required for '+ item.OfferName__c);
                    missing = true;
                }
            }
        }
        if(missing)
            errors.put('UIErrorBilling', 'Billing Addresses are missing.');
        //Map<String, Object> myErrorMap = new Map<String, Object>();
        //myErrorMap.put('ValidationErrorDisplay', 'BillingAddress Test Error'); // additionalDetails
        //outputMap.put('vlcValidationErrors',myErrorMap);
        //outputMap.put('error', myErrorMap);
    }

    //ADHARSH: COMMENTING OUT CONTRACT RELATED CODE FOR PILOT-A LAUNCH
    /*
    private void amendContracts(Map<String, Object> inputMap, Map<String, Object> outputMap, Map<String, Object> options) {
        map<string,orderItem> LinenumberTOCliIDMap = new map<string,orderItem>();

        try { 
            String  OrderId = (string)inputMap.get('ContextId');
            Order objOrder = new Order(id=OrderId);
            boolean isItemsAmended = false;
            system.debug('Order Id_____:' + objOrder);

            //Iterate trough Order Items to see if any of them are Amended.--- Check if we need to use a Specific value for AmendStatus
            list<OrderItem> orderItemsList = getOffers(OrderId);
            if (orderItemsList.size() > 0 && !orderItemsList.isEmpty()) {
                for (orderItem oli:orderItemsList) {
                    string lnumber = string.valueof(oli.vlocity_cmt__LineNumber__c) != null? string.valueof(oli.vlocity_cmt__LineNumber__c):'';
                    string prarentLineNumber  = OCOM_ContractUtil.getParentlineNumber(lnumber);   
                    if(prarentLineNumber != null && LinenumberTOCliIDMap.containsKey(prarentLineNumber) && oli.amendStatus__c != null){
                        isItemsAmended = true;
                        break; 
                    } else if(!LinenumberTOCliIDMap.containsKey(prarentLineNumber) && oli.PriceBookEntry.product2.Sellable__c == true &&  oli.Pricebookentry.Product2.vlocity_cmt__TrackAsAgreement__c == true ){
                        if( oli.amendStatus__c != null){
                            isItemsAmended = true;
                            system.debug('isItemsAmended_________:'+isItemsAmended + '__Item Name__'+ oli.Pricebookentry.Product2.Name);
                            break; 
                         } else{
                            LinenumberTOCliIDMap.put(prarentlinenumber,oli);
                            system.debug('LinenumberTOCliIDMap_________:'+LinenumberTOCliIDMap);
                        }
                    }
                }
            }
            if (isItemsAmended == false) {
                sErrorMessage = Label.SUCCESS;
            } else if(isItemsAmended == true) {
                map<String,object> outMap = OCOM_ContractUtil.AmendOrder2(objOrder);
                sErrorMessage=(string)outMap.get('amendContractResponse');
                isRemoteCall=(boolean)outMap.get('isCallContinuation');
                System.debug('OutMap--->' + outMap);
            }
            if (sErrorMessage != Label.SUCCESS) {  
                errorMsgs = sErrorMessage;
            }
            system.debug('****isRemoteCall*****'+isRemoteCall + '***sErrorMessage****'+sErrorMessage);
        } catch(exception e) {
            String errorMessage=Label.OCOM_ExceptionMessagePrefix+e.getMessage();
            System.debug(LoggingLevel.ERROR, 'Exception is '+e);
            System.debug(LoggingLevel.ERROR, 'Exception stack trace '+e.getStackTraceString());
            errorMsgs = errorMessage;
            // outPutMap.put('error',errorMessage);
        }
    }*/
    //ADHARSH:COMMENT END
/*
    private void addErrorMsg(String msg) {
        if(String.isBlank(errorMsgs))
           errorMsgs = msg;
        else
           errorMsgs = errorMsgs + '\n' + msg;
    }
*/
}