@IsTest(seeAllData=true)
public class OCOM_AutoAttachDocExt_Test {
    static testmethod void OCOM_AutoAttachDocExtUnitTest(){
        TestDataHelper.testContractVersionCreation();
        TestDataHelper.testCustomSettingCreation_BA();
        TestDataHelper.testvlocityDocTemplateteCreation();
        Test.startTest();
        Apexpages.currentPage().getparameters().put('isInConsole','true');
        Apexpages.currentPage().getparameters().put('id',TestDataHelper.testContractObj.id);
        Apexpages.currentPage().getparameters().put('templatePicker','Business Anywhere Test');
        
       // ApexPages.currentPage().getParameters().put('isUpdate','true');
        OCOM_AutoAttachDocExt objOCOM_AutoAttachDocExt=new OCOM_AutoAttachDocExt(new vlocity_cmt.ContractDocumentCreationController());
        boolean isInConsole=objOCOM_AutoAttachDocExt.isInConsole;
        system.assertEquals(isInConsole, true);
        objOCOM_AutoAttachDocExt.attachDoc();
       // ApexPages.currentPage().getParameters().put('isUpdate','false');
        Apexpages.currentPage().getparameters().put('isInConsole','false');
        ApexPages.currentPage().getParameters().put('isUpdate','true');
        Apexpages.currentPage().getparameters().put('id',TestDataHelper.testContractObj.id);
         Apexpages.currentPage().getparameters().put('templatePicker','Business Anywhere Test');
        OCOM_AutoAttachDocExt objOCOM_AutoAttachDocExt1=new OCOM_AutoAttachDocExt(new vlocity_cmt.ContractDocumentCreationController());
        boolean isInConsole1=objOCOM_AutoAttachDocExt1.isInConsole;
        
        Apexpages.currentPage().getparameters().put('isInConsole','false');
        ApexPages.currentPage().getParameters().put('isUpdate','true');
        Apexpages.currentPage().getparameters().put('id',TestDataHelper.testContractObj.id);
         Apexpages.currentPage().getparameters().put('templatePicker','Business Anywhere Test');
        OCOM_AutoAttachDocExt objOCOM_AutoAttachDocExt2=new OCOM_AutoAttachDocExt(new vlocity_cmt.ContractDocumentCreationController());
        boolean isInConsole2=objOCOM_AutoAttachDocExt2.isInConsole;
        system.assertEquals(isInConsole2, false);
        OCOM_AutoAttachDocExt.activeContVersion =TestDataHelper.testContractVersionObj;
        objOCOM_AutoAttachDocExt2.attachDoc();
        
        Test.stopTest();
    }

    static testmethod void OCOM_AutoAttachDocExtUnitNoTemplateTest(){
        TestDataHelper.testContractVersionCreation();
        TestDataHelper.testCustomSettingCreation_BA();
        TestDataHelper.testvlocityDocTemplateteCreation();
        for (vlocity_cmt__DocumentTemplate__c docT: [SELECT Id,Name FROM vlocity_cmt__DocumentTemplate__c 
                                                     WHERE (Name = 'Amendment Agreement' AND vlocity_cmt__IsActive__c = TRUE 
                                                     AND vlocity_cmt__ApplicableTypes__c INCLUDES ('Contract'))]){
            System.debug('docT.Name____' + docT.name);
        }
        Test.startTest();
        Apexpages.currentPage().getparameters().put('isInConsole','true');
        Apexpages.currentPage().getparameters().put('id',TestDataHelper.testContractObj.id);
        Apexpages.currentPage().getparameters().put('templatePicker','');
        OCOM_AutoAttachDocExt objOCOM_AutoAttachDocExt=new OCOM_AutoAttachDocExt(new vlocity_cmt.ContractDocumentCreationController());
        boolean isInConsole=objOCOM_AutoAttachDocExt.isInConsole;
        system.assertEquals(isInConsole, true);
        objOCOM_AutoAttachDocExt.attachDoc();
        Test.stopTest();
    }
        
}