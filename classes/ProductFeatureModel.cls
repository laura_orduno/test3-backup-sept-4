public class ProductFeatureModel {
	public ProductVO.Feature vo {get; set;} 
	public List<ProductOptionModel> options {get; set;}
	public ProductOptionSelectionModel selections {get; set;}
	
	public ProductModel configuredProduct {get; private set;}
	public Boolean valid {get; private set;}
	public String validationMessage {get; set;}
	public Integer index {get; set;}
	
	private String key;
	private Map<Id,ProductOptionModel> optionsById;
	public Id id {get{return (vo != null) ? vo.record.Id : null;}}

	/**
	 * Default constructor.
	 */
	public ProductFeatureModel(ProductModel configuredProduct) {
		this.configuredProduct = configuredProduct;
		options = new List<ProductOptionModel>();
		index = 0;
		key = configuredProduct.getId() + '_other';
	}
	
	public ProductFeatureModel(ProductModel configuredProduct, ProductVO.Feature vo) {
		this(configuredProduct);
		this.vo = vo;
		key = (String)vo.record.Id;
		addOptions(vo.options);
	}
	
	/**
	 * Returns the title to use for Configure Option dialog.
	 */
	public String getAddOptionDialogTitle() {
		String title = 'Add Option';
		if (vo != null) {
			Schema.SObjectField titleField = MetaDataUtils.getField(SBQQ__ProductFeature__c.sObjectType, 'AddOptionDialogTitle__c');
			if ((titleField != null) && (vo.record.get(titleField) != null)) {
				title = String.valueOf(vo.record.get(titleField));
			}
		}
		return title;
	}
	
	/**
	 * Returns the label to use for Add Option button.
	 */
	public String getAddOptionButtonLabel() {
		String label = 'Add Option';
		if (vo != null) {
			Schema.SObjectField labelField = MetaDataUtils.getField(SBQQ__ProductFeature__c.sObjectType, 'AddOptionButtonLabel__c');
			if ((labelField != null) && (vo.record.get(labelField) != null)) {
				label = String.valueOf(vo.record.get(labelField));
			}
		}
		return label;
	}
	
	/**
	 * Returns comma separated API field names matching Option Lookup Fieldset to exclude on Add Product action   
	 */
	public String getAddOptionExcludedFields() {
		String fields = '';
		if (vo != null) {
			Schema.SObjectField labelField = MetaDataUtils.getField(SBQQ__ProductFeature__c.sObjectType, 'AddOptionExcludedFields__c');
			if ((labelField != null) && (vo.record.get(labelField) != null)) {
				fields = String.valueOf(vo.record.get(labelField));
			}
		}
		return fields;
	}
	
	public String getAdditionalInstructions() {
		String msg = '';
		if (vo != null) {
			Schema.SObjectField msgField = MetaDataUtils.getField(SBQQ__ProductFeature__c.sObjectType, 'AdditionalInstructions__c');
			if ((msgField != null) && (vo.record.get(msgField) != null)) {
				msg = String.valueOf(vo.record.get(msgField));
			}
		}
		return msg;
	}
	
	
	public String getKey() {
		return key;
	}
	
	public String getTitle() {
		return (vo != null) ? vo.record.Name : 'Other Options';
	}
	
	public void addOptions(List<ProductVO.Option> optionVOs) {
		for (ProductVO.Option vo : optionVOs) {
			ProductOptionModel option = new ProductOptionModel(configuredProduct, this, vo);
			options.add(option);
		}
	}
	
	public Boolean getMultipleSelection() {
		if (vo == null) {
			return false;
		}
		return (vo.record.SBQQ__MaxOptionCount__c != null) && (vo.record.SBQQ__MaxOptionCount__c > 1);
	}
	
	/**
	 * Used by productOptionLookup component to obtain the list of options
	 * available to be added to this feature.
	 */
	public List<ProductOptionModel> getAvailableOptions() {
		List<ProductOptionModel> result = new List<ProductOptionModel>();
		for (ProductOptionModel option : options) {
			if (!option.vo.record.SBQQ__Required__c && (option.vo.record.SBQQ__UnitPrice__c != null)) {
				result.add(option);
			}
		}
		return result;
	}
	
	private ProductOptionModel getOptionById(Id optionId) {
		if (optionsById == null) {
			optionsById = new Map<Id,ProductOptionModel>();
			for (ProductOptionModel option : options) {
				optionsById.put(option.id, option);
			}
		}
		return optionsById.get(optionId);
	}
}