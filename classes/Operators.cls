public class Operators {
    public static final String EQ = 'equals';
    public static final String NEQ = 'not equals';
    public static final String LT = 'less than';
    public static final String LTE = 'less or equals';
    public static final String GT = 'greater than';
    public static final String GTE = 'greater or equals';
    public static final String SW = 'starts with';
    public static final String EW = 'ends with';
    public static final String CONT = 'contains';
    
    private static Map<String,Logical> OPERATOR_INDEX = new Map<String,Logical> {
        EQ => new Equals(),NEQ => new NotEquals(),LT => new LessThan(),
        LTE => new LessThanEquals(), GT => new GreaterThan(),
        GTE => new GreaterThanEquals(), SW => new StartsWith(), EW => new EndsWith(), CONT => new Contains()
    };
    
    public static Logical getInstance(String key) {
        return OPERATOR_INDEX.get(key);
    }
    
    public interface Logical {
        Boolean evaluate(Object leftValue, Object rightValue);
        
        QueryBuilder.OperatorExpression createQueryExpression(String fieldName, Object value);
    }
    
    public class Equals implements Logical {
        private Map<String,Value> testValueCache = new Map<String,Value>();
        private Map<String,Value> fieldValueCache = new Map<String,Value>();
        
        public Boolean evaluate(Object fieldValue, Object testValue) {
            if ((fieldValue == null) && (testValue == null)) {
                return true;
            }
            if ( ((fieldValue == null) && (testValue != null)) || ((fieldValue != null) && (testValue == null)) ) {
                return false;
            }
            
            if (fieldValue instanceof String) {
                //String fv = ((String)fieldValue).toLowerCase();
                //String tv = String.valueOf(testValue).toLowerCase();
                if (!testValueCache.containsKey(String.valueOf(testValue).toLowerCase())) {
                    testValueCache.put(String.valueOf(testValue).toLowerCase(), Operators.valueFor(String.valueOf(testValue).toLowerCase()));
                }
                if (!fieldValueCache.containsKey( ((String)fieldValue).toLowerCase()) ) {
                    fieldValueCache.put( ((String)fieldValue).toLowerCase(), Operators.valueFor( ((String)fieldValue).toLowerCase() ));
                }
                return testValueCache.get( String.valueOf(testValue).toLowerCase() ).equals(fieldValueCache.get( ((String)fieldValue).toLowerCase() ));
            } else if (fieldValue instanceof Boolean) {
                return ( ((Boolean)fieldValue) == String.valueOf(testValue).toLowerCase().equals('true'));
            } else if (fieldValue instanceof Decimal) {
                return ( ((Decimal)fieldValue) == toDecimalSkipNullCheck(testValue));
            } else if (fieldValue instanceof Date) {
                return ( ((Date)fieldValue) == toDateSkipNullCheck(testValue));
            } else if (fieldValue instanceof DateTime) {
                return ( ((DateTime)fieldValue) == toDateTimeSkipNullCheck(testValue));
            } 
            
            /* else {
                throw new UnsupportedTypeException(fieldValue);
            } */
            
            return false;
        }
        
        public QueryBuilder.OperatorExpression createQueryExpression(String fieldName, Object value) {
            return new QueryBuilder.OperatorExpression(fieldName, QueryBuilder.EQUALS, value);
        }
    }
    
    public class NotEquals implements Logical {
        private Equals equals = new Equals();
        public Boolean evaluate(Object fieldValue, Object testValue) {
            return !equals.evaluate(fieldValue, testValue);
        }
        
        public QueryBuilder.OperatorExpression createQueryExpression(String fieldName, Object value) {
            return new QueryBuilder.OperatorExpression(fieldName, QueryBuilder.NOT_EQUALS, value);
        }
    }
    
    public class GreaterThan implements Logical {
        public Boolean evaluate(Object fieldValue, Object testValue) {
            if ((fieldValue == null) || (testValue == null)) {
                return false;
            }
            if (fieldValue instanceof Decimal) {
                return ( ((Decimal)fieldValue) > toDecimalSkipNullCheck(testValue));
            } else if (fieldValue instanceof Date) {
                return ( ((Date)fieldValue) > toDateSkipNullCheck(testValue));
            } else if (fieldValue instanceof DateTime) {
                return ( ((DateTime)fieldValue) > toDateTimeSkipNullCheck(testValue));
            }
            return false;
        }
        
        public QueryBuilder.OperatorExpression createQueryExpression(String fieldName, Object value) {
            return new QueryBuilder.OperatorExpression(fieldName, QueryBuilder.GREATER_THAN, value);
        }
    }
    
    public class GreaterThanEquals implements Logical {
        public Boolean evaluate(Object fieldValue, Object testValue) {
            if ((fieldValue == null) || (testValue == null)) {
                return false;
            }
            if (fieldValue instanceof Decimal) {
                return ( ((Decimal)fieldValue) >= toDecimalSkipNullCheck(testValue));
            } else if (fieldValue instanceof Date) {
                return ( ((Date)fieldValue) >= toDateSkipNullCheck(testValue));
            } else if (fieldValue instanceof DateTime) {
                return ( ((DateTime)fieldValue) >= toDateTimeSkipNullCheck(testValue));
            }
            return false;
        }
        
        public QueryBuilder.OperatorExpression createQueryExpression(String fieldName, Object value) {
            return new QueryBuilder.OperatorExpression(fieldName, QueryBuilder.GREATER_EQUALS, value);
        }
    }
    
    public class LessThan implements Logical {
        public Boolean evaluate(Object fieldValue, Object testValue) {
            if ((fieldValue == null) || (testValue == null)) {
                return false;
            }
            if (fieldValue instanceof Decimal) {
                return ( ((Decimal)fieldValue) < toDecimalSkipNullCheck(testValue));
            } else if (fieldValue instanceof Date) {
                return ( ((Date)fieldValue) < toDateSkipNullCheck(testValue));
            } else if (fieldValue instanceof DateTime) {
                return ( ((DateTime)fieldValue) < toDateTimeSkipNullCheck(testValue));
            }
            return false;
        }
        
        public QueryBuilder.OperatorExpression createQueryExpression(String fieldName, Object value) {
            return new QueryBuilder.OperatorExpression(fieldName, QueryBuilder.LESS_THAN, value);
        }
    }
    
    public class LessThanEquals implements Logical {
        public Boolean evaluate(Object fieldValue, Object testValue) {
            if ((fieldValue == null) || (testValue == null)) {
                return false;
            }
            if (fieldValue instanceof Decimal) {
                return ( ((Decimal)fieldValue) <= toDecimalSkipNullCheck(testValue));
            } else if (fieldValue instanceof Date) {
                return ( ((Date)fieldValue) <= toDateSkipNullCheck(testValue));
            } else if (fieldValue instanceof DateTime) {
                return ( ((DateTime)fieldValue) <= toDateTimeSkipNullCheck(testValue));
            }
            return false;
        }
        
        public QueryBuilder.OperatorExpression createQueryExpression(String fieldName, Object value) {
            return new QueryBuilder.OperatorExpression(fieldName, QueryBuilder.LESS_EQUALS, value);
        }
    }
    
    public class StartsWith implements Logical {
        public Boolean evaluate(Object fieldValue, Object testValue) {
            if ((fieldValue == null) || (testValue == null)) {
                return false;
            }
            
            return String.valueOf(fieldValue).toLowerCase().startsWith(String.valueOf(testValue).toLowerCase());
        }
        
        public QueryBuilder.OperatorExpression createQueryExpression(String fieldName, Object value) {
            if (value != null) {
                value = value + '%';
            }
            return new QueryBuilder.OperatorExpression(fieldName, QueryBuilder.LKE, value);
        }
    }
    
    public class EndsWith implements Logical {
        public Boolean evaluate(Object fieldValue, Object testValue) {
            if ((fieldValue == null) || (testValue == null)) {
                return false;
            }
            
            return String.valueOf(fieldValue).toLowerCase().endsWith(String.valueOf(testValue).toLowerCase());
        }
        
        public QueryBuilder.OperatorExpression createQueryExpression(String fieldName, Object value) {
            if (value != null) {
                value = '%' + value;
            }
            return new QueryBuilder.OperatorExpression(fieldName, QueryBuilder.LKE, value);
        }
    }
    
    public class Contains implements Logical {
        public Boolean evaluate(Object fieldValue, Object testValue) {
            if ((fieldValue == null) || (testValue == null)) {
                return false;
            }
            
            String fv = String.valueOf(fieldValue).toLowerCase();
            String tv = String.valueOf(testValue).toLowerCase();
            return fv.contains(tv);
        }
        
        public QueryBuilder.OperatorExpression createQueryExpression(String fieldName, Object value) {
            if (value != null) {
                value = '%' + value + '%';
            }
            return new QueryBuilder.OperatorExpression(fieldName, QueryBuilder.LKE, value);
        }
    }
    
    private virtual class Value {
        private String value;
        
        private Value() {
        }
        
        private Value(String value) {
            this.value = value;
        }
        
        public String getValue() {
            return value;
        }
        
        public virtual boolean equals(Value value) {
            return this.value.equals(value.getValue());
        }
    }
    
    private class MultipleValue extends Value {
        private Set<String> values;
        
        private MultipleValue(String value) {
            values = new Set<String>();
            for (String v : value.split(',')) {
                values.add(v.trim());
            }
        }
        
        public Set<String> getValues() {
            return values;
        }
        
        public override boolean equals(Value other) {
            if (other instanceof MultipleValue) {
                Set<String> otherValues = ((MultipleValue)other).getValues();
                for (String value : otherValues) {
                    if (values.contains(value)) {
                        return true;
                    }
                }
                return false;
            } else {
                return values.contains(other.getValue());
            }
        }
    }
    
    private static Decimal toDecimal(Object value) {
        if (value == null) {
            return null;
        }
        return toDecimalSkipNullCheck(value);
    }
    
    private static Decimal toDecimalSkipNullCheck(Object value) {
        Decimal result = null;
        try {
            result =  Decimal.valueOf(String.valueOf(value));
        } catch (TypeException te) {
        }
        return result;
    }
    
    private static Date toDate(Object value) {
        if (value == null) {
            return null;
        }
        return toDateSkipNullCheck(value);
    }
    
    private static Date toDateSkipNullCheck(Object value) {
        Date result = null;
        try {
            result =  Date.valueOf(String.valueOf(value));
        } catch (TypeException te) {
        }
        return result;
    }
    
    private static DateTime toDateTime(Object value) {
        if (value == null) {
            return null;
        }
        return toDateTimeSkipNullCheck(value);
    }
    
    private static DateTime toDateTimeSkipNullCheck(Object value) {
        DateTime result = null;
        try {
            result =  DateTime.valueOf(String.valueOf(value));
        } catch (TypeException te) {
        }
        return result;
    }
    
    private static Value valueFor(String value) {
        //value = value.toLowerCase();
        if ( (value.indexOf(',') > -1) && (!value.startsWith('"')) && (!value.endsWith('"')) ) {
            return new MultipleValue(value);
        } else {
            return new Value(value);
        }
    }
}