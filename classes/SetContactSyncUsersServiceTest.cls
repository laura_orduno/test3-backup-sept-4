@isTest
private class SetContactSyncUsersServiceTest {
// TESTS
	
	private static testMethod void testHandleInboundEmail() {
		User u = insertTestUser();
		
		Messaging.InboundEmail email = new Messaging.Inboundemail();
		email.plainTextBody = u.id;
		
		Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
		
		SetContactSyncUsersService service = new SetContactSyncUsersService();
		
		User runner = u.clone();
    runner.username = Datetime.now().getTime() + 'RUNNER@TRACTIONSM.COM';
    
    system.runas(runner) {
			service.handleInboundEmail(email, envelope);
    }
		
		u = [SELECT uas__Sync_to_Contact__c FROM User WHERE Id = :u.id];
		system.assert(u.uas__Sync_to_Contact__c);
	}
	
	private static testMethod void testUpdateUsers() {
		User u = insertTestUser();
		
		SetContactSyncUsersService service = new SetContactSyncUsersService();
		
		User runner = u.clone();
    runner.username = Datetime.now().getTime() + 'RUNNER@TRACTIONSM.COM';
    
    system.runas(runner) {
			service.updateUsers(u.id);
    }
		
		u = [SELECT uas__Sync_to_Contact__c FROM User WHERE Id = :u.id];
		system.assert(u.uas__Sync_to_Contact__c);
	}
	
	private static testMethod void testUpdateUsersIdWithExtraChars() {
		User u = insertTestUser();
		
		SetContactSyncUsersService service = new SetContactSyncUsersService();
		
		User runner = u.clone();
    runner.username = Datetime.now().getTime() + 'RUNNER@TRACTIONSM.COM';
    
    system.runas(runner) {
			service.updateUsers('dfsgdsgdg$' + u.id + '^jfiehgfiuegfi');
    }
		
		u = [SELECT uas__Sync_to_Contact__c FROM User WHERE Id = :u.id];
		system.assert(u.uas__Sync_to_Contact__c);
	}
	
	// TEST HELPERS
	
	@isTest
	private static User insertTestUser() {
		Id PROFILEID = [SELECT id FROM Profile WHERE name='Standard User' LIMIT 1].id;
        
    User u = new User(
    	username = Datetime.now().getTime() + 'TEST@TRACTIONSM.COM',
      email = 'test@example.com',
      title = 'test',
      lastname = 'test',
      alias = 'test',
      TimezoneSIDKey = 'America/Los_Angeles',
      LocaleSIDKey = 'en_US',
      EmailEncodingKey = 'UTF-8',
      ProfileId = PROFILEID,
      LanguageLocaleKey = 'en_US'
    );
    
    insert u;
    
    return u;
	}
}