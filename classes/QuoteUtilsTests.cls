/**
 * Test suite for <code>QuoteUtils</code> class.
 * 
 * @author Max Rudman
 * @since 4/7/2010
 */
@isTest
private class QuoteUtilsTests {
	testMethod static void testGetCurrencySymbol() {
		if (UserInfo.isMultiCurrencyOrganization()) {
			System.assertEquals('', QuoteUtils.getCurrencySymbol(new SBQQ__Quote__c()));
		} else {
			System.assertEquals('$', QuoteUtils.getCurrencySymbol(new SBQQ__Quote__c()));
		}
	}
	
	testMethod static void testGetCurrencyFormatPattern() {
		System.assertEquals('$#,##0.00', QuoteUtils.getCurrencyFormatPattern('$',2));
	}
	
	testMethod static void testToAmount() {
		System.assertEquals(20, QuoteUtils.toAmount(200,10));
	}
	
	testMethod static void testToPercent() {
		System.assertEquals(50, QuoteUtils.toPercent(200,100));
	}
	
	/* testMethod static void testFormatDecimal() {
		QuoteUtils.formatDecimal(45.67);
	} */
	
	testMethod static void testToDouble() {
		Double value = 45.67;
		System.assertEquals(value, QuoteUtils.toDouble(value.format()));
	}
	
	testMethod static void testCollectIds() {
		Opportunity opp = new Opportunity(Name='Test',StageName='Prospecting',CloseDate=System.today());
		insert opp;
		
		SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
		insert quote;
		
		Set<Id> result = QuoteUtils.collectIds(new List<SBQQ__Quote__c>{quote});
		System.assertEquals(1, result.size());
	}
	
	testMethod static void testCollectProductIds() {
		Opportunity opp = new Opportunity(Name='Test',StageName='Prospecting',CloseDate=System.today());
		insert opp;
		
		Product2 p = new Product2(Name='Test');
		insert p;
		
		SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
		insert quote;
		
		SBQQ__QuoteLine__c line = new SBQQ__QuoteLine__c(SBQQ__Quote__c=quote.Id,SBQQ__Product__c=p.Id);
		insert line;
		
		Set<Id> result = QuoteUtils.collectProductIds([SELECT Name, (SELECT SBQQ__Product__c FROM SBQQ__LineItems__r) FROM SBQQ__Quote__c WHERE Id = :quote.Id]);
		System.assertEquals(1, result.size());
	}
	
	testMethod static void testCollectOpportunityIds() {
		Opportunity opp = new Opportunity(Name='Test',StageName='Prospecting',CloseDate=System.today());
		insert opp;
		
		SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c=opp.Id);
		
		Set<Id> result = QuoteUtils.collectOpportunityIds(new List<SBQQ__Quote__c>{quote});
		System.assertEquals(1, result.size());
	}
	
	testMethod static void testIsFieldMappable() {
		System.assert(!QuoteUtils.isFieldMappable(Contact.AccountId, Contact.LastName));
		System.assert(QuoteUtils.isFieldMappable(Contact.FirstName, Contact.LastName));
		System.assert(!QuoteUtils.isFieldMappable(Contact.AccountId, SBQQ__Quote__c.SBQQ__Opportunity__c));
		System.assert(QuoteUtils.isFieldMappable(Contact.AccountId, Opportunity.AccountId));
	}
	
	testMethod static void testSafeMathOperations() {
		System.assertEquals(null, QuoteUtils.multiply(null, 45.67));
		System.assertEquals(20.40, QuoteUtils.multiply(2, 10.20));
		System.assertEquals(0, QuoteUtils.multiplyAsZero(null, 10.20));
		System.assertEquals(20.40, QuoteUtils.multiplyAsZero(2, 10.20));
		System.assertEquals(null, QuoteUtils.subtract(25, null));
		System.assertEquals(20, QuoteUtils.subtract(25, 5));
	}
	
	testMethod static void testMapAdditionalInfoCustomFields() {
		SBQQ__QuoteLine__c quoteLine = new SBQQ__QuoteLine__c();
		SBQQ__ProductOption__c productOption = new SBQQ__ProductOption__c();
		QuoteUtils.mapAdditionalInfoCustomFields(quoteLine, productOption);
	}
	
	testMethod static void testCopyAdditionalInformation() {
		SBQQ__ProductOption__c productOption = new SBQQ__ProductOption__c();
		String encodedAdditionalInfo = 'ComponentCode__c:123';
		QuoteUtils.copyAdditionalInformation(productOption, encodedAdditionalInfo);
	}
	
	testMethod static void testAppendAdditionalInfo() {
		String encodedAdditionalInfo = '';
		String fieldName = 'Selection_Id';
		String value = '123';
		QuoteUtils.appendAdditionalInfo(encodedAdditionalInfo, fieldName, value);
	}
	
	testMethod static void testMapCustomFields() {
		SBQQ__ProductOption__c src = new SBQQ__ProductOption__c();
		SBQQ__ProductOption__c dest = new SBQQ__ProductOption__c();
		List<String> fields = new List<String>();
		fields.add('Id');
		QuoteUtils.mapCustomFields(dest, src, fields);		
 	}
 	
	testMethod static void testConverNumber() {
		Double doubleValue = 1.0;
		SObjectField doubleField = SBQQ__QuoteLine__c.SBQQ__Quantity__c;
		Integer integerValue = 1;
		Object result = QuoteUtils.convertNumber(doubleField, integerValue);
		System.assert(result instanceof Double);
		result = QuoteUtils.convertNumber(doubleField, doubleValue);
		System.assert(result instanceof Double);
		result = QuoteUtils.convertNumber(doubleField, null);
		System.assertEquals(null, result);		
	}
	
	// Coverage Increase - Traction 2013-03
	
	private static testMethod void testToDoubleEmpty() {
		system.assertEquals(null,QuoteUtils.toDouble(''));
	}
	
	private static testMethod void testToDoubleNoDecimalPoint() {
		system.assertEquals(22.0, QuoteUtils.toDouble('22'));
	}
	
	private static testMethod void testGetCurrencySymbolRecordWithIso() {
		Account a = new Account(name = 'test', CurrencyIsoCode = 'CAD');
		
		system.assertEquals('CAD ', QuoteUtils.getCurrencySymbol(a));
	}
	
	private static testMethod void testGetCurrencyFormatPatternFromSObject() {
		Account a = new Account(name = 'test', CurrencyIsoCode = 'CAD');
		
		String pattern = QuoteUtils.getCurrencyFormatPattern(a, 2);
		system.assertEquals('CAD #,##0.00',pattern);
	}
	
	private static testMethod void testFormatDecimal() {
		String result = QuoteUtils.formatDecimal(3.12552);
		system.assertEquals('3.12552', result);
		
		result = QuoteUtils.formatDecimal(3.0);
		system.assertEquals('3', result);
	}
	
	private static testMethod void testHtmlEncode() {
		String result = QuoteUtils.htmlEncode('Test & test');
		
		system.assertEquals('Test &amp; test', result);
	}
	
	private static testMethod void testHtmlEncodeNull() {
		String result = QuoteUtils.htmlEncode(null);
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testIsUnitPriceField() {
		Boolean result = QuoteUtils.isUnitPriceField(SBQQ__QuoteLine__c.SBQQ__ListPrice__c);
		system.assert(result);
		
		result = QuoteUtils.isUnitPriceField(Account.Name);
		system.assertEquals(false, result);
	}
	
	private static testMethod void testIsValueTrue() {
		Boolean result = QuoteUtils.isValueTrue(null);
		system.assertEquals(false,result);
		
		result = QuoteUtils.isValueTrue(true);
		system.assert(result);
		
		result = QuoteUtils.isValueTrue(false);
		system.assertEquals(false,result);
		
		result = QuoteUtils.isValueTrue(1.0);
		system.assert(result);
		
		result = QuoteUtils.isValueTrue(0.0);
		system.assertEquals(false,result);
		
		result = QuoteUtils.isValueTrue('Yes');
		system.assert(result);
		
		result = QuoteUtils.isValueTrue('');
		system.assertEquals(false,result);
	}
	
	private static testMethod void testDivide() {
		Decimal result = QuoteUtils.divide(5.0, 2.5);
		
		system.assertEquals(2.0, result);
	}
	
	private static testMethod void testDivideNull() {
		Decimal result = QuoteUtils.divide(5.0, null);
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testAdd() {
		Decimal result = QuoteUtils.add(1.75, 1.5);
		
		system.assertEquals(3.25, result);
	}
	
	private static testMethod void testAddNull() {
		Decimal result = QuoteUtils.add(1.75, null);
		
		system.assertEquals(null, result);
	}
	
	private static testMethod void testAddAsZero() {
		Decimal result = QuoteUtils.addAsZero(2.5, null);
		
		system.assertEquals(2.5, result);
		
		result = QuoteUtils.addAsZero(null, null);
		
		system.assertEquals(0.0, result);
	}
	
	private static testMethod void testIndexFieldsByName() {
		List<Schema.SObjectField> fieldList = new List<Schema.SObjectField>{Account.Name, Account.Phone};
		
		Map<String,Schema.SObjectField> results = QuoteUtils.indexFieldsByName(fieldList);
		
		system.assert(results.containsKey('Name'));
		system.assert(results.containsKey('Phone'));
	}
	
	private static testMethod void testIsUnitCostField() {
		Boolean result = QuoteUtils.isUnitCostField(Schema.SObjectType.SBQQ__QuoteLine__c.fields.SBQQ__UnitCost__c.getName());
		system.assert(result);
		
		result = QuoteUtils.isUnitCostField(Schema.SObjectType.Account.fields.Name.getName());
		system.assertEquals(false, result);
	}
	
	private static testMethod void testCopyAdditionalInformationMultiple() {
		SBQQ__ProductOption__c productOption = new SBQQ__ProductOption__c();
		
		String encodedAdditionalInfo = 'ConfigureDialogTitle__c:test;City_of_Use__c:Vancouver';
		
		QuoteUtils.copyAdditionalInformation(productOption, encodedAdditionalInfo);
		
		system.assertEquals('test', productOption.ConfigureDialogTitle__c);
		system.assertEquals('Vancouver', productOption.City_of_Use__c);
	}
	
	private static testMethod void testMapCustomLineFields() {
		SBQQ__QuoteLine__c line = new SBQQ__QuoteLine__c();
		Product2 product = new Product2(SBQQ__CompoundDiscountRate__c = 0.25);
		List<Schema.SObjectField> fields = new List<Schema.SObjectField>{SBQQ__QuoteLine__c.SBQQ__CompoundDiscountRate__c};
		
		QuoteUtils.mapCustomLineFields(line, product, fields);
		
		system.assertEquals(0.25, line.SBQQ__CompoundDiscountRate__c);
	}
	
	private static testMethod void testMapCustomLineFieldsNoDestField() {
		SBQQ__QuoteLine__c line = new SBQQ__QuoteLine__c();
		Product2 product = new Product2(X1_Year__c = 0.25);
		List<Schema.SObjectField> fields = new List<Schema.SObjectField>{Product2.X1_Year__c};
		
		QuoteUtils.mapCustomLineFields(line, product, fields);
	}
	
	private static testMethod void testMapCustomLineFieldsLookup() {
		SBQQ__DiscountSchedule__c dis = new SBQQ__DiscountSchedule__c();
		insert dis;
		
		SBQQ__QuoteLine__c line = new SBQQ__QuoteLine__c();
		Product2 product = new Product2(SBQQ__DiscountSchedule__c = dis.id);
		List<Schema.SObjectField> fields = new List<Schema.SObjectField>{SBQQ__QuoteLine__c.SBQQ__DiscountSchedule__c};
		
		QuoteUtils.mapCustomLineFields(line, product, fields);
		
		system.assertEquals(dis.id, line.SBQQ__DiscountSchedule__c);
	}
	
	private static testMethod void testGroupByField() {
		List<SObject> records = new List<SObject>{
			new Account(name = 'test1'),
			new Account(name = 'test1'),
			new Account(name = 'test2')
		};
		
		Map<String,List<SObject>> results = QuoteUtils.groupByField(records, Account.name);
		
		system.assertEquals(2, results.get('test1').size());
		system.assertEquals(1, results.get('test2').size());
	}
	
	private static testMethod void testToObject() {
		Object result = QuoteUtils.toObject(Opportunity.name, 'test');
		system.assert(result instanceOf String);
		
		result = QuoteUtils.toObject(Opportunity.amount, '3.5');
		system.assert(result instanceOf Decimal);
		
		result = QuoteUtils.toObject(Opportunity.closedate, '2014-01-01');
		system.assert(result instanceOf Date);
		
		result = QuoteUtils.toObject(Opportunity.Appointment_Date__c, '2001-01-01 21:32:52');
		system.assert(result instanceOf Datetime);
	}
	
	private static testMethod void testGetCurrencyCode() {
		Account a = new Account(name = 'test', CurrencyIsoCode = 'CAD');
		
		String result = QuoteUtils.getCurrencyCode(a);
		
		system.assertEquals('CAD', result);
	}
	
	private static testMethod void testSetCurrencyCode() {
		Account a = new Account(name = 'test');
		
		QuoteUtils.setCurrencyCode(a, 'CAD');
		
		system.assertEquals('CAD', a.CurrencyIsoCode);
	}
	
	private static testMethod void testGetInstanceName() {
		String result = QuoteUtils.getInstanceName();
		
		system.assertNotEquals(null, result);
	}
	
	private static testMethod void testToServerity() {
		ApexPages.Severity result = QuoteUtils.toSeverity('Info');
		system.assertEquals(ApexPages.Severity.INFO, result);
		
		result = QuoteUtils.toSeverity('Error');
		system.assertEquals(ApexPages.Severity.ERROR, result);
		
		result = QuoteUtils.toSeverity('Warning');
		system.assertEquals(ApexPages.Severity.WARNING, result);
		
		result = QuoteUtils.toSeverity('Fatal');
		system.assertEquals(ApexPages.Severity.FATAL, result);
		
		result = QuoteUtils.toSeverity('Confirm');
		system.assertEquals(ApexPages.Severity.CONFIRM, result);
		
		result = QuoteUtils.toSeverity('Invalid');
		system.assertEquals(ApexPages.Severity.INFO, result);
	}
	
	private static testMethod void testToHtml() {
		String result = QuoteUtils.toHTML('"test"\n');
		
		system.assertEquals('&quot;test&quot;<br>', result);
	}
	
	private static testMethod void testGetDeveloperPrefix() {
		String result = QuoteUtils.getDeveloperPrefix();
		
		system.assertNotEquals(null, result);
	}
 	
}