public without sharing class CreditAssessmentRedirectController {

    private final Credit_Profile__c creditProfile;
    private final Account account;
    private final Credit_Assessment__c creditAssessment;
    
    public Flow.Interview.Credit_Create_New_Assessment createCARFlow {get; set;}
    public Flow.Interview.Privacy_Policy_Review privacyFlow {get; set;}  
    public String outputCARID {get;set;}
  public String outputCaseID {get;set;}
    public String outputLegalEntityType {get;set;}
    public String selectedLegalEntityType {get;set;}
  
  /*start -  TBO Request(Record type) creating credit assessment through case */
    public String paramCaseID{
      get {
        System.Debug('Get paramCaseID: ' + ApexPages.currentPage().getParameters().get('paramCaseID'));
        return ApexPages.currentPage().getParameters().get('paramCaseID');
      }
      set;
    }
    public String paramRCID{
      get {
        System.Debug('Get paramRCID: ' + ApexPages.currentPage().getParameters().get('paramRCID'));
        return ApexPages.currentPage().getParameters().get('paramRCID');
      }
      set;
    }
  /*End -  TBO Request(Record type) creating credit assessment through case */
  
    public String paramAccountID{
      get {
        System.Debug('Get paramAccountID: ' + ApexPages.currentPage().getParameters().get('paramAccountID'));
        return ApexPages.currentPage().getParameters().get('paramAccountID');
      }
      set;
    }

    public String paramUserAlias{
      get {
        System.Debug('Get paramUserAlias: ' + ApexPages.currentPage().getParameters().get('paramUserAlias'));
        return ApexPages.currentPage().getParameters().get('paramUserAlias');
      }
      private set;
    }

    public String paramCARType{
      get {
        System.Debug('Get paramCARType: ' + ApexPages.currentPage().getParameters().get('paramCARType'));
        return ApexPages.currentPage().getParameters().get('paramCARType');
      }
      private set;
    }

    public String paramOppID{
      get {
        System.Debug('Get paramOppID: ' + ApexPages.currentPage().getParameters().get('paramOppID'));
        return ApexPages.currentPage().getParameters().get('paramOppID');
      }
      private set;
    }
     public String paramOrderID{
      get {
        system.debug('OCOM_CreditAssessmentRedirectCtrl::paramOrderID(): ' + ApexPages.currentPage().getParameters().get('paramOrderID'));
        return ApexPages.currentPage().getParameters().get('paramOrderID');
      }
      private set;
    }

    public String paramCBCBypass{
      get {
        System.Debug('Get paramCBCBypass: ' + ApexPages.currentPage().getParameters().get('paramCBCBypass'));
        return ApexPages.currentPage().getParameters().get('paramCBCBypass');
      }
      private set;
    }

    public String paramPPBypass{
      get {
        System.Debug('Get paramPPBypass: ' + ApexPages.currentPage().getParameters().get('paramPPBypass'));
        return ApexPages.currentPage().getParameters().get('paramPPBypass');
      }
      private set;
    }

    public String paramLegalEntityType{
      get {
        System.Debug('Get paramLegalEntityType: ' + ApexPages.currentPage().getParameters().get('paramLegalEntityType'));
        return ApexPages.currentPage().getParameters().get('paramLegalEntityType');
      }
      private set;
    }

    public CreditAssessmentRedirectController(ApexPages.StandardController stdController) {
        system.debug('....................... Controller Start .......................');

        this.account = (Account)stdController.getRecord();
        this.creditProfile= [Select ID, name from credit_Profile__c where Account__c = :ApexPages.currentPage().getParameters().get('paramAccountID') LIMIT 1];
        String inputCARID = ApexPages.currentPage().getParameters().get('paramCARID');
    /*start -  TBO Request(Record type) add field case__c,Outgoing_Customer_RCID__c to creating credit assessment through case */
    /*end -  TBO Request(Record type) creating credit assessment through case */
        if (inputCARID != null) {
            this.creditAssessment= [Select ID,case__c, name, car_status__c, CAR_Result__c,Outgoing_Customer_RCID__c, Number_of_Times_Submitted__c 
                                    from credit_Assessment__c 
                                    where id = :inputCARID LIMIT 1];            
        }            
    }

    public PageReference redirect(){
        /* PageReference for an autolaunch flow */
        system.debug('....................... Create CAR Redirect Start .......................');

        Map<String, Object> inputMap = new Map<String, Object>();

        String inputOperation = ApexPages.currentPage().getParameters().get('operation');
        //If the value in operation is "Submit", then the status of the CAR should be set accordingly

        if (inputOperation==null) {
            String inputCARType = ApexPages.currentPage().getParameters().get('paramCARType');
            String inputOppID = ApexPages.currentPage().getParameters().get('paramOppID');
             String inputOrderID = ApexPages.currentPage().getParameters().get('paramOrderID');
       /*start -  TBO Request(Record type) creating credit assessment through case */
            String inputCaseID = ApexPages.currentPage().getParameters().get('paramCaseID');
            String inputRCID = ApexPages.currentPage().getParameters().get('paramRCID');
      /*end -  TBO Request(Record type) creating credit assessment through case */
            String inputLegalEntityType = ApexPages.currentPage().getParameters().get('paramLegalEntityType');
            String inputCreditBureauCheckRequest = ApexPages.currentPage().getParameters().get('outCreditBureauCheckRequest');
            String inputResidentialAddress = ApexPages.currentPage().getParameters().get('outResidentialAddress');

            Credit_ID_mapping__c Credit_Field_IDs = Credit_ID_Mapping__c.getInstance('Credit Product');
            
            if (Credit_Field_IDs==null) return null;
            
            String inputProductFieldID = Credit_Field_IDs.Field_ID__c;
            String inputMRCRecordTypeID =Schema.SObjectType.Credit_Related_Product__c.getRecordTypeInfosByName().get(Label.MRC).getRecordTypeId();
            String inputNRCRecordTypeID =Schema.SObjectType.Credit_Related_Product__c.getRecordTypeInfosByName().get(Label.NRC).getRecordTypeId();
        
            System.debug('CAR Input: inputCARType         : ' + inputCARType);
            System.debug('CAR Input: inputCPID            : ' + creditProfile.id);
            System.debug('CAR Input: inputOppID           : ' + inputOppID);
            System.debug('CAR Input: inputOrderID         : ' + inputOrderID);
      /*start -  TBO Request(Record type) creating credit assessment through case */
            System.debug('CAR Input: inputCaseID          : ' + inputCaseID);
            System.debug('CAR Input: inputRCID            : ' + inputRCID);
      /*end -  TBO Request(Record type) creating credit assessment through case */
            System.debug('CAR Input: inputProductFieldID  : ' + inputProductFieldID);
            System.debug('CAR Input: inputMRCRecordTypeID : ' + inputMRCRecordTypeID);
            System.debug('CAR Input: inputNRCRecordTypeID : ' + inputNRCRecordTypeID);
            System.debug('CAR Input: inputLegalEntityType : ' + inputLegalEntityType);

            System.debug('CAR Input: inputCreditBureauCheckRequest: ' + inputCreditBureauCheckRequest);
            System.debug('CAR Input: inputResidentialAddress      : ' + inputResidentialAddress);

            inputMap.put('paramCARType', inputCARType);
            inputMap.put('paramCPID', creditProfile.id);
            inputMap.put('paramOppID', inputOppID);
             inputMap.put('paramOrderID', inputOrderID);
       /*start -  TBO Request(Record type) creating credit assessment through case */
            inputMap.put('paramCaseID', inputCaseID);
            inputMap.put('paramRCID', inputRCID);
      /*end -  TBO Request(Record type) creating credit assessment through case */
            inputMap.put('paramProductFieldID', inputProductFieldID);
            inputMap.put('paramMRCRecordTypeID', inputMRCRecordTypeID);
            inputMap.put('paramNRCRecordTypeID', inputNRCRecordTypeID);
            inputMap.put('paramLegalEntityType', inputLegalEntityType);
            inputMap.put('paramCreditBureauCheckRequest', inputResidentialAddress);
            inputMap.put('paramResidentialAddress', inputResidentialAddress);

            createCARFlow = new Flow.Interview.Credit_Create_New_Assessment(inputMap);
            createCARFlow.start();

            outputCARID = (String)createCARFlow.getVariableValue('varAssessmentID');
           
            System.debug('CAR Output: outputCARID          : ' + outputCARID);
            System.debug('CAse ID == Output: outputCaseID          : ' + createCARFlow);

        } else {
            if (inputOperation =='Submit') {
                
                creditAssessment.OwnerId = CreditWebServiceClientUtil.getQueueId('Credit Assessment Team');                              
                
                if (creditAssessment.Number_of_Times_Submitted__c==null) creditAssessment.Number_of_Times_Submitted__c=1;
                else creditAssessment.Number_of_Times_Submitted__c++;
                
                if (creditAssessment.CAR_Status__c == 'Completed' && creditAssessment.CAR_Result__c == 'Denied') {
                   creditAssessment.CAR_Status__c = 'Queued';
                   creditAssessment.CAR_Code__c = 'Resubmitted: New Information Provided';
                } else {                 
                   creditAssessment.CAR_Status__c = 'Submitted';
                }
                try {
                   update creditAssessment;
                }
                catch (DMLException e) {
                    System.debug('Error in updating Credit Assessment: ' + e.getMessage());
                }
                outputCARID=creditAssessment.ID;
            }
        }
        
         PageReference p;
         if (outputCARID == null) 
             p=new PageReference('/'+ creditProfile.id);
         else
             p=new PageReference('/'+ outputCARID);
             
         p.setRedirect(true);
         return p;
            
    }
    
    public PageReference getPolicyRedirect(){
        /* PageReference for an autolaunch flow */
        system.debug('....................... Privacy Policy Start .......................');
        String inputAccountID = ApexPages.currentPage().getParameters().get('paramAccountID');
        String inputCARType = ApexPages.currentPage().getParameters().get('paramCARType');
        String inputOppID = ApexPages.currentPage().getParameters().get('paramOppID');
        String inputOrderID = ApexPages.currentPage().getParameters().get('paramOrderID');
    /*start -  TBO Request(Record type) creating credit assessment through case */
        String inputCaseID = ApexPages.currentPage().getParameters().get('paramCaseID');
        String inputRCID = ApexPages.currentPage().getParameters().get('paramRCID');      
    /*end -  TBO Request(Record type) creating credit assessment through case */
        String inputUserAlias = ApexPages.currentPage().getParameters().get('paramUserAlias');
        String inputCBCBypass = ApexPages.currentPage().getParameters().get('paramCBCBypass');
        String inputPPBypass = ApexPages.currentPage().getParameters().get('paramPPBypass');
        String inputLegalEntityType = ApexPages.currentPage().getParameters().get('paramLegalEntityType');

        System.debug('PP Input: inputAccountID         : ' + inputAccountID);
        System.debug('PP Input: inputCARType           : ' + inputCARType);
        System.debug('PP Input: inputOppID             : ' + inputOppID);
        System.debug('PP Input: inputOrderID           : ' + inputOrderID);
    /*start -  TBO Request(Record type) creating credit assessment through case */
        System.debug('PP Input: inputCaseID             : ' + inputCaseID);
        System.debug('PP Input: inputRCID             : ' + inputRCID);
    /*end -  TBO Request(Record type) creating credit assessment through case */
        System.debug('PP Input: inputUserAlias         : ' + inputUserAlias);
        System.debug('PP Input: inputCBCBypass         : ' + inputCBCBypass);
        System.debug('PP Input: inputPPBypass          : ' + inputPPBypass);

        System.debug('PP Input: inputLegalEntityType   : ' + inputLegalEntityType);
        System.debug('PP Input: privacyFlow            : ' + privacyFlow);

        //System.debug('PP Output: varLegalEntityType     : ' + (String) privacyFlow.getVariableValue('varLegalEntityType'));
        //System.debug('PP Output: selectedLegalEntityType: ' + (String) privacyFlow.getVariableValue('selectedLegalEntityType'));
        //System.debug('PP Output: selectedLegalEntityType: ' + privacyFlow.SelectedLegalEntityType);

        String outLegalEntityType=null;

        if (privacyFlow!=null) {
            outLegalEntityType = privacyFlow.SelectedLegalEntityType;
            //outLegalEntityType = ApexPages.currentPage().getParameters().get('paramLegalEntityType');

            if (outLegalEntityType=='null') {
                outputLegalEntityType=''; 
            } else {
                outputLegalEntityType=CreditReferenceCOde.getLegalEntityTypeCd(outLegalEntityType);
                System.debug('.........param: selectedLegalEntityType          : ' + outLegalEntityType);            
            }
            System.debug('.........param: selectedLegalEntityType          : ' + outputLegalEntityType);            
        }
         PageReference p;
     
        //Samir - Fixed URL redirect issue on Create Credit profile button click for delear url should prefix with Partner
     	String url = '/apex/CreditAssessmentRedirect?paramAccountID='+ inputAccountID+'&paramCaseID='+inputCaseID+'&paramRCID='+inputRCID+'&paramUserAlias='+inputUserAlias+'&paramCARType='+inputCARType+'&paramOppID='+inputOppId+'&paramOrderID='+inputOrderID+'&paramCBCBypass='+inputCBCBypass+'&paramLegalEntityType='+ outputLegalEntityType;
        String sitePrefix=Site.getPathPrefix();
        system.debug('Site.prefix:'+sitePrefix);
        if(String.isNotBlank(sitePrefix)){
        	url = sitePrefix+url;
         }
         system.debug       (url);
   	     p=new PageReference(url);             
         p.setRedirect(true);
         return p;
            
    }    
}