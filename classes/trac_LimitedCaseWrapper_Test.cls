@isTest(seeAllData = false)
public with sharing class trac_LimitedCaseWrapper_Test {
	public static testMethod void testLimitedViewCaseWrapper(){
		
		Case testCase1 = new Case(	SuppliedEmail = 'test1@example.com',
									Status = 'New',
									Subject = 'test');
									
		Case testCase2 = new Case(	SuppliedEmail = 'test2@example.com',
									Status = 'New',
									Subject = 'test');
									
		Case testCase3 = new Case(	SuppliedEmail = 'test3@example.com',
									Status = 'New',
									Subject = 'test');
		
		insert new Case[] {testCase1,testCase2,testCase3};
		
		List<Case> tempCases1 = new List<Case>(); 
		
		tempCases1 = [	SELECT CaseNumber, SuppliedEmail, Status, Subject, CreatedDate 
						FROM Case];
		
		List<LimitedViewCaseWrapper> testWrappers = new List<LimitedViewCaseWrapper>();
		
		for(Case cs : tempCases1){
			testWrappers.add(new LimitedViewCaseWrapper(cs));
		}
		
		testWrappers.sort();
		
		Case testCase4 = new Case(	SuppliedEmail = 'test4@example.com',
									Status = 'New',
									Subject = 'test');
									
		Case testCase5 = new Case(	SuppliedEmail = 'test5@example.com',
									Status = 'New',
									Subject = 'test');
									
		Case testCase6 = new Case(	SuppliedEmail = 'test6@example.com',
									Status = 'New',
									Subject = 'test');
									
		List<Case> tempCases2 = new List<Case>(); 					
		tempCases2 = [	SELECT CaseNumber, SuppliedEmail, Status, Subject, CreatedDate 
						FROM Case
						WHERE 	SuppliedEmail like '%4%' or
								SuppliedEmail like '%5%' or
								SuppliedEmail like '%6%'  ];
								
		insert new Case[] {testCase4, testCase5, testCase6};
		for(Case cs : tempCases2){
			testWrappers.add(new LimitedViewCaseWrapper(cs));
		}
		
		testWrappers.sort();
	}
}