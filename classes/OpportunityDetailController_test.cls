/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * Name - OpportunityDetailController_test
 * 1. Created By - Sandip - 06 Feb 2016
 * 2. Modified By 
 */
@isTest
private class OpportunityDetailController_test{
    static testMethod void runTestCase() {
        test.startTest();
        
        Account acc = new Account(Name='Testing Software', BillingCountry = 'US', BillingState = 'IL');
        insert acc;
        
        Opportunity oppt = new Opportunity();
        oppt.AccountId = acc.Id;
        oppt.Name = 'Opportunity for ' + acc.Name;
        oppt.CloseDate = Date.today();
        oppt.StageName = 'I have a Potential Opportunity (20%)';
        oppt.Order_Type__c = 'All Other Orders' ;
        oppt.City__c = 'Markham';
        oppt.State_Province__c = 'ON';
        oppt.Country__c = 'CAN';
        oppt.Postal_Code__c = 'M1T 3N3';
        oppt.Suite_Unit__c = '703';
        oppt.Street__c = 'Carabob';
        oppt.Street_Direction__c = 'N';
        insert oppt;
        
        PageReference page = new PageReference('/apex/OpportunityDetail'); 
        page.getParameters().put('Id', Oppt.Id);
        Test.setCurrentPage(page);
        ApexPages.standardController controller = new ApexPages.standardController(Oppt);
        OpportunityDetailController obj = new OpportunityDetailController (controller);
        obj.updateContractBillingAddress();
        test.stopTest();             
    }
    
}