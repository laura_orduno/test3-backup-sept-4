public with sharing class LegacyOrder_AsyncJob implements System.Queueable {
    
    private final String sessionId;

    public id caseOwnerId;
    public id taskOwnerId;
    
    public RecordType caseRecordType;
    public RecordType taskRecordType;
    public RecordType accountRecordType;
    public RecordType orderRecordTypeDefault;
    public map<string,RecordType> sourceSystemToRecordTypeMap;
    public list<string> HoldCodesToCaseList;
    
    public string HoldCodesCaseRecordType;
    public string HoldCodesOwnerGroup;
    public string HoldCodesTaskRecordType;
    public string HoldCodesToCase;
    public string HoldCodesDefaultOwner;
    public string HoldCodesLegacyOrderSourceSystems;
    public string HoldCodesOrderRecordTypeDefault;
    
    public List<OrderStatusUpdates_ComplexTypes.LineItem> legacyOrdersList;

    // pair container for Legacy_Order and Case
    public class LegacyOrderCase {
        Legacy_Order__c orderRec;
        Case caseRec;
        LegacyOrderCase(Legacy_Order__c orderObj,Case caseObj) {
            orderRec = orderObj;
            caseRec = caseObj;
        }
    }

    // Constructor
    public LegacyOrder_AsyncJob(String sid, List<OrderStatusUpdates_ComplexTypes.LineItem> legacyOrders) {
        sessionId = sid;
        legacyOrdersList = legacyOrders;
        
        caseRecordType = new RecordType();
        taskRecordType = new RecordType();
        orderRecordTypeDefault = new RecordType();
        sourceSystemToRecordTypeMap = new map<string,RecordType>();
        HoldCodesToCaseList = new list<string>();
    }
    
    // Asynchronous execution
    public void execute(System.QueueableContext ctx) {
        initialize();
        system.debug('execute:legacyOrdersList.size() = '+legacyOrdersList.size());
        reconcileLegacyOrdersBatch();
    }

    public void initialize(){
        
        system.debug('initialize():intializing class');
        
        try{
            for(Hold_Codes_Settings__mdt itm: [select DeveloperName , value__c from Hold_Codes_Settings__mdt]){
                if(itm.DeveloperName.equalsIgnoreCase('Case_Record_Type'))
                    HoldCodesCaseRecordType = itm.value__c;
                else if(itm.DeveloperName.equalsIgnoreCase('Default_Owner'))
                    HoldCodesDefaultOwner = itm.value__c;
                else if(itm.DeveloperName.equalsIgnoreCase('Group_Queue'))
                    HoldCodesOwnerGroup = itm.value__c;
                else if(itm.DeveloperName.equalsIgnoreCase('Hold_Code_To_Case'))
                    HoldCodesToCase = itm.value__c;
                else if(itm.DeveloperName.equalsIgnoreCase('Legacy_Order_Record_Type_Default'))
                    HoldCodesOrderRecordTypeDefault = itm.value__c;
                else if(itm.DeveloperName.equalsIgnoreCase('Legacy_Order_Source_Systems'))
                    HoldCodesLegacyOrderSourceSystems = itm.value__c;
                else if(itm.DeveloperName.equalsIgnoreCase('Task_Record_Type'))
                    HoldCodesTaskRecordType = itm.value__c;
                
                system.debug('initialize():Loading Custom Metadata Types: '+ itm.DeveloperName  + ' = '+itm.value__c);
            }
            if(HoldCodesToCase != null)
                HoldCodesToCaseList.addAll(HoldCodesToCase.split('\\W\\s*'));
        }
        catch (Exception e) {
            System.debug('Initialize member data: Query custom metadata types: received exception: ' + e.getMessage());
        }

        try{
            // Query the info
            caseRecordType = [select Id, DeveloperName from RecordType where SObjectType = 'Case' and DeveloperName = : HoldCodesCaseRecordType and IsActive = true limit 1];
        }
        catch (Exception e) {
            System.debug('Initialize member data: Query Case record type: received exception: ' + e.getMessage());
        }
        try{
            taskRecordType = [select Id, DeveloperName from RecordType where SObjectType = 'Task' and DeveloperName = : HoldCodesTaskRecordType and IsActive = true limit 1];
        }
        catch (Exception e) {
            System.debug('Initialize member data: Query Task record type: received exception: ' + e.getMessage());
        }
        try{
            accountRecordType = [select Id, DeveloperName from RecordType where SObjectType = 'Account' and DeveloperName = : 'RCID' and IsActive = true limit 1];
        }
        catch (Exception e) {
            System.debug('Initialize member data: Query Account RCID record type: received exception: ' + e.getMessage());
        }
            
        loadOrderRecordTypes();
      
        User ownerDefault = new User();
        Group ownerGroup = new Group();

        try{
            ownerDefault = [select Id from User where name = : HoldCodesDefaultOwner limit 1];
            if(ownerDefault != null){
                taskOwnerId = ownerDefault.Id;
            }
            else{
                system.debug('Cannot find user ID for HoldCodesDefaultOwner: '+HoldCodesDefaultOwner);
                taskOwnerId = id.valueof('');
            }
        }
        catch (Exception e) {
            System.debug('Initialize member data: Query Task Owner: received exception: ' + e.getMessage());
        }

        try{
            ownerGroup = [select Id from group where DeveloperName = : HoldCodesOwnerGroup limit 1];
            if(ownerGroup != null){
                caseOwnerId = ownerGroup.Id;
            }
            else{
                system.debug('Cannot find group ID for HoldCodesOwnerGroup: '+HoldCodesOwnerGroup);
                if(ownerDefault != null)
                    caseOwnerId = ownerDefault.Id;
            }
        }
        catch (Exception e) {
            System.debug('Initialize member data: Query Group/Queue ID: received exception: ' + e.getMessage());
        }
    }

    public void loadOrderRecordTypes(){
        string[] orderSrcSysList = HoldCodesLegacyOrderSourceSystems.split('[,]{1}[\\s]?');
        try{
            list<RecordType> orderRecordTypeList = [select Id, Name from RecordType where SObjectType = 'Legacy_Order__c' and IsActive = true];
            
            // Set the default recordtype
            for(RecordType rtype : orderRecordTypeList){
                if(rtype.Name.containsIgnoreCase(HoldCodesOrderRecordTypeDefault))
                    orderRecordTypeDefault = rtype;
            }
            
            if(!orderSrcSysList.isempty()) {
                for(string orderSrcSys : orderSrcSysList){
                    boolean found = false;
                    for(RecordType rtype : orderRecordTypeList){
                        if(orderSrcSys.containsIgnoreCase(rtype.Name)){
                            found = true;
                            sourceSystemToRecordTypeMap.put(orderSrcSys,rtype);
                        }
                    }
                    if(!found){
                        sourceSystemToRecordTypeMap.put(orderSrcSys,orderRecordTypeDefault);
                    }
                }
            }
        }
        catch (Exception e) {
            System.debug('Initialize member data: Query Legacy_Order__c record types: received exception: ' + e.getMessage());
        }
    }
    
    public RecordType getOrderRecordType(string recordTypeName){
        for(string systemStr : sourceSystemToRecordTypeMap.keyset()){
            if(recordTypeName.containsIgnoreCase(systemStr))
                return sourceSystemToRecordTypeMap.get(systemStr);
        }
        return orderRecordTypeDefault;
    }
    
    public Case createCase(Legacy_Order__c orderObj, string holdCode) {
        
        // check if a case should be created for this hold code
        boolean found = false;
        for(string itm : HoldCodesToCaseList){
            if(itm.equalsIgnoreCase(holdCode))
                found = true;
        }
        if(!found)
            return null;
        
        system.debug('createCase():Creating case for order.Id = '+orderObj.Id+': hold code = '+holdCode);

        Case caseRec = new Case();
        caseRec.RecordTypeId = caseRecordType.id;
        caseRec.Type = 'Hold Codes';
        caseRec.Subject = 'Hold Code (' + holdCode + ')';
        caseRec.Priority = 'Medium';
        caseRec.Case_Creator_Non_Care__c = true;
        caseRec.OwnerId = caseOwnerId;
        caseRec.Legacy_Order__r = orderObj;
        caseRec.Current_Due_Date__c = orderObj.Scheduled_Due_Date__c;

        if(orderObj.Scheduled_Due_Date__c != null)
            caseRec.Customer_requested_Due_Date__c  = orderObj.Scheduled_Due_Date__c;

        return caseRec;
    }

    // Update the Case for Legacy Order
    public Case updateCase(Legacy_Order__c orderObj, string holdCode, string updateType, list<Case> openCases) {
        
        Case caseRec = null;

        for(Case itm:openCases){
            if(itm.Legacy_Order__c == orderObj.Id && itm.Subject.contains('(' + holdCode + ')'))
                caseRec = itm;
        }

        // Update the Case
        if(caseRec != null) {
            system.debug('updateCase():Updating case Id = '+caseRec.Id+' for order.Id = '+orderObj.Id+': hold code = '+holdCode);
            
            if(updateType == 'del')
                caseRec.Subject = 'Hold Code (' + holdCode + '): ' + system.label.Hold_Code_Confirm_Case_Closed; // 'Confirm the case has been closed'
            else if(updateType == 'dup')
                caseRec.Subject = 'Hold Code (' + holdCode + '): ' + system.label.Hold_Code_Verify_Case_Assigned; // 'Verify the case is assigned'
        }
        return caseRec;
    }
    
    // Create a Task on the Case for notification
    public Task notifyCase(Legacy_Order__c orderObj, string holdCode, string updateType, list<Case> openCases) {
        
        Case caseRec = null;
        Task taskRec = null;

        for(Case itm:openCases){
            if(itm.Legacy_Order__c == orderObj.Id && itm.Subject.contains('(' + holdCode + ')'))
                caseRec = itm;
        }

        // Create a new Task
        if(caseRec != null) {
            system.debug('notifyCase():Updating case Id = '+caseRec.Id+' for order.Id = '+orderObj.Id+': hold code = '+holdCode);
            
            taskRec = new Task();
            taskRec.RecordTypeId = taskRecordType.Id;
            taskRec.Priority = 'Normal';
            taskRec.Status = 'Not Started';
            taskRec.WhatId = caseRec.Id;
            if(updateType == 'del')
                taskRec.Subject = 'Order ('+orderObj.Order_Number__c+'): Hold Code (' + holdCode + '): ' + system.label.Hold_Code_Confirm_Case_Closed;  // 'Confirm the case has been closed'
            else if(updateType == 'dup')
                taskRec.Subject = 'Order ('+orderObj.Order_Number__c+'): Hold Code (' + holdCode + '): ' + system.label.Hold_Code_Update_Open_Case; // 'Update received on open case'

            if(caseRec.Owner.Type == 'User')
                taskRec.OwnerId = caseRec.OwnerId;
            else
                taskRec.OwnerId = taskOwnerId;
        }
        else{
            system.debug('notifyCase(): No open case found for order.Id = '+orderObj.ID+' : received update to hold code = '+holdCode);
        }
        return taskRec;
    }
    
    // return map<holdCode,updateType> : updateType = 'add', 'del' or 'dup'
    public map<string,string> compareCodes(string oldHoldCodes, string newHoldCodes){
        map<string,string> resultsMap = new map<string,string>();
        
        // hold codes are delimited by commas (but supports any non-alphanumeric delimiter)
        list<string> oldCodeList = new list<string>();
        list<string> newCodeList = new list<string>();

        if(!string.isBlank(oldHoldCodes))
            oldCodeList.addAll(oldHoldCodes.split('\\W\\s*'));
        if(!string.isBlank(newHoldCodes))
            newCodeList.addAll(newHoldCodes.split('\\W\\s*'));
        
        // check for new codes
        for(string newItm : newCodeList){
            boolean found = false;
            for(string oldItm : oldCodeList){
                if(newItm.equalsIgnoreCase(oldItm))
                    found = true;
            }
            if(!found){
                resultsMap.put(newItm,'add');
            }
        }

        // check for deleted or duplicate codes
        for(string oldItm : oldCodeList){
            boolean found = false;            
            for(string newItm : newCodeList){
                if(oldItm.equalsIgnoreCase(newItm)){
                    found = true;
                    resultsMap.put(oldItm,'dup');
                }
            }
            if(!found){
                resultsMap.put(oldItm,'del');
            }
        }        
        return resultsMap;
    }
    
    public Legacy_Order__c findOrder(string legacyOrderId, List<Legacy_Order__c> legacyOrderCache){
        for(Legacy_Order__c itm:legacyOrderCache){
            if(itm.Order_Number__c.equals(legacyOrderId)){
                system.debug('findOrder:found order number = ' + legacyOrderId);
                return itm;
            }
        }
        return null;
    }

    public Account findAccount(string rcidStr, List<Account> accountCache){
        for(Account itm:accountCache){
            if(itm.RCID__c != null && itm.RCID__c.equals(rcidStr)){
                system.debug('findAccount:found order Account RCID = ' + rcidStr);
                return itm;
            }
        }
        return null;
    }

    // Process the records in batches based on the source system (billing)
    public void reconcileLegacyOrdersBatch(){
        
        map<string,integer> orderIdMap = new map<string,integer>();
        
        map<string,list<OrderStatusUpdates_ComplexTypes.LineItem> > sourceSystemMap = 
            new map<string,list<OrderStatusUpdates_ComplexTypes.LineItem> >();
        
        string defaultBillingSystem = 'undefined';
        
        system.debug('reconcileLegacyOrdersBatch:legacyOrdersList.size() = '+legacyOrdersList.size());
        
        // pre-process order numbers and sequence number - only update the order for the highest seq number
        for(OrderStatusUpdates_ComplexTypes.LineItem itm : legacyOrdersList){

            if(itm.LegacyOrderID != null && itm.IssueDate != null && !string.isBlank(itm.LegacyOrderID) && !string.isBlank(itm.IssueDate)){

                integer newSeq = 0;
                if(itm.Sequence != null && !string.isBlank(itm.sequence) && itm.Sequence.isNumeric())
                    newSeq = integer.valueOf(itm.Sequence);

                if(orderIdMap.containsKey(itm.LegacyOrderID)) {
                    if(newSeq > orderIdMap.get(itm.LegacyOrderID))
                        orderIdMap.put(itm.LegacyOrderID,newSeq);
                }
                else {
                    orderIdMap.put(itm.LegacyOrderID,newSeq);
                }
            }
        }
        
        set<string> orderIdsAdded = new set<string>(); // keep track of IDs already added to avoid duplicates
        
        // collect the records for each source system
        for(OrderStatusUpdates_ComplexTypes.LineItem itm : legacyOrdersList){
            
            integer newSeq = 0;
            if(itm.Sequence != null && !string.isBlank(itm.sequence) && itm.Sequence.isNumeric())
                newSeq = integer.valueOf(itm.Sequence);

            // only add orders that are in the orderIdMap
            if(orderIdMap.containsKey(itm.LegacyOrderID) && orderIdMap.get(itm.LegacyOrderID) == newSeq){
                if(sourceSystemMap.containsKey(itm.BillingSystem)) {
                    if(orderIdsAdded.add(itm.LegacyOrderID))
                        sourceSystemMap.get(itm.BillingSystem).add(itm);
                }
                else {
                    if(itm.BillingSystem == null || string.isBlank(itm.BillingSystem)){
                        if(sourceSystemMap.containsKey(defaultBillingSystem)) {
                            if(orderIdsAdded.add(itm.LegacyOrderID))
                                sourceSystemMap.get(defaultBillingSystem).add(itm);
                        }
                        else {
                            orderIdsAdded.add(itm.LegacyOrderID);
                            sourceSystemMap.put(defaultBillingSystem,new list<OrderStatusUpdates_ComplexTypes.LineItem> {itm});
                        }
                    }
                    else {
                        orderIdsAdded.add(itm.LegacyOrderID);
                        sourceSystemMap.put(itm.BillingSystem,new list<OrderStatusUpdates_ComplexTypes.LineItem> {itm});
                    }
                }
            }
        }
        
        system.debug('reconcileLegacyOrdersBatch:sourceSystemMap.size() = '+sourceSystemMap.size());

        // process the orders in batches
        for(string systemName : sourceSystemMap.keyset()){
            List<OrderStatusUpdates_ComplexTypes.LineItem> orderList = sourceSystemMap.get(systemName);
            system.debug('reconcileLegacyOrdersBatch:source system name: '+systemName+' orderList.size() = '+orderList.size());

            if(!orderList.isEmpty())
                reconcileLegacyOrders(systemName,orderList);
        }
    }
    
    public void reconcileLegacyOrders(string systemName, List<OrderStatusUpdates_ComplexTypes.LineItem> legacyOrders){
        
        set<string> orderIdSet = new set<string>();
        set<string> accountRCIDSet = new set<string>();
        list<Legacy_Order__c> newOrderList = new list<Legacy_Order__c >();
        list<Legacy_Order__c> legacyOrderCache = new list<Legacy_Order__c >();
        list<LegacyOrderCase> orderCaseList = new list<LegacyOrderCase>();
        list<Case> openCases = new list<Case>();
        list<Task> taskList = new list<Task>();
        list<Account> accountCache = new list<Account>();
        
        system.debug('reconcileLegacyOrders:legacyOrders.size() = '+legacyOrders.size());
        
        // collect the order numbers and account RCIDs
        for(OrderStatusUpdates_ComplexTypes.LineItem itm:legacyOrders){
            orderIdSet.add(itm.LegacyOrderID);
            if(string.isNotblank(itm.RCID))
                accountRCIDSet.add(itm.RCID);
        }
        
        system.debug('reconcileLegacyOrders:orderIdSet.size() = ' + orderIdSet.size());

        RecordType systemRecordType = getOrderRecordType(systemName);
        
        system.debug('reconcileLegacyOrders:systemRecordType.name = ' + systemRecordType.name);

        // load the cache
        try{
            legacyOrderCache = [select ID, RecordTypeId, Name, Order_Number__c, Issue_Date__c, Sequence_Number__c, Hold_Code__c
                                from Legacy_Order__c where RecordTypeId = :systemRecordType.id and Order_Number__c in : orderIdSet];
        }
        catch (Exception e) {
            System.debug('reconcileLegacyOrders(): Query for legacyOrderCache: received exception: ' + e.getMessage());
        }

        // load related RCID accounts
        try{
            accountCache = [select ID, RCID__c from Account where RecordTypeId = :accountRecordType.id and RCID__c in : accountRCIDSet];
        }
        catch (Exception e) {
            System.debug('reconcileLegacyOrders(): Query for accountRCIDSet: received exception: ' + e.getMessage());
        }

        system.debug('reconcileLegacyOrders:legacyOrderCache.size() = '+legacyOrderCache.size());
        system.debug('reconcileLegacyOrders:accountCache.size() = '+accountCache.size());

        // Get a list of open cases related to legacy orders
        try{
            openCases = [select ID, RecordTypeId, Status, Subject, Legacy_Order__c, Legacy_Order__r.Order_Number__c, OwnerId, Owner.Type
                         from Case where RecordTypeId = :caseRecordType.id and Status != 'Closed' and Status != 'Cancelled'
                         and Legacy_Order__r.Order_Number__c in : orderIdSet];
        }
        catch (Exception e) {
            System.debug('reconcileLegacyOrders(): Query for openCases: received exception: ' + e.getMessage());
        }

        system.debug('reconcileLegacyOrders:openCases.size() = '+openCases.size());

        for(OrderStatusUpdates_ComplexTypes.LineItem orderItm:legacyOrders)
        {
            date issueDate;
            string seqNum = orderItm.Sequence == null? '0' : orderItm.Sequence;
            string legacyOrderName = orderItm.LegacyOrderID + '-' + seqNum;
            
            if(string.isNotblank(orderItm.IssueDate)){
                issueDate = Date.valueOf(orderItm.IssueDate);
                legacyOrderName += '-' + issueDate.year() + '-' + issueDate.month() + '-' + issueDate.day();
            }
            
            Legacy_Order__c newObj = findOrder(orderItm.LegacyOrderID,legacyOrderCache);

            if(newObj == null) {
                if(string.isBlank(orderItm.CRIS3HoldCode))
                    continue;

                newObj = new Legacy_Order__c();
                newObj.RecordTypeId = systemRecordType.id;
                newObj.Name = legacyOrderName;
                system.debug('reconcileLegacyOrders:findOrder() returned null:legacyOrderName = '+legacyOrderName);
            }
            else{
                system.debug('reconcileLegacyOrders:findOrder() returned found:legacyOrderName = '+legacyOrderName);
            }

            newObj.Issue_Date__c = issueDate;
            newObj.Sequence_Number__c = seqNum;

            if(string.isNotblank(orderItm.LegacyOrderID))
                newObj.Order_Number__c = orderItm.LegacyOrderID;
            if(string.isNotBlank(orderItm.BillingAddress))
                newObj.Billing_Address__c = orderItm.BillingAddress;
            if(string.isNotblank(orderItm.BTN))
                newObj.BTN__c = orderItm.BTN;
            if(string.isNotblank(orderItm.OriginatorID))
                newObj.Originator_ID__c  = orderItm.OriginatorID;
            if(string.isNotblank(orderItm.CustomerName))
                newObj.Customer_Name__c = orderItm.CustomerName;
            if(string.isNotblank(orderItm.CRIS3OrderStatus))
                newObj.Order_Status__c = orderItm.CRIS3OrderStatus;
            if(string.isNotblank(orderItm.ServiceAddress))
                newObj.Service_Address__c = orderItm.ServiceAddress;
            if(string.isNotblank(orderItm.ServiceCity))
                newObj.Service_City__c = orderItm.ServiceCity;
            if(string.isNotblank(orderItm.ServiceProvince))
                newObj.Service_Province__c = orderItm.ServiceProvince;
            if(string.isNotblank(orderItm.WorkInstallType))
                newObj.Work_Install_Type__c = orderItm.WorkInstallType;
            if(orderItm.DDate != null && orderItm.DDate.DDScheduleDate != null)
                newObj.Scheduled_Due_Date__c = orderItm.DDate.DDScheduleDate;

            if(string.isNotblank(orderItm.RCID)) {
                newObj.RCID__c = orderItm.RCID;
                Account aRec = findAccount(orderItm.RCID,accountCache);
                if(aRec != null)
                    newObj.Account__c = aRec.id;
            }
            
//            if(string.isNotblank(orderItm.BusinessUnit))
//                newObj.Business_Unit__c = ;
            
            if(string.isNotblank(orderItm.CRIS3HoldCode) || string.isNotBlank(newObj.Hold_Code__c)) {
                map<string,string> resultsMap = compareCodes(newObj.Hold_Code__c,orderItm.CRIS3HoldCode);
                
                system.debug('reconcileLegacyOrders:compareCodes() -> resultsMap.size() = '+resultsMap.size());

                for(string codeStr : resultsMap.keySet()){
                    
                    string updateType = resultsMap.get(codeStr);
                    if(updateType == 'add'){
                        Case caseObj = createCase(newObj,codeStr);
                        if(caseObj != null){
                            LegacyOrderCase obj = new LegacyOrderCase(newObj,caseObj);
                            orderCaseList.add(obj);
                        }
                    }
                    else{
                        Case caseObj = updateCase(newObj,codeStr,updateType,openCases);
                        if(caseObj != null){
                            LegacyOrderCase obj = new LegacyOrderCase(newObj,caseObj);
                            orderCaseList.add(obj);
                        }
                        Task taskObj = notifyCase(newObj,codeStr,updateType,openCases);
                        if(taskObj != null)
                            taskList.add(taskObj);
                    }
                }
            }
            newObj.Hold_Code__c = orderItm.CRIS3HoldCode;
            newOrderList.add(newObj);
        }
        
        try{
            if(!newOrderList.isEmpty())
                upsert newOrderList;
        }
        catch(DmlException e) {
            System.debug('Upsert newOrderList: received exception: ' + e.getMessage());
        }

        try{
            if(!orderCaseList.isEmpty()){
                list<Case> caseList = new list<Case>();
                for(LegacyOrderCase ord : orderCaseList){                
                    // update order ID in case record
                    ord.caseRec.Legacy_Order__c = ord.orderRec.Id;
                    caseList.add(ord.caseRec);
                }
                upsert caseList;
            }
        }
        catch(DmlException e) {
            System.debug('Upsert caseList: received exception: ' + e.getMessage());
        }

        try {
            if(!taskList.isEmpty()){
                insert taskList;
            }
        }
        catch(DmlException e) {
            System.debug('Insert taskList: received exception: ' + e.getMessage());
        }
    }
}