@isTest(SeeAllData=true)
public class TestForShowSearchDetails{
    public static testMethod void testMethodForCodeCoverage(){       
       String cbucid='0002229208';
       String rcid='0001362414';
       String strProv='PQ';
       Integer newPageIndex=1;
       PageReference objPage;
       System.currentPagereference().getParameters().put('cbucid','0002229208');
       System.currentPagereference().getParameters().put('strcustomerName','test');
       System.currentPagereference().getParameters().put('strCbucid','0002229208');
       System.currentPagereference().getParameters().put('rcid','0001362414');       
       System.currentPagereference().getParameters().put('strProv','PQ');   
       System.currentPagereference().getParameters().put('strAlpa','PQ');   
       System.currentPagereference().getParameters().put('strIndex','0.0');   
           
           
       ShowDetailForAssignmentProv obj=new ShowDetailForAssignmentProv();
       obj.strProv='BC';
       
       obj.Rcidnumber='0001362414';
       obj.cbucidnumber='0002229208';
     
       obj.customerName='LES EDITIONS CEC';
       obj.cbucidName='QUEBECOR MEDIA INC';
       obj.banOrBcan='LES EDITIONS CEC INC';
       
       obj.selectedTabForSearch='customerName';
       obj.txtForSearch='zzzzzz';
       obj.searchCustomerName();
       obj.txtForSearch='CITY OF VANCOUVER';
       obj.searchCustomerName();
       
       obj.selectedTabForSearch='cbucidName';
       obj.txtForSearch='zzzzzz';
       obj.searchCbucidName();
       obj.txtForSearch='QUEBECOR MEDIA INC';
       obj.searchCbucidName();
       
       obj.selectedTabForSearch='banOrBcan';
       obj.txtForSearch='zzzzzz';
       obj.searchBanOrBcan();
       obj.txtForSearch='LES EDITIONS CEC INC';
       obj.searchBanOrBcan();
       
       obj.selectedTabForSearch='Cbucidnumber';
       obj.txtForSearch='0002229208';
       obj.searchCbucidnumber();
       
       obj.selectedTabForSearch='rcidnumber';
       obj.txtForSearch='0001362414';
       obj.searchRcidnumber();

      
       objPage=obj.firstBtnClick();
       System.assertEquals(null,objPage);
       objPage=obj.previousBtnClick(); 
       System.assertEquals(null,objPage);
       objPage=obj.nextBtnClick(); 
       System.assertEquals(null,objPage);
       objPage=obj.lastBtnClick(); 
       System.assertEquals(null,objPage); 
       newPageIndex=2;         
       obj.getPageNumber();
       obj.getPageSize();
       obj.getTotalPageNumber();
       obj.getPageAccountList();
       
       obj.getSalesList();
       obj.getShowClientServiceCreditList();
       obj.getCustStatusTool();
       obj.getAccountDetail();
       obj.getMsgForSearchResult();
       obj.getMsgForPosition();
       obj.showCustomerDetailWithStatus();
       obj.showSalesDetail(strProv); 

       System.currentPagereference().getParameters().put('strColorStatus','Red');   
       obj.searchCustomerBasedOnStatus();
       System.currentPagereference().getParameters().put('strColorStatus','Yellow');   
       obj.searchCustomerBasedOnStatus();
       System.currentPagereference().getParameters().put('strColorStatus','Green');   
       obj.searchCustomerBasedOnStatus();
       
       objPage=obj.doSearchProv();
       System.assertEquals(new PageReference('/apex/ShowDetailForAssignmentProv').getURL(),objPage.getURL());
       obj.doSearchCbucid();
       obj.showSearchResult();
       obj.searchFromAccountTab(); 
       obj.showAtoZEasyMap();
       obj.showAtoZEasyMapFR();
          
       Map<String, AtoZ_Assignment__c> mapAtoZ=obj.populateNatlAssignments();
       Schema.DescribeFieldResult implMtrxStage = AtoZ_Assignment__c.AtoZ_Field__c.getDescribe();
       List<Schema.PicklistEntry> Ple = implMtrxStage.getPicklistValues();
       
       List<AtoZ_Assignment__c> clientServiceCreditList=[
        Select a.Tel__c, a.Email__c, a.AtoZ_Field__c, a.AtoZ_Contact__r.Id, a.AtoZ_Contact__r.Full_Name__c,
        a.AtoZ_Contact__r.Email__c ,a.AtoZ_Contact__r.Phone__c, a.AtoZ_Contact__r.External_Email__c
        From AtoZ_Assignment__c a
        where a.Prov_Team__c=:strProv
        limit 1
       ];
       
       Map<String, String> assignClientServiceCredit1=obj.assignClientServiceCreditValue(Ple,clientServiceCreditList);
       Map<String, String> assignClientServiceCredit2=obj.assignClientServiceCreditValueNew (mapAtoZ);  
       String strProvSt = 'BC';     
       
       List<Sales_Assignment__c> findSalesList=[
        Select s.Id,s.User__r.Id, s.User__r.Team_TELUS_ID__c, s.Role__c, s.User__r.firstname,s.User__r.lastname, s.User__r.Email,
        s.User__r.Phone,s.User__r.UserRoleID,s.User__r.UserRole.Name, s.Account__r.rcid__c
        From Sales_Assignment__c s
        where s.Account__r.rcid__c='0001362414'
        limit 1
       ];
       
       Map<String, String> showSalesRole=obj.showSalesRoleUserName(findSalesList, strProvSt);  

    }

}