@IsTest
public class SMB_WO_PageRedirector_Controller_Test
{
    public static testMethod void TestNoOpp()
    {
        PageReference pageRef = new PageReference('/apex/SMB_WO_PageRedirector');
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('FOBOOppId', '006f0000004w3IaAAI');
        SMB_WO_PageRedirector_Controller sbmWOPR = new SMB_WO_PageRedirector_Controller();
    }
    
    public static testMethod void TestValidOpp()
    {
        PageReference pageRef = new PageReference('/apex/SMB_WO_PageRedirector');
        Test.setCurrentPage(pageRef);
        
        
            
            Opportunity Opp1 = smb_test_utility.createOpportunity(Null, null, null, false);
            Opp1.StageName = 'Order Request New' ;
            Opp1.Type = 'All Other Orders';
            insert Opp1;
            
        ApexPages.currentPage().getParameters().put('FOBOOppId', String.ValueOf(Opp1.Id));
        SMB_WO_PageRedirector_Controller sbmWOPR = new SMB_WO_PageRedirector_Controller();
    }
}