/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class Operators {
    global Operators() {

    }
    global static ADRA.Operators.Logical getInstance(String key) {
        return null;
    }
global class Contains implements ADRA.Operators.Logical {
    global Contains() {

    }
    global Boolean evaluate(Object fieldValue, Object testValue) {
        return null;
    }
}
global class EndsWith implements ADRA.Operators.Logical {
    global EndsWith() {

    }
    global Boolean evaluate(Object fieldValue, Object testValue) {
        return null;
    }
}
global class Equals implements ADRA.Operators.Logical {
    global Equals() {

    }
    global Boolean evaluate(Object fieldValue, Object testValue) {
        return null;
    }
}
global class GreaterThan implements ADRA.Operators.Logical {
    global GreaterThan() {

    }
    global Boolean evaluate(Object fieldValue, Object testValue) {
        return null;
    }
}
global class GreaterThanEquals implements ADRA.Operators.Logical {
    global GreaterThanEquals() {

    }
    global Boolean evaluate(Object fieldValue, Object testValue) {
        return null;
    }
}
global class LessThan implements ADRA.Operators.Logical {
    global LessThan() {

    }
    global Boolean evaluate(Object fieldValue, Object testValue) {
        return null;
    }
}
global class LessThanEquals implements ADRA.Operators.Logical {
    global LessThanEquals() {

    }
    global Boolean evaluate(Object fieldValue, Object testValue) {
        return null;
    }
}
global interface Logical {
    Boolean evaluate(Object param0, Object param1);
}
global class NotEquals implements ADRA.Operators.Logical {
    global NotEquals() {

    }
    global Boolean evaluate(Object fieldValue, Object testValue) {
        return null;
    }
}
global class StartsWith implements ADRA.Operators.Logical {
    global StartsWith() {

    }
    global Boolean evaluate(Object fieldValue, Object testValue) {
        return null;
    }
}
}
