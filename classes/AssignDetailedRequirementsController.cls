public class AssignDetailedRequirementsController 
{
	public Integer searchResultTotalIncluded {get; set;}
	
	public string componentId {get; set;}
	private String soql {get;set;}
	public List<Astadia_Detailed_Requirements__c> lstDetailReq {get;set;}
	public List<dDetailReqRecord> dRequirementList {get; set;} 
	public Astadia_Detailed_Requirements__c detailReq {get; set;}
	public Astadia_Component__c component {get; set;}
	public Astadia_Project__c project {get; private set;}
	public Astadia_High_Level_Requirement__c HighLevelReq {get;  set;}
	public boolean errormsg=false;
	public boolean assignedCheck {get; set;}		
	/**************************************/
	public String strProject{get; set;}
	public String strHLReq{get; set;}
	public String strFunctionOrObject{get; set;}
	public String ReqType {get; set;}
	public String status {get; set;}
	public String businessGroup {get; set;}
    public String requirementName {get; set;}
	/**************************************/

	public AssignDetailedRequirementsController(ApexPages.StandardController stdController)
    {               
        detailReq =  new Astadia_Detailed_Requirements__c();
        
        lstDetailReq =  new list<Astadia_Detailed_Requirements__c>();
        componentId = stdController.getId();     
               
    }
    
public class dDetailReqRecord
{
	public Astadia_Detailed_Requirements__c dReq {get; set;}
 	public Boolean selected {get; set;}
 	public String discoverySessionName {get;set;}

 	public dDetailReqRecord(Astadia_Detailed_Requirements__c d)
 	{
     	dReq = d;
     	selected = false;
     	discoverySessionName = d.Discovery_Session__r.Name;
 	}
}
        
    public string getComponentNameForButton()
    {
		Astadia_Component__c comp = [Select name from Astadia_Component__c where id = :componentId ];        
    	return comp.Name;
    }

	public List<SelectOption> getProjectNames()
	{
		List<SelectOption> options = new List<SelectOption>();
		List<Astadia_Project__c> projectList = new List<Astadia_Project__c>();
		projectList = [Select Id, Name FROM Astadia_Project__c ];
		options.add(new SelectOption('--SELECT ONE--','--SELECT ONE--'));
	  	for (Integer j=0;j<projectList.size();j++)
		  	{
 		      	options.add(new SelectOption(projectList[j].Id,projectList[j].Name));
		  	}
		  	return options;
	}
	
	public List<SelectOption> getHighLevelRequirements()
	{
		List<SelectOption> options = new List<SelectOption>();
		List<Astadia_High_Level_Requirement__c> hLevelReqList = new List<Astadia_High_Level_Requirement__c>();
		hLevelReqList = [Select Id, Name FROM Astadia_High_Level_Requirement__c where Project__c = :strProject ];
		options.add(new SelectOption('--SELECT ONE--','--SELECT ONE--'));
		for (Integer j=0;j<hLevelReqList.size();j++)
			{
				options.add(new SelectOption(hLevelReqList[j].id,hLevelReqList[j].Name));
			}
			return options;
	}
		
	public List<SelectOption> getFunctionListData()
	{
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('--All--','--All--'));
		for (Integer j=0;j<functionListValues.size();j++)
		{
			options.add(new SelectOption(functionListValues[j],functionListValues[j]));
		}
		return options;
	}		

	public List<dDetailReqRecord> getResults()
	{
		dRequirementList = new List<dDetailReqRecord>();
		Set<Id> existingDRSet = new Set<Id>();
		
		if (assignedCheck != null && !assignedCheck) {
      		
      		// get list of existing relationship items for filtering out from results 
      		List<DetReqBuildComp__c> existing = [SELECT Detailed_Requirement__c, Build_Component__c FROM DetReqBuildComp__c WHERE Build_Component__c =: componentId];
      		for (DetReqBuildComp__c drbc: existing) {
      			if (!existingDRSet.contains(drbc.Detailed_Requirement__c))
      				existingDRSet.add(drbc.Detailed_Requirement__c);
      		}
      	}	
      	
		for(Astadia_Detailed_Requirements__c dr : lstDetailReq)
     	{
     		if (!existingDRSet.contains(dr.Id))
         		dRequirementList.add(new dDetailReqRecord(dr));
     	}
 		return dRequirementList;
	}	
		
	/* Method to Search the contact database to fetch the query results */
	public PageReference searchRecords()  
	{
		errormsg=false;
		if (strProject.equals('--SELECT ONE--'))
		{
			System.debug('errorprint');
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a project to proceed'));
			 //System.debug('errorprint'+ message);
             return null;
		}
		String parentReq = detailReq.Parent_Requirement__c;
		ReqType = detailReq.Type__c;	
		status = detailReq.Status__c;
		businessGroup = detailReq.Business_Group__c;
        requirementName = detailReq.Requirement_Name__c;
		soql = 'select ID, Name,component__r.name,component__c, Discovery_Session__c,Discovery_Session__r.Name, Primary_Function_or_Object__c, Complexity__c, Business_Group__c, Requirement_Name__c, Status__c, Createddate from Astadia_Detailed_Requirements__c where High_Level_Requirement__r.Project__c =\''+strProject+'\'';
    	if ( strHLReq != null && !strHLReq.equals('--SELECT ONE--'))
      		soql += ' and High_Level_Requirement__c = \''+strHLReq+'\'';	
    	if (parentReq != null && !parentReq.equals(''))
      		soql += ' and Parent_Requirement__c = \''+parentReq+'\'';
    	if ( ReqType != null && !ReqType.equals(''))
      		soql += ' and Type__c LIKE \''+ReqType+'%\'';	
      	if ( status != null && !status.equals(''))
      		soql += ' and Status__c LIKE \''+status+'%\'';
      	if ( businessGroup != null && !businessGroup.equals(''))
      		soql += ' and Business_Group__c LIKE \''+businessGroup+'%\'';
      	if ( strFunctionOrObject != null && !strFunctionOrObject.equals('--All--'))
      		soql += ' and Primary_Function_or_Object__c INCLUDES ( \''+strFunctionOrObject+'\')';	
      		
      	if ( requirementName != null && !requirementName.equals(''))
      		soql += ' and Requirement_Name__c LIKE \'%'+requirementName+'%\'';
      			
      	//if ( assignedCheck!= null && !assignedCheck)
      	//	soql += ' and Component__c = null';
      	
      	      			
      	System.debug('----soqlstring---- '+ soql);
      	runQuery();
      	searchResultTotalIncluded = lstDetailReq.size();	
      	if (lstDetailReq == null || lstDetailReq.size()==0)
      	{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No detailed requirements found for specified filters'));
			return null;
      	}
      	return null;			
	}
		
	public  PageReference  AssignToComponent()
	{            
    	PageReference componentPage = new PageReference('/'+ componentId);
  		//List<Astadia_Detailed_Requirements__c> selectedReq = new List<Astadia_Detailed_Requirements__c>();
  		List<DetReqBuildComp__c> drbc = new List<DetReqBuildComp__c>();
  		
  		// re-query list in case someone else has added this relationship
  		Set<Id> existingDRSet = new Set<Id>();
  		List<DetReqBuildComp__c> existing = [SELECT Detailed_Requirement__c, Build_Component__c FROM DetReqBuildComp__c WHERE Build_Component__c =: componentId];
      	for (DetReqBuildComp__c drbc2: existing) {
      		if (!existingDRSet.contains(drbc2.Detailed_Requirement__c))
      			existingDRSet.add(drbc2.Detailed_Requirement__c);
      	}
      		
  		if (dRequirementList!= null)
  		{
    		for(dDetailReqRecord detReq : dRequirementList)
    		{
       			if(detReq.selected == true)
       			{
       				//selectedReq.add(detReq.dReq);
       				if (!existingDRSet.contains(detReq.dReq.Id)) {
	       				DetReqBuildComp__c workingDRBC = new DetReqBuildComp__c();
	       				workingDRBC.Detailed_Requirement__c = detReq.dReq.Id;
	       				workingDRBC.Build_Component__c = componentId;
	       				drbc.add(workingDRBC);
       				}
         		}
     		}    			
  		}	
 		/* return error message if no requirement is selected */
 		//if (selectedReq.size()==0)
		if (drbc.size()==0)
		{
		    errormsg=true;
		    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You either selected no components for assignment or the items that you selected have been previously added.'));
		    return null;
		}
		else
		{
		    errormsg=false;
			//for(Astadia_Detailed_Requirements__c d : selectedReq )
			//{
        	//	d.Component__c = componentId;         			
     		//}
     		try
     		{
     		 	//update selectedReq;
     		 	System.debug('Inserting X DRBC M:M records : ' + drbc.size());
     		 	insert drbc;
     			componentPage.setRedirect(true);
      			return componentPage;
     		}
     		catch (Dmlexception e) 
    		{
    			errormsg=true;
      			ApexPages.addMessages(e);
                
      			return null;
    		}
			    
		}   
	}
	public boolean geterrormsg()
	{
 		return errormsg;
	}
        
	public void runQuery() 
    { 
		try 
    	{
    		lstDetailReq = Database.query(soql);
    	} 
    	catch (Exception e ) 
    	{
    		errormsg=true;
      		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No detailed requirements found for specified filters'));
    	}
  	}
  		
  	public PageReference  Cancel()
  	{
  	    PageReference componentPage = new PageReference('/'+ componentId);  			    
    	componentPage.setRedirect(true);
    	return componentPage;  		
  	}
  		
  	public PageReference  Reset()
  	{
		PageReference pRef =  ApexPages.currentPage();
  		pRef.setRedirect(True);
        return pRef; 
  	}
	
	public List<String> functionListValues 
	{
    	get 
    	{
			if (functionListValues == null) 
			{ 
				functionListValues = new List<String>();
				Schema.DescribeFieldResult field = Astadia_Detailed_Requirements__c.Primary_Function_or_Object__c.getDescribe();
 
				for (Schema.PicklistEntry f : field.getPicklistValues())
				functionListValues.add(f.getLabel()); 
			}
			return functionListValues;          
    	}
    	set;
  	}
  	
  	
}