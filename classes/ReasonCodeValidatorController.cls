public class ReasonCodeValidatorController{

    private static List<String> fieldTypeList = new List<String>{'Product','Adjustment Driver','Description','Reason' ,'Responsible Area'};
    private static Map<String, String> fieldTypeAPINames = new Map<String, String>{'Product'=>'RC Product','Adjustment Driver'=>'RC Level 1','Description'=>'RC Level 2','Reason'=>'RC Level 3' ,'Responsible Area'=>'RC Responsible Area'};
    private static Map<String,List<Billing_Adjustment_RC_Mapping__c>> resultList = new Map<String,List<Billing_Adjustment_RC_Mapping__c>>();
      
    public static List<String> summary = new List<String>();
    public static Map<String,List<String>> resultMap = new Map<String,List<String>>();
    
    public ReasonCodeValidatorController(){
        initData();
        validateRCDropDownCodes();
    }
    
    public void initData(){
      List<Billing_Adjustment_RC_Mapping__c>  datalist =  [select Name,field_type__c from Billing_Adjustment_RC_Mapping__c where field_type__c in :fieldTypeList];
      for(Billing_Adjustment_RC_Mapping__c item : datalist){
            if(resultList.containsKey(item.field_type__c)){
                resultList.get(item.field_type__c).add(item);
            }else{
                List<Billing_Adjustment_RC_Mapping__c> itemList = new List<Billing_Adjustment_RC_Mapping__c>{item};
                    resultList.put(item.field_type__c,itemList);
            } 
       }
    }
    
    public List<String> getResult(){
        return summary;
    }
    
    public Map<String,List<String>> getResultMap(){
        return resultMap;
    }
    
    private static void validateRCDropDownCodes(){
        System.debug('fieldTypeList' + fieldTypeAPINames);
        for(String fieldType : fieldTypeAPINames.keySet()){
            List<String> rcLevelOptions =  getFieldOptions(fieldTypeAPINames.get(fieldType));
            Set<String> rcLevel1Set = getAsSet(resultList.get(fieldType)==null?new List<Billing_Adjustment_RC_Mapping__c>():resultList.get(fieldType));
            for(String option : rcLevelOptions){
                if(!rcLevel1Set.contains(option)){
                String errMsg = fieldType + ' option: "' + option + '" Not Found';
                    if(resultMap.containsKey(fieldType)){
                        resultMap.get(fieldType).add(errMsg);
                    }else{
                        List<String> errorList = new List<String>{errMsg};
                        resultMap.put(fieldType,errorList);
                    }
                }
            }
            if(resultMap.get(fieldType)==null){
               summary.add('No error found for ' + fieldType + ' Options'); 
            }else{
               summary.add(resultMap.get(fieldType).size() + ' error(s) found for RC Level ' + fieldType + ' options');
            } 
       }     
    }
    
    public static List<String> getFieldOptions(String fieldType){
        String fieldApiName =  fieldType.replaceAll(' ','_') +'__C';
        return  Util.getPickListValues(new Billing_Adjustment__c(),fieldApiName);    
    }
    
    private static Set<String> getAsSet(List<Billing_Adjustment_RC_Mapping__c> items){
         Set<String> asSet = new Set<String>();
        for(Billing_Adjustment_RC_Mapping__c item : items){
            asSet.add(item.Name);
        }
        return asSet;
    } 
    
    public PageReference updateContactSelectFiledIds(){
        Map<String,String> cobjFiedlIdMap = new Map<String,String>();
        ToolingAPILight toolingAPI = new ToolingAPILight();
        Set<String> fieldsSet = new Set<String>();fieldsSet.add('Account');fieldsSet.add('Contact');
         
        // Query CustomObject object by DeveloperName (note no __c suffix required)
        List<ToolingAPILight.CustomObject> customObjects = (List<ToolingAPILight.CustomObject>)
             toolingAPI.query('Select Id, DeveloperName, NamespacePrefix From CustomObject Where DeveloperName = \'Billing_Adjustment\'').records;
         
        // Query CustomField object by TableEnumOrId (use CustomObject Id not name for Custom Objects)
        ToolingAPILight.CustomObject customObject = customObjects[0];
        Id customObjectId = customObject.Id;
        List<ToolingAPILight.CustomField> customFields = (List<ToolingAPILight.CustomField>)
             toolingAPI.query('Select Id, DeveloperName, NamespacePrefix, TableEnumOrId From CustomField Where TableEnumOrId = \'' + customObjectId + '\'').records;
         System.debug('@customFields' + customFields);
        for(ToolingAPILight.CustomField customField : customFields){
            String strid = customField.Id;
            strid = strid.substring(0,strid.length()-3);
            if(fieldsSet.contains(customField.DeveloperName)){
                cobjFiedlIdMap.put(customField.DeveloperName,strid);
            }
         }
        System.debug('@cobjFiedlIdMap' + cobjFiedlIdMap);
        
        if(ContactSelectFieldMaping__c.getValues('Billing_Adjustment__c')==null){
            upsert new ContactSelectFieldMaping__c(
                Name = 'Billing_Adjustment__c',
                Account_Field_Id__c = cobjFiedlIdMap.get('Account'),
                Contact_Field_Id__c = cobjFiedlIdMap.get('Contact') 
            );
            summary.add('ContactSelectFieldMaping__c setting for Billing_Adjustment__c Population is done');
        }    
        return null;
    }
    
    public PageReference updateApprovalFlowSettings(){
   
       if(Billing_Adjustment_Approval_Flow__c.getAll().size()==0){ 
        List<Billing_Adjustment_Approval_Flow__c> approvalSettings = new list<Billing_Adjustment_Approval_Flow__c>();
        approvalSettings.add(new Billing_Adjustment_Approval_Flow__c(Name = 'Credit', Relase_Code_Base_Approval__c =true ,Controller_Approval__c = true,Engage_Sales_Approval__c =true));
        approvalSettings.add(new Billing_Adjustment_Approval_Flow__c(Name = 'Debit',Relase_Code_Base_Approval__c =true ,Controller_Approval__c = true,Engage_Sales_Approval__c =false));
        approvalSettings.add(new Billing_Adjustment_Approval_Flow__c(Name = 'Debt Write-off',Relase_Code_Base_Approval__c =true ,Controller_Approval__c = true,Engage_Sales_Approval__c =true));                                                        
        approvalSettings.add(new Billing_Adjustment_Approval_Flow__c(Name = 'Rebill',Relase_Code_Base_Approval__c =true ,Controller_Approval__c = true,Engage_Sales_Approval__c =false));                                                        
        approvalSettings.add(new Billing_Adjustment_Approval_Flow__c(Name = 'TLC Charge',Relase_Code_Base_Approval__c =true ,Controller_Approval__c = false,Engage_Sales_Approval__c =false));                                                        
        approvalSettings.add(new Billing_Adjustment_Approval_Flow__c(Name = 'TLC Waive',Relase_Code_Base_Approval__c =true ,Controller_Approval__c = true,Engage_Sales_Approval__c =true));                                                                                    
        approvalSettings.add(new Billing_Adjustment_Approval_Flow__c(Name = 'Transfer',Relase_Code_Base_Approval__c =true ,Controller_Approval__c = false,Engage_Sales_Approval__c =false));                                                        
        upsert approvalSettings;
        summary.add('Billing_Adjustment_Approval_Flow__c setting Population is done');    
       }
        
       return null;
    
    }
}