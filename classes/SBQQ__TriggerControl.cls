/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class TriggerControl {
    global TriggerControl() {

    }
    global Boolean alreadyRun(SObject record) {
        return null;
    }
    global Boolean alreadyRun(List<SObject> records) {
        return null;
    }
    global void block() {

    }
    global static SBQQ.TriggerControl getInstance() {
        return null;
    }
    global Boolean isDisabled() {
        return null;
    }
    global void reset() {

    }
    global void setDisabled() {

    }
    global Boolean shouldRun(SObject record) {
        return null;
    }
}
