@isTest
private class trac_VendorCommunicationDispatcherTest {
	
	@isTest(SeeAllData=false)
	static void testCreateCommentCaseLookup() {		
		//
		List<Vendor_Communication__c> newVendorComms = new List<Vendor_Communication__c>();
		List<Case> testCases = new List<Case>();		
		List<CaseComment> testCaseComments = new List<CaseComment>();

		Set<Id> caseIds = new Set<Id>();

		
		Case testCase = new Case(Subject = 'Test Subject 2', Priority = '1');

		testCases.add(testCase);
		insert testCase;

		caseIds.add(testCase.Id);

    	newVendorComms.add(new Vendor_Communication__c(	Case__c = testCase.Id,
													   	Vendor_Comments__c='Test number 2 for real ',
                                                       	isPublished__c = false
    	));

    	insert newVendorComms;		

    	newVendorComms = [
    		SELECT Id, Vendor_Comments__c
    		FROM Vendor_Communication__c
    		WHERE Case__c In :caseIds    		
    	];

    	System.Debug('this is the size of newVendorComms: ' + newVendorComms.size());
    	for(Vendor_Communication__c ven : newVendorComms){
    		ven.Vendor_Comments__c += ' this is a test';
    	}
    	Test.StartTest();
    	update newVendorComms;
    	Test.StopTest();

    	//testCaseComments = [
    	//	SELECT Id, CommentBody
    	//	FROM CaseComment
    	//	WHERE Id 
    	//]
    	//System.assertEquals()
	}

	@isTest(SeeAllData=false)
	static void testCreateCommentCaseIdString() {

		List<Vendor_Communication__c> newVendorComms = new List<Vendor_Communication__c>();
		List<Case> testCases = new List<Case>();

		Case testCase = new Case(Subject = 'Test Subject 1', Priority = 'Medium');
		
		testCases.add(testCase);

		insert testCases;

		newVendorComms.add(new Vendor_Communication__c(Case_ID__c = String.valueOf(testCase.Id),															
													   	Vendor_Comments__c='Test number 1 for sure ',
                                                       	isPublished__c = true
    	));

		insert newVendorComms;	

		String caseIdString = String.valueOf(testCase.Id);
    	newVendorComms = [
    		SELECT Id, Vendor_Comments__c
    		FROM Vendor_Communication__c
    		WHERE Case_ID__c <> null
    		AND Case_ID__c = :caseIdString    		
    	];

    	System.Debug('this is the size of newVendorComms: ' + newVendorComms.size());
    	for(Vendor_Communication__c ven : newVendorComms){
    		ven.Vendor_Comments__c += ' this is a test';
    	}
    	Test.StartTest();
    	update newVendorComms;
    	Test.StopTest();
	}
	
	
}