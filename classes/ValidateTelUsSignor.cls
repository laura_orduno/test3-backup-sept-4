global with sharing class ValidateTelUsSignor implements vlocity_cmt.VlocityOpenInterface{
    
    public Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        Boolean success = true;
        try{
            if(methodName == 'validateSignor') {             
                 validateSignor(inputMap, outMap, options);                
            }
          }catch(Exception e){
                    
                    success=false;
         }

        return success;        
    }
    
     private static void validateSignor(Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options){
         system.debug('ValiateTelUSSignor:updateStatus '+inputMap);
         list<string> errorMessages = new list<string>();
         if(inputMap.containsKey('contextId'))
         {
             Id cmId = (id)inputMap.get('contextId');
           	 errorMessages = eSignatureValidation.validate(cmId);
             if(errorMessages.size() > 0 && !errorMessages.isEmpty())
            	 outMap.put('Error', errorMessages[0]);
         	
         }
    }


}