public class NAAS_Async_SearchAppointment_ping_v1 {
    public class pingResponse_elementFuture extends System.WebServiceCalloutFuture {
        public String getValue() {
        SMB_Appointment_ping_v1.pingResponse_element response = new SMB_Appointment_ping_v1.pingResponse_element();
            if(Test.isRunningTest()) {
                response.version = 'test';
            } else {
            response = (SMB_Appointment_ping_v1.pingResponse_element)System.WebServiceCallout.endInvoke(this);
            }
            return response.version;
        }
    }
}