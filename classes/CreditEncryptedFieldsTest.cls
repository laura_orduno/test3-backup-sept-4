@isTest
public class CreditEncryptedFieldsTest {

    static testMethod void testBulkInsert() {
        List<account> Accounts = new List<account>();
        List<Credit_Profile__c> creditProfiles = new List<Credit_Profile__c>();
        List<Credit_Assessment__c> creditAssessments = new List<Credit_Assessment__c>();
        
        // add 200 accounts to the list to be inserted
        for (Integer i=0; i<200; i++) {
          Account acc= new Account(
            Name = 'RCID Test'+i,
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            RCID__c = '987654'+i
          );
         accounts.add(acc);
        }
        
        Test.startTest();
        insert accounts; 
        for (Integer i=0; i<200; i++) {
            Credit_Profile__c creditProfile=new Credit_Profile__c(name='CP-RCID'+i,
                                                                  account__c=accounts[i].id
            );
            creditProfiles.add(creditProfile);
        }
        insert creditProfiles;

        for (Integer i=0; i<200; i++) {
            Credit_Assessment__c creditAssessment=new Credit_Assessment__c(CAR_account__c=accounts[i].id,
                                                                           credit_profile__c = creditProfiles[i].id
                                                                          );
 			creditAssessments.add(creditAssessment);
        }
        insert creditAssessments;
        Test.stopTest();
        
        //Query that all 200 records were inserted
        List<Credit_Profile__c> insertedProfiles = [Select name, driver_s_license_hash__c
                                                               , health_card_no_hash__c
                                                               , sin_hash__c
                                                               , provincial_id_hash__c
                                                               , Passport_hash__c
                                                    from credit_profile__c 
                                                    where id in :creditProfiles];
        
        //Assert that the Encrypted Fields are all set to empty
        for (Credit_Profile__c cp : insertedProfiles) {
            System.assertEquals(null,cp.driver_s_license_hash__c);
            System.assertEquals(null,cp.health_card_no_hash__c);
            System.assertEquals(null,cp.sin_hash__c);
            System.assertEquals(null,cp.provincial_id_hash__c);
            System.assertEquals(null,cp.Passport_hash__c);
        }

        //Query that all 200 records were inserted
        List<Credit_Assessment__c> insertedCARs = [Select name, driver_s_license_hash__c
                                                               , health_card_no_hash__c
                                                               , sin_hash__c
                                                               , provincial_id_hash__c
                                                               , Passport_hash__c
                                                    from credit_assessment__c 
                                                    where id in :creditAssessments];
        
        //Assert that the Encrypted Fields are all set to empty
        for (Credit_Assessment__c car : insertedCARs) {
            System.assertEquals(null,car.driver_s_license_hash__c);
            System.assertEquals(null,car.health_card_no_hash__c);
            System.assertEquals(null,car.sin_hash__c);
            System.assertEquals(null,car.provincial_id_hash__c);
            System.assertEquals(null,car.Passport_hash__c);
        }
        
    }
    
    static testMethod void testNonEmptyFieldsCP() {        
        Account acc= new Account(
            Name = 'RCID Test',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            RCID__c = '987654');
        insert acc; 
        Credit_Profile__c creditProfile=new Credit_Profile__c(name='CP-RCID',
                                                              account__c=acc.id,
                                                              SIN__c='220380919',
                                                              Driver_s_License__c='DL123',
                                                              Drivers_License_province__c = 'British Columbia',
                                                              Health_Card_No__c ='HC123',
                                                              Provincial_ID__c='BC123',
                                                              Provincial_ID_Province__c = 'British Columbia',
                                                              Passport__c='PP1234',
                                                              Passport_Country__c = 'Canada'
                                                             );                                                                                                                          
        insert creditProfile;

        //Query to see what was inserted
        Credit_Profile__c cp = [Select name, driver_s_license_hash__c
                                           , health_card_no_hash__c
                                           , sin_hash__c
                                           , provincial_id_hash__c
                                		   , Passport_hash__c
                               from credit_profile__c 
                               where id =:creditProfile.id];
        
        //Assert that the Encrypted Fields are all not empty
        System.assertNotEquals(null,cp.driver_s_license_hash__c);
        System.assertNotEquals(null,cp.health_card_no_hash__c);
        System.assertNotEquals(null,cp.sin_hash__c);
        System.assertNotEquals(null,cp.provincial_id_hash__c);
        System.assertNotEquals(null,cp.Passport_hash__c);
        
    }    
    
    static testMethod void testNonEmptyFieldsCAR() {        
        Account acc= new Account(
            Name = 'RCID Test',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('RCID').getRecordTypeId(),
            BillingStreet = '3777 Kingsway',
            BillingCity = 'Burnaby',
            BillingState = 'BC',
            BillingCountry = 'CAN',
            BillingPostalCode = 'V7C 2K5',
            RCID__c = '987654');
        insert acc; 
        Credit_Profile__c creditProfile=new Credit_Profile__c(name='CP-RCID',
                                                              account__c=acc.id,
                                                              SIN__c='220380919',
                                                              Driver_s_License__c='DL123',
                                                              Drivers_License_province__c = 'British Columbia',
                                                              Health_Card_No__c ='HC123',
                                                              Provincial_ID__c='BC123',
                                                              Provincial_ID_Province__c = 'British Columbia',
                                                              Passport__c='PP1234',
                                                              Passport_Country__c = 'Canada'
                                                             );                                                                                                                          
        insert creditProfile;

        Credit_Assessment__c creditAssessment=new Credit_Assessment__c(CAR_Account__c = acc.id,
                                                                       Credit_Profile__c=creditProfile.id,
		                                                              SIN__c='220380919',
        		                                                      Driver_s_License__c='DL123',
                                                                      Drivers_License_province__c = 'British Columbia',
                		                                              Health_Card_No__c ='HC123',
                                                              		  Provincial_ID__c='BC123',
                                                              		  Provincial_ID_Province__c = 'British Columbia',
                                                              		  Passport__c='PP1234',
                                                             		  Passport_Country__c = 'Canada'
                                                          			   );                                                                                                                           
        
		insert creditAssessment;
        
        //Query to see what was inserted
        Credit_Assessment__c car = [Select name, driver_s_license_hash__c
                                           , health_card_no_hash__c
                                           , sin_hash__c
                                           , provincial_id_hash__c
                                    	   , Passport_hash__c
                               from credit_Assessment__c 
                               where id =:creditAssessment.id];
        
        //Assert that the Encrypted Fields are all not empty
        System.assertNotEquals(null,car.driver_s_license_hash__c);
        System.assertNotEquals(null,car.health_card_no_hash__c);
        System.assertNotEquals(null,car.sin_hash__c);
        System.assertNotEquals(null,car.provincial_id_hash__c);
        System.assertNotEquals(null,car.Passport_hash__c);
                
    }        
}