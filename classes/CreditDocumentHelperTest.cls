@isTest
public class CreditDocumentHelperTest {
    
    @isTest
    public static void deleteDocuments() {  
        
        Test.startTest();    
        
        CreditUnitTestHelper.createTestuser();         
        User testUser = CreditUnitTestHelper.getTestuser();
        System.debug('... dyy ... testuser = ' + testUser.Name);
        
        
        System.runAs(testUser){  
            
            Test.setCurrentPageReference(new PageReference('dyy test page'));         
            System.currentPageReference().getParameters().put('ids', null);           
            String retUrl = '/' + SObjectType.Credit_Assessment__c.keyPrefix;   
            System.currentPageReference().getParameters().put('retUrl', retUrl);
            CreditUnitTestHelper.getCri();
            CreditUnitTestHelper.getCri();
            CreditDocumentHelper.deleteDocuments();
            
        }
        
        Test.StopTest();    
    }
}