public class OrdrSubmitCustomerOrderWsCallout {
    public static TpFulfillmentCustomerOrderV3.CustomerOrder submitOrder(TpFulfillmentCustomerOrderV3.CustomerOrder customerOrder,boolean isAmend) {
        TpFulfillmentCustomerOrderV3.CustomerOrder respCustomerOrder;
        try {
            if(isAmend){
                OrdrWsSubmitCustomerOrder.AmendCustomerOrderSOAPPort servicePort = new OrdrWsSubmitCustomerOrder.AmendCustomerOrderSOAPPort();
                servicePort = prepareCalloutForAmend(servicePort);
                
                respCustomerOrder = servicePort.amendCustomerOrder(customerOrder); 
                
            } else {
                OrdrWsSubmitCustomerOrder.SubmitCustomerOrderSOAPPort servicePort = new OrdrWsSubmitCustomerOrder.SubmitCustomerOrderSOAPPort();
                servicePort = prepareCallout(servicePort); 
                
                respCustomerOrder = servicePort.submitCustomerOrder(customerOrder);
                
                
            }        
            
        } 
        catch (CalloutException e) {
            System.debug(e.getmessage());
            throw e;
        }
        return respCustomerOrder;
    }
    private static OrdrWsSubmitCustomerOrder.SubmitCustomerOrderSOAPPort prepareCallout(OrdrWsSubmitCustomerOrder.SubmitCustomerOrderSOAPPort servicePort) {
        Ordering_WS__c submitCustomerOrderEndpoint = Ordering_WS__c.getValues('SubmitCustomerOrderEndpoint');  
        
        
        servicePort.endpoint_x = submitCustomerOrderEndpoint.value__c;
        System.debug('SubmitCustomerOrderEndpoint: '+ submitCustomerOrderEndpoint.value__c);
        
        /*  if (servicePort.endpoint_x.startsWith(OrdrConstants.ENDPOINT_XML_GATEWAY_SYMBOL)) {
                Ordering_WS__c wsUserName =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_USERNAME);
                Ordering_WS__c wsPassword =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_PASSWORD);  
                if (String.isNotBlank(wsUserName.value__c) && String.isNotBlank(wsPassword.value__c)) {
                String credentials = wsUserName.value__c + ':' + wsPAssword.value__c;
                String encodedUserNameAndPassword = EncodingUtil.base64Encode(Blob.valueOf(credentials));
                servicePort.inputHttpHeaders_x = new Map<String, String>();
                servicePort.inputHttpHeaders_x.put(OrdrConstants.AUTHORIZATION, OrdrConstants.BASIC + encodedUserNameAndPassword);
                }   
                }
                else {
                servicePort.clientCertName_x = OrdrConstants.SDF_CERT_NAME;
            }*/
        servicePort.clientCertName_x = OrdrConstants.SDF_CERT_NAME;          
        // Set SFDC Webservice call timeout
        servicePort.timeout_x = OrdrConstants.WS_TIMEOUT_MS;
        return servicePort;
    } 
    private static OrdrWsSubmitCustomerOrder.AmendCustomerOrderSOAPPort prepareCalloutForAmend(OrdrWsSubmitCustomerOrder.AmendCustomerOrderSOAPPort servicePort) {
        
        Ordering_WS__c amendCustomerOrderEndpoint = Ordering_WS__c.getValues('AmendCustomerOrderEndpoint');       
        servicePort.endpoint_x =amendCustomerOrderEndpoint.value__c;
        System.debug('SubmitCustomerOrderEndpoint: '+ amendCustomerOrderEndpoint.value__c);
        
        /* if (servicePort.endpoint_x.startsWith(OrdrConstants.ENDPOINT_XML_GATEWAY_SYMBOL)) {
                Ordering_WS__c wsUserName =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_USERNAME);
                Ordering_WS__c wsPassword =  Ordering_WS__c.getValues(OrdrConstants.CREDENTIAL_PASSWORD);  
                if (String.isNotBlank(wsUserName.value__c) && String.isNotBlank(wsPassword.value__c)) {
                String credentials = wsUserName.value__c + ':' + wsPAssword.value__c;
                String encodedUserNameAndPassword = EncodingUtil.base64Encode(Blob.valueOf(credentials));
                servicePort.inputHttpHeaders_x = new Map<String, String>();
                servicePort.inputHttpHeaders_x.put(OrdrConstants.AUTHORIZATION, OrdrConstants.BASIC + encodedUserNameAndPassword);
                }   
                }
                else {
                servicePort.clientCertName_x = OrdrConstants.SDF_CERT_NAME;
                }*/
        servicePort.clientCertName_x = OrdrConstants.SDF_CERT_NAME;            
        // Set SFDC Webservice call timeout
        servicePort.timeout_x = OrdrConstants.WS_TIMEOUT_MS;
        return servicePort;
    } 
}