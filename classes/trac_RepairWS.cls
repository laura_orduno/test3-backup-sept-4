/*
 * Web Services for Telus RTS system integration. Includes services for updating and closing repair case records
 * Part of the SMB Repairs project
 *
 * @author: Grant Adamson, Traction On Demand 
 */

global class trac_RepairWS {
	public static final String CANCELLED_STATUS = 'Cancelled';
	public static final String SRD_CANCELLED_STATUS_Code = '3';
	public static final String SRD_CANCELLED_STATUS_EN = 'Cancel';
	public static final String SRD_CANCELLED_STATUS_FR = 'Annuler';
	public static final String CLOSED_STATUS = 'Closed';
	public static final String WS_SUCCESS = 'Success';
	public static final String WS_FAILURE = 'Failure';
	public static final Integer EXCEPTION_FAILURE_CODE = -1;
	public static final Integer ID_MATCH_FAILURE_CODE = -2;
	public static final Integer CASE_CLOSED_FAILURE_CODE = -3;
	public static final Integer CASE_CANCELLED_FAILURE_CODE = -4;
	public static final Integer REPAIR_ID_MISMATCH_FAILURE_CODE = -5;
	public static final Integer REPAIR_IMEI_MISMATCH_FAILURE_CODE = -6;
	public static final String ACCESSORY_REPAIR_TYPE = 'Accessory';
	
	private enum CallType {RECEIVE, CANCEL}
	
	
	WebService static RepairResult cancelRepair(Long repairId) {
		RepairRequest req = new RepairRequest();
		req.repairId = repairId;
		
		return processRequest(CallType.CANCEL, req);
	}
	
	
	WebService static RepairResult receiveRepairIdAndData(RepairRequest req) {
		return processRequest(CallType.RECEIVE, req);
	}
	
	
	private static RepairResult processRequest(CallType cType, RepairRequest req) {
		RepairResult res;
		Case c;
		
		try {
			c = findCase(req,ctype);
			res = checkCaseValidity(c,req,cType);
			
			if(res.status != WS_FAILURE) {
				if(cType == CallType.RECEIVE) {
					trac_WSFieldMapper.tracRepairResultToCase(req, c);
				
				} else if (cType == CallType.CANCEL){
					c.Repair_in_Progress__c = false; // prevent validation rules from blocking close
					c.status = CANCELLED_STATUS;
					c.SRD_Repair_Status_Id__c = SRD_CANCELLED_STATUS_Code;
					c.SRD_Repair_Status__c = SRD_CANCELLED_STATUS_EN;
					c.SRD_Repair_Status_FR__c = SRD_CANCELLED_STATUS_FR;
				}
				update c;
				
				res.status = WS_SUCCESS;
			}
			
		} catch (Exception e) {
			res = new RepairResult();
			res.status = WS_FAILURE;
			res.errorCode = EXCEPTION_FAILURE_CODE;
			res.errorMessage = e.getMessage();
			
			if (c != null) {
				res.caseNumber = c.CaseNumber;
			}
		}
		
		trac_RepairLogger.log(cType == CallType.RECEIVE ? 'receiveRepairIdAndData' : 'cancelRepair',
			'Returning status: ' + res.status + ' to RTS' + res.status == WS_SUCCESS ? '' : res.toString(),
			c != null ? c.id : null);
		
		return res;
	}
	
	
	private static Case findCase(RepairRequest req, CallType cType) {
		if(cType == CallType.RECEIVE) {
			return findCaseById(req.caseId);
		} else { //(cType == CallType.CANCEL)
			return findCaseByRepairID(req.repairId);
		}
	}
	
	
	private static Case findCaseById(String caseId) {
		List<Case> cases = [
			SELECT caseNumber, repair_type__c, status, repair__c, ESN_MEID_IMEI__c
			FROM Case
			WHERE id = :caseId
		];
		
		if(cases.size() > 0) {
			return cases[0];
		} else {
			return null;
		}
	}
	
	
	private static Case findCaseByRepairId(Long repairId) {
		List<Case> cases = [
			SELECT caseNumber, repair_type__c, status, repair__c, ESN_MEID_IMEI__c
			FROM Case
			WHERE repair__c = :repairId + '' // convert repairId to a string without adding any formatting
		];
		
		if(cases.size() > 0) {
			return cases[0];
		} else {
			return null;
		}
	}
	
	private static RepairResult checkCaseValidity(Case c, RepairRequest req, CallType cType) {
		RepairResult res = new RepairResult();
		
		if(c == null) {
			res.status = WS_FAILURE;
			res.errorCode = ID_MATCH_FAILURE_CODE;
			res.errorMessage = (cType == CallType.RECEIVE ? Label.CASE_ID_NOT_FOUND : Label.REPAIR_ID_NOT_FOUND);
		
		} else {			
			res.caseNumber = c.CaseNumber;
			
			if(c.repair__c != null && c.repair__c != req.repairID + '') {
				res.status = WS_FAILURE;
				res.errorCode = REPAIR_ID_MISMATCH_FAILURE_CODE;
				res.errorMessage = Label.REPAIR_ID_MISMATCH;
			} else if(c.repair__c == null && c.ESN_MEID_IMEI__c != req.defectiveSerialNumber){
				res.status = WS_FAILURE;
				res.errorCode = REPAIR_IMEI_MISMATCH_FAILURE_CODE;
				res.errorMessage = Label.IMEI_MISMATCH;
			} else if(c.status == CLOSED_STATUS) {
				res.status = WS_FAILURE;
				res.errorCode = CASE_CLOSED_FAILURE_CODE;
				res.errorMessage = Label.CASE_ALREADY_CLOSED;
			} else if (c.status == CANCELLED_STATUS) {
				res.status = WS_FAILURE;
				res.errorCode = CASE_CANCELLED_FAILURE_CODE;
				res.errorMessage = Label.CASE_ALREADY_CANCELLED;
			}
		}
		return res;
	}
	
	
	// Web Service DTOs
	
	global class RepairRequest {
		WebService Long repairID;
		WebService String caseId;
		WebService Name clientName;
		WebService String defectiveSerialNumber;
		WebService String mobileNumber;
		WebService String defectiveSKU;
		WebService Name contactName;
		WebService String contactPhoneNumber;
		WebService String contactEmailAddress;
		WebService RepairRequestAddress clientShippingAddress;
		WebService String clientBusinessName;
		WebService RepairRequestAddress address;
		WebService Boolean physicalDamageInd;
		WebService String issue;
		WebService String defectsBehaviour;
		WebService String descriptionAndTroubleshooting;
		WebService Boolean cosmeticDamageInd;
		WebService String descriptionOfCosmeticDamage;
		WebService Double estimatedRepairCost;
		WebService String languageCD;
		
		
		public RepairRequest() {
			clientName = new Name();
			contactName = new Name();
			clientShippingAddress = new RepairRequestAddress();
			address = new RepairRequestAddress();
		}
	}
	
	
	global class RepairRequestAddress {
		WebService String streetName;
		WebService String municipalityName;
		WebService String provinceStateCode;
		WebService String postalZipCode;
	}
	
	
	global class Name {
  	WebService String title;
    WebService String firstName;
    WebService String middleName;
    WebService String lastName;
	}
	
	
	global class RepairResult {
		WebService String status;
		WebService String caseNumber;
		WebService Integer errorCode;
		WebService String errorMessage; 
	}
	
}