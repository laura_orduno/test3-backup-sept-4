public with sharing class S2C_MyWorkRequestsController {
    /*public list<work_request__c> myWorkRequests{get;set;}
    public MyWorkRequestsController(){
        init();
    }
    void init(){
        string query='select id,name,createddate,lastmodifieddate,opportunity__c,recordtypeid,status__c,type__c,service_request__c,account__c,account__r.name,account_owner__c,ownerid,owner.name from work_request__c where status__c !=\'closed\' and createdbyid=\''+userinfo.getuserid()+'\' order by createddate limit 20';
        myWorkRequests=database.query(query);
    }*/

   
    public list<Work_Request__c> lstWorkRequests{get{
        return sortList(lstWorkRequests);
    } set;}
    
    public String sortBy { get; set; }
    public String sortDir { get; set; }
    
    public S2C_MyWorkRequestsController(){
        //CF: Change query to use boolean rather than status for closed cases
        lstWorkRequests =         
                    [select id,name,createddate,lastmodifieddate,opportunity__c,
                     recordtypeid,record_type_name__c,opportunity__r.name,Order_Id_Text__c,status__c,type__c,service_request__c,Service_Request_Id_Text__c,service_request__r.name,account__c,Account_Name__c,
                     account__r.name,account_owner__c,ownerid,owner.name from work_request__c where status__c !='Closed' and status__c !='Cancelled' and status__c !='Completed' and Ownerid=: userinfo.getuserid() 
                     order by createddate limit 20];
        
         system.debug('@@@@lstWorkRequests : '+lstWorkRequests);
     }
    
    
    public PageReference empty() { return null; }
    
    public List<SObject> sortList(List<SObject> cleanList){
        
        system.debug('@@@@@cleanList : '+cleanList);
        system.debug('@@@@@sortBy : '+sortBy);
        
        if (sortBy == null) { return cleanList; }
        
        List<SObject> resultList = new List<SObject>();
        Map<Object, List<SObject>> objectMap = new Map<Object, List<SObject>>();
        
        for (SObject item : cleanList) {
          system.debug('@@@@@item:'+item);
          
          if (objectMap.get(item.get(sortBy)) == null) {
            objectMap.put(item.get(sortBy), new List<SObject>());
          }
          objectMap.get(item.get(sortBy)).add(item);
        }
        
        List<Object> keys = new List<Object>(objectMap.keySet());
        keys.sort();
        
        for(Object key : keys) {
          resultList.addAll(objectMap.get(key));
        }
        
        cleanList.clear();
        
        if (sortDir == 'ASC') {
          for (SObject item : resultList) {
            cleanList.add(item);
          }
        } else {
          for (Integer i = resultList.size()-1; i >= 0; i--) {
            cleanList.add(resultList[i]);
          }
        }
        
        return cleanList;
  }
  
}