public class CreditCrypto
{
	public static String hash(String value)
    {
        Blob digest = Crypto.generateDigest('SHA-512', Blob.valueOf(value));

        return EncodingUtil.base64Encode(digest);
    }
}