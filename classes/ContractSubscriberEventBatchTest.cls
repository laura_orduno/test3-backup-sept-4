@IsTest(SeeAllData=false)
public class ContractSubscriberEventBatchTest {
    
    @isTest
    public static void testSubsriberEventTrigger(){
        
/*
account acct = [select id,name,can__c from account where id = '0014000000QKKyIAAX'];
system.debug('acct='+acct);
map<id,contract> ctr = new map<id,contract>([select id,name from contract where AccountId =: acct.Id]);
system.debug('ctr='+ctr);
list<agreement_subscriber_event__c> recs = [select id,ban__c,contract__c from agreement_subscriber_event__c where contract__c =:ctr.keyset() limit 1];
system.debug('recs='+recs);

*/
        Test.startTest();
        Account rcid1 = new Account(Name='RCID ACC');
        Account rcid2 = new Account(Name='RCID ACC');
        insert rcid1 ;
        insert rcid2 ;
        Account ban =  new Account(Name='BAN ACC');
        
        ban.ParentId = rcid1.id;
        ban.CAN__c = '123456';
        
        insert ban;
        
        Id ContractRecId = Util.getRecordTypeIdByName('Contract', 'Corporate Wireless');
        contract contractObj = new contract(Name='TestAGR', AccountId=rcid1.Id, RecordTypeId=ContractRecId);
        //contract contractObj = new contract(Name='TestAGR', AccountId=rcid1.Id, RecordTypeId=ContractRecId, status='Contract Registered');
        insert contractObj;
        //contractObj.status = 'Contract Registered';
        //update contractObj;

        system.debug('contract = '+contractObj);
        
        Id ChurnRecTypeIdFixed = Util.getRecordTypeIdByName('Agreement_Churn_Allotment__c', 'Fixed Amount For Contract Term');
        Id ChurnRecTypeIdPeriod = Util.getRecordTypeIdByName('Agreement_Churn_Allotment__c', 'Specified Amount For a Specific Period');
        
        Agreement_Churn_Allotment__c churnRec1 = new Agreement_Churn_Allotment__c(Allowed_Churn_Units__c = 10 , Contract__c = contractObj.id, RecordTypeId= ChurnRecTypeIdFixed) ;
        Agreement_Churn_Allotment__c churnRec2 = new Agreement_Churn_Allotment__c(Allowed_Churn_Units__c = 10 , Contract__c = contractObj.id,
                     Start_Date__c = Date.newInstance(2016, 1, 1),End_Date__c =   Date.newInstance(2016, 5, 30), RecordTypeId= ChurnRecTypeIdPeriod );
        
        
        Id hardwareRecTypeIdFixed = Util.getRecordTypeIdByName('Agreement_Hardware_Allotment__c', 'Fixed Amount For Contract Term');
        Id hardwareRecTypeIdPeriod = Util.getRecordTypeIdByName('Agreement_Hardware_Allotment__c', 'Specified Amount For a Specific Period');
        Agreement_Hardware_Allotment__c hardwareRec1 = new Agreement_Hardware_Allotment__c(Allowed_Hardware_Units__c = 30 , Contract__c = contractObj.id, RecordTypeId = hardwareRecTypeIdFixed);
        Agreement_Hardware_Allotment__c hardwareRec2 = new Agreement_Hardware_Allotment__c(Allowed_Hardware_Units__c = 30 , Contract__c = contractObj.id,
                     Start_Date__c = Date.newInstance(2016, 1, 1),End_Date__c =   Date.newInstance(2016, 5, 30),RecordTypeId= hardwareRecTypeIdPeriod);
        List<SObject> toInsert = new List<SObject>();
        toInsert.add(churnRec1);toInsert.add(churnRec2);
        toInsert.add(hardwareRec1);toInsert.add(hardwareRec2);
        insert toInsert; 
        
        toInsert.clear();
        
        String [] eventStatuses = new String[] {'A','C','C','R','S'};
        for(String st :eventStatuses){
            toInsert.add(new Agreement_Subscriber_Event__c(BAN__c='123456', Subscriber_Status__c=st, Effective_Date__c= Date.today()));   
        } 
        insert toInsert;
        
        ContractSubscriberEventBatch btch = new ContractSubscriberEventBatch();
        list<sobject> lst = [select id,ban__c,contract__c from agreement_subscriber_event__c order by lastmodifieddate desc nulls last limit 300];
        btch.execute(null,lst);
        
        List<Contract> clist = new List<Contract>();
        clist.add(contractObj);
        map<string,id> accountCanMap=new map<string,id>();
        accountCanMap.put(ban.CAN__c,ban.ParentId);
        map<string,id> canAccountMap=new map<string,id>();
        canAccountMap.put(ban.CAN__c,ban.id);
        btch.processAgreementSubscriberEvents(clist,accountCanMap,canAccountMap,lst);

        Test.stopTest();
    }
    @isTest
    public static void testSchedulable(){
        ContractSubscriberEventSchedulable schedObj = new ContractSubscriberEventSchedulable();
        String schedCron = '0 0 23 * * ?';
        system.schedule('test ContractSubscriberEventSchedulable',schedCron,schedObj);
    }
}