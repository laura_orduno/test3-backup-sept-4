@isTest
private class SummaryVariableCalculatorTest {
	private static testMethod void testSummaryVariableCalculator() {
		Opportunity o = new Opportunity(name = 'test', closedate = Date.today(), stagename = 'test');
		insert o;
		
		SBQQ__Quote__c quote = new SBQQ__Quote__c(SBQQ__Opportunity__c = o.id);
		insert quote;
		
		List<SObject> records = new List<SObject>();
		
		List<SBQQ__SummaryVariable__c> vars = new List<SBQQ__SummaryVariable__c>{}; 
		
		
		
		SummaryVariableCalculator calc = new SummaryVariableCalculator(vars);
		
		Set<String> fields = calc.getReferencedFields();
		
		calc.calculate(quote, records);
		
		//Decimal result = calc.getResult(variableId);
		
	}
}