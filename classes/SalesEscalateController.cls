public class SalesEscalateController {
    public Case caseRecord {get; set;}
    public String formId {get; set;}
  public String caseType {get; set;}
    public String errorEscalationReason {get; set;}
    public Set<String> restrictedProfiles;
    public Boolean isSubTypeAvailable {get; set;}
    public Map<String, Map<String, String>> formData;
    public Map<String, Set<String>> subTypePicklistValues;
    public Map<String, Integer> fieldLengthValidation;
  public List<SelectOption> subTypes {get; set;}    
    public Boolean hasAccessToEscalation {get; set;}
    public Boolean isError {get; set;}
    
    public static final String FORM_ID_HELD_ORDERS = 'Held Orders';

    public SalesEscalateController(ApexPages.StandardController sContact){
        try{            
            Contact contactRecord = (Contact)sContact.getRecord();                 
            caseRecord = new Case();            
            caseRecord.CreatedVia__c = 'Salesforce1';               
            
            if(contactRecord != null && contactRecord.id != null){
                caseRecord.ContactId = contactRecord.id;
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Error, contact was not found.'));
                
                isError = true;
            }
            
            Map<String, Schema.RecordTypeInfo> rtMap = Schema.SObjectType.Case.getRecordTypeInfosByName();
    
            Id profileId = UserInfo.getProfileId();
            String profileName = [Select Id, Name from Profile where Id = :profileId].Name;
            // define user profiles that are restricted from th ebilling escalation
            restrictedProfiles = new Set<String>{'SMB Sales Manager / Director / MD', 'SMB Sales Prime/ASR/Coordinator'};
            // set access flag for menu options          
            hasAccessToEscalation = restrictedProfiles.contains(profileName);
              
            // default values for the specified forms
            formData = new Map<String, Map<String, String>>{
                'Billing Escalation' => new Map<String, String>{
                    'recordTypeName' => 'SMB Care Billing',
                    'recordTypeId' => rtMap.get('SMB Care Billing').getRecordTypeId(),
                    'type' => 'SMB Care Billing',
                    'origin' => 'Mobile',
                    'Case_Icon_Mapping__c'=>'Billing Escalation'
                },  
                'Wireline/Voice/ADSL' => new Map<String, String>{
                    'recordTypeName' => 'SMB Care TELUS Service',
                    'recordTypeId' => rtMap.get('SMB Care TELUS Service').getRecordTypeId(),
                    'type' => 'Move/Add/Change',
                    'origin' => 'Mobile',
                    'Case_Icon_Mapping__c'=>'Wireline/Voice/ADSL'
                },
                'Wireless Billing Dispute' => new Map<String, String>{
                    'recordTypeName' => 'SMB Care Billing',
                    'recordTypeId' => rtMap.get('SMB Care Billing').getRecordTypeId(),
                    'type' => 'Billing Dispute',
                    'origin' => 'Mobile',
                    'Case_Icon_Mapping__c'=>'Wireless Billing Dispute'
                },
                'Held Orders' => new Map<String, String>{
                    'recordTypeName' => 'SMB Care TELUS Service',
                    'recordTypeId' => rtMap.get('SMB Care TELUS Service').getRecordTypeId(),
                    'type' => 'New',
                    'origin' => 'Mobile',
                    'subType' => 'Held Order',
                    'Case_Icon_Mapping__c'=>'Held Orders'
                    },
                'PRI/Data Expedites' => new Map<String, String>{
                    'recordTypeName' => 'SMB Care TELUS Service',
                    'recordTypeId' => rtMap.get('SMB Care TELUS Service').getRecordTypeId(),
                    'type' => 'Move/Add/Change',
                    'origin' => 'Mobile',
                    'Case_Icon_Mapping__c'=>'PRI/Data Expedites'
                }
            };            
            
            // filtered sub types for each specified control type    
            subTypePicklistValues = new Map<String, Set<String>>{
                'New' => new Set<String>{'IVR/Queue wait times', 'Held Order', 'Regulatory', 'Executive and/or Media', 'TBO', 'Other'},        
                'VoIP Device Back Order' => new Set<String>{'Customer Notification', 'Order Cancellation'}        
            }; 
                
            fieldLengthValidation = new Map<String, Integer>{ 
              'subject' => SObjectType.Case.fields.subject.length,
              'description' => SObjectType.Case.fields.description.length,
                'escalation_reason__c' => SObjectType.Case.fields.escalation_reason__c.length   
            };    
        }
        catch(QueryException e){
            throw e;            
        }   
        catch(Exception e){
            throw e;
        }
    }
    
    public void updateSubTypePicklist(){ 
        if(caseRecord != null){
            caseRecord.type = caseType; 
            subTypes = getSubTypePicklistValues('Case', 'SubType__c', caseRecord.type);        
        }
    }
    
    public List<SelectOption> getSubTypePicklistValues(String ObjectApi_name, String Field_name, String type){ 
      List<SelectOption> lstPickvals = new List<SelectOption>();
    
        if(subTypePicklistValues.containsKey(type)){
            for(String value : subTypePicklistValues.get(type)){
              lstPickvals.add(new SelectOption(value, value));
            }
        }
        else{
            if(formData.get(formId).containsKey('subType')){
              lstPickvals.add(new SelectOption(formData.get(formId).get('subType'), formData.get(formId).get('subType')));            
            }
        }
        
        return lstPickvals;    
    }
            
    public void setFormInFocus(){
      isSubTypeAvailable = formId.contains(FORM_ID_HELD_ORDERS);
        
        if(!String.isBlank(formId)){            
            caseRecord.recordTypeId = formData.get(formId).get('recordTypeId');
			//added for Case Icon mapping
			caseRecord.Case_Icon_Mapping__c = formData.get(formId).get('Case_Icon_Mapping__c');
            caseRecord.type = caseRecord.type == null ? formData.get(formId).get('type') : caseRecord.type; 
            caseType = caseRecord.type;
      updateSubTypePicklist();
            
            caseRecord.origin = caseRecord.origin == null ? formData.get(formId).get('origin') : caseRecord.origin;  
            caseRecord.subType__c = caseRecord.subType__c == null && formData.get(formId).containsKey('subType') ? formData.get(formId).get('subType') : caseRecord.subType__c;  
        }
        else{
            caseRecord.Case_Icon_Mapping__c = null;
            caseRecord.recordTypeId = null;            
            caseRecord.type = null;            
            caseRecord.origin = null;                        
            caseRecord.subType__c = null;  
        }        
    }
        
    private Boolean validate(){        
        String phoneRegEx = '\\D*?(\\d\\D*?){10}';
        Pattern MyPattern = Pattern.compile(phoneRegEx);
        Matcher MyMatcher = MyPattern.matcher(caseRecord != null ? caseRecord.WTN__c : '');
        Boolean isValidWTN = MyMatcher.matches();
                        
        errorEscalationReason = '';  
        if(caseRecord != null && String.isBlank(caseRecord.Escalation_Reason__c) == true){               
            errorEscalationReason = 'The Escalation Reason field must be populated before transferring the case to the Escalations Queue.';  
            return false;
        }
        
        if(!isValidWTN){
            if(caseRecord != null){
              caseRecord.WTN__c.addError('The WTN field must be populated with a 10 digit number before transferring the case to the Escalations Queue.');                                
            }
            return false;            
        }
        
        SObjectType caseObjectType = Schema.getGlobalDescribe().get('Case');
        Map<String,Schema.SObjectField> caseFields = caseObjectType.getDescribe().fields.getMap();
        
    // field length validation   
        if(caseRecord != null){            
            for(String fieldName : fieldLengthValidation.keyset()){
                String fieldValue = (String)caseRecord.get(fieldName);        
                
                if(fieldValue != null && fieldValue.length() > fieldLengthValidation.get(fieldName)){           
                    if(fieldName.equalsIgnoreCase('subject')){
                        caseRecord.subject.addError('Subject cannot exceed ' + fieldLengthValidation.get(fieldName) + ' characters.');            
                    }
                    else if(fieldName.equalsIgnoreCase('description')){
                        caseRecord.description.addError('Description cannot exceed ' + fieldLengthValidation.get(fieldName) + ' characters.');            
                    }
                    else if(fieldName.equalsIgnoreCase('escalation_reason__c')){
                  errorEscalationReason = 'Escalation reason cannot exceed ' + fieldLengthValidation.get(fieldName) + ' characters.';  
                    }
                    
                    return false;            
                }       
            }
        }     
        
        return true;
    }    
    
    public void saveRecord() {            
        if(validate()){                
            try 
            {
                system.debug('*** caseRecord'+caseRecord);
                insert caseRecord;
            }
            catch (DMLException ex) {
                system.debug('Exception: ' + ex);
                throw ex;
            }
            catch (System.Exception ex) {
                system.debug('Exception: ' + ex);
                throw ex;
            }
        }  
    }
}