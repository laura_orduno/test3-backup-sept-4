@isTest
public with sharing class telus_Cloud_Project_SignOff_Test {
    
    private static testMethod void testSignOff() {
        insertTestExternalUserPortalSettings();
        
        SMET_Project__c a = new SMET_Project__c(Name = 'TestAccount');
        insert a;
        
        SMET_Requirement__c c = new SMET_Requirement__c(
        	Related_SMET_Project__c = a.id,
        	Name = 'Miller',
        	Order__c = 1
        );
        insert c;
        
        Cloud_Project_Sign_Off__c t = new Cloud_Project_Sign_Off__c(
        	Asignee_Name__c = 'test',
        	Assignee_Email__c = 'test@test.com',
        	Cloud_Project_Key_Deliverable__c = c.id,
        	status__c = 'test'
        );
        insert t;
        
        Note nt = new Note(
        	parentid = t.id,
        	title = 'test@test.com'
        );
        insert nt;
        
        Cloud_Project_Sign_Off__c txi = [
        	Select Token__c
        	From Cloud_Project_Sign_Off__c
        	Where Id = :t.Id
        ];
        system.assertNotEquals(null, txi.Token__c);
        
        update new Cloud_Project_Sign_Off__c(id = t.id, Status__c = 'Completed');
    }
    
    @isTest
    private static void insertTestExternalUserPortalSettings() {
    	// if org already contains custom settings, remove for test
    	delete [SELECT Id from CloudProjectPortal__c];
    	
    	CloudProjectPortal__c eups = new CloudProjectPortal__c(
    		Base_URL__c = 'http://login.salesforce.com',
    		HMAC_Digest_Key__c = '/eYPmJ0eXTb+8R/X7YE9Skv6Ly8tfChkR0gdWt9ngXg=',
    		Portal_URL__c = 'http://telus.force.com/externalservice'
    	);
    	
    	insert eups;
    }
    
    
}