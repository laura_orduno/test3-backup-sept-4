public class smb_CloneClosedCaseExtension {
/*
Author : Scott Fawcett
Date   : April 17, 2013

Description
-----------
	Allows for a Closed Case to be Cloned


History
-------
April 17, 2013 - Initial Version

*/
	Case pageCase;

	private Id CaseIdPath;
	
	public Case getpageCase() { return this.pageCase;}
	public void setpageCase(Case cs) {this.pageCase = cs;}
	public ApexPages.standardController controller {get; set;}
	
	private final Case c;
		
	public smb_CloneClosedCaseExtension(ApexPages.StandardController stdcontroller) {
		controller = stdcontroller;
		this.c = (Case)controller.getRecord();
	}
	
	public PageReference returnToCase() {
		
		PageReference pageRef1 = new PageReference('/' + CaseIdPath);
        pageRef1.setRedirect(true);
        return pageRef1;
	}
	
	
	public PageReference init() {
		
		try {
			
			Case newCase = new Case();
			
			// acquire current case
			pageCase = [SELECT IsClosed, Status, Id, RecordTypeId, AccountId, ContactId, Type, Subject,
							Outage_Case__c, Outage_Notification_Preference__c, Origin, CaseNumber, Description
							FROM Case WHERE Id =: c.Id];
							
			CaseIdPath = c.Id;			
			
			if (!pageCase.IsClosed) {
				throwExceptionMsg('This Case cannot be cloned because it is still open. Close the Case and then select the Clone option again.', 'warning');
				return null;	
			}
			
			try {
				
				newCase.RecordTypeId = pageCase.RecordTypeId;
				newCase.Status = 'New';
				newCase.AccountId = pageCase.AccountId;
				newCase.ContactId = pageCase.ContactId;
				newCase.Type = pageCase.Type;
				newCase.Subject = pageCase.Subject;
				newCase.ParentId = pageCase.Id;
				
				if (newCase.Outage_Case__c != null && newCase.Outage_Case__c)
					newCase.Outage_Notification_Preference__c = pageCase.Outage_Notification_Preference__c;	
					
				newCase.Origin = pageCase.Origin;
				
				String description = '';
				
				if (pageCase.Description != null)
					description+= pageCase.Description;
					
				description+= '\nThis case is cloned from closed case # ' + pageCase.CaseNumber;
				
				newCase.Description = description;
				newCase.Cloned_Closed_Case__c = true;
				
				insert newCase;
				
			} catch (Exception e) {
				throwExceptionMsg('An unhandled exception saving the new Case' + e, 'error');
				return null;	
			}
		
			PageReference newPage;
        	newPage = new PageReference('/' + newCase.Id + '/e');
        	newPage.getParameters().put('retURL', newCase.Id);
        
        	return newPage.setRedirect(true);		
			
	
		} catch (Exception e) {
			throwExceptionMsg('An unhandled exception occured creating the new Case : ' + e, 'error');
			return null;
		}
	}
	
	public void throwExceptionMsg(String message, String severity) {
            
        if (severity.equalsIgnoreCase('warning')) {     
            System.debug('Throwing this warning message : ' + message); 
            ApexPages.Message msg= new ApexPages.Message(ApexPages.Severity.WARNING, message, message);
            ApexPages.addmessage(msg);
        } else { //assume error
            System.debug('Throwing this error message : ' + message);   
            ApexPages.Message msg= new ApexPages.Message(ApexPages.Severity.ERROR, message, message);
            ApexPages.addmessage(msg);
        }   
    }

}