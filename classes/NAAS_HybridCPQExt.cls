/*
    *******************************************************************************************************************************
    Class Name:    NAAS_HybridCPQExt 
    Purpose:       Extends the vlocity managed class vlocity_cmt.CardCanvasController with the init mehtod to 
                   allow navigation from any Omniscript related page in NAAS Multisite Order Capture to Select & Configure page        
    Created by:        
    Modified by:      
    *******************************************************************************************************************************
*/

Global class NAAS_HybridCPQExt {

    //public vlocity_cmt.CardCanvasController controllerHybridcpqRef;
    
    // fhong - outside order flow origination
    public String addressOrigin{get;set;}
    Public String original_param_order_id;
    
    public String SelectedServiceAddress{get;set;}
    Public Id SMBCareAddressId{get;set;}

    public NAAS_HybridCPQExt(Object controller) {
            
        //this.controllerHybridcpqRef=controller;
        if(System.currentPageReference().getParameters().get('smbCareAddrId') != null) //Arpit : 3-Feb-2017 : Added null check
            SMBCareAddressId = System.currentPageReference().getParameters().get('smbCareAddrId');
        
        List<ServiceAddressOrderEntry__c> LstSAO = [SELECT Address__c, Name, ServiceAddress__c,Order__c, AccountName__c, Address__r.Address__c, OrderNumber__c, Order__r.Status, Order__r.Account.Name  FROM ServiceAddressOrderEntry__c where Address__c =:SMBCareAddressId];
       if(LstSAO.size() > 0){
           SelectedServiceAddress = LstSAO[0].ServiceAddress__c;
       }
    }
    
    public PageReference init() {
                
        String src = ApexPages.currentPage().getParameters().get('src')==null?'':ApexPages.currentPage().getParameters().get('src');
        
        original_param_order_id = ApexPages.currentPage().getParameters().get('id')==null?'':ApexPages.currentPage().getParameters().get('id');
                
        // fhong - outside order flow origination
        addressOrigin = System.currentPageReference().getParameters().get('addressOrigin');

        if(src=='os'){ // if the link to hypercpq page is coming from any of the susequent pages managed by OmniScript    
            Order theOrder = [SELECT Id, Service_Address__c, Service_Address__r.Id, Service_Address__r.Service_Account_Id__c, Service_Address__r.Service_Account_Id__r.Id, Service_Address__r.Service_Account_Id__r.ParentId, CustomerAuthorizedBy.Id FROM Order WHERE Id=: original_param_order_id ];
            ApexPages.currentPage().getParameters().put('contactId', theOrder.CustomerAuthorizedBy.Id);
            ApexPages.currentPage().getParameters().put('smbCareAddrId', theOrder.Service_Address__r.Id);
            ApexPages.currentPage().getParameters().put('ServiceAcctId', theOrder.Service_Address__r.Service_Account_Id__r.Id);
            ApexPages.currentPage().getParameters().put('ParentAccId', theOrder.Service_Address__r.Service_Account_Id__r.ParentId);
            ApexPages.currentPage().getParameters().put('orderId', original_param_order_id);
        }  
        return null; 
    }   
    
    @RemoteAction
    Global static void UpdateOrderHeader(Id SelectedServiceAddressID,Id IdOfOrder) { //
        system.debug('>>>>>>>>>>><<<<<>>>>RemoteActionCalled###SelectedServiceAddressID::::'+SelectedServiceAddressID);
        List<ServiceAddressOrderEntry__c> ListOfServiceAddressOrderEntry = [select id,Address__c,Address__r.Address__c,Address__r.Service_Account_Id__c from ServiceAddressOrderEntry__c where id=:SelectedServiceAddressID limit 1];
        List<order> ListOfordertoUpdate = [select id,Service_Address__c,Service_Address_Text__c,ServiceAccountId__c from order where id=:IdOfOrder limit 1];//and  = );
        ListOfordertoUpdate[0].Service_Address__c= ListOfServiceAddressOrderEntry[0].Address__c ;
        //ListOfordertoUpdate[0].Service_Address_Text__c=ListOfServiceAddressOrderEntry[0].Address__r.Address__c ;
        ListOfordertoUpdate[0].ServiceAccountId__c =ListOfServiceAddressOrderEntry[0].Address__r.Service_Account_Id__c;
        update ListOfordertoUpdate;
        system.debug('>>>>>>>>>>><<<<<>>>>RemoteActionCalled###UpdateOrderHeader::::'+ListOfordertoUpdate);
    }
    @RemoteAction
    Global static void AddOrderlineItems(string str) {

        //addToCart(str);
        system.debug('>>>>>>>>>>><<<<<>>>>RemoteActionCalled###'+str);
    }
    //Piyush Nandan:14-oct-2016:- To update CompletionStatus__c status of ServiceAddressOrderEntry__c on click of save button
    @RemoteAction
    Global static void saveStatus(Id orderId) {
        system.debug('orderId---------------------'+ orderId);
        List<order> ListOforder = new List<order>();
        if(!String.isEmpty(orderId)) { 
        
           ListOforder = [select id,ServiceAccountId__c,Service_Address__c from order where id=:orderId limit 1];
        }
        
        //Id ServiceAccountId = ListOforder[0].ServiceAccountId__c;
        
        system.debug(' ListOforder[0].Service_Address__c ---------------------'+ ListOforder[0].Service_Address__c);
       // system.debug('ServiceAccountId ---------------------'+ ServiceAccountId);
        List<ServiceAddressOrderEntry__c> ListOfServiceAddressOrderEntry = [select id, CompletionStatus__c from ServiceAddressOrderEntry__c where Address__c =: ListOforder[0].Service_Address__c ];
        
        if(ListOfServiceAddressOrderEntry.size()>0)
        {
            for(Integer i=0; i<ListOfServiceAddressOrderEntry.size();i++)
            {
                ListOfServiceAddressOrderEntry[i].CompletionStatus__c = 'Completed';
                update ListOfServiceAddressOrderEntry;
            }
        }
    }
    
    //Arpit : 27-Dec-2016 :Sprint-11 : Added remote method call for setting checkout readiness for each site
    @remoteAction
    global static void setCheckoutReadiness(Id serviceAddressId , Id orderId){
      /*system.debug('Inside setCheckoutReadiness'); 
      if(serviceAddressId != null && orderId != null) {      
          system.debug('serviceAddressId ----->'+ serviceAddressId );
          ServiceAddressOrderEntry__c serviceAddressOrderEntryObj = [select Id, isSiteReadyForCheckout__c, Address__c from ServiceAddressOrderEntry__c where Id =: serviceAddressId and Order__C =: orderId];
          Integer oliCountOnServiceAddress = [Select count() from OrderItem where Service_Address__c =: serviceAddressOrderEntryObj.Address__c ];
          if(!serviceAddressOrderEntryObj.isSiteReadyForCheckout__c && oliCountOnServiceAddress > 0 ){ 
              serviceAddressOrderEntryObj.isSiteReadyForCheckout__c = true;          
              update serviceAddressOrderEntryObj ;          
          }
          system.debug('serviceAddressOrderEntryObj after update -->' + serviceAddressOrderEntryObj  ); 
      }*/
    }
    
    //Arpit : 27-Dec-2016 :Sprint-11 : Added remote method call for setting checkout readiness for each site
    @remoteAction
    global static void setCheckoutReadinessFalse(Id serviceAddressId , Id orderId){
      /*system.debug('Inside setCheckoutReadinessFalse'); 
      if(serviceAddressId != null && orderId != null) {      
          system.debug('serviceAddressId ----->'+ serviceAddressId );
          system.debug('orderId----->'+ orderId);
          ServiceAddressOrderEntry__c serviceAddressOrderEntryObj = [select Id, isSiteReadyForCheckout__c, Address__c from ServiceAddressOrderEntry__c where Id =: serviceAddressId and Order__C =: orderId];
          Integer oliCountOnServiceAddress = [Select count() from OrderItem where Service_Address__c =: serviceAddressOrderEntryObj.Address__c ];
          if(serviceAddressOrderEntryObj.isSiteReadyForCheckout__c) { 
               if(oliCountOnServiceAddress >= 0){
                  serviceAddressOrderEntryObj.isSiteReadyForCheckout__c = false;
                  update serviceAddressOrderEntryObj ; 
              }  
          }           
          
          system.debug('serviceAddressOrderEntryObj after update -->' + serviceAddressOrderEntryObj  ); 
      }*/
    }
}