public class trac_Help_Viewer_Controller {
    // Input Variables
    Public String SourceId = ApexPages.currentPage().getParameters().get('id'); // The record where the Get Help button was clicked.
    Private String SourceId2 = SourceId;
    Public String Query {get; set;}
    Private String Query2 = Query;
    Public String HelpAssetID = ApexPages.currentPage().getParameters().get('haid'); // The record where the Get Help button was clicked.
    Private String HelpAssetID2 = HelpAssetID;
    Public String Show = ApexPages.currentPage().getParameters().get('p');
    Public String pageURL {get; Private set;}
    
    Public String smetSubject {get; set;}
    Public String smetDescription {get; set;}
    Public String smetCategory {get; set;}

    // Processing Variables
    Private String SourceObjectType; // Holds object type of source.
    Private sObject Source; // Holds source object.
    Private Attachment PDFfile; // Used in conjunction with SWFfile to display PDFs inline.
    Private String SWFfile; // Used in conjunction with PDFfile to display PDFs inline.
    Private String YouTubeID;
    Private Help_Asset__c ha; // A single help asset. To output, add this to HAs
    Private Map<String, List<Help_Asset__c>> ObjHAMap;
    Private Map<String, String> ObjLabelMap;

    // Output Variables
    Public Help_Asset__c[] has {get; Private set;} // A list of Help Assets to display in a list format
    Public String ObjectName {get; Private set;}
    Public String RecordId {get; Private set;}
    Public String SourceName {get; Private set;}
    
    /*
    Public Void submitRating(Integer rating) {
        ha.Number_of_votes__c = ha.Number_of_votes__c + 1;
        ha.Vote_total__c = ha.Vote_total__c + rating;
        Update ha;
    }
    */
    
    Public String getPrefixForSMETRequest() {
        String objName = 'SMET_Request__c';
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Return gd.get(objName).getDescribe().getKeyPrefix();
    }
    
    Public Boolean getWasSourceId() {
        if (SourceId == null) {
            return false;
        }
        return true;
    }
    
    Public Boolean getDisplaySingleRecord() {
        if (HAs.size() == 1 && Show != 'home') {
            return true;
        }
        return false;
    }
    
    Public Boolean getDisplaySearchResults() {
        if (HAs.size() > 1 && Show != 'home') {
            return true;
        }
        return false;
    }
    
    Public Boolean getDisplaySearchReturnedNothing() {
        if (HAs.size() == 0 && Show != 'home' && Query != null) {
            return true;
        }
        return false;
    }
    
    Public Boolean getDisplayHomePage() {
        if ((Query == null && HelpAssetID == null && SourceId2 == null) || (Show == 'home')) {
            return true;
        }
        return false;
    }
    
    Public List<SelectOption> getSMETrts() {
        List<Schema.RecordTypeInfo> rts = SMET_Request__c.SObjectType.getDescribe().getRecordTypeInfos();
        List<SelectOption> options = new List<SelectOption>();
        for (Schema.RecordTypeInfo rt : rts) {
            if (true == rt.Available) {
                if (false == rt.getName().contains('Master')) {
                    options.add(new SelectOption(rt.getRecordTypeID(), rt.getName().replace('SFDC Admin - ', '')));
                }
            }
        }
        return options;
    }
    
    Public pageReference createRequest() {
        /* SMET_Request__c r = new SMET_Request__c(Description__c = smetDescription, Subject__c = smetSubject, RecordTypeId = smetCategory, Submitted_by__c = UserInfo.getUserId());
        Insert r;
        if (null != r.Id) { */
        
            return new PageReference('/' + Schema.sObjectType.SMET_Request__c.getKeyPrefix() + '/e?RecordType=' + smetCategory + '&00N400000022onWEAQ=' + smetDescription + '&00N400000022ooF=' + smetSubject);
        // } else { return null; }
    }
    
    Public List<SMET_Request__c> getMyRequests() {
        return [Select Id, Name, Status__c, Status_Internal__c, Description__c, Subject__c From SMET_Request__c Where OwnerId = :UserInfo.getUserId() Order By CreatedDate Desc Limit 5];
    }
    
    Public List<Help_Asset__c> getFeaturedVideos() {
        List<Help_Asset__c> fhas = new List<Help_Asset__c>();
        fhas = [Select Id, Average_Rating__c, Name, Title__c, Embed_Height__c, Embed_Width__c, Help_Asset_Level__c, Help_User_Profile_Function__c,  Main_Tag__c, Object__c, PDF_ID__c, Sub_Category__c, Summary__c, YouTube_ID__c, Details__c, RecordTypeId FROM Help_Asset__c WHERE Hidden__c = false AND Featured__c = true LIMIT 5];
        return fhas;
    }
    
    Public String getSidebarHTML() {
        System.debug(objHamap);
        String treeHTML = '<ul>';
        
        List<Help_Asset__c> lhas = [Select Id, Name, Title__c, Object__c, Youtube_Id__c, PDF_id__c From Help_Asset__c Where Hidden__c = false And (Youtube_Id__c != null Or PDF_Id__c != null)];
        List<Help_Asset__c> tl;
        for (Help_Asset__c lha : lhas) {
            if (null != lha.Object__c) {
                tl = ObjHAMap.get(lha.Object__c);
                tl.add(lha);
                ObjHAMap.put(lha.Object__c, tl);
            }
        }
        Set<String> ObjStringSet = ObjHAMap.keySet();
        for (String obj : ObjStringSet) {
            if (ObjHAMap.get(obj).size() > 0) {
                String oClass = '';
                if (null != HelpAssetId && obj == has[0].Object__c) {oClass = ' jstree-open'; }
                treeHTML += '<li rel="object" class="'+oClass+'"><span class="object">' + ObjLabelMap.get(obj) + '</span><ul>';
                for (Help_Asset__c h : ObjHAMap.get(obj)) {
                    PageReference pr = ApexPages.currentPage();
                    String q = Query;
                    if (q == null) { q = ''; }
                    String t = '';
                    if (null != h.pdf_id__c) {t = 'document';}
                    if (null != h.youtube_id__c) {t = 'video';}
                    treeHTML += '<li rel="'+t+'"><a title="'+h.Title__c+'" href="/apex/Help_Viewer_New?id='+SourceId+'&haid='+h.Id+'&q='+q+'">' + h.Title__c + '</a></li>';
                }
                treeHTML += '</ul></li>';
            }
        }
        treeHTML += '</ul>';
        return treeHTML;
    }
    
    // Init Methods
    Public trac_Help_Viewer_Controller() {
    
        smetSubject = 'Subject';
        smetDescription = 'Description';
    
        if (SourceId == '' || SourceId =='null') {
            SourceId = null;
            SourceId2 = null;
        }
        has = new List<Help_Asset__c>();
        Query = ApexPages.currentPage().getParameters().get('q'); // The record where the Get Help button was clicked.
        if (Query != null && Query.length() < 2) { Query=null; }
        
        pageURL = ApexPages.currentPage().getURL();
        if (Show != 'home') {
            if (SourceId == null && HelpAssetId == null && Query == null) {
                pageURL = pageURL + '?p=home';
            } else { pageURL = pageURL + '&p=home'; }
        }
    
        // Object type finder
            Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe(); 
            Map<String,String> keyPrefixMap = new Map<String,String>{};
            Set<String> keyPrefixSet = gd.keySet();
            ObjHAMap = new Map<String, List<Help_Asset__c>>();
            ObjLabelMap = new Map<String, String>();
            for(String sObj : keyPrefixSet){
               Schema.DescribeSObjectResult r =  gd.get(sObj).getDescribe();
               String tempName = r.getName();
               String tempPrefix = r.getKeyPrefix();
               System.debug('Processing Object['+tempName + '] with Prefix ['+ tempPrefix+']');
               keyPrefixMap.put(tempPrefix,tempName);
               ObjHAMap.put(tempName, new List<Help_Asset__c>());
               ObjLabelMap.put(r.getName(), r.getLabel());
            }
            
            ObjHAMap.put('Chatter', new List<Help_Asset__c>());
            ObjLabelMap.put('Chatter', 'Chatter');
            ObjHAMap.put('Other', new List<Help_Asset__c>());
            ObjLabelMap.put('Other', 'Other');
    
        if (SourceId2 != null) {
            RecordId = SourceId;
            SourceObjectType = keyPrefixMap.get(SourceId.subString(0,3));
            ObjectName = SourceObjectType;
            System.debug(sourceobjecttype);
            
            sObject s = Database.query('SELECT Name FROM ' + ObjectName + ' WHERE Id = \'' + SourceId2 + '\' LIMIT 1');
            SourceName = String.valueof(s.get('Name'));
            if (SourceName != null) {
                SourceName = ' to ' + ObjLabelMap.get(SourceObjectType); // + ' "' + SourceName + '"';
            }
        }
        if (SourceId != null || Query != '' || HelpAssetID != null) {
            // HA ID Specified, show only this asset.
            if (HelpAssetID != null) { // Grab the Help Asset with the ID provided 
                ha = [SELECT Id, Name, Number_of_votes__c, Vote_total__c, Average_rating__c, Title__c, Embed_Height__c, Embed_Width__c, Help_Asset_Level__c, Help_User_Profile_Function__c,  Main_Tag__c, Object__c, PDF_ID__c, Sub_Category__c, Summary__c, YouTube_ID__c, Details__c, RecordTypeId From Help_Asset__c Where Id = :HelpAssetID And (Youtube_Id__c != null Or PDF_Id__c != null) LIMIT 1];
                if (ha.Id != null) {
                    has.add(ha);
                }
            }
            // Query specified, show search results
            else if (Query != null) {
                String qx = '*' + Query + '*';
                List<List<SObject>> searchList = [FIND :qx IN ALL FIELDS RETURNING Help_Asset__c (Id, Name, Number_of_votes__c, Vote_total__c, Average_rating__c, Title__c, Embed_Height__c, Embed_Width__c, Help_Asset_Level__c, Help_User_Profile_Function__c,  Main_Tag__c, Object__c, PDF_ID__c, Sub_Category__c, Summary__c, YouTube_ID__c, Details__c, RecordTypeId Where Youtube_Id__c != null Or PDF_Id__c != null)];
                Help_Asset__c [] hax = ((List<Help_Asset__c>)searchList[0]);
                if (hax.size() != 0) {
                    for (Help_Asset__c ha : hax) {
                        has.add(ha);
                    }    
                }
            }
            // No query, no HA ID... search for HAs based on object, record type etc.
            else { // Grab the first Help Asset related to this record if now query string was sent and no Help Asset ID was specified.
                has = [SELECT Id, Name, Number_of_votes__c, Vote_total__c, Average_rating__c, Title__c, Embed_Height__c, Embed_Width__c, Help_Asset_Level__c, Help_User_Profile_Function__c,  Main_Tag__c, Object__c, PDF_ID__c, Sub_Category__c, Summary__c, YouTube_ID__c, Details__c, RecordTypeId FROM Help_Asset__c Where Hidden__c = false And Object__c = :SourceObjectType And (Youtube_Id__c != null Or PDF_Id__c != null)];
            }
            for (Help_Asset__c ha : has) {
                ha.Object__c = ObjLabelMap.get(ha.Object__c);
            }
        }
    }

}