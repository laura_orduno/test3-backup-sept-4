public without sharing class QuoteCloneController {
    
    public QuoteVO quote {get; private set;}
    public Quote_Clone_Request__c request {get; private set;}
    public String requesterName {get; set;}
    public String requesterEmail {get; set;}
    
    public Web_Account__c account {get; private set;}
    
    public Boolean dealerUser {get{return QuotePortalUtils.isUserDealer();}}
    
    private String returnURL;
    
    // Request Vars
    public Boolean showRequestForm {get; set;}
    public Boolean showRequestConfirmation {get; set;}
    public String requestComment {get; set;}
    public String requestNumber {get; private set;}
    
    // Response Vars
    public Boolean showCloneForm {get; set;}
    public Boolean hasOutstandingRequest {get; set;}
    public Boolean showCloneConfirmation {get; set;}
    public String responseComment {get; set;}
    public Id selectedOwnerId {get; set;}
    
    public OwnerOption originalQuoteOwner {get; set;}
    public OwnerOption cloneRequester {get; set;}
    public OwnerOption currentUser {get; set;}
    public transient Map<Id, OwnerOption> ownerOptionMap {get; set;}
    public OwnerOption[] options {get; set;}
    public String userSearchTerm {get; set;}
    
    public class OwnerOption {
        public User user {get; set;}
        public Id id {get{return user.Id;}}
        public String name {get{return user.Name;}}
        public String email {get{return user.Email;}}
        public String channel {get{return user.Channel__c;}}
        public String rel {get; set;}
        public String optionName {get{return this.name + ' ' + this.email + ' ' + this.channel;}}
        public OwnerOption(User u) {
            user = u;
            rel = '';
        }
        public OwnerOption(User u, String rel) {
            user = u;
            this.rel = (rel==null ? '' : rel);
        }
    }
    
    public void updateSelectedOwner() {
        // Used to set the selected owner ID.
        return;
    }
    
    public PageReference findUsers() {
        try {
            resetSearchList();
            if (userSearchTerm == null || userSearchTerm == '') {
                addDefaultOwnerOptions();
            } else {
                // Actually perform the search!
                options = filterOwnerOptionsBySearchTerm(userSearchTerm.toLowercase());
            }
        } catch (Exception e) {QuoteUtilities.log(e);} return null;
    }
    
    
    private OwnerOption[] filterOwnerOptionsBySearchTerm(String term) {
        OwnerOption[] result = new List<OwnerOption>();
        
        if (term.length() < 3) {
            return result;
        }
        
        for (OwnerOption option : ownerOptionMap.values()) {
            if (option.optionName.toLowercase().contains(term)) {
                result.add(option);
            }
        }
        
        return result;
    }
    
    private void resetSearchList() {
        if (ownerOptionMap == null) {
            loadOwnerOptions();
        }
        options = new List<OwnerOption>();
        selectedOwnerId = null;
    }
    
    private void addDefaultOwnerOptions() {
        options.add(originalQuoteOwner);
        options.add(cloneRequester);
        options.add(currentUser);
    }
    
    
    private void loadOwnerOptions() {
        ownerOptionMap = new Map<Id, OwnerOption>();
            
        User originalQuoteOwnerUser = [Select Name, Email, Channel__c, Contact.Account.Name From User Where Id = :quote.record.SBQQ__SalesRep__c];
        originalQuoteOwner = new OwnerOption(originalQuoteOwnerUser, 'Original Quote Owner');
        ownerOptionMap.put(originalQuoteOwner.Id, originalQuoteOwner);
        
        if (UserInfo.getUserId() != originalQuoteOwner.id) {
            User you = [Select Name, Email, Channel__c, Contact.Account.Name From User Where Id = :UserInfo.getUserId()];
            currentUser = new OwnerOption(you, 'Yourself');
            ownerOptionMap.put(currentUser.Id, currentUser);
        }
        
        if (request != null && request.Requester__c != originalQuoteOwner.Id) {
            cloneRequester = new OwnerOption(request.Requester__r, 'Requester');
            ownerOptionMap.put(cloneRequester.Id, cloneRequester);
        }
            
        Map<Id, Profile> profiles = new Map<Id, Profile>([Select Id From Profile Where Name = 'Quote Tool - Agent' or Name = 'Quote Tool - Dealer']);
        User[] users = [Select Id, Name, Email, Channel__c, Contact.Account.Name From User Where IsActive = true and ProfileId = :profiles.keyset()];
        for (User u : users) {
            if (!ownerOptionMap.containsKey(u.id)) {
                OwnerOption oo = new OwnerOption(u);
                ownerOptionMap.put(oo.id, oo);
            }
        }
    }
    
    public QuoteCloneController() {
        try {
        returnURL = ApexPages.currentPage().getParameters().get('retURL');
        
        // If page messages are specified in the URL, display them
        String messagesURLen = ApexPages.currentPage().getParameters().get('messages');
        if (messagesURLen != null) { String[] messages = EncodingUtil.urlDecode(messagesURLen, 'UTF-8').split('\\|'); if (messages != null && messages.size() != 0) { for (String s : messages) { ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.INFO,  s) ); } } }
        } catch (Exception e) {QuoteUtilities.logNow(e);}
    }
    
    public PageReference initRequest() {
        try {
            if (!init(ApexPages.currentPage().getParameters().get('qid'))) {
                QuoteUtilities.addMessageToPage('Unable to load quote.');
                return null;
            }
            loadRequest();
            if (request != null) {
                QuoteUtilities.addMessageToPage('A clone request already exists for this quote. You cannot create another request.');
                return null;
            }
            showRequestForm = checkRequestPermissions();
            showCloneForm = false;
            showRequestConfirmation = false;
            showCloneConfirmation = false;
        } catch (Exception e) { ApexPages.addMessages(e); }
        return null;
    }
    
    public PageReference initDoClone() {
        try {
            if (!init(ApexPages.currentPage().getParameters().get('qid'))) {
                QuoteUtilities.addMessageToPage('Unable to load quote.');
                return null;
            }
            showRequestForm = false;
            showRequestConfirmation = false;
            loadRequest();
            hasOutstandingRequest = (request != null);
            if (request != null) {
                requestNumber = request.Name;
                requesterName = request.Requester_Name__c;
                requesterEmail = request.Requester_email__c;
                requestComment = request.Requester_Comment__c;
            }
            showCloneConfirmation = false;
            
            resetSearchList();
            addDefaultOwnerOptions();
            
            if (options != null && options.size() > 0) {
                selectedOwnerId = options.get(0).Id;
            }
            
            showCloneForm = checkClonePermissions();
        } catch (Exception e) { QuoteUtilities.log(e); }
        return null;
    }
    
    public void loadRequest() {
        Quote_Clone_Request__c[] requests =
        [Select Name, 
                Cloner__c,
                Cloner__r.Name,
                Cloner__r.Email,
                Cloner__r.Channel__c,
                Cloner__r.Contact.Account.Name,
                Cloner_Name__c,
                Cloner_Email__c,
                Cloner_Comment__c,
                Destination_Quote__c,
                Destination_Quote_ID__c,
                Destination_Quote_Owner_Name__c,
                Destination_Quote_Owner_Email__c,
                Source_Quote__c,
                Source_Quote_ID__c,
                Source_Quote_Owner_Name__c,
                Source_Quote_Owner_Email__c,
                Requester__c,
                Requester__r.Name,
                Requester__r.Email,
                Requester__r.Channel__c,
                Requester__r.Contact.Account.Name,
                Requester_Name__c,
                Requester_Email__c,
                Requester_Comment__c,
                Status__c
                from Quote_Clone_Request__c Where Source_Quote__c = :quote.id and Status__c = 'Sent'];
        if (requests != null && requests.size() > 0) {
            request = requests.get(0);
        }
    }
    
    public PageReference onDeclineRequest() {
        try {
            showCloneForm = false;
            if (request == null) {
                QuoteUtilities.addMessageToPage('Unable to find request. Press return to go to the quote summary page.');
            } else {
                request.Status__c = 'Declined';
                request.Cloner__c = UserInfo.getUserId();
                request.Cloner_Comment__c = responseComment;
                update request;
                QuoteUtilities.addMessageToPage('Request declined. Press return to go to the quote summary page.');
            }
        } catch (Exception e) { QuoteUtilities.log(e); }
        return null;
    }
    
    public PageReference onSubmitRequest() {
        try {
            Quote_Clone_Request__c request = new Quote_Clone_Request__c();
            request.Source_Quote__c = quote.id;
            request.Requester__c = UserInfo.getUserId();
            request.Requester_Comment__c = requestComment;
            request.Status__c = 'Sent';
            insert request;
            request = [Select Name From Quote_Clone_Request__c Where Id = :request.Id];
            requestNumber = request.Name;
            showRequestForm = false;
            showRequestConfirmation = true;
        } catch (Exception e) { QuoteUtilities.log(e); } return null;
    }
    
    private boolean init(Id quoteId) {
        try {
        SBQQ__Quote__c q = QuoteUtilities.loadQuote(quoteId);
        if (q == null) { return false; }
        quote = new QuoteVO(q);
        
        Id accountId = quote.record.SBQQ__Opportunity__r.Web_Account__c;
        account = QuotePortalUtils.loadAccountById(accountId);
        
        } catch (Exception e) { QuoteUtilities.log(e); } return true;
    }
    
    public PageReference onClone() {
        Savepoint sp = Database.setSavepoint();
        try {
        
        if (selectedOwnerId == null) {
            QuoteUtilities.addMessageToPage('You must select a new owner before continuing.');
            return null;
        }
        
        showRequestForm = false;
        showRequestConfirmation = false;
        hasOutstandingRequest = false;
        showCloneConfirmation = false;
        showCloneForm = false;
        
        if (request == null) {
            request = new Quote_Clone_Request__c(Requester__c = UserInfo.getUserId(), Source_Quote__c = quote.Id, Status__c = 'Inflight');
            insert request;
        }
        request.Cloner__c = UserInfo.getUserId();
        request.Cloner_Comment__c = responseComment;
        update request;
        
        
        QuoteCloner qc = new QuoteCloner(Quote.Id);
        if (qc.doClone()) {
            // Move them to the new quote
            PageReference pr = Page.SiteConfigureProducts;
            QuotePortalUtils.addConfigurationSummaryParams(pr);
            pr.getParameters().put('qid', qc.newQuoteId);
            pr.getParameters().put('step', 'step');
            pr.getParameters().put('retURL', Site.getCurrentSiteUrl() + Page.QuoteSummary.getUrl().replaceAll('/apex/','') + '?qid=' + qc.newQuoteId );
            pr.getParameters().put('saveURL', Site.getCurrentSiteUrl() + Page.QuoteSummary.getUrl().replaceAll('/apex/','') + '?qid=' + qc.newQuoteId );
            QuoteVO qv = new QuoteVO( QuoteUtilities.loadQuote( qc.newQuoteId ) );
            pr.getParameters().put('pids', qv.pid);
            
            SBQQ__Quote__c nq = new SBQQ__Quote__c(Id = qc.newQuoteId, SBQQ__SalesRep__c = selectedOwnerId);
            update nq;
            
            request.Status__c = 'Completed';
            String url = Site.getCurrentSiteUrl().replace('/apex/','') + pr.getURL().replace('/apex/','');
            String urlpartone = url.replace('dealerquoteportal', 'agentquoteportal');
            String urlparttwo = null;
            String urlpartthree = null;
            String urlpartfour = null;
            if (urlpartone.length() > 255) {
                urlparttwo = urlpartone.substring(254);
                urlpartone = urlpartone.substring(0,254);
                if (urlparttwo.length() > 255) {
                    urlpartthree = urlparttwo.substring(254);
                    urlparttwo = urlparttwo.substring(0, 254);
                    if (urlpartthree.length() > 255) {
                        urlpartfour = urlpartthree.substring(254);
                        urlpartthree = urlpartthree.substring(0, 254);
                        if (urlpartfour.length() > 255) {
                            urlpartfour = urlpartfour.substring(0,254);
                        }
                    }
                }
            }
            request.Quote_Detail_URL__c = urlpartone;
            request.Quote_Detail_URL_Two__c = urlparttwo;
            request.Quote_Detail_URL_Three__c = urlpartthree;
            request.Quote_Detail_URL_Four__c = urlpartfour;
            
            String durlpartone = url.replace('agentquoteportal', 'dealerquoteportal');
            String durlparttwo = null;
            String durlpartthree = null;
            String durlpartfour = null;
            if (durlpartone.length() > 255) {
                durlparttwo = durlpartone.substring(254);
                durlpartone = durlpartone.substring(0,254);
                if (durlparttwo.length() > 255) {
                    durlpartthree = durlparttwo.substring(254);
                    durlparttwo = durlparttwo.substring(0, 254);
                    if (durlpartthree.length() > 255) {
                        durlpartfour = durlpartthree.substring(254);
                        durlpartthree = durlpartthree.substring(0, 254);
                        if (durlpartfour.length() > 255) {
                            durlpartfour = durlpartfour.substring(0,254);
                        }
                    }
                }
            }
            request.Dealer_Quote_Detail_URL__c = durlpartone;
            request.Dealer_Quote_Detail_URL_Two__c = durlparttwo;
            request.Dealer_Quote_Detail_URL_Three__c = durlpartthree;
            request.Dealer_Quote_Detail_URL_Four__c = durlpartfour;
            
            request.Destination_Quote__c = qc.newQuoteId;
            update request;
            update new SBQQ__Quote__c(Id = qc.newQuoteId, SBQQ__SalesRep__c = selectedOwnerId);
            showCloneConfirmation = true;
        } else {
            Database.rollback(sp);
        }
        
        } catch (Exception e) {Database.rollback(sp); QuoteUtilities.log(e);} return null;
    }
    
    public PageReference onCancel() {
        PageReference pref = null;
        try {
        if (returnURL == null) {
            pref = Page.QuoteSummary;
            pref.getParameters().put('qid', quote.id);
        } else {
            pref = new PageReference(returnURL);
        }
        pref.setRedirect(true);
        } catch (Exception e) {QuoteUtilities.log(e);} return pref;
    }
    
    public Boolean checkClonePermissions() {
        if (!getUserCanCloneQuotes()) {
            QuoteUtilities.addMessageToPage('You are not permitted to request cloning.');
            return false;
        }
        if (!quote.isCloneable) {
            QuoteUtilities.addMessageToPage('Cloning is not currently permitted for this quote.');
            return false;
        }
        return true;
    }
    
    public Boolean getCanClone() {
        return quote.isCloneable && getUserCanCloneQuotes();
    }
    public Boolean getUserCanCloneQuotes() {
        return channelCanClone(QuotePortalUtils.getChannel());
    }
    
    public Boolean checkRequestPermissions() {
        if (!getUserCanIssueCloneRequests()) {
            QuoteUtilities.addMessageToPage('You are not permitted to request cloning.');
            return false;
        }
        if (!quote.isCloneable) {
            QuoteUtilities.addMessageToPage('Cloning is not currently permitted for this quote.');
            return false;
        }
        return true;
    }
    
    public Boolean getCanRequestClone() {
        // Temp override
        return quote.isCloneable && getUserCanIssueCloneRequests();
    }
    public Boolean getUserCanIssueCloneRequests() {
        return channelCanRequestClone(QuotePortalUtils.getChannel());
    }
    
    
    
    public static boolean channelCanClone(String channel) {
        if (channel == null) { return false; }
        channel = channel.toLowercase();
        return (channelCanCloneMap.get(channel) != null && channelCanCloneMap.get(channel) == true);
    }
    public static Map<String, Boolean> channelCanCloneMap = new Map<String, Boolean> {
        'inbound' => false,
        'outbound' => false,
        'inside sales' => false,
        'dealer' => false,
        'unassigned ee' => false,
        'assigned ee' => false,
        'pre-sales' => true,
        'post-sales' => true,
        'bacc' => true,
        'nac' => true
    };
    public static boolean channelCanRequestClone(String channel) {
        if (channel == null) { return false; }
        channel = channel.toLowercase();
        return (channelCanRequestCloneMap.get(channel) != null && channelCanRequestCloneMap.get(channel) == true);
    }
    public static Map<String, Boolean> channelCanRequestCloneMap = new Map<String, Boolean> {
        'inbound' => true,
        'outbound' => true,
        'inside sales' => true,
        'dealer' => true,
        'unassigned ee' => true,
        'assigned ee' => true,
        'pre-sales' => false,
        'post-sales' => false,
        'bacc' => false,
        'nac' => false
    };
    
}