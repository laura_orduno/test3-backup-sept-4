/**
 * @description Domain class for Case
 * @author      Dane Peterson, Traction on Demand
 * @date        2017-12-07
 */
public with sharing class trac_Case extends trac_TriggerHandlerBase {
    static Boolean afterUpdateRunOnce = false;

    public override void handleBeforeInsert() {

    	// Replaces the need for EmailToCaseTrigger.Trigger
    	EmailToCaseTriggerHandler handler = new EmailToCaseTriggerHandler();

        brandMinder_associateWithAccount.execute((List<Case>) newRecordsList);
        new smbCaseHelper( (List<Case>) newRecordsList, (Map<Id, Case>) newRecordsMap, (Map<Id, Case>) oldRecordsMap ).execute();

        trac_CaseTriggerDispatcher dis = new trac_CaseTriggerDispatcher();
        dis.init();
        dis.execute();
        dis.finish();
    }

    public override void handleBeforeUpdate() {

        // Replace brandMinder_associateWithAccount.Trigger
        brandMinder_associateWithAccount.execute((List<Case>) newRecordsList);
        new smbCaseHelper( (List<Case>) newRecordsList, (Map<Id, Case>) newRecordsMap, (Map<Id, Case>) oldRecordsMap ).execute();

        trac_CaseTriggerDispatcher dis = new trac_CaseTriggerDispatcher();
        dis.init();
        dis.execute();
        dis.finish();
    }

    public override void handleAfterInsert() {
    	B2BService_NotificationHelper.processComments((List<Case>) newRecordsList, null);
        smb_cronicTriggerOnCaseHelper.executeUpdateAccounts((List<Case>) newRecordsList, null);

       	ENTPNewCaseTriggerHelper.ENTPCreateLegacyTickets((Map<Id, Case>) newRecordsMap);
        new smbCaseHelper( (List<Case>) newRecordsList, (Map<Id, Case>) newRecordsMap, (Map<Id, Case>) oldRecordsMap ).execute();

        trac_CaseTriggerDispatcher dis = new trac_CaseTriggerDispatcher();
        dis.init();
        dis.execute();
        dis.finish();
    }


    public override void handleAfterUpdate() {
        
        // Protected from Recursion
        if(!afterUpdateRunOnce){
            B2BService_NotificationHelper.processComments((List<Case>) newRecordsList, (Map<Id, Case>) oldRecordsMap); 
        }
        new smbCaseHelper( (List<Case>) newRecordsList, (Map<Id, Case>) newRecordsMap, (Map<Id, Case>) oldRecordsMap ).execute();
        smb_cronicTriggerOnCaseHelper.executeUpdateAccounts((List<Case>) newRecordsList, (Map<Id, Case>) oldRecordsMap);

        trac_CaseTriggerDispatcher dis = new trac_CaseTriggerDispatcher();
        dis.init();
        dis.execute();
        dis.finish();

       	//VITILcareCaseTriggerHelper.afterUpdate((Map<Id,Case>) oldRecordsMap, (Map<Id,Case>) newRecordsMap);
        afterUpdateRunOnce = true;
    }

}