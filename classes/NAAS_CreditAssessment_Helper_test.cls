/*test class for NAAS_CreditAssessment_Helper class 
author:Danish Hamdani(Accenture)
date:20-feb-2017*/
@isTest
Public class NAAS_CreditAssessment_Helper_test{
   @isTest
    Public static void setupTestData(){
        String orderId= OrdrTestDataFactory.singleMethodForOrderId();
        Order ordObj=[select Account.Credit_Profile__c,AccountId,Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c FROM Order where id=:orderId];
        ordObj.Name='TestOrderforCreditCheck';
        ordObj.status='Not Submitted';
        ordObj.Rush__c=true;
        ordObj.Ban__c='12345';
        ordObj.Shipping_Address__c='test addressTestOrderforCreditCheck';
        ordObj.Credit_Check_Not_Required__c=false;
        Date day1 = Date.today();
        Date todayPlus = day1.addDays(4);
        ordObj.RequestedDate__C=todayPlus;
        update ordObj;
        
        Product2 prod2 = new Product2(Name = 'multiline prod Spec', 
                                      Family = 'Hardware4',orderMgmtId__c='9143976374713722153',vlocity_cmt__TrackAsAgreement__c=true);
        insert prod2;
        Product2 prod0 = new Product2(Name = 'Wireless Device Product Component', 
                                      Family = 'Hardware2',orderMgmtId__c='9145419241113909328',
                                      ProductSpecification__c=prod2.id);
        insert prod0;
        // Insert a test product.
        Product2 prod = new Product2(Name = 'overline', 
                                     Family = 'Hardware',OCOM_Bookable__c='Yes',ProductSpecification__c=prod0.id,orderMgmtId__c='9143976374713722151');
        insert prod;
        
         
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
              // Create a custom price book
        Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true);
        insert customPB;
        
        // 2. Insert a price book entry with a custom price.
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = customPB.Id, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
        // 2. Insert a price book entry with a custom price.
        
        Account anAccount = [select Account_Status__c from Account where id=:ordObj.AccountId limit 1];
        anAccount.Account_Status__c = 'ACTIVE';
        update anAccount;
        
        Credit_Assessment__c aCAR = new Credit_Assessment__c(CAR_Status__c='Pending');
        insert aCAR;
        
        Credit_Profile__c creditProfile=new Credit_Profile__c(name='CP-RCID', credit_reference_TL__c='123456', account__c=ordObj.accountid, RecordTypeId = Schema.SObjectType.Credit_Profile__c.getRecordTypeInfosByName().get('Business').getRecordTypeId(), Legal_Entity_Type__c='Incorporated (INC)');
        insert creditProfile;
        Orderitem anOrderItem = [select  Credit_Assessment_Required__c, vlocity_cmt__RecurringTotal__c, vlocity_cmt__OneTimeTotal__c, vlocity_cmt__ProvisioningStatus__c, amendStatus__c from OrderItem where OrderId=:orderId Limit 1]; 
        anOrderItem.Credit_Assessment_Required__c = 'Yes';
        anOrderItem.amendStatus__c = 'Add' ;
        anOrderItem.vlocity_cmt__RecurringTotal__c = 300; 
        anOrderItem.vlocity_cmt__OneTimeTotal__c = 0;
        update anOrderItem;
        
        /*Orderitem scndOrderItem = [select  Credit_Assessment_Required__c, vlocity_cmt__RecurringTotal__c, vlocity_cmt__OneTimeTotal__c, vlocity_cmt__ProvisioningStatus__c, amendStatus__c from OrderItem where OrderId=:orderId AND Id !=: anOrderItem.Id Limit 1];       
        scndOrderItem.vlocity_cmt__RecurringTotal__c=400.00;
        scndOrderItem.Credit_Assessment_Required__c = 'No';
        scndOrderItem.amendStatus__c = 'Add' ;
        update scndOrderItem;*/
       
        ordObj.Account.Credit_Profile__c =creditProfile.id;
        ordObj.Credit_Assessment__c=aCAR.id;
        update ordObj;
        
        String methodName = 'creditCheckValidation';
        Map<String,Object> inputMap = new Map<String,Object>();
        test.startTest();
            inputMap.put('ContextId',ordObj.Id);
            Map<String,Object> outputMap = new Map<String,Object>();
            Map<String,Object> options = new Map<String,Object>();
            NAAS_CreditAssessment_Helper NAAS_Credit_Helper = new NAAS_CreditAssessment_Helper();
            NAAS_Credit_Helper.invokeMethod('creditCheckValidation',inputMap,outputMap,options); 
        test.stopTest();
        
        
    }
     @isTest
    Public static void setupTestData2(){
        String orderId2= OrdrTestDataFactory.singleMethodForOrderId();
        Order scndordObj=[select Account.Credit_Profile__c,AccountId,Type,orderMgmtId_Status__c ,Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c FROM Order where id=:orderId2 Limit 1];
        scndordObj.Account.Credit_Profile__c = null;
        scndordObj.orderMgmtId_Status__c = 'Processing';
        scndordObj.status = 'Not Submitted'; 
        scndordObj.Type = 'Move';
        update scndordObj;
          integer numberOfContractableOffers = -1;
         test.startTest();
            Map<String,Object> inputMap1 = new Map<String,Object>();
            inputMap1.put('ContextId',scndordObj.Id);
            Map<String,Object> outputMap1 = new Map<String,Object>();
            Map<String,Object> options1 = new Map<String,Object>();
            NAAS_CreditAssessment_Helper NAAS_Credit_Helper1 = new NAAS_CreditAssessment_Helper();
        
            NAAS_Credit_Helper1.invokeMethod('creditCheckValidation',inputMap1,outputMap1,options1); 
        test.stopTest();
 }
    @isTest
    Public static void setupTestData3(){
        String orderId3= OrdrTestDataFactory.singleMethodForOrderId();
        Order thrdordObj=[select Account.Credit_Profile__c,AccountId,Type,orderMgmtId_Status__c ,Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c FROM Order where id=:orderId3];
        thrdordObj.Type = 'Change';
         update thrdordObj;
         Orderitem anOrderItem = [select  Credit_Assessment_Required__c, vlocity_cmt__RecurringTotal__c, vlocity_cmt__OneTimeTotal__c, vlocity_cmt__ProvisioningStatus__c, amendStatus__c from OrderItem where OrderId=:thrdordObj.id Limit 1]; 
        anOrderItem.Credit_Assessment_Required__c = 'No';
        anOrderItem.amendStatus__c = 'Add' ;
        anOrderItem.vlocity_cmt__RecurringTotal__c = 300; 
        anOrderItem.vlocity_cmt__OneTimeTotal__c = 0;
        update anOrderItem;
       
        test.startTest();
            Map<String,Object> inputMap = new Map<String,Object>();
            inputMap.put('ContextId',thrdordObj.Id);
            Map<String,Object> outputMap = new Map<String,Object>();
            Map<String,Object> options = new Map<String,Object>();
            NAAS_CreditAssessment_Helper NAAS_Credit_Helper = new NAAS_CreditAssessment_Helper();
            NAAS_Credit_Helper.invokeMethod('creditCheckValidation',inputMap,outputMap,options); 
        test.stopTest();
    
    }
    
    @isTest
     Public static void setupTestData4(){
        String orderId3= OrdrTestDataFactory.singleMethodForOrderId();
        // String orderId4= OrdrTestDataFactory.singleMethodForOrderId();
        Order thrdordObj=[select Account.Credit_Profile__c,AccountId,Type,orderMgmtId_Status__c ,Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c FROM Order where id=:orderId3];
        thrdordObj.Type = 'Change';
        update thrdordObj;
        Orderitem anOrderItem = [select  Credit_Assessment_Required__c, vlocity_cmt__RecurringTotal__c, vlocity_cmt__OneTimeTotal__c, vlocity_cmt__ProvisioningStatus__c, amendStatus__c from OrderItem where OrderId=:thrdordObj.id Limit 1]; 
        anOrderItem.Credit_Assessment_Required__c = 'Yes';
        anOrderItem.amendStatus__c = 'Add' ;
        anOrderItem.vlocity_cmt__RecurringTotal__c = 300; 
        anOrderItem.vlocity_cmt__OneTimeTotal__c = 300;
        update anOrderItem;
         Orderitem anOrderItem1 = [select  Credit_Assessment_Required__c, vlocity_cmt__RecurringTotal__c, vlocity_cmt__OneTimeTotal__c, vlocity_cmt__ProvisioningStatus__c, amendStatus__c from OrderItem where OrderId=:orderId3 Limit 1]; 
        anOrderItem1.Credit_Assessment_Required__c = 'Yes';
        anOrderItem1.amendStatus__c = 'New' ;
        anOrderItem1.vlocity_cmt__RecurringTotal__c = 30; 
        anOrderItem1.vlocity_cmt__OneTimeTotal__c = 30;
        update anOrderItem1;
        test.startTest();
            Map<String,Object> inputMap = new Map<String,Object>();
            inputMap.put('ContextId',thrdordObj.Id);
            Map<String,Object> outputMap = new Map<String,Object>();
            Map<String,Object> options = new Map<String,Object>();
            NAAS_CreditAssessment_Helper NAAS_Credit_Helper = new NAAS_CreditAssessment_Helper();
            NAAS_Credit_Helper.invokeMethod('creditCheckValidation',inputMap,outputMap,options); 
        test.stopTest();
    
    }
    
    @isTest
     Public static void setupTestData5(){
        String orderId3= OrdrTestDataFactory.singleMethodForOrderId();
        // String orderId4= OrdrTestDataFactory.singleMethodForOrderId();
        Order thrdordObj=[select Account.Credit_Profile__c,AccountId,Type,orderMgmtId_Status__c ,Credit_Check_Not_Required__c,RequestedDate__C,status,Rush__c,Ban__c,Shipping_Address__c FROM Order where id=:orderId3];
        thrdordObj.Type = 'Change';
        update thrdordObj;
        Orderitem anOrderItem = [select  Credit_Assessment_Required__c, vlocity_cmt__RecurringTotal__c, vlocity_cmt__OneTimeTotal__c, vlocity_cmt__ProvisioningStatus__c, amendStatus__c from OrderItem where OrderId=:thrdordObj.id Limit 1]; 
        anOrderItem.Credit_Assessment_Required__c = 'Yes';
        anOrderItem.amendStatus__c = 'Add' ;
        anOrderItem.vlocity_cmt__RecurringTotal__c = 300; 
        anOrderItem.vlocity_cmt__OneTimeTotal__c = 300;
       // anOrderItem.Pricebookentry.Product2.vlocity_cmt__TrackAsAgreement__c = true;
        update anOrderItem;
         Orderitem anOrderItem1 = [select  Credit_Assessment_Required__c, vlocity_cmt__RecurringTotal__c, vlocity_cmt__OneTimeTotal__c, vlocity_cmt__ProvisioningStatus__c, amendStatus__c,orderMgmtId_Status__c from OrderItem where OrderId=:orderId3 Limit 1]; 
        anOrderItem1.Credit_Assessment_Required__c = 'Yes';
        anOrderItem1.amendStatus__c = 'New' ;
        anOrderItem1.vlocity_cmt__RecurringTotal__c = 30; 
        anOrderItem1.vlocity_cmt__OneTimeTotal__c = 30;
        anOrderItem1.orderMgmtId_Status__c = 'Cancelled';
       // anOrderItem.Pricebookentry.Product2.vlocity_cmt__TrackAsAgreement__c = true;
        update anOrderItem1;
        test.startTest();
            Map<String,Object> inputMap = new Map<String,Object>();
            inputMap.put('ContextId',thrdordObj.Id);
            Map<String,Object> outputMap = new Map<String,Object>();
            Map<String,Object> options = new Map<String,Object>();
            NAAS_CreditAssessment_Helper NAAS_Credit_Helper = new NAAS_CreditAssessment_Helper();
            NAAS_Credit_Helper.invokeMethod('creditCheckValidation',inputMap,outputMap,options); 
        test.stopTest();
    
    }
    
        @isTest
        Public static void invokeMethodTestNegative(){
           test.startTest();
            Map<String,Object> inputMap = new Map<String,Object>();
            Map<String,Object> outputMap = new Map<String,Object>();
            Map<String,Object> options = new Map<String,Object>();
            NAAS_CreditAssessment_Helper NAAS_Credit_Helper = new NAAS_CreditAssessment_Helper();
            NAAS_Credit_Helper.invokeMethod('WrongNameinstring',inputMap,outputMap,options); 
        test.stopTest(); 
        }
        
    /* public static testMethod void updateOrderCarNotRequiredTest(){
            test.startTest();
            List<Account> accList = TestDataCreationFactory.createRCIDAccounts(1);
            Order orderObj = TestDataCreationFactory.createOrder(accList[0]);
             Map<String,Object> inputMap = new Map<String,Object>();
            Map<String,Object> outputMap = new Map<String,Object>();
            Map<String,Object> options = new Map<String,Object>();
            NAAS_CreditAssessment_Helper NAAS_Credit_Helper = new NAAS_CreditAssessment_Helper();
            NAAS_Credit_Helper.invokeMethod('WrongNameinstring',inputMap,outputMap,options); 
            NAAS_CreditAssessment_Helper.updateOrderCarNotRequired(orderObj , true);
            test.stopTest();
        }
      public static testMethod void updateOrderCarNotRequiredTest1(){
             test.startTest();
            List<Account> accList = TestDataCreationFactory.createRCIDAccounts(1);
            Order orderObj = TestDataCreationFactory.createOrder(accList[0]);
             Map<String,Object> inputMap = new Map<String,Object>();
            Map<String,Object> outputMap = new Map<String,Object>();
            Map<String,Object> options = new Map<String,Object>();
            NAAS_CreditAssessment_Helper NAAS_Credit_Helper = new NAAS_CreditAssessment_Helper();
            NAAS_Credit_Helper.invokeMethod('WrongNameinstring',inputMap,outputMap,options); 
            NAAS_CreditAssessment_Helper.updateOrderCarNotRequired(orderObj ,false);
            test.stopTest();
        }*/
}