public class ENTPNewCaseCtlr2_new {
	public class NewCaseException extends Exception {
	}
	String userAccountId;
	public string CatListPull = '';
	private static final Map<String, String> requestTypeRecordTypeMap;
	private static final Map<String, String> recordTypeRequestTypeMap;
	private static final Map<String, Id> caseRTNameToId;
	private static final Map<String, List<String>> typeFieldsOrder;
	private static final Map<String, List<ParentToChild__c>> ptcListByCategoryMap;

	private static final Map<String, List<String>> categoryRequestTypesMap;
	private static final Map<String, String> requestTypesCategoryMap;
	public string LynxTickNum { get; set; }
	public static Map<String, String> reqTypeFormIdMap { get; set; }
	public String reqTypeFormIdMapJSON { get {return JSON.serialize(reqTypeFormIdMap);} }
	public string lastname { get; set; }
	public string ticketType { get; set; }
	public string firstname { get; set; }
	public string flastname { get; set; }
	public String ReportedByCompany1 { get; set; }
	public String ReportedByPhNum1 { get; set; }
	public String ReportedByEm1 { get; set; }
	public string JEMail { get; set; }
	public string JSPhone { get; set; }
	public string VoiceIssueType { get; set; }
	public static String userLanguage {
		get {
			return PortalUtils.getUserLanguage();
		}
		set;
	}
//test
	public Boolean createNewOnSubmit {
		get {
			if (createNewOnSubmit == null) return false;
			return createNewOnSubmit;
		}
		set;
	}

	static {
		List<TypeToFields__c> ttfs = TypeToFields__c.getAll().values();
		typeFieldsOrder = new Map<String, List<String>>();

		//  List<ExternalToInternal__c> etis = ExternalToInternal__c.getAll().values();
		// List<ExternalToInternal__c> etis = [Select Name, External__c, External_FR__c, Internal__c, Identifier__c from ExternalToInternal__c where Identifier__c = 'onlineRequestTypeENTP' ];
		List<ExternalToInternal__c> etis = [Select Name, External__c, External_FR__c, Internal__c, Identifier__c from ExternalToInternal__c where Identifier__c = 'onlineStatus'];
		requestTypeRecordTypeMap = new Map<String, String>();
		recordTypeRequestTypeMap = new Map<String, String>();
		for (ExternalToInternal__c eti : etis) {
			if (eti.Identifier__c != null && eti.Identifier__c.equals('onlineStatus')) {
				requestTypeRecordTypeMap.put(eti.External__c, eti.Internal__c);
				recordTypeRequestTypeMap.put(eti.Internal__c, eti.External__c);
				// system.debug('requestTypeRecordTypeMap: '+requestTypeRecordTypeMap.put(eti.External__c,eti.Internal__c));
				// system.debug('recordTypeRequestTypeMap: '+recordTypeRequestTypeMap.put(eti.Internal__c,eti.External__c));
			}
		}

		List<ParentToChild__c> ptcs = ParentToChild__c.getAll().values();
		ptcListByCategoryMap = new Map<String, List<ParentToChild__c>>();
		categoryRequestTypesMap = new Map<String, List<String>>();
		requestTypesCategoryMap = new Map<String, String>();
		reqTypeFormIdMap = new Map<String, String>();
		reqTypeFormIdMap.put(label.mbrSelectType, '');
		for (ParentToChild__c ptc : ptcs) {
			if (ptc.Identifier__c.equals('ENTPCatRequest')) {
				// build ptc list map so it can be used for sorting the request type(child) entries
				if (!ptcListByCategoryMap.containsKey(ptc.parent__c)) {
					ptcListByCategoryMap.put(ptc.parent__c, new List<ParentToChild__c>());
				}
				List<ParentToChild__c> ptcListForCategory = ptcListByCategoryMap.get(ptc.parent__c);
				ptcListForCategory.add(ptc);
				ptcListByCategoryMap.put(ptc.parent__c, ptcListForCategory);
				////////////////////////////////////////////////////////////////////////////////////
				requestTypesCategoryMap.put(ptc.Child__c, ptc.Parent__c);
				if (ptc.FormComponent__c == null || ptc.FormComponent__c.equals('')) {
					reqTypeFormIdMap.put(ptc.Child__c, 'descriptionField');
				} else {
					reqTypeFormIdMap.put(ptc.Child__c, ptc.FormComponent__c);
				}
			}
		}

		// perform ptc list sorting for each category
		Map<Integer, ParentToChild__c> ptcOrderedMap = new Map<Integer, ParentToChild__c>();
		List<ParentToChild__c> ptcOrphanedList = new List<ParentToChild__c>();

		for (String category : ptcListByCategoryMap.keyset()) {
			Integer minOrderNumber = null;
			Integer maxOrderNumber = null;

			ptcOrderedMap.clear();
			ptcOrphanedList.clear();
			List<ParentToChild__c> ptcSortedList = new List<ParentToChild__c>();

			for (ParentToChild__c ptc : ptcListByCategoryMap.get(category)) {
				Integer orderNumber = Integer.valueOf(ptc.order_number__c);
				// init
				minOrderNumber = minOrderNumber == null ? orderNumber : minOrderNumber;
				maxOrderNumber = maxOrderNumber == null ? orderNumber : maxOrderNumber;
				if (orderNumber == null || ptcOrderedMap.containsKey(orderNumber)) {
					ptcOrphanedList.add(ptc);
				} else {
					// update
					minOrderNumber = orderNumber < minOrderNumber ? orderNumber : minOrderNumber;
					maxOrderNumber = orderNumber > maxOrderNumber ? orderNumber : maxOrderNumber;
					ptcOrderedMap.put(orderNumber, ptc);
				}
			}

			for (Integer key = minOrderNumber; key <= maxOrderNumber; key++) {
				if (ptcOrderedMap.containsKey(key)) {
					ParentToChild__c ptc = ptcOrderedMap.get(key);
					ptcSortedList.add(ptc);
				}
			}

			ptcSortedList.addAll(ptcOrphanedList);

			List<String> ptcSortedStringList = new List<String>();
			for (ParentToChild__c ptc : ptcSortedList) {
				ptcSortedStringList.add(ptc.child__c);
			}
			categoryRequestTypesMap.put(category, ptcSortedStringList);
		}
		/////////////////////////////////////////////////////////////////////////////////////

		for (String s : categoryRequestTypesMap.keySet()) {
			// system.debug('categoryRequestTypesMap: '+s+' values: '+categoryRequestTypesMap.get(s));
		}

		Schema.DescribeSObjectResult caseToken = Schema.SObjectType.Case;
		Map<String, RecordTypeInfo> nameToRTInfo = caseToken.getRecordTypeInfosByName();
		caseRTNameToId = new Map<String, Id>();
		for (String s : nameToRTInfo.keyset()) {
			caseRTNameToId.put(s, nameToRTInfo.get(s).getRecordTypeId());
		}
	}

	/* Input Variables */
	public String selectedCategory { get; set; } {
		selectedCategory = Label.MBRSelectCategory;
	}

	public Case caseToInsert { get; set; }
	public static String parentCaseType;
	public static String parentCategory;
	public ENTPFormWrapper formWrapper { get; set; }
	public Account account { get; private set; }
	public Boolean caseSubmitted { get; set; }
	public String requestType {
		get;
		set {
			requestType = value;
			if (requestTypeRecordTypeMap.get(requestType) != null && caseRTNameToId.get(requestTypeRecordTypeMap.get(requestType)) != null) {
				caseToInsert.recordtypeid = caseRTNameToId.get(requestTypeRecordTypeMap.get(requestType));
			}
		}
	} {
		requestType = 'ENT Care Assure';
	}

	public Attachment attachment {
		get {
			if (attachment == null)
				attachment = new Attachment();
			return attachment;
		}
		set;
	}

	public ENTPNewCaseCtlr2_new() {
		priorityError = '';
		typeError = '';
		formWrapper = new ENTPFormWrapper();

		ENTP_Customer_Interface_Settings__c cis = ENTP_Customer_Interface_Settings__c.getInstance();
		if (cis.Case_Origin_Value__c != null) {
//System.debug('CASE ORIGIN SETTING: ' + cis.Case_Origin_Value__c);
			caseToInsert = new case(Origin = cis.Case_Origin_Value__c, NotifyCustomer__c = true);
			// caseToInsert = new case(Origin = 'ENTP', NotifyCustomer__c=true);
		} else {
//System.debug('CASE ORIGIN: ENTP');
			caseToInsert = new case(Origin = 'ENTP', NotifyCustomer__c = true);
		}
		caseSubmitted = false;

		if (ApexPages.currentPage() != null) {
			String parentCaseNumber = ApexPages.currentPage().getParameters().get('reopen');
			if (parentCaseNumber != null) {
				List<Case> parentCaseList = [SELECT Id, Subject, Description, My_Business_Requests_Type__c, LastModifiedDate, NotifyCollaboratorString__c, CreatedDate, CaseNumber, recordType.name, Type, Status, Lynx_Ticket_Number__c FROM Case WHERE CaseNumber = :parentCaseNumber AND Status = 'Closed'];
				if (parentCaseList.size() > 0) {
					caseToInsert.Parent = parentCaseList[0];
					LynxTickNum = parentCaseList[0].Lynx_Ticket_Number__c;
					// System.debug('LynxTicketNumber: ' + LynxTickNum);
					caseToInsert.ParentId = parentCaseList[0].Id;
					caseToInsert.Subject = Label.VITILcareRequestSubjectreopen + LynxTickNum + '] ' + caseToInsert.Parent.Subject;
					List<String> lines = new List<String>();
					if (caseToInsert.Parent.Description != null) {
						lines = caseToInsert.Parent.Description.split('\n');
					}
					String description = '';
					for (String s : lines) {
						description += '\n> ' + s;
					}
					caseToInsert.Description = '\n\n\n\n' + Label.VITILcareSubmitRequestReopenDescp + LynxTickNum + '\n>' + description;
					caseToInsert.NotifyCollaboratorString__c = caseToInsert.Parent.NotifyCollaboratorString__c;
					parentCaseType = caseToInsert.Parent.My_Business_Requests_Type__c;
					//   System.debug('requestTypesCategoryMap: ' + requestTypesCategoryMap);
					//    System.debug('parentCaseType: ' + parentCaseType);
					if (parentCaseType != null && requestTypesCategoryMap.get(parentCaseType) != null) {
						parentCategory = requestTypesCategoryMap.get(parentCaseType);
						selectedCategory = parentCategory;
						requestType = parentCaseType;
					}
				} else {
					// invalid parent caseNumber specified...
				}
			}
		}
	}

	public transient String pageValidationError { get; set; }
	public transient String priorityError { get; set; }
	public transient String typeError { get; set; }

	public PageReference initCheck() {
		ENTPFormWrapper.GetTicketInfoAct getTicketInfo = formWrapper.getTicketInfo;
		//  System.debug('Subject: ' + getTicketInfo.subject);
		//   System.debug('CaseToInsert.Subject: ' + caseToInsert.subject);

		//append request type in front of subject

		//String userAccountId;
		//userAccountId = (String)UserUtil.CurrentUser.AccountId;
		String userAccountId;
		List<User> users = [SELECT Id, AccountId, LastName, FirstName, phone, email, Contact.Account.Name FROM User WHERE Id = :UserInfo.getUserId()];
		if (users.size() > 0) {
			userAccountId = users[0].AccountId;
			//    System.debug('Line 223: Account ID: ' + userAccountId);
			lastname = users[0].LastName;
			//    System.debug('Line 221: LastName: ' + lastname);
			firstname = users[0].FirstName;
			//   System.debug('Line 223: firstname: ' + firstname);
			JEMail = users[0].email;
			JSPhone = users[0].phone;
			flastname = firstname + ' ' + lastname;
			ReportedByCompany1 = users[0].Contact.Account.Name;
			ReportedByPhNum1 = users[0].phone;
			ReportedByEm1 = users[0].email;
		}
		Id aid = (Id) userAccountId;
		if (caseToInsert.Parent == null) {
			//  System.debug('Line 227: caseToInsert.Parent: ' + caseToInsert.Parent);
			resetValues();
		}
		// System.debug('Line 230: Account ID: ' + aid);
		caseToInsert.AccountId = aid;
		return null;
	}

	public void resetValues() {
		caseToInsert.description = '';
		caseToInsert.subject = '';
		requestType = 'ENT Care Assure';
		String categoryType = ApexPages.currentPage().getParameters().get('category');
		if (caseSubmitted) {
			selectedCategory = Label.MBRSelectCategory;
		} else if (categoryType != null) {
			ParentToChild__c categType = ParentToChild__c.getAll().get(categoryType);
			selectedCategory = categType.Parent__c;
			categoryType = null;
		}
	}

	public String getRecordType(String externalName) {
		String typeName = '';
		// String typeName = 'ENT Care Assure';
//System.debug('externalName: ' + externalName);
		if (!Test.isRunningTest()) {
			//return typeName;
			List<ExternalToInternal__c> etis = ExternalToInternal__c.getAll().values();

			for (ExternalToInternal__c eti : etis) {
//System.debug('eti.Identifier__c: ' + eti.Identifier__c);
//System.debug('eti.External__c: ' + eti.External__c);
				if (eti.Identifier__c.equals('onlineRequestTypeENTP') && eti.External__c.equals(externalName)) {
					string userLanguageRecType = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;

					if (userLanguageRecType == 'en_US') {
						typeName = eti.Internal__c;
					}
					if (userLanguageRecType == 'fr' || userLanguageRecType == 'fr_CA') {
						typeName = eti.Internal_FR__c;

					}
				}
			}

		} else {
			typeName = 'ENT Care Assure';
		}
		return typeName;
	}

	public PageReference createNewCase() {
//System.debug('ENTPNewCase::createNewCase, caseToInsert: ' + caseToInsert);
		PageReference submitNewPage = Page.ENTPNewCase_new;
		submitNewPage.setRedirect(true);
		submitNewPage.getParameters().put('previousCaseSubmitted', '1');

		if (caseToInsert == null) {
//System.debug('ENTPNewCase::return null 267');
			return null;
		}

		System.debug('requestType: ' + requestType);
		String recType = getRecordType(requestType);
		System.debug('recType: ' + recType);
		System.debug('Schema.SObjectType.Case.RecordTypeInfosByName: ' + Schema.SObjectType.Case.RecordTypeInfosByName);
		Id recTypeId = Schema.SObjectType.Case.RecordTypeInfosByName.get(recType).RecordTypeId;
		System.debug('recTypeId: ' + recTypeId);
		if (recTypeId != null) {
			caseToInsert.recordtypeid = recTypeId;
		}

		if (caseToInsert.recordtypeid == null) {
			pageValidationError = label.mbrMissingRecordType;
//System.debug('ENTPNewCase::createNewCase, error: ' + pageValidationError);
//System.debug('ENTPNewCase::return null 280');
			return null;
		}

		caseToInsert.My_Business_Requests_Type__c = requestType;

		ENTPFormWrapper.GetTicketInfoAct getTicketInfo = formWrapper.getTicketInfo;
		//  System.debug('Subject: ' + getTicketInfo.subject);
		//   System.debug('CaseToInsert.Subject: ' + caseToInsert.subject);
		string ticketType = '';
		//append request type in front of subject

		if (caseToInsert.subject == null || caseToInsert.subject.equals('')) {

			caseToInsert.Subject = getTicketInfo.subject;
			//   System.debug('CaseToInsert.Subject: ' + caseToInsert.subject);
		}
		if (caseToInsert.Customer_First_Name_contact__c == null || caseToInsert.Customer_First_Name_contact__c.equals('')) {
			//RB - added reportedby info coming from form or coming from contact info
			if (String.isNotBlank(getTicketInfo.ReportedBy)) {
				caseToInsert.Customer_First_Name_contact__c = getTicketInfo.ReportedBy;
			} else {
				caseToInsert.Customer_First_Name_contact__c = flastname;
			}
			//System.debug('CaseToInsert.Customer_First_Name_contact__c: ' + caseToInsert.Customer_First_Name_contact__c);
		}
		if (caseToInsert.Customer_Phone_contact__c == null || caseToInsert.Customer_Phone_contact__c .equals('')) {
			//RB - added reportedby phone Num info coming from form or coming from contact info
			if (String.isNotBlank(getTicketInfo.ReportedByPhNum)) {
				caseToInsert.Customer_Phone_contact__c = getTicketInfo.ReportedByPhNum;
			} else {
				caseToInsert.Customer_Phone_contact__c = ReportedByPhNum1;
			}
			//  System.debug('CaseToInsert.Customer_Phone_contact__c : ' + caseToInsert.Customer_Phone_contact__c );
			//}
		}
		if (caseToInsert.ENTP_Condition__c == null || caseToInsert.ENTP_Condition__c.equals('')) {
			caseToInsert.ENTP_Condition__c = getTicketInfo.ConditionTypes;
			System.debug('CaseToInsert.ENTP_Condition__c : ' + caseToInsert.ENTP_Condition__c);
		}
		if (caseToInsert.ENTP_AccessDay__c == null || caseToInsert.ENTP_AccessDay__c.equals('')) {
			caseToInsert.ENTP_AccessDay__c = getTicketInfo.AccessHours;
			// System.debug('CaseToInsert.ENTP_AccessDay__c : ' + caseToInsert.ENTP_AccessDay__c );
		}
		if (caseToInsert.Project_number__c == null || caseToInsert.Project_number__c.equals('')) {
			Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9]');
			string projNum = getTicketInfo.projectNum.touppercase();
			projNum = projNum.remove('-');
			//  system.debug('projNum='+projNum);
			if (projNum.containsWhitespace()) {
				string projNumNW = projNum.deleteWhitespace();

				Matcher matcher = nonAlphanumeric.matcher(projNumNW);
				projNumNw = matcher.replaceAll('');
				// system.debug('projNumNW='+projNumNW);

				caseToInsert.Project_number__c = projNumNw;
			} else {
				Matcher matcher = nonAlphanumeric.matcher(projNum);
				projNum = matcher.replaceAll('');
				caseToInsert.Project_number__c = projNum;
			}

			ticketType = 'Mainstream';

		}
		if (caseToInsert.Project_number__c == null || caseToInsert.Project_number__c.equals('')) {
			caseToInsert.Project_number__c = getTicketInfo.CSID;
			//  System.debug('CaseToInsert.Proj Num - Enhanced: ' +  caseToInsert.Project_number__c);
			ticketType = 'Enhanced';
		}
		if (caseToInsert.Project_number__c == null || caseToInsert.Project_number__c.equals('')) {
			caseToInsert.Project_number__c = getTicketInfo.phNum;
			ticketType = 'Voice';
		}
		if (caseToInsert.Project_number__c == null || caseToInsert.Project_number__c.equals('')) {
			caseToInsert.Project_number__c = getTicketInfo.PRINum;
			ticketType = 'Voice-PRI';
		}
		if (caseToInsert.Project_number__c == null || caseToInsert.Project_number__c.equals('')) {
			Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9]');
			string projNum = getTicketInfo.circuitNum.touppercase();
			projNum = projNum.remove('-');

			//  system.debug('projNum='+projNum);
			if (projNum.containsWhitespace()) {
				string projNumNW = projNum.deleteWhitespace();

				Matcher matcher = nonAlphanumeric.matcher(projNumNW);
				projNumNw = matcher.replaceAll('');
				//   system.debug('projNumNW='+projNumNW);
				caseToInsert.Project_number__c = projNumNW;
			} else {
				Matcher matcher = nonAlphanumeric.matcher(projNum);
				projNum = matcher.replaceAll('');
				caseToInsert.Project_number__c = projNum;
			}

			ticketType = 'L1L';
			caseToInsert.ENTP_AccessDay__c = getTicketInfo.AccessHoursL1L;
		}
		//  if(caseToInsert.Contact_Phone_Number2__c == null || caseToInsert.Contact_Phone_Number2__c.equals('')){

		caseToInsert.Contact_Phone_Number2__c = getTicketInfo.OnsiteContactNumber;
		// }
		if (ticketType != '') {
			caseToInsert.My_Business_Requests_Type__c = ticketType;
		}

		if (caseToInsert.Trouble_Ticket__c == null || caseToInsert.Trouble_Ticket__c.equals('')) {
			Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9]');
			string getCustTickNum = getTicketInfo.CustomerTicketNum;
			if (getCustTickNum.containsWhitespace()) {
				string CustTickNum = getCustTickNum.deleteWhitespace();

				Matcher matcher = nonAlphanumeric.matcher(CustTickNum);
				CustTickNum = matcher.replaceAll('');
				//   system.debug('CustTickNum='+CustTickNum);
				caseToInsert.Trouble_Ticket__c = CustTickNum;
			} else {
				Matcher matcher = nonAlphanumeric.matcher(getCustTickNum);
				getCustTickNum = matcher.replaceAll('');
				caseToInsert.Trouble_Ticket__c = getCustTickNum;
			}

			//caseToInsert.Trouble_Ticket__c  = getTicketInfo.CustomerTicketNum;
		}

		/*  if(ticketType == 'Voice' || ticketType == 'Voice-PRI'){
			 if(!Test.isRunningTest()){
		   if(String.isNotBlank(getTicketInfo.selectedOption)){
			   VoiceIssueType =  getTicketInfo.selectedOption;
	  System.debug('getTicketInfo.selectedOption: ' +  getTicketInfo.selectedOption);
		   }
		if(String.isBlank(getTicketInfo.selectedOption)){
			  pageValidationError = 'You have errors in your form submission. Please choose at least 1 issue type';
		   // TypeOfIncidentError = 'Please choose at least 1 incident type';
  System.debug('ENTPNewCase::createNewCase, error, TypeofIncident: ' + pageValidationError);
			  //return null;
			// ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select at least one incident type'));
	  return null;
		  }
		   }
		  }*/
		// Added for running Case Assignment Rules -- Dan
		Database.DMLOptions dml = new Database.DMLOptions();
		dml.AssignmentRuleHeader.useDefaultRule = true;
		if (caseToInsert.description == null || caseToInsert.description.equals('')) {
			caseToInsert.description = '';
			caseToInsert.description += '\n' + populateDescription(requestType);
		}
		caseToInsert.Ticketing_System__c = 'LYNX';

//System.debug('ENTPNewCase::caseToInsert.id before: ' + caseToInsert.id);

		Database.Saveresult sr = Database.insert(caseToInsert, dml);

//System.debug('ENTPNewCase::caseToInsert.id: ' + caseToInsert.id);
//System.debug('sr.isSuccess(): ' + sr.isSuccess());

		if (!sr.isSuccess()) {
			for (Database.Error error : sr.getErrors()) {
				pageValidationError = error.getMessage();
			}
//System.debug('ENTPNewCase::createNewCase, DML error: ' + pageValidationError);
		}

		if (caseToInsert.Id == null) {
//System.debug('ENTPNewCase::createNewCase, DML error: case id null');
//System.debug('ENTPNewCase::return null 339');
			return null;
		} else {
			if (attachment.body != null && attachment.body.size() > 0) {
				attachment.OwnerId = UserInfo.getUserId();
				attachment.ParentId = caseToInsert.id;
				attachment.IsPrivate = false;
				caseToInsert.description += '\n\n' + Label.MBR_See_Attachment + attachment.name;
				try {
					insert attachment;
					update caseToInsert;
				} catch (DMLException e) {
					ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error uploading attachment'));
//System.debug('ENTPNewCase::return null 351');
					return null;
				} finally {
					attachment = new Attachment();
				}
				ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Attachment uploaded successfully'));
			}

			//SendLynxCreate(caseToInsert.id, ticketType); /*Send the request to Lynx for ticket add */

			PageReference page = System.Page.ENTPCaseDetailPage_new;
//System.debug('ENTPNewCase::caseToInsert.id?: ' + caseToInsert.id);

			try {
				Case c = [Select id, casenumber from Case where id = :caseToInsert.id LIMIT 1];
// *****            c = [Select id, casenumber from Case where id = :caseToInsert.id LIMIT 1];
				page.getParameters().put('caseNumber', c.casenumber);
			} catch (QueryException e) {
				page.getParameters().put('caseNumber', '');
			}
			page.getParameters().put('TicketType', 'New');
			page.setRedirect(true);
			caseSubmitted = true;
			caseToInsert = new case();
			selectedCategory = label.mbrSelectCategory;

//System.debug('ENTPNewCase::return: ' + createNewOnSubmit);

			return createNewOnSubmit ? submitNewPage : page;
		}
	}

	public String populateDescription(String reqType) {
		Integer index = 1;
		String description = '';
		String formId = reqTypeFormIdMap.get(reqType);
		ENTPFormWrapper.GetTicketInfoAct getTicketInfo = formWrapper.getTicketInfo;

		/*  system.debug('VoiceIssueType:'+getTicketInfo.VoiceIssueType);*/
		if (getTicketInfo.VoiceIssueType.size() > 0) {
			description = '\n<br/>' + Label.ENTPVoiceIssues + ': ' + getTicketInfo.VoiceIssueType;
			// system.debug('VoiceIssueType:'+getTicketInfo.VoiceIssueType );
			/* if(String.isNotBlank(getTicketInfo.selectedOption)){
				description = '\n<br/>Voice Issues: ' + getTicketInfo.selectedOption ;*/
		}
		if (String.isNotBlank(getTicketInfo.subject)) {
			description += '\n\n<br/>' + Label.Subject + ': ' + getTicketInfo.subject;
		}

		if (String.isNotBlank(getTicketInfo.additionalNote)) {
			description += '\n<br/>' + Label.TTDetailComments + ': ' + getTicketInfo.additionalNote;
		}
		if (String.isNotBlank(getTicketInfo.projectNum)) {
			Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9]');
			string projNum = getTicketInfo.projectNum.touppercase();
			projNum = projNum.remove('-');
			// system.debug('projNum='+projNum);

			description += '\n\n<br/>' + Label.ENTPCircuitNum + ': ';
			if (projNum.containsWhitespace()) {
				string projNumNW = projNum.deleteWhitespace();
				//  system.debug('projNumNW='+projNumNW);
				Matcher matcher = nonAlphanumeric.matcher(projNumNW);
				//  system.debug(matcher.replaceAll(''));
				projNumNW = matcher.replaceAll('');
				description += projNumNW;
			} else {
				Matcher matcher = nonAlphanumeric.matcher(projNum);
				//  system.debug(matcher.replaceAll(''));
				projNum = matcher.replaceAll('');
				description += projNum;
			}

		}
		if (String.isNotBlank(getTicketInfo.CSID)) {
			description += '\n\n<br/>' + Label.ENTPTPSCSID + ': ' + getTicketInfo.CSID;
		}
		if (String.isNotBlank(getTicketInfo.phNum)) {
			description += '\n\n<br/>' + Label.ENTPPhoneNumAffect + ': ' + getTicketInfo.phNum;
		}
		if (String.isNotBlank(getTicketInfo.PRINum)) {
			description += '\n\n<br/>PRI#: ' + getTicketInfo.PRINum;
		}
		if (String.isNotBlank(getTicketInfo.circuitNum)) {
			Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9]');
			string projNum = getTicketInfo.circuitNum.touppercase();
			projNum = projNum.remove('-');
			//   system.debug('projNum='+projNum);

			description += '\n\n<br/>' + Label.ENTPCircuitNum + ': ';
			if (projNum.containsWhitespace()) {
				string projNumNW = projNum.deleteWhitespace();
				//  system.debug('projNumNW='+projNumNW);
				Matcher matcher = nonAlphanumeric.matcher(projNumNW);
				// system.debug(matcher.replaceAll(''));
				projNumNW = matcher.replaceAll('');
				description += projNumNW;
			} else {
				Matcher matcher = nonAlphanumeric.matcher(projNum);
				// system.debug(matcher.replaceAll(''));
				projNum = matcher.replaceAll('');
				description += projNum;
			}

			//description += '\n\n<br/>Circuit Number#: ' + getTicketInfo.circuitNum;
		}

		if (String.isNotBlank(getTicketInfo.siteAddress)) {
			description += '\n<br/>' + Label.ENTPSiteAddressLabel + ': ' + getTicketInfo.siteAddress;
		}

		/************* If ReportedBy is blank use user contact ******/
		if (String.isNotBlank(getTicketInfo.ReportedBy)) {
			description += '\n<br/>' + Label.ENTPReportedByNameLabel + ': ' + getTicketInfo.ReportedBy;
		} else {
			description += '\n<br/>' + Label.ENTPReportedByNameLabel + ': ' + flastname;
		}

		if (String.isNotBlank(getTicketInfo.ReportedByCompany)) {
			description += '\n<br/>' + Label.ENTPReportByCompany + ': ' + getTicketInfo.ReportedByCompany;
		} else {
			description += '\n<br/>' + Label.ENTPReportByCompany + ': ' + ReportedByCompany1;
		}
		if (String.isNotBlank(getTicketInfo.ReportedByPhNum)) {
			description += '\n<br/>' + Label.ENTPReportedbyPhNumber + ': ' + getTicketInfo.ReportedByPhNum;
		} else {
			description += '\n<br/>' + Label.ENTPReportedbyPhNumber + ': ' + ReportedByPhNum1;
		}
		if (String.isNotBlank(getTicketInfo.ReportedByEm)) {
			description += '\n<br/>' + Label.ENTPReportedByEmail + ': ' + getTicketInfo.ReportedByEm;
		} else {
			description += '\n<br/>' + Label.ENTPReportedByEmail + ': ' + ReportedByEm1;
		}
		/*******************************************************/
		if (String.isNotBlank(getTicketInfo.OnsiteContactName)) {
			description += '\n<br/>' + Label.ENTPReportedByOnsiteName + ': ' + getTicketInfo.OnsiteContactName;
		}
		if (String.isNotBlank(getTicketInfo.OnsiteContactNumber)) {
			description += '\n<br/>' + Label.ENTPOnsiteContactPhNumber + ': ' + getTicketInfo.OnsiteContactNumber;
		}
		if (String.isNotBlank(getTicketInfo.SiteCompanyName)) {
			description += '\n<br/>' + Label.VITILcareSiteCompanyLabel + ': ' + getTicketInfo.SiteCompanyName;
		}
		if (String.isNotBlank(getTicketInfo.AccessHours)) {
			description += '\n<br/>' + Label.VITILcareAccessHoursTitle + ': ' + getTicketInfo.AccessHours;
		}
		if (ticketType == 'L1L') {
			description += '\n<br/>' + Label.VITILcareAccessHoursTitle + ': ' + getTicketInfo.AccessHoursL1L;
		}
		if (String.isNotBlank(getTicketInfo.CustomerTicketNum)) {
			Pattern nonAlphanumeric = Pattern.compile('[^a-zA-Z0-9]');
			string getCustTickNum = getTicketInfo.CustomerTicketNum;
			getCustTickNum = getCustTickNum.remove('-');
			//   system.debug('projNum='+projNum);

			description += '\n\n<br/>' + Label.VITILcareCustNumTitle + ': ';

			if (getCustTickNum.containsWhitespace()) {
				string CustTickNum = getCustTickNum.deleteWhitespace();

				Matcher matcher = nonAlphanumeric.matcher(CustTickNum);
				CustTickNum = matcher.replaceAll('');
				//   system.debug('CustTickNum='+CustTickNum);
				description += '\n<br/>' + Label.VITILcareCustNumTitle + ': ' + CustTickNum;
			} else {
				Matcher matcher = nonAlphanumeric.matcher(getCustTickNum);
				getCustTickNum = matcher.replaceAll('');
				description += '\n<br/>' + Label.VITILcareCustNumTitle + ': ' + getCustTickNum;
			}

		}

		if (String.isNotBlank(getTicketInfo.HazSafIssue)) {
			description += '\n<br/>' + Label.hazardousorsafety + ': ' + getTicketInfo.HazSafIssue;
		}
		if (String.isNotBlank(getTicketInfo.IntrusiveTest)) {
			description += '\n<br/>' + Label.intrusiveTesting + ': ' + getTicketInfo.IntrusiveTest;
		}
		if (String.isNotBlank(getTicketInfo.NoIntrusiveTestText)) {
			description += '\n<br/>' + Label.ENTPIntrusiveTestingWindow + ': ' + getTicketInfo.NoIntrusiveTestText;
		}
		if (String.isNotBlank(getTicketInfo.PowerIssue)) {
			description += '\n<br/>' + Label.ENTPPowertoSiteVerified + ': ' + getTicketInfo.PowerIssue;
		}
		if (String.isNotBlank(getTicketInfo.PowerToTelusEquip)) {
			description += '\n<br/>' + Label.ENTPPowerToTelusEquip_head + ': ' + getTicketInfo.PowerToTelusEquip;
		}
		if (String.isNotBlank(getTicketInfo.TELUSEquipConn)) {
			description += '\n<br/>' + Label.ENTPTELUSEquipConn_Header + ': ' + getTicketInfo.TELUSEquipConn;
		}
		if (String.isNotBlank(getTicketInfo.CirUsedFor)) {
			description += '\n<br/>' + Label.ENTPCircuitUsedFor + ': ' + getTicketInfo.CirUsedFor;
		}
		if (String.isNotBlank(getTicketInfo.HowManyUsers)) {
			description += '\n<br/>' + Label.ENTPHowManyUsers + ': ' + getTicketInfo.HowManyUsers;
		}
		if (String.isNotBlank(getTicketInfo.IsBackupThere)) {
			description += '\n<br/>' + Label.ENTPIstherebackup_header + ': ' + getTicketInfo.IsBackupThere;
		}
		if (String.isNotBlank(getTicketInfo.TypeOfCircuit)) {
			description += '\n<br/>' + Label.ENTPWhatTypeofCirc + ': ' + getTicketInfo.TypeOfCircuit;
		}
		if (String.isNotBlank(getTicketInfo.ConditionTypes)) {
			description += '\n<br/>' + label.ENTPConditionLabel + '?: ' + getTicketInfo.ConditionTypes;
		}
		if (String.isNotBlank(getTicketInfo.OutageSig)) {
			description += '\n<br/>' + Label.ENTPHowSignificant + ': ' + getTicketInfo.OutageSig;
		}
		if (String.isNotBlank(getTicketInfo.SitesImpacted)) {
			description += '\n<br/>' + Label.ENTPHowmanySites + ': ' + getTicketInfo.SitesImpacted;
		}
		if (String.isNotBlank(getTicketInfo.ServiceEverWorked)) {
			description += '\n<br/>' + Label.ENTPHasServiceWorked + ': ' + getTicketInfo.ServiceEverWorked;
		}
		if (String.isNotBlank(getTicketInfo.JacksAffected)) {
			description += '\n<br/>' + Label.ENTPJacksAffected + ': ' + getTicketInfo.JacksAffected;
		}
		if (String.isNotBlank(getTicketInfo.SetsAffected)) {
			description += '\n<br/>' + Label.ENTPSetsAffected + ': ' + getTicketInfo.SetsAffected;
		}
		if (String.isNotBlank(getTicketInfo.TestResults)) {
			description += '\n<br/>' + Label.ENTPTestResults + ': ' + getTicketInfo.TestResults;
		}
		if (String.isNotBlank(getTicketInfo.AssocPhoneNumber)) {
			description += '\n<br/>' + Label.ENTPAssocphnum + ': ' + getTicketInfo.AssocPhoneNumber;
		}
		if (String.isNotBlank(getTicketInfo.POI)) {
			description += '\n<br/>' + Label.ENTPPOI + ': ' + getTicketInfo.POI;
		}
		if (String.isNotBlank(getTicketInfo.CentralOffice)) {
			description += '\n<br/>' + Label.ENTPCentralOffice + ': ' + getTicketInfo.CentralOffice;
		}
		if (String.isNotBlank(getTicketInfo.SchdDateOfInstall)) {
			description += '\n<br/>' + Label.ENTPScdDateInstall + ': ' + getTicketInfo.SchdDateOfInstall;
		}
		if (String.isNotBlank(getTicketInfo.SPID)) {
			description += '\n<br/>' + Label.ENTPSPID + ': ' + getTicketInfo.SPID;
		}
		if (String.isNotBlank(getTicketInfo.FailedProvisioning)) {
			description += '\n<br/>' + Label.failedProvisioning + ': ' + getTicketInfo.FailedProvisioning;
		}
		index++;

		return description;
	}

	public void clear() {
		caseToInsert = null;
	}
	public String[] getCATLISTPULL() {

		userAccountId = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()].AccountId;

		List<Account_Configuration__c> AccountConfig = [SELECT Id, TELUS_Product__c FROM Account_Configuration__c WHERE Account__c = :userAccountId];
		// system.debug('AccountConfig[0].TELUS_Product__c: ' + AccountConfig[0].TELUS_Product__c);

		CatListPull = AccountConfig[0].TELUS_Product__c;

		String[] CatListPullT = CatListPull.split(';');

		system.debug('CatListPullT: ' + CatListPullT);

		return CatListPullT;

	}

	Public void setCATLISTPULL(String catListPull) {
		catListPull = CatListPull;
	}
	/*   public List<SelectOption> getRequestTypes() {
		 Map<String, List<String>> rts = categoryRequestTypesMap;
		   List<SelectOption> options = new List<SelectOption>();
		   selectedCategory = 'Enterprise';
		 List<String> temps = categoryRequestTypesMap.get(selectedCategory);

		 if(temps == null) {
			  //options.add(new SelectOption(label.mbrSelectCategoryFirst,label.mbrSelectCategoryFirst));
		   options.add( new SelectOption(label.mbrSelectType, label.mbrSelectType) );

			   return options;
		 }*/
	public List<SelectOption> getRequestTypes() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption(label.mbrSelectType, label.mbrSelectType));
		options.add(new SelectOption('Mainstream/Data (i.e. T1, T3, DS0, etc)', Label.ENTPMainstreamLabel));
		options.add(new SelectOption('Enhanced - Standard (i.e. Ethernet 10 Mg, 100Mg, WAN ADSL, etc.)', Label.ENTPEnhancedLabel));
		options.add(new SelectOption('Voice - Phone Number (i.e. 10 digit)', label.ENTPVoiceLabel));
		options.add(new SelectOption('Voice - PRI', label.ENTPVoicePRILabel));
		options.add(new SelectOption('L1L (Leased Loops, Unbundled Loops) (i.e. No Dial Tone)', label.ENTPL1LLabel));
		options.add(new SelectOption('Other', label.ENTPOther));
		return options;
	}
	/*  Set<String> tempsSet = new Set<String>(temps);
		if(!tempsSet.contains(requestType)) {
			requestType = label.mbrSelectType;
		}
		options.add( new SelectOption(label.mbrSelectType, label.mbrSelectType) );
		 for(String temp : temps) {
			options.add(new SelectOption(temp,temp));
		}
		 options.add( new SelectOption('Other', 'Other (will initiate a chat)') );
		return options;
	}*/

	public List<SelectOption> getCategories() {
		Map<String, List<String>> rts = categoryRequestTypesMap;
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption(label.mbrSelectCategory, label.mbrSelectCategory));
		for (String temp : rts.keySet()) {
			if (!temp.equals('Tech support') || !MBRUtils.restrictTechSupport()) {
				options.add(new SelectOption(temp, temp));
			}
		}
		return options;
	}

	public Case getParentCase() {
		return caseToInsert.Parent;
	}

	public String getParentCaseType() {
		return parentCaseType;
	}

	public String getParentCategory() {
		return parentCategory;
	}

	public Case getCaseToInsert() {
		return caseToInsert;
	}

	public static void updateCasePriorityToUrgent() {

		List<Case> cases = new List<Case>();

		// find the Case Origin string
		String originValue = 'ENTP';

		//  System.debug('originValue: ' + originValue);

		// query for all relevant cases
		DateTime cutoffDateTime = DateTime.now().addHours(-24);
		//  System.debug('Rbrekke: cutoffDateTime: ' + cutoffDateTime);
		if (!Test.isRunningTest()) {

			cases = [
					SELECT Id, Subject, Status, Priority, CreatedDate
					FROM Case
					WHERE Origin = :originValue
					AND Status = 'New'
					AND Priority != 'Urgent'
					AND (CreatedDate <= :cutoffDateTime)
			];
			//  system.debug('Rbrekke: Select Statement Line 726:'+ cutoffDateTime);
		} else {
			// remove the createdDate requirement for testing
			cases = [
					SELECT Id, Subject, Status, Priority, CreatedDate
					FROM Case
					WHERE Origin = 'ENTPTest'
					AND Status = 'New'
					AND Priority != 'Urgent'
			];
		}

		// System.debug('akong: cases: ' + cases);

		// loop through each case to determine if we should update priority
		Date sunday = Date.newInstance(1900, 1, 7); // 1900-01-07 is a sunday

		//  System.debug('akong: sunday: ' + sunday);

		for (Case c : cases) {
			// System.debug('akong: c.Id: ' + c.Id);
			Integer dayOfWeek = Math.mod(sunday.daysBetween(c.CreatedDate.date()), 7); // dayOfWeek, 0 is sunday
			if (Test.isRunningTest()) {
				// System.debug('Rbrekke: dayOfWeekBeforeIntegerCheck: ' + c.Subject);
				string RemStringDay = c.Subject;
				if (RemStringDay.isNumeric()) {
					dayOfWeek = Integer.valueOf(RemStringDay);
				} else {
					RemStringDay = c.Subject.remove('TEST');
					dayOfWeek = Integer.valueOf(RemStringDay);
				}
				// System.debug('Rbrekke: dayOfWeekAfterIntegerCheck: ' + dayOfWeek);
			}
			// System.debug('akong: dayOfWeek: ' + dayOfWeek);
			if (dayOfWeek >= 1 && dayOfWeek <= 4) {
				// monday to thursday, 24 hour check in soql query is sufficient
				c.Priority = 'Urgent';
			} else if (dayOfWeek == 5) {
				// friday, 72 hour check
				Integer hours = (Integer) (DateTime.now().getTime() - c.CreatedDate.getTime()) / 1000 / 60 / 60;
				if (hours >= 72 || Test.isRunningTest()) {
					c.Priority = 'Urgent';
				}
			} else if (dayOfWeek == 6) {
				// saturday, custom check for monday right before midnight (i.e. monday at 11:59:59 pm)
				DateTime mondayMidnight = DateTime.newInstance(c.CreatedDate.date().addDays(2), Time.newInstance(23, 59, 59, 999));
				if (mondayMidnight <= DateTime.now() || Test.isRunningTest()) {
					c.Priority = 'Urgent';
				}
			} else if (dayOfWeek == 0) {
				// sunday, custom check for monday right before midnight (i.e. monday at 11:59:59 pm)
				DateTime mondayMidnight = DateTime.newInstance(c.CreatedDate.date().addDays(1), Time.newInstance(23, 59, 59, 999));
				if (mondayMidnight <= DateTime.now() || Test.isRunningTest()) {
					c.Priority = 'Urgent';
				}
			}
		}

		// update
		update cases;
		//  System.debug('akong: final cases: ' + cases);

	}

}