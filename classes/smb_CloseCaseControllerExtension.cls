public with sharing class smb_CloseCaseControllerExtension {

	private final Case caseRecord {get;set;}
	
	public boolean CaseClosed {get;set;}
	
	public List<string> headerFieldNames {get;set;}
	
	public List<string> bodyFieldNames {get;set;}
	
	public List<Schema.FieldSetMember> headerFields {get;set;}
	
	public List<Schema.FieldSetMember> bodyFields {get;set;}
	
	public smb_CloseCaseControllerExtension (ApexPages.StandardController stdController) {
		populateFields();
		
		if (!Test.isRunningTest()) {
			stdController.addFields(headerFieldNames);
			stdController.addFields(bodyFieldNames);
			stdController.addFields(new List<string>{'IsClosed','escalation_queue_to_first_callback__c','closed_by__c','createdbyid','createddate'});
			
			stdController.reset();
		}
		
		this.caseRecord = (Case)stdController.getRecord();
		
		initialise();
	}

	private void initialise() {
		CaseClosed = false;
		
		checkPrerequisites();
		setDefaults();
	}
	
	private void checkPrerequisites() {
		if (this.caseRecord.IsClosed) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Case is already closed.'));
			CaseClosed = true;
		}
	}
	
	private void setDefaults() {
		this.caseRecord.status = getClosedCaseStatus();
		this.caseRecord.Case_Resolution__c = 'Resolved';
        businesshours bh=null;
        try{
            bh=[select id from businesshours where isactive=true and name='Escalation'];
            task firstCallbackTask=[select id,createddate from task where whatid=:this.caseRecord.id and subject like 'First Callback%' order by createddate limit 1];
            list<casehistory> histories=[select caseid,case.owner.profile.name,createddate,field,newvalue from casehistory where caseid=:this.caseRecord.id order by caseid,createddate desc];
            map<id,datetime> caseHistoryMap=new map<id,datetime>();
            for(casehistory history:histories){
                if(history.newvalue!=null&&((string)history.newvalue).equalsignorecase('smb care escalations')){
                    caseHistoryMap.put(history.caseid,history.createddate);
                }
            }
            if(caseHistoryMap.isempty()){
                user caseCreatedByUser=[select Care_Type__c from user where id=:this.caseRecord.createdbyid];
                if(string.isnotblank(caseCreatedByUser.care_type__c)&&caseCreatedByUser.Care_Type__c.containsignorecase('escalation')){
                    caseHistoryMap.put(this.caseRecord.id,this.caseRecord.createddate);
                }
            }
            if(bh!=null){
                if(this.caseRecord.escalation_queue_to_first_callback__c==null){
                    if(test.isrunningtest()){
                        if(caseHistoryMap.isempty()){
                            caseHistoryMap.put(this.caseRecord.id,this.caseRecord.createddate.adddays(-3));
                        }
                    }
                    assignFirstCallback(bh,caseHistoryMap,firstCallbackTask);
                }
            }
        }catch(queryexception qe){
            system.debug(qe.getmessage());
            Util.emailSystemError(qe);
        }
	}
    void assignFirstCallback(businesshours bh,map<id,datetime> caseHistoryMap,task firstCallbackTask){
        if(caseHistoryMap.containskey(this.caseRecord.id)){
            integer increment=1;
            integer total=0;
            datetime assignedToQueueTime=datetime.newinstance(caseHistoryMap.get(this.caseRecord.id).date(),caseHistoryMap.get(this.caseRecord.id).time());
            while(assignedToQueueTime<firstCallbackTask.createddate){
                if(businesshours.iswithin(bh.id,assignedToQueueTime)){
                    total+=increment;
                }
                assignedToQueueTime=assignedToQueueTime.addminutes(increment);
            }
            this.caseRecord.escalation_queue_to_first_callback__c=total;
            this.caseRecord.closed_by__c=userinfo.getuserid();
        }
    }
	
	private string getClosedCaseStatus() {
		
		List<CaseStatus> statuses = [SELECT MasterLabel FROM CaseStatus WHERE IsClosed = true];
		
		if (statuses.size() == 0) return null;
		
		return statuses[0].MasterLabel;
	}
	
	private void populateFields() {
		
		Id caseId = ApexPages.currentPage().getParameters().get('Id');
		
		List<RecordType> rt = [SELECT DeveloperName FROM RecordType WHERE Id IN (SELECT RecordTypeId FROM Case WHERE Id = :caseId)];
		
		string recordTypeName = 'Default';
		
		if (rt.size() > 0) recordTypeName = rt.get(0).DeveloperName;
		
		Close_Case_Field_Set_Assignment__c fieldSetSettings = Close_Case_Field_Set_Assignment__c.getInstance(recordTypeName);
		
		if (fieldSetSettings == null) {
			fieldSetSettings = Close_Case_Field_Set_Assignment__c.getInstance('Default');
		}
		
		this.headerFields = SObjectType.Case.FieldSets.getMap().get(fieldSetSettings.Header_Set__c).getFields();
		this.bodyFields = SObjectType.Case.FieldSets.getMap().get(fieldSetSettings.Body_Set__c).getFields();
		
		this.headerFieldNames = getFieldNames(headerFields);
		this.bodyFieldNames = getFieldNames(bodyFields);

	}
	
	private static List<string> getFieldNames(List<Schema.FieldSetMember> fieldSetFields) {
		List<string> fieldNames = new List<string>();
		
		for (Schema.FieldSetMember field : fieldSetFields) {
			fieldNames.add(field.FieldPath);
		}
		
		return fieldNames;
	} 
}