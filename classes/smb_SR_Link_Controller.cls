global without sharing class smb_SR_Link_Controller {
       
    String serviceRequestID='';    
    public String accountId {get;set;}
    public string accountRCID {get;set;}
    public string orderId {get;set;}    
    public list<String> lstLegacyID {get;set;} 
    public list<String> lstLegacySeqNbr {get;set;}   
    public list<String> lstLegacyOrders {get;set;}    
    
    list<Service_Request__c> lstSR = new list<Service_Request__c>();

    public smb_SR_Link_Controller(){
        accountRCID = ApexPages.currentPage().getParameters().get('rcid');
        orderId= ApexPages.currentPage().getParameters().get('orderId');
        accountId=ApexPages.currentPage().getParameters().get('accountId');
        serviceRequestID=ApexPages.currentPage().getParameters().get('srID');               
    }   
 
    public pageReference init(){

        lstSR = [select Legacy_Order_ID__c from Service_Request__c where id =:serviceRequestID];                
        lstLegacyOrders = new list<string>();       
        
        list<String> lstParseLegacyID = new list<String>();
        for(Service_Request__c sr:lstSR){
            lstLegacyID = new list<string>();
            lstLegacySeqNbr = new list<string>();
            lstParseLegacyID = sr.Legacy_Order_ID__c.split(',');            
            for(String s:lstParseLegacyID){
                lstLegacyID.add(s.trim());
            }
                        
            for(String oID:lstLegacyID){
                integer result = oID.lastIndexOf('-');
                if(result != -1){
                    lstLegacyOrders.add(oID.substring(0, result));
                    lstLegacySeqNbr.add(oID.substring(result+1,oID.length()));
                    system.debug('lstLegacyOrders: '+lstLegacyOrders);
                    system.debug('lstLegacySeqNbr: '+lstLegacySeqNbr);
                }else{
                    lstLegacyOrders.add(oID);  //If no sequence number add legacy order id
                    system.debug('If no sequence # legacyID: '+oID);
                }
            }                       
        } 
        
        return null;
    }

    @RemoteAction
    global static Pagereference getLegacyOrders(string accountRCID, string orderId, string accountId){          
        string legOrderID='';
        string lstLegacySeqNbr='';      
        string targetURL='';
        //system.debug('orderId 1: '+orderId);
        integer result = orderId.lastIndexOf('-');
        if(result != -1){
            legOrderID = orderId.substring(0, result);
            lstLegacySeqNbr = orderId.substring(result+1,orderId.length()); 
        }else{
            legOrderID = orderId;           
        }       
        SMB_WebserviceHelper.OrderWrapper orderWrapper=null;        
        orderWrapper=SMB_WebserviceHelper.getLegacyOrder(accountRCID,'500',legOrderID, lstLegacySeqNbr);        
        system.debug('legOrderID: '+legOrderID);
        system.debug('lstLegacySeqNbr: '+lstLegacySeqNbr);
        //system.debug('accountRCID: '+accountRCID);    
        //system.debug('Order List: '+orderWrapper);        
 
        if(null==orderWrapper){         
            return null;
        }else{                      
            Datetime dt = orderWrapper.issueDate;
            Datetime GMTDate = Datetime.newInstanceGmt(dt.year(), dt.month(), dt.day(), dt.hour(), dt.minute(), dt.second());           
            
            //targetURL ='/apex/smb_SVOC_OrdersDetail?id='+accountId+'&orderId='+orderWrapper.orderID+'&orderActionId='+orderWrapper.orderActionID+'&seqNumber='+orderWrapper.seqNumber+'&sysName='+orderWrapper.systemName+'&issueDate='+'Mon Oct 22 00:00:00 GMT 2012';
            targetURL ='/apex/smb_SVOC_OrdersDetail?isdtp=vw'+'&id='+accountId+'&orderId='+orderWrapper.orderID+'&orderActionId='+orderWrapper.orderActionID+'&seqNumber='+orderWrapper.seqNumber+'&sysName='+orderWrapper.systemName+'&issueDate='+'Mon Oct 22 00:00:00 GMT 2012';
            system.debug('targetURL: '+targetURL);                  
            return new Pagereference(targetURL);
            //?isdtp=vw         
        }           
    }
}