global with sharing class Naas_ServiceChargesTable implements vlocity_cmt.VlocityOpenInterface {

  global Boolean invokeMethod(String methodName, Map<String,Object> inputMap, Map<String,Object> outMap, Map<String,Object> options) {
        Boolean success = true;
        if(methodName == 'buildDocumentSectionContent')
        { 
             Id contractId = (Id) inputMap.get('contextObjId');
            map<string,object> mapTableInfo = Naas_DynamicTableHelper.getServiceChargesData(contractId);
                       
            if(mapTableInfo.size() > 0 && !mapTableInfo.isEmpty()){
                inputMap.putall(mapTableInfo);
                map<string,object> outputMap =  Naas_DynamicTableHelper.buildDocumentSectionContent(inputMap,outMap, options);
                outMap.putAll(outputMap);
            }
            else {
                return false;
            }
                
         
             }       
          return success; 
  }

}