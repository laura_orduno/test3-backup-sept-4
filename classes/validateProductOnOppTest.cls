@isTest
public class validateProductOnOppTest {
	
	private static testMethod void testTrigger() {
		Id PROFILEID = [SELECT id FROM Profile WHERE name='Standard User' LIMIT 1].id;
    
    User runner = new User(
      username = Datetime.now().getTime() + 'runner@TRACTIONSM.COM',
      email = 'test@example.com',
      title = 'test',
      lastname = 'test',
      alias = 'test',
      TimezoneSIDKey = 'America/Los_Angeles',
      LocaleSIDKey = 'en_US',
      EmailEncodingKey = 'UTF-8',
      ProfileId = PROFILEID,
      LanguageLocaleKey = 'en_US'
    );
		
		
		validateProductOnOppSettings__c settings = new validateProductOnOppSettings__c(
			name = 'Telus Partner User Test',
			Exclude_Profile_Name__c = 'Telus Partner User'
		);
		insert settings;
		
		Opportunity o = new Opportunity(name = 'test', stagename = 'test', closedate = Date.today());
		insert o;
		
		Product2 p = new Product2(name = 'Test Product 1', isActive = true, Product_Family__c = 'test family');
		
		system.runas(runner) { // fix for MIXED_DML_OPERATION exception during testing due to existing trigger on Product
			insert p;
		}

		Opp_Product_Item__c opi = new Opp_Product_Item__c(
			Product__c = p.id,
			Opportunity__c = o.id
		);
		insert opi;
		
		o.stagename = 'new';
		/*
		* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
		* START
		*/
			o.Stage_update_time__c=System.Now();
		/*
		* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
		* END
		*/
		update o;
		
		p.isActive = false;
		
		system.runas(runner) {
			update p;
		}
		
		o.stagename = 'new2';
		/*
		* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
		* START
		*/
			o.Stage_update_time__c=System.Now();
		/*
		* Fix for defect 32933 - whereever the Opporunity Stage is updated, the field Update_Stage__c needs to be updated too
		* END
		*/
		
		Boolean caughtException = false;
		try {
			update o;
		} catch (DMLException e) {
			caughtException = true;
			system.assert(e.getMessage().contains('Inactive. Please remove or reselect.'));
		}
		system.assert(caughtException);
	}
}