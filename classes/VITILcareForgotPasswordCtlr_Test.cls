@isTest(SeeAllData=false)
public class VITILcareForgotPasswordCtlr_Test {
    @isTest(SeeAllData=false) static void VITILcare_testSuccess() {
		User u = MBRTestUtils.createPortalUser('');
		VITILcareForgotPasswordCtlr ctlr = new VITILcareForgotPasswordCtlr();
		ctlr.username = u.UserName;
		ctlr.forgotPassword();
		System.debug(u.username);
		System.debug([SELECT UserName, FirstName, LastName, Id, IsActive, UserType FROM User WHERE Id=:u.Id]);
		System.debug([SELECT UserName, FirstName, LastName, Id, IsActive, UserType FROM User WHERE Username=:u.UserName]);
		System.debug('Reset Complete? ' + ctlr.resetComplete);
		System.debug('Reset Message: ' + ctlr.resetComplete);
		System.debug('Username Error: ' + ctlr.usernameError);

		// Site.forgotPassword will always return false during unit test
	}
	
	@isTest(SeeAllData=false) static void VITILcare_testError() {
		VITILcareForgotPasswordCtlr ctlr = new VITILcareForgotPasswordCtlr();
		ctlr.username = 'invalid@username.com';
		ctlr.forgotPassword();
		System.assert(ctlr.usernameError.length() > 0);
	}


}