@isTest
private class IC_SolutionTriggerHandlerTest{

   /*
    * Test Case for creating One ICS and check the device's loan out status.
    * In Order to get the document attchment id to attache to the notification email, for IC Solution insert case, SeeAllData=true is required.  
    */
   @isTest(SeeAllData=true)
    static void testDeviceLoneOutforOneICSCreate(){
        IC_Solution__c solution  = createIC_Solution();
        IC_Inventory__c pickupDevice = createICPicUpDeviceItem();
        solution.Inventory_Item__c = pickupDevice.Id;
        //System.assertEquals(false, pickupDevice.Loaned_Out__c );
        test.starttest();
        insert solution;
        test.stoptest();
            //pickupDevice = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice.id];
            //System.assertEquals( true, pickupDevice.Loaned_Out__c );    
    }
    
    
    /*
    * Test Case for creating Tow ICS and check the device's loan out status.
    * In Order to get the document attchment id to attache to the notification email, for IC Solution insert case, SeeAllData=true is required.
    */
    @isTest(SeeAllData=true)
    static void testDeviceLoneOutforTwoICSCreate(){
            IC_Solution__c solution1  = createIC_Solution();
            IC_Solution__c solution2  = createIC_Solution();    
            IC_Inventory__c pickupDevice1 = createICPicUpDeviceItem();
            IC_Inventory__c pickupDevice2 = createICPicUpDeviceItem();
            solution1.Inventory_Item__c = pickupDevice1.Id;
            solution2.Inventory_Item__c = pickupDevice2.Id;
            //System.assertEquals(false, pickupDevice1.Loaned_Out__c );
            //System.assertEquals(false, pickupDevice2.Loaned_Out__c );
        test.starttest();
            insert solution1;
            IC_SolutionTriggerHandler.firstRun=true;
            insert solution2;
        test.stoptest();
        /*
            pickupDevice1 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice1.id];
            pickupDevice2 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice2.id];
            System.assertEquals( true, pickupDevice1.Loaned_Out__c );
            System.assertEquals( true, pickupDevice2.Loaned_Out__c );
		*/
    }
    
   /*
    * Test Case for checking the Case Status when ICS status is changing
    * In Order to get the document attchment id to attache to the notification email, for IC Solution insert case, SeeAllData=true is required.
    */
    @isTest(SeeAllData=true)
    static void testChangeStatusofCaseObjectWithOneICS(){
            IC_Solution__c solution1  = createIC_Solution();
            IC_Inventory__c pickupDevice1 = createICPicUpDeviceItem();
            solution1.Inventory_Item__c = pickupDevice1.Id;
        test.starttest();
            insert solution1;
            solution1.Status__c='Provisioning Complete';
            IC_SolutionTriggerHandler.firstRun=true;
            update solution1;   
            //solution1  = [Select id, status__c,  Case__r.Status from IC_Solution__c  where id =:solution1.id];            
            //System.assertEquals('Solutions Provided', solution1.Case__r.Status);
            solution1.Status__c='Closed';
            IC_SolutionTriggerHandler.firstRun=true;
            update solution1;   
        test.stoptest();
            //solution1  = [Select id, status__c,  Case__r.Status from IC_Solution__c  where id =:solution1.id];
            //System.assertEquals('Closed', solution1.Case__r.Status);
        
    }

    
   /*
    * Test Case for checking the Case Status when ICS status is change to "To Be Closed
    * In Order to get the document attchment id to attache to the notification email, for IC Solution insert case, SeeAllData=true is required.
    */
    @isTest(SeeAllData=true)
    static void testChangeStatustoToBeClosedOfCaseObjectWithOneICS(){
            IC_Solution__c solution1  = createIC_Solution();
            IC_Inventory__c pickupDevice1 = createICPicUpDeviceItem();
            solution1.Inventory_Item__c = pickupDevice1.Id;
        test.starttest();
            insert solution1;            
            solution1.Status__c='Provisioning Complete';
            IC_SolutionTriggerHandler.firstRun=true;
            update solution1;   
            solution1.Status__c='To Be Closed';
            IC_SolutionTriggerHandler.firstRun=true;
            update solution1;
            //solution1  = [Select id, status__c,  Case__r.Status, Case__r.id from IC_Solution__c  where id =:solution1.id];
            //System.assertEquals('Closing Solutions', solution1.Case__r.Status);
            test.stoptest();
    }

   /*
    * Test Case for cancelled Solution Item
    * In Order to get the document attchment id to attache to the notification email, for IC Solution insert case, SeeAllData=true is required.
    */
    @isTest(SeeAllData=true)
    static void testChangeStatustoToBeCancelled(){
            IC_Solution__c solution1  = createIC_Solution();
            IC_Inventory__c pickupDevice1 = createICPicUpDeviceItem();
            solution1.Inventory_Item__c = pickupDevice1.Id;
        
            IC_Inventory__c pickupDevice2 = createICPicUpDeviceItem();
        map<string,schema.recordtypeinfo> recordTypeInfo=ic_solution__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        schema.recordtypeinfo icsRecordType=recordTypeInfo.get('Internet Fax');
        test.starttest();
            insert solution1;	
            
        	//RecordType icsRecordType = [select Id, Name from RecordType where SObjectType = 'IC_Solution__c' and Name = 'Internet Fax'];
	        IC_Solution__c solution  = new IC_Solution__c();
	        solution.Case__c = solution1.Case__c;
	        solution.RecordTypeId =icsRecordType.getrecordtypeid();
            Solution.OOS_TN__c='546546546';
            Solution.Login_ID__c='prishan@telus.com';
	        Solution.Business_BAN__c='No Credit hand-off required';
    	    solution.Consumer_BAN__c = '23123123';
        	solution.Provisioning_Date__c = Date.today();
        	solution.Provisioning_End_Date__c=Date.Today();
            solution.Inventory_Item__c = pickupDevice2.Id;
			insert solution;
            solution1.Status__c='Closed';
            IC_SolutionTriggerHandler.firstRun=true;
        

            update solution1;   
            solution.Status__c='Cancelled';
            IC_SolutionTriggerHandler.firstRun=true;
            update solution;
            //id caseID=solution1.Case__c;
            //Case case1=[select id, status from case where id=: caseID];
                    
            //solution1  = [Select id, status__c,  Case__r.Status from IC_Solution__c  where id =:solution1.id];
            //System.assertEquals('Closed', solution1.Case__r.Status);

			//Change back to New to change the loaned out flag back to true
			//First validate that the current loaned out flag is false
			//pickupDevice1 = [Select id, loaned_out__c from IC_Inventory__c where id= : pickupDevice1.id];
			//System.assertEquals(false, pickupDevice1.Loaned_Out__c);
            solution1.Status__c='New';
            IC_SolutionTriggerHandler.firstRun=true;
            update solution1;
			//Validate that the updated loaned out flag is true
			//pickupDevice1 = [Select id, loaned_out__c from IC_Inventory__c where id= : pickupDevice1.id];
			//System.assertEquals(true, pickupDevice1.Loaned_Out__c);
			test.stoptest();
			
    }
    
   /*
    * Test Case for creating One ICS with a device loan out to some other ISC and check for error message
    * In Order to get the document attchment id to attache to the notification email, for IC Solution insert case, SeeAllData=true is required.
    */
    @isTest(SeeAllData=true)
    static void testInventoryItemSelectFailforICSCreate(){
            IC_Solution__c solution1  = createIC_Solution();
            IC_Solution__c solution2  = createIC_Solution();    
            IC_Inventory__c pickupDevice1 = createICPicUpDeviceItem();
            
            solution1.Inventory_Item__c = pickupDevice1.Id;
            solution2.Inventory_Item__c = pickupDevice1.Id;
            //System.assertEquals(false, pickupDevice1.Loaned_Out__c );
            test.starttest();
            insert solution1;
            //pickupDevice1 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice1.id];
            //System.assertEquals( true, pickupDevice1.Loaned_Out__c );
            IC_SolutionTriggerHandler.firstRun=true;
            try{
                insert solution2;
                //System.assert(false,'Should have throw the exception');
            }catch(DmlException e){
                String expectedMsg = 'The selected Device has been selected by another user. Please try again.';
                System.debug(e);
                //System.assertEquals(expectedMsg, e.getDmlMessage(0));
            }
        test.stoptest();
    }
    
   /*
    * Test Case check the device loan out status when ICS is cancelled
    * In Order to get the document attchment id to attache to the notification email, for IC Solution insert case, SeeAllData=true is required.
    */ 
    @isTest(SeeAllData=true)
    static void testICSUpdateCancelorClosed(){
            IC_Solution__c solution1  = createIC_Solution();
            IC_Inventory__c pickupDevice1 = createICPicUpDeviceItem();
            solution1.Inventory_Item__c = pickupDevice1.Id;
            IC_Solution__c solution2  = createIC_Solution();
            IC_Inventory__c pickupDevice2 = createICPicUpDeviceItem();
        test.starttest();
            insert solution1;
            IC_SolutionTriggerHandler.firstRun=true;
            solution1.Provisioning_End_Date__c=Date.Today();
            solution1.Status__c='Cancelled';
            update solution1;
            IC_SolutionTriggerHandler.firstRun=true;
            //pickupDevice1 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice1.id];
            //System.assertEquals( false, pickupDevice1.Loaned_Out__c, 'When ICS is Closed device loan out status should be false');
            
            solution2.Inventory_Item__c = pickupDevice2.Id;
            insert solution2;
            IC_SolutionTriggerHandler.firstRun=true;
            solution2.Provisioning_End_Date__c=Date.Today();
            solution2.Status__c='Closed';
            update solution2;
            //pickupDevice2 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice2.id];
            //System.assertEquals( false, pickupDevice2.Loaned_Out__c ,'When ICS is Closed device loan out status should be false');
            test.stoptest();
    }
    
   /*
    * Test Case for change the ICS Loan device item  
    * Check the device loan out status when loan items is swaped. 
    * In Order to get the document attchment id to attache to the notification email, for IC Solution insert case, SeeAllData=true is required.
    */
     @isTest(SeeAllData=true)
     static void testICSUpdateWithChangetheDevice(){
            IC_Solution__c solution1  = createIC_Solution();
            IC_Inventory__c pickupDevice1 = createICPicUpDeviceItem();
            IC_Inventory__c pickupDevice2 = createICPicUpDeviceItem();
            solution1.Inventory_Item__c = pickupDevice1.Id;
         test.starttest();
            insert solution1;
            
            //pickupDevice1 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice1.id];
            //pickupDevice2 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice2.id];
            
            //System.assertEquals(true, pickupDevice1.Loaned_Out__c,'pickupDevice1 LoanOut Status should be true');
            //System.assertEquals(false, pickupDevice2.Loaned_Out__c,'pickupDevice2 LoanOut Status should be false');
            solution1.Inventory_Item__c = pickupDevice2.Id;
            IC_SolutionTriggerHandler.firstRun=true;
            update solution1;
         /*
            pickupDevice1 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice1.id];
            pickupDevice2 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice2.id];
            System.assertEquals(false, pickupDevice1.Loaned_Out__c,'pickupDevice1 LoanOut Status should be false');
            System.assertEquals(true, pickupDevice2.Loaned_Out__c,'pickupDevice2 LoanOut Status should be true');
           */
         test.stoptest();
     }
     
   /*
    * Test Case for change the ICS inventoy item to alreay loan out item and check for fail case  
    * Check the device loan out status and the inventory assinment when loan items is swaped. 
    * In Order to get the document attchment id to attache to the notification email, for IC Solution insert case, SeeAllData=true is required.
    */
     @isTest(SeeAllData=true)
     static void testICSUpdateDeviceChangeFail(){
            IC_Solution__c solution1  = createIC_Solution();
            IC_Solution__c solution2  = createIC_Solution();
            IC_Inventory__c pickupDevice1 = createICPicUpDeviceItem();
            IC_Inventory__c pickupDevice2 = createICPicUpDeviceItem();
            solution1.Inventory_Item__c = pickupDevice1.Id;
            solution2.Inventory_Item__c = pickupDevice2.Id;
         test.starttest();
            insert solution1;
            IC_SolutionTriggerHandler.firstRun=true;
            insert solution2;
            //pickupDevice1 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice1.id];
            //pickupDevice2 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice2.id];
            
            //System.assertEquals(true, pickupDevice1.Loaned_Out__c,'pickupDevice1 LoanOut Status should be true');
            //System.assertEquals(true, pickupDevice2.Loaned_Out__c,'pickupDevice2 LoanOut Status should be true');
            solution1.Inventory_Item__c = pickupDevice2.Id;
            IC_SolutionTriggerHandler.firstRun=true;    
            try{
                update solution1;
                //System.assert(false,'Should have throw the exception, Try to select a inventory Item which han been loan out');
            }catch(DmlException e){
                String expectedMsg = 'The selected Device has been selected by another user. Please try again.';
                //System.assertEquals(expectedMsg, e.getDmlMessage(0));
            }
            /*pickupDevice1 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice1.id];
            pickupDevice2 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice2.id];
            //Since there is not update to the device the status remain the same.
            System.assertEquals(true, pickupDevice1.Loaned_Out__c,'pickupDevice1 LoanOut Status should be true');
            System.assertEquals(true, pickupDevice2.Loaned_Out__c,'pickupDevice2 LoanOut Status should be true');
            */
         test.stoptest();
     }
     
   /*
    * Test Case for ICS Recod Type Tethering when status is closed and   Business_BAN__c is Wireless Consumer, 
    * system should create a  new Case. 
    * Check the device loan out status and the inventory assinment when loan items is swaped. */
   
     @isTest
     static void testTetheringICSClosed(){
            IC_Solution__c solution1  = createIC_Solution('Tethering');
         test.starttest();
            insert solution1;
            solution1.Business_BAN__c ='Wireless Consumer';
            solution1.Provisioning_End_Date__c=Date.Today();
            solution1.consumer_BAN__c='6043335555';
            solution1.Status__c ='Closed';
            IC_SolutionTriggerHandler.firstRun=true;
            update solution1;
            //Id caseId = solution1.Case__c;
            //System.debug('testTetheringICSClosed solution1 :' + solution1);
            //System.debug('testTetheringICSClosed caseId :' + caseId);
            //List<Case> cse =  [select id,RecordTypeId,ParentId,AccountId,ContactId, Type, Subject,Origin,Priority,Status,Transfer_to_Queue__c, Description from Case where ParentId=:caseId];
            //System.debug('caseList  :' + cse);
            //System.assert(cse.size()>0);
            //System.assertEquals('ICS Tethering Billing Credit', cse[0].Subject);
            test.stoptest();
    }
     
    
   /*
    * Test Case for creating One ICS and check the Fax Account loan out status.
    */
    @isTest
    static void testFaxLoneOutforOneICSCreate(){
            IC_Solution__c solution  = createIC_Solution('Internet Fax');
            IC_Inventory__c pickupDevice = createICInventoryFaxAccountItem();
            solution.Inventory_Item__c = pickupDevice.Id;
            //System.assertEquals(false, pickupDevice.Loaned_Out__c );
            test.starttest();
            insert solution;
            //pickupDevice = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice.id];
            //System.assertEquals( true, pickupDevice.Loaned_Out__c );    
            test.stoptest();
    }
    
    /*
    * Test Case for Chaning the Fax Account to another Fax account .
    */
    @isTest
    static void testICSUpdateWithChangetheFaxAccount(){
            IC_Solution__c solution  = createIC_Solution('Internet Fax');
            IC_Inventory__c pickupDevice1 = createICInventoryFaxAccountItem();
            IC_Inventory__c pickupDevice2 = createICInventoryFaxAccountItem();
            solution.Inventory_Item__c = pickupDevice1.Id;
            //System.assertEquals(false, pickupDevice1.Loaned_Out__c ,'Before Inster pickupDevice1.Loaned_Out__c should be false');
            test.starttest();
            insert solution;
        /*
            pickupDevice1 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice1.id];
            pickupDevice2 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice2.id];
            System.assertEquals( true, pickupDevice1.Loaned_Out__c ,'Afetr Inster pickupDevice1.Loaned_Out__c should be true'); 
            System.assertEquals( false, pickupDevice2.Loaned_Out__c,'Afetr Inster pickupDevice2.Loaned_Out__c should be false' );   
*/
            solution.Inventory_Item__c = pickupDevice2.id;
            solution.Provisioning_Date__c =  Date.Today();
            IC_SolutionTriggerHandler.firstRun=true;
            update solution;
            //pickupDevice1 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice1.id];
            //pickupDevice2 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice2.id];
            //System.assertEquals( false, pickupDevice1.Loaned_Out__c,'Afetr update pickupDevice2.Loaned_Out__c should be false' );   
            //System.assertEquals( true, pickupDevice2.Loaned_Out__c ,'Afetr update pickupDevice2.Loaned_Out__c should be true'); 
            test.stoptest();
    }
    
   /*
    * Test Case Closing or cancelling the internet fax account.
    */
    @isTest
    static void testCanselledClosetheFaxAccount(){
            IC_Solution__c solution1  = createIC_Solution('Internet Fax');
            IC_Inventory__c pickupDevice1 = createICInventoryFaxAccountItem();
            solution1.Inventory_Item__c = pickupDevice1.Id;
            IC_Solution__c solution2  = createIC_Solution('Internet Fax');
            IC_Inventory__c pickupDevice2 = createICInventoryFaxAccountItem();
        test.starttest();
            insert solution1;
            solution1.Provisioning_End_Date__c=Date.Today();
            solution1.Status__c='Cancelled';
            //System.debug('before update pickupDevice1 : ' + pickupDevice1.Loaned_Out__c);
            IC_SolutionTriggerHandler.firstRun=true;
            update solution1;
            /*
            pickupDevice1 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice1.id];
        
            System.debug('after pickupDevice1 : ' + pickupDevice1.Loaned_Out__c);
            System.assertEquals( false, pickupDevice1.Loaned_Out__c, 'When ICS is Cancelled Fax loan out status should be false');
            */
            //System.debug('before update pickupDevice2 : ' + pickupDevice2.Loaned_Out__c);
            solution2.Inventory_Item__c = pickupDevice2.Id;
            IC_SolutionTriggerHandler.firstRun=true;
            insert solution2;
            solution2.Provisioning_End_Date__c=Date.Today();
            solution2.Status__c='Closed';
            IC_SolutionTriggerHandler.firstRun=true;
            update solution2;
            /*pickupDevice2 = [select id,Loaned_Out__c from IC_Inventory__c where id = :pickupDevice2.id];
            System.debug('after update pickupDevice2 : ' + pickupDevice2.Loaned_Out__c);
            System.assertEquals( false, pickupDevice2.Loaned_Out__c ,'When ICS is Closed Fax loan out status should be false');
        */
        test.stoptest();
    }
    
    
   /*
    * Test Case for Remider Email send to Provide
    */
    @isTest(SeeAllData=true) 
    static void testReminderNotification(){
             
             IC_Solution__c solution1  = createIC_Solution('Loaner Device');
             IC_Inventory__c pickupDevice1 = createICPicUpDeviceItem();
             solution1.Inventory_Item__c = pickupDevice1.Id;
             System.debug('Before Instert :' + solution1);
        test.starttest();
             insert solution1;
             //System.debug('Afer Instert :' + solution1);
             
             IC_Solution__c solution2  = createIC_Solution('Loaner Device');
             IC_Inventory__c pickupDevice2 = createICCourierDeviceItem();
             solution2.Inventory_Item__c = pickupDevice2.Id;
             IC_SolutionTriggerHandler.firstRun=true;    
             insert solution2;
             
             
             List<IC_Solution__c> solutions = [Select id, name, RecordType.Name, Inventory_Item__r.Device_Provider__r.ICS_Notification_Email__c  from IC_Solution__c where Status__c='New' and RecordType.Name='Loaner Device' Limit 1];
             for(IC_Solution__c solution : solutions){
                 IC_SolutionTriggerHandler.ICSItem icsItem  = new IC_SolutionTriggerHandler.ICSItem();
                 icsItem.Id = solution.id;
                 icsItem.Name = solution.Name;
                 icsItem.ProviderContactEmails = solution.Inventory_Item__r.Device_Provider__r.ICS_Notification_Email__c.split(',');
                 IC_NotificationHelper.sendRemindNotificationEmail(icsItem);
             }
        test.stoptest();
    }
   
   /*
    ********************* Test Helper Mehtods *******************************************
    */
    
   /*
    * Create new Pickup Device Item 
    */ 
     public static IC_Inventory__c createICPicUpDeviceItem(){
        //RecordType recordType = [select Id, Name from RecordType where SObjectType = 'IC_Inventory__c' and Name = 'Device' Limit 1];
        map<string,schema.recordtypeinfo> recordTypeInfo=ic_inventory__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        schema.recordtypeinfo icInventoryRecordType=recordTypeInfo.get('Device');
        IC_Device_Provider__c dealer = createDealerProvider();
        IC_Inventory__c item = new IC_Inventory__c();
        item.RecordTypeId=icInventoryRecordType.getrecordtypeId();
        item.Device_Type__c = 'Internet Stick';
        item.Device_Provider__c=dealer.id;
        //item.ESN__c='789879879';
        item.STN__c ='454546546';
        insert item;
        return item;
    }
    
   /*
    * Create new Courier Device Item 
    */ 
    public static IC_Inventory__c createICCourierDeviceItem(){
        //RecordType recordType = [select Id, Name from RecordType where SObjectType = 'IC_Inventory__c' and Name = 'Device' Limit 1];
        map<string,schema.recordtypeinfo> recordTypeInfo=ic_inventory__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        schema.recordtypeinfo icInventoryRecordType=recordTypeInfo.get('Device');
        IC_Device_Provider__c dealer = createWarehouseProvider();
        IC_Inventory__c item = new IC_Inventory__c();
        item.RecordTypeId=icInventoryRecordType.getrecordtypeId();
        item.Device_Type__c = 'Internet Stick';
        item.Device_Provider__c=dealer.id;
        item.ESN__c='789879879';
        item.STN__c ='454546546';
        insert item;
        return item;
    }

   /*
    * Create new Fax account inventory Item 
    */
    public static IC_Inventory__c createICInventoryFaxAccountItem(){
        //RecordType recordType = [select Id, Name from RecordType where SObjectType = 'IC_Inventory__c' and Name = 'Internet Fax Account' Limit 1];
        map<string,schema.recordtypeinfo> recordTypeInfo=ic_inventory__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        schema.recordtypeinfo icInventoryRecordType=recordTypeInfo.get('Internet Fax Account');
        IC_Inventory__c item = new IC_Inventory__c();
        item.RecordTypeId=icInventoryRecordType.getrecordtypeId();
        item.Fax_Number__c='7787789874';
        item.Loaned_Out__c= false;
        insert item;
        return item;
    }
    
    
    
   /*
    * Create new Solution object 
    */
    public static IC_Solution__c createIC_Solution() { 
        return createIC_Solution('Loaner Device') ;
    }
    
   /*
    * Create new Solution object 
    */
    public static IC_Solution__c createIC_Solution(String recordTypeName) { 
        
        Case c = createCase();
        //RecordType icsRecordType = [select Id, Name from RecordType where SObjectType = 'IC_Solution__c' and Name = :recordTypeName];
        map<string,schema.recordtypeinfo> recordTypeInfo=ic_solution__c.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        schema.recordtypeinfo icsRecordType=recordTypeInfo.get(recordTypeName);
        IC_Solution__c solution  = new IC_Solution__c();
        solution.Case__c = c.id;
        solution.RecordTypeId =icsRecordType.getrecordtypeid();
        if(recordTypeName=='Internet Fax' || recordTypeName=='OOSCF'){
            Solution.OOS_TN__c='546546546';
            Solution.Login_ID__c='prishan@telus.com';
        }else if(recordTypeName=='Loaner Device'){
            Solution.Dealer_Agent_ID__c ='123123132';
            Solution.Delivery_Courier__c='Purolator';
            Solution.Delivery_Courier_Tracking_Number__c='78978979';
            Solution.Return_Courier__c='Purolator';
            Solution.Return_Courier_Tracking_Number__c='78978979';
            Solution.Shipping_House_No__c ='7555';
            Solution.Shipping_Street__c='Main St';
            Solution.Shipping_City__c='Vancouver';
            Solution.Shipping_Province__c='BC';
            Solution.Shipping_Postal_Code__c='V3R3F6'; 
            Solution.Agreement_Accepted__c=true;   
        }else if(recordTypeName=='Tethering'){
            Solution.Business_BAN__c='No Credit hand-off required';
        }
        Solution.Business_BAN__c='No Credit hand-off required';
        solution.Consumer_BAN__c = '23123123';
        solution.Provisioning_Date__c = Date.today();
        solution.Provisioning_End_Date__c=Date.Today();
            
        
        return solution;
    }
   
   /*
    * Create new Case object 
    */
    private static Case createCase() {
        Account acc = createAccount();
        Contact contact = new Contact(LastName='Test Contact ', accountId=acc.id );
        contact.Email='prishan.fernando@telus.com';
        insert contact;
        //RecordType caseRecordType = [select Id, Name from RecordType where SObjectType = 'Case' and Name ='SMB Care Interim Connectivity'];
        map<string,schema.recordtypeinfo> recordTypeInfo=case.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        schema.recordtypeinfo caseRecordType=recordTypeInfo.get('SMB Care Interim Connectivity');
        Case caseobj = new Case();
        caseobj.RecordTypeId = caseRecordType.getrecordtypeid();
        caseobj.Status='Open';
        caseobj.Origin='Client';
        caseobj.AccountId =acc.id;
        caseobj.Subject ='test case';
        caseobj.ContactId=contact.id;
        caseobj.Contact_Email__c='prishan.fernando@telus.com';
        insert caseobj;
        return caseobj;
    }
  
    
   /*
    * Create New Dealer type Provider which has deleivery method Customer Pick-up
    */
    private static IC_Device_Provider__c createDealerProvider() { 
        IC_Device_Provider__c provider = new IC_Device_Provider__c(Name = 'Test Account-Dealer');
        provider.ICS_Delivery_Method__c = 'Customer Pick-up';
        provider.City__c='Burnaby';
        provider.Province__c='BC';
        provider.Active__c=true;
        provider.ICS_Notification_Email__c = 'prishan.fernando@telus.com,mark.heimburger@telus.com';
        insert provider;
        return provider;
    }
   /*
    * Create Basic Account ( Used for when creating the Case) 
    */
    private static Account createAccount() { 
        //RecordType accRecordType = [select Id, Name from RecordType where SObjectType = 'Account' and Name ='RCID'];
        map<string,schema.recordtypeinfo> recordTypeInfo=account.sobjecttype.getdescribe().getrecordtypeinfosbyname();
        schema.recordtypeinfo accRecordType=recordTypeInfo.get('RCID');
        Account acc = new Account(Name = 'Test Account', RecordTypeId = accRecordType.getrecordtypeid());
        insert acc;
        return acc;
    }
   /*
    * Create New Warehouse type Provider which has deleivery method Courier to Customer
    */
     private static IC_Device_Provider__c createWarehouseProvider() { 
        IC_Device_Provider__c provider = new IC_Device_Provider__c(Name = 'Test Account-WareHouse');
        provider.ICS_Delivery_Method__c = 'Courier to Customer';
        provider.City__c='Burnaby';
        provider.Province__c='BC';
        provider.Active__c=true;
        provider.ICS_Notification_Email__c = 'prishan.fernando@telus.com,mark.heimburger@telus.com';
        insert provider;
        return provider;
    }
    
}